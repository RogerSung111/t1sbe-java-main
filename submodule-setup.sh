#!/bin/sh
cwd=$(pwd)
echo Current working directory: $cwd
cd src/main/java/com/tripleonetech/sbe
for module in 'common' 'game'
do
  echo "Linking $module submodule... [t1sbe-java-submodules-$module] -> [src/main/java/com/tripleonetech/sbe/$module]"
  ln -s ../../../../../../t1sbe-java-submodules-$module $module
done
