#!/bin/bash

project=$1
#[api, main]
profile=$2 

react_project_path="/home/vagrant/deploy/gitlab-ci/frontend"
client_folders="/home/vagrant/deploy/site"


cd ${react_project_path}
#remove existing contents
if [ -d ./dist || ./${profile}-frontend ]; then
	rm -r dist && rm -r ${profile}-frontend
fi

tar xzf ${project}.tar && mv dist ${profile}-frontend
if [ ! -d ${react_project_path}/${profile}-frontend ]; then
		echo "ERROR: Fail to decompress ${project}.tar"
	exit 0
fi

cd $client_folders
for dir in */; do
	cp -r ${react_project_path}/${profile}-frontend/ $dir
	echo "Copied project [${react_project_path}/${profile}-frontend] to [ $dir ]"
done