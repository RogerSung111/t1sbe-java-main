#!/bin/bash

#Init. parameters
service=$1 #[main, api]
server_id=$2
domain_name=$3
declare -a types=("war" "react")

template_dir="${script_dir}/template"
client_dir="${sites_dir}/${server_id}"
tomcat_dir="/home/vagrant/deploy/apache-tomcat"
tomcat_conf_dir="${tomcat_dir}/conf/Catalina"


if [ ! $# -eq 3 ]; then
	echo "ERROR: The number of parameters are wrong! The parameters should include following parameters: [ service, server_id, domain_name ]"
	exit 0
fi

#Create a HOST
curl -u admin:dcrajUg01 http://localhost:8080/host-manager/text/add\?name\=${domain_name}\&appBase\=${server_id}/${service}\&autoDeploy\=true\&manager\=true
if [ ! -d ${tomcat_dir}/${server_id}/${service} ]; then
	echo "ERROR:${tomcat_dir}/${server_id}/${service} is not existed"	
fi
if [ ! -d ${tomcat_conf_dir}/${domain_name} ]; then
	echo "ERROR:${tomcat_conf_dir}/${domain_name} is not existed"	
	exit 0	
fi

#Persist new host by adding new_host string instead of curling Tomcat API, because other spring projects will be restarted.
#curl -u admin:admin http://localhost:8080/host-manager/text/persist?name=${domain_name}

add_line_number="$(grep -n '</Engine>' "${tomcat_dir}/conf/server.xml" | awk '{print $1}' FS=":" )"
new_host="<Host appBase=\"${server_id}/${service}\" name=\"${domain_name}\"></Host>"
sed -i "${add_line_number}i\ \t${new_host}\n" ${tomcat_dir}/conf/server.xml
echo "LOG: Appended a new <Host> into server.xml: \n Host string: ${new_host}"


#Deploy WAR, VUE, Properties & XML
for type in "${types[@]}"
do
	if [ $type = "war" ]; then
		appBase="${server_id}/${service}"
		#2. Create conf/Catalina/Domain name/api.xml
		cd ${tomcat_conf_dir}/${domain_name}
		sed -e "s|{tomcatDir}|${tomcat_dir}|g" -e "s/{serverId}/${server_id}/g" -e "s/{service}/${service}/g" ${template_dir}/api.xml > api.xml
		echo "LOG: Created api.xml in /conf/Catalina/${domain_name}"

		#3. Create symbolic links for [ war, application-main/api.properties, application-db-main/api.properties, logback-spring.xml ]
		ln -s ${client_dir}/${service}.war ${tomcat_dir}/${appBase}/ROOT.war
		ln -s ${client_dir}/application-${service}.properties ${tomcat_dir}/${appBase}/application-${service}.properties
		ln -s ${client_dir}/application-db.properties ${tomcat_dir}/${appBase}/application-db.properties
		ln -s ${client_dir}/logback-spring.xml ${tomcat_dir}/${appBase}/logback-spring.xml

		echo "LOG : Created symbolic links for ROOT.war, application-*.properties & logback-spring.xml"
	else
		mkdir ${tomcat_dir}/${server_id}/${service}-frontend
		#1. Create conf/Catalina/Domain name/ROOT.xml
		cd ${tomcat_conf_dir}/${domain_name}
		sed -e "s|{tomcatDir}|${tomcat_dir}|g" -e "s/{serverId}/${server_id}/g" -e "s/{service}/${service}/g" ${template_dir}/ROOT.xml > ROOT.xml
		#2. Create symbolic links for VUE
		ln -s ${client_dir}/${service}-frontend/ ${tomcat_dir}/${server_id}/${service}-frontend/ROOT
		echo "LOG : Created symbolic links for React project [${service}]"

	fi
done
