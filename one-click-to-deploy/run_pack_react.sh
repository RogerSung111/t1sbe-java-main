#!/bin/bash

project=$1
server_ip=$2

client_id="T1SBE"
if [ "$project" = "game" ]; then
  client_id="T1SBE-player"
fi
client_secret="T1SBE-rocks"
deploy_to_server="vagrant@${server_ip}"
deploy_to_dir="/deploy/gitlab-ci/frontend"

if [ ! $# -eq 2 ]; then
	echo "ERROR: The number of parameters are wrong! The parameters should include following parameters: [ project, server_ip ]"
	exit 0
fi

if [ -d ./$project ]; then
	cd $project
	working_dir=$PWD
	echo "LOG: Working Dir: $working_dir" 
else
	echo "ERROR: $project is not existed"
	exit 0;	
fi

printf " { \"api\":\"/api\", \n" > src/config.json
printf " \"client_id\":\"$client_id\", \n" >> src/config.json
printf " \"client_secret\":\"$client_secret\" }" >> src/config.json
yarn run build release

if [ "$project" = "game" ]; then	
	mkdir dist
	cp -r build/* dist
fi

if [ ! -d "./dist" ]; then
	echo "ERROR: Fail to execute yarn run build release"
	exit 0
fi

tar -zcvf $project.tar dist/
if [ ! -f "$project.tar" ]; then
	echo "ERROR: Fail to compress $project.tar"
fi

scp $working_dir/${project}.tar ${deploy_to_server}:~${deploy_to_dir}

echo "LOG: Deployed ${project}.tar to ${deploy_to_server}:~${deploy_to_dir}"
