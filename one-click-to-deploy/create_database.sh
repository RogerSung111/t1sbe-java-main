#!/bin/bash

server_id=$1
db_ip=$2
db_user=$3
db_password=$4

if [ ! $# -eq 4 ]; then
	echo "ERROR: The number of parameters are wrong! The parameters should include following parameters: [ server_id, db_ip, db_user, db_password ]"
	exit 0
fi

echo "LOG: [DB] Parameter : { Server_id : $server_id }"
mysql -h $db_ip -u $db_user -p$db_password -P 3306 -AN -e "create database sbe_${server_id};"
DBEXISTS=$(mysql -h $db_ip -u $db_user -p$db_password -P 3306 --batch --skip-column-names -e "SHOW DATABASES LIKE 'sbe_${server_id}';" | grep "sbe_${server_id}" > /dev/null; echo "$?")
if [ ! $DBEXISTS -eq 0 ]; then
	echo "ERROR: Create database fail"
	exit 0
fi
echo "LOG: [DB] createing sbe_${server_id} database done"