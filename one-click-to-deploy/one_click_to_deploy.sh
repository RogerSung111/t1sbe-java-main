#!/bin/bash

server_id=$1
site_public_key=$2
site_private_key=$3
daemonserver_public_key=$4
player_prefix=$5
tomcat_server_ip=$6

#set paths
export script_dir=$(cd "$(dirname "$0")"; pwd)
cd $script_dir 
export sites_dir=$(cd "/home/vagrant/deploy/site"; pwd)

#set the path of packed front-end project 
frontend_project_path="/home/vagrant/deploy/gitlab-ci/frontend"

#set domain name
admin_domain_name="admin.${server_id}.totonestop.com"
www_domain_name="www.${server_id}.totonestop.com"

#set db info
db_ip="10.140.0.115"
db_user="t1sbe"
db_password="dcrajUg01"

#set tomcat server ip when there is no tomcat_server_ip using default server_ip [10.140.0.144]
if [ -z $tomcat_server_ip ]; then
	$tomcat_server_ip="10.140.0.144"
fi

#START BUILDING PROCESS
echo "LOG: Start deploying a new site : [$1]"
client_dir=${sites_dir}/${server_id}
	#stop one-click-to-deploy process when site is existed.
if [ -d ${client_dir} ]; then
	echo -e "ERROR: Server ID: [ $server_id ] is existed. Please check if build ${server_id} again?"
    exit 0
fi

	# Create client's folder in ${sites_dir}
mkdir $client_dir
echo "LOG: Create a site folder [ ${server_id} ], directory path: $client_dir"

#STEP1.Create database
bash $script_dir/create_database.sh $server_id $db_ip $db_user $db_password

#STEP2.Create  DNS for SBE(main, api) Using wildcard so don't have to create a new domain by using following script
# bash $script_dir/add_new_domain/add_new_live_zone.sh ${server_id}

#STEP3.Build WARs [main, api, scheduled] & VUEs[main, api]
	#3.1 rebuild deploy folders[war, react]
rm -r ${frontend_project_path}/*
	#3.2 trigger gitlab-ci
curl --request POST --form token="d41770dfaec7b2cddec83229cb3242" --form ref="prod" \
	--form "variables[serverId]=${server_id}" --form "variables[dbIp]=${db_ip}" \
	--form "variables[dbUser]=${db_user}" --form "variables[dbPassword]=${db_password}" \
	--form "variables[sitePrivateKey]=${site_private_key}" --form "variables[sitePublicKey]=${site_public_key}"\
	--form "variables[daemonserverPublicKey]=${daemonserver_public_key}" --form "variables[SKIP_TEST]=true" \
	--form "variables[playerPrefix]=${player_prefix}" --form "variables[serverIp]=${tomcat_server_ip}" \
	"https://git.smartbackend.com/api/v4/projects/189/trigger/pipeline"

war_existed="false"
vue_existed="false"
let waiting_time=1
while [ "$war_existed" == "false" ] || [ "$vue_existed" == "false" ]
do
	# wait 1 minute for git pipeline done, waiting will be up to 10 times
	echo "LOG: Waiting for packing WARs and React projects"
	sleep 1m
	
	#War
	if [ -f ${client_dir}/main.war ] && [ -f ${client_dir}/api.war ]; then
		echo "LOG: WARs are packed done"
		war_existed="true"
	fi
	
	if [ -f ${frontend_project_path}/backoffice.tar ] && [ -f ${frontend_project_path}/game.tar ]; then
		echo "LOG: React projects are packed done"
		vue_existed="true"
	fi
	
	if [ $waiting_time -gt 15 ]; then
		echo "ERROR: Fail to pack War & React, Waiting time: $waiting_time"
		exit 0
	fi
	waiting_time=$(( waiting_time + 1 ))
	
done

#STEP4. Checking packed react
	# backoffice.tar
cd $frontend_project_path
cp ${frontend_project_path}/backoffice.tar $client_dir
cd $client_dir
echo "Current working DIR : $client_dir" 
tar xzf backoffice.tar && mv dist main-frontend 
if [ ! -d ${client_dir}/main-frontend ]; then
	echo "ERROR: Fail to decompress backoffice.tar"
	exit 0
fi

	# game.tar
cd $packed_react
cp ${frontend_project_path}/game.tar $client_dir
cd $client_dir
echo "Current working DIR : $client_dir" 
tar xzf ${client_dir}/game.tar && mv dist api-frontend
if [ ! -d ${client_dir}/api-frontend ]; then
	echo "ERROR: Fail to decompress api-frontend.tar"
	exit 0
fi

#STEP5.Deploy Jars & Vue (api, main) on Tomcat server
	#Deploy admin
$script_dir/deploy_to_tomcat.sh main $server_id $admin_domain_name
	#Deploy WWW
$script_dir/deploy_to_tomcat.sh api $server_id $www_domain_name

#STEP6. Startup scheduled.jar
cd $client_dir
nohup java -jar `realpath scheduled.jar` > /dev/null 2>&1 &

echo "LOG: deployment done"

