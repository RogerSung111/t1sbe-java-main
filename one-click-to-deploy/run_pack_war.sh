#!/bin/bash

profile=$1
server_id=$2
db_ip=$3
db_user=$4
db_password=$5
player_prefix=$6
tomcat_server_ip=$7
site_private_key=$8
site_public_key=$9
daemonserver_public_key=$10


echo "LOG:[WAR] profile=$1, server_id=$2, db_ip=$3, db_user=$4, db_password=$5, player_prefix=$6, tomcat_server_ip=$7, " \
	"site_private_key= $8, site_public_key=$9, daemonserver_public_key=$10"

script_dir=$(cd "$(dirname "$0")"; pwd)
echo "LOG: [WAR] Run_Package_and_deploy : script_dir: $script_dir"

#deploy server
deploy_to_client_dir="/home/vagrant/deploy/site/${server_id}"
deploy_to_server="vagrant@${tomcat_server_ip}"

#Start packing process
cd $script_dir && cd ..

mvn clean package -P ${profile} -Dmaven.test.skip=true

if [ $? -eq 0 ]; then
  echo "LOG: [WAR] Packaging done"
  
  #application-db.properties
  printf "spring.datasource.url=jdbc:mysql://${db_ip}:3306/sbe_${server_id}?autoReconnect=true&useSSL=false&characterEncoding=UTF-8" > $script_dir/application-db.properties
  printf "\nspring.datasource.username=${db_user}" >> $script_dir/application-db.properties
  printf "\nspring.datasource.password=${db_password}" >> $script_dir/application-db.properties
  printf "\nflyway.schemas=sbe_${server_id}" >> $script_dir/application-db.properties
  echo "LOG: [WAR] scp $script_dir/application-db.properties =>>> ${deploy_to_server}:${deploy_to_client_dir}/application-db.properties"
  scp $script_dir/application-db.properties ${deploy_to_server}:${deploy_to_client_dir}/application-db.properties
  
  #application-common.properties
  sed -e "s/{player_prefix}/${player_prefix}/g" $script_dir/template/application-common.properties > $script_dir/application-common.properties
  scp $script_dir/application-common.properties ${deploy_to_server}:${deploy_to_client_dir}/application-common.properties
  
  #logback-spring.xml
  echo "LOG: [WAR] scp $script_dir/logback-spring.xml =>>> ${deploy_to_server}:${deploy_to_client_dir}/logback-spring.xml"
  scp $script_dir/template/logback-spring.xml ${deploy_to_server}:${deploy_to_client_dir}/logback-spring.xml
  
  #application-${profile}.properties ${profile}=@profile
  echo "LOG: [WAR] Writing current time and branch information to properties file ==> $script_dir/application-${profile}.properties"
  printf "\nconstant.swagger.last-updated=$(date)" > $script_dir/application-${profile}.properties
	# Logback related configs
  printf "\nlogback.name=${profile}\n" >> $script_dir/application-${profile}.properties
  printf "\nlogback.serverId=${server_id}\n" >> $script_dir/application-${profile}.properties
  
  # @profile=scheduled
  if [ ${profile} == "scheduled" ]; then
    printf "\nlogging.config=file:./logback-spring.xml\n" >> $script_dir/application-${profile}.properties
   	echo "LOG: [WAR] scp $script_dir/application-${profile}.properties =>>> ${deploy_to_server}:${deploy_to_client_dir}/application-db-${profile}.properties"
  	cat $script_dir/template/application-scheduled.properties >> $script_dir/application-${profile}.properties
  	scp $script_dir/application-${profile}.properties ${deploy_to_server}:${deploy_to_client_dir}/application-${profile}.properties
  	echo "LOG: [WAR] scp target/${profile}.jar =>>> ${deploy_to_server}:${deploy_to_client_dir}/${profile}.jar"
  	scp target/${profile}.jar ${deploy_to_server}:${deploy_to_client_dir}/${profile}.jar
  
  # @profile=main 
  elif [ ${profile} == "main" ]; then
    printf "\nlogging.config=file:$2/${profile}/logback-spring.xml\n" >> $script_dir/application-${profile}.properties
    cat $script_dir/application-${profile}.properties $script_dir/template/application-${profile}.properties 
    sed -e "s|{site_public_key}|${site_public_key}|g" -e "s|{site_private_key}|${site_private_key}|g" -e "s|{daemonserver_public_key}|${daemonserver_public_key}|g" \
    	-e "s/{server_id}/${server_id}/g" $script_dir/template/application-${profile}.properties >> $script_dir/application-${profile}.properties
  	scp $script_dir/application-${profile}.properties ${deploy_to_server}:${deploy_to_client_dir}/application-${profile}.properties
  	echo "LOG: [WAR] scp $script_dir/application-${profile}.properties =>>> ${deploy_to_server}:${deploy_to_client_dir}/application-db-${profile}.properties"
  	scp $script_dir/template/application-redis.properties ${deploy_to_server}:${deploy_to_client_dir}/application-redis.properties
  	echo "LOG: [WAR] scp $script_dir/application-redis.properties =>>> ${deploy_to_server}:${deploy_to_client_dir}/application-redis.properties"
  	scp target/${profile}.war ${deploy_to_server}:${deploy_to_client_dir}/${profile}.war
   	echo "LOG: [WAR] scp target/${profile}.war =>>> ${deploy_to_server}:${deploy_to_client_dir}/${profile}.war"
  
  # @profile=api
  else
    printf "\nlogging.config=file:$2/${profile}/logback-spring.xml\n" >> $script_dir/application-${profile}.properties
 	echo "LOG: [WAR] scp $script_dir/application-${profile}.properties =>>> ${deploy_to_server}:${deploy_to_client_dir}/application-db-${profile}.properties"
    scp $script_dir/application-${profile}.properties ${deploy_to_server}:${deploy_to_client_dir}/application-${profile}.properties
  	echo "LOG: [WAR] scp target/${profile}.war =>>> ${deploy_to_server}:${deploy_to_client_dir}/${profile}.war"
  	scp target/${profile}.war ${deploy_to_server}:${deploy_to_client_dir}/${profile}.war
  fi
  
  echo "LOG: [WAR] Deployment done"
else
  echo "ERROR: Fail to run mvn package"
fi
