package com.tripleonetech.sbe.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class DomainConfigMapperTest extends BaseTest {

    @Autowired
    private DomainConfigMapper domainConfigMapper;
    
    @Test
    public void testList() {
        
        assertTrue(domainConfigMapper.list().size() > 0);
        
    }
}
