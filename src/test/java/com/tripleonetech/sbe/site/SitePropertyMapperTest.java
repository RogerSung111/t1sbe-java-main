package com.tripleonetech.sbe.site;

import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.site.SitePropertyKey.LiveChatInfoEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SitePropertyMapperTest extends BaseMainTest {
    @Autowired
    private SitePropertyMapper sitePropertyMapper;

    private String liveChatUrl = "http://tripleonetech.net";
    private String liveChatCode = "test-code";

    @Test
    public void testGetString() {
        assertEquals(liveChatUrl, sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_URL.toString()).getValue());
        assertEquals(liveChatCode, sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_CODE.toString()).getValue());
    }

    @Test
    public void testGetSitePropertyKey() {
        assertEquals(liveChatUrl, sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_URL).getValue());
        assertEquals(liveChatCode, sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_CODE).getValue());
    }

    @Test
    public void testListListOfString() {
        List<SiteProperty> list = sitePropertyMapper.list(
                Arrays.asList(LiveChatInfoEnum.LIVE_CHAT_URL.toString(),
                        LiveChatInfoEnum.LIVE_CHAT_CODE.toString()));
        assertEquals(2, list.size());
        list.forEach(property -> {
            if (property.getKey().equals(LiveChatInfoEnum.LIVE_CHAT_URL.name())) {
                assertEquals(liveChatUrl, property.getValue());
            } else {
                assertEquals(liveChatCode, property.getValue());
            }
        });
    }

    @Test
    public void testListSitePropertyKeyArray() {
        List<SiteProperty> list = sitePropertyMapper.list(LiveChatInfoEnum.values());
        assertEquals(2, list.size());
        list.forEach(property -> {
            if (property.getKey().equals(LiveChatInfoEnum.LIVE_CHAT_URL.name())) {
                assertEquals(liveChatUrl, property.getValue());
            } else {
                assertEquals(liveChatCode, property.getValue());
            }
        });
    }

    @Test
    public void testUpdate() {
        SiteProperty property = new SiteProperty();
        property.setKey(LiveChatInfoEnum.LIVE_CHAT_CODE);
        property.setValue("testUpdateInUnitTest");
        sitePropertyMapper.update(property);

        assertEquals(property.getValue(), sitePropertyMapper.get(property.getKey()).getValue());
    }

    @Test
    public void testSaveOrInsert() {
        SiteProperty property = new SiteProperty();
        property.setKey("myTestKey");
        property.setValue("testUpdateInUnitTest");
        sitePropertyMapper.saveOrInsert(property);

        assertEquals(property.getValue(), sitePropertyMapper.get(property.getKey()).getValue());
    }
}
