package com.tripleonetech.sbe.cms;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class ContentStoreServiceTest extends BaseTest {

    @Autowired
    ContentStoreService storeService;
    @Autowired
    ContentStoreMapper storeMapper;

    @Test
    public void testGetAll() {
        Map<String, ContentStoreView> all = storeService.getAll();
        assertEquals(2, all.size());
        assertEquals(ContentStoreTypeEnum.TEXT, all.get("key1").getType());
        assertEquals("content1", all.get("key1").getContent());
    }

    @Test
    public void testGet() {
        ContentStoreView storeEntry2 = storeService.get("key2");
        assertEquals(ContentStoreTypeEnum.TEXT, storeEntry2.getType());
        assertEquals("content2", storeEntry2.getContent());
    }

    @Test
    public void testSave() {
        ContentStoreView storeEntry3 = new ContentStoreView();
        storeEntry3.setType(ContentStoreTypeEnum.TEXT);
        storeEntry3.setContent("content3");
        storeService.save("key3", storeEntry3);

        List<ContentStore> list = storeMapper.list();
        assertEquals(3, list.size());

        ContentStoreView storeEntry = storeMapper.get("key3");
        assertEquals(ContentStoreTypeEnum.TEXT, storeEntry.getType());
        assertEquals("content3", storeEntry.getContent());
    }

}
