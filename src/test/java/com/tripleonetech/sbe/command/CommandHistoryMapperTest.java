package com.tripleonetech.sbe.command;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CommandHistoryMapperTest extends BaseTest {

    @Autowired
    private CommandHistoryMapper commandHistoryMapper;

    private String testContent = "[{\"id\":3,\"playerId\":2,\"gameApiId\":1,\"gameApiCode\":\"MockGame\",\"gameCode\":\"FD1\",\"playerUsername\":\"test002\",\"bet\":50.000,\"win\":0.000,\"loss\":50.000,\"payout\":0.000,\"betTimeHour\":\"2018-09-20T13:00:00\",\"dirty\":true,\"createdAt\":\"2018-09-20T17:36:57.97\",\"updatedAt\":\"2020-06-01T07:13:18\",\"gameTypeId\":1,\"gameTypeName\":null,\"currency\":\"CNY\"},{\"id\":4,\"playerId\":3,\"gameApiId\":1,\"gameApiCode\":\"MockGame\",\"gameCode\":\"FD1\",\"playerUsername\":\"test003\",\"bet\":150.000,\"win\":100.000,\"loss\":200.000,\"payout\":0.000,\"betTimeHour\":\"2018-09-20T12:00:00\",\"dirty\":true,\"createdAt\":\"2018-09-20T17:36:57.971\",\"updatedAt\":\"2020-06-01T07:13:18\",\"gameTypeId\":1,\"gameTypeName\":null,\"currency\":\"CNY\"}]";

    @Before
    public void init() {

    }

    @Test
    public void testGetCommandHistory() {
        int id = 1;
        CommandHistory commandHistory = commandHistoryMapper.get(id);
        assertNotNull(commandHistory);
        assertNotNull(commandHistory.getContent());
        assertEquals(CommandHistory.CommandTypeEnum.PUBLISH, commandHistory.getCommandType());
        assertEquals("method name should be equals GAME_LOG_HOURLY_REPORT",
                T1DaemonMethodEnum.GAME_LOG_HOURLY_REPORT.toString(), commandHistory.getMethodName());
    }

    @Test
    public void testInsert() {
        CommandHistory mockCommandHistory = new CommandHistory();
        mockCommandHistory.setMethodName(T1DaemonMethodEnum.GAME_LOG_HOURLY_REPORT.toString());
        mockCommandHistory.setCommandType(CommandHistory.CommandTypeEnum.PUBLISH);
        mockCommandHistory.setContent(testContent);

        int count = commandHistoryMapper.insert(mockCommandHistory);
        assertEquals("table command_history should be inserted one record", 1, count);
    }

}
