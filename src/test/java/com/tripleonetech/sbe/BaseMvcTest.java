package com.tripleonetech.sbe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.common.web.RestResponseCodeEnum;

import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

/**
 *
 * This base class defines a MockMvc object alongside with util response verification methods for ease of writing
 * mvc-related unit tests.
 *
 * Note: Instead of extending this base class, you may want to use BaseApiTest and BaseDevTest which both defines
 * their own logic of obtaining a access token.
 *
 * @author Yunfei<yunfei.dev@tripleonetech.net>
 *         Created on 15 Aug 2019
 */
@AutoConfigureMockMvc
public abstract class BaseMvcTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    protected abstract String obtainAccessToken();

    // perform a mock mvc call and return the result as a parsed JsonNode
    protected JsonNode mockMvcPerform(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                requestBuilder
                        .header("Authorization", "Bearer " + obtainAccessToken())
                        .header("Content-Type", "application/json")
                        .with(csrf())
                        .header("Accept-Language", "en-US"))
                .andExpect(status().isOk()).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(result);
    }

    // perform a mock mvc call without providing authorization header
    // URL under this test should not require login
    protected JsonNode mockMvcPerformNoLogin(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                requestBuilder
                        .header("Content-Type", "application/json")
                        .with(csrf())
                        .header("Accept-Language", "en-US"))
                .andExpect(status().isOk()).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(result);
    }

    // perform a mock mvc call with and return the result as byte array
    protected byte[] mockMvcPerformDownloadFile(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                requestBuilder
                        .header("Authorization", "Bearer " + obtainAccessToken())
                        .header("Content-Type", "application/json")
                        .with(csrf())
                        .header("Accept-Language", "en-US"))
                .andExpect(status().isOk()).andReturn();
        return mvcResult.getResponse().getContentAsByteArray();
    }

    protected void assertOk(JsonNode node) {
        assertEquals(RestResponseCodeEnum.OK.getCode(), node.get("code").asInt());
    }

    protected void assertIPBlocked(JsonNode node) {
        assertEquals(RestResponseCodeEnum.IP_RESTRICTED.getCode(), node.get("code").asInt());
    }

    protected void assertMultiStatus(JsonNode node) {
        assertEquals(RestResponseCodeEnum.MULTI_STATUS.getCode(), node.get("code").asInt());
    }

    // Return code starts with '4'
    protected void assertClientError(JsonNode node) {
        char clientErrorStartChar = Integer.toString(RestResponseCodeEnum.GENERAL_CLIENT_ERROR.getCode()).charAt(0);
        assertTrue(node.get("code").toString().startsWith("" + clientErrorStartChar));
    }

    protected void assertOperationFailed(JsonNode node) {
        assertEquals(RestResponseCodeEnum.OPERATION_FAILED.getCode(), node.get("code").asInt());
    }

    protected void assertInsufficientBalance(JsonNode node) {
        assertEquals(RestResponseCodeEnum.INSUFFICIENT_BALANCE.getCode(), node.get("code").asInt());
    }

    protected void assertApiFailed(JsonNode node) {
        assertEquals(RestResponseCodeEnum.API_OPERATION_FAILED.getCode(), node.get("code").asInt());
    }

    protected void assertInvalidParameter(JsonNode node) {
        assertEquals(RestResponseCodeEnum.INVALID_PARAMETER.getCode(), node.get("code").asInt());
    }

    protected void assertInvalidStatus(JsonNode node) {
        assertEquals(RestResponseCodeEnum.INVALID_STATUS.getCode(), node.get("code").asInt());
    }

    protected void assertInvalidPassword(JsonNode node) {
        assertEquals(RestResponseCodeEnum.INVALID_PASSWORD.getCode(), node.get("code").asInt());
    }

    protected void assertUsernameAlreadyExist(JsonNode node) {
        assertEquals(RestResponseCodeEnum.USERNAME_ALREADY_EXISTS.getCode(), node.get("code").asInt());
    }

    protected void assertAccessForbidden(JsonNode node) {
        assertEquals(RestResponseCodeEnum.UNAUTHORIZED.getCode(), node.get("code").asInt());
    }

    protected void assertEntityNotFound(JsonNode node) {
        assertEquals(RestResponseCodeEnum.NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertPlayerNotFound(JsonNode node) {
        assertEquals(RestResponseCodeEnum.PLAYER_NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertPlayerBlocked(JsonNode node) {
        assertEquals(RestResponseCodeEnum.PLAYER_BLOCKED.getCode(), node.get("code").asInt());
    }

    protected void assertCurrencyNotAvailable(JsonNode node) {
        assertEquals(RestResponseCodeEnum.CURRENCY_NOT_AVAILABLE.getCode(), node.get("code").asInt());
    }

    protected void assertPlayerGroupNotFound(JsonNode node) {
        assertEquals(RestResponseCodeEnum.PLAYER_GROUP_NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertPlayerBankAccountNotFound(JsonNode node) {
        assertEquals(RestResponseCodeEnum.PLAYER_BANK_ACCOUNT_NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertPlayerAnnouncementNotFound(JsonNode node) {
        assertEquals(RestResponseCodeEnum.PLAYER_ANNOUNCEMENT_NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertBadWalletRequest(JsonNode node) {
        assertEquals(RestResponseCodeEnum.WALLET_NOT_FOUND.getCode(), node.get("code").asInt());
    }

    protected void assertDataSuccess(JsonNode node) {
        assertTrue(node.get("success").asBoolean());
    }

    protected void assertDataFail(JsonNode node) {
        assertFalse(node.get("success").asBoolean());
    }

    protected MockMvc getMockMvc() {
        return this.mockMvc;
    }

    protected static final Object unwrapProxy(Object bean) throws Exception {
        /*
         * If the given object is a proxy, set the return value as the object
         * being proxied, otherwise return the given object.
         */
        if (AopUtils.isAopProxy(bean) && bean instanceof Advised) {
            Advised advised = (Advised) bean;
            bean = advised.getTargetSource().getTarget();
        }
        return bean;
    }
}
