package com.tripleonetech.sbe.game;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class GameNameMapperTest extends BaseTest {

    @Autowired
    private GameNameMapper gameNameMapper;

    @Test
    public void getGameByGameCodeTest() {
        int gameTypeId = 3;
        List<GameName> gameNames = gameNameMapper.getGamesByType("T1GPT", gameTypeId, "en-US", "en-US");
        assertNotNull(gameNames);
    }

    @Test
    public void listTest() {
        GameName condition = new GameName();
        condition.setGameApiCode("T1GPT");

        GameName gameName = gameNameMapper.list(condition).get(0);
        assertEquals(condition.getGameApiCode(), gameName.getGameApiCode());
    }

    @Test
    public void listByIdsTest() {
        List<Integer> ids = Arrays.asList(4792, 4615);

        List<GameName> gameNames = gameNameMapper.listByIds(ids);
        assertEquals("T1GPT", gameNames.get(0).getGameApiCode());
        assertEquals("T1GPT", gameNames.get(1).getGameApiCode());
    }

    @Test
    public void testUpdateEnable() {
        List<Integer> data = Collections.singletonList(4792);
        gameNameMapper.updateEnable(false, data);

        GameName condition = new GameName();
        condition.setId(4792);
        GameName gameName = gameNameMapper.list(condition).get(0);
        assertFalse(gameName.getUserEnabled());
    }

    @Test
    public void testQuery() {
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameApiCode("T1GPT");
        formObj.setGameName("ba");

        List<GameNameView> gameNameViews = gameNameMapper.query(formObj, 1, 10);
        GameNameView result = gameNameViews.get(0);
        assertEquals(formObj.getGameApiCode(), result.getGameApiCode());
        assertNotNull(result.getGameName());
        assertNotNull(result.getGameTypeName());
    }

    @Test
    public void testUpsertGameName() {
        GameName gameName = new GameName();
        GameNameQueryForm formObj = new GameNameQueryForm();
        List<GameNameView> gameNameViews;

        List<GameName> gameNames = gameNameMapper.list(null);
        GameName existGameName = gameNames.get(0);
        gameName.setGameApiCode(existGameName.getGameApiCode());
        gameName.setGameCode(existGameName.getGameCode());
        gameName.setGameTypeId(existGameName.getGameTypeId());
        gameName.setMobile(!existGameName.isMobile());
        gameName.setWeb(!existGameName.isWeb());

        gameNameMapper.upsertGameName(gameName);

        GameName gameNameResult = gameNameMapper.getGameByCode(gameName.getGameApiCode(), gameName.getGameCode(), "en-US", "en-US");
        assertEquals(existGameName.getId(), gameNameResult.getId());
        assertEquals(existGameName.getGameApiCode(), gameNameResult.getGameApiCode());
        assertNotEquals(existGameName.isMobile(), gameNameResult.isMobile());
        assertNotEquals(existGameName.isWeb(), gameNameResult.isWeb());

        gameName = new GameName();
        gameName.setGameApiCode("T1GPT");
        gameName.setGameCode("TEST");
        gameName.setGameTypeId(1);

        gameNameMapper.upsertGameName(gameName);

        formObj.setGameApiCode("T1GPT");
        formObj.setGameCode("TEST");

        gameNameViews = gameNameMapper.query(formObj, 1, 10);
        assertFalse(gameNameViews.isEmpty());

        GameNameView result = gameNameViews.get(0);
        assertEquals(formObj.getGameApiCode(), result.getGameApiCode());
        assertNotNull(result.getGameTypeName());
    }

    @Test
    public void testUpsertBatchGameNameI18n() {
        List<GameName> gameNames = gameNameMapper.getGamesByType("T1GPT", 1, null, null);
        GameName existGameName = gameNames.get(0);

        List<GameNameI18n> gameNameI18nList = new ArrayList<>();
        GameNameI18n gameNameI18n = new GameNameI18n();
        gameNameI18n.setGameNameId(existGameName.getId());
        gameNameI18n.setLocale("en-US");
        gameNameI18n.setName("TEST");
        gameNameI18nList.add(gameNameI18n);

        GameNameI18n gameNameI18n1 = new GameNameI18n();
        gameNameI18n1.setGameNameId(existGameName.getId());
        gameNameI18n1.setLocale("test-locale");
        gameNameI18n1.setName("TESTNAME");
        gameNameI18nList.add(gameNameI18n1);

        gameNameMapper.upsertBatchGameNameI18n(gameNameI18nList);

        GameName updatedGameName = gameNameMapper.getGameByCode(existGameName.getGameApiCode(), existGameName.getGameCode(), "en-US", "en-US");
        assertEquals("TEST", updatedGameName.getGameNameI18n().getName());
        updatedGameName = gameNameMapper.getGameByCode(existGameName.getGameApiCode(), existGameName.getGameCode(), "test-locale", "en-US");
        assertEquals("TESTNAME", updatedGameName.getGameNameI18n().getName());
    }

    @Test
    public void testBatchUpdateSort() {
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameApiCode("T1GPT");

        List<GameNameView> gameNameViews = gameNameMapper.query(formObj, 1, 10);
        GameNameView originalLast = gameNameViews.get(gameNameViews.size() - 1);

        GameName gameName = new GameName();
        BeanUtils.copyProperties(originalLast, gameName);
        gameName.setSort(999); // change sort to first

        gameNameMapper.batchUpdateSort(Collections.singletonList(gameName));

        gameNameViews = gameNameMapper.query(formObj, 1, 10);
        assertEquals(originalLast.getGameApiCode(), gameNameViews.get(0).getGameApiCode());
        assertEquals(originalLast.getGameCode(), gameNameViews.get(0).getGameCode());
    }
}
