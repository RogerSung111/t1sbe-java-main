package com.tripleonetech.sbe.game.info;

import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;

import static org.junit.Assert.assertEquals;

public class GameInfoMapperTest extends BaseTest {

    @Autowired
    private GameInfoMapper gameInfoMapper;

    @Test
    public void testGetGameInfoList() {

        GameInfoQueryForm queryForm = new GameInfoQueryForm();
        queryForm.setFeatured(false);
        queryForm.setWeb(true);
        queryForm.setFeatured(true);
        queryForm.setGameApiId(1);
        queryForm.setGameTypeId(1);
        Locale.setDefault(Locale.CHINA);
        List<GameInfoView> gameInfos = gameInfoMapper.getGameInfoList(null, SiteCurrencyEnum.CNY, queryForm, "zh-CN", "en-US",
                queryForm.getPage(), queryForm.getLimit());
        assertEquals("There should be 4 records", 4, gameInfos.size());
    }

}
