package com.tripleonetech.sbe.game.config;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.Md5Utils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GameApiConfigMapperTest extends BaseTest {

    private final String[] MD5_FILEDS = {
            "enabled",
            "code",
            "currency",
            "name",
            "syncEnabled",
            "meta",
            "remark"};

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    
    private final int MOCKGAME = 1;
    private final int SLOT_GAME_TYPE_ID = 1;
    @Rule
    public TestName name = new TestName();

    private GameApiConfig mockGameConfig;
    private String methodName;
    private final static String GAME_API_NAME_MOCK_GAME_3 = "MockGame3";
    @Before
    public void setUp() throws Exception {
        methodName = name.getMethodName();
        mockGameConfig = new GameApiConfig();
        mockGameConfig.setName(methodName);
        mockGameConfig.setCurrency(SiteCurrencyEnum.CNY);
        mockGameConfig.setCode(GAME_API_NAME_MOCK_GAME_3);
        mockGameConfig.setSyncEnabled(false);
        mockGameConfig.setStatus(ApiConfigStatusEnum.ENABLED);
        mockGameConfig.setRemark("MockGame3 test");
        Map<String, String> fields = mockGameConfig.getFields();
        fields.put("url", methodName);
        fields.put("merchant_id", methodName);
        fields.put("username", methodName);

        mockGameConfig.setMd5(Md5Utils.generateMd5Sum(mockGameConfig, MD5_FILEDS, null, null));
    }

    @Test
    public void testGetGameApiConfig() {
        int apiId = 1;
        GameApiConfig mgApiConfig = gameApiConfigMapper.get(apiId);
        assertNotNull(mgApiConfig);
        assertEquals("MockGame", mgApiConfig.getCode());
        assertEquals("http://api.mockgame.com/service", mgApiConfig.get("url"));
        assertTrue(!mgApiConfig.isSyncEnabled());
        assertTrue(mgApiConfig.getStatus().equals(ApiConfigStatusEnum.ENABLED));
    }

    @Test
    public void testListGameApiConfig() {
        List<GameApiConfig> apiConfigs = gameApiConfigMapper.list();
        assertTrue(apiConfigs != null && apiConfigs.size() >= 2);
    }

    @Test
    public void testListGameApiConfigByCode() {
        List<String> availableGameApiCodes = new ArrayList<>();
        availableGameApiCodes.add("MockGame"); // id = 1, disabled, 2 currencies
        availableGameApiCodes.add("T1GCQ9"); // id = 15, enabled
        boolean showDeletedApi = false;
        List<GameApiConfig> apiConfigs = gameApiConfigMapper.listGameApiConfigByCode(availableGameApiCodes, showDeletedApi);
        assertTrue(apiConfigs != null);
        assertEquals(3, apiConfigs.size());
        showDeletedApi = true;
        apiConfigs = gameApiConfigMapper.listGameApiConfigByCode(availableGameApiCodes, showDeletedApi);
        assertTrue(apiConfigs != null);
        assertEquals(3, apiConfigs.size());
    }

    @Test
    public void testInsert() {
        int count = gameApiConfigMapper.insert(mockGameConfig);
        assertTrue("table game_api should be inserted one record", count == 1);
    }

    @Test
    public void testUpdate() {
        gameApiConfigMapper.insert(mockGameConfig);
        // simulate the case where not all fields are given
        mockGameConfig.setSyncEnabled(null);
        mockGameConfig.getFields().put("test", "update");
        mockGameConfig.setMd5(Md5Utils.generateMd5Sum(mockGameConfig, MD5_FILEDS, null, null));
        int count = gameApiConfigMapper.update(mockGameConfig);
        assertTrue("table game_api should be updated one record", count == 1);
        mockGameConfig = gameApiConfigMapper.get(mockGameConfig.getId());
        assertEquals("the field 'test' of meta should be updated into meta", "update", mockGameConfig.get("test"));
    }

    @Test
    public void testUpdateStatus() {
        gameApiConfigMapper.insert(mockGameConfig);

        mockGameConfig.setStatus(ApiConfigStatusEnum.ENABLED);
        mockGameConfig.setMd5(Md5Utils.generateMd5Sum(mockGameConfig, MD5_FILEDS, null, null));
        int count = gameApiConfigMapper.updateStatus(mockGameConfig);
        assertTrue("table game_api should be updated one record", count == 1);
        mockGameConfig = gameApiConfigMapper.get(mockGameConfig.getId());
        assertTrue("the status should be updated to ture", mockGameConfig.getStatus().equals(ApiConfigStatusEnum.ENABLED));
    }
    
    
    @Test
    public void testUpsert() {
        GameApiConfig gameApiConfig = gameApiConfigMapper.get(MOCKGAME);
        gameApiConfig.setStatus(ApiConfigStatusEnum.DELETED);
        
        List<GameApiConfig> gameApiConfigs = new ArrayList<>();
        gameApiConfigs.add(gameApiConfig);
        gameApiConfigs.add(mockGameConfig);
        assertTrue(gameApiConfigs.size() == 2);
        gameApiConfigMapper.upsert(gameApiConfigs);
        
        //Update
        GameApiConfig mockGameApiConfig = gameApiConfigMapper.getIgnoreDeleted(MOCKGAME);
        assertNotNull(mockGameApiConfig);
        assertTrue(mockGameApiConfig.getStatus().equals(ApiConfigStatusEnum.DELETED));
        
        //Insert
        List<GameApiConfig> newMockGameApiConfig = gameApiConfigMapper.listByCode(GAME_API_NAME_MOCK_GAME_3);
        assertNotNull(newMockGameApiConfig);

    }
    
    @Test
    public void testListByCode() {
        String gameApiCode = "T1GAGIN";
        List<GameApiConfig> apiConfigs = gameApiConfigMapper.listByCode(gameApiCode);
        assertNotNull(apiConfigs);
        apiConfigs.forEach(apiConfig -> {
            assertTrue(apiConfig.getCode().equals(gameApiCode));
        });
    }

    @Test
    public void testSetAndListGameListLastUpdatedTime() {
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        gameApiConfigMapper.setGameListLastUpdatedTime("T1GPT", now);
        List<GameApiConfig> apiConfigs = gameApiConfigMapper.listT1GGameListLastUpdatedDateTime();
        String lastUpdatedTime = apiConfigs.stream().filter(x->x.getCode().equals("T1GPT")).findAny().map(GameApiConfig::getGamelistLastUpdatedDatetime).orElse(null);
        assertEquals(DateUtils.getLocalDateTimeFromString(lastUpdatedTime), now);
    }
    
    @Test
    public void testListGameApiByGameTypeId() {
        List<GameApiConfig> gameApiConfigs = gameApiConfigMapper.listGameApiByGameTypeId(SLOT_GAME_TYPE_ID);
        assertTrue(gameApiConfigs.size() > 0);
        GameApiConfig gameApiConfig = gameApiConfigs.get(0);
        //Ensure that following columns will be found out.
        assertNotNull(gameApiConfig.getId());
        assertNotNull(gameApiConfig.getCode());
        assertNotNull(gameApiConfig.getName());
        
    }

}
