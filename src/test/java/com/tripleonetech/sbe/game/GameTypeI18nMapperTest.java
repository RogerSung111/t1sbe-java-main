package com.tripleonetech.sbe.game;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GameTypeI18nMapperTest extends BaseTest {

    @Autowired
    private GameTypeI18nMapper gameTypeI18nMapper;

    private List<GameTypeI18n> mockGameTypeI18nList;

    @Before
    public void setUp() throws Exception {
        mockGameTypeI18nList = gameTypeI18nMapper.listByGameType(1);
    }

    @Test
    public void testUpsert() {
        Integer mockGameTypeId = mockGameTypeI18nList.get(0).getGameTypeId();

        // Before upsert
        for (GameTypeI18n gameTypeI18n : mockGameTypeI18nList) {
            String mockNameI18n = gameTypeI18n.getLocale() + "mock name for unit test";
            assertNotEquals(mockNameI18n, gameTypeI18n.getName());
            gameTypeI18n.setName(mockNameI18n);
        }

        gameTypeI18nMapper.upsert(mockGameTypeId, mockGameTypeI18nList);

        // After upsert
        List<GameTypeI18n> datas = gameTypeI18nMapper.listByGameType(mockGameTypeId);
        for (GameTypeI18n gameTypeI18n : datas) {
            String mockNameI18n = gameTypeI18n.getLocale() + "mock name for unit test";
            assertEquals(mockNameI18n, gameTypeI18n.getName());
        }
    }

    @Test
    public void testListByGameType() {
        Integer mockGameTypeId = 1;

        List<GameTypeI18n> datas = gameTypeI18nMapper.listByGameType(mockGameTypeId);

        for (GameTypeI18n gameTypeI18n : datas) {
            assertEquals(mockGameTypeId, gameTypeI18n.getGameTypeId());
        }
    }

}
