package com.tripleonetech.sbe.game.integration.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.game.integration.GameLogApiResult;
import com.tripleonetech.sbe.game.log.impl.GameLogT1;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;

@Ignore("This testing is without mockito")
public class GameApiT1GImplTest extends BaseTest {
    //PT:9, TF:16
    private static final int T1_PT_API_ID = 9;
    private static final int TEST002_PLAYER_ID = 18;

    @Autowired
    private GameApiFactory apiFactory;
    @Autowired
    private PlayerMapper playerMapper;

    private GameApiT1G api;
    private Player player;

    
    @Before
    public void setup() {
        api = (GameApiT1G) apiFactory.getById(T1_PT_API_ID);
        player = playerMapper.get(TEST002_PLAYER_ID);
        api.fetchAuthToken();
    }

    
    @Test
    public void testLaunchGame() {
        api = (GameApiT1G) apiFactory.getById(T1_PT_API_ID);
        Map<String, String> options = new HashMap<>();
        options.put("Accept-Language", "zh-cn");
        String mode = "real";
        String platform = "pc";
        GameName gameName = new GameName();
        gameName.setGameCode("bob");
        GameApiResult result = api.launchGame(player, gameName, platform, mode, "zh-CN");
        assertTrue(result.isSuccess());
        
    }
    
    @Test
    public void testRegister() {
        ApiResult result = api.register(player);
        assertTrue(result.isSuccess());
    }
    
    @Test
    public void testGetBalance() throws UnsupportedOperationException, IOException {

        GameApiResult result = api.getBalance(player);
        assertTrue(result.isSuccess());
    }
    
    @Test
    public void testDeposit() throws UnsupportedOperationException, IOException {

        BigDecimal amount = new BigDecimal("50");
        GameApiResult result = api.deposit(player, amount);
        assertTrue(result.isSuccess());
    }
    
    @Test
    public void testWithdraw() throws UnsupportedOperationException, IOException {

        BigDecimal amount = new BigDecimal("50");
        GameApiResult result = api.withdraw(player, amount);
        assertTrue(result.isSuccess());
    }
    
    @Test
    public void testSyncGameLogs() throws ClientProtocolException, IOException {

        GameLogApiResult<GameLogT1> result = api.syncGameLog(LocalDateTime.now().minusMinutes(60), LocalDateTime.now());
        assertTrue(result.isSuccess());
       
    }
    
}
