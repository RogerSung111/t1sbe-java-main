package com.tripleonetech.sbe.game.integration.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.game.integration.GameLogApiResult;
import com.tripleonetech.sbe.game.log.impl.GameLogT1;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;

public class GameApiT1GTest extends BaseTest {
    private static final int T1G_BBIN_API_ID = 6;
    private static final int TEST002_PLAYER_ID = 2;

    @Autowired
    private GameApiFactory apiFactory;
    @Autowired
    private PlayerMapper playerMapper;

    private GameApiT1G api;
    private Player player;
    
    @Autowired
    private WalletMapper walletMapper;
    
    @Before
    public void setup() {
        api = (GameApiT1G) apiFactory.getById(T1G_BBIN_API_ID);
        player = playerMapper.get(TEST002_PLAYER_ID);

        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockTokenResponse }));
        // this method will only call http service when token cache is empty
        // isolate the call of fetch token here to avoid interfering with tests
        api.fetchAuthToken();
    }

    @Test
    public void testRegister() {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockRegisterResponse }));

        ApiResult result = api.register(player);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testFailedDeposit() throws UnsupportedOperationException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockDepositFailedResponse }));
        Wallet befoeDepositMainWallet = walletMapper.getMainWallet(player.getId());
        BigDecimal amount = new BigDecimal("2000");
        GameApiResult result = api.deposit(player, amount);
        GameApiT1GCommonResponse commonResponse = (GameApiT1GCommonResponse)result.getParam("response");
        assertTrue(result.isSuccess());
        assertEquals("unknown", commonResponse.getDetail("transfer_status"));
        
        //Balance(Main Wallet) should not be changed.
        Wallet afterDepositMainWallet = walletMapper.getMainWallet(player.getId());
        assertEquals(befoeDepositMainWallet.getBalance(), afterDepositMainWallet.getBalance());
    }

    @Test
    public void testDeposit() throws UnsupportedOperationException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockDepositResponse }));

        BigDecimal amount = new BigDecimal("1");
        GameApiResult result = api.deposit(player, amount);
        assertTrue(result.isSuccess());
        assertEquals("approved", result.getParam("status"));
        assertEquals("CI2_AGINT743107421617", result.getTransactionId());
    }
    
    @Test
    public void testWithdraw() throws UnsupportedOperationException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockWithdrawResponse }));

        BigDecimal amount = new BigDecimal("1");
        GameApiResult result = api.withdraw(player, amount);
        assertTrue(result.isSuccess());
        assertEquals("approved", result.getParam("status"));
        assertEquals("CI2_AGINT054549104916", result.getTransactionId());
    }
    
    @Test
    public void testFailedWithdrawBecasueNotEnoughBalance() throws UnsupportedOperationException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockWithdrawFailedResponse }));
        Wallet befoeWithdrawGameWallet = walletMapper.get(player.getId(), api.getId());

        BigDecimal amount = new BigDecimal("10000");
        GameApiResult result = api.withdraw(player, amount);
        GameApiT1GCommonResponse commonResponse = (GameApiT1GCommonResponse)result.getParam("response");
        assertTrue(!result.isSuccess());
        assertEquals("No enough balance", commonResponse.getDetail("message"));
        assertEquals("2", commonResponse.getDetail("reason_id"));
        
        //Withdraw failed, The amount of game wallet should not be changed
        Wallet afterWithdrawGameWallet = walletMapper.get(player.getId(), api.getId());
        assertEquals(befoeWithdrawGameWallet.getBalance(), afterWithdrawGameWallet.getBalance());
    }

    
    @Test
    public void testGetBalance() throws UnsupportedOperationException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockPlayerBalanceResponse }));

        GameApiResult result = api.getBalance(player);
        assertTrue(result.isSuccess());
        assertTrue(new BigDecimal("102").compareTo(result.getBalance())==0);
    }
    
    @Test
    public void testLaunchGame() {
        api.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockLaunchGameResponse }));

        Map<String, String> options = new HashMap<>();
        options.put("Accept-Language", "zh-cn");
        String mode = "trial";
        String platform = "pc";
        GameName gameName = new GameName();
        gameName.setGameCode("0");
        gameName.setGameTypeId(1);
        GameApiResult result = api.launchGame(player, gameName, platform, mode, "zh-CN");
        assertTrue(result.isSuccess());
        assertEquals(
                "http://player.gamegateway.t1t.games/player_center/launch_game_with_token/4476ed84401e844c58d4d728f85fd0a9/72/0/zh-cn/trial/pc/_null/testmerchant/_null?append_target_db=0",
                result.getGameUrl());
    }

    @Test
    public void testSyncGameLogs() throws ClientProtocolException, IOException {
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockGameLogResponse }));

        GameLogApiResult<GameLogT1> result = api.syncGameLog(LocalDateTime.now(), LocalDateTime.now());
        assertTrue(result.isSuccess());
        assertFalse(result.isNoop());
        assertEquals(2, result.getGameLogs().size());
        assertEquals(2, result.getGameLogs().get(0).getPlayerId().intValue());
        assertEquals("test002", result.getGameLogs().get(0).getPlayerUsername());
        assertEquals("72181117020911015", result.getGameLogs().get(1).getExternalUid());
        // fetch betDetails
        result.getGameLogs().forEach(gameLog -> {
            JsonNode betDetails = gameLog.getBetDetails();
            assertNotNull(betDetails.get("gameDetails"));
            assertNotNull(betDetails.get("roundNumber"));
            assertNotNull(betDetails.get("betDetails"));
            assertNotNull(betDetails.get("betType"));
            assertNotNull(betDetails.get("detailStatus"));
        });
    }

    @Test
    public void testFetchAuthToken() {
        String authToken = api.fetchAuthToken();
        assertEquals("abcd", authToken);
    }

    @Test
    public void testRequestApiWithInvalidToken() {
        api.setHttpService(MockApiHttpServiceFactory
                .get(new String[] { mockWithInvalidTokenResponse, mockTokenResponse, mockRegisterResponse }));

        ApiResult result = api.register(player);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testParseGameLog() {
        GameApiT1GCommonGameLogResponse responseObj = JsonUtils.jsonToObject(mockGameLogResponse,
                GameApiT1GCommonGameLogResponse.class, PropertyNamingStrategy.SNAKE_CASE);
        assertNotNull(responseObj);
        assertEquals(1, responseObj.getDetail().getCurrentPage());
        assertEquals(2, responseObj.getDetail().getTotalRowsCurrentPage());
        assertEquals(2, responseObj.getDetail().getGameHistory().size());
        assertEquals("181022028954084", responseObj.getDetail().getGameHistory().get(0).getGameExternalUniqueid());
    }

    @BeforeClass
    public static void initMocks() {
        LocalDateTime oneDayLater = LocalDateTime.now().plusDays(1);

        mockTokenResponse = "{\n" +
                "   \"success\":true,\n" +
                "   \"version\":\"3.15000000\",\n" +
                "   \"code\":\"0\",\n" +
                "   \"message\":\"success\",\n" +
                "   \"request_id\":\"7ba70bdfec6f8f0d1d935ac7675d7050\",\n" +
                "   \"detail\":{\n" +
                "      \"auth_token\" : \"abcd\",\n" +
                "      \"timeout_datetime\" : \"" + oneDayLater.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\"\n" +
                "   }\n" +
                "}";

        mockGameLogResponse = "{\n" +
                "   \"success\":true,\n" +
                "   \"version\":\"3.15000000\",\n" +
                "   \"code\":\"0\",\n" +
                "   \"message\":\"success\",\n" +
                "   \"request_id\":\"7ba70bdfec6f8f0d1d935ac7675d7050\",\n" +
                "   \"server_time\": \"2019-10-05 10:40:43\",\n" +
                "   \"cost\": 0,\n" +
                "   \"detail\":{\n" +
                "      \"game_history\":[\n" +
                "         {\n" +
                "            \"uniqueid\":\"12036281\",\n" +
                "            \"external_uniqueid\":\"72181022028954084\",\n" +
                "            \"game_external_uniqueid\":\"181022028954084\",\n" +
                "            \"username\":\"devtest002\",\n" +
                "            \"merchant_code\":\"testmerchant\",\n" +
                "            \"game_platform_id\":\"72\",\n" +
                "            \"game_code\":\"BAC\",\n" +
                "            \"game_name\":\"_json:{\\\"1\\\":\\\"Baccarat\\\",\\\"2\\\":\\\"\\u767e\\u5bb6\\u4e50\\\",\\\"3\\\":\\\"Baccarat\\\",\\\"4\\\":\\\"Baccarat\\\",\\\"5\\\":\\\"Baccarat\\\"}\",\n"
                +
                "            \"game_finish_time\":\"2018-10-22 14:10:58\",\n" +
                "            \"game_details\":\"181022028954084\",\n" +
                "            \"bet_time\":\"2018-10-22 14:10:58\",\n" +
                "            \"payout_time\":\"2018-10-22 14:10:58\",\n" +
                "            \"round_number\":\"GC00118A2202Q\",\n" +
                "            \"real_bet_amount\":\"100\",\n" +
                "            \"effective_bet_amount\":\"95\",\n" +
                "            \"result_amount\":\"95\",\n" +
                "            \"payout_amount\":\"195\",\n" +
                "            \"after_balance\":\"6669\",\n" +
                "            \"bet_details\":\"{\\\"bet_details\\\":{\\\"181022028954084\\\":{\\\"odds\\\":\\\"null\\\",\\\"win_amount\\\":\\\"95\\\",\\\"bet_amount\\\":\\\"100\\\",\\\"bet_placed\\\":\\\"Banker\\\",\\\"won_side\\\":\\\"Yes\\\",\\\"winloss_amount\\\":\\\"95\\\"}},\\\"Created At\\\":\\\"2018-10-22 14:15:42\\\"}\",\n"
                +
                "            \"md5_sum\":\"d83d804c50a960d84ef10dda28bcd0be\",\n" +
                "            \"ip_address\":null,\n" +
                "            \"bet_type\":\"Single Bet\",\n" +
                "            \"odds_type\":null,\n" +
                "            \"odds\":\"0\",\n" +
                "            \"response_result_id\":\"30159464\",\n" +
                "            \"game_status\":\"settled\",\n" +
                "            \"detail_status\":\"1\",\n" +
                "            \"updated_at\":\"2018-10-22 14:10:58\"\n" +
                "         },\n" +
                "         {\n" +
                "            \"uniqueid\":\"17071781\",\n" +
                "            \"external_uniqueid\":\"72181117020911015\",\n" +
                "            \"game_external_uniqueid\":\"181117020911015\",\n" +
                "            \"username\":\"devtest002\",\n" +
                "            \"merchant_code\":\"testmerchant\",\n" +
                "            \"game_platform_id\":\"72\",\n" +
                "            \"game_code\":\"BAC\",\n" +
                "            \"game_name\":\"_json:{\\\"1\\\":\\\"Baccarat\\\",\\\"2\\\":\\\"\\u767e\\u5bb6\\u4e50\\\",\\\"3\\\":\\\"Baccarat\\\",\\\"4\\\":\\\"Baccarat\\\",\\\"5\\\":\\\"Baccarat\\\"}\",\n"
                +
                "            \"game_finish_time\":\"2018-11-17 14:46:25\",\n" +
                "            \"game_details\":\"181117020911015\",\n" +
                "            \"bet_time\":\"2018-11-17 14:46:25\",\n" +
                "            \"payout_time\":\"2018-11-17 14:46:25\",\n" +
                "            \"round_number\":\"GC00118B1704Y\",\n" +
                "            \"real_bet_amount\":\"200\",\n" +
                "            \"effective_bet_amount\":\"190\",\n" +
                "            \"result_amount\":\"190\",\n" +
                "            \"payout_amount\":\"390\",\n" +
                "            \"after_balance\":\"390\",\n" +
                "            \"bet_details\":\"{\\\"bet_details\\\":{\\\"181117020911015\\\":{\\\"odds\\\":\\\"null\\\",\\\"win_amount\\\":\\\"190\\\",\\\"bet_amount\\\":\\\"200\\\",\\\"bet_placed\\\":\\\"Banker\\\",\\\"won_side\\\":\\\"Yes\\\",\\\"winloss_amount\\\":\\\"190\\\"}},\\\"Created At\\\":\\\"2018-11-17 14:51:37\\\"}\",\n"
                +
                "            \"md5_sum\":\"9d16b044c853c5e0a6403ce4c4329fdd\",\n" +
                "            \"ip_address\":null,\n" +
                "            \"bet_type\":\"Single Bet\",\n" +
                "            \"odds_type\":null,\n" +
                "            \"odds\":\"0\",\n" +
                "            \"response_result_id\":\"34319716\",\n" +
                "            \"game_status\":\"settled\",\n" +
                "            \"detail_status\":\"1\",\n" +
                "            \"updated_at\":\"2018-11-17 14:46:25\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"total_pages\":1,\n" +
                "      \"current_page\":1,\n" +
                "      \"total_rows_current_page\":2\n" +
                "   }\n" +
                "}";

        mockPlayerBalanceResponse = "{\"success\":true,\"version\":\"5.06\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"ef423d677e7da5854d0feac4aeb66771\",\"server_time\":\"2019-10-07 11:04:47\",\"cost\":1,\"detail\":{\"game_platform_balance\":102}}";
        mockDepositResponse = "{\"success\":true,\"version\":\"5.06\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"67c398a3be8d29770f5a9d7df28ccbbc\",\"server_time\":\"2019-10-07 11:07:39\",\"cost\":1,\"detail\":{\"updated\":true,\"transaction_id\":\"CI2_AGINT743107421617\",\"transfer_status\":\"approved\",\"reason_id\":999,\"response_result_id\":142576881}}";
        mockWithdrawResponse = "{\"success\":true,\"version\":\"5.06\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"6642ae8141b0c67973e4d27791507d10\",\"server_time\":\"2019-10-07 11:08:57\",\"cost\":3,\"detail\":{\"updated\":true,\"transaction_id\":\"CI2_AGINT054549104916\",\"transfer_status\":\"approved\",\"reason_id\":999,\"response_result_id\":142577237}}";
        mockRegisterResponse = "{\"success\":true,\"version\":\"5.06\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"6a2415be084f58b27bb5eba6ceb273bd\",\"server_time\":\"2019-10-07 10:52:14\",\"cost\":0,\"detail\":{\"username\":\"devtest003\",\"created_mode\":\"created_only\"}}";
        mockLaunchGameResponse = "{\"success\":true,\"version\":\"5.06\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"565afc00b47b67216c71bedc85c4da37\",\"server_time\":\"2019-10-07 11:09:56\",\"cost\":0,\"detail\":{\"launcher\":{\"success\":true,\"url\":\"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/4476ed84401e844c58d4d728f85fd0a9/72/0/zh-cn/trial/pc/_null/testmerchant/_null?append_target_db=0\"}}}";
        mockWithdrawFailedResponse = "{\"success\":false,\"version\":\"5.07\",\"code\":\"9999\",\"message\":\"internal error\",\"request_id\":\"442fbcc314aff94602227ce24aeba15f\",\"server_time\":\"2020-04-07 15:59:37\",\"cost\":1,\"external_request_id\":null,\"detail\":{\"success\":false,\"reason_id\":2,\"message\":\"No enough balance\"}}";
        mockDepositFailedResponse = "{\"success\":true,\"version\":\"5.07\",\"code\":\"0\",\"message\":\"success\",\"request_id\":\"fc801801d587c6cb7d9e6c02dfc2fa5c\",\"server_time\":\"2020-04-07 16:44:50\",\"cost\":1,\"external_request_id\":null,\"detail\":{\"updated\":true,\"transaction_id\":\"52621483\",\"transfer_status\":\"unknown\",\"reason_id\":999,\"response_result_id\":202867542}}";
        mockWithInvalidTokenResponse = "{\"success\":false,\"version\":\"5.11\",\"code\":\"4\",\"message\":\"invalid auth_token\",\"request_id\":\"57425e9e4b31e28da3db342fc52e2c39\",\"server_time\":\"2020-08-10 17:11:23\",\"cost\":0,\"external_request_id\":false,\"detail\":null}";
    }

    private static String mockTokenResponse;
    private static String mockGameLogResponse;
    private static String mockPlayerBalanceResponse;
    private static String mockWithdrawResponse;
    private static String mockDepositResponse;
    private static String mockRegisterResponse;
    private static String mockLaunchGameResponse;
    private static String mockWithdrawFailedResponse;
    private static String mockDepositFailedResponse;
    private static String mockWithInvalidTokenResponse;
}
