package com.tripleonetech.sbe.game.integration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class GameApiPlayerAccountServiceTest extends BaseTest {
    @Autowired
    private GameApiPlayerAccountService service;

    private int gameApiId = 1; // MockGameCNY
    private int playerId = 2; // test002
    private String playerUsername = "cnytest002";

    @Test
    public void testGetPlayerId() {
        Integer playerId = service.getPlayerId(gameApiId, playerUsername);
        assertEquals(this.playerId, playerId.intValue());
    }
}
