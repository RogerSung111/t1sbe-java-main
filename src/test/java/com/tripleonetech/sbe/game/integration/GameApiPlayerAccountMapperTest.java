package com.tripleonetech.sbe.game.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class GameApiPlayerAccountMapperTest extends BaseTest {
    // private static final Logger logger = LoggerFactory.getLogger(GameApiPlayerAccountMapperTest.class);

    @Autowired
    private GameApiPlayerAccountMapper mapper;

    private int gameApiId = 1; // MockGameCNY
    private int playerId = 2; // test002
    private String playerUsername = "cnytest002";
    private int recordId = 1;

    @Test
    public void testGet() {
        GameApiPlayerAccount account = mapper.get(gameApiId, playerId);
        assertNotNull(account);
        assertEquals("cnytest002", account.getUsername());
        assertTrue("Password validation works", account.validatePassword("123456"));
        assertEquals("Password can be decrypted properly", "123456", account.getPassword());
    }

    @Test
    public void testGetByPlayer() {
        List<GameApiPlayerAccount> accounts = mapper.getByPlayer(playerId);
        assertNotNull(accounts);
        assertEquals(16, accounts.size());

        GameApiPlayerAccount account = accounts.get(0);
        assertNotNull(account);
        assertEquals("cnytest002", account.getUsername());
        assertTrue("Password validation works", account.validatePassword("123456"));
        assertEquals("Password can be decrypted properly", "123456", account.getPassword());
    }

    @Test
    public void testGetIdByUsername() {
        Integer playerId = mapper.getIdByUsername(gameApiId, playerUsername);

        assertNotNull(playerId);
        assertEquals(2, playerId.intValue());
    }

    @Test
    public void testGetIdByNonExistingUsername() {
        Integer playerId = mapper.getIdByUsername(gameApiId, "Username-does-not-exist");
        assertNull(playerId);
    }

    @Test
    public void testInsert() {
        GameApiPlayerAccount account = new GameApiPlayerAccount();
        account.setUsername("cnytest004");
        account.setGameApiId(1);
        account.setPlayerId(4);
        account.setPassword("654321");
        assertTrue(mapper.upsert(account) > 0);

        account = mapper.get(1, 4);
        assertNotNull(account);
        assertEquals("cnytest004", account.getUsername());
        assertTrue("Password validation works", account.validatePassword("654321"));
    }

    @Test
    public void testDelete() {
        mapper.delete(recordId);
        GameApiPlayerAccount account = mapper.get(gameApiId, playerId);
        assertNull(account);
    }

}
