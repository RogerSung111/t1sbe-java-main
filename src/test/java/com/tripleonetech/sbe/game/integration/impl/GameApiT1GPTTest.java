package com.tripleonetech.sbe.game.integration.impl;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.GameNameMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class GameApiT1GPTTest extends BaseTest {
    private static final int T1G_PT_API_ID = 9;

    @Autowired
    private GameApiFactory apiFactory;
    @Autowired
    private GameNameMapper gameNameMapper;

    private GameApiT1G api;
    
    @Before
    public void setup() {
        api = (GameApiT1G) apiFactory.getById(T1G_PT_API_ID);

        // api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockTokenResponse }));
        // this method will only call http service when token cache is empty
        // isolate the call of fetch token here to avoid interfering with tests
    }

    @Test
    public void testSyncImageUrl() {
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockGameLogResponse }));
        api.syncImageUrl();
        GameName spotCheckGame = gameNameMapper.getGameByCode(api.getApiCode(), "bj_mh5", "en-US", "en-US");
        assertEquals("http://www.gamegateway.t1t.games/includes/images/playtech/bj_mh5.jpg",
                spotCheckGame.getGameNameI18n().getImgUrl());

    }
    @BeforeClass
    public static void initMocks() {

        mockGameLogResponse = "{\n"
                +
                "  \"game_list\": [\n" + 
                "    {\n" + 
                "      \"game_type_code\": \"slots\",\n" + 
                "      \"game_name\": \"_json:{\\\"1\\\":\\\"Vacation Station\\\",\\\"2\\\":\\\"开心假期\\\",\\\"3\\\":\\\"Vacation Station\\\",\\\"4\\\":\\\"Vacation Station\\\",\\\"5\\\":\\\"Vacation Station\\\"}\",\n" + 
                "      \"game_name_en\": \"Vacation Station\",\n" + 
                "      \"game_name_cn\": \"开心假期\",\n" + 
                "      \"game_name_indo\": \"Vacation Station\",\n" + 
                "      \"game_name_vn\": \"Vacation Station\",\n" + 
                "      \"game_name_kr\": \"Vacation Station\",\n" + 
                "      \"provider_name\": \"PlayTech\",\n" + 
                "      \"provider_code\": \"PT\",\n" + 
                "      \"in_flash\": \"1\",\n" + 
                "      \"in_html5\": \"1\",\n" + 
                "      \"in_mobile\": \"1\",\n" + 
                "      \"available_on_android\": \"1\",\n" + 
                "      \"available_on_ios\": \"1\",\n" + 
                "      \"note\": null,\n" + 
                "      \"status\": \"1\",\n" + 
                "      \"top_game_order\": \"0\",\n" + 
                "      \"enabled_freespin\": \"0\",\n" + 
                "      \"sub_game_provider\": null,\n" + 
                "      \"flag_new_game\": \"0\",\n" + 
                "      \"progressive\": \"0\",\n" + 
                "      \"game_launch_url\": {\n" + 
                "        \"web\": \"player_center/goto_ptgame/default/er\",\n" + 
                "        \"mobile\": \"player_center/goto_ptgame/default/er\",\n" + 
                "        \"sample\": \"player_center/goto_ptgame/<siteName>/<game_launch_code>/<mode>/<is_mobile>\"\n" + 
                "      },\n" + 
                "      \"game_launch_code_other_settings\": null,\n" + 
                "      \"image_path\": {\n" + 
                "        \"en\": \"http://www.gamegateway.t1t.games/includes/images/playtech/er.jpg\",\n" + 
                "        \"cn\": \"http://www.gamegateway.t1t.games/includes/images/cn/playtech/er.jpg\"\n" + 
                "      },\n" + 
                "      \"sub_category\": null,\n" + 
                "      \"game_id_mobile\": \"er\",\n" + 
                "      \"game_id_desktop\": \"er\",\n" + 
                "      \"game_unique_id\": \"er\",\n" + 
                "      \"release_date\": \"2018-08-22 14:03:22\"\n" + 
                "    },\n" + 
                "    {\n" + 
                "      \"game_type_code\": \"slots\",\n" + 
                "      \"game_name\": \"_json:{\\\"1\\\":\\\"Football Rules\\\",\\\"2\\\":\\\"终极足球\\\",\\\"3\\\":\\\"Football Rules\\\",\\\"4\\\":\\\"Football Rules\\\",\\\"5\\\":\\\"Football Rules\\\"}\",\n" + 
                "      \"game_name_en\": \"Football Rules\",\n" + 
                "      \"game_name_cn\": \"终极足球\",\n" + 
                "      \"game_name_indo\": \"Football Rules\",\n" + 
                "      \"game_name_vn\": \"Football Rules\",\n" + 
                "      \"game_name_kr\": \"Football Rules\",\n" + 
                "      \"provider_name\": \"PlayTech\",\n" + 
                "      \"provider_code\": \"PT\",\n" + 
                "      \"in_flash\": \"1\",\n" + 
                "      \"in_html5\": \"1\",\n" + 
                "      \"in_mobile\": \"1\",\n" + 
                "      \"available_on_android\": \"1\",\n" + 
                "      \"available_on_ios\": \"1\",\n" + 
                "      \"note\": null,\n" + 
                "      \"status\": \"1\",\n" + 
                "      \"top_game_order\": \"0\",\n" + 
                "      \"enabled_freespin\": \"1\",\n" + 
                "      \"sub_game_provider\": null,\n" + 
                "      \"flag_new_game\": \"0\",\n" + 
                "      \"progressive\": \"0\",\n" + 
                "      \"game_launch_url\": {\n" + 
                "        \"web\": \"player_center/goto_ptgame/default/fbr\",\n" + 
                "        \"mobile\": \"player_center/goto_ptgame/default/fbr\",\n" + 
                "        \"sample\": \"player_center/goto_ptgame/<siteName>/<game_launch_code>/<mode>/<is_mobile>\"\n" + 
                "      },\n" + 
                "      \"game_launch_code_other_settings\": null,\n" + 
                "      \"image_path\": {\n" + 
                "        \"en\": \"http://www.gamegateway.t1t.games/includes/images/playtech/fbr.jpg\",\n" + 
                "        \"cn\": \"http://www.gamegateway.t1t.games/includes/images/cn/playtech/fbr.jpg\"\n" + 
                "      },\n" + 
                "      \"sub_category\": null,\n" + 
                "      \"game_id_mobile\": \"fbr\",\n" + 
                "      \"game_id_desktop\": \"fbr\",\n" + 
                "      \"game_unique_id\": \"fbr\",\n" + 
                "      \"release_date\": \"2018-08-22 14:03:22\"\n" + 
                "    },\n" + 
                "    {\n" + 
                "      \"game_type_code\": \"table_and_cards\",\n" + 
                "      \"game_name\": \"_json:{\\\"1\\\":\\\"Blackjack\\\",\\\"2\\\":\\\"21点\\\",\\\"3\\\":\\\"Blackjack\\\",\\\"4\\\":\\\"Blackjack\\\",\\\"5\\\":\\\"Blackjack\\\"}\",\n" + 
                "      \"game_name_en\": \"Blackjack\",\n" + 
                "      \"game_name_cn\": \"21点\",\n" + 
                "      \"game_name_indo\": \"Blackjack\",\n" + 
                "      \"game_name_vn\": \"Blackjack\",\n" + 
                "      \"game_name_kr\": \"Blackjack\",\n" + 
                "      \"provider_name\": \"PlayTech\",\n" + 
                "      \"provider_code\": \"PT\",\n" + 
                "      \"in_flash\": \"1\",\n" + 
                "      \"in_html5\": \"0\",\n" + 
                "      \"in_mobile\": \"0\",\n" + 
                "      \"available_on_android\": \"0\",\n" + 
                "      \"available_on_ios\": \"0\",\n" + 
                "      \"note\": null,\n" + 
                "      \"status\": \"1\",\n" + 
                "      \"top_game_order\": \"0\",\n" + 
                "      \"enabled_freespin\": \"0\",\n" + 
                "      \"sub_game_provider\": null,\n" + 
                "      \"flag_new_game\": \"0\",\n" + 
                "      \"progressive\": \"0\",\n" + 
                "      \"game_launch_url\": {\n" + 
                "        \"web\": \"player_center/goto_ptgame/default/bj_mh5\",\n" + 
                "        \"sample\": \"player_center/goto_ptgame/<siteName>/<game_launch_code>/<mode>/<is_mobile>\"\n" + 
                "      },\n" + 
                "      \"game_launch_code_other_settings\": null,\n" + 
                "      \"image_path\": {\n" + 
                "        \"en\": \"http://www.gamegateway.t1t.games/includes/images/playtech/bj_mh5.jpg\",\n" + 
                "        \"cn\": \"http://www.gamegateway.t1t.games/includes/images/cn/playtech/bj_mh5.jpg\"\n" + 
                "      },\n" + 
                "      \"sub_category\": null,\n" + 
                "      \"game_id_desktop\": \"bj_mh5\",\n" + 
                "      \"game_unique_id\": \"bj_mh5\",\n" + 
                "      \"release_date\": \"2018-08-22 14:03:22\"\n" + 
                "    }\n" + 
                "  ]\n" + 
                "}";

    }

    private static String mockGameLogResponse;
}
