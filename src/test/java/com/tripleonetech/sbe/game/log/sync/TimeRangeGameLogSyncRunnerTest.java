package com.tripleonetech.sbe.game.log.sync;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.GameLog;
import com.tripleonetech.sbe.game.log.GameLogMapper;
import com.tripleonetech.sbe.game.log.impl.GameLogMockGame;
import com.tripleonetech.sbe.game.log.impl.GameLogMockGameMapper;

public class TimeRangeGameLogSyncRunnerTest extends BaseTest {
    @Autowired
    private GameApiFactory apiFactory;
    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private GameLogMapper gameLogMapper;
    @Autowired
    private GameLogSyncTaskMapper syncTaskMapper;
    @Autowired
    private GameLogMockGameMapper gameLogMockGameMapper;

    private GameApi<GameLogMockGame> mockGameApi;
    private TimeRangeGameLogSyncRunner<GameLogMockGame> mockGameSyncRunner;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        mockGameApi = (GameApi<GameLogMockGame>) apiFactory.getById(1);
        mockGameSyncRunner = appContext.getBean(
                TimeRangeGameLogSyncRunner.class,
                mockGameApi);
    }

    @Test
    @Ignore
    public void testRun() throws InterruptedException, ParseException {
        // Run sync once
        mockGameSyncRunner.run();
        assertFalse("Sync runner is in auto-run state", mockGameSyncRunner.isManual());

        GameLogMockGame apiGameLog = gameLogMockGameMapper.getByExternalUid("1810030696387520");
        assertNotNull("The game log (hard-coded in mock game implementation) is inserted into game_log_mock_game table",
                apiGameLog);

        GameLog sysGameLog = gameLogMapper.getByExternalUid(apiGameLog.getExternalUid(), 1);

        assertEquals(sysGameLog.getBet(), apiGameLog.getBet());
        assertEquals(sysGameLog.getPlayerUsername(), apiGameLog.getPlayerUsername());
        assertEquals(sysGameLog.getGameApiId().intValue(), mockGameApi.getId());
        assertEquals(sysGameLog.getGameApiCode(), "MockGame");
    }

    // @Test
    // public void testRunAsync() {
    // GameLogSyncTask syncTask = syncTaskMapper.get(new BigInteger("1"));
    // String syncUsername = "test001";
    //
    // assertNotEquals(syncTask.getSyncUsername(), syncUsername);
    //
    // mockGameSyncRunner.runAsync(syncTask, syncUsername);
    //
    // assertEquals(syncTask.getSyncUsername(), syncUsername);
    // assertEquals(syncTask.getParams(), SyncParamEnum.toJson(mockGameSyncRunner.getBenchmark().getSyncParams()));
    // }
}
