package com.tripleonetech.sbe.game.log.sync;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.GameLog;
import com.tripleonetech.sbe.game.log.GameLogMapper;
import com.tripleonetech.sbe.game.log.impl.GameLogMockGame2;
import com.tripleonetech.sbe.game.log.impl.GameLogMockGame2Mapper;

public class IndexGameLogSyncRunnerTest extends BaseTest {
    @Autowired
    private GameApiFactory apiFactory;
    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private GameLogMapper gameLogMapper;
    @Autowired
    private GameLogSyncTaskMapper syncTaskMapper;
    @Autowired
    private GameLogMockGame2Mapper gameLogMockGame2Mapper;

    private GameApi<GameLogMockGame2> mockGameApi;
    private IndexGameLogSyncRunner<GameLogMockGame2> mockGameSyncRunner;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        mockGameApi = (GameApi<GameLogMockGame2>) apiFactory.getById(2);
        mockGameSyncRunner = appContext.getBean(IndexGameLogSyncRunner.class,
                mockGameApi);
    }

    @Test
    @Ignore
    public void testRun() throws InterruptedException, ParseException {
        // Run sync once
        mockGameSyncRunner.run();
        assertFalse("Sync runner is in auto-run state", mockGameSyncRunner.isManual());

        GameLogMockGame2 apiGameLog = gameLogMockGame2Mapper.getByExternalUid("47865551721");
        assertNotNull(
                "The game log (hard-coded in mock game implementation) is inserted into game_log_mock_game2 table",
                apiGameLog);
        String logBetTime = apiGameLog.getBetTime();
        assertNotNull(logBetTime);
        assertNotNull(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(logBetTime));

        // test for game log in game_log_YYYYMM table (without query, view does not exist yet)
        GameLog sysGameLog = gameLogMapper.getByExternalUid(apiGameLog.getExternalUid(), 2);
        assertEquals(sysGameLog.getBet(), apiGameLog.getBet());
        assertEquals(sysGameLog.getPlayerUsername(), apiGameLog.getPlayerUsername());
        assertEquals(sysGameLog.getGameApiId().intValue(), mockGameApi.getId());
        assertEquals(sysGameLog.getGameApiCode(), "MockGame2");

    }

    // @Test
    // public void testRunAsync() {
    // GameLogSyncTask syncTask = syncTaskMapper.get(new BigInteger("2"));
    // String syncUsername = "test001";
    //
    // assertNotEquals(syncTask.getSyncUsername(), syncUsername);
    //
    // mockGameSyncRunner.runAsync(syncTask, syncUsername);
    //
    // assertEquals(syncTask.getSyncUsername(), syncUsername);
    // assertEquals(syncTask.getParams(), SyncParamEnum.toJson(mockGameSyncRunner.getBenchmark().getSyncParams()));
    // }
}
