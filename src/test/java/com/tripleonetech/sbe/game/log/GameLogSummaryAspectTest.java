package com.tripleonetech.sbe.game.log;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.integration.impl.GameApiT1GAGIN;

public class GameLogSummaryAspectTest extends BaseTest {
    @Autowired
    private GameApiT1GAGIN gameApi;

    @Autowired
    private GameLogHourlyReportMapper hourlyReportMapper;

    private List<GameLog> gameLogs;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:00:00");

    @Before
    public void setUp() throws Exception {
        LocalDateTime fourHoursAgo = LocalDateTime.now().plusHours(-4);
        gameLogs = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            GameLog gameLog = new GameLog();
            gameLog.setId(UUID.randomUUID().toString());
            gameLog.setGameApiId(1);
            gameLog.setGameApiCode("T1GAGIN");
            gameLog.setGameCode("DUMMY-test");
            gameLog.setPlayerId(2);
            gameLog.setPlayerUsername("test002");
            gameLog.setExternalUid("testuuid_" + i);
            gameLog.setBet(new BigDecimal(i * 10));
            
            //計算邏輯：Payout(領回的彩金<涵蓋本金>） = bet(投注金額<本金>） + payoff（領回彩金） 
            //這邊假設 payoff = bet (不賺不賠情情況）
            BigDecimal bet = ObjectUtils.defaultIfNull(gameLog.getBet(), BigDecimal.ZERO);
            BigDecimal payoff = ObjectUtils.defaultIfNull(gameLog.getBet(), BigDecimal.ZERO);
            gameLog.setPayout(bet.add(payoff));
            
            LocalDateTime mockBetTime = LocalDateTime
                    .of(fourHoursAgo.getYear(),
                            fourHoursAgo.getMonth(),
                            fourHoursAgo.getDayOfMonth(),
                            fourHoursAgo.getHour(),
                            1,
                            fourHoursAgo.getSecond())
                    .plusMinutes(i * 30);
            gameLog.setBetTime(mockBetTime);
            gameLog.setSettledTime(gameLog.getBetTime());
            gameLog.setStatus(GameLogStatusEnum.SETTLED);

            gameLogs.add(gameLog);
        }
    }

    @Test
    public void testSetAsDirty() throws Exception {
        // trigger AOP
        LocalDateTime startTime = LocalDateTime.now();
        gameApi.saveGameLogs(gameLogs, null);

        // After
        Thread.sleep(2000);
        List<GameLogHourlyReport> hourlyReports = hourlyReportMapper.listByUpdatedAt(startTime, LocalDateTime.now());
        assertEquals(2, hourlyReports.size());
        for (int i = 0; i < hourlyReports.size(); i++) {
            int startIndex = i * 2;
            GameLogHourlyReport hourlyReport = hourlyReports.get(i);
            assertEquals(Boolean.TRUE, hourlyReport.getDirty());
            assertEquals(gameLogs.get(startIndex).getBetTime().format(formatter),
                    hourlyReport.getBetTimeHour().format(formatter));
        }
    }

    @Test
    public void testSetAsDirty_ListIncludeStatusPendding_BeExcluded() throws Exception {
        gameLogs.get(0).setStatus(GameLogStatusEnum.PENDING);
        gameLogs.get(1).setStatus(GameLogStatusEnum.PENDING);
        LocalDateTime startTime = LocalDateTime.now();
        // trigger AOP
        gameApi.saveGameLogs(gameLogs, null);

        // After
        Thread.sleep(2000);
        List<GameLogHourlyReport> hourlyReports = hourlyReportMapper.listByUpdatedAt(startTime, LocalDateTime.now());
        assertEquals(1, hourlyReports.size());
    }

}
