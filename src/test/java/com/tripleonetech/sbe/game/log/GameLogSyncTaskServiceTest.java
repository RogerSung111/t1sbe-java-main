package com.tripleonetech.sbe.game.log;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTask;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTaskMapper;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTaskService;

public class GameLogSyncTaskServiceTest extends BaseTest {

    @InjectMocks
    GameLogSyncTaskService syncTaskService;

    @Mock
    GameApiFactory apiFactory;

    @Mock
    GameLogSyncTaskMapper syncTaskMapper;

    @Mock
    ApplicationContext appContext;

    GameLogSyncTask syncTaskMock;

    @Before
    public void setup() {
        syncTaskMock = new GameLogSyncTask();
        Integer syncTaskId = new Integer("1");
        int apiId = 1;
        syncTaskMock.setId(syncTaskId);
        syncTaskMock.setGameApiId(apiId);

    }

    @Test
    public void findAll() {
        // setup
        Mockito.when(syncTaskMapper.list()).thenReturn(Arrays.asList(syncTaskMock));

        // call method
        List<GameLogSyncTask> syncTasks = syncTaskService.findAll();

        // verification
        verify(syncTaskMapper, times(1)).list();
        verifyNoMoreInteractions(syncTaskMapper);

        assertEquals(1, syncTasks.size());
    }

    @Test
    public void findById() {
        // setup
        Mockito.when(syncTaskMapper.get(syncTaskMock.getId())).thenReturn(syncTaskMock);

        // call method
        GameLogSyncTask syncTask = syncTaskService.findById(syncTaskMock.getId());

        // verification
        verify(syncTaskMapper, times(1)).get(syncTaskMock.getId());
        verifyNoMoreInteractions(syncTaskMapper);

        assertEquals(syncTask.getId(), syncTaskMock.getId());
    }

    // @SuppressWarnings({ "unchecked", "rawtypes" })
    // @Test
    // public void reSyncForTimeRangeParams() {
    // // setup
    // String syncUsername = "test001";
    // GameApiMockGame gameApi = Mockito.mock(GameApiMockGame.class);
    // TimeRangeGameLogSyncRunner<GameLogMockGame> syncRunner = Mockito.mock(TimeRangeGameLogSyncRunner.class);
    //
    // Mockito.when(appContext.getBean(TimeRangeGameLogSyncRunner.class, gameApi)).thenReturn(syncRunner);
    // Mockito.when(syncTaskMapper.get(syncTaskMock.getId())).thenReturn(syncTaskMock);
    // Mockito.when(apiFactory.getById(syncTaskMock.getGameApiId())).thenReturn((GameApi) gameApi);
    //
    // // call method
    // syncTaskService.reSync(syncTaskMock.getId(), syncUsername);
    //
    // // verification
    // verify(syncTaskMapper, times(1)).get(syncTaskMock.getId());
    // verify(apiFactory, times(1)).getById(syncTaskMock.getGameApiId());
    // verify(appContext, times(1)).getBean(TimeRangeGameLogSyncRunner.class, gameApi);
    // verify(syncRunner, times(1)).runAsync(syncTaskMock, syncUsername);
    // verifyNoMoreInteractions(syncTaskMapper);
    //
    // }

    // @SuppressWarnings({ "unchecked", "rawtypes" })
    // @Test
    // public void reSyncForIndexParams() {
    // // setup
    // String syncUsername = "test001";
    // GameApiMockGame2 gameApi = Mockito.mock(GameApiMockGame2.class);
    // IndexGameLogSyncRunner<GameLogMockGame2> syncRunner = Mockito.mock(IndexGameLogSyncRunner.class);
    //
    // Mockito.when(appContext.getBean(IndexGameLogSyncRunner.class, gameApi)).thenReturn(syncRunner);
    // Mockito.when(syncTaskMapper.get(syncTaskMock.getId())).thenReturn(syncTaskMock);
    // Mockito.when(apiFactory.getById(syncTaskMock.getGameApiId())).thenReturn((GameApi) gameApi);
    //
    // // call method
    // syncTaskService.reSync(syncTaskMock.getId(), syncUsername);
    //
    // // verification
    // verify(syncTaskMapper, times(1)).get(syncTaskMock.getId());
    // verify(apiFactory, times(1)).getById(syncTaskMock.getGameApiId());
    // verify(appContext, times(1)).getBean(IndexGameLogSyncRunner.class, gameApi);
    // verify(syncRunner, times(1)).runAsync(syncTaskMock, syncUsername);
    // verifyNoMoreInteractions(syncTaskMapper);
    //
    // }

}
