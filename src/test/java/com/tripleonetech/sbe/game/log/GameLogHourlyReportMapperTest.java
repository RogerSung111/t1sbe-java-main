package com.tripleonetech.sbe.game.log;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryQueryForm.GroupByEnum;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryView.ListView;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GameLogHourlyReportMapperTest extends BaseTest {
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    @Test
    public void testListReport() {
        
        ReportGameLogSummaryQueryForm form = new ReportGameLogSummaryQueryForm();
        form.setDateStart(LocalDate.now().minusDays(10));
        form.setDateEnd(LocalDate.now());
        form.setTimezoneOffset(12);
        form.setGroupBy(GroupByEnum.GAME_PLATFORM.getCode());
        List<ListView> view = gameLogHourlyReportMapper.listReport(form, 1, 10, "en-US", "en-US");
        
        assertTrue(view.size() > 0);
        assertNotNull(view.get(0).getGameApi().getCode());
    }

}
