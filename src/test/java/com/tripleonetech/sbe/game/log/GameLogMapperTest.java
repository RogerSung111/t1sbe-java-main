package com.tripleonetech.sbe.game.log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.ListView;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.SummaryView;

public class GameLogMapperTest extends BaseTest {
    @Autowired
    private GameLogMapper mapper;
    private GameLogReportQueryForm queryForm;

    @Before
    public void setup() {
        // Define dates used for query, move forward/backward by 1 hour to make sure the range covers
        // pre-existing logs
        ZonedDateTime queryStartDate = ZonedDateTime.now().minusDays(7);
        ZonedDateTime todayDate = ZonedDateTime.now().plusHours(1);

        queryForm = new GameLogReportQueryForm();
        queryForm.setBetTimeStart(queryStartDate);
        queryForm.setBetTimeEnd(todayDate);
        queryForm.setPage(1);
        queryForm.setLimit(5);
        queryForm.setPlayerId(2);
    }

    @Test
    public void listReport() {
        Locale.setDefault(Locale.CHINA);
        List<ListView> response = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertTrue((long) response.size() > 0);
    }

    @Test
    public void listForCount() {
        Locale.setDefault(Locale.CHINA);
        queryForm.setLimit(9999);
        List<ListView> response = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertEquals("Should include only test002 game logs", 74, response.size());

        queryForm.setPlayerId(null);
        response = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertEquals("Should include both test002 and test003 game logs", 126, response.size());
    }

    @Test
    public void listReportSameWithNameLike() {
        Locale.setDefault(Locale.CHINA);
        queryForm.setUsername("test002");
        List<ListView> expected = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());

        queryForm.setUsername("002");
        List<ListView> response = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());

        assertEquals(expected.size(), response.size());
    }

    @Test
    public void listReport_WithStatusCancelled() {
        queryForm.setStatus(GameLogStatusEnum.CANCELED.getCode());
        List<ListView> list = mapper.listReport(queryForm, queryForm.getPage(), queryForm.getLimit());
        list.forEach(data -> assertEquals(GameLogStatusEnum.CANCELED.getCode(), data.getStatus().intValue()));
    }

    @Test
    public void testQueryReportSummaryByApi() {
        Locale.setDefault(Locale.CHINA);
        List<SummaryView> response = mapper.queryReportSummaryByApi(queryForm);
        assertTrue((long) response.size() > 0);
    }

    @Test
    public void testQueryReportSummaryByApiSameWithNameLike() {
        Locale.setDefault(Locale.CHINA);
        queryForm.setUsername("test002");
        List<SummaryView> expected = mapper.queryReportSummaryByApi(queryForm);

        queryForm.setUsername("002");
        List<SummaryView> result = mapper.queryReportSummaryByApi(queryForm);

        assertEquals(expected.size(), result.size());
        assertEquals(expected.get(0).getBet(), result.get(0).getBet());
    }
}
