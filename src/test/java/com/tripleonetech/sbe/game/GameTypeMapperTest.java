package com.tripleonetech.sbe.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.privilege.SiteConfigs;

public class GameTypeMapperTest extends BaseTest {
    
    private GameType gameType;
    
    @Autowired
    private GameTypeMapper gameTypeMapper;
    
    @Autowired
    private Constant constant;
    
    @Autowired
    private SiteConfigs siteConfigs;
    

    @Before
    public void init(){
        gameType = new GameType();
        gameType.setEnabled(true);
    }

    @Test
    public void getGameTypeByI18nTest(){
        String locale = "zh-CN";
        List<GameType> gameType = gameTypeMapper.getGameTypes(locale, constant.getBackend().getDefaultLocale());
        assertNotNull(gameType);
    }
    
    @Test
    public void insertGameTypeTest(){
        int returnUid = gameTypeMapper.insertGameType(gameType);
        assertEquals(1, returnUid);
    }
    
    @Test
    public void testListGameTypeWithGameApiByGameCodes() {
        List<String> apiCodes = siteConfigs.getAvailableAPI(SitePrivilegeTypeEnum.GAME_API);
        LocaleAwareQuery localeQuery = new LocaleAwareQuery() {};
        List<GameTypeWithGameApi> gameTypes = gameTypeMapper.listGameTypeWithGameApiByGameCodes(localeQuery, apiCodes);
        assertTrue(gameTypes.size() > 0);
    }

}
