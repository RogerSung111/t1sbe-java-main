package com.tripleonetech.sbe.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame;
import com.tripleonetech.sbe.game.log.ApiGameLog;

public class GameApiFactoryTest extends BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(GameApiFactoryTest.class);

    // number of records in game_api
    private static int numOfGameApiConfig = 18;

    @Autowired
    private GameApiFactory factory;

    @Test
    public void testGetAll() {
        Collection<GameApi<? extends ApiGameLog>> apis = factory.getAll();
        for (GameApi<? extends ApiGameLog> api : apis) {
            logger.debug("Get all [{}]", api.getClass().getName());
            assertEquals("com.tripleonetech.sbe.game.integration.impl", api.getClass().getPackage().getName());
        }
        assertEquals(numOfGameApiConfig, apis.size());
    }

    @Test
    public void testGetApi() {
        GameApi<? extends ApiGameLog> api = factory.getById(1);
        assertTrue(api instanceof GameApiMockGame);
    }

    @Test
    public void testRefreshApi() {
        factory.refreshById(1);
        GameApi<? extends ApiGameLog> api = factory.getById(1);
        assertTrue("After refresh, API must be re-created by ApiFactory", api instanceof GameApiMockGame);
    }
}
