package com.tripleonetech.sbe.redis.listener;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;

public class T1SBEMessageListenerServiceTest extends BaseTest {

    @Autowired
    private T1SBEMessageListenerService t1SBEMessageListenerService;

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    private final static int T1GAGIN_API_ID = 5;
    private final static int T1GBBIN_API_ID = 6;
    private final static int T1GISB2_API_ID = 20;

    @Test
    public void testSetGameApiConfiT1GAginShouldBeDeleted() {
        t1SBEMessageListenerService.setGameApiConfig(BROADCAST_MESSAGE);

        GameApiConfig gameApiConfig = gameApiConfigMapper.getIgnoreDeleted(T1GAGIN_API_ID);
        assertTrue("AGIN_API status should be [DELETED]", gameApiConfig.getStatus().equals(ApiConfigStatusEnum.DELETED));
    }

    @Test
    public void testSetGameApiConfigT1GBbinShouldNotUpdateStatus() {
        GameApiConfig originalBbinGameApiConfig = gameApiConfigMapper.getIgnoreDeleted(T1GBBIN_API_ID);
        t1SBEMessageListenerService.setGameApiConfig(BROADCAST_MESSAGE);

        GameApiConfig gameApiConfig = gameApiConfigMapper.getIgnoreDeleted(T1GBBIN_API_ID);
        assertTrue("BBIN_API status should not be changed", originalBbinGameApiConfig.getStatus().equals(gameApiConfig.getStatus()));

    }

    /**
     * 新的game_api, 且不存在在site_privilege
     */
    @Test
    public void testSetGameApiConfigT1GISBShouldBeInactive() {
        t1SBEMessageListenerService.setGameApiConfig(BROADCAST_MESSAGE);

        GameApiConfig gameApiConfig = gameApiConfigMapper.getIgnoreDeleted(T1GISB2_API_ID);
        assertTrue("ISB_API status should be INACTIVE", ApiConfigStatusEnum.INACTIVE.equals(gameApiConfig.getStatus()));

    }

    /**
     * 存在 Site_privilege = {T1GISB, Active}
     */
    @Test
    public void testSetGameApiConfigT1GISBShouldBeDisabled() {

        //Setup a new Site_Privilege's GAME_API
        SitePrivilege isbGameSitePrivilege = new SitePrivilege();
        isbGameSitePrivilege.setType(SitePrivilegeTypeEnum.GAME_API.name());
        isbGameSitePrivilege.setValue("T1GISB");
        isbGameSitePrivilege.setActive(true);
        List<SitePrivilege> sitePrivileges = new ArrayList<>();
        sitePrivileges.add(isbGameSitePrivilege);
        sitePrivilegeMapper.batchUpdateInsert(sitePrivileges);

        t1SBEMessageListenerService.setGameApiConfig(BROADCAST_MESSAGE);
        GameApiConfig gameApiConfig = gameApiConfigMapper.getIgnoreDeleted(T1GISB2_API_ID);
        assertTrue("ISB_API status should be DISABLED", ApiConfigStatusEnum.DISABLED.equals(gameApiConfig.getStatus()));

    }

    private final String BROADCAST_MESSAGE ="[\n" +
            "  {\n" +
            "    \"id\": \"5\",\n" +
            "    \"code\": \"T1GAGIN\",\n" +
            "    \"name\": \"T1GAGIN CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"20\",\n" +
            "    \"md5\": \"05ad8cb38d0182095be88be831052a48\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"6\",\n" +
            "    \"code\": \"T1GBBIN\",\n" +
            "    \"name\": \"T1GBBIN CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"1\",\n" +
            "    \"md5\": \"1b55513d6f2d35ba19e05bc1afe3b338\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"7\",\n" +
            "    \"code\": \"T1GPRAGMATICPLAY\",\n" +
            "    \"name\": \"T1GPRAGMATICPLAY CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"1\",\n" +
            "    \"md5\": \"e45c6cfc711d5cf13e3c7859a68dfcb0\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"8\",\n" +
            "    \"code\": \"T1GKYCARD\",\n" +
            "    \"name\": \"T1GKYCARD CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"10\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"9\",\n" +
            "    \"code\": \"T1GPT\",\n" +
            "    \"name\": \"T1GPT CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"0\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  }\n" +
            "  ,\n" +
            "  {\n" +
            "    \"id\": \"10\",\n" +
            "    \"code\": \"T1GMGPLUS\",\n" +
            "    \"name\": \"T1GMGPLUS CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"0\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  }\n" +
            "  ,\n" +
            "  {\n" +
            "    \"id\": \"11\",\n" +
            "    \"code\": \"T1GAB\",\n" +
            "    \"name\": \"T1GAB CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"0\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  }\n" +
            "  ,\n" +
            "  {\n" +
            "    \"id\": \"12\",\n" +
            "    \"code\": \"T1GEBET\",\n" +
            "    \"name\": \"T1GEBET CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"0\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  }\n" +
            "  ,\n" +
            "  {\n" +
            "    \"id\": \"20\",\n" +
            "    \"code\": \"T1GISB\",\n" +
            "    \"name\": \"T1GISB CNY\",\n" +
            "    \"currency\": \"CNY\",\n" +
            "    \"status\": \"0\",\n" +
            "    \"md5\": \"40b01e8e0576d9e93a47ff22887547e4\",\n" +
            "    \"meta\":\"{\\\"url\\\": \\\"http://admin.gamegateway.t1t.games/gamegateway\\\",    \\\"merchant_code\\\": \\\"testmerchant\\\",    \\\"secured_key\\\": \\\"511fb01b2f801e9c9f89a61f88152641\\\",    \\\"sign_key\\\": \\\"813067f02aa371611ce5cd1c409b9387\\\",    \\\"prefix_for_username\\\": \\\"dev\\\"  }\"\n" +
            "  }\n" +
            "]";
}
