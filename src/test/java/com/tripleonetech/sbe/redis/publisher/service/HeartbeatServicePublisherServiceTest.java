package com.tripleonetech.sbe.redis.publisher.service;

import static org.junit.Assert.assertNotNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tripleonetech.sbe.BaseTest;

public class HeartbeatServicePublisherServiceTest extends BaseTest {

    @Autowired
    private HeartbeatPublisherService heartbeatPublisherService;

    @Test
    public void getHeartbeatDataTest() throws JSONException {
        String heartbeatData = heartbeatPublisherService.getHeartbeatData();
        JSONObject json = new JSONObject(heartbeatData);

        assertNotNull(json.get("lastReportTime"));
        assertNotNull(json.get("privilege"));
        assertNotNull(json.get("apiStatus"));
        assertNotNull(json.get("refreshToken"));
        assertNotNull(json.get("currentTemplate"));
    }

}
