package com.tripleonetech.sbe.redis;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.SerializationException;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.T1SBEApplication;

@Ignore("Require Redis Setup")
@SpringBootTest(properties = { "spring.profiles.active=test, db-test, daemon, main" }, classes = T1SBEApplication.class)
public class RedisTest extends BaseTest {
    private Logger logger = LoggerFactory.getLogger(RedisTest.class);
    private String CHANNEL_CLIENT_1 = "client.1";
    private String CHANNEL_CLIENT_2 = "client.2";
    private String CHANNEL_DAEMON_SERVER = "daemon.server";
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate<String, T1DaemonCommand> redisTemplate;
    @Autowired
    private RedisService redisService;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void get() throws InterruptedException {
        // flushDB
        stringRedisTemplate.execute(new RedisCallback() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                connection.flushDb();
                return "ok";
            }
        });
        // 测试线程安全
        ExecutorService executorService = Executors.newFixedThreadPool(1000);
        IntStream.range(0, 1000)
                .forEach(i -> executorService.execute(() -> stringRedisTemplate.opsForValue().increment("kk", 1)));
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        Assert.assertEquals("1000", stringRedisTemplate.opsForValue().get("kk"));
    }

    @Test
    public void testString() throws Exception {
        // 保存字符串
        stringRedisTemplate.opsForValue().set("k1", "v1");
        final String k1 = stringRedisTemplate.opsForValue().get("k1");
        logger.info("[字符缓存结果] - [{}]", k1);
        Assert.assertEquals("v1", stringRedisTemplate.opsForValue().get("k1"));
    }

    @Test
    public void testObject() {
        // 以下只演示整合，具体Redis命令可以参考官方文档，Spring Data Redis 只是改了个名字而已，Redis支持的命令它都支持
        String key = "mock:object:1";
        T1DaemonCommand object = new T1DaemonCommand();
        object.setMethodName(T1DaemonMethodEnum.ANNOUNCEMENT);
        redisTemplate.opsForValue().set(key, object);
        // 对应 String（字符串）
        object = redisTemplate.opsForValue().get(key);
        logger.info("[对象缓存结果] - [{}]", object);
        Assert.assertEquals(T1DaemonMethodEnum.ANNOUNCEMENT, object.getMethodName());
    }

    @Test
    public void publishEncryptedAndSigned()
            throws SerializationException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
            NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, SignatureException {
        String serverPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9p+OIzyPhSdzWr8jMnswAkp1nCrKgwSlvLg5pu6/3nHkn92w3FAn3L8T4OBMhyQx17MkBoWyKjCargpea04XGwxxoimo0NHV83l954ErAnBsVFCfdoXoheWvq9ANs517dAEWxCqk0TYuHLR9NbG7Bf4sT3QmFwwX4MTgyMhTcuQIDAQAB";
        String serverPrivateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAL2n44jPI+FJ3NavyMyezACSnWcKsqDBKW8uDmm7r/eceSf3bDcUCfcvxPg4EyHJDHXsyQGhbIqMJquCl5rThcbDHGiKajQ0dXzeX3ngSsCcGxUUJ92heiF5a+r0A2znXt0ARbEKqTRNi4ctH01sbsF/ixPdCYXDBfgxODIyFNy5AgMBAAECgYEAlZiOI2WwWY4XgcgaTTyckX8UZSjNCXwToLzlZYVEIb3s2xeQ5kkKK04kwd4JSUb6azIgwwZ/RTGTdwmFZ/ycsXJ9hnaBfeRW4BssPwwam0hLP+f4iChEriJIBgXXssmp7sTFIeaqDOQwJmIaOdwB2DZLe++QskZA1EoNPprbtmECQQD8QpzU7PAETqc8KyMlGwQcWVft2JJ8f55U1mEFNw35V77DkJ1lbnMuzAHnbswWoObKu482bLB7+j4WC2HODSItAkEAwHetev5yriwFsJBrVKhOW3RTJXSuoWx1+aNBQ717OFPLzxXjqNCuF65c/+FGXO1HryMKiKiSF00LR0/A7e2YPQJAbTAsECHqlgKxcmK9mJ8D/X1SiPnYFhlrIA+W+Rnma4nmYWHkLvZK1vOo01Lq9dR5cUET8RvEnDUp0ILmt7iJlQJAPfmLfBJY/SqhvmQSm5UCfkJrz6DN5p7VMkhWaKin99BLzWgAAckIdgmoWqDAloW1UnjXGIAnYqdlXVYaCsM6BQJADroSGVHRFMolgSwEe72A9v/s4eqRqyX7eFUxQOTKeMTAal9P9YYDLlrNE31NJnHV7wFE0IsFkjMg2a4DF/BZsA==";
        String clientPrivateKey1 = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJB9DB0wgzuj85UjE+DDavVEdGT1pCWBETpviSSackTdcX2WbfJR4OdnSjCAAP8DY6jiDg8sO/9TbLKVmtPQtoBqny3k+w2GG3FvgSRQABBv8BloQRhXHEtIp5L4d21pxViBLHzNP8sH/TKlqbdXXjUnBugZxXBGuNhoNZLOjSSrAgMBAAECgYBYNQ7VLw2etkt5C24SNBqy2vSXweJQxweGfhriTqtd4TZsrg429cdwXX1MOIjjJeWMb1yX5LBIAl0EdRKNJwiSVAwfIRCs165BAGHpbI88ZkfDqlaoC526ln2NzUFcKx4a7uQ07F7byn6eWxlUYsTrMEyLruL04hLhxjdDZmsamQJBAOgHREXK4ymFiUpz2y/QBxEoQVCQtyuudQ44VxXLaLi7DIUbZVloHzVU9cUuxqcDBBYytL61SuHbvqJz5cTPWZUCQQCfaoHckHvM3ey1kF4nn/xveTMjEekN2zmt/3zTXZUCDppL8Giw+2s9bWXI4MHjoeB31AcoeNJT48nDprO6AXU/AkBlN45L19bCrTMX3HR4Zgo/LM1ZRNfy4TbFDkTHCCXVuBsdEbdjii1bCYXxD5evEXy95j68lr9kyYRev6oy8bUFAkAhK0QB5H4dzj8mN/efPwaDGzCsp1labLa/oV0TjXEnkGpCSq89w9I4Yb6/4SP2fQP7ar5iiWEZ21drwg+7L/p5AkBmskxDh6iRP1yAHfzkaTDU5mX7lO2s3sAqXFuPijiArd6DzYAjKyV0Ezd8f7O4GQc+knS3iKxxHuDDE3FdwPQB";
        String clientPrivateKey2 = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAI2o7HUb41i5g+MDUu9+Rpy/1g3yRu6w0wJDP/bje0QLaAIO8WI0f+jF8XRUqukrd85WZ5JbeVDZFTGSKiewHMEJy7RAWCiv5GsVnjHEy6wRpBHFgaZv7SdSDLyeArFh4Z5qTFPKcn6yTMLxaYfDlRydlbtB0Zt9RD6R+jGzzzFDAgMBAAECgYEAi781ZWIZqZF5th21V2XvKvtko2V67EWsI/NLyrcnBz1pYX50SjkYZf2/8M8Fu/iRvBApJIzw5P0Cl2BmDbkC40MoCPl3QWL/I+urNKQ+Aiu6A392Ga98rLKysejW/PAOMowdP6ybsppQhc7it0oDEE8MbBLlsFK7Tj7D+VVX5wECQQDP1MppQvLmax8iGplrvRX3LT+uwMVDFtsr71hL44sibQ+lI6twuWAG3UR05t/fe46ethg+lKatvXAAOAawj/ZjAkEArn4ALrulqYqAcfkl9qAJ9MiE+wN+DPXc9s6jxbFVQsDPekpU14ASd0hG8U1t6y+sDIoYgaOsSKl5TT9fQlffoQJBAJzD63H+t+2qO6+j1Ln5xMnPbcvJSIJUiTnYrgeAOYDcNwLfpv++NHq5CFwNkiymrGt2+06pHhjLXeIsYI9p5/UCQG1bWF/xiGyN+dv0Kr+7IyI/PWBXmvsiZ35imS8/6VBxqFZbGXsJsAkDyjpWrYnVEb22Go+89S+Azx4Dln7staECQQC3eyq9iJfWs8S5YndFq5n01WFRl6ghr0uPz/LsUsF6Fh06e+Xk0GIArqwOBUFTPenxP3ifK964m0BHOtYr1MUM";
        T1DaemonCommand object = new T1DaemonCommand();
        // server publish for clients
        object.setMethodName(T1DaemonMethodEnum.ANNOUNCEMENT);
        redisService.publishSigned(object, CHANNEL_DAEMON_SERVER, serverPrivateKey);
        // clent 1 publish for server
        object.setMethodName(T1DaemonMethodEnum.ANNOUNCEMENT);
        redisService.publishEncryptedAndSigned(object, CHANNEL_CLIENT_1, serverPublicKey, clientPrivateKey1);
        // client 2 publish for server
        object.setMethodName(T1DaemonMethodEnum.ANNOUNCEMENT);
        redisService.publishEncryptedAndSigned(object, CHANNEL_CLIENT_2, serverPublicKey, clientPrivateKey2);
    }

}
