package com.tripleonetech.sbe.report.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.PlayerActivityTypeEnum;

public class PlayerActivityReportMapperTest extends BaseTest {
    
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    
    private PlayerActivityReport playerActivityReport;
    private int PLAYER_ID = 1;
    private long MOCK_PLAYER_ACTIVITY_REPORT_ID_1 = 1;
    private long MOCK_PLAYER_ACTIVITY_REPORT_ID_2 = 2;
    @Before
    public void init() {
        playerActivityReport = new PlayerActivityReport();
        playerActivityReport.setPlayerId(PLAYER_ID);
        playerActivityReport.setAction(PlayerActivityTypeEnum.LOGIN);
        playerActivityReport.setReturnCode(20000);
    }
    
    @Test
    public void testInsert() {
        playerActivityReportMapper.insert(playerActivityReport);
        assertNotNull(playerActivityReportMapper.get(playerActivityReport.getId()));
    }
    
    @Test
    public void testGet() {
        PlayerActivityReport report = playerActivityReportMapper.get(MOCK_PLAYER_ACTIVITY_REPORT_ID_1);
        assertNotNull(report);
        assertTrue(report.getId() > 0);
    }

    @Test
    public void testGetSecondToLastLogin() {
        PlayerActivityReport reportSecondToLast = playerActivityReportMapper.getSecondToLastLogin(PLAYER_ID);
        PlayerActivityReport reportId2 = playerActivityReportMapper.get(MOCK_PLAYER_ACTIVITY_REPORT_ID_2);
        assertNotNull(reportSecondToLast);
        assertEquals(reportSecondToLast.getCreatedAt(), reportId2.getCreatedAt());
    }
}
