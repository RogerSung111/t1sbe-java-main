package com.tripleonetech.sbe.report.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerActivityTypeEnum;

public class PlayerActivityReportServiceTest extends BaseTest {
    
    @Autowired
    private PlayerActivityReportService playerActivityReportService;
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;

    private long MOCK_PLAYER_ACTIVITY_REPORT_ID_2 = 2;
    
    private int MOCK_PLAYER_ID = 1;
    
    @Test
    public void testAddPlayerActivityReport() {
        Player player = new Player();
        player.setId(MOCK_PLAYER_ID);
        playerActivityReportService.addPlayerActivityReport(player, PlayerActivityTypeEnum.LOGIN,
                PlayerActivityReportService.LOGIN_SUCCESS);
    }

    @Test
    public void testGetSecondToLastLogin() {
        PlayerActivityReport reportSecondToLast = playerActivityReportService.getSecondToLastLogin(MOCK_PLAYER_ID);
        PlayerActivityReport reportId2 = playerActivityReportMapper.get(MOCK_PLAYER_ACTIVITY_REPORT_ID_2);
        assertNotNull(reportSecondToLast);
        assertEquals(reportSecondToLast.getCreatedAt(), reportId2.getCreatedAt());
    }
}
