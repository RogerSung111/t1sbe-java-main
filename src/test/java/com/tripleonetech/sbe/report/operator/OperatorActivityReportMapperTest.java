package com.tripleonetech.sbe.report.operator;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.config.Slf4jMDCFilterConfiguration;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class OperatorActivityReportMapperTest extends BaseTest {

    @Autowired
    private OperatorActivityReportMapper operatorActivityReportMapper;

    @Test
    public void testInsertAndGet() {
        OperatorActivityReport report = new OperatorActivityReport();
        report.setInvoke("testClass.testMethod");
        report.setDevice(ClientHttpInfoUtils.getDeviceType());
        report.setIp(ClientHttpInfoUtils.getIp());
        report.setOperatorId(1);
        report.setUsername("Admin");
        report.setReturnCode(20000);
        report.setHttpMethod("POST");
        report.setRequestBody("{}");
        report.setException(null);
        report.setRequestUUID(MDC.get(Slf4jMDCFilterConfiguration.DEFAULT_MDC_UUID_TOKEN_KEY));
        operatorActivityReportMapper.insert(report);

        OperatorActivityReport queried = operatorActivityReportMapper.get(report.getId());
        assertEquals(report.getInvoke(), queried.getInvoke());
        assertEquals(report.getDevice(), queried.getDevice());
        assertEquals(report.getIp(), queried.getIp());
        assertEquals(report.getOperatorId(), queried.getOperatorId());
        assertEquals(report.getUsername(), queried.getUsername());
        assertEquals(report.getReturnCode(), queried.getReturnCode());
        assertEquals(report.getRequestBody(), queried.getRequestBody());
        assertEquals(report.getHttpMethod(), queried.getHttpMethod());
        assertEquals(report.getException(), queried.getException());
    }

    @Test
    public void testGetReport() {
        OperatorActivityReportQueryForm queryForm = new OperatorActivityReportQueryForm();

        // Query report without params
        List<OperatorActivityReportView> reports = operatorActivityReportMapper.getReport(queryForm, 1, 3);
        assertEquals(3, reports.size());

        // Query report with all params
        queryForm.setCreatedAtStartLd(LocalDateTime.now().minusMinutes(3));
        queryForm.setCreatedAtEndLd(LocalDateTime.now());
        queryForm.setUsername("superadmin");
        queryForm.setInvoke("testMethod");
        queryForm.setReturnCode(20000);
        queryForm.setDevice(2);
        queryForm.setIp("45");
        reports = operatorActivityReportMapper.getReport(queryForm, null, null);

        assertEquals(1, reports.size());
        OperatorActivityReportView queried = reports.get(0);
        assertEquals("testClass.testMethod", queried.getInvoke());
        assertEquals(Integer.valueOf(2), queried.getDevice());
        assertEquals("114.32.45.146", queried.getIp());
        assertEquals(Integer.valueOf(1), queried.getOperatorId());
        assertEquals("superadmin", queried.getUsername());
        assertEquals(Integer.valueOf(20000), queried.getReturnCode());
    }

    @Test
    public void testGetReportByUsernameLike() {
        String testName = "admin";
        OperatorActivityReportQueryForm queryForm = new OperatorActivityReportQueryForm();
        // Query report only with Username
        queryForm.setUsername(testName);

        List<OperatorActivityReportView> reports = operatorActivityReportMapper.getReport(queryForm, null, null);

        assertEquals(5, reports.size());
        OperatorActivityReportView queried = reports.get(0);
        assertEquals("114.32.45.146", queried.getIp());
        assertEquals(Integer.valueOf(20000), queried.getReturnCode());
        assertThat(queried.getUsername(), CoreMatchers.containsString(testName));
    }
}
