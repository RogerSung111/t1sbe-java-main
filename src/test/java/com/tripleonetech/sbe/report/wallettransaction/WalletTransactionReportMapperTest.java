package com.tripleonetech.sbe.report.wallettransaction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class WalletTransactionReportMapperTest extends BaseTest {
    private static ZoneId z = ZoneId.of("Z");
    private ZonedDateTime yesterdayDate = Instant.now().atZone(z)
            .minus(2, ChronoUnit.DAYS)
            .minus(1, ChronoUnit.HOURS);
    private ZonedDateTime todayDate = Instant.now().atZone(z)
            .plus(1, ChronoUnit.HOURS);

    @Autowired
    private WalletTransactionReportMapper mapper;
    private WalletTransactionReportQueryForm queryForm;

    @Before
    public void setup() {
        List<Integer> gameApiIds = new ArrayList<>();
        gameApiIds.add(9); // T1GPT CNY
        gameApiIds.add(15); // T1GCQ9 CNY
        queryForm = new WalletTransactionReportQueryForm();
        queryForm.setCurrency(SiteCurrencyEnum.CNY);
        queryForm.setCreatedAtStart(ZonedDateTime.ofInstant(yesterdayDate.toInstant(), ZoneId.systemDefault()));
        queryForm.setCreatedAtEnd(ZonedDateTime.ofInstant(todayDate.toInstant(), ZoneId.systemDefault()));
        queryForm.setGameApiIds(gameApiIds);
    }

    @Test
    public void testQuery() {
        List<WalletTransactionRecord> report = mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);

        WalletTransactionRecord sampleRecord = report.get(0);
        assertTrue(sampleRecord.getId() > 0);
    }

    @Test
    public void testQueryByPlayerCredentialId() {
        WalletTransactionReportQueryForm query = new WalletTransactionReportQueryForm();
        query.setPlayerId(2);
        query.setCurrency(SiteCurrencyEnum.CNY);
        query.setSort("username");

        List<WalletTransactionRecord> report = mapper.query(query, query.getPage(), query.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);
    }

    @Test
    public void testSort() {
        List<WalletTransactionRecord> report = null;
        queryForm.setSort("amount");
        report = mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);

        queryForm.setSort("username");
        report = mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);

        queryForm.setSort("before_balance");
        report = mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);

        queryForm.setSort("after_balance");
        report = mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(report);
        assertTrue(report.size() > 0);
    }
}
