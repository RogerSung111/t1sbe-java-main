package com.tripleonetech.sbe.payment.service;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.player.tag.PlayerTagLogicalOperator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PaymentMethodServiceTest extends BaseTest {
    @Autowired
    private PaymentMethodService paymentMethodService;

    @Test
    public void testQuery() {
        PaymentMethodQueryForm query = new PaymentMethodQueryForm();
        query.setCurrency(SiteCurrencyEnum.CNY);
        query.setName("人工");
        query.setPlayerTagIds(Arrays.asList(1,2,3));
        query.setType(PaymentMethodTypeEnum.BANK_TRANSFER.getCode());
        query.setSort("name DESC");

        List<PaymentMethodDetailView> paymentMethods = paymentMethodService.query(query);
        assertEquals("Query should return 1 result", 1, paymentMethods.size());

        PaymentMethodDetailView paymentMethod = paymentMethods.get(0);
        assertNotNull(paymentMethod.getName());
        assertNotNull(paymentMethod.getType());
        assertNotNull(paymentMethod.getChargeRatio());
        assertNotNull(paymentMethod.getDailyMaxDeposit());
        assertNotNull(paymentMethod.getMinDepositPerTrans());
        assertNotNull(paymentMethod.getMaxDepositPerTrans());
        assertNotNull(paymentMethod.getNote());
        assertEquals(1, paymentMethod.getPlayerTags().size());
        assertEquals(3, paymentMethod.getPlayerGroups().size());
    }

    @Test
    public void testQueryByCurrencyAndPage() {
        PaymentMethodQueryForm query = new PaymentMethodQueryForm();
        query.setCurrency(SiteCurrencyEnum.CNY);

        List<PaymentMethodDetailView> paymentMethods = paymentMethodService.query(query);

        PaymentMethodDetailView paymentMethod = paymentMethods.get(0);
        assertNotNull(paymentMethod.getName());
        assertNotNull(paymentMethod.getType());
        assertNotNull(paymentMethod.getChargeRatio());
        assertNotNull(paymentMethod.getDailyMaxDeposit());
        assertNotNull(paymentMethod.getMinDepositPerTrans());
        assertNotNull(paymentMethod.getMaxDepositPerTrans());
        assertNotNull(paymentMethod.getNote());
        assertEquals(1, paymentMethod.getPlayerTags().size());
        assertEquals(3, paymentMethod.getPlayerGroups().size());
    }

    @Test
    public void testQueryAvailable() {
        // Payment Method id = 1: Group = 1,2,3, Tag = 6
        // [Disabled] Payment Method id = 2: Group = 1,2,3,4
        // [Disabled] Payment Method id = 3: Tag = 6,7, Tag Operator = any
        // [Disabled] Payment Method id = 4: Player = 5
        // Payment Method id = 5: Player = 1,2
        // Payment Method id = 12: Player = 1,2

        List<PaymentMethodDetailView> paymentMethods = null;
        // Player credential id = 2, currency = CNY (player id = 2): Group = 1, Tag = 6, 8
        // Applicable payment methods: No limit = [5,12], By tag and group = [1,2,3], By player id = []
        paymentMethods = paymentMethodService.queryAvailableFor(2, SiteCurrencyEnum.CNY);
        assertEquals("There should be 6 payment methods available for player test002 currency = CNY", 3, paymentMethods.size());

        // Player credential id = 4, currency = CNY (player id = 5): Group = 1, Tag = 7
        // Applicable payment methods: No limit = [5,12], By tag and group = [2,3], By player id = [4]
        paymentMethods = paymentMethodService.queryAvailableFor(4, SiteCurrencyEnum.CNY);
        assertEquals("There should be 5 payment methods available for player test004 currency = CNY", 2, paymentMethods.size());

        // Player credential id = 5, currency = CNY (player id = 6): Group = 1
        // Applicable payment methods: No limit = [5,12], By tag and group = [2], By player id = []
        // Note: Payment Method id = 1 does not qualify because of matching group by unmatching tag
        paymentMethods = paymentMethodService.queryAvailableFor(5, SiteCurrencyEnum.CNY);
        assertEquals("There should be 2 payment methods available for player test005 currency = CNY", 2, paymentMethods.size());
    }

    @Test
    public void testQueryAvailableWithLimit() {
        // Payment Method id = 1: Group = 1,2,3, Tag = 6
        // [Disabled] Payment Method id = 2: Group = 1,2,3,4
        // [Disabled] Payment Method id = 3: Tag = 6,7
        // [Disabled] Payment Method id = 4: Player = 5
        // Payment Method id = 5: Player = [1,2]
        // Payment Method id = 12: Player = [1,2]

        List<PaymentMethodView> paymentMethods = null;
        // Player credential id = 2, currency = CNY (player id = 2): Group = 1, Tag = 6, 8
        // Applicable payment methods: No limit = [5,12], By tag and/or group = [1,2,3], By player id = []
        // But with limit, payment method id = 1 is over limit already (due to deposit request id = 2)
        // Removing disabled 2,3, we are left with 5, 12
        paymentMethods = paymentMethodService.queryAvailableForPayment(2, SiteCurrencyEnum.CNY);
        assertEquals("There should be 2 payment methods available for player test002 currency = CNY", 2, paymentMethods.size());
        paymentMethods.stream().forEach(paymentMethod -> {
            assertNotEquals("Payment method ID = 1 should not be returned", 1, paymentMethod.getId());
            assertNotNull(paymentMethod.getMaxDeposit());
            assertTrue(paymentMethod.getMaxDeposit().compareTo(BigDecimal.ZERO) > 0);
            assertNotNull(paymentMethod.getMinDeposit());
            assertTrue(paymentMethod.getMinDeposit().compareTo(BigDecimal.ZERO) > 0);
        });
    }

    @Test
    public void testGet() {
        PaymentMethod paymentMethod = paymentMethodService.get(1);
        assertEquals(1, paymentMethod.getId());
        assertNotNull(paymentMethod.getPlayerTagIds());
    }

    @Test
    public void testGetDetail() {
        PaymentMethodDetailView paymentMethod = paymentMethodService.getDetail(1);
        assertNotNull(paymentMethod);
        assertEquals(1, paymentMethod.getId());
        assertEquals(1, paymentMethod.getPlayerTags().size());
        assertEquals(3, paymentMethod.getPlayerGroups().size());
    }

    @Test
    public void testEmptyPlayer() {
        // This payment method has no related player tag or player
        PaymentMethodDetailView paymentMethod = paymentMethodService.getDetail(10);
        assertNotNull(paymentMethod);
        assertEquals(10, paymentMethod.getId());
        assertEquals("Should contain empty array of players", 0, paymentMethod.getPlayers().size());
        assertEquals("Should contain empty array of player tags", 0, paymentMethod.getPlayerTagsAndGroups().size());
    }

    @Test
    public void testCreate() {
        PaymentMethodForm newPaymentMethod = new PaymentMethodForm();
        newPaymentMethod.setName("BOC");
        newPaymentMethod.setType(PaymentMethodTypeEnum.BANK_TRANSFER.getCode());
        newPaymentMethod.setDailyMaxDeposit(new BigDecimal(10000));
        newPaymentMethod.setMinDepositPerTrans(new BigDecimal(10));
        newPaymentMethod.setMaxDepositPerTrans(new BigDecimal(1000));
        newPaymentMethod.setCurrency(SiteCurrencyEnum.CNY);
        newPaymentMethod.setChargeRatio(new BigDecimal(1));
        newPaymentMethod.setFixedCharge(new BigDecimal(10));
        newPaymentMethod.setPlayerBearPaymentCharge(false);
        newPaymentMethod.setNote("测试note");
        newPaymentMethod.setPriority(3);
        newPaymentMethod.setPlayerIds(Arrays.asList(2,3,4,5));
        newPaymentMethod.setPlayerTagIds(Arrays.asList(6));
        newPaymentMethod.setPlayerGroupIds(Arrays.asList(1,3));
        newPaymentMethod.setPlayerTagOperator(PlayerTagLogicalOperator.ALL);

        Integer newId = paymentMethodService.create(newPaymentMethod);
        PaymentMethod createdPaymentMethod = paymentMethodService.get(newId);
        assertEquals(newPaymentMethod.getName(), createdPaymentMethod.getName());
        assertEquals(newPaymentMethod.getTypeEnum(), createdPaymentMethod.getType());
        assertEquals(newPaymentMethod.getCurrency(), createdPaymentMethod.getCurrency());
        assertEquals(0, newPaymentMethod.getDailyMaxDeposit().compareTo(createdPaymentMethod.getDailyMaxDeposit()));
        assertEquals(0, newPaymentMethod.getMinDepositPerTrans().compareTo(createdPaymentMethod.getMinDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getMaxDepositPerTrans().compareTo(createdPaymentMethod.getMaxDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getChargeRatio().compareTo(createdPaymentMethod.getChargeRatio()));
        assertEquals(0, newPaymentMethod.getFixedCharge().compareTo(createdPaymentMethod.getFixedCharge()));
        assertEquals(newPaymentMethod.isPlayerBearPaymentCharge(), createdPaymentMethod.isPlayerBearPaymentCharge());
        assertEquals(newPaymentMethod.getNote(), createdPaymentMethod.getNote());
        assertEquals(newPaymentMethod.getPriority(), createdPaymentMethod.getPriority());
        assertEquals(3, createdPaymentMethod.getPlayerTagIds().size());
        assertEquals(4, createdPaymentMethod.getPlayerIds().size());
        assertTrue("Player credential IDs 2,3,4,5 should be converted to player IDs 2,3,5,6",
                createdPaymentMethod.getPlayerIds().containsAll(Arrays.asList(2,3,5,6)));
        assertEquals(PlayerTagLogicalOperator.ALL, createdPaymentMethod.getPlayerTagOperator());
    }

    @Test
    public void testCreateWithNoPlayerIds() {
        PaymentMethodForm newPaymentMethod = new PaymentMethodForm();
        newPaymentMethod.setName("BOC");
        newPaymentMethod.setType(PaymentMethodTypeEnum.BANK_TRANSFER.getCode());
        newPaymentMethod.setDailyMaxDeposit(new BigDecimal(10000));
        newPaymentMethod.setMinDepositPerTrans(new BigDecimal(10));
        newPaymentMethod.setMaxDepositPerTrans(new BigDecimal(1000));
        newPaymentMethod.setCurrency(SiteCurrencyEnum.CNY);
        newPaymentMethod.setChargeRatio(new BigDecimal(1));
        newPaymentMethod.setFixedCharge(new BigDecimal(10));
        newPaymentMethod.setPlayerBearPaymentCharge(false);
        newPaymentMethod.setNote("测试note");
        newPaymentMethod.setPriority(3);
        newPaymentMethod.setPlayerTagIds(Arrays.asList(6));
        newPaymentMethod.setPlayerGroupIds(Arrays.asList(1,3));
        newPaymentMethod.setPlayerTagOperator(PlayerTagLogicalOperator.ALL);

        Integer newId = paymentMethodService.create(newPaymentMethod);
        PaymentMethod createdPaymentMethod = paymentMethodService.get(newId);
        assertEquals(newPaymentMethod.getName(), createdPaymentMethod.getName());
        assertEquals(newPaymentMethod.getTypeEnum(), createdPaymentMethod.getType());
        assertEquals(newPaymentMethod.getCurrency(), createdPaymentMethod.getCurrency());
        assertEquals(0, newPaymentMethod.getDailyMaxDeposit().compareTo(createdPaymentMethod.getDailyMaxDeposit()));
        assertEquals(0, newPaymentMethod.getMinDepositPerTrans().compareTo(createdPaymentMethod.getMinDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getMaxDepositPerTrans().compareTo(createdPaymentMethod.getMaxDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getChargeRatio().compareTo(createdPaymentMethod.getChargeRatio()));
        assertEquals(0, newPaymentMethod.getFixedCharge().compareTo(createdPaymentMethod.getFixedCharge()));
        assertEquals(newPaymentMethod.isPlayerBearPaymentCharge(), createdPaymentMethod.isPlayerBearPaymentCharge());
        assertEquals(newPaymentMethod.getNote(), createdPaymentMethod.getNote());
        assertEquals(newPaymentMethod.getPriority(), createdPaymentMethod.getPriority());
        assertEquals(3, createdPaymentMethod.getPlayerTagIds().size());
        assertEquals(PlayerTagLogicalOperator.ALL, createdPaymentMethod.getPlayerTagOperator());
    }

    @Test
    public void testUpdate() {
        PaymentMethodForm newPaymentMethod = new PaymentMethodForm();
        newPaymentMethod.setName("BOC");
        newPaymentMethod.setType(PaymentMethodTypeEnum.BANK_TRANSFER.getCode());
        newPaymentMethod.setDailyMaxDeposit(new BigDecimal(10000));
        newPaymentMethod.setMinDepositPerTrans(new BigDecimal(10));
        newPaymentMethod.setMaxDepositPerTrans(new BigDecimal(1000));
        newPaymentMethod.setCurrency(SiteCurrencyEnum.CNY);
        newPaymentMethod.setChargeRatio(new BigDecimal(1));
        newPaymentMethod.setFixedCharge(new BigDecimal(10));
        newPaymentMethod.setPlayerBearPaymentCharge(true);
        newPaymentMethod.setNote("测试note");
        newPaymentMethod.setPriority(3);
        newPaymentMethod.setPlayerIds(Arrays.asList(2,3,4,5));
        newPaymentMethod.setPlayerTagIds(Arrays.asList(6));
        newPaymentMethod.setPlayerGroupIds(Arrays.asList(1,3));
        newPaymentMethod.setPlayerTagOperator(PlayerTagLogicalOperator.ALL);

        paymentMethodService.update(1, newPaymentMethod);

        PaymentMethod updatedPaymentMethod = paymentMethodService.get(1);
        assertEquals(newPaymentMethod.getName(), updatedPaymentMethod.getName());
        assertEquals(newPaymentMethod.getTypeEnum(), updatedPaymentMethod.getType());
        assertEquals(newPaymentMethod.getCurrency(), updatedPaymentMethod.getCurrency());
        assertEquals(0, newPaymentMethod.getDailyMaxDeposit().compareTo(updatedPaymentMethod.getDailyMaxDeposit()));
        assertEquals(0, newPaymentMethod.getMinDepositPerTrans().compareTo(updatedPaymentMethod.getMinDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getMaxDepositPerTrans().compareTo(updatedPaymentMethod.getMaxDepositPerTrans()));
        assertEquals(0, newPaymentMethod.getChargeRatio().compareTo(updatedPaymentMethod.getChargeRatio()));
        assertEquals(0, newPaymentMethod.getFixedCharge().compareTo(updatedPaymentMethod.getFixedCharge()));
        assertEquals(newPaymentMethod.isPlayerBearPaymentCharge(), updatedPaymentMethod.isPlayerBearPaymentCharge());
        assertEquals(newPaymentMethod.getNote(), updatedPaymentMethod.getNote());
        assertEquals(newPaymentMethod.getPriority(), updatedPaymentMethod.getPriority());
        assertEquals(3, updatedPaymentMethod.getPlayerTagIds().size());
        assertEquals(4, updatedPaymentMethod.getPlayerIds().size());
        assertEquals(PlayerTagLogicalOperator.ALL, updatedPaymentMethod.getPlayerTagOperator());
    }

    @Test
    public void testUpdateEnabled() {
        paymentMethodService.updateEnabled(1, false);

        PaymentMethod paymentMethod = paymentMethodService.get(1);
        assertFalse("paymentMethod Enabled should be false", paymentMethod.isEnabled());
    }

    @Test
    public void testDelete() {
        int testId = 4;
        paymentMethodService.deleteById(testId);

        PaymentMethod paymentMethod = paymentMethodService.get(testId);
        assertTrue(paymentMethod.isDeleted());
    }

}
