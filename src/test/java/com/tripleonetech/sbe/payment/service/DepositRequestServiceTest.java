package com.tripleonetech.sbe.payment.service;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.operator.OperatorMapper;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.PaymentRequestForm.OperatorPaymentRequestForm;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class DepositRequestServiceTest extends BaseTest {

    @Autowired
    private DepositRequestService depositRequestService;

    @Autowired
    private DepositRequestMapper depositRequestMapper;

    @Autowired
    private PlayerMapper playerMapper;

    @Autowired
    private OperatorMapper operatorMapper;

    @Autowired
    private WalletMapper walletMapper;

    @Autowired
    private PaymentMethodMapper paymentMethodMapper;

    private DepositRequest depositRequest;
    private final int TEST_PAYMENT_METHOD_ID_1 = 1;
    private final int TEST_PAYMENT_METHOD_ID_4 = 4;
    private final String TEST_BANK_CODE = "XCJH23";
    private final int TEST_PLAYER_ID_1 = 1;
    private final BigDecimal TEST_DEPOSIT_AMOUNT_10 = BigDecimal.TEN;
    private final Integer operatorId = 1;

    private void init() {
        PaymentMethod paymentMethod = paymentMethodMapper.get(TEST_PAYMENT_METHOD_ID_1);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));

        depositRequest = new DepositRequest();
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(6));
        depositRequest.setAmount(TEST_DEPOSIT_AMOUNT_10);
        depositRequest.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_1);
        depositRequest.setPlayerId(TEST_PLAYER_ID_1);
        depositRequest.setPaymentCharge(TEST_DEPOSIT_AMOUNT_10.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequestMapper.insert(depositRequest);
        depositRequest = depositRequestMapper.get(depositRequest.getId());
    }

    @Test
    public void testCreateDepositRequestByPlayer() {
        Player player = playerMapper.get(TEST_PLAYER_ID_1);
        PaymentRequestForm form = new PaymentRequestForm();
        form.setAmount(BigDecimal.TEN);
        form.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_1);
        form.setBankCode(TEST_BANK_CODE);

        DepositRequest depositRequest = depositRequestService.createDepositRequest(form, player);
        assertNotNull(depositRequest);
        assertNotNull(depositRequest.getId());
    }

    @Test
    public void testCreateDepositRequestByOperator() {
        Operator operator = operatorMapper.get(1);
        OperatorPaymentRequestForm form = new OperatorPaymentRequestForm();
        form.setAmount(BigDecimal.TEN);
        form.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_1);
        form.setBankCode(TEST_BANK_CODE);
        form.setPlayerId(TEST_PLAYER_ID_1);

        DepositRequest depositRequest = depositRequestService.createDepositRequest(form, operator);
        assertNotNull(depositRequest);
        assertNotNull(depositRequest.getId());
    }

    @Test
    public void testCreateDepositRequestAndPaymentChargeShouldNotBeZero() {
        Player player = playerMapper.get(TEST_PLAYER_ID_1);
        PaymentRequestForm form = new PaymentRequestForm();
        form.setAmount(new BigDecimal(1000));
        form.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_4);
        form.setBankCode(TEST_BANK_CODE);

        DepositRequest depositRequest = depositRequestService.createDepositRequest(form, player);
        assertNotNull(depositRequest);
        //parment_charge = 1000 * 1.5% + 10 = 25
        assertTrue(depositRequest.getPaymentCharge().compareTo(new BigDecimal(25)) == 0);
    }

    @Test
    public void testProcessWithdrawRequest() {
        init();
        depositRequestService.processDepositRequest(depositRequest, DepositStatusEnum.REJECTED, operatorId);
        DepositRequest newStatusDepositRequest = depositRequestMapper.get(depositRequest.getId());
        assertEquals(DepositStatusEnum.REJECTED, newStatusDepositRequest.getStatus());
    }


    @Test
    public void testApproveDepositRequest() {
        init();
        BigDecimal originalMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1).getBalance();
        depositRequestService.approveDepositRequest(depositRequest, operatorId);
        BigDecimal newMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1).getBalance();
        assertTrue(originalMainWallet.add(TEST_DEPOSIT_AMOUNT_10).compareTo(newMainWallet) == 0);
    }


    @Test
    public void testRejectDepositRequest() {
        init();
        depositRequestService.processDepositRequest(depositRequest, DepositStatusEnum.REJECTED, operatorId);
        DepositRequest newStatusDepositRequest = depositRequestMapper.get(depositRequest.getId());
        assertEquals(DepositStatusEnum.REJECTED, newStatusDepositRequest.getStatus());
    }

    @Test
    public void testApproveDepositRequestShouldMinusPaymentCharge() {
        BigDecimal depositAmount = new BigDecimal(100);
        PaymentRequestForm form = new PaymentRequestForm();
        form.setAmount(depositAmount);
        form.setBankCode(TEST_BANK_CODE);
        form.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_4);
        Player player = playerMapper.get(TEST_PLAYER_ID_1);
        depositRequest = depositRequestService.createDepositRequest(form, player);
        //pament_charge = 100 * 1.5% + 10 = 11.5
        BigDecimal paymentCharge = depositRequest.getPaymentCharge();

        BigDecimal originalMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1).getBalance();
        depositRequestService.approveDepositRequest(depositRequest, operatorId);
        BigDecimal newMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1).getBalance();
        assertTrue(originalMainWallet.add(depositAmount).subtract(paymentCharge).compareTo(newMainWallet) == 0);
    }

}
