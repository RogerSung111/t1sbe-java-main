package com.tripleonetech.sbe.payment.history;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DepositRequestProcessHistoryMapperTest extends BaseTest {

    @Autowired
    private DepositRequestProcessHistoryMapper requestHistoryMapper;

    @Test
    public void testInsert() {
        long depositRequestId = 100003;
        int operatorId = 2;

        DepositRequestProcessHistory requestHistory = new DepositRequestProcessHistory();
        requestHistory.setComment("Create deposit request");
        requestHistory.setCreatedBy(operatorId);
        requestHistory.setUpdatedBy(operatorId);
        requestHistory.setDepositRequestId(depositRequestId);
        requestHistory.setFromStatus(DepositStatusEnum.OPEN);
        requestHistory.setToStatus(DepositStatusEnum.APPROVED);
        requestHistoryMapper.insert(requestHistory);

        assertNotNull(requestHistory.getId());

        DepositRequestProcessHistoryView newProcessHistory = requestHistoryMapper.getComments(depositRequestId)
                .stream().filter(h -> h.getToStatus().equals(DepositStatusEnum.APPROVED)).findAny().get();
        assertNotNull(newProcessHistory);
    }

    @Test
    public void testGetComment() {
        long depositRequestId = 100001;
        List<DepositRequestProcessHistoryView> comments = requestHistoryMapper.getComments(depositRequestId);
        assertTrue(comments.size() > 0);

        assertTrue(StringUtils.isNotBlank(comments.get(0).getFromStatus().name()));
        assertTrue(StringUtils.isNotBlank(comments.get(0).getToStatus().name()));
        assertNotNull(comments.get(0).getCreatedBy());
        assertNotNull(comments.get(0).getUpdatedBy());
    }
}
