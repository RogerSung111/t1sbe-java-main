package com.tripleonetech.sbe.payment;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.payment.integration.impl.PaymentApiMockPay;

public class PaymentApiFactoryTest extends BaseTest {
    @Autowired
    private PaymentApiFactory factory;

    @Test
    public void testGetApi() {
        PaymentApi api = factory.getById(1);
        assertTrue(api instanceof PaymentApiMockPay);
    }

    @Test
    public void testRefreshApi() {
        factory.refreshById(1);
        PaymentApi api = factory.getById(1);
        assertTrue("After refresh, API must be re-created by ApiFactory", api instanceof PaymentApiMockPay);
    }
}
