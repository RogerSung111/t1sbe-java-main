package com.tripleonetech.sbe.payment.report;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.TimeRangeForm;

public class DepositSummaryReportMapperTest extends BaseTest {
    @Autowired
    private DepositSummaryReportMapper depositSummaryReportMapper;
    
    @Test
    public void testUpsertDirty() {
        DepositSummaryReport depositSummaryReport = new DepositSummaryReport();
        LocalDateTime timeHour = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);
        depositSummaryReport.setTimeHour(timeHour);
        depositSummaryReport.setCurrency(SiteCurrencyEnum.IDR);
        depositSummaryReport.setDepositAmount(new BigDecimal(100));
        depositSummaryReport.setDepositCount(1);
        depositSummaryReport.setDepositorCount(1);
        depositSummaryReport.setDirty(false);
        depositSummaryReportMapper.upsertDirty(depositSummaryReport);
        
        DepositSummaryReport newDepositSummaryReport = depositSummaryReportMapper.get(timeHour, SiteCurrencyEnum.IDR);
        assertNotNull(newDepositSummaryReport);
    }
    
    @Test
    public void  testCalculateSummaryReport() {
        List<DepositSummaryReport> depositSummaryReportItems = depositSummaryReportMapper.listDirty();
        assertTrue(depositSummaryReportItems.size() > 0);
        
        depositSummaryReportMapper.refreshDirty();
        List<DepositSummaryReport> refreshedDepositSummaryReportItems = depositSummaryReportMapper.listDirty();
        assertTrue(refreshedDepositSummaryReportItems.size() == 0);
    }
    
    @Test
    public void testGet() {
        //配合v3測試資料
        LocalDate date = LocalDate.now().minusDays(1);
        LocalTime time = LocalTime.of(13, 00);
        assertNotNull(depositSummaryReportMapper.get(date.atTime(time), SiteCurrencyEnum.CNY));
    }
    
    @Test
    public void testListDirty() {
        assertTrue(depositSummaryReportMapper.listDirty().size() > 0);
    }

    @Test
    public void testQuery() {
        TimeRangeForm form = new TimeRangeForm();
        form.setDateStart(LocalDate.now().minusDays(7));
        form.setDateEnd(LocalDate.now().minusDays(1));
        assertTrue(depositSummaryReportMapper.query(form, form.getPage(), form.getLimit()).size() > 0);
    }
}
