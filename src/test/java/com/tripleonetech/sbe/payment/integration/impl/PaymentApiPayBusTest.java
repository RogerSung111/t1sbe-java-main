package com.tripleonetech.sbe.payment.integration.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.integration.impl.paybus.PayBusPlatformInformationResponse;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;

public class PaymentApiPayBusTest extends BaseApiTest {

    private static final int PAYBUS_HIPAY_EBANK = 14;
    private static final int PAYMENT_METHOD_ID = 12;
    private static final int PLAYER_ID = 2;
    private static final int CALLBACK_DEPOSIT_REQUEST_ID = 131146;
    private static final int SUBMIT_DEPOSIT_REQUEST_ID = 111132;
    private DepositRequest depositRequest;
    private PaymentApiPayBus api;

    @Autowired
    private PaymentApiFactory factory;
    
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    @Autowired
    private WalletMapper walletMapper;
    
    @Before
    public void init(){
        
        api = (PaymentApiPayBus)factory.getById(PAYBUS_HIPAY_EBANK);        
    }
    
    private void createDepositRequest() {
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(100.00);
        depositRequest = new DepositRequest();
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setRequestedDate(LocalDateTime.now());
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);   
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequestMapper.insert(depositRequest);
        
    }
    @Test
    public void testSubmitRequestShouldBeSuccess() {
        createDepositRequest();
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { SUCCESSFUL_DEPOSIT_REQUISITION }));
        PaymentApiResult apiResult = api.submitRequest(depositRequest);
        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
        assertNotNull(apiResult.getRedirectUrl());
        assertNotNull(apiResult.getRedirectHttpMethod());
        assertNotNull(apiResult.getRedirectParams());
        
        DepositRequest depositRequest = depositRequestMapper.get(SUBMIT_DEPOSIT_REQUEST_ID);
        assertNotNull(depositRequest.getExternalUid());
    }
    
    @Test
    public void testSubmitRequestShouldBeSignatureVerificationFailure() {
        createDepositRequest();
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { DEPOSIT_REQUISITION_WITH_WRONG_SIGN }));
        PaymentApiResult apiResult = api.submitRequest(depositRequest);
        assertEquals(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED, apiResult.getResponseEnum());

    }
    
    @Test
    public void testCallbackShouldBeSuccess() {
        Wallet originalWallet = walletMapper.getMainWallet(PLAYER_ID);
        // the following parameters from payment-api document
        String mockSign = "f5434192e491036377dcb8efe4cc11eb9f18561d";
        BigDecimal depsoitAmount = new BigDecimal(200);
        Map<String, Object> callbackParameters = new HashMap<>();
        callbackParameters.put("merchant_code", "0356727fbc6241368785dc2ea74304ae");
        callbackParameters.put("amount", depsoitAmount);
        callbackParameters.put("real_amount", depsoitAmount);
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("status", 7);
        callbackParameters.put("order_no", "gk9G153xZhA");
        callbackParameters.put("custom_no", "131146");
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(ApiResponseEnum.OK, result.getResponseEnum());
        
        Wallet newWallet = walletMapper.getMainWallet(PLAYER_ID);
        assertTrue(newWallet.getBalance().compareTo(originalWallet.getBalance().add(depsoitAmount)) == 0);
    }
    
    @Test
    public void testCallbackShouldBeSignatureVerificationFailue() {
        Wallet originalWallet = walletMapper.getMainWallet(PLAYER_ID);
        // the following parameters from payment-api document
        String mockSign = "f5434192e491036377dcb8efe4cc11eb9f18561e";
        BigDecimal depsoitAmount = new BigDecimal(200);
        Map<String, Object> callbackParameters = new HashMap<>();
        callbackParameters.put("merchant_code", "0356727fbc6241368785dc2ea74304ae");
        callbackParameters.put("amount", depsoitAmount);
        callbackParameters.put("real_amount", depsoitAmount);
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("status", 7);
        callbackParameters.put("order_no", "gk9G153xZhA");
        callbackParameters.put("custom_no", "131146");
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED, result.getResponseEnum());
        //player's balance shouldn't be changed.
        Wallet newWallet = walletMapper.getMainWallet(PLAYER_ID);
        assertTrue(newWallet.getBalance().compareTo(originalWallet.getBalance()) == 0);
    }
    
    @Test
    public void testCallbackShouldBeSuccessButDepsoitIsRejected() {
        Wallet originalWallet = walletMapper.getMainWallet(PLAYER_ID);
        // the following parameters from payment-api document
        String mockSign = "617f9136a25f3ca4c735480cf003da969edf3d8c";
        BigDecimal depsoitAmount = new BigDecimal(200);
        Map<String, Object> callbackParameters = new HashMap<>();
        callbackParameters.put("merchant_code", "0356727fbc6241368785dc2ea74304ae");
        callbackParameters.put("amount", depsoitAmount);
        callbackParameters.put("real_amount", depsoitAmount);
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("status", 3);
        callbackParameters.put("order_no", "gk9G153xZhA");
        callbackParameters.put("custom_no", CALLBACK_DEPOSIT_REQUEST_ID);
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(ApiResponseEnum.OK, result.getResponseEnum());
        
        Wallet newWallet = walletMapper.getMainWallet(PLAYER_ID);
        assertTrue(newWallet.getBalance().compareTo(originalWallet.getBalance()) == 0);
        
        DepositRequest depositRequest = depositRequestMapper.get(CALLBACK_DEPOSIT_REQUEST_ID);
        assertTrue("Deposit_Request's status should be Rejected", depositRequest.getStatus().equals(DepositStatusEnum.REJECTED));
    }
    
    
    @Test
    public void testCallbackShouldBeFailureBecauseWrongDepositRequestId() {
        Wallet originalWallet = walletMapper.getMainWallet(PLAYER_ID);
        // the following parameters from payment-api document
        String mockSign = "a18cf97a95f1aaaaa94709d71a6a334b1d6df361";
        BigDecimal depsoitAmount = new BigDecimal(200);
        Map<String, Object> callbackParameters = new HashMap<>();
        callbackParameters.put("merchant_code", "0356727fbc6241368785dc2ea74304ae");
        callbackParameters.put("amount", depsoitAmount);
        callbackParameters.put("real_amount", depsoitAmount);
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("status", 7);
        callbackParameters.put("order_no", "gk9G153xZhA");
        callbackParameters.put("custom_no", "1234567");
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(ApiResponseEnum.ACTION_UNSUCCESSFUL, result.getResponseEnum());
        
        Wallet newWallet = walletMapper.getMainWallet(PLAYER_ID);
        assertTrue(newWallet.getBalance().compareTo(originalWallet.getBalance()) == 0);
        
    }
    
    
    @Test
    public void testQueryPlatformInfomation() {
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { PLATFORM_INFORMATION }));
        PayBusPlatformInformationResponse response = api.queryPlatformInfomation();
        assertNotNull(response);
        assertTrue(response.getTotal() > 0);
        assertNotNull(response.getItems().get(0));
        assertNotNull(response.getItems().get(0).getDepositChannels().size() > 0);
        
        PayBusPlatformInformationResponse.Item.DepositChannel depositChannel = response.getItems().get(0).getDepositChannels().stream()
                .filter(channel -> channel.getName().equals("ALI_PAY")).findAny().orElse(null);
        assertNotNull(depositChannel);
        //depending on following PLATFORM_INFORMATION String
        assertTrue(depositChannel.getMax().compareTo(new BigDecimal(10000)) == 0);
        assertTrue(depositChannel.getMin().compareTo(new BigDecimal(300)) == 0);
    }
    
    @Test
    public void testQueryDepositInformation() {
        DepositRequest depositRequest = depositRequestMapper.get(131146);
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { DEPOSIT_REQUISITION_INFO }));
        PaymentApiResult result = api.queryDepositInformation(depositRequest);
        assertEquals(ApiResponseEnum.OK, result.getResponseEnum());
        assertNotNull(result.getParam("response"));
    }
    
    private final String SUCCESSFUL_DEPOSIT_REQUISITION = "{\n" + 
            "  \"success\": true,\n" + 
            "  \"message\": \"OK\",\n" + 
            "  \"code\": 1,\n" + 
            "  \"data\": {\n" + 
            "    \"merchant_code\": \"0356727fbc6241368785dc2ea74304ae\",\n" + 
            "    \"custom_no\": \"" + SUBMIT_DEPOSIT_REQUEST_ID + "\",\n" + 
            "    \"pay_url\": \"https://winhipay.net:443/hipay_load/gateway/Pay_Index.html\",\n" + 
            "    \"form\": {\n" + 
            "      \"pay_memberid\": \"HP123108\",\n" + 
            "      \"pay_orderid\": \"qVg5wPr0vcW\",\n" + 
            "      \"pay_amount\": 100,\n" + 
            "      \"pay_applydate\": \"20200724094101\",\n" + 
            "      \"pay_channelCode\": \"IMB\",\n" + 
            "      \"pay_userid\": \"1\",\n" + 
            "      \"pay_notifyurl\": \"https://pay-api.lottery-demo.tripleone.tech/pay/notify/hipay\",\n" + 
            "      \"pay_md5sign\": \"07B6D3B23B2CAE2F235C2757F69D59DF\"\n" + 
            "    },\n" + 
            "    \"amount\": 100,\n" + 
            "    \"real_amount\": null,\n" + 
            "    \"status\": 5,\n" + 
            "    \"order_no\": \"qVg5wPr0vcW\",\n" + 
            "    \"sign\": \"2fae3d351a13d4fcec88585e22ddc55a28537010\"\n" + 
            "  }\n" + 
            "}";
    
    private final String DEPOSIT_REQUISITION_WITH_WRONG_SIGN = "{\n" + 
            "  \"success\": true,\n" + 
            "  \"message\": \"OK\",\n" + 
            "  \"code\": 1,\n" + 
            "  \"data\": {\n" + 
            "    \"merchant_code\": \"0356727fbc6241368785dc2ea74304ae\",\n" + 
            "    \"custom_no\": \""+ SUBMIT_DEPOSIT_REQUEST_ID +"\",\n" + 
            "    \"pay_url\": \"https://winhipay.net:443/hipay_load/gateway/Pay_Index.html\",\n" + 
            "    \"form\": {\n" + 
            "      \"pay_memberid\": \"HP123108\",\n" + 
            "      \"pay_orderid\": \"qVg5wPr0vcW\",\n" + 
            "      \"pay_amount\": 100,\n" + 
            "      \"pay_applydate\": \"20200724094101\",\n" + 
            "      \"pay_channelCode\": \"IMB\",\n" + 
            "      \"pay_userid\": \"1\",\n" + 
            "      \"pay_notifyurl\": \"https://pay-api.lottery-demo.tripleone.tech/pay/notify/hipay\",\n" + 
            "      \"pay_md5sign\": \"07B6D3B23B2CAE2F235C2757F69D59DE\"\n" + 
            "    },\n" + 
            "    \"amount\": 100,\n" + 
            "    \"real_amount\": null,\n" + 
            "    \"status\": 5,\n" + 
            "    \"order_no\": \"qVg5wPr0vcW\",\n" + 
            "    \"sign\": \"2fae3d351a13d4fcec88585erererewre28537010\"\n" + 
            "  }\n" + 
            "}";
    
    private final String DEPOSIT_REQUISITION_INFO = "{\n" + 
            "  \"success\": true,\n" + 
            "  \"message\": \"OK\",\n" + 
            "  \"code\": 1,\n" + 
            "  \"data\": {\n" + 
            "    \"merchant_code\": \"0356727fbc6241368785dc2ea74304ae\",\n" + 
            "    \"amount\": 1000,\n" + 
            "    \"real_amount\": 1000,\n" + 
            "    \"status\": 7,\n" + 
            "    \"refund_status\": \"\",\n" + 
            "    \"order_no\": \"3R5VR4LJoh8\",\n" + 
            "    \"custom_no\": \"111132\",\n" + 
            "    \"bank\": \"\",\n" + 
            "    \"currency\": \"CNY\",\n" + 
            "    \"platform_order_no\": null\n" + 
            "  }\n" + 
            "}";
    
    private final String PLATFORM_INFORMATION = "{\n" + 
            "  \"total\": 2,\n" + 
            "  \"items\": [\n" + 
            "    {\n" + 
            "      \"id\": 86,\n" + 
            "      \"platform\": \"HIPAY\",\n" + 
            "      \"merchant_code\": \"HP123108\",\n" + 
            "      \"deposit_channels\": [\n" + 
            "        {\n" + 
            "          \"name\": \"WECHAT_PAY\"\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"WECHAT_H5\",\n" + 
            "          \"sub\": [\n" + 
            "            \"WW\",\n" + 
            "            \"WW2\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"ALI_PAY\",\n" + 
            "          \"sub\": [\n" + 
            "            \"IMA\",\n" + 
            "            \"EA\",\n" + 
            "            \"A\",\n" + 
            "            \"A2\",\n" + 
            "            \"A3\",\n" + 
            "            \"AP\",\n" + 
            "            \"ATB\"\n" + 
            "          ],\n" + 
            "          \"max\": 10000,\n" + 
            "          \"min\": 300\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"ALI_H5\",\n" + 
            "          \"sub\": [\n" + 
            "            \"AW\",\n" + 
            "            \"AW2\",\n" + 
            "            \"APW\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"UNION_PAY\",\n" + 
            "          \"sub\": [\n" + 
            "            \"UN\",\n" + 
            "            \"UN2\",\n" + 
            "            \"UNB\",\n" + 
            "            \"UNB2\",\n" + 
            "            \"KJ\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"EBANK\",\n" + 
            "          \"sub\": [\n" + 
            "            \"IMB\",\n" + 
            "            \"B\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"JD_PAY\"\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"JD_H5\"\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"QQ_PAY\"\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"QQ_H5\"\n" + 
            "        }\n" + 
            "      ],\n" + 
            "      \"withdraw_channels\": null\n" + 
            "    },\n" + 
            "    {\n" + 
            "      \"id\": 87,\n" + 
            "      \"platform\": \"TXH\",\n" + 
            "      \"merchant_code\": \"test\",\n" + 
            "      \"deposit_channels\": [\n" + 
            "        {\n" + 
            "          \"name\": \"WECHAT_PAY\",\n" + 
            "          \"sub\": [\n" + 
            "            \"WECHAT_PAY\",\n" + 
            "            \"ZSM\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"WECHAT_H5\",\n" + 
            "          \"sub\": [\n" + 
            "            \"WECHAT_H5\",\n" + 
            "            \"ZSM\"\n" + 
            "          ]\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"ALI_PAY\",\n" + 
            "          \"sub\": [\n" + 
            "            \"ALI_PAY\",\n" + 
            "            \"ACT\"\n" + 
            "          ],\n" + 
            "          \"max\": 10000,\n" + 
            "          \"min\": 300\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"ALI_H5\",\n" + 
            "          \"sub\": [\n" + 
            "            \"ALI_H5\",\n" + 
            "            \"ACT\"\n" + 
            "          ],\n" + 
            "          \"max\": 10000,\n" + 
            "          \"min\": 300\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"EBANK_H5\"\n" + 
            "        },\n" + 
            "        {\n" + 
            "          \"name\": \"EBANK\"\n" + 
            "        }\n" + 
            "      ],\n" + 
            "      \"withdraw_channels\": []\n" + 
            "    }\n" + 
            "  ]\n" + 
            "}";
}
