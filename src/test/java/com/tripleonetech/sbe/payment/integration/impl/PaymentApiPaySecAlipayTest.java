package com.tripleonetech.sbe.payment.integration.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.payment.*;

public class PaymentApiPaySecAlipayTest extends BaseApiTest {

    private final static Logger logger = LoggerFactory.getLogger(PaymentApiPaySecAlipayTest.class);

    private static final int PAYSEC_ID = 7;
    private static final int PAYMENT_METHOD_PAYSEC_ALIPAY_ID = 7;
    private static final int PLAYER_ID = 1;
    private static final String MERCHANT_CODE = "838bccf9-5d94-41bc-af9a-76541f46140d";

    private PaymentApiPaySecAlipay api;
    private DepositRequest depositRequest;

    @Autowired
    private PaymentApiFactory factory;
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    @Before
    public void init() {

        api = (PaymentApiPaySecAlipay) factory.getById(PAYSEC_ID);
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_PAYSEC_ALIPAY_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(100.00);
        // mock deposit_request
        depositRequest = new DepositRequest();
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_PAYSEC_ALIPAY_ID);
        depositRequest.setRequestedDate(LocalDateTime.now());
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));


    }

    private static String mockTokenResponse;
    private static String mockQrCodeUrl;

    @BeforeClass
    public static void initMocks() {
        mockTokenResponse = "{\n" +
                " \"header\":{ \n" +
                "       \"status\": \"SUCCESS\",\n" +
                "       \"statusMessage\":null\n" +
                "       },\n" +
                " \"body\":{\n" +
                "       \"token\":\"3WNTT1TU1EX6WXUKW83UPJ7D2K4F1V4EIJWSN1AK6BSA7ZX15A6ONXVLZMY\"\n" +
                "       }\n" +
                "}";

        mockQrCodeUrl = "{\n" +
                "       \"qrCode\":\"weixin://wxpay/bizpayurl?pr=6DCkOVi\",\n" +
                "       \"transactionReference\":\"Trsansaction-123456\",\n" +
                "       \"status\":\"SUCCESS\",\n" +
                "       \"statusMessage\":\"\" \n" +
                "}";
    }

    @Test
    public void depositTest() {
        depositRequestMapper.insert(depositRequest);
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockTokenResponse, mockQrCodeUrl }));
        ApiResult result = api.submitRequest(depositRequest);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);
    }

    // Callback - Success
    @Test
    public void callbackTest() {
        depositRequestMapper.insert(depositRequest);
        depositRequest = depositRequestMapper.get(depositRequest.getId());

        Map<String, String> signatureParams = new LinkedHashMap<>();
        signatureParams.put("cartId", String.valueOf(depositRequest.getId()));
        signatureParams.put("orderAmount", depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        signatureParams.put("currency", "CNY");
        signatureParams.put("merchantCode", MERCHANT_CODE);
        signatureParams.put("version", "3.0");
        signatureParams.put("status", "SUCCESS");
        String signature = api.getSignature(signatureParams);

        long orderTime = DateUtils.localToInstant(depositRequest.getRequestedDate()).toEpochMilli();

        ZonedDateTime nowZd = LocalDateTime.now().atZone(ZoneId.systemDefault());
        long completedTime = nowZd.toInstant().toEpochMilli();

        Map<String, String> mockRespondedParams = new HashMap<>();
        mockRespondedParams.put("status", "SUCCESS");
        mockRespondedParams.put("statusMessage", "Completed Deposit");
        mockRespondedParams.put("cartId", String.valueOf(depositRequest.getId()));
        mockRespondedParams.put("transactionReference", "PAYSEC-1551154707046");
        mockRespondedParams.put("currency", "CNY");
        mockRespondedParams.put("orderAmount",
                depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        mockRespondedParams.put("orderTime", String.valueOf(orderTime));
        mockRespondedParams.put("completedTime", String.valueOf(completedTime));
        mockRespondedParams.put("version", "3.0");
        mockRespondedParams.put("signature", signature);

        logger.info("=============Starting callback testing (1)==================");
        logger.info("Use case : Successfully execute callback");
        ApiResult result = api.callback(mockRespondedParams);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);

        logger.info("=============Starting callback testing (2)==================");
        logger.info("Use case : callback again from PAYSEC");
        result = api.callback(mockRespondedParams);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);
    }

}
