package com.tripleonetech.sbe.payment.integration.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.payment.*;

public class PaymentApiOnePayBankTransferPayTest extends BaseApiTest {
    
    private static final int PAYSEC_ID = 10;
    private static final int PAYMENT_METHOD_ID = 2;
    private static final int PLAYER_ID = 1;
    
    private DepositRequest depositRequest;
    private PaymentApiOnePayBankTransferPay api;
    
    @Autowired
    private PaymentApiFactory factory;
    
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    Map<String, String> callbackParameters = new HashMap<>();
    
    @Before
    public void init(){
        api = (PaymentApiOnePayBankTransferPay)factory.getById(PAYSEC_ID);
    }
    
    @Test
    public void testSubmitRequest() {
        
        //mock deposit_request
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(999.00);
        
        depositRequest = new DepositRequest();
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setRequestedDate(LocalDateTime.now());
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);   
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequestMapper.insert(depositRequest);      

        ApiResult apiResult = api.submitRequest(depositRequest);
        assertEquals(apiResult.getResponseEnum(), ApiResponseEnum.OK);
    }
    
    private void initCallback() {
        //create a mock deposit_request which should be matched with [mock sign]
        String requestDate = "2017-04-20T19:52:57";
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(100.00);
        
        depositRequest = new DepositRequest();
        depositRequest.setId(4245368915748504L);
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setRequestedDate(LocalDateTime.parse(requestDate));
        depositRequest.setExpirationDate(LocalDateTime.parse(requestDate).plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequestMapper.insert(depositRequest);
    }
    
    @Test
    public void testCallback() {
        
        initCallback();
        
        // the following parameters from payment-api document
        String mockSign = "6B4B676F6B72444562582F615278723571344F50735A725A495634666E666353754B6E5A786C74723"
                + "642755A7439686B707754366F535A787079612B4C455A704F395970695A6B46387478720A4E7764344D2F7"
                + "6556453475330326E70744C754F6B55706B764171426C444C305531374A49775535303573327658484E7438"
                + "4F7032746D7975632F393677766250456D742F44363750366B670A4D2B57532F41374E636C48657671385A4747553D";
        
        callbackParameters.put("amountFee", "1");
        callbackParameters.put("merchantId", "752");
        callbackParameters.put("tradeStatus", "PS_PAYMENT_SUCCESS");
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("signType", "RSA");
        callbackParameters.put("merchantTradeId", "4245368915748504");
        callbackParameters.put("payEndTime", "2017-04-20 20:52:57");
        callbackParameters.put("currency", "JPY");
        callbackParameters.put("version", "1.0");
        callbackParameters.put("pwTradeId", "174124007522");
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);
    }
}
