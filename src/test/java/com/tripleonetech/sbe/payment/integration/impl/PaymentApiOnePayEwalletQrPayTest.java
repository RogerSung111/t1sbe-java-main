package com.tripleonetech.sbe.payment.integration.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.config.PaymentApiConfig;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;

public class PaymentApiOnePayEwalletQrPayTest extends BaseApiTest {

    private static final int PAYMENT_API_CONFIG_ID = 11;
    private static final int PAYMENT_METHOD_ID = 3;
    private static final int PLAYER_ID = 1;
    
    private DepositRequest depositRequest;
    private PaymentApiOnePayEwalletQrPay api;
    
    @Autowired
    private PaymentApiConfigMapper apiConfigMapper;
    
    @Autowired
    private PaymentApiFactory factory;
    
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    Map<String, String> callbackParameters = new HashMap<>();
    
    @Before
    public void init(){
        
        api = (PaymentApiOnePayEwalletQrPay) factory.getById(PAYMENT_API_CONFIG_ID);
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        // mock deposit_request
        BigDecimal depositAmount = new BigDecimal(100.00);
        depositRequest = new DepositRequest();
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setPaymentChannel("ALIPAY");
        depositRequest.setRequestedDate(LocalDateTime.now());
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequestMapper.insert(depositRequest);

    }
    
    String mockRequestResponse = "{\n" + 
            "  \"flag\": \"SUCCESS\",\n" + 
            "  \"data\": {\n" + 
            "    \"qrUrl\": \"weixin://wxpay/bizpayurl?pr=jBVI759\",\n" + 
            "    \"sign\": \"723146784631702F546F646859362B69794D76536E3561424367764F46757353524446587A6D39366251482B38357032666F4169387350474B33394642456237424138494B66706E614F427A0A466D4134374A56714154546D4F2B6A31714A45754A653330473562436668515A6C7438526F79505177505A7453585A455365396B345675484E61685A4C513746373675696D3231734B514E640A7074767375367A52724E6E4765743556514A383D\"\n" + 
            "  }\n" + 
            "}";
    private void initSubmitRequest() {
        //set apiconfig which should be matched with test case from of payment-api doc.
        PaymentApiConfig apiConfig = apiConfigMapper.get(PAYMENT_API_CONFIG_ID);
        ObjectNode metaJson = JsonUtils.jsonToObject(apiConfig.getMeta(), ObjectNode.class);
        String mockPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDmwwFxxkClvsBzZUtE0CN4S7P0QWZxnpxn2De0zlqbjY6Put/8738SXYkGsuBIb5QZU3tDb/0hmON3zQ84BLexksP2iNqY1q1VSeY2NkV/QxrCUefUedTFsDU+ZcIB5JJ02m4fqpYtzYowtf5JrgjYHcyrO1IaX3NVITm9EPOMHQIDAQAB";
        metaJson.put("publicKey", mockPublicKey);

        apiConfig.setMeta(metaJson.toString());
        apiConfigMapper.update(apiConfig);
        apiConfig = apiConfigMapper.get(PAYMENT_API_CONFIG_ID);
    }
    @Test
    public void testSubmitRequest() {
        
        initSubmitRequest();
        
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockRequestResponse }));
        ApiResult result = api.submitRequest(depositRequest);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);

    }

    @Test
    public void testCallback() {
        
      //create a mock deposit_request which should be macted with callback order
        String requestDate = "2017-04-20T19:52:57";
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(100.00);
        
        depositRequest = new DepositRequest();
        depositRequest.setId(3093449721012028L);
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentChannel("WECHAT");
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setRequestedDate(LocalDateTime.parse(requestDate));
        depositRequest.setExpirationDate(LocalDateTime.parse(requestDate).plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));

        depositRequestMapper.insert(depositRequest);      
        
        // callback parameters from payment provider
        String sign = "61702B30323578776C6D6D624C684B54544B715856346A467A35754F3262336A"
                + "574D614F333179316346706D394C4F473354325A576E4C765A72736E6E53454531505"
                + "733306B7A75656E62660A367230396D7433774D4C512B422B526B6D3358393338692B3"
                + "4667A5843544872612B466E2B625536784133706A32446C32654F435971426C36764D54"
                + "3665474147596C3667724630537642300A49584C4763354E5235504B654E7663432F42513D";
        
        
        callbackParameters.put("amount", "0.10");
        callbackParameters.put("order_no", "3093449721012028");
        callbackParameters.put("payment_channel", "WECHAT");
        callbackParameters.put("payment_id", "177776007522");
        callbackParameters.put("currency", "CNY");
        callbackParameters.put("body", "paytest");
        callbackParameters.put("app_id", "752");
        callbackParameters.put("finish_time", "20180201110028");
        callbackParameters.put("status", "PS_PAYMENT_SUCCESS");
        callbackParameters.put("sign", sign);
        
        ApiResult result = api.callback(callbackParameters);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);
        
    }
}
