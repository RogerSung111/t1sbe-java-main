package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DepositRequestMapperTest extends BaseTest {
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    private DepositRequest depositRequest;
    private BigDecimal paymentCharge;

    @Before
    public void init() {
        int playerId = 1;
        int paymentMethodId = 1;

        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentMethodId);
        depositRequest = new DepositRequest();
        depositRequest.setPlayerId(playerId);
        depositRequest.setPaymentMethodId(paymentMethodId);
        depositRequest.setAmount(BigDecimal.valueOf(100));
        depositRequest.setExpirationDate(LocalDateTime.now());
        paymentCharge = paymentMethod.getChargeRatio().divide(new BigDecimal(100)).multiply(depositRequest.getAmount()).add(paymentMethod.getFixedCharge());
        depositRequest.setPaymentCharge(paymentCharge);
        depositRequest.setStatus(DepositStatusEnum.OPEN);
    }

    @Test
    public void testInsert() {
        depositRequestMapper.insert(depositRequest);
        assertNotNull(depositRequest.getId());
        assertEquals(paymentCharge, depositRequest.getPaymentCharge());
    }

    @Test
    public void testApprove() {
        long depositRequestId = 100001;
        depositRequestMapper.updateStatus(depositRequestId, DepositStatusEnum.REJECTED);
        depositRequest = depositRequestMapper.get(depositRequestId);
        int status = depositRequest.getStatus().getCode();
        assertEquals(DepositStatusEnum.REJECTED.getCode(), status);

    }

    @Test
    public void testGet() {
        long depositRequestId = 100001;
        depositRequest = depositRequestMapper.get(depositRequestId);
        assertNotNull(depositRequest.getId());
    }

    @Test
    public void testList() {
        DepositRequestQueryForm queryForm = new DepositRequestQueryForm();
        queryForm.setCurrency(SiteCurrencyEnum.CNY);
        queryForm.setPage(1);
        queryForm.setLimit(10);
        queryForm.setStatus(DepositStatusEnum.APPROVED.getCode());
        queryForm.setSort("expiration_date DESC");
        List<DepositRequestView> depositRequestList = depositRequestMapper.list(queryForm, queryForm.getPage(),
                queryForm.getLimit());
        assertNotNull(depositRequestList);
        assertTrue(depositRequestList.size() > 0);

        queryForm = new DepositRequestQueryForm();
        queryForm.setPlayerId(2);
        depositRequestList = depositRequestMapper.list(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertNotNull(depositRequestList);
        assertTrue(depositRequestList.size() > 0);
        assertEquals("test002", depositRequestList.get(0).getUsername());

    }

    @Test
    public void testGetPendingRequestCount() {
        long pendingRequestCount = depositRequestMapper.getPendingRequestCount();
        DepositRequestQueryForm queryForm = new DepositRequestQueryForm();
        queryForm.setStatus(DepositStatusEnum.OPEN.getCode());
        long expectedCount = depositRequestMapper.list(queryForm, 1, 999999).size();
        assertEquals(expectedCount, pendingRequestCount);
    }
}
