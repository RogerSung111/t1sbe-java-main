package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PaymentMethodMapperTest extends BaseTest {
    @Autowired
    private PaymentMethodMapper mapper;

    @Test
    public void testGet() {
        PaymentMethod paymentMethod = mapper.get(1);
        assertNotNull(paymentMethod);
        assertNotNull(paymentMethod.getCurrency());
    }

    @Test
    public void testQuery() {
        PaymentMethodQueryForm query = new PaymentMethodQueryForm();
        query.setCurrency(SiteCurrencyEnum.CNY);
        query.setName("人工");
        query.setPlayerTagIds(Arrays.asList(1,2,3));

        List<Integer> paymentMethodIds = mapper.query(query);
        assertEquals(1, paymentMethodIds.size());
        assertEquals(1, paymentMethodIds.get(0).intValue());
    }

    @Test
    public void testQueryIdOnly() {
        PaymentMethodQueryForm query = new PaymentMethodQueryForm();
        query.setId(1);

        List<Integer> paymentMethodIds = mapper.query(query);
        assertEquals(1, paymentMethodIds.size());
        assertEquals(1, paymentMethodIds.get(0).intValue());
    }

    @Test
    public void testQueryAvailable() {
        List<PaymentMethodTagFiltered> paymentMethods = mapper.queryAvailable(SiteCurrencyEnum.CNY);
        assertEquals("There are 3 payment methods enabled under CNY", 3, paymentMethods.size());
    }

    @Test
    public void testGetById() {
        PaymentMethodQueryForm query = new PaymentMethodQueryForm();
        query.setSort("name DESC");

        List<PaymentMethodDetailView> paymentMethods = mapper.getById(new HashSet<>(Arrays.asList(1,2,3)), query);
        assertEquals(3, paymentMethods.size());
        IntStream.range(0, paymentMethods.size()).forEach(i -> {
            PaymentMethodDetailView paymentMethod = paymentMethods.get(i);
            assertNotNull(paymentMethod);
            assertEquals(SiteCurrencyEnum.CNY, paymentMethod.getCurrency());
            assertTrue(paymentMethod.getMaxDepositPerTrans().intValue() > 0);
        });
    }

    @Test
    public void testUpdate() {
        PaymentMethod paymentMethod = mapper.get(2);
        paymentMethod.setPaymentApiId(null);
        mapper.update(paymentMethod);

        PaymentMethod updatedPaymentMethod = mapper.get(2);
        assertNull(updatedPaymentMethod.getPaymentApiId());
    }
}
