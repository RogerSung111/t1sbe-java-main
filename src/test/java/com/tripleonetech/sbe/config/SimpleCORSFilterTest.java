package com.tripleonetech.sbe.config;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import com.tripleonetech.sbe.BaseMainTest;

public class SimpleCORSFilterTest extends BaseMainTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Test
    public void testEnabledCORS() throws Exception {
        
        this.mockMvc.perform(get("/players")
                .header("Authorization", "Bearer " + obtainAccessToken())
                .header("Content-Type", "application/json")
                .with(csrf())
                .header("Accept-Language", "en-US"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(header().string("Access-Control-Allow-Origin", "*"));
        
    }
    
}
