package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class SummaryDailyMapperTest extends BaseTest {
    @Autowired
    private SummaryDailyMapper summaryDailyMapper;

    @Before
    public void setUp() {
    }

    @Test
    public void testInsertSummary() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryDaily summaryDaily = new SummaryDaily();
        summaryDaily.setTimezone("UTC");
        summaryDaily.setCurrency(SiteCurrencyEnum.CNY);
        summaryDaily.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryDaily.setTotalPlayerCount(3L);
        summaryDaily.setTotalDepositCount(3L);
        summaryDaily.setTotalDepositAmount(new BigDecimal("333.33"));
        summaryDaily.setTotalWithdrawalCount(3L);
        summaryDaily.setTotalWithdrawalAmount(new BigDecimal("333.33"));
        summaryDaily.setBonus(new BigDecimal("33"));
        summaryDaily.setCashback(new BigDecimal("33"));
        summaryDaily.setBetAmount(new BigDecimal("33"));
        summaryDaily.setNewPlayerWithoutDeposit(3L);
        summaryDaily.setNewPlayerWithDeposit(3L);
        summaryDaily.setActivePlayerCount(3L);
        summaryDaily.setActivePlayerCountByGametype("{\"Live Game\":33}");
        summaryDaily.setPlayerTotalWin(new BigDecimal("33"));
        summaryDaily.setPlayerTotalLoss(new BigDecimal("333.33"));

        int count = summaryDailyMapper.insert(summaryDaily);
        assertEquals("Summary daily should be updated as a new record", 2, count);

        SummaryDaily newRecord = summaryDailyMapper.query(SiteCurrencyEnum.CNY);
        assertEquals("Default timezone does not match, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", SiteCurrencyEnum.CNY, newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime), newRecord.getStartDate());
        assertEquals("Failed matching new ActivePlayerCountByGametype, please check!", "{\"Live Game\":33}",
                newRecord.getActivePlayerCountByGametype());
    }
}
