package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.tomcat.jni.Local;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryBetGrossProfitStatusMapperTest extends BaseTest {
    @Autowired
    private SummaryBetGrossProfitStatusMapper summaryBetGrossProfitStatusMapper;

    @Test
    public void testInsertSummaryBetGrossProfitStatus() {
        ZoneId zoneId = ZoneId.of("UTC");
        // as per the requirement specification doc, Sunday will be the first day of the week
        LocalDate startTime = LocalDate.now(zoneId).minusDays(6 + LocalDate.now(zoneId).plusDays(1).getDayOfWeek().getValue());

        SummaryBetGrossProfitStatus summaryBetGrossProfitStatus = new SummaryBetGrossProfitStatus();
        summaryBetGrossProfitStatus.setPeriod(SummaryPeriodTypeEnum.LAST_WEEK);
        summaryBetGrossProfitStatus.setTimezone("UTC");
        summaryBetGrossProfitStatus.setCurrency(SiteCurrencyEnum.CNY);
        summaryBetGrossProfitStatus.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryBetGrossProfitStatus.setBetAmount(new BigDecimal(66.660));
        summaryBetGrossProfitStatus.setPlayerWin(new BigDecimal(66.660));
        summaryBetGrossProfitStatus.setPlayerLoss(new BigDecimal(66.660));

        int count = summaryBetGrossProfitStatusMapper.insert(summaryBetGrossProfitStatus);
        assertEquals("Summary bet gross profit status should be updated as a new record", 2, count);

        SummaryBetGrossProfitStatus newRecord = summaryBetGrossProfitStatusMapper.query(SummaryPeriodTypeEnum.LAST_WEEK,
                SiteCurrencyEnum.CNY, 1).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        BigDecimal expectedValue = new BigDecimal("66.660");
        assertEquals("New bet amount should match ", expectedValue, newRecord.getBetAmount());
        assertEquals("New player win should match ", expectedValue, newRecord.getPlayerWin());
        assertEquals("New player loss should match ", expectedValue, newRecord.getPlayerLoss());
    }
}
