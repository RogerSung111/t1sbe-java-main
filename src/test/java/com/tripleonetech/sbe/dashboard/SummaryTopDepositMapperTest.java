package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopDepositCategoryEnum;

public class SummaryTopDepositMapperTest extends BaseTest {
    @Autowired
    private SummaryTopDepositMapper summaryTopDepositMapper;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testInsertSummaryTopDeposit() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryTopDeposit summaryTopDeposit = new SummaryTopDeposit();
        summaryTopDeposit.setPeriod(SummaryPeriodTypeEnum.TODAY);
        summaryTopDeposit.setTimezone("UTC");
        summaryTopDeposit.setCurrency(SiteCurrencyEnum.CNY);
        summaryTopDeposit.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopDeposit.setCategory(SummaryTopDepositCategoryEnum.BY_COUNT);
        summaryTopDeposit.setRank((byte) 1);
        summaryTopDeposit.setPlayerUsername("test002");
        summaryTopDeposit.setDepositCount(3L);
        summaryTopDeposit.setDepositMethod("bank deposit");
        summaryTopDeposit.setDepositAmount(new BigDecimal(666.666));

        int count = summaryTopDepositMapper.insert(summaryTopDeposit);
        assertEquals("Summary top deposit should be updated as a new record", 2, count);

        SummaryTopDeposit newRecord = summaryTopDepositMapper.query(SummaryPeriodTypeEnum.TODAY,
                SiteCurrencyEnum.CNY, SummaryTopDepositCategoryEnum.BY_COUNT).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New bet amount should match ", 3L, (long) newRecord.getDepositCount());
        assertEquals("New player win should match ", "bank deposit", newRecord.getDepositMethod());
        assertEquals("New player loss should match ", new BigDecimal("666.666"), newRecord.getDepositAmount());

    }
}
