package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryTopWinnerMapperTest extends BaseTest {
    @Autowired
    private SummaryTopWinnerMapper summaryTopWinnerMapper;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testInsertSummaryTopWinner() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryTopWinner summaryTopWinner = new SummaryTopWinner();
        summaryTopWinner.setPeriod(SummaryPeriodTypeEnum.TODAY);
        summaryTopWinner.setTimezone("UTC");
        summaryTopWinner.setCurrency(SiteCurrencyEnum.CNY);
        summaryTopWinner.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopWinner.setRank((byte) 1);
        summaryTopWinner.setPlayerUsername("test002");
        summaryTopWinner.setPlayerWin(new BigDecimal(666.666));

        int count = summaryTopWinnerMapper.insert(summaryTopWinner);
        assertEquals("Summary top winner should be updated as a new record", 2, count);

        SummaryTopWinner newRecord = summaryTopWinnerMapper.query(SummaryPeriodTypeEnum.TODAY,
                SiteCurrencyEnum.CNY).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New player ID should match 3", "test002", newRecord.getPlayerUsername());
        assertEquals("New player win should match 666.666", new BigDecimal("666.666"), newRecord.getPlayerWin());
    }
}
