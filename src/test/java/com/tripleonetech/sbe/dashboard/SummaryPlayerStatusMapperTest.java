package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryPlayerStatusMapperTest extends BaseTest {
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testInsertSummaryPlayerStatus() {
        ZoneId zoneId = ZoneId.of("UTC");
        // as per the requirement specification doc, Sunday will be the first day of the week
        LocalDate startTime = LocalDate.now(zoneId).minusDays(6 + LocalDate.now(zoneId).plusDays(1).getDayOfWeek().getValue());

        long dummyCount = 6L;
        SummaryPlayerStatus summaryPlayerStatus = new SummaryPlayerStatus();
        summaryPlayerStatus.setPeriod(SummaryPeriodTypeEnum.LAST_WEEK);
        summaryPlayerStatus.setTimezone("UTC");
        summaryPlayerStatus.setCurrency(SiteCurrencyEnum.CNY);
        summaryPlayerStatus.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryPlayerStatus.setNewPlayerWithoutDeposit(dummyCount);
        summaryPlayerStatus.setNewPlayerWithDeposit(dummyCount);
        summaryPlayerStatus.setTotalPlayerCount(dummyCount);

        int count = summaryPlayerStatusMapper.insert(summaryPlayerStatus);
        assertEquals("Summary player status should be updated as a new record", 2, count);

        SummaryPlayerStatus newRecord = summaryPlayerStatusMapper.query(SummaryPeriodTypeEnum.LAST_WEEK,
                SiteCurrencyEnum.CNY, 1).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New bet amount should match ", dummyCount, newRecord.getNewPlayerWithoutDeposit());
        assertEquals("New player win should match ", dummyCount, newRecord.getNewPlayerWithDeposit());
        assertEquals("New player loss should match ", dummyCount, newRecord.getTotalPlayerCount());
    }
}
