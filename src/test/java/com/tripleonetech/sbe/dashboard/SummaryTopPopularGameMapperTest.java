package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopPopularGameCategoryEnum;

public class SummaryTopPopularGameMapperTest extends BaseTest {
    @Autowired
    private SummaryTopPopularGameMapper summaryTopGamePolpularMapper;
    @Autowired
    private Constant constant;

    private String locale;

    @Before
    public void setUp() throws Exception {
        locale = constant.getBackend().getDefaultLocale();
    }

    @Test
    public void testInsertSummaryTopGamePopular() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryTopPopularGame summaryTopPopularGame = new SummaryTopPopularGame();
        summaryTopPopularGame.setPeriod(SummaryPeriodTypeEnum.TODAY);
        summaryTopPopularGame.setTimezone("UTC");
        summaryTopPopularGame.setCurrency(SiteCurrencyEnum.CNY);
        summaryTopPopularGame.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopPopularGame.setCategory(SummaryTopPopularGameCategoryEnum.BY_PLAYER);
        summaryTopPopularGame.setRank((byte) 1);
        summaryTopPopularGame.setGameCode("FD1");
        summaryTopPopularGame.setPlayerCount(3);
        summaryTopPopularGame.setBetAmount(new BigDecimal(999999.999));
        summaryTopPopularGame.setPlayerWin(new BigDecimal(999999.999));
        summaryTopPopularGame.setPlayerLoss(new BigDecimal(999999.999));

        int count = summaryTopGamePolpularMapper.insert(summaryTopPopularGame);
        assertEquals("Summary top game popular should be updated as a new record", 2, count);

        SummaryTopPopularGame newRecord = summaryTopGamePolpularMapper.query(SummaryPeriodTypeEnum.TODAY,
                SiteCurrencyEnum.CNY, SummaryTopPopularGameCategoryEnum.BY_PLAYER, locale, null).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New GameApiName should match MockGame", "MockGame", newRecord.getGameApiCode());
        assertEquals("New Game Code should match FD1", "FD1", newRecord.getGameCode());
        assertEquals("New Game Name should match Fortune Day", "Fortune Day", newRecord.getGameName());
        assertEquals("New player count should match 3", 3, newRecord.getPlayerCount());
        assertEquals("New bet amount should match 666.666", new BigDecimal("999999.999"), newRecord.getBetAmount());
        assertEquals("New player win should match 666.666", new BigDecimal("999999.999"), newRecord.getPlayerWin());
        assertEquals("New player loss should match 666.666", new BigDecimal("999999.999"), newRecord.getPlayerLoss());
    }
}
