package com.tripleonetech.sbe.dashboard;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopWithdrawCategoryEnum;

public class SummaryTopWithdrawalMapperTest extends BaseTest {
    @Autowired
    private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testInsertSummaryTopWithdrawal() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryTopWithdrawal summaryTopWithdrawal = new SummaryTopWithdrawal();
        summaryTopWithdrawal.setPeriod(SummaryPeriodTypeEnum.TODAY);
        summaryTopWithdrawal.setTimezone("UTC");
        summaryTopWithdrawal.setCurrency(SiteCurrencyEnum.CNY);
        summaryTopWithdrawal.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopWithdrawal.setCategory(SummaryTopWithdrawCategoryEnum.BY_COUNT);
        summaryTopWithdrawal.setRank((byte) 1);
        summaryTopWithdrawal.setPlayerUsername("test002");
        summaryTopWithdrawal.setWithdrawalCount(6L);
        summaryTopWithdrawal.setWithdrawalAmount(new BigDecimal(666.666));

        int count = summaryTopWithdrawalMapper.insert(summaryTopWithdrawal);
        assertEquals("Summary top withdrawal should be updated as a new record", 2, count);

        SummaryTopWithdrawal newRecord = summaryTopWithdrawalMapper.query(SummaryPeriodTypeEnum.TODAY,
                SiteCurrencyEnum.CNY, SummaryTopWithdrawCategoryEnum.BY_COUNT).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New player ID should match 3", "test002", newRecord.getPlayerUsername());
        assertEquals("New withdrawal count should match 6", 6L, (long) newRecord.getWithdrawalCount());
        assertEquals("New withdrawal amount should match 666.666", new BigDecimal("666.666"),
                newRecord.getWithdrawalAmount());

    }
}
