package com.tripleonetech.sbe.dashboard.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constants;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.*;

public class CalculatorMonthlyTest extends BaseTest {
    @Autowired
    CalculatorMonthly calculatorMonthly;
    @Autowired
    private SummaryBetGrossProfitStatusMapper summaryBetGrossProfitStatusMapper;
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;
    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;
    // @Autowired
    // private SummaryTopDepositMapper summaryTopDepositMapper;
    // @Autowired
    // private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;
    // @Autowired
    // private SummaryTopWinnerMapper summaryTopWinnerMapper;
    // @Autowired
    // private SummaryTopPopularGameMapper summaryTopPopularGameMapper;
    // @Autowired
    // private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;

    private static LocalDate today;
    private SummaryPeriodTypeEnum period = SummaryPeriodTypeEnum.LAST_MONTH;
    private SiteCurrencyEnum siteCurrency;

    private LocalDate dayStart;

    @Before
    public void setUp() throws Exception {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
        calculatorMonthly.run();

        LocalDate previousMonth = today.minusMonths(1);
        dayStart = previousMonth.withDayOfMonth(1);
    }

    // private void setStartDateParam() {
    // LocalDate previousMonth = today.minusMonths(1);
    // dayStart = previousMonth.withDayOfMonth(1);
    // }

    // private void assertTopDeposit(SummaryTopDeposit newRecord, SummaryTopDepositCategoryEnum category) {
    // assertEquals("Failed matching new timezone, please check!", "UTC",
    // newRecord.getTimezone());
    // assertEquals("Failed matching new currency, please check!", siteCurrency,
    // newRecord.getCurrency());
    // assertEquals("Failed matching new startDate, please check!",
    // DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
    // assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    // assertEquals("Failed matching new top deposit category, please check!", category, newRecord.getCategory());
    // }
    //
    // private void assertTopWithdraw(SummaryTopWithdrawal newRecord, SummaryTopWithdrawCategoryEnum category) {
    // assertEquals("Failed matching new timezone, please check!", "UTC",
    // newRecord.getTimezone());
    // assertEquals("Failed matching new currency, please check!", siteCurrency,
    // newRecord.getCurrency());
    // assertEquals("Failed matching new startDate, please check!",
    // DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
    // assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    // assertEquals("Failed matching new top withdraw category, please check!", category,
    // newRecord.getCategory());
    // }
    //
    // private void assertTopPopularGame(SummaryTopPopularGame newRecord, SummaryTopPopularGameCategoryEnum category) {
    // assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    // assertEquals("Failed matching new timezone, please check!", "UTC",
    // newRecord.getTimezone());
    // assertEquals("Failed matching new currency, please check!", siteCurrency,
    // newRecord.getCurrency());
    // assertEquals("Failed matching new startDate, please check!",
    // DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
    // assertEquals("Failed matching new top withdraw category, please check!", category,
    // newRecord.getCategory());
    // }

    @Test
    public void testSummaryBetGrossProfitStatus() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // for (int i = 1; i < 12; i++) {
            // setStartDateParam();
            SummaryBetGrossProfitStatus newRecord = summaryBetGrossProfitStatusMapper.query(period, siteCurrency, 1)
                    .get(0);

            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC", newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getBetAmount());
            assertNotNull(newRecord.getPlayerWin());
            assertNotNull(newRecord.getPlayerLoss());
            // }
        }
    }

    @Test
    public void testSummaryPlayerStatus() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // for (int i = 1; i < 12; i++) {
            // setStartDateParam();
            SummaryPlayerStatus newRecord = summaryPlayerStatusMapper.query(period, siteCurrency, 1).get(0);
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getNewPlayerWithDeposit());
            assertNotNull(newRecord.getNewPlayerWithoutDeposit());
            assertNotNull(newRecord.getTotalPlayerCount());
            // }
        }
    }

    @Test
    public void testSummaryNetProfit() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // for (int i = 1; i < 12; i++) {
            // setStartDateParam();
            SummaryNetProfit newRecord = summaryNetProfitMapper.query(period, siteCurrency, 1).get(0);

            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC", newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getBonus());
            assertNotNull(newRecord.getCashback());
            assertNotNull(newRecord.getPlayerWin());
            assertNotNull(newRecord.getPlayerLoss());
            // }
        }
    }

    // @Test
    // public void testSummaryTopDeposit() {
    // setStartDateParam(1);
    // for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
    // siteCurrency = sc;
    // for (SummaryTopDepositCategoryEnum c : SummaryTopDepositCategoryEnum.values()) {
    // SummaryTopDeposit newRecord = summaryTopDepositMapper.query(period, siteCurrency, sDayStart, c, rank)
    // .get(0);
    // assertTopDeposit(newRecord, c);
    // }
    // }
    // }

    // @Test
    // public void testSummaryTopWithdrawal() {
    // setStartDateParam(1);
    // for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
    // siteCurrency = sc;
    // for (SummaryTopWithdrawCategoryEnum c : SummaryTopWithdrawCategoryEnum.values()) {
    // SummaryTopWithdrawal newRecord = summaryTopWithdrawalMapper.query(period, siteCurrency, sDayStart, c,
    // rank).get(0);
    // assertTopWithdraw(newRecord, c);
    // }
    // }
    // }

    // @Test
    // public void testSummaryTopWinner() {
    // setStartDateParam(1);
    // for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
    // siteCurrency = sc;
    // SummaryTopWinner newRecord = summaryTopWinnerMapper.query(period, siteCurrency, sDayStart, rank).get(0);
    //
    // assertEquals("Failed matching new timezone, please check!", "UTC",
    // newRecord.getTimezone());
    // assertEquals("Failed matching new currency, please check!", siteCurrency,
    // newRecord.getCurrency());
    // assertEquals("Failed matching new startDate, please check!",
    // DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
    // assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    // }
    // }

    // @Test
    // public void testSummaryTopPopularGame() {
    // setStartDateParam(1);
    // for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
    // siteCurrency = sc;
    // for (SummaryTopPopularGameCategoryEnum c : SummaryTopPopularGameCategoryEnum.values()) {
    // SummaryTopPopularGame newRecord = summaryTopPopularGameMapper.query(period, siteCurrency, sDayStart,
    // c, rank, locale, null).get(0);
    // assertTopPopularGame(newRecord, c);
    // }
    // }
    // }

    // @Test
    // public void testSummaryGameProviderInfo() {
    // setStartDateParam(0);
    // for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
    // siteCurrency = sc;
    // SummaryGameProviderInfo newRecord = summaryGameProviderInfoMapper.query(period, siteCurrency, sDayStart,
    // rank, locale, locale).get(0);
    //
    // assertEquals("Failed matching new timezone, please check!", "UTC",
    // newRecord.getTimezone());
    // assertEquals("Failed matching new currency, please check!", siteCurrency,
    // newRecord.getCurrency());
    // assertEquals("Failed matching new startDate, please check!",
    // DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
    // assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    // }
    //
    // }
}
