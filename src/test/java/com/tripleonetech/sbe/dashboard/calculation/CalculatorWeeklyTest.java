package com.tripleonetech.sbe.dashboard.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.*;

public class CalculatorWeeklyTest extends BaseTest {
    @Autowired
    CalculatorWeekly calculatorWeekly;
    @Autowired
    private SummaryBetGrossProfitStatusMapper summaryBetGrossProfitStatusMapper;
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;
    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;

    private static LocalDate today;
    private SummaryPeriodTypeEnum period = SummaryPeriodTypeEnum.LAST_WEEK;
    private SiteCurrencyEnum siteCurrency;

    private LocalDate dayStart;

    @Before
    public void setUp() throws Exception {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
        calculatorWeekly.run();
    }

    private void setStartDateParam() {
        // as per the requirement specification doc, Sunday will be the first day of the week
        dayStart = today.minusDays(6 + today.plusDays(1).getDayOfWeek().getValue());
    }

    @Test
    public void testSummaryBetGrossProfitStatus() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            setStartDateParam();
            SummaryBetGrossProfitStatus newRecord = summaryBetGrossProfitStatusMapper.query(period, siteCurrency,8)
                    .get(0);
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getBetAmount());
            assertNotNull(newRecord.getPlayerWin());
            assertNotNull(newRecord.getPlayerLoss());
        }
    }

    @Test
    public void testSummaryPlayerStatus() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // for (int i = 1; i < 8; i++) {
            setStartDateParam();
            SummaryPlayerStatus newRecord = summaryPlayerStatusMapper.query(period, siteCurrency,8).get(0);
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getNewPlayerWithDeposit());
            assertNotNull(newRecord.getNewPlayerWithoutDeposit());
            assertNotNull(newRecord.getTotalPlayerCount());
            // }
        }
    }

    @Test
    public void testSummaryNetProfit() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // for (int i = 1; i < 8; i++) {
            setStartDateParam();
            SummaryNetProfit newRecord = summaryNetProfitMapper.query(period, siteCurrency,8).get(0);
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
            assertNotNull(newRecord.getBonus());
            assertNotNull(newRecord.getCashback());
            assertNotNull(newRecord.getPlayerWin());
            assertNotNull(newRecord.getPlayerLoss());
            // }
        }
    }
}
