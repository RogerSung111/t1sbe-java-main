package com.tripleonetech.sbe.dashboard.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.*;
import com.tripleonetech.sbe.dashboard.calculation.SummaryGameProviderInfoCalculatorSource.TopGameProviderByBet;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

public class CalculatorRealTimeTest extends BaseTest {
    @Autowired
    private CalculatorRealTime calculatorRealTime;
    @Autowired
    private SummaryDailyMapper summaryDailyMapper;
    @Autowired
    private SummaryTopDepositMapper summaryTopDepositMapper;
    @Autowired
    private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;
    @Autowired
    private SummaryTopWinnerMapper summaryTopWinnerMapper;
    @Autowired
    private SummaryTopPopularGameMapper summaryTopPopularGameMapper;
    @Autowired
    private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private SummaryGameProviderInfoCalculatorSource summaryGameProviderInfoCalculatorSource;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private SummaryPeriodTypeEnum period = SummaryPeriodTypeEnum.TODAY;
    private SiteCurrencyEnum siteCurrency;
    private Byte rank = 1;
    private String locale;

    @Before
    public void setUp() throws Exception {
        locale = constant.getBackend().getDefaultLocale();
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId); // Today date as of UTC, same as the data inserted in afterMigrate

        calculatorRealTime.run();
    }

    private void assertTopDeposit(SummaryTopDeposit newRecord, SummaryTopDepositCategoryEnum category) {
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new top deposit category, please check!", category, newRecord.getCategory());
    }

    private void assertTopWithdraw(SummaryTopWithdrawal newRecord, SummaryTopWithdrawCategoryEnum category) {
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new top withdraw category, please check!", category,
                newRecord.getCategory());
    }

    private void assertTopPopularGame(SummaryTopPopularGame newRecord, SummaryTopPopularGameCategoryEnum category) {
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
        assertEquals("Failed matching new top withdraw category, please check!", category,
                newRecord.getCategory());
    }

    @Test
    public void testSummaryDaily() {
        siteCurrency = SiteCurrencyEnum.CNY;
        SummaryDaily newRecord = summaryDailyMapper.query(siteCurrency);

        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency, newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
        assertNotNull(newRecord.getTotalPlayerCount());
        assertNotNull(newRecord.getNewPlayerWithDeposit());
        assertNotNull(newRecord.getNewPlayerWithoutDeposit());
        assertNotNull(newRecord.getActivePlayerCountByGametype());
        assertNotNull(newRecord.getActivePlayerCount());
        assertNotNull(newRecord.getTotalDepositAmount());
        assertNotNull(newRecord.getTotalDepositCount());
        assertNotNull(newRecord.getTotalWithdrawalAmount());
        assertNotNull(newRecord.getTotalWithdrawalCount());
        assertNotNull(newRecord.getBonus());
        assertNotNull(newRecord.getCashback());
        assertNotNull(newRecord.getBetAmount());
        assertNotNull(newRecord.getPlayerTotalLoss());
        assertNotNull(newRecord.getPlayerTotalWin());
    }

    @Test
    public void testSummaryTopDeposit() {
        siteCurrency = SiteCurrencyEnum.CNY;
        SummaryTopDeposit newRecord = summaryTopDepositMapper.query(period, siteCurrency,
                SummaryTopDepositCategoryEnum.BY_COUNT)
                .get(0);
        assertTopDeposit(newRecord, SummaryTopDepositCategoryEnum.BY_COUNT);
    }

    @Test
    public void testSummaryTopWithdrawal() {
        siteCurrency = SiteCurrencyEnum.CNY;
        SummaryTopWithdrawal newRecord = summaryTopWithdrawalMapper.query(period, siteCurrency,
                SummaryTopWithdrawCategoryEnum.BY_COUNT).get(0);
        assertTopWithdraw(newRecord, SummaryTopWithdrawCategoryEnum.BY_COUNT);
    }

    @Test
    public void testSummaryTopWinner() {
        siteCurrency = SiteCurrencyEnum.CNY;
        SummaryTopWinner newRecord = summaryTopWinnerMapper.query(period, siteCurrency).get(0);

        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
    }

    @Test
    public void testSummaryTopPopularGame() {
        siteCurrency = SiteCurrencyEnum.CNY;
        SummaryTopPopularGame newRecord = summaryTopPopularGameMapper.query(period, siteCurrency,
                SummaryTopPopularGameCategoryEnum.BY_PLAYER, locale, null).get(0);
        assertTopPopularGame(newRecord, SummaryTopPopularGameCategoryEnum.BY_PLAYER);
    }

    @Test
    public void testSummaryGameProviderInfo() {

        /*
         * query real time data from game_log_hourly_report
         */
        LocalDateTime startTime = LocalDateTime.of(today, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(today, LocalTime.MAX);

        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        locale);
        rank = 1;

        siteCurrency = SiteCurrencyEnum.CNY;
        List<SummaryGameProviderInfo> newRecords = summaryGameProviderInfoMapper
                .query(period, siteCurrency, locale, locale);

        if (!gameLogHourlyReports.isEmpty()) {
            Map<Integer, Map<Integer, BigDecimal>> resultMapByPlayer = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    GameLogHourlyReport::getGameApiId,
                                    Collectors.groupingBy(GameLogHourlyReport::getGameTypeId,
                                            Collectors.reducing(
                                                    BigDecimal.ZERO, GameLogHourlyReport::getBet, BigDecimal::add))));

            List<TopGameProviderByBet> topGameProviderByBetList = new ArrayList<>();

            Iterator<Map.Entry<Integer, Map<Integer, BigDecimal>>> p = resultMapByPlayer.entrySet().iterator();
            while (p.hasNext()) {
                Map.Entry<Integer, Map<Integer, BigDecimal>> pPair = p.next();
                Iterator<Map.Entry<Integer, BigDecimal>> c = (pPair.getValue()).entrySet().iterator();
                while (c.hasNext()) {
                    TopGameProviderByBet topGameProviderByBet = summaryGameProviderInfoCalculatorSource.new TopGameProviderByBet();
                    Map.Entry<Integer, BigDecimal> cPair = c.next();
                    topGameProviderByBet.setGameApiId(pPair.getKey());
                    topGameProviderByBet.setGameTypeId(cPair.getKey());
                    topGameProviderByBet.setBet(cPair.getValue());
                    topGameProviderByBetList.add(topGameProviderByBet);
                }
            }

            topGameProviderByBetList.sort(Comparator.comparing(TopGameProviderByBet::getBet).reversed());

            Iterator<TopGameProviderByBet> iTopGameProviderByBet = topGameProviderByBetList.iterator();

            while (iTopGameProviderByBet.hasNext()) {
                TopGameProviderByBet providerByBet = iTopGameProviderByBet.next();
                BigDecimal bet = gameLogHourlyReports.stream()
                        .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                        .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                        .map(GameLogHourlyReport::getBet)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                BigDecimal win = gameLogHourlyReports.stream()
                        .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                        .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                        .map(GameLogHourlyReport::getWin)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                BigDecimal loss = gameLogHourlyReports.stream()
                        .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                        .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                        .map(GameLogHourlyReport::getLoss)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                SummaryGameProviderInfo newRecord = newRecords.get(rank - 1);

                assertEquals("Failed matching new timezone, please check!", "UTC",
                        newRecord.getTimezone());
                assertEquals("Failed matching new currency, please check!", siteCurrency,
                        newRecord.getCurrency());
                assertEquals("Failed matching new startsDate, please check!",
                        DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
                assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
                assertEquals("Failed matching gameApiId", providerByBet.getGameApiId(), newRecord.getGameApiId());
                assertEquals("Failed matching gameTypeId", providerByBet.getGameTypeId(), newRecord.getGameTypeId());
                assertEquals("Failed matching bet amount", bet, newRecord.getBet());
                assertEquals("Failed matching loss amount", loss, newRecord.getLoss());
                assertEquals("Failed matching win amount", win, newRecord.getWin());
                assertEquals("Failed matching rank", (byte) rank, newRecord.getRank());
                rank++;
            }
        } else {
            SummaryGameProviderInfo newRecord = newRecords.get(0);

            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency,
                    newRecord.getCurrency());
            assertEquals("Failed matching new startsDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(today), newRecord.getStartDate());
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
            assertEquals("Failed matching gameApiId", 0, newRecord.getGameApiId());
            assertEquals("Failed matching gameTypeId", 0, newRecord.getGameTypeId());
            assertEquals("Failed matching bet amount", new BigDecimal("0.000"), newRecord.getBet());
            assertEquals("Failed matching loss amount", new BigDecimal("0.000"), newRecord.getLoss());
            assertEquals("Failed matching win amount", new BigDecimal("0.000"), newRecord.getWin());
            assertEquals("Failed matching rank", (byte) rank, newRecord.getRank());
        }
    }

}
