package com.tripleonetech.sbe.dashboard.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.*;

public class CalculatorDailyTest extends BaseTest {
    @Autowired
    private CalculatorDaily calculatorDaily;
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;
    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;
    @Autowired
    private SummaryTopDepositMapper summaryTopDepositMapper;
    @Autowired
    private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;
    @Autowired
    private SummaryTopWinnerMapper summaryTopWinnerMapper;
    @Autowired
    private SummaryTopPopularGameMapper summaryTopPopularGameMapper;
    @Autowired
    private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;
    @Autowired
    private Constant constant;

    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;

    private LocalDate today;
    private LocalDate dayStart;
    private String locale;

    @Before
    public void setUp() throws Exception {
        locale = constant.getBackend().getDefaultLocale();
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);

        calculatorDaily.run();
    }

    private void setStartDateParam(Integer i) {
        dayStart = today.minus(Period.ofDays(i));
    }

    private void assertTopDeposit(SummaryTopDeposit newRecord, SummaryTopDepositCategoryEnum category) {
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new top deposit category, please check!", category, newRecord.getCategory());
    }

    private void assertTopWithdraw(SummaryTopWithdrawal newRecord, SummaryTopWithdrawCategoryEnum category) {
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new top withdraw category, please check!", category,
                newRecord.getCategory());
    }

    private void assertTopPopularGame(SummaryTopPopularGame newRecord, SummaryTopPopularGameCategoryEnum category) {
        assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        assertEquals("Failed matching new timezone, please check!", "UTC",
                newRecord.getTimezone());
        assertEquals("Failed matching new currency, please check!", siteCurrency,
                newRecord.getCurrency());
        assertEquals("Failed matching new startDate, please check!",
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
        assertEquals("Failed matching new top withdraw category, please check!", category,
                newRecord.getCategory());
    }

    @Test
    public void testSummaryPlayerStatus() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            period = SummaryPeriodTypeEnum.TODAY;
            // for (int i = 1; i < 30; i++) {
            setStartDateParam(1);
            SummaryPlayerStatus newRecord = summaryPlayerStatusMapper.query(period, siteCurrency, 1).get(0);
            assertEquals("Failed matching new period, please check!", period,
                    newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency,
                    newRecord.getCurrency());
            assertNotNull(newRecord.getNewPlayerWithDeposit());
            assertNotNull(newRecord.getNewPlayerWithoutDeposit());
            assertNotNull(newRecord.getTotalPlayerCount());
            // }
        }
    }

    @Test
    public void testSummaryNetProfit() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            period = SummaryPeriodTypeEnum.TODAY;
            // for (int i = 1; i < 30; i++) {
            setStartDateParam(1);
            SummaryNetProfit newRecord = summaryNetProfitMapper.query(period, siteCurrency, 1).get(0);
            assertEquals("Failed matching new period, please check!", period,
                    newRecord.getPeriod());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency,
                    newRecord.getCurrency());
            assertNotNull(newRecord.getBonus());
            assertNotNull(newRecord.getCashback());
            assertNotNull(newRecord.getPlayerWin());
            assertNotNull(newRecord.getPlayerLoss());
            // }
        }
    }

    @Test
    public void testSummaryTopDeposit() {
        setStartDateParam(8);
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            period = SummaryPeriodTypeEnum.LAST_7_DAYS;

            for (SummaryTopDepositCategoryEnum c : SummaryTopDepositCategoryEnum.values()) {
                SummaryTopDeposit newRecord = summaryTopDepositMapper.query(period, siteCurrency, c).get(0);
                assertTopDeposit(newRecord, c);
            }
        }
    }

    @Test
    public void testSummaryTopWithdrawal() {
        setStartDateParam(8);
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            period = SummaryPeriodTypeEnum.LAST_7_DAYS;

            for (SummaryTopWithdrawCategoryEnum c : SummaryTopWithdrawCategoryEnum.values()) {
                SummaryTopWithdrawal newRecord = summaryTopWithdrawalMapper.query(period, siteCurrency, c).get(0);
                assertTopWithdraw(newRecord, c);
            }
        }
    }

    @Test
    public void testSummaryTopWinner() {
        setStartDateParam(8);
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            period = SummaryPeriodTypeEnum.LAST_7_DAYS;

            SummaryTopWinner newRecord = summaryTopWinnerMapper.query(period, siteCurrency).get(0);
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency,
                    newRecord.getCurrency());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        }
    }

    @Test
    public void testSummaryTopPopularGame() {
        setStartDateParam(8);
        period = SummaryPeriodTypeEnum.LAST_7_DAYS;

        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopPopularGameCategoryEnum c : SummaryTopPopularGameCategoryEnum.values()) {
                SummaryTopPopularGame newRecord = summaryTopPopularGameMapper
                        .query(period, siteCurrency, c, locale, null).get(0);
                assertTopPopularGame(newRecord, c);
            }
        }
    }

    @Test
    public void testSummaryGameProviderInfo() {
        setStartDateParam(31);
        period = SummaryPeriodTypeEnum.LAST_30_DAYS;

        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            SummaryGameProviderInfo newRecord = summaryGameProviderInfoMapper
                    .query(period, siteCurrency, locale, locale).get(0);
            assertEquals("Failed matching new timezone, please check!", "UTC",
                    newRecord.getTimezone());
            assertEquals("Failed matching new currency, please check!", siteCurrency,
                    newRecord.getCurrency());
            assertEquals("Failed matching new startDate, please check!",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayStart), newRecord.getStartDate());
            assertEquals("Failed matching new period, please check!", period, newRecord.getPeriod());
        }
    }
}
