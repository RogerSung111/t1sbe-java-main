package com.tripleonetech.sbe.dashboard;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class SummaryNetProfitMapperTest extends BaseTest {
//    private static final Logger logger = LoggerFactory.getLogger(SummaryNetProfitMapperTest.class);

    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;

    @Test
    public void testInsertSummaryNetProfit() {
        ZoneId zoneId = ZoneId.of("UTC");
        // as per the requirement specification doc, Sunday will be the first day of the week
        LocalDate startTime = LocalDate.now(zoneId).minusDays(6 + LocalDate.now(zoneId).plusDays(1).getDayOfWeek().getValue());

        SummaryNetProfit summaryNetProfit = new SummaryNetProfit();
        summaryNetProfit.setPeriod(SummaryPeriodTypeEnum.LAST_WEEK);
        summaryNetProfit.setTimezone("UTC");
        summaryNetProfit.setCurrency(SiteCurrencyEnum.CNY);
        summaryNetProfit.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryNetProfit.setCashback(new BigDecimal(66.660));
        summaryNetProfit.setBonus(new BigDecimal(66.660));
        summaryNetProfit.setPlayerWin(new BigDecimal(66.660));
        summaryNetProfit.setPlayerLoss(new BigDecimal(66.660));

        int count = summaryNetProfitMapper.insert(summaryNetProfit);
        assertEquals("Summary net profit should be updated as a new record", 2, count);

        SummaryNetProfit newRecord = summaryNetProfitMapper
                .query(SummaryPeriodTypeEnum.LAST_WEEK, SiteCurrencyEnum.CNY, 1).get(0);
        BigDecimal expectedValue = new BigDecimal("66.660");
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New cashback amount should match ", expectedValue, newRecord.getCashback());
        assertEquals("New bonus amount should match ", expectedValue, newRecord.getBonus());
        assertEquals("New player loss should match ", expectedValue, newRecord.getPlayerLoss());
    }
}
