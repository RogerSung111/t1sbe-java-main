package com.tripleonetech.sbe.dashboard;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class SummaryGameProviderInfoMapperTest extends BaseTest {

    @Autowired
    private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;
    @Autowired
    private Constant constant;

    private String locale;

    @Before
    public void setUp() throws Exception {
        locale = constant.getBackend().getDefaultLocale();
    }

    @Test
    public void testInsertSummaryGameProviderInfo() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate startTime = LocalDate.now(zoneId);

        SummaryGameProviderInfo summaryGameProviderInfo = new SummaryGameProviderInfo();
        summaryGameProviderInfo.setPeriod(SummaryPeriodTypeEnum.TODAY);
        summaryGameProviderInfo.setTimezone("UTC");
        summaryGameProviderInfo.setCurrency(SiteCurrencyEnum.CNY);
        summaryGameProviderInfo.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryGameProviderInfo.setRank((byte) 1);
        summaryGameProviderInfo.setGameApiId(3); // BBIN THB
        summaryGameProviderInfo.setGameTypeId(3); // Casino
        summaryGameProviderInfo.setBet(new BigDecimal("666.666"));
        summaryGameProviderInfo.setWin(new BigDecimal("666.666"));
        summaryGameProviderInfo.setLoss(new BigDecimal("666.666"));

        int count = summaryGameProviderInfoMapper.insert(summaryGameProviderInfo);
        assertEquals("Summary bet gross profit status should be updated as a new record", 2, count);

        SummaryGameProviderInfo newRecord = summaryGameProviderInfoMapper.query(SummaryPeriodTypeEnum.TODAY,
                SiteCurrencyEnum.CNY, locale, locale).get(0);
        assertEquals("Default timezone does not match, please check!", "UTC", newRecord.getTimezone());
        assertEquals("New game API ID should match 3", 3, (int) newRecord.getGameApiId());
        assertEquals("New Game API Name should match Other", "BBIN THB", newRecord.getGameApiName());
        assertEquals("New game API ID should match 3", 3, (int) newRecord.getGameTypeId());
        assertEquals("New Game Type Name should match Other", "Casino", newRecord.getGameTypeName());
        assertEquals("New player bet should match 666.666", new BigDecimal("666.666"), newRecord.getBet());
        assertEquals("New player win should match 666.666", new BigDecimal("666.666"), newRecord.getWin());
        assertEquals("New player loss should match 666.666", new BigDecimal("666.666"), newRecord.getLoss());
    }
}
