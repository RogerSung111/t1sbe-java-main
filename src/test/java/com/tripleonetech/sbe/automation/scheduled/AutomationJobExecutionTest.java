package com.tripleonetech.sbe.automation.scheduled;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.automation.*;

public class AutomationJobExecutionTest extends BaseTest {

    @Autowired
    private AutomationJobMapper automationJobMapper;
    @Autowired
    private AutomationRecordMapper automationRecordMapper;
    @Autowired
    private AutomationJobExecution automationJobExecution;
    @Autowired
    private AutomationService automationService;

    private Consumer<AutomationJob> autoExecuteFunction = job -> automationJobExecution.executeAutomatically(job);
    private Consumer<AutomationJob> manualExecuteFunction = job -> automationJobExecution.executeManually(job);

    @Test
    public void testExecuteAutomatically() {
        AutomationJob job = automationJobMapper.get(1);
        validateSuccessExecution(job, autoExecuteFunction);
    }

    @Test
    public void testExecuteAutomatically_IsBeforeStartAt_NoExecuting() {
        AutomationJob job = automationJobMapper.get(1);
        job.setStartAt(LocalDateTime.now().plusHours(1));
        validateNoExecuting(job, autoExecuteFunction);
    }

    @Test
    public void testExecuteAutomatically_RunFrequencyIsNever_NoExecuting() {
        AutomationJob job = automationJobMapper.get(4);
        automationJobExecution.executeAutomatically(job);
        validateNoExecuting(job, autoExecuteFunction);
    }

    @Test
    public void testExecuteManually_RunFrequencyIsNever_Executing() {
        AutomationJob job = automationJobMapper.get(4);
        automationJobExecution.executeAutomatically(job);
        validateSuccessExecution(job, manualExecuteFunction);
    }

    private void validateNoExecuting(AutomationJob job, Consumer<AutomationJob> action) {
        AutomationRecordQueryForm form = new AutomationRecordQueryForm();
        form.setJobId(job.getId());
        AutomationRecord lastRecord = automationRecordMapper.list(form).stream().findFirst()
                .orElse(new AutomationRecord());

        action.accept(job);

        // job will not be executed
        AutomationJob updatedJob = automationJobMapper.get(job.getId());
        AutomationRecord updatedLastRecord = automationRecordMapper.list(form).stream().findFirst()
                .orElse(new AutomationRecord());
        assertEquals(job.getRunCount(), updatedJob.getRunCount());
        assertEquals(lastRecord.getId(), updatedLastRecord.getId());
    }

    private void validateSuccessExecution(AutomationJob job, Consumer<AutomationJob> action) {
        LocalDateTime preExecuteTime = LocalDateTime.now();
        action.accept(job);

        AutomationRecordQueryForm form = new AutomationRecordQueryForm();
        form.setJobId(job.getId());
        AutomationRecord record = automationRecordMapper.list(form).get(0);
        assertFalse(record.getCompletedAt().truncatedTo(ChronoUnit.SECONDS)
                .isBefore(preExecuteTime.atZone(ZoneId.systemDefault()).truncatedTo(ChronoUnit.SECONDS)));
        assertTrue(record.getRunCount().compareTo(job.getRunCount() + 1) == 0);
        assertEquals(AutomationRecordStatusEnum.COMPLETED.getCode(), record.getStatus().intValue());

        List<Integer> playerIds = automationService.getPlayerIdsByFilter(job.getFilters());
        assertEquals(playerIds.stream().map(String::valueOf).collect(Collectors.joining(",")),
                record.getAffectedPlayerIds());
        assertEquals(playerIds.size(), Optional.ofNullable(record.getAffectedPlayerCount()).orElse(Integer.valueOf(0)).intValue());
    }

}
