package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.DateUtils;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AutomationRecordMapperTest extends BaseTest {
    @Autowired
    private AutomationRecordMapper mapper;

    private AutomationRecord sampleRecord;

    @Before
    public void setup() {
        sampleRecord = new AutomationRecord();
        sampleRecord.setJobId(1);
        sampleRecord.setJobName("AutoJob1");
        sampleRecord.setRunCount(4);
        sampleRecord.setStatus((byte)AutomationRecordStatusEnum.IN_PROGRESS.getCode());
        sampleRecord.setAutomated(true);
    }

    @Test
    public void testGetLastAutomatedRunTime() {
        Integer jobId = 1;
        LocalDateTime lastAutomatedRunTime = mapper.getLastAutomatedRunTime(jobId);

        AutomationRecordQueryForm form = new AutomationRecordQueryForm();
        form.setJobId(jobId);
        AutomationRecord lastRecord = mapper.list(form).stream().findFirst().orElse(new AutomationRecord());
        ZonedDateTime lastRecordRunTime = lastRecord.getCompletedAt() != null ? lastRecord.getCompletedAt()
                : lastRecord.getCreatedAt();

        assertTrue(lastAutomatedRunTime.isEqual(DateUtils.zonedToLocal(lastRecordRunTime)));
    }

    @Test
    public void testList() {
        AutomationRecordQueryForm query = new AutomationRecordQueryForm();
        query.setStatus((byte)AutomationRecordStatusEnum.COMPLETED.getCode());
        query.setSort("createdAt DESC");

        List<AutomationRecord> records = mapper.list(query);
        assertTrue(records.size() > 1);
        assertEquals(query.getStatus(), records.get(0).getStatus());
        assertNotNull(records.get(0).getCompletedAt());
    }

    @Test
    public void testInsert() {
        assertTrue(mapper.insert(sampleRecord) > 0);
        assertNotNull(sampleRecord.getId());

        AutomationRecord insertedRecord = mapper.get(sampleRecord.getId());
        assertNotNull(insertedRecord);
        assertTrue(insertedRecord.isAutomated());
    }

    @Test
    public void testUpdate() {
      sampleRecord.setId(1L);
      assertTrue(mapper.update(sampleRecord) > 0);

      AutomationRecord record = mapper.get(sampleRecord.getId());
      assertNotNull(record);
      assertEquals(sampleRecord.getJobId(), record.getJobId());
      assertEquals(sampleRecord.getJobName(), record.getJobName());
      assertEquals(sampleRecord.getRunCount(), record.getRunCount());
      assertEquals(sampleRecord.getStatus().byteValue(), record.getStatus().byteValue());
    }

}
