package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.automation.AutomationAction.AutomationActionEnum;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerStatsMapper;
import com.tripleonetech.sbe.player.PlayerStatusEnum;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class AutomationServiceTest extends BaseTest {
    @Autowired
    private AutomationService automationService;
    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerStatsMapper playerStatsMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    // tags all contains
    @Test
    public void testGetPlayerIdsByFilter_Tag() {
        PlayerFilter filter = new PlayerFilter();
        filter.setId(PlayerFilter.PlayerFilterEnum.TAG.getCode());
        filter.setOperator(PlayerFilter.PlayerFilterOperatorEnum.ALL.getOperator());
        filter.setValue(Arrays.asList("6", "8"));
        List<PlayerFilter> filters = Arrays.asList(filter);
        List<Integer> playerIds = automationService.getPlayerIdsByFilter(Arrays.asList(filters));

        assertTrue(playerIds.containsAll(Arrays.asList(1, 2)));
    }

    // DEPOSIT > 7000
    @Test
    public void testGetPlayerIdsByFilter_Deposit() {
        PlayerFilter filter = new PlayerFilter();
        filter.setId(PlayerFilter.PlayerFilterEnum.DEPOSIT.getCode());
        filter.setOperator(PlayerFilter.PlayerFilterOperatorEnum.MORE_THAN.getOperator());
        filter.setValue(Arrays.asList("CNY", "7000"));
        List<PlayerFilter> filters = Arrays.asList(filter);
        List<Integer> playerIds = automationService.getPlayerIdsByFilter(Arrays.asList(filters));

        List<Integer> expected = playerStatsMapper.getPlayerIdByFilterDeposit(SiteCurrencyEnum.valueOf("CNY"), ">", "7000");
        assertEquals(expected.size(), playerIds.size());
        assertTrue(expected.containsAll(playerIds));
    }

    // Bet < 1500
    @Test
    public void testGetPlayerIdsByFilter_Bet() {
        PlayerFilter filter = new PlayerFilter();
        filter.setId(PlayerFilter.PlayerFilterEnum.BET.getCode());
        filter.setOperator(PlayerFilter.PlayerFilterOperatorEnum.LESS_THAN.getOperator());
        filter.setValue(Arrays.asList("CNY", "1500.00"));
        List<PlayerFilter> filters = Arrays.asList(filter);
        List<Integer> playerIds = automationService.getPlayerIdsByFilter(Arrays.asList(filters));

        List<Integer> expected = gameLogHourlyReportMapper.getPlayerIdByFilter(SiteCurrencyEnum.valueOf("CNY"), "<", "1500.00");
        assertEquals(expected.size(), playerIds.size());
        assertTrue(expected.containsAll(playerIds));
    }

    // registerDate < 7 hours ago
    @Test
    public void testGetPlayerIdsByFilter_RegisterDate() {
        PlayerFilter filter = new PlayerFilter();
        filter.setId(PlayerFilter.PlayerFilterEnum.REGISTER_DATE.getCode());
        filter.setOperator(PlayerFilter.PlayerFilterOperatorEnum.LESS_THAN.getOperator());
        filter.setValue(Arrays.asList(ZonedDateTime.now().plusHours(-7).format(DateTimeFormatter.ISO_ZONED_DATE_TIME)));
        List<PlayerFilter> filters = Arrays.asList(filter);
        List<Integer> playerIds = automationService.getPlayerIdsByFilter(Arrays.asList(filters));

        assertTrue(playerIds.containsAll(Arrays.asList(9, 48, 51)));
    }

    @Test
    public void testApplyActions_AssignNonGroupTag() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.ASSIGN_TAG.getCode());
        action.setValue(Arrays.asList("6", "8"));
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        action.getValue().forEach(tagStr -> {
            List<PlayersPlayerTag> tags = playersPlayerTagMapper.listByPlayerTagId(Integer.valueOf(tagStr));
            List<Integer> tagPlayerIds = tags.stream().map(tag -> tag.getPlayerId()).collect(Collectors.toList());
            playerIds.forEach(playerId -> assertTrue(tagPlayerIds.contains(playerId)));
        });
    }

    @Test
    public void testApplyActions_AssignGroupTag() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.ASSIGN_GROUP.getCode());
        action.setValue(Arrays.asList("2", "3"));
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        List<PlayersPlayerTag> tags = playersPlayerTagMapper.listGroupByPlayerIds(playerIds);
        tags.forEach(tag -> assertEquals(Integer.valueOf(action.getValue().get(0)), tag.getPlayerTagId()));
    }

    @Test
    public void testApplyActions_AssignGroupTag_ButValueIsNotGroupTag() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.ASSIGN_GROUP.getCode());
        action.setValue(Arrays.asList("6"));
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2);
        automationService.applyActions(actions, playerIds);

        List<PlayersPlayerTag> tags = playersPlayerTagMapper.listGroupByPlayerIds(playerIds);
        tags.forEach(tag -> assertNotEquals(Integer.valueOf(action.getValue().get(0)), tag.getPlayerTagId()));
    }

    @Test
    public void testApplyActions_DeleteTag() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.DELETE_TAG.getCode());
        action.setValue(Arrays.asList("2", "3", "6", "8"));
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        action.getValue().forEach(tagStr -> {
            List<PlayersPlayerTag> tags = playersPlayerTagMapper.listByPlayerTagId(Integer.valueOf(tagStr));
            tags.forEach(tag -> {
                assertFalse(playerIds.contains(tag.getPlayerId()));
            });
        });
    }

    @Test
    public void testApplyActions_DisablePlayer() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.DISABLE_PLAYER.getCode());
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        players.forEach(player -> assertEquals(PlayerStatusEnum.BLOCKED, player.getStatus()));
    }

    @Test
    public void testApplyActions_DisableCashback() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.DISABLE_CASHBACK.getCode());
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        players.forEach(player -> assertEquals(false, player.getCashbackEnabled()));
    }

    @Test
    public void testApplyActions_DisableCampaign() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.DISABLE_CAMPAIGN.getCode());
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        players.forEach(player -> assertEquals(false, player.getCampaignEnabled()));
    }

    @Test
    public void testApplyActions_DisableWithdraw() {
        AutomationAction action = new AutomationAction();
        action.setId(AutomationActionEnum.DISABLE_WITHDRAW.getCode());
        List<AutomationAction> actions = Arrays.asList(action);
        List<Integer> playerIds = Arrays.asList(1, 2, 12, 13);
        automationService.applyActions(actions, playerIds);

        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        players.forEach(player -> assertEquals(false, player.getWithdrawEnabled()));
    }
}
