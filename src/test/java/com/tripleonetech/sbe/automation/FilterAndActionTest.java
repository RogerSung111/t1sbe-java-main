package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.JsonUtils;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FilterAndActionTest {
    private String filterJson = "[[{\"id\":1,\"value\":[7,8]},{\"id\":2,\"value\":[5999],\"operator\":\">\"},{\"id\":3,\"value\":[100],\"operator\":\"<=\"}],[{\"id\":1,\"value\":[38],\"operator\":\"all\"}]]";
    private String actionJson = "[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}, {\"id\": 2, \"value\": [1, 0]}, {\"id\": 2, \"value\": [3]}]";

    @Test
    public void testParseFilterJson() {
        List<List<PlayerFilter>> filter = JsonUtils.jsonTo2DObjectList(filterJson, PlayerFilter.class);
        assertNotNull(filter);
        List<PlayerFilter> firstGroup = filter.get(0);
        assertNotNull(firstGroup);
        PlayerFilter firstFilter = firstGroup.get(0);
        assertEquals("8", firstFilter.getValue().get(1));
    }

    @Test
    public void testParseActionJson() {
        List<AutomationAction> actions = JsonUtils.jsonToObjectList(actionJson, AutomationAction.class);
        assertNotNull(actions);
        AutomationAction firstAction = actions.get(0);
        assertEquals("43", firstAction.getValue().get(0));
    }
}
