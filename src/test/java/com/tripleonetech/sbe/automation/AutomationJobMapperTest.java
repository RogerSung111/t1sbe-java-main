package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.model.NoPageQueryForm;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AutomationJobMapperTest extends BaseTest {
    @Autowired
    private AutomationJobMapper mapper;

    private AutomationJob sampleJob;

    @Before
    public void setup() {
        sampleJob = new AutomationJob();
        sampleJob.setName("A sample automation job");
        sampleJob.setMaxRun(3);
        sampleJob.setFiltersJson("[[{\"id\": 2, \"value\": [5999], \"operator\": \">\"}, {\"id\": 3, \"value\": [100], \"operator\": \"<=\"}]]");
        sampleJob.setActionsJson("[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}]");
        sampleJob.setStartAt(LocalDateTime.now());
        sampleJob.setRunFrequency((byte)AutomationJobRunFrequencyEnum.EVERY_DAY.getCode());
    }

    @Test
    public void testList() {
        NoPageQueryForm query = new NoPageQueryForm();
        List<AutomationJob> jobs = mapper.list(query);
        assertNotNull(jobs);
        assertTrue(jobs.size() > 0);

        AutomationJob job = jobs.get(jobs.size() - 1);
        assertNotNull(job.getFilters());
        assertTrue(job.getFilters().size() > 0);
        assertNotNull(job.getActions());
        assertTrue(job.getActions().size() > 0);
        assertTrue(job.getRunCount() > 0);
    }

    @Test
    public void testListExecutableJob() {
        List<AutomationJob> jobs = mapper.listExecutableJob();
        jobs.forEach(job -> {
            assertFalse(job.isDeleted());
            assertTrue(job.getFiltersJson() != null);
            assertTrue(job.getActionsJson() != null);

            if (!job.getMaxRun().equals(0)) {
                assertTrue(job.getMaxRun().compareTo(job.getRunCount()) > 0);
            }
        });
    }

    @Test
    public void testInsert() {
        AutomationJobForm form = new AutomationJobForm();
        BeanUtils.copyProperties(sampleJob, form);
        mapper.insert(form);

        AutomationJob insertJob = mapper.get(form.getId());
        assertNotNull(insertJob);
        assertEquals(form.getName(), insertJob.getName());
        assertEquals(form.getFilters().size(), insertJob.getFilters().size());
        assertEquals(form.getActions().size(), insertJob.getActions().size());
    }

    @Test
    public void testUpdate() {
        AutomationJobForm form = new AutomationJobForm();
        BeanUtils.copyProperties(sampleJob, form);
        form.setId(1); // to update the job whose id = 1
        mapper.update(form);

        AutomationJob updateJob = mapper.get(form.getId());
        assertNotNull(updateJob);
        assertEquals(form.getName(), updateJob.getName());
        assertEquals(form.getFilters().size(), updateJob.getFilters().size());
        assertEquals(form.getActions().size(), updateJob.getActions().size());
    }

    @Test
    public void testDelete() {
        assertTrue(mapper.delete(1) > 0);
        assertTrue(mapper.get(1).isDeleted());
    }
}
