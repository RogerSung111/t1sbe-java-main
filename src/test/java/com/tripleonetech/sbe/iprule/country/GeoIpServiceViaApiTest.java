package com.tripleonetech.sbe.iprule.country;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

@Ignore
public class GeoIpServiceViaApiTest extends BaseTest {

    @Autowired
    GeoIpServiceViaApi geoIpService;
    
    @Test
    public void testGetCountryCode() {
        String countryCode = geoIpService.getCountryCode("90.135.118.23");
        assertEquals("LT", countryCode);
    }

}
