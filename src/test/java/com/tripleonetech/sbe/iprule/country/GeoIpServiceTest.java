package com.tripleonetech.sbe.iprule.country;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class GeoIpServiceTest extends BaseTest {

    @Autowired
    GeoIpService geoIpService;
    
    @Test
    public void testServiceInstanceType() {

        assertEquals(GeoIpServiceViaApi.class, geoIpService.getClass());
    }

}
