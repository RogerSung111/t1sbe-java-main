package com.tripleonetech.sbe.iprule.country;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IpRuleCountryMapperTest extends BaseTest {

    @Autowired
    private IpRuleCountryMapper mapper;

    @Test
    public void testGetEnabled() {
        Boolean enabled = mapper.getEnabled("LT");
        assertFalse(enabled);
    }

    @Test
    public void testList() {
        List<IpRuleCountryView> list = mapper.list(false,"zh-CN", "en-US");
        assertEquals(241, list.size());
        assertEquals("AD", list.get(0).getCountryCode());
        assertEquals("安道尔", list.get(0).getName());
    }

    @Test
    public void testListEnabled() {
        List<IpRuleCountryView> list = mapper.list(true,"zh-CN", "en-US");
        assertEquals(6, list.size());
    }

    /**
     * While input locale is not exists, then return en-US
     */
    @Test
    public void testListDefaultLocale() {
        List<IpRuleCountryView> list = mapper.list(false,"zh-TW", "en-US");
        assertEquals(241, list.size());
        assertEquals("AE", list.get(1).getCountryCode());
        assertEquals("United Arab Emirates", list.get(1).getName());
    }

    @Test
    public void testUpdateEnabled() {
        mapper.setEnabled(Arrays.asList("AD"));
        Boolean enabled = mapper.getEnabled("AD");
        assertTrue(enabled);
    }
}
