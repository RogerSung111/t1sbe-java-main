package com.tripleonetech.sbe.iprule.country;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;

public class GeoIpServiceViaApiWithMockTest extends BaseTest {

    @Autowired
    GeoIpServiceViaApi geoIpService;
    
    @Before
    public void setup() {

        geoIpService.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockAccessAuthentication }));
        // this method will only call http service when token cache is empty
        // isolate the call of fetch token here to avoid interfering with tests
        geoIpService.fetchAuthToken();
    }

    @Test
    public void testGetCountryCode() {
        geoIpService.setHttpService(MockApiHttpServiceFactory.get(
                new String[] { mockLocationPosition }));
        String countryCode = geoIpService.getCountryCode("90.135.118.23");
        assertEquals("CN", countryCode);
    }

    @BeforeClass
    public static void initMocks() {

        mockLocationPosition = "{\n" +
                "    \"country_code\": \"CN\",\n" +
                "    \"region_name\": \"Beijing\",\n" +
                "    \"city_name\": \"Beijing\"\n" +
                "}";
        mockAccessAuthentication = "{\n" +
                "    \"token\": \"e7516c26d28642d5a5449cbc7e991d1a\",\n" +
                "    \"ttl\": 3775\n" +
                "}";

    }

    private static String mockLocationPosition;
    private static String mockAccessAuthentication;
}
