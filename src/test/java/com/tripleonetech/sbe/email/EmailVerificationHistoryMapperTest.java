package com.tripleonetech.sbe.email;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class EmailVerificationHistoryMapperTest extends BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(EmailVerificationHistoryMapperTest.class);

    @Autowired
    private EmailVerificationHistoryMapper emailVerificationHistoryMapper;

    private final static String MOCK_OTP_CODE = "232323";
    private final static String MOCK_EMAIL = "tripleonetech@heaven.con.sg";
    private final static int MOCK_PLAYER_ID = 1; // username: test001
    private final static String MOCK_PLAYER_USERNAME = "test001";

    private final static EmailVerificationHistory mockEmailVerificationHistory = new EmailVerificationHistory();

    private void insert() {

        mockEmailVerificationHistory.setOtpCode(MOCK_OTP_CODE);
        mockEmailVerificationHistory.setEmail(MOCK_EMAIL);
        mockEmailVerificationHistory.setPlayerId(MOCK_PLAYER_ID);
        mockEmailVerificationHistory.setVerified(false);
        mockEmailVerificationHistory.setExpirationTime(LocalDateTime.now().plusMinutes(10));
        emailVerificationHistoryMapper.insert(mockEmailVerificationHistory);
    }

    @Test
    public void testInsertAndGetByPlayerIdAndEmail() {
        // test insert
        insert();

        // test getByEmailAndCode
        EmailVerificationHistory newEmailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(MOCK_PLAYER_ID, mockEmailVerificationHistory.getEmail());
        assertNotNull(newEmailVerificationHistory);
        assertTrue(!newEmailVerificationHistory.getVerified());
        assertEquals(mockEmailVerificationHistory.getOtpCode(), newEmailVerificationHistory.getOtpCode());
        assertEquals(mockEmailVerificationHistory.getExpirationTime().truncatedTo(ChronoUnit.SECONDS), newEmailVerificationHistory.getExpirationTime().truncatedTo(ChronoUnit.SECONDS));
    }

    @Test
    public void testUpdateRecordToVerified() {
        insert();
        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(MOCK_PLAYER_ID, MOCK_EMAIL);
        emailVerificationHistoryMapper.updateRecordToVerified(emailVerificationHistory.getId());
        EmailVerificationHistory updatedEmailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(MOCK_PLAYER_ID, MOCK_EMAIL);

        assertNotNull(updatedEmailVerificationHistory);
        assertTrue(updatedEmailVerificationHistory.getVerified());
        assertEquals(MOCK_OTP_CODE, updatedEmailVerificationHistory.getOtpCode());
    }

    @Test
    public void testListEmailHistory() {
        // Insert a mock data of player test001
        insert();

        // query form
        EmailVerificationHistoryQueryForm form = new EmailVerificationHistoryQueryForm();
        form.setCreatedAtStart(ZonedDateTime.now().minusMinutes(5));
        form.setCreatedAtEnd(ZonedDateTime.now().plusMinutes(5));
        form.setUsername(MOCK_PLAYER_USERNAME);

        List<EmailVerificationHistoryView> result = emailVerificationHistoryMapper.listEmailHistory(form,
                form.getPage(), form.getLimit());

        // assert
        assertTrue(result.size() > 0);
        assertEquals(MOCK_PLAYER_USERNAME, result.get(0).getUsername());
        assertNotNull(result.get(0).getEmail());
        assertNotNull(result.get(0).getExpirationTime());
    }

    @Test
    public void testListEmailHistory_OnlyRequiredParams() {
        EmailVerificationHistoryQueryForm form = new EmailVerificationHistoryQueryForm();

        insert();
        List<EmailVerificationHistoryView> result = emailVerificationHistoryMapper.listEmailHistory(form,
                form.getPage(), form.getLimit());
        assertTrue(result.size() > 0);
    }
}
