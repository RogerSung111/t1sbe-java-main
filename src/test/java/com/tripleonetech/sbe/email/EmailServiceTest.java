package com.tripleonetech.sbe.email;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;

public class EmailServiceTest extends BaseTest {
    @Autowired
    private EmailService emailService;
    @Autowired
    private EmailVerificationHistoryMapper emailVerificationHistoryMapper;
    @Autowired
    private EmailHelperT1Notification emailHelperT1Notification;
    @Autowired
    private T1NotificationApi notificationApi;

    private static final int TEST_PLAYER_ID = 1;
    private static final String TEST_EMAIL_ADDRESS = "tripleonetechsg@gmail.com";

    @Before
    public void init() {
        emailService.setEmailHelper(emailHelperT1Notification);
    }

    @Test
    public void testSendVerificationEmail() throws Exception {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));

        String otp = emailService.sendVerificationEmail(TEST_EMAIL_ADDRESS, TEST_PLAYER_ID);
        assertTrue(StringUtils.isNotBlank(otp));

        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(TEST_PLAYER_ID, TEST_EMAIL_ADDRESS);
        assertEquals(otp, emailVerificationHistory.getOtpCode());
        assertEquals(TEST_PLAYER_ID, emailVerificationHistory.getPlayerId().intValue());
        assertFalse(emailVerificationHistory.getVerified());
        assertTrue(emailVerificationHistory.getExpirationTime().isAfter(LocalDateTime.now()));
    }

    @Test
    public void testSendVerificationEmail_Failed() throws Exception {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockFailedResponse }));

        String otp = emailService.sendVerificationEmail(TEST_EMAIL_ADDRESS, TEST_PLAYER_ID);
        assertFalse(StringUtils.isNotBlank(otp));
    }

    @Test
    public void testValidateEmailToken() throws Exception {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String otp = emailService.sendVerificationEmail(TEST_EMAIL_ADDRESS, TEST_PLAYER_ID);

        EmailVerificationResultEnum resultEnum = emailService.validateEmailToken(TEST_PLAYER_ID, otp, TEST_EMAIL_ADDRESS);
        assertEquals(EmailVerificationResultEnum.PASS, resultEnum);

        EmailVerificationHistory result = emailVerificationHistoryMapper.getByPlayerIdAndEmail(TEST_PLAYER_ID, TEST_EMAIL_ADDRESS);
        assertEquals(otp, result.getOtpCode());
        assertTrue(result.getVerified());
    }

    @Test
    public void testValidateEmailToken_NotEquals_Failed() throws Exception {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        emailService.sendVerificationEmail(TEST_EMAIL_ADDRESS, TEST_PLAYER_ID);

        EmailVerificationResultEnum resultEnum = emailService.validateEmailToken(TEST_PLAYER_ID, "123456", TEST_EMAIL_ADDRESS);
        assertEquals(EmailVerificationResultEnum.FAIL_WRONG_OTP, resultEnum);
    }

    @Test
    public void testValidateEmailToken_AlreadyVerified_Failed() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String otpCode = emailService.sendVerificationEmail(TEST_EMAIL_ADDRESS, TEST_PLAYER_ID);
        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper.getByPlayerIdAndEmail(TEST_PLAYER_ID, TEST_EMAIL_ADDRESS);
        emailVerificationHistoryMapper.updateRecordToVerified(emailVerificationHistory.getId());

        EmailVerificationResultEnum resultEnum = emailService.validateEmailToken(TEST_PLAYER_ID, otpCode, TEST_EMAIL_ADDRESS);
        assertEquals(EmailVerificationResultEnum.FAIL_OTP_USED, resultEnum);
    }

    @Test
    public void testValidateEmailToken_Expired_Failed() {
        EmailVerificationResultEnum resultEnum = emailService.validateEmailToken(TEST_PLAYER_ID, "123321", "test@tripleonetech.net");
        assertEquals(EmailVerificationResultEnum.FAIL_OTP_EXPIRED, resultEnum);
    }

    private static String mockSuccessResponse;
    private static String mockFailedResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
        mockFailedResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":90}}";
    }
}
