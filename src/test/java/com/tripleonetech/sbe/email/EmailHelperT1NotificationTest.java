package com.tripleonetech.sbe.email;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;

public class EmailHelperT1NotificationTest extends BaseTest {
    @Autowired
    private EmailHelperT1Notification emailHelper;
    @Autowired
    private T1NotificationApi notificationApi;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private String expectedSender = "java@tripleonetech.net";
    private String recipient = "java@tripleonetech.net";
    private String subject = "notification subject for unit test";
    private String content = "Hi, this is notification content for unit test.";

    @Test
    public void testSendMail() throws Exception {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        MailModel mailModel = new MailModel();
        mailModel.setSubject(subject);
        mailModel.setContent(content);
        mailModel.addRecipients(recipient);
        emailHelper.sendMail(mailModel);

        assertEquals(expectedSender, mailModel.getFrom());
    }

    @Test
    public void testSendMail_Failed() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Failed to send mail.");

        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockFailedResponse }));
        MailModel mailModel = new MailModel();
        mailModel.setSubject(subject);
        mailModel.setContent(content);
        mailModel.addRecipients(recipient);
        emailHelper.sendMail(mailModel);
    }

    private static String mockSuccessResponse;
    private static String mockFailedResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
        mockFailedResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":90}}";
    }

}
