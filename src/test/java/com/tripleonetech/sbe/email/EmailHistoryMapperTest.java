package com.tripleonetech.sbe.email;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EmailHistoryMapperTest extends BaseTest {
    @Autowired
    private EmailHistoryMapper emailHistoryMapper;

    private static final Integer TEST_PLAYER_ID = 2;
    private static String TEST_SENDER_ADDRESS = "testSender@tripleonetech.com";
    private static String TEST_RECIPIENT_ADDRESS = "testRecipient@tripleonetech.com";
    private static String TEST_EMAIL_SUBJECT = "test email subject";
    private static String TEST_EMAIL_CONTENT = "test email content";

    @Test
    public void testInsert() {
        EmailHistory emailHistory = EmailHistory.create(TEST_SENDER_ADDRESS, TEST_RECIPIENT_ADDRESS, TEST_EMAIL_SUBJECT,
                TEST_EMAIL_CONTENT, TEST_PLAYER_ID);
        int success = emailHistoryMapper.insert(emailHistory);

        assertTrue(success == 1);

        EmailHistoryQueryForm queryForm = new EmailHistoryQueryForm();
        queryForm.setSenderAddress(TEST_SENDER_ADDRESS);
        queryForm.setSubject(TEST_EMAIL_SUBJECT);
        List<EmailHistory> result = emailHistoryMapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        assertEquals(TEST_EMAIL_SUBJECT, result.get(0).getSubject());
        assertEquals(TEST_EMAIL_CONTENT, result.get(0).getContent());

    }

    @Test
    public void testQuery() {
        EmailHistoryQueryForm form = new EmailHistoryQueryForm();
        form.setContent("unit test");
        List<EmailHistory> emailHistories = emailHistoryMapper.query(form, form.getPage(), form.getLimit());
        assertTrue(emailHistories.size() > 0);
        assertTrue(emailHistories.get(0).getContent().contains("unit test"));
    }

    @Test
    public void testQueryResultSameWithNameLike() {
        EmailHistoryQueryForm form = new EmailHistoryQueryForm();
        form.setUsername("test002");
        List<EmailHistory> expected = emailHistoryMapper.query(form, form.getPage(), form.getLimit());

        form.setUsername("002");
        List<EmailHistory> emailHistories = emailHistoryMapper.query(form, form.getPage(), form.getLimit());

        assertEquals(expected.size(), emailHistories.size());
    }

    @Test
    public void testQuery_OnlyRequiredParams() {
        EmailHistoryQueryForm form = new EmailHistoryQueryForm();
        List<EmailHistory> emailHistories = emailHistoryMapper.query(form, form.getPage(), form.getLimit());
        assertTrue(emailHistories.size() > 0);
    }
}
