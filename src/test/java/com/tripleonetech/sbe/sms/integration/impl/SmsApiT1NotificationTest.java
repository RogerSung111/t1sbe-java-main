package com.tripleonetech.sbe.sms.integration.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.integration.SmsApiFactory;

public class SmsApiT1NotificationTest extends BaseApiTest {
    private SmsApiT1Notification smsApi;

    @Autowired
    private SmsApiFactory apiFactory;
    @Autowired
    private T1NotificationApi notificationApi;

    @Before
    public void setup() {
        smsApi = (SmsApiT1Notification) apiFactory.getById(3);
    }

    @Test
    public void testSendMessage() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("Hi, this is notification content for unit test.");
        smsMessage.setReceiverNumber("886-917123456");
        ApiResult apiResult = smsApi.sendMessage(smsMessage);

        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
    }

    @Test
    public void testSendMessage_Failed() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockFailedResponse }));
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("Hi, this is notification content for unit test.");
        smsMessage.setReceiverNumber("886-917272945");
        ApiResult apiResult = smsApi.sendMessage(smsMessage);

        assertEquals(ApiResponseEnum.ACTION_UNSUCCESSFUL, apiResult.getResponseEnum());
    }

    @Test(expected = RestResponseException.class)
    public void testSendMessage_InvalidPhone_Failed() throws RestResponseException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setReceiverNumber("886-091712345");
        smsApi.sendMessage(smsMessage);
    }

    @Test
    public void testGetBalance() {
        ApiResult apiResult = smsApi.getBalance();
        assertEquals(ApiResponseEnum.NOOP, apiResult.getResponseEnum());
        assertEquals("Not in service", apiResult.getMessage());
    }

    private static String mockSuccessResponse;
    private static String mockFailedResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
        mockFailedResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":90}}";
    }
}
