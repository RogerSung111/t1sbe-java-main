package com.tripleonetech.sbe.sms.integration.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.integration.SmsApiFactory;

public class SmsApiIhuyiTest extends BaseTest {
    
    
    @Autowired
    private SmsApiFactory apiFactory;
    
    private final static int IHUYI_SMS_API_ID = 2;
    private SmsApiIhuyi api;
    
    @Before
    public void setup() {
        api = (SmsApiIhuyi) apiFactory.getById(IHUYI_SMS_API_ID);
    }
    
    @Test
    public void testSend() {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("您的验证码是：503868。请不要把验证码泄露给其他人。");
        smsMessage.setReceiverNumber("16559181054");
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { MOCK_SMS_SUCCESS_RESPONSE }));
        ApiResult result = api.sendMessage(smsMessage);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);

    }
    
    @Test
    public void testSendSmsFailed() {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("您的验证码是：503868。请不要把验证码泄露给其他人。");
        smsMessage.setReceiverNumber("+8616559181054");
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { MOCK_SMS_FAILURE_RESPONSE }));
        ApiResult result = api.sendMessage(smsMessage);
        assertEquals(result.getResponseEnum(), ApiResponseEnum.ACTION_UNSUCCESSFUL);

    }
    
    @Test
    public void testGetBalance() {
        api.setHttpService(MockApiHttpServiceFactory.get(new String[] { MOCK_BALANCE_RESPONSE }));
        ApiResult result = api.getBalance();
        assertEquals(result.getResponseEnum(), ApiResponseEnum.OK);
    }

    private final static String MOCK_BALANCE_RESPONSE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
            "<GetNumResult xmlns=\"http://106.ihuyi.com/\">\n" + 
            "<code>2</code>\n" + 
            "<msg>查询成功</msg>\n" + 
            "<num>10</num>\n" + 
            "</GetNumResult>";
    
    private final static String MOCK_SMS_FAILURE_RESPONSE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
            "<SubmitResult xmlns=\"http://106.ihuyi.com/\">\n" + 
            "<code>406</code>\n" + 
            "<msg>手机格式不正确，格式为11位的数字.</msg>\n" + 
            "<smsid>0</smsid>\n" + 
            "</SubmitResult>";
    
    private final static String MOCK_SMS_SUCCESS_RESPONSE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
            "<SubmitResult xmlns=\"http://106.ihuyi.com/\">\n" + 
            "<code>2</code>\n" + 
            "<msg>提交成功</msg>\n" + 
            "<smsid>15856527107358444194</smsid>\n" + 
            "</SubmitResult>";
}
