package com.tripleonetech.sbe.sms;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class SmsApiConfigMapperTest extends BaseTest {

    @Autowired
    private SmsApiConfigMapper mapper;
    
    @Test
    public void testGetEnableSmsApi() {
        SmsApiConfig apiConfig = mapper.getEnabledSmsApi();
        assertNotNull(apiConfig);
    }
}
