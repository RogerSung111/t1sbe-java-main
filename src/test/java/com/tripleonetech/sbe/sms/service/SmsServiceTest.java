package com.tripleonetech.sbe.sms.service;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.sms.SmsApiConfigMapper;
import com.tripleonetech.sbe.sms.history.*;
import com.tripleonetech.sbe.sms.integration.SmsApiFactory;
import com.tripleonetech.sbe.sms.integration.impl.SmsApiMockSms;
import com.tripleonetech.sbe.sms.integration.impl.SmsApiT1Notification;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class SmsServiceTest extends BaseTest {
    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;

    @Autowired
    private SmsService smsService;

    @Autowired
    private SmsApiT1Notification smsApi;

    @Autowired
    private SmsApiConfigMapper smsApiConfigMapper;

    @Autowired
    private T1NotificationApi notificationApi;

    private final static int PLAYER_ID_TEST001 = 1;
    private final static String MOCK_PHONE_NUMBER = "886-917123456";
    private final static BigDecimal MOCK_BALANCE = new BigDecimal(20);

    @Before
    public void init() {
        smsApi.setConfig(smsApiConfigMapper.get(3));
        SmsApiFactory apiFactory = Mockito.mock(SmsApiFactory.class);
        smsService.setApiFactory(apiFactory);
        when(apiFactory.getEnabledSmsApi()).thenReturn(smsApi);
    }

    @Test
    public void testGeneralSms() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String message = "Dear players, The maintenance of SBE Game Hall will be held at 09:00 a.m. on November 21.";
        boolean isSuccess = smsService.sendGeneralSms(MOCK_PHONE_NUMBER, message);
        assertTrue("Result should be true, result: " + isSuccess, isSuccess);
    }

    @Test
    public void testGetSmsBalance() {
        // get balance of SmsApiMockSms
        ApiResult result = new ApiResult(ApiResponseEnum.OK);
        result.setParam("response", MOCK_BALANCE);

        SmsApiMockSms mockSmsApi = Mockito.mock(SmsApiMockSms.class);
        SmsApiFactory apiFactory = Mockito.mock(SmsApiFactory.class);
        smsService.setApiFactory(apiFactory);
        when(apiFactory.getEnabledSmsApi()).thenReturn(mockSmsApi);
        when(mockSmsApi.getBalance()).thenReturn(result);
        BigDecimal balance = smsService.getSmsBalance();
        assertEquals("The balance should be " + MOCK_BALANCE + ", "
                + "Real result: " + balance, MOCK_BALANCE, balance);
    }

    @Test
    public void testSendOtpSms() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String otpCode = smsService.sendOtpSms(MOCK_PHONE_NUMBER, PLAYER_ID_TEST001);

        assertNotNull(otpCode);
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(PLAYER_ID_TEST001, MOCK_PHONE_NUMBER);
        assertNotNull(smsOtpHistory);
        assertEquals(otpCode, smsOtpHistory.getOtpCode());
        assertEquals(smsApi.getId(), smsOtpHistory.getSmsApiId().intValue());
        assertEquals(PLAYER_ID_TEST001, smsOtpHistory.getPlayerId().intValue());
        assertFalse(smsOtpHistory.isVerified());
        assertTrue(smsOtpHistory.isDelivered());
        assertTrue(smsOtpHistory.getExpirationTime().isAfter(LocalDateTime.now()));
    }

    @Test
    public void testSendOtpSms_Failed() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockFailedResponse }));
        smsService.sendOtpSms(MOCK_PHONE_NUMBER, PLAYER_ID_TEST001);

        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(PLAYER_ID_TEST001, MOCK_PHONE_NUMBER);
        assertFalse(smsOtpHistory.isVerified());
        assertFalse(smsOtpHistory.isDelivered());
    }

    @Test
    public void testValidateOtpCode() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String otpCode = smsService.sendOtpSms(MOCK_PHONE_NUMBER, PLAYER_ID_TEST001);

        SmsOtpValidationResultEnum result = smsService.validateOtpCode(PLAYER_ID_TEST001, otpCode, MOCK_PHONE_NUMBER);
        assertEquals(SmsOtpValidationResultEnum.PASS, result);

        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(PLAYER_ID_TEST001, MOCK_PHONE_NUMBER);
        assertEquals(otpCode, smsOtpHistory.getOtpCode());
        assertTrue(smsOtpHistory.isVerified());
    }

    @Test
    public void testValidateOtpCode_NotEquals_Failed() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        smsService.sendOtpSms(MOCK_PHONE_NUMBER, PLAYER_ID_TEST001);

        SmsOtpValidationResultEnum result = smsService.validateOtpCode(PLAYER_ID_TEST001, "123456", MOCK_PHONE_NUMBER);
        assertEquals(SmsOtpValidationResultEnum.NOT_PASS_OTP_WRONG, result);
    }

    @Test
    public void testValidateOtpCode_AlreadyVerified_Failed() throws RestResponseException {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String otpCode = smsService.sendOtpSms(MOCK_PHONE_NUMBER, PLAYER_ID_TEST001);
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(PLAYER_ID_TEST001, MOCK_PHONE_NUMBER);
        smsOtpHistoryMapper.updateRecordToVerified(smsOtpHistory.getId());

        SmsOtpValidationResultEnum result = smsService.validateOtpCode(PLAYER_ID_TEST001, otpCode, MOCK_PHONE_NUMBER);
        assertEquals(otpCode, smsOtpHistory.getOtpCode());
        assertEquals(SmsOtpValidationResultEnum.NOT_PASS_OTP_WRONG, result);
    }

    @Test
    public void testValidateOtpCode_Expired_Failed() {
        SmsOtpValidationResultEnum result = smsService.validateOtpCode(PLAYER_ID_TEST001, "123321", "16559181234");
        assertEquals(SmsOtpValidationResultEnum.NOT_PASS_OTP_EXPIRED, result);
    }

    private static String mockSuccessResponse;
    private static String mockFailedResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
        mockFailedResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":90}}";
    }

}
