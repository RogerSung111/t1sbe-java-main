package com.tripleonetech.sbe.sms.history;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class SmsOtpHistoryMapperTest extends BaseTest {

    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;

    private final static String MOCK_OTP_CODE = "232323";
    private final static String MOCK_PHONE = "16559181054";
    private int mockPlayerIdTest001 = 1;

    @Test
    public void testInsert() {
        int mockSmsApiId = 1;

        SmsOtpHistory smsOtpHistory = new SmsOtpHistory();
        smsOtpHistory.setOtpCode(MOCK_OTP_CODE);
        smsOtpHistory.setPhoneNumber(MOCK_PHONE);
        smsOtpHistory.setPlayerId(mockPlayerIdTest001);
        smsOtpHistory.setVerified(false);
        smsOtpHistory.setSmsApiId(mockSmsApiId);
        smsOtpHistory.setExpirationTime(LocalDateTime.now().plusMinutes(10));
        smsOtpHistory.setDelivered(true);
        smsOtpHistoryMapper.insert(smsOtpHistory);

        SmsOtpHistory newSmsOtpHistory = smsOtpHistoryMapper.get(smsOtpHistory.getId());
        assertNotNull(newSmsOtpHistory);
        assertTrue(newSmsOtpHistory.isDelivered());
        assertTrue(!newSmsOtpHistory.isVerified());
        assertNotNull(newSmsOtpHistory.getExpirationTime());
    }

    @Test
    public void testGet() {
        long mockSmsRequestHistoryId = 1;
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.get(mockSmsRequestHistoryId);
        assertNotNull(smsOtpHistory);
        assertNotNull(smsOtpHistory.getExpirationTime());
        assertNotNull(smsOtpHistory.getOtpCode());
        assertNotNull(smsOtpHistory.getPhoneNumber());

    }

    @Test
    public void testGetByPlayerIdAndPhone() {
        testInsert();
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(mockPlayerIdTest001, MOCK_PHONE);
        assertNotNull(smsOtpHistory);
        assertNotNull(smsOtpHistory.getExpirationTime());
        assertEquals(MOCK_OTP_CODE, smsOtpHistory.getOtpCode());
        assertEquals(MOCK_PHONE, smsOtpHistory.getPhoneNumber());
    }

    @Test
    public void testListOtpHistoryByCriteria() {
        // Insert a mock data of player test001
        testInsert();

        // query form
        String mockPlayer = "test001";
        SmsOtpHistoryQueryForm form = new SmsOtpHistoryQueryForm();
        form.setCreatedAtStart(ZonedDateTime.now().minusHours(1));
        form.setCreatedAtEnd(ZonedDateTime.now());
        form.setUsername(mockPlayer);

        List<SmsOtpHistoryView> result = smsOtpHistoryMapper.listOtpHistoryByCriteria(form, form.getPage(),
                form.getLimit());

        // assert
        assertTrue(result.size() > 0);
        assertEquals(mockPlayer, result.get(0).getUsername());
        assertNotNull(result.get(0).getSmsApi());
        assertTrue(result.get(0).getExpirationTime().compareTo(result.get(0).getCreatedAt()) > 0);
    }

    @Test
    public void testListOtpHistoryByCriteria_OnlyRequiredParams() {
        SmsOtpHistoryQueryForm form = new SmsOtpHistoryQueryForm();

        List<SmsOtpHistoryView> result = smsOtpHistoryMapper.listOtpHistoryByCriteria(form, form.getPage(),
                form.getLimit());

        // assert
        assertTrue(result.size() > 0);
    }

    @Test
    public void testupdateRecordToVerified() {
        long id = 2;
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.get(id);
        assertFalse(smsOtpHistory.isVerified());
        smsOtpHistoryMapper.updateRecordToVerified(id);
        smsOtpHistory = smsOtpHistoryMapper.get(id);
        assertTrue(smsOtpHistory.isVerified());
    }
}
