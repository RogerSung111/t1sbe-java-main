package com.tripleonetech.sbe.promo.scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSetting;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSettingMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PromoReferralCalculationTest extends BaseTest {


    @Autowired
    private PromoReferralCalculation promoReferralCalculation;

    @Autowired
    private PromoBonusMapper promoBonusMapper;

    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    @Autowired
    private PromoPlayerReferralSettingMapper promoReferralMapper;

    @Autowired
    private Constant constant;

    private final static int TEST_RULE = 5;
    private PromoBonus promoBonus;
    private final static int REFERRER_PLAYER_ID = 2;
    private final static BigDecimal EXCEPTED_BONUS = new BigDecimal("18.0");

    @Before
    public void init() {
        promoBonus = new PromoBonus();
        promoBonus.setBonusDate(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS)
                .plusHours(Constant.getInstance().getCashback().getCalculationTime()).minusSeconds(1));
        promoBonus.setPlayerId(REFERRER_PLAYER_ID);
        promoBonus.setRuleId(TEST_RULE);
        promoBonus.setPromoType(PromoTypeEnum.REFERRAL);
    }

    @Test
    public void testDailyBetRevenue() {
        promoReferralCalculation.dailyCalculate();
        List<PromoBonus> existingPromoBonus = promoBonusMapper.query(promoBonus);
        existingPromoBonus.forEach(pb -> assertTrue(BigDecimal.ZERO.compareTo(pb.getReferenceAmount()) < 0));
        BigDecimal bonus = existingPromoBonus
                .stream().map(PromoBonus::getBonusAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        assertThat(EXCEPTED_BONUS, Matchers.comparesEqualTo(bonus));
    }

    @Test
    public void testGetReferralRule() {
        int reportId = 29;
        Integer exceptedRuleId = 1;

        GameLogHourlyReport report = gameLogHourlyReportMapper.get(reportId);
        SiteCurrencyEnum currency = SiteCurrencyEnum.valueOf(report.getCurrency());
        promoReferralCalculation.loadPromoSettings(Collections.singletonList(report));
        PromoPlayerReferralSetting rule = promoReferralCalculation.getRule(report.getPlayerId(), currency);

        assertEquals(exceptedRuleId, rule.getId());
    }

    @Test
    public void testGetDefaultReferralRule() {
        int reportId = 40; // game log hourly record for player test004, whose referer is test003, who does not have
        GameLogHourlyReport report = gameLogHourlyReportMapper.get(reportId);
        SiteCurrencyEnum currency = SiteCurrencyEnum.valueOf(report.getCurrency());
        promoReferralCalculation.loadPromoSettings(Collections.singletonList(report));
        PromoPlayerReferralSetting rule = promoReferralCalculation.getRule(report.getPlayerId(), currency);

        assertNotNull("Rule must exist", rule);
        assertNull("Rule must be default (with null player id)", rule.getPlayerCredentialId());
    }

    @Test
    public void testGetNoReferralRule() {
        int reportId = 49;// game log hourly record for player test003, who does not have referer
        GameLogHourlyReport report = gameLogHourlyReportMapper.get(reportId);
        SiteCurrencyEnum currency = SiteCurrencyEnum.valueOf(report.getCurrency());
        promoReferralCalculation.loadPromoSettings(Collections.singletonList(report));
        PromoPlayerReferralSetting rule = promoReferralCalculation.getRule(report.getPlayerId(), currency);

        assertNull(rule);
    }

    @Test
    public void testCalculateBonus() {
        BigDecimal expectedBonus = new BigDecimal("20");
        int referralRule = 5;
        PromoPlayerReferralSetting referralSetting = promoReferralMapper.get(referralRule);
        BigDecimal bonusAmount = promoReferralCalculation.calculateBonus(new BigDecimal(500), referralSetting);
        assertThat(expectedBonus, Matchers.comparesEqualTo(bonusAmount));
    }

    @Test
    public void testCalculateBonusWithMaxBonusAmount() {
        BigDecimal expectedBonus = new BigDecimal("100");
        int referralRule = 5;
        PromoPlayerReferralSetting referralSetting = promoReferralMapper.get(referralRule);
        BigDecimal bonusAmount = promoReferralCalculation.calculateBonus(new BigDecimal(5000), referralSetting);
        assertThat(expectedBonus, Matchers.comparesEqualTo(bonusAmount));
    }

}
