package com.tripleonetech.sbe.promo.scheduled;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.cashback.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PromoCashbackCalculationTest extends BaseTest {


    @Autowired
    private PromoCashbackCalculation promoCashbackCalculation;

    @Autowired
    private PromoBonusMapper promoBonusMapper;

    @Autowired
    private PromoCashbackMapper promoCashbackMapper;

    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;

    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    @Autowired
    private Constant constant;
    @Autowired
    private PlayerMapper playerMapper;

    private final static int BET_BASED_CASHBACK_RULE_NORMAL_CASHBACK = 1;
    private PromoBonus promoBonus;
    private final static int PLAYER_ID = 6;
    private final static BigDecimal EXCEPTED_BONUS = new BigDecimal("40.1");

    @Before
    public void init() {
        ZoneId zoneId = ZoneId.of(constant.getTimezone());
        LocalDate dailyCalculateDate = LocalDate.now(zoneId);
        promoBonus = new PromoBonus();
        promoBonus.setBonusDate(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS)
                .plusHours(Constant.getInstance().getCashback().getCalculationTime()).minusSeconds(1));
        promoBonus.setPlayerId(PLAYER_ID);
        promoBonus.setRuleId(BET_BASED_CASHBACK_RULE_NORMAL_CASHBACK);
        promoBonus.setPromoType(PromoTypeEnum.CASHBACK);
    }


    @Test
    public void testRealTimeCashback() {
        PromoCashbackSetting promoCashbackSetting = promoCashbackSettingMapper.get();
        PromoCashbackSettingForm form = new PromoCashbackSettingForm();
        BeanUtils.copyProperties(promoCashbackSetting, form);
        form.setExecutionType(CashbackExecutionTypeEnum.REALTIME.getCode());
        form.setEnabled(true);
        promoCashbackSettingMapper.update(form);

        promoCashbackCalculation.realtimeCalculate();
        List<PromoBonus> existingPromoBonus = promoBonusMapper.query(promoBonus);
        existingPromoBonus.forEach(pb -> assertTrue(BigDecimal.ZERO.compareTo(pb.getReferenceAmount()) < 0));
        BigDecimal bonus = existingPromoBonus
                .stream().map(PromoBonus::getBonusAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        assertThat(EXCEPTED_BONUS, Matchers.comparesEqualTo(bonus));
    }

    @Test
    public void testDailyCashback() {
        PromoCashbackSetting promoCashbackSetting = promoCashbackSettingMapper.get();
        PromoCashbackSettingForm form = new PromoCashbackSettingForm();
        BeanUtils.copyProperties(promoCashbackSetting, form);
        form.setExecutionType(CashbackExecutionTypeEnum.DAILY.getCode());
        form.setEnabled(true);
        promoCashbackSettingMapper.update(form);

        promoCashbackCalculation.dailyCalculate();
        List<PromoBonus> existingPromoBonus = promoBonusMapper.query(promoBonus);
        existingPromoBonus.forEach(pb -> assertTrue(BigDecimal.ZERO.compareTo(pb.getReferenceAmount()) < 0));
        BigDecimal bonus = existingPromoBonus
                .stream().map(PromoBonus::getBonusAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        assertThat(EXCEPTED_BONUS, Matchers.comparesEqualTo(bonus));
    }

    @Test
    public void testGetCashbackRule() {
        int reportId = 19;
        Integer exceptedGroupId = 1;
        GameLogHourlyReport gameLogHourlyReport = gameLogHourlyReportMapper.get(reportId);
        promoCashbackCalculation.loadPromoSettings(Collections.singletonList(gameLogHourlyReport));
        Cashback cashback = promoCashbackCalculation.getCashbackRule(gameLogHourlyReport);

        assertEquals(exceptedGroupId, cashback.getGroupId());
    }

    @Test
    public void testGetDefaultCashbackRule() {
        int reportId = 32;
        GameLogHourlyReport gameLogHourlyReport = gameLogHourlyReportMapper.get(reportId);
        promoCashbackCalculation.loadPromoSettings(Collections.singletonList(gameLogHourlyReport));
        Cashback cashback = promoCashbackCalculation.getCashbackRule(gameLogHourlyReport);

        assertNull(cashback.getGroupId());
    }

    @Test
    public void testGetNoCashbackRule() {
        int reportId = 1;
        GameLogHourlyReport gameLogHourlyReport = gameLogHourlyReportMapper.get(reportId);
        promoCashbackCalculation.loadPromoSettings(Collections.singletonList(gameLogHourlyReport));
        Cashback cashback = promoCashbackCalculation.getCashbackRule(gameLogHourlyReport);

        assertNull(cashback);
    }

    @Test
    public void testPreCalculateBonus() {
        BigDecimal expectedBonus = new BigDecimal("45.1");
        Integer cashbackRule = 1;
        List<PromoCashbackBetRange> betRagnges = promoCashbackMapper.getSettings(cashbackRule);
        BigDecimal cashbackAmount = promoCashbackCalculation.preCalculateBonus(new BigDecimal(500), betRagnges);
        assertThat("bonus should be " + expectedBonus + ", result :" + cashbackAmount, expectedBonus, Matchers.comparesEqualTo(cashbackAmount));
    }

    @Test
    public void testPreCalculateBonusWhenLimitWasUntilZero() {
        BigDecimal expectedBonus = new BigDecimal("44.85");
        Integer cashbackRule = 2;
        List<PromoCashbackBetRange> betRanges = promoCashbackMapper.getSettings(cashbackRule);
        BigDecimal cashbackAmount = promoCashbackCalculation.preCalculateBonus(new BigDecimal(600), betRanges);
        assertThat("bonus should be " + expectedBonus + ", result :" + cashbackAmount, expectedBonus, Matchers.comparesEqualTo(cashbackAmount));
    }

    @Test
    public void testExecute_CashbackStatusIsDisabled() {
        Integer mockPlayerId = 6;
        playerMapper.updateCashbackEnabled(mockPlayerId, false);

        promoCashbackCalculation.dailyCalculate();

        LocalDateTime now = LocalDateTime.now();
        List<PromoBonus> list = promoBonusMapper.listByPlayerId(mockPlayerId, now.plusMinutes(-1), now);
        PromoBonus promoBonus = list.get(list.size() - 1);
        assertEquals(BonusStatusEnum.REJECTED, promoBonus.getStatus());
        assertEquals("player's cashback status is inactive", promoBonus.getComment());
    }

}
