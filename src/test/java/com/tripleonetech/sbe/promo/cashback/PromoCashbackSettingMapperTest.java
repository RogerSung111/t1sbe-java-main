package com.tripleonetech.sbe.promo.cashback;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.model.MultiCurrencyAmount;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PromoCashbackSettingMapperTest extends BaseTest {
    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;

    @Test
    public void testGet() {
        PromoCashbackSetting setting = promoCashbackSettingMapper.get();
        assertNotNull(setting);
        assertNotNull(setting.getMinBonusAmount());
        assertTrue("There should be more than 1 currency in min bonus amount field", setting.getMinBonusAmount().size() > 1);
        assertNotNull(setting.getMaxBonusAmount());
        assertTrue("There should be global withdraw condition multiplier", setting.getWithdrawConditionMultiplier() > 0);
    }


    @Test
    public void testUpdate() {
        PromoCashbackSettingForm form = new PromoCashbackSettingForm();
        form.setEnabled(false);
        form.setExecutionType(CashbackExecutionTypeEnum.REALTIME.getCode());
        form.setMinBonusAmount(new MultiCurrencyAmount(Arrays.asList(
                new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.CNY, "100.00"),
                new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.USD, "20.00")
        )));
        form.setMaxBonusAmount(new MultiCurrencyAmount(Arrays.asList(
                new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.CNY, "1000.00"),
                new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.USD, "200.00")
        )));
        promoCashbackSettingMapper.update(form);

        PromoCashbackSetting promoCashbackSetting = promoCashbackSettingMapper.get();
        assertEquals(promoCashbackSetting.isEnabled(), form.getEnabled());
        assertNotNull(promoCashbackSetting.getMinBonusAmount());
        assertEquals(
                form.getMinBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(),
                promoCashbackSetting.getMinBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(), 0.01);
        assertEquals(
                form.getMaxBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(),
                promoCashbackSetting.getMaxBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(), 0.01);
    }
}

