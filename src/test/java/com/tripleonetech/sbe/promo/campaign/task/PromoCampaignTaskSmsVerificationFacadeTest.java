package com.tripleonetech.sbe.promo.campaign.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.PlayerProfile;
import com.tripleonetech.sbe.player.PlayerProfileMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.sms.history.SmsOtpValidationSuccessEvent;

public class PromoCampaignTaskSmsVerificationFacadeTest extends BaseTest {
    @Autowired
    private PromoCampaignTaskSmsVerificationFacade facade;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PromoCampaignTaskMapper promoCampaignTaskMapper;

    private SmsOtpValidationSuccessEvent event;
    private final int MOCK_PLAYER_TEST002_ID = 2;
    private final int MOCK_PLAYER_TEST003_ID = 3;

    private PromoCampaignTask MOCK_PHONE_CAMPAIGN;

    @Before
    public void setUp() {
        MOCK_PHONE_CAMPAIGN = promoCampaignTaskMapper
                .queryAvailableRule(PromoCampaignTaskTypeEnum.SmsVerificationBonus.getCode(), SiteCurrencyEnum.CNY);
    }

    @Test
    public void testCalculatePromo() {
        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(MOCK_PLAYER_TEST002_ID);
        playerProfile.setPhoneVerified(true);
        playerProfileMapper.update(playerProfile);

        event = new SmsOtpValidationSuccessEvent(this, MOCK_PLAYER_TEST002_ID, "123456");
        facade.calculatePromo(event);

        PromoBonus expectedRecordQuery = new PromoBonus(MOCK_PLAYER_TEST002_ID, MOCK_PHONE_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_TASK, null, null, null, null);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertEquals(1, record.size());
        assertEquals(MOCK_PHONE_CAMPAIGN.getFixedBonus(), record.get(0).getBonusAmount());
        assertTrue(BigDecimal.ZERO.compareTo(record.get(0).getReferenceAmount()) == 0);
    }

    @Test
    public void testFailJoinPhoneCampaignOneTimeConstraint() {
        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(MOCK_PLAYER_TEST003_ID);
        playerProfile.setPhoneVerified(true);
        playerProfileMapper.update(playerProfile);

        event = new SmsOtpValidationSuccessEvent(this, MOCK_PLAYER_TEST003_ID, "123456");
        facade.calculatePromo(event);

        PromoBonus expectedRecordQuery = new PromoBonus(MOCK_PLAYER_TEST003_ID, MOCK_PHONE_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_TASK, null, null, null, null);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertEquals(1, record.size());
        assertNotEquals(MOCK_PHONE_CAMPAIGN.getFixedBonus(), record.get(0).getBonusAmount());
        assertTrue(BigDecimal.ZERO.compareTo(record.get(0).getReferenceAmount()) == 0);
    }
}
