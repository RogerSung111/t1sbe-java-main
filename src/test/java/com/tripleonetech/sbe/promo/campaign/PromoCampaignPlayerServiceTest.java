package com.tripleonetech.sbe.promo.campaign;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.tripleonetech.sbe.BaseTest;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PromoCampaignPlayerServiceTest extends BaseTest {
    @Autowired
    private PromoCampaignPlayerService promoCampaignPlayerService;

    @Test
    public void testGetLiveCampaignsForPlayer() {
        List<PromoCampaignPlayerView> campaigns = promoCampaignPlayerService.getLiveCampaignsForPlayer(2);

        assertNotNull(campaigns);
        assertEquals("There should be 5 campaigns visible to this player", 5, campaigns.size());

        PromoCampaignPlayerView campaign = campaigns.get(0);
        assertTrue(campaign.isJoined());
        assertNotNull(campaign.getEffectiveStartTime());
        assertNull(campaign.getEffectiveEndTime());
    }
}
