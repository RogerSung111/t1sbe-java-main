package com.tripleonetech.sbe.promo.campaign.task;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.PlayerLoginSuccessEvent;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLogin;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginFacade;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PromoCampaignLoginFacadeTest extends BaseTest {

    @Autowired
    private PromoCampaignLoginFacade facade;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PromoCampaignLoginMapper campaignLoginMapper;

    private PlayerLoginSuccessEvent playerLoginSuccessEvent;
    private final int MOCK_PLAYER_TEST002_ID = 2;

    private PromoCampaignLogin MOCK_LOGIN_CAMPAIGN;

    @Before
    public void setUp() {
        MOCK_LOGIN_CAMPAIGN = campaignLoginMapper.queryAvailableRule(SiteCurrencyEnum.CNY);
        playerLoginSuccessEvent = new PlayerLoginSuccessEvent(this, MOCK_PLAYER_TEST002_ID);
    }

    @Test
    public void testCalculate() {
        facade.calculatePromo(playerLoginSuccessEvent);

        PromoBonus expectedRecordQuery = new PromoBonus(MOCK_PLAYER_TEST002_ID, MOCK_LOGIN_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_LOGIN, null, null, DateUtils.getStartOfDay().minusDays(1), null);
        expectedRecordQuery.setLoginDay(1);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertEquals(1, record.size());
        assertEquals(MOCK_LOGIN_CAMPAIGN.getFixedBonus((byte) 1), record.get(0).getBonusAmount());

        PromoBonus expectedRecordQueryDay2 = new PromoBonus(MOCK_PLAYER_TEST002_ID, MOCK_LOGIN_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_LOGIN, null, null, DateUtils.getStartOfDay(), null);
        expectedRecordQueryDay2.setLoginDay(2);
        List<PromoBonus> recordDay2 = promoBonusMapper.query(expectedRecordQueryDay2);

        System.out.println("MOCK LOGIN CAMPAIGN's rules: " + MOCK_LOGIN_CAMPAIGN.getRules().toString());
        assertEquals(MOCK_LOGIN_CAMPAIGN.getFixedBonus((byte) 2), recordDay2.get(0).getBonusAmount());

        assertTrue(BigDecimal.ZERO.compareTo(record.get(0).getReferenceAmount()) == 0);
    }
}
