package com.tripleonetech.sbe.promo.campaign;


import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PromoCampaignListMapperTest extends BaseTest {
    @Autowired
    private PromoCampaignListMapper mapper;

    @Test
    public void testQuery(){
        PromoCampaignListQueryForm form = new PromoCampaignListQueryForm();
        form.setName("大礼包");
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setAutoJoin(true);
        form.setStatus(Arrays.asList(CampaignApprovalStatusEnum.APPROVED.getCode()));
        form.setType(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        form.setStartTime(ZonedDateTime.now().minusDays(1));
        form.setEndTime(ZonedDateTime.now().plusDays(1));

        List<PromoCampaignListView> campaigns = mapper.query(form, form.getPage(), form.getLimit());
        assertEquals("The query should return exactly one result", 1, campaigns.size());

        PromoCampaignListView theDepositCampaign = campaigns.get(0);
        assertNotNull(theDepositCampaign.getEffectiveStartTime());
        // This campaign has end time set, but end time is a future date, so no effective end time yet.
        assertNotNull(theDepositCampaign.getEndTime());
        assertNull(theDepositCampaign.getEffectiveEndTime());
        assertTrue(theDepositCampaign.getBonusCount() > 0);
        assertEquals(0, theDepositCampaign.getPendingBonusCount());
        assertTrue(theDepositCampaign.getBonusPlayerCount() > 0);
    }

    @Test
    public void testQueryNoParam() {
        PromoCampaignListQueryForm form = new PromoCampaignListQueryForm();
        List<PromoCampaignListView> campaigns = mapper.query(form, form.getPage(), form.getLimit());
        assertTrue("The query should return all results", campaigns.size() > 0);

        for(PromoCampaignListView campaign : campaigns){
            assertEquals(SiteCurrencyEnum.CNY, campaign.getCurrency());
            assertNotNull(campaign.getStartTime());
        }
    }
}
