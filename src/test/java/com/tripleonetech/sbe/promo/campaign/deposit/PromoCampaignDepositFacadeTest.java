package com.tripleonetech.sbe.promo.campaign.deposit;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusView;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.bonus.PromoCampaignBonusQueryForm;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class PromoCampaignDepositFacadeTest extends BaseTest {

    @Autowired
    PromoCampaignDepositFacade facade;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    WithdrawConditionMapper withdrawConditionMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private PromoCampaignDepositMapper promoCampaignDepositMapper;

    private DepositRequest depositRequest;
    private int TEST_PAYMENT_METHOD_ID_1 = 1;
    private int TEST_PLAYER_ID_1 = 1;
    private final int MOCK_PLAYER_TEST001_CREDENTIAL_ID = 1;
    private BigDecimal TEST_DEPOSIT_AMOUNT = new BigDecimal(100);
    private final int MOCK_DEPOSIT_CAMPAIGN_ID = 1;

    private PromoCampaignDeposit MOCK_DEPOSIT_CAMPAIGN;


    @Before
    public void setUp() {
        depositRequest = new DepositRequest();
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(6));
        depositRequest.setAmount(TEST_DEPOSIT_AMOUNT);
        depositRequest.setPaymentMethodId(TEST_PAYMENT_METHOD_ID_1);
        depositRequest.setPlayerId(TEST_PLAYER_ID_1);
        depositRequest.setRequestedDate(LocalDateTime.now());
        // depositRequestMapper.insert(depositRequest);
        // depositRequest = depositRequestMapper.get(depositRequest.getId());

        MOCK_DEPOSIT_CAMPAIGN = promoCampaignDepositMapper.get(MOCK_DEPOSIT_CAMPAIGN_ID);
    }

    @Test
    public void testProcess_WithOneTimeBonusReceiveCycleConstraint() {
        BigDecimal lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(TEST_PLAYER_ID_1);
        List<BonusView> bonus = promoBonusMapper.queryExistsBonus(TEST_PLAYER_ID_1, PromoTypeEnum.CAMPAIGN_DEPOSIT, MOCK_DEPOSIT_CAMPAIGN_ID,
                null, null);
        Wallet mainWallet = walletService.getMainWallet(TEST_PLAYER_ID_1);
        assertEquals(0, bonus.size());
        assertThat(new BigDecimal(9834), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(BigDecimal.ZERO, Matchers.comparesEqualTo(lockedOnAmountSum));

        facade.process(depositRequest);

        bonus = promoBonusMapper.queryExistsBonus(TEST_PLAYER_ID_1, PromoTypeEnum.CAMPAIGN_DEPOSIT, MOCK_DEPOSIT_CAMPAIGN_ID, null, null);
        mainWallet = walletService.getMainWallet(TEST_PLAYER_ID_1);

        lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(TEST_PLAYER_ID_1);

        assertEquals(1, bonus.size());
        assertEquals(0, MOCK_DEPOSIT_CAMPAIGN.getFixedBonus().compareTo(bonus.get(0).getBonusAmount()));
        assertThat(new BigDecimal(9834).add(bonus.get(0).getBonusAmount()), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(bonus.get(0).getBonusAmount(), Matchers.comparesEqualTo(lockedOnAmountSum));
    }

    @Test
    public void testProcess_WithMonthlyBonusReceiveCycleConstraint() {
        // Insert mock bonus record to test BonusReceiveCycleConstraint
        PromoBonus promoBonus = new PromoBonus(TEST_PLAYER_ID_1, MOCK_DEPOSIT_CAMPAIGN_ID, MOCK_DEPOSIT_CAMPAIGN.getType(), MOCK_DEPOSIT_CAMPAIGN.getFixedBonus(), MOCK_DEPOSIT_CAMPAIGN.getMinDeposit(),
                DateUtils.getStartOfDay().minusWeeks(1), MOCK_DEPOSIT_CAMPAIGN.getWithdrawConditionMultiplier());
        promoBonusMapper.insert(promoBonus);

        PromoCampaignBonusQueryForm expectedRecordQuery = new PromoCampaignBonusQueryForm();
        List<Integer> promoTypeEnums = new ArrayList<>();
        promoTypeEnums.add(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());

        expectedRecordQuery.setPlayerId(MOCK_PLAYER_TEST001_CREDENTIAL_ID);
        expectedRecordQuery.setCurrency(SiteCurrencyEnum.CNY);
        expectedRecordQuery.setPromoTypes(promoTypeEnums);
        expectedRecordQuery.setRuleId(MOCK_DEPOSIT_CAMPAIGN_ID);
        expectedRecordQuery.setBonusDateStart(ZonedDateTime.now().minusMonths(1));
        expectedRecordQuery.setBonusDateEnd(ZonedDateTime.now());

        List<BonusView> beforeRecord = promoBonusMapper.queryByCampaignQueryForm(expectedRecordQuery,
                expectedRecordQuery.getPage(), expectedRecordQuery.getLimit());

        PromoCampaignDeposit updatedCampaign = new PromoCampaignDeposit();
        updatedCampaign.setId(MOCK_DEPOSIT_CAMPAIGN_ID);
        updatedCampaign.setBonusReceiveCycle(BonusReceiveCycleEnum.MONTHLY);
        updatedCampaign.setFixedBonus(new BigDecimal(160));
        updatedCampaign.setAutoJoin(true);

        promoCampaignDepositMapper.update(updatedCampaign);

        facade.process(depositRequest);

        List<BonusView> record = promoBonusMapper.queryByCampaignQueryForm(expectedRecordQuery,
                expectedRecordQuery.getPage(), expectedRecordQuery.getLimit());
        assertEquals(beforeRecord.size(), record.size());
        assertEquals(MOCK_DEPOSIT_CAMPAIGN.getFixedBonus(), record.get(0).getBonusAmount());
    }

}
