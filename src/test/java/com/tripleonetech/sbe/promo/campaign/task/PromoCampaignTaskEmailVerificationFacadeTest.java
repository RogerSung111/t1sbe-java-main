package com.tripleonetech.sbe.promo.campaign.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.email.EmailVerificationSuccessEvent;
import com.tripleonetech.sbe.player.PlayerCredential;
import com.tripleonetech.sbe.player.PlayerCredentialMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;

public class PromoCampaignTaskEmailVerificationFacadeTest extends BaseTest {
    @Autowired
    PromoCampaignTaskEmailVerificationFacade facade;
    @Autowired
    PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    PromoBonusMapper promoBonusMapper;
    @Autowired
    PromoCampaignTaskMapper promoCampaignTaskMapper;

    private EmailVerificationSuccessEvent emailVerificationSuccessEvent;
    private int MOCK_PLAYER_TEST002_ID = 2;
    private int MOCK_PLAYER_TEST003_ID = 3;

    private PromoCampaignTask MOCK_EMAIL_CAMPAIGN;

    @Before
    public void setUp() {
        MOCK_EMAIL_CAMPAIGN = promoCampaignTaskMapper
                .queryAvailableRule(PromoCampaignTaskTypeEnum.EmailVerificationBonus.getCode(), SiteCurrencyEnum.CNY);
    }

    @Test
    public void testCalculatePromo() {
        PlayerCredential playerCredential = playerCredentialMapper.getByPlayerId(MOCK_PLAYER_TEST002_ID);
        playerCredential.setEmailVerified(true);
        playerCredentialMapper.update(playerCredential);

        emailVerificationSuccessEvent = new EmailVerificationSuccessEvent(this, MOCK_PLAYER_TEST002_ID,
                "unitTestToken");
        facade.calculatePromo(emailVerificationSuccessEvent);
        PromoBonus expectedRecordQuery = new PromoBonus(MOCK_PLAYER_TEST002_ID, MOCK_EMAIL_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_TASK, null, null, null, null);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertTrue(playerCredential.isEmailVerified());
        assertEquals(1, record.size());
        assertEquals(MOCK_EMAIL_CAMPAIGN.getFixedBonus(), record.get(0).getBonusAmount());
        assertTrue(BigDecimal.ZERO.compareTo(record.get(0).getReferenceAmount()) == 0);
    }

    @Test
    public void testFailJoinEmailCampaignOneTimeConstraint() {
        PlayerCredential playerCredential = playerCredentialMapper.getByPlayerId(MOCK_PLAYER_TEST003_ID);
        playerCredential.setEmailVerified(true);
        playerCredentialMapper.update(playerCredential);
        playerCredential = playerCredentialMapper.getByPlayerId(MOCK_PLAYER_TEST003_ID);

        emailVerificationSuccessEvent = new EmailVerificationSuccessEvent(this, MOCK_PLAYER_TEST003_ID,
                "unitTestToken");
        facade.calculatePromo(emailVerificationSuccessEvent);

        PromoBonus expectedRecordQuery = new PromoBonus(MOCK_PLAYER_TEST003_ID, MOCK_EMAIL_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_TASK, null, null, null, null);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertTrue(playerCredential.isEmailVerified());
        assertEquals(1, record.size());
        assertNotEquals(MOCK_EMAIL_CAMPAIGN.getFixedBonus(), record.get(0).getBonusAmount());
        assertTrue(BigDecimal.ZERO.compareTo(record.get(0).getReferenceAmount()) == 0);
    }

}
