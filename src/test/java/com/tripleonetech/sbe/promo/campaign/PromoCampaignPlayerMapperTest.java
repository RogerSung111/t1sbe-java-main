package com.tripleonetech.sbe.promo.campaign;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PromoCampaignPlayerMapperTest extends BaseTest {

    @Autowired
    PromoCampaignPlayerMapper mapper;

    @Test
    public void testInsertBatch() {
        mapper.insertBatch(initialAList());
        List<PromoCampaignPlayer> list = mapper.list(1);
        assertEquals(2, CollectionUtils.size(list));
    }

    @Test
    public void testList() {
        List<PromoCampaignPlayer> list = mapper.list(1);
        assertTrue(CollectionUtils.isEmpty(list));
        mapper.insertBatch(initialAList());
        list = mapper.list(1);
        assertEquals(2, CollectionUtils.size(list));
    }

    private List<PromoCampaignPlayer> initialAList() {
        List<PromoCampaignPlayer> list = new ArrayList<>();
        PromoCampaignPlayer mapping1 = new PromoCampaignPlayer();
        PromoCampaignPlayer mapping2 = new PromoCampaignPlayer();
        mapping1.setPlayerId(1);
        mapping2.setPlayerId(1);
        mapping1.setCampaignId(1);
        mapping2.setCampaignId(2);
        mapping1.setPromoType(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        mapping2.setPromoType(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        list.add(mapping1);
        list.add(mapping2);
        return list;
    }
}
