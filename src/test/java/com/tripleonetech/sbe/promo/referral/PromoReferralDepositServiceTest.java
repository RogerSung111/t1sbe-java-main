package com.tripleonetech.sbe.promo.referral;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusView;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;

public class PromoReferralDepositServiceTest extends BaseTest {

    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    WithdrawConditionMapper withdrawConditionMapper;
    @Autowired
    PromoReferralDepositService promoReferralDepositService;
    @Autowired
    private WalletService walletService;

    private DepositRequest depositRequest;

    @Before
    public void setUp() {
        depositRequest = new DepositRequest();
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(6));
        depositRequest.setAmount(new BigDecimal("100"));
        depositRequest.setPaymentMethodId(1);
        depositRequest.setRequestedDate(LocalDateTime.now());
    }

    @Test
    public void testDepositRevenueProcess() {
        int testRuleId = 4;
        int testReferralPlayerId = 7; // test006 + CNY
        int testReferrerPlayerId = 1; // test001 + CNY
        depositRequest.setPlayerId(testReferralPlayerId);

        BigDecimal lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        List<BonusView> bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId,
                null, null);
        Wallet mainWallet = walletService.getMainWallet(testReferrerPlayerId);
        assertEquals(0, bonus.size());
        assertThat(new BigDecimal(9834), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(BigDecimal.ZERO, Matchers.comparesEqualTo(lockedOnAmountSum));

        promoReferralDepositService.process(depositRequest);

        bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId, null, null);
        mainWallet = walletService.getMainWallet(testReferrerPlayerId);

        lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        assertEquals(1, bonus.size());
        assertThat(new BigDecimal(9844), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(BigDecimal.TEN, Matchers.comparesEqualTo(lockedOnAmountSum));
    }

    @Test
    public void testOneTimeRevenueProcess() {
        int testRuleId = 6;
        int testReferralPlayerId = 5; // test004 + CNY
        int testReferrerPlayerId = 3; // test003 + CNY
        depositRequest.setPlayerId(testReferralPlayerId);

        BigDecimal lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        List<BonusView> bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId,
                null, null);
        Wallet mainWallet = walletService.getMainWallet(testReferrerPlayerId);
        assertEquals(0, bonus.size());
        assertThat(new BigDecimal(3827), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(BigDecimal.ZERO, Matchers.comparesEqualTo(lockedOnAmountSum));

        promoReferralDepositService.process(depositRequest);

        bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId, null, null);
        mainWallet = walletService.getMainWallet(testReferrerPlayerId);

        lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        assertEquals(1, bonus.size());
        assertThat(new BigDecimal(3915), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(new BigDecimal(88), Matchers.comparesEqualTo(lockedOnAmountSum));
    }

    @Test
    public void testProcessWithDefaultRule() {
        int testRuleId = 3;
        int testReferralPlayerId = 57; // test007 + USD
        int testReferrerPlayerId = 60; // test006 + USD
        depositRequest.setPlayerId(testReferralPlayerId);

        BigDecimal lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        List<BonusView> bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId,
                null, null);
        Wallet mainWallet = walletService.getMainWallet(testReferrerPlayerId);
        assertEquals(0, bonus.size());
        assertThat(new BigDecimal(700), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(BigDecimal.ZERO, Matchers.comparesEqualTo(lockedOnAmountSum));

        promoReferralDepositService.process(depositRequest);

        bonus = promoBonusMapper.queryExistsBonus(testReferrerPlayerId, PromoTypeEnum.REFERRAL, testRuleId, null, null);
        mainWallet = walletService.getMainWallet(testReferrerPlayerId);

        lockedOnAmountSum = withdrawConditionMapper.getLockedOnAmountSum(testReferrerPlayerId);
        assertEquals(1, bonus.size());
        assertThat(new BigDecimal(738), Matchers.comparesEqualTo(mainWallet.getBalance()));
        assertThat(new BigDecimal(38), Matchers.comparesEqualTo(lockedOnAmountSum));
    }

}
