package com.tripleonetech.sbe.promo.bonus;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionQueryForm;
import com.tripleonetech.sbe.web.api.ApiReportController;
import com.tripleonetech.sbe.withdraw.condition.WithdrawCondition;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class BonusServiceTest extends BaseTest {
    @Autowired
    private BonusService bonusService;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private WithdrawConditionMapper withdrawConditionMapper;

    private String TEST_OPERATOR_USERNAME = "superadmin";
    private Integer TEST_OPERATOR_ID = 1;

    @Test
    public void testQuery() {
        PromoBonusQueryForm formObj = new PromoBonusQueryForm();
        LocalDateTime mockDateEnd = LocalDateTime.now();
        LocalDateTime mockDateStart = mockDateEnd.plusDays(-1);
        formObj.setStatus(BonusStatusEnum.PENDING_APPROVAL.getCode());
        formObj.setBonusDateStart(DateUtils.localToZoned(mockDateStart));
        formObj.setBonusDateEnd(DateUtils.localToZoned(mockDateEnd));
        formObj.setPage(1);
        formObj.setLimit(10);

        List<BonusView> bonuses = bonusService.query(formObj);
        assertTrue("There should be more than one bonus pending approval", bonuses.size() > 1);
        for (BonusView data : bonuses) {
            assertEquals(BonusStatusEnum.PENDING_APPROVAL.getCode(), data.getStatus().intValue());
        }
    }

    @Test
    public void testQueryCampaign() {
        PromoCampaignBonusQueryForm formObj = new PromoCampaignBonusQueryForm();
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-100);
        formObj.setStatus(BonusStatusEnum.APPROVED.getCode());
        formObj.setBonusDateStart(mockDateStart);
        formObj.setBonusDateEnd(mockDateEnd);

        List<BonusView> bonuses = bonusService.queryCampaign(formObj);
        assertTrue("There should be more than one campaign bonus already approved", bonuses.size() > 1);
        for (BonusView data : bonuses) {
            assertEquals(BonusStatusEnum.APPROVED.getCode(), data.getStatus().intValue());
        }
        BonusView data = bonuses.get(0);
        assertEquals("The bonus should be generated from deposit bonus", 11, data.getPromoType().intValue());
        assertTrue("Bonus amount should be greater than 0", data.getBonusAmount().compareTo(BigDecimal.ZERO) > 0);

        formObj = new PromoCampaignBonusQueryForm();
        formObj.setBonusDateStart(mockDateStart);
        formObj.setBonusDateEnd(mockDateEnd);

        bonuses = bonusService.queryCampaign(formObj);
        assertTrue("There should be more than one campaign bonus already approved", bonuses.size() > 1);
    }

    @Test
    public void testApprove() {
        Integer mockRequestId = 3;
        BonusStatusEnum mockStatus = BonusStatusEnum.APPROVED;

        // Before
        PromoBonus promoBonus = promoBonusMapper.queryById(mockRequestId);
        BigDecimal originalMainWalletBalance = walletService.getMainWalletBalance(promoBonus.getPlayerId());
        assertNotEquals(mockStatus, promoBonus.getStatus());

        // After
        bonusService.approve(promoBonus, TEST_OPERATOR_USERNAME, TEST_OPERATOR_ID);
        promoBonus = promoBonusMapper.queryById(mockRequestId);
        BigDecimal mainWalletBalance = walletService.getMainWalletBalance(promoBonus.getPlayerId());
        assertEquals(mockStatus, promoBonus.getStatus());
        assertEquals((originalMainWalletBalance.add(promoBonus.getBonusAmount())), mainWalletBalance);

        // check walletTransaction
        WalletTransactionQueryForm walletTransactionQueryForm = new WalletTransactionQueryForm();
        walletTransactionQueryForm.setPlayerId(promoBonus.getPlayerId());
        walletTransactionQueryForm.setType(WalletTransactionTypeEnum.CASHBACK.getCode());

        List<ApiReportController.WalletTransactionView> transactionViews = walletTransactionMapper.list(walletTransactionQueryForm, 1, 99);
        transactionViews.sort(Comparator.comparing(ApiReportController.WalletTransactionView::getId).reversed());
        assertEquals(promoBonus.getBonusAmount() ,transactionViews.get(0).getAmount());
        assertEquals(promoBonus.getPromoType().getCode(), transactionViews.get(0).getPromoType().intValue());
        assertEquals(promoBonus.getRuleId(), transactionViews.get(0).getPromoRuleId());

        // check withdraw condition
        WithdrawCondition withdrawCondition = withdrawConditionMapper.getUnsettledConditionsForPlayer(promoBonus.getPlayerId()).get(0);
        assertEquals(withdrawCondition.getBetRequired(), promoBonus.getBonusAmount()
                .multiply(new BigDecimal(promoBonus.getWithdrawConditionMultiplier())));
    }

    @Test
    public void testFailedApproveInvalidStatus() {
        Integer mockRequestId = 3;
        BonusStatusEnum mockStatus = BonusStatusEnum.APPROVED;

        // Before
        PromoBonus promoBonus = promoBonusMapper.queryById(mockRequestId);
        BigDecimal originalMainWalletBalance = walletService.getMainWalletBalance(promoBonus.getPlayerId());
        assertNotEquals(mockStatus, promoBonus.getStatus());

        // After
        bonusService.approve(promoBonus, TEST_OPERATOR_USERNAME, TEST_OPERATOR_ID);
        promoBonus = promoBonusMapper.queryById(mockRequestId);
        BigDecimal mainWalletBalance = walletService.getMainWalletBalance(promoBonus.getPlayerId());
        assertEquals(mockStatus, promoBonus.getStatus());
        assertEquals((originalMainWalletBalance.add(promoBonus.getBonusAmount())), mainWalletBalance);
    }

    @Test
    public void testReject() {
        Integer mockRequestId = 3;
        BonusStatusEnum mockStatus = BonusStatusEnum.REJECTED;

        // Before
        PromoBonus promoBonus = promoBonusMapper.queryById(mockRequestId);
        assertNotEquals(mockStatus, promoBonus.getStatus());

        // After
        bonusService.reject(promoBonus, "unit test");
        promoBonus = promoBonusMapper.queryById(mockRequestId);
        assertEquals(mockStatus, promoBonus.getStatus());
    }

}
