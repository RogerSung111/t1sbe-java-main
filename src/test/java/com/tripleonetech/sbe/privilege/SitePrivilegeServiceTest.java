package com.tripleonetech.sbe.privilege;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.common.integration.UserPrivilegeView;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.player.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SitePrivilegeServiceTest extends BaseTest {

    @Autowired
    private SitePrivilegeService sitePrivilegeService;

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    
    @Autowired
    private PlayerService playerService;
    
    @Autowired
    private PlayerMapper playerMapper;

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    
    @Test
    @Ignore("This requires redis connection")
    public void testDeletePrivilege() {
        UserPrivilegeView mockUserPrivilegeView = new UserPrivilegeView();
        mockUserPrivilegeView.setType("SITE_LANGUAGE");
        mockUserPrivilegeView.setValue("ko-KR");

        // Before
        List<SitePrivilege> allSitePrivileges = sitePrivilegeMapper.allSitePrivileges();
        assertTrue(allSitePrivileges.stream()
                .anyMatch(privilege -> privilege.getType().equals(mockUserPrivilegeView.getType())
                        && privilege.getValue().equals(mockUserPrivilegeView.getValue())));

        sitePrivilegeService.deletePrivilege(mockUserPrivilegeView);

        // After
        allSitePrivileges = sitePrivilegeMapper.allSitePrivileges();
        assertFalse(allSitePrivileges.stream()
                .anyMatch(privilege -> privilege.getType().equals(mockUserPrivilegeView.getType())
                        && privilege.getValue().equals(mockUserPrivilegeView.getValue())));

    }
    
    @Test
    public void testAddNewSiteCurrency() {
        //Sign up a new player
        String newPlayerUsername = "test999";
        PlayerNewForm form = new PlayerNewForm();
        form.setUsername(newPlayerUsername);
        form.setCurrency(SiteCurrencyEnum.CNY);
        String password = "123456";
        DeviceTypeEnum registerDevice = DeviceTypeEnum.COMPUTER;
        String registerIp = "168.168.168.168";
        playerService.register(form, password, registerDevice, registerIp);
        
        //Before
        SiteCurrencyEnum addCurrency = SiteCurrencyEnum.IDR;
        Player player = playerMapper.getByUniqueKey(newPlayerUsername, addCurrency);
        assertNull(player);
        
        List<SitePrivilege> newSiteCurrencies = new ArrayList<>();
        SitePrivilege newSiteCurrency = new SitePrivilege();
        newSiteCurrency.setType(SitePrivilegeTypeEnum.SITE_CURRENCY.name());
        newSiteCurrency.setValue(SiteCurrencyEnum.IDR.name());
        newSiteCurrency.setActive(true);
        newSiteCurrencies.add(newSiteCurrency);
        sitePrivilegeService.processSiteCurrency(newSiteCurrencies);
        player = playerMapper.getByUniqueKey(newPlayerUsername, addCurrency);
        assertNotNull(player);
        assertTrue("The player_status should be CURRENCY_INACTIVE, Result:" + player.getStatus(), PlayerStatusEnum.CURRENCY_INACTIVE.equals(player.getStatus()));

    }

    @Test
    public void testRemoveSiteCurrency() throws Exception {
        List<SitePrivilege> removeCurrencies = new ArrayList<>();
        SiteCurrencyEnum removeCurrency = SiteCurrencyEnum.CNY;
        SitePrivilege removeSiteCurrency = new SitePrivilege();
        removeSiteCurrency.setType(SitePrivilegeTypeEnum.SITE_CURRENCY.name());
        removeSiteCurrency.setValue(removeCurrency.name());
        removeSiteCurrency.setActive(false);
        removeCurrencies.add(removeSiteCurrency);
        
        sitePrivilegeService.processSiteCurrency(removeCurrencies);
        Player player = playerMapper.getByUniqueKey("test002", removeCurrency);
        assertTrue(player.getStatus().equals(PlayerStatusEnum.CURRENCY_INACTIVE));
        
    }
    
    @Test
    public void testSetSitePrivilegeShouldInactiveGameApiBBIN() {
        sitePrivilegeService.setPrivilege(content);
        SitePrivilege sitePrivilege = sitePrivilegeMapper.getSitePrivilege(SitePrivilegeTypeEnum.GAME_API, "T1GBBIN");
        assertFalse("Site Privilege of GameAPI BBIN should be updated to INACTIVE", sitePrivilege.isActive());
        //Check gameApiConfigs
        List<GameApiConfig> gameApiConfigs = gameApiConfigMapper.list();
        List<GameApiConfig> t1BBINs = gameApiConfigs.stream().filter(config -> config.getCode().equals("T1GBBIN")).collect(Collectors.toList());
        for(GameApiConfig bbin : t1BBINs) {
            assertTrue(bbin.getStatus().equals(ApiConfigStatusEnum.INACTIVE));
        }
    }
    
    @Test
    public void testSetSitePrivilegeShouldActiveCurrecnyTHB() {
        sitePrivilegeService.setPrivilege(content);
        List<SitePrivilege> activeSitePrivileges = sitePrivilegeMapper.allSitePrivileges().stream()
                .filter(sitePrivilege -> sitePrivilege.isActive() == true)
                .collect(Collectors.toList());
        assertTrue(activeSitePrivileges.size() == 5);
        
        SitePrivilege currencyCnySitePrivilege = activeSitePrivileges.stream()
                .filter(sitePrivilege -> sitePrivilege.getType().equals(SitePrivilegeTypeEnum.SITE_CURRENCY.name())
                    && sitePrivilege.getValue().equals(SiteCurrencyEnum.THB.name())).findAny().get();
        assertNotNull(currencyCnySitePrivilege);
        assertTrue(currencyCnySitePrivilege.isActive());
    }
    
    private String content = "[\n" + 
            "{\n" + 
            "  \"type\": \"SITE_CURRENCY\",\n" + 
            "  \"value\": \"CNY\"\n" + 
            "},{\n" + 
            "  \"type\": \"SITE_CURRENCY\",\n" + 
            "  \"value\": \"THB\"\n" + 
            "},{\n" + 
            "  \"type\": \"GAME_API\",\n" + 
            "  \"value\": \"T1GAGIN\"\n" +
            "},{\n" + 
            "  \"type\": \"SITE_LANGUAGE\",\n" + 
            "  \"value\": \"zh-CN\"\n" + 
            "},{\n" + 
            "  \"type\": \"SITE_LANGUAGE\",\n" + 
            "  \"value\": \"th\"\n" + 
            "}\n" + 
            "]";
    
    

}
