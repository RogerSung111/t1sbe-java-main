package com.tripleonetech.sbe.withdraw.workflow;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.tripleonetech.sbe.withdraw.WithdrawStatusEnum.*;
import static org.junit.Assert.*;

public class WithdrawWorkflowSettingMapperTest extends BaseTest {

    @Autowired
    private WithdrawWorkflowSettingMapper withdrawWorkflowSettingMapper;

    @Test
    public void testList() {
        List<WithdrawWorkflowSetting> list = withdrawWorkflowSettingMapper.list();
        assertNotNull(list);
        assertEquals(12, list.size());
    }

    @Test
    public void testUpdateWorkflowSetting() {
        String testName = "Financial Review by accountant section";
        WithdrawWorkflowSetting workflowSetting = new WithdrawWorkflowSetting();
        workflowSetting.setId(REVIEW1.getCode());
        workflowSetting.setName(testName);
        workflowSetting.setEnabled(false);
        withdrawWorkflowSettingMapper.updateWorkflowSetting(workflowSetting);

        WithdrawWorkflowSetting setting = withdrawWorkflowSettingMapper.getWorkflowSettingById(REVIEW1);
        assertEquals(testName, setting.getName());
    }

    @Test
    public void testGetWorkflowSettingByCode() {
        WithdrawWorkflowSetting setting = withdrawWorkflowSettingMapper.getWorkflowSettingById(CREATED);
        assertNotNull(setting);
        assertEquals(CREATED, WithdrawStatusEnum.valueOf(setting.getId()));
    }

    @Test
    public void testGetWorkflowWithProcessHistory() {
        Integer testId = 100002;
        List<WithdrawWorkflow> list = withdrawWorkflowSettingMapper.getWorkflowWithProcessHistory(testId);

        assertNotNull(list);
        assertEquals(CREATED, WithdrawStatusEnum.valueOf(list.get(0).getId()));
        assertNull(list.get(1).getWithdrawRequestProcessHistoryIds());
        assertEquals(CANCELED, WithdrawStatusEnum.valueOf(list.get(list.size() - 1).getId()));
    }
}
