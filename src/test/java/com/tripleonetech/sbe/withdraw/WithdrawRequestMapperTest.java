package com.tripleonetech.sbe.withdraw;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class WithdrawRequestMapperTest extends BaseTest {

    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;

    private WithdrawRequest withdrawRequest;

    private final static WithdrawStatusEnum PAYMENT_STATUS_OPEN = WithdrawStatusEnum.CREATED;
    private final static WithdrawStatusEnum PAYMENT_STATUS_APPROVAL = WithdrawStatusEnum.PAID;

    private final static int MOCK_WITHDRAW_REQUEST_NUM = 100001;

    @Before
    public void init() {
        int playerId = 1;
        int bankId = 1;

        withdrawRequest = new WithdrawRequest();
        withdrawRequest.setPlayerId(playerId);
        withdrawRequest.setAmount(BigDecimal.valueOf(200));
        withdrawRequest.setBankId(bankId);
        withdrawRequest.setAccountNumber("1234000055556666");
        withdrawRequest.setBankBranch("Beijing Branch");
        withdrawRequest.setStatus(PAYMENT_STATUS_OPEN);

    }

    @Test
    public void testInsert() {
        withdrawRequestMapper.insert(withdrawRequest);
        assertNotNull(withdrawRequest.getId());
    }

    @Test
    public void testApprove() {
        withdrawRequestMapper.insert(withdrawRequest);
        withdrawRequestMapper.updateStatus(MOCK_WITHDRAW_REQUEST_NUM, WithdrawStatusEnum.PAID);
        withdrawRequest = withdrawRequestMapper.get(MOCK_WITHDRAW_REQUEST_NUM);
        assertEquals(PAYMENT_STATUS_APPROVAL, withdrawRequest.getStatus());

    }

    @Test
    public void testGet() {
        withdrawRequestMapper.insert(withdrawRequest);
        withdrawRequest = withdrawRequestMapper.get(MOCK_WITHDRAW_REQUEST_NUM);
        assertNotNull(withdrawRequest.getId());
    }

    @Test
    public void testList() {
        WithdrawRequestQueryForm query = new WithdrawRequestQueryForm();
        query.setPlayerId(2);

        List<WithdrawRequestView> requests = withdrawRequestMapper.list(query, query.getPage(), query.getLimit());
        assertTrue(requests.size() > 0);
        assertEquals("test002", requests.get(0).getUsername());
    }

    @Test
    public void testGetPendingRequestCount() {
        long pendingRequestCount = withdrawRequestMapper.getPendingRequestCount();
        WithdrawRequestQueryForm queryForm = new WithdrawRequestQueryForm();
        queryForm.setStatus(WithdrawStatusEnum.CREATED.getCode());
        long expectedCount = withdrawRequestMapper.list(queryForm, 1, 999999).size();
        assertEquals(expectedCount, pendingRequestCount);
    }
}
