package com.tripleonetech.sbe.withdraw;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.withdraw.integration.WithdrawApi;
import com.tripleonetech.sbe.withdraw.integration.impl.WithdrawApiMockWithdraw;

public class WithdrawApiFactoryTest extends BaseTest {
    @Autowired
    private WithdrawApiFactory factory;

    @Test
    public void testGetApi() {
        WithdrawApi api = factory.getById(1);
        assertTrue(api instanceof WithdrawApiMockWithdraw);
    }

    @Test
    public void testRefreshApi() {
        factory.refreshById(1);
        WithdrawApi api = factory.getById(1);
        assertTrue("After refresh, API must be re-created by ApiFactory", api instanceof WithdrawApiMockWithdraw);
    }
}
