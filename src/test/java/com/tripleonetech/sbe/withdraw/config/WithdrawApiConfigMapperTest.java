package com.tripleonetech.sbe.withdraw.config;

import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class WithdrawApiConfigMapperTest extends BaseMainTest {
    @Autowired
    private WithdrawApiConfigMapper mapper;

    @Test
    public void testListByCurrency() {
        List<WithdrawApiConfig> configs = mapper.listByCurrency(SiteCurrencyEnum.CNY);
        assertNotNull(configs);
        assertTrue("There should be at least one withdraw API config under CNY", configs.size() > 0);
    }
}
