package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSetting;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class WithdrawRequestProcessHistoryServiceTest extends BaseTest {

    @Autowired
    private WithdrawRequestProcessHistoryService withdrawRequestProcessHistoryService;

    @Test
    public void testGetAvailableWithdrawWorkflowWithUnfinishedProcess() {
        Integer withdrawRequest = 100003;
        List<WithdrawWorkflowSetting> workflowSettings = withdrawRequestProcessHistoryService.getAvailableWithdrawRequestWorkflow(withdrawRequest);
        assertTrue("Available workflow code should be included REJECTED",
                workflowSettings.stream().anyMatch(w -> w.getId().equals(WithdrawStatusEnum.REJECTED.getCode())));
        assertTrue("Available workflow code should be included CANCELED",
                workflowSettings.stream().anyMatch(w -> w.getId().equals(WithdrawStatusEnum.CANCELED.getCode())));
    }

    @Test
    public void testGetAvailableWithdrawWorkflowWithFinishedProcess() {
        Integer withdrawRequest = 100001;
        List<WithdrawWorkflowSetting> workflowSettings = withdrawRequestProcessHistoryService.getAvailableWithdrawRequestWorkflow(withdrawRequest);
        assertTrue("Available workflow should be empty", CollectionUtils.isEmpty(workflowSettings));
    }

}
