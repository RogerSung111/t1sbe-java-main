package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class WithdrawRequestProcessHistoryMapperTest extends BaseTest {

    @Autowired
    private WithdrawRequestProcessHistoryMapper requestHistoryMapper;

    @Test
    public void testInsert() {
        Integer withdrawRequestId = 100003;
        Integer operatorId = 1;

        WithdrawRequestProcessHistory requestHistory = new WithdrawRequestProcessHistory();
        requestHistory.setComment("Create withdraw request");
        requestHistory.setCreatedBy(operatorId);
        requestHistory.setUpdatedBy(operatorId);
        requestHistory.setWithdrawRequestId(withdrawRequestId);
        requestHistory.setFromStatus(WithdrawStatusEnum.PAYING);
        requestHistory.setToStatus(WithdrawStatusEnum.PAID);
        requestHistoryMapper.insert(requestHistory);

        assertNotNull(requestHistory.getId());

        WithdrawRequestProcessHistory newRequestHistory = requestHistoryMapper.get(requestHistory.getId());
        assertNotNull(newRequestHistory);
    }

    @Test
    public void testGetComment() {
        Integer withdrawRequestId = 100001;
        List<WithdrawRequestProcessHistoryView> comments = requestHistoryMapper.getComments(withdrawRequestId);
        assertTrue(comments.size() > 0);

        assertNotNull(comments.get(0).getFromStatus());
        assertNotNull(comments.get(0).getToStatus());
        assertNotNull(comments.get(0).getCreatedBy().getName());
        assertNotNull(comments.get(0).getUpdatedBy().getName());
    }

    @Test
    public void testGetWorkflowHistory() {
        Integer withdrawRequestId = 100001;
        List<WithdrawRequestProcessHistory> comments = requestHistoryMapper.getWorkflowHistory(withdrawRequestId);
        assertTrue(comments.size() > 0);
    }
}
