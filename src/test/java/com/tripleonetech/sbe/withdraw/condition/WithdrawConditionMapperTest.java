package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WithdrawConditionMapperTest extends BaseTest {

    @Autowired
    private WithdrawConditionMapper mapper;

    int playerId_1 = 1;
    int playerId_2 = 2;

    @Test
    public void testInsert() {
        WithdrawConditionQueryForm queryForm = new WithdrawConditionQueryForm();
        queryForm.setPlayerId(playerId_1);
        queryForm.setCurrency(SiteCurrencyEnum.CNY);
        List<WithdrawCondition> conditions = mapper.getUnsettledConditions(queryForm, queryForm.getPage(),
                queryForm.getLimit());
        assertTrue(CollectionUtils.isEmpty(conditions));
        WithdrawCondition record = new WithdrawCondition();
        record.setBeginAt(LocalDateTime.now());
        record.setBetRequired(new BigDecimal(200));
        record.setLockedOnAmount(new BigDecimal(200));
        record.setPlayerId(playerId_1);
        record.setSourceId(1);
        record.setSourceType(WithdrawConditionSourceTypeEnum.DEPOSIT);
        mapper.insert(record);
        conditions = mapper.getUnsettledConditionsForPlayer(playerId_1);
        assertEquals(1, CollectionUtils.size(conditions));
    }

    @Test
    public void testInsertPormoWithdrawalCondition() {
        WithdrawConditionQueryForm queryForm = new WithdrawConditionQueryForm();
        queryForm.setPlayerId(playerId_1);
        queryForm.setCurrency(SiteCurrencyEnum.CNY);

        List<WithdrawCondition> conditions = mapper.getUnsettledConditions(queryForm, queryForm.getPage(),
                queryForm.getLimit());
        assertTrue(CollectionUtils.isEmpty(conditions));

        WithdrawCondition record = new WithdrawCondition();
        record.setBeginAt(LocalDateTime.now());
        record.setBetRequired(new BigDecimal(200));
        record.setLockedOnAmount(new BigDecimal(200));
        record.setPlayerId(playerId_1);
        record.setSourceId(1);
        record.setSourceType(WithdrawConditionSourceTypeEnum.BONUS);
        record.setPromoType(PromoTypeEnum.CAMPAIGN_TASK);
        record.setPromotionId(1);
        mapper.insert(record);
        conditions = mapper.getUnsettledConditionsForPlayer(playerId_1);
        assertEquals(1, CollectionUtils.size(conditions));
        assertEquals(conditions.get(0).getPromoType(), PromoTypeEnum.CAMPAIGN_TASK);
        assertEquals(conditions.get(0).getPromotionId().intValue(), 1);
    }

    @Test
    public void testGetUnsettledConditionsByPlayerId() {
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals(2, CollectionUtils.size(conditions));
    }

    /**
     * query conditions with player id is null
     */
    @Test
    public void testGetAllUnsettledConditions() {
        List<WithdrawCondition> conditions = mapper.getAllUnsettledConditions();
        assertEquals(2, CollectionUtils.size(conditions));
    }

    @Test
    public void testGetLockedOnAmountSum() {
        BigDecimal lockedAmount = mapper.getLockedOnAmountSum(3);
        assertEquals(lockedAmount, new BigDecimal("0.000"));
        lockedAmount = mapper.getLockedOnAmountSum(playerId_2);
        // 166 + 288
        assertEquals(new BigDecimal("454.000"), lockedAmount);
    }

    @Test
    public void testUpdateBetAmount() {
        int count = mapper.updateBetAmount(1L, new BigDecimal("50"));
        assertEquals(1, count);
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals(new BigDecimal("50.000"), conditions.get(0).getBetAmount());
    }

    @Test
    public void testUpdateStatus() {
        int count = mapper.updateStatus(1L, WithdrawConditionStatusEnum.COMPLETED);
        assertEquals(1, count);
        BigDecimal lockedAmount = mapper.getLockedOnAmountSum(playerId_2);
        assertEquals(new BigDecimal("288.000"), lockedAmount);
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals(1, CollectionUtils.size(conditions));
    }

    @Test
    public void testBatchUpdateStatus() {
        List<Long> conditionIds = Arrays.asList(1L, 2L);
        int count = mapper.batchUpdateStatus(conditionIds, WithdrawConditionStatusEnum.COMPLETED);
        assertEquals(2, count);
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals(0, CollectionUtils.size(conditions));
    }

}
