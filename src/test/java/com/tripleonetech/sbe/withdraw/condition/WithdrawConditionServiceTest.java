package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WithdrawConditionServiceTest extends BaseTest {

    @Autowired
    private WithdrawConditionMapper mapper;
    @Autowired
    private WithdrawConditionService service;
    @Autowired
    private WalletMapper walletMapper;

    int playerId_1 = 1;
    int playerId_2 = 2;

    @Test
    public void testCreate() {
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_1);
        assertTrue(CollectionUtils.isEmpty(conditions));
        WithdrawCondition record = new WithdrawCondition();
        record.setBeginAt(LocalDateTime.now());
        record.setBetRequired(new BigDecimal(200));
        record.setLockedOnAmount(new BigDecimal(200));
        record.setPlayerId(playerId_1);
        record.setSourceId(1);
        record.setSourceType(WithdrawConditionSourceTypeEnum.DEPOSIT);
        service.create(record);
        conditions = mapper.getUnsettledConditionsForPlayer(playerId_1);
        assertTrue(CollectionUtils.size(conditions) == 1);
    }

    @Test
    public void testGetValidBalance() {
        BigDecimal validBalance = service.getValidBalance(playerId_2);
        // main + game1 + game2 - condition1 - condition2
        // 666 + 150 + 130 - 166 - 288
        BigDecimal mainBalance = walletMapper.getBalanceSumByPlayerId(playerId_2);
        BigDecimal lockedOnAmount= mapper.getLockedOnAmountSum(playerId_2);
        BigDecimal expectedValue = mainBalance.subtract(lockedOnAmount);
        assertEquals(expectedValue, validBalance);
    }

    @Test
    public void testProcessWithdrawConditions() {
        service.processWithdrawConditions();
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals("Withdraw conditions for playerId_2 are expected to be all completed",
                0, CollectionUtils.size(conditions));
    }

    @Test
    public void testCancel() {
        service.cancel(1l);
        List<WithdrawCondition> conditions = mapper.getUnsettledConditionsForPlayer(playerId_2);
        assertEquals(1, CollectionUtils.size(conditions));
    }

}
