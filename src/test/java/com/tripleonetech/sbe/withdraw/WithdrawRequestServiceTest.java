package com.tripleonetech.sbe.withdraw;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.player.wallet.WalletService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WithdrawRequestServiceTest extends BaseTest {

    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;

    @Autowired
    private WithdrawRequestService withdrawRequestService;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private WalletService walletService;

    private int TEST_PLAYER_ID_1 = 1;
    private BigDecimal TEST_WITHDRAW_AMOUNT = BigDecimal.TEN;
    private int TEST_BANK_ID_1 = 1;
    private WithdrawRequest withdrawRequest;
    private String TEST_WITHDRAW_ACCOUNT = "1232232313123122323";
    private String TEST_BANK_BRANCH_NAME = "Beijing branch";

    @Before
    public void init() {
        withdrawRequest = new WithdrawRequest();
        withdrawRequest.setAmount(TEST_WITHDRAW_AMOUNT);
        withdrawRequest.setPlayerId(TEST_PLAYER_ID_1);
        withdrawRequest.setBankId(TEST_BANK_ID_1);
        withdrawRequest.setRequestedDate(LocalDateTime.now());
        withdrawRequest.setAccountNumber(TEST_WITHDRAW_ACCOUNT);
        withdrawRequest.setBankBranch(TEST_BANK_BRANCH_NAME);
    }

    @Test
    public void testNewWithdrawal() {
        assertTrue(withdrawRequestService.newWithdrawal(withdrawRequest));
    }

    @Test
    public void testApproveWithdrawRequest() {
        WithdrawStatusEnum testStatus = WithdrawStatusEnum.PAID;
        //init
        Wallet originalMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1);

        withdrawRequestMapper.insert(withdrawRequest);
        withdrawRequest = withdrawRequestMapper.get(withdrawRequest.getId());
        walletService.freezeWithdrawAmount(TEST_PLAYER_ID_1, TEST_WITHDRAW_AMOUNT);
        withdrawRequest.setStatus(testStatus);

        withdrawRequestService.approveWithdrawRequest(withdrawRequest);
        WithdrawRequest newStatusWithdrawRequest = withdrawRequestMapper.get(withdrawRequest.getId());
        Wallet newMainWallet = walletMapper.getMainWallet(TEST_PLAYER_ID_1);

        assertEquals(testStatus, newStatusWithdrawRequest.getStatus());
        assertTrue(originalMainWallet.getBalance().subtract(TEST_WITHDRAW_AMOUNT).compareTo(newMainWallet.getBalance()) == 0);

    }


    @Test
    public void testRejectWithdrawRequest() {
        WithdrawStatusEnum testStatus = WithdrawStatusEnum.REJECTED;

        withdrawRequestMapper.insert(withdrawRequest);
        withdrawRequest = withdrawRequestMapper.get(withdrawRequest.getId());
        walletService.freezeWithdrawAmount(TEST_PLAYER_ID_1, TEST_WITHDRAW_AMOUNT);

        withdrawRequestService.rejectWithdrawRequest(withdrawRequest);
        WithdrawRequest newStatusWithdrawRequest = withdrawRequestMapper.get(withdrawRequest.getId());

        assertEquals(testStatus, newStatusWithdrawRequest.getStatus());
    }
}
