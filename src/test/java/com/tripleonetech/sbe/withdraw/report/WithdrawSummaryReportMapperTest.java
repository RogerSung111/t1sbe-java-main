package com.tripleonetech.sbe.withdraw.report;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.TimeRangeForm;

public class WithdrawSummaryReportMapperTest extends BaseTest {
    @Autowired
    private WithdrawSummaryReportMapper withdrawSummaryReportMapper;
    
    @Test
    public void testUpsertDirty() {
        WithdrawSummaryReport withdrawSummaryReport = new WithdrawSummaryReport();
        LocalDateTime timeHour = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);
        withdrawSummaryReport.setTimeHour(timeHour);
        withdrawSummaryReport.setCurrency(SiteCurrencyEnum.IDR);
        withdrawSummaryReport.setWithdrawalAmount(new BigDecimal(100));
        withdrawSummaryReport.setWithdrawalCount(1);
        withdrawSummaryReport.setWithdrawerCount(1);
        withdrawSummaryReport.setDirty(false);
        withdrawSummaryReportMapper.upsertDirty(withdrawSummaryReport);
        
        WithdrawSummaryReport newWithdrawSummaryReport = withdrawSummaryReportMapper.get(timeHour, SiteCurrencyEnum.IDR);
        assertNotNull(newWithdrawSummaryReport);
    }
    
    @Test
    public void  testCalculateSummaryReport() {
        List<WithdrawSummaryReport> withdrawSummaryReportItems = withdrawSummaryReportMapper.listDirty();
        assertTrue(withdrawSummaryReportItems.size() > 0);
        
        withdrawSummaryReportMapper.refreshDirty();
        List<WithdrawSummaryReport> refreshedWithdrawSummaryReportItems = withdrawSummaryReportMapper.listDirty();
        assertTrue(refreshedWithdrawSummaryReportItems.size() == 0);
    }
    
    @Test
    public void testGet() {
        //配合v3測試資料
        LocalDate date = LocalDate.now().minusDays(1);
        LocalTime time = LocalTime.of(13, 00);
        assertNotNull(withdrawSummaryReportMapper.get(date.atTime(time), SiteCurrencyEnum.CNY));
    }
    
    @Test
    public void testListDirty() {
        assertTrue(withdrawSummaryReportMapper.listDirty().size() > 0);
        
    }

    @Test
    public void testQuery() {
        TimeRangeForm form = new TimeRangeForm();
        form.setDateStart(LocalDate.now().minusDays(7));
        form.setDateEnd(LocalDate.now().minusDays(1));
        assertTrue(withdrawSummaryReportMapper.query(form, form.getPage(), form.getLimit()).size() > 0);
    }

}
