package com.tripleonetech.sbe;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * All unit-test-related setup goes here.
 * 
 * * Sets spring active profile to test so that application-test.properties file is used.
 * * Transactional annotation makes sure the changes are rolled back at the end of test, leaving database
 * in the state of afterMigrate.
 * 
 * @author yunfei
 *         Created on 9 Jan 2018
 */
@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest(properties = { "spring.profiles.active=test,db-test,main", "scheduling.enabled=false", "redis.enabled=false" }, classes = T1SBEApplication.class)
public abstract class BaseTest {
}
