package com.tripleonetech.sbe.player;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class PlayerVerificationQuestionI18nMapperTest extends BaseTest {

    @Autowired
    private PlayerVerificationQuestionI18nMapper playerVerificationQuestionI18nMapper;

    @Test
    public void testGet() {
        PlayerVerificationQuestionI18n data = playerVerificationQuestionI18nMapper.get(1, "zh-CN", "en-US");
        assertEquals(1, data.getQuestionId().intValue());
        assertEquals("最喜歡的食物？", data.getQuestion());
    }
    
    @Test
    public void testGet_SpecifiedLocaleNoExists_FetchDefaultLocale() {
        PlayerVerificationQuestionI18n data = playerVerificationQuestionI18nMapper.get(4, "zh-CN", "en-US");
        assertEquals(4, data.getQuestionId().intValue());
        assertEquals("What is your favorite sport?", data.getQuestion());
    }

    @Test
    public void testList() {
        List<PlayerVerificationQuestionI18n> list = playerVerificationQuestionI18nMapper.list("zh-CN", "en-US");
        assertEquals(5, list.size());
    }

}
