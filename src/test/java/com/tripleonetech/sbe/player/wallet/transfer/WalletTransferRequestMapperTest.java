package com.tripleonetech.sbe.player.wallet.transfer;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferRequestMapper;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferStatusEnum;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferRequest;

public class WalletTransferRequestMapperTest extends BaseTest {

    private static int playerTestId = 1;
    private static int gameApiTestId = 1;
    
    private WalletTransferRequest walletTransferRequest;
    
    @Autowired
    private WalletTransferRequestMapper walletTransferRequestMapper;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        walletTransferRequest = new WalletTransferRequest(playerTestId, gameApiTestId, BigDecimal.TEN, WalletTransferStatusEnum.CREATE);
    }

    @Test
    public void testInsert() {
        walletTransferRequestMapper.insert(walletTransferRequest);
    }

    @Test
    public void testUpdateStatus() {
        walletTransferRequestMapper.insert(walletTransferRequest);
        walletTransferRequestMapper.updateStatus(walletTransferRequest.getId(), WalletTransferStatusEnum.PROCESSING);
    }

}
