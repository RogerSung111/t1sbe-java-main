package com.tripleonetech.sbe.player.wallet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;

public class WalletMapperTest extends BaseTest {

    private static int playerTestId = 1;
    private static int gameApiTestId = 1;

    private static final BigDecimal EXPECTED_WITHDRAW_AMOUNT = BigDecimal.TEN;
    private static final BigDecimal EXPECTED_DEPOSIT_AMOUNT = BigDecimal.TEN;
    
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    
    @Autowired
    private WalletMapper walletMapper;

    @Test
    public void testInsert() {
        Wallet wallet = new Wallet(3, 1);
        assertEquals("There should be one record inserted", 1, walletMapper.insert(wallet));
    }

    @Test
    public void testGet() {
        Wallet wallet = new Wallet(3, 1);
        walletMapper.insert(wallet);

        Wallet walletRead = walletMapper.get(3, 1);
        assertNotNull("There should be one record be queried", walletRead);
        assertNotNull("Wallet should have name", walletRead.getName());
    }

    @Test
    public void testGetMainWallets() {
        List<Wallet> mainWallets = walletMapper.getMainWallets(Collections.singletonList(playerTestId));
        assertEquals("There should be only one wallet", 1, mainWallets.size());
    }

    @Test
    public void tesGetActiveGameWalletsByPlayerId() {
 
        gameApiConfigMapper.delete(gameApiTestId);
        Map<Integer, Wallet> gameWallets = walletMapper.getActiveGameWalletsByPlayerId(playerTestId);
        
        assertTrue("There should be at least 1 wallet", gameWallets.size() > 0);
        assertNull(gameWallets.get(gameApiTestId));
    }
    
    @Test
    public void tesGetAllGameWalletsByPlayerId() {
        assertTrue("There should be at least 1 wallet", walletMapper.getAllGameWalletsByPlayerId(playerTestId).size() > 0);
    }

    @Test
    public void testUpdate() {
        Wallet wallet = walletMapper.getActiveGameWalletsByPlayerId(playerTestId).get(gameApiTestId);
        LocalDateTime currentTime = LocalDateTime.now();
        BigDecimal newBalance = wallet.getBalance().add(BigDecimal.TEN);
        wallet.setBalance(newBalance);
        wallet.setCleanDate(currentTime);
        assertEquals("There should be one record updated", 1, walletMapper.update(wallet));
        wallet = walletMapper.get(wallet.getPlayerId(), wallet.getGameApiId());
        assertTrue("clean date should be close to current time",
                ChronoUnit.MINUTES.between(currentTime, wallet.getCleanDate()) < 1);
        assertEquals("balance should be modified", newBalance, wallet.getBalance());
    }

    @Test
    public void testSetWalletsToDirty() {
        Wallet wallet = walletMapper.get(playerTestId, gameApiTestId);
        wallet.setCleanDate(LocalDateTime.now());
        walletMapper.update(wallet);
        wallet = walletMapper.get(playerTestId, gameApiTestId);
        assertFalse("wallet should be clean now", wallet.isDirty());
        walletMapper.setGameWalletsToDirty(gameApiTestId, playerTestId);
        assertTrue("wallet should be dirty now", walletMapper.get(playerTestId, gameApiTestId).isDirty());
    }

    @Test
    public void adjustMainWalletTest() {
        Wallet originalWallet = walletMapper.getMainWallet(playerTestId);
        walletMapper.adjustMainWallet(playerTestId, EXPECTED_DEPOSIT_AMOUNT);
        Wallet newWallet = walletMapper.getMainWallet(playerTestId);
        assertEquals(originalWallet.getBalance().add(EXPECTED_DEPOSIT_AMOUNT), newWallet.getBalance());
    }

    @Test
    public void moveMainBalanceToPendingTest() {
        Wallet originalWallet = walletMapper.getMainWallet(playerTestId);
        walletMapper.moveMainBalanceToPending(playerTestId, EXPECTED_WITHDRAW_AMOUNT);
        Wallet newWallet = walletMapper.getMainWallet(playerTestId);
        assertEquals(originalWallet.getPendingWallet().add(EXPECTED_WITHDRAW_AMOUNT), newWallet.getPendingWallet());
        assertEquals(originalWallet.getBalance().subtract(EXPECTED_WITHDRAW_AMOUNT), newWallet.getBalance());
    }

    @Test
    public void getBalanceSumByPlayerIdTest() {
        BigDecimal totalBalance = walletMapper.getBalanceSumByPlayerId(playerTestId);
        assertTrue(totalBalance != null && totalBalance.compareTo(BigDecimal.ZERO) != 0);
    }

}
