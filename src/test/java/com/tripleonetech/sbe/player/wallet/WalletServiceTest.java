package com.tripleonetech.sbe.player.wallet;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.PlayerMessageMapper;
import com.tripleonetech.sbe.player.message.PlayerMessageQueryForm;
import com.tripleonetech.sbe.player.message.PlayerMessageView;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionQueryForm;
import com.tripleonetech.sbe.web.api.ApiReportController.WalletTransactionView;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WalletServiceTest extends BaseTest {

    private final static Logger logger = LoggerFactory.getLogger(WalletServiceTest.class);

    private static final int playerTestId_1 = 2;
    private static final int gameApiTestId = 1;
    @Autowired
    private WalletService walletService;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;

    private final BigDecimal EXPECTED_WITHDRAW_AMOUNT = BigDecimal.TEN;
    private final Integer operatorId = 1;
    private final String operatorUsername = "superadmin";

    @Test
    public void testAdjustMainWallet() {
        BigDecimal balance = walletMapper.getMainWallet(playerTestId_1).getBalance();
        walletService.depositToMainWallet(playerTestId_1, BigDecimal.TEN, "Unit Test", operatorId);
        assertEquals("balance should be deposited", walletMapper.getMainWallet(playerTestId_1).getBalance(),
                balance.add(BigDecimal.TEN));
    }

    @Test
    public void testDepositGame() throws InterruptedException {

        Player player = playerMapper.get(playerTestId_1);
        Wallet wallet = walletMapper.get(playerTestId_1, gameApiTestId);
        wallet = walletService.getCurrentBalance(player, wallet);
        BigDecimal mainBalance = walletMapper.getMainWallet(playerTestId_1).getBalance();
        BigDecimal gameBalance = wallet.getBalance();

        walletService.depositGame(playerTestId_1, gameApiTestId, BigDecimal.TEN, operatorId);

        BigDecimal newMainBalance = walletMapper.getMainWallet(playerTestId_1).getBalance();
        BigDecimal newGameBalance = walletService.getCurrentBalance(player, wallet).getBalance();
        assertEquals("Main balance should be deducted", newMainBalance, mainBalance.subtract(BigDecimal.TEN));
        assertEquals("Game balance should be added", newGameBalance, gameBalance.add(BigDecimal.TEN));
    }

    @Test
    public void testFreezeWithdrawAmount() {
        Wallet originalMainWallet = walletMapper.getMainWallet(playerTestId_1);
        walletService.freezeWithdrawAmount(playerTestId_1, BigDecimal.TEN);

        Wallet newMainWallet = walletMapper.getMainWallet(playerTestId_1);
        logger.info("Original pending wallet:[{}] , new pending wallet:[{}]", originalMainWallet.getPendingWallet(),
                newMainWallet.getPendingWallet());

        assertEquals(newMainWallet.getPendingWallet(),
                originalMainWallet.getPendingWallet().add(this.EXPECTED_WITHDRAW_AMOUNT));
        assertEquals(newMainWallet.getBalance(),
                originalMainWallet.getBalance().subtract(this.EXPECTED_WITHDRAW_AMOUNT));
    }

    @Test
    public void testUnfreezeWithdrawAmount() {
        // release amount : Approval
        walletMapper.moveMainBalanceToPending(playerTestId_1, this.EXPECTED_WITHDRAW_AMOUNT);
        Wallet originalMainWallet = walletMapper.getMainWallet(playerTestId_1);

        walletService.deductMainBalanceFromPending(playerTestId_1, this.EXPECTED_WITHDRAW_AMOUNT);
        Wallet newMainWallet = walletMapper.getMainWallet(playerTestId_1);
        logger.info("Relase APPROVAL - Original pending wallet:[{}] , new pending wallet:[{}]",
                originalMainWallet.getPendingWallet(), newMainWallet.getPendingWallet());
        assertEquals(newMainWallet.getPendingWallet(),
                originalMainWallet.getPendingWallet().subtract(this.EXPECTED_WITHDRAW_AMOUNT));

        // release amount : Reject
        walletMapper.moveMainBalanceToPending(playerTestId_1, this.EXPECTED_WITHDRAW_AMOUNT);
        originalMainWallet = walletMapper.getMainWallet(playerTestId_1);

        walletService.returnMainBalanceFromPending(playerTestId_1, this.EXPECTED_WITHDRAW_AMOUNT);
        newMainWallet = walletMapper.getMainWallet(playerTestId_1);
        logger.info("Relase REJECT - Original pending wallet:[{}] , new pending wallet:[{}]",
                originalMainWallet.getPendingWallet(), newMainWallet.getPendingWallet());
        logger.info("Relase REJECT - Original balance :[{}] , new balance :[{}]", originalMainWallet.getBalance(),
                newMainWallet.getBalance());
        assertEquals(newMainWallet.getPendingWallet(),
                originalMainWallet.getPendingWallet().subtract(this.EXPECTED_WITHDRAW_AMOUNT));
        assertEquals(newMainWallet.getBalance(), originalMainWallet.getBalance().add(this.EXPECTED_WITHDRAW_AMOUNT));

    }

    @Test
    public void testDepositToMainWallet_CheckSystemMessage() {
        BigDecimal amount = new BigDecimal("200");
        walletService.depositToMainWallet(playerTestId_1, amount);

        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        queryForm.setPlayerId(playerTestId_1);
        queryForm.setSort("updatedAt");
        queryForm.setAsc(false);
        PlayerMessageView messageView = playerMessageMapper.query(queryForm, 1, 1).get(0);
        assertEquals(playerTestId_1, messageView.getPlayerId().intValue());
        assertTrue(messageView.getContent().contains(amount.toString()));
    }

    @Test
    public void testWithdrawToMainWallet_CheckSystemMessage() {
        BigDecimal amount = new BigDecimal("100");
        walletService.withdrawFromMainWallet(playerTestId_1, amount, operatorId);

        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        queryForm.setPlayerId(playerTestId_1);
        queryForm.setSort("updatedAt");
        queryForm.setAsc(false);
        PlayerMessageView messageView = playerMessageMapper.query(queryForm, 1, 1).get(0);
        assertEquals(playerTestId_1, messageView.getPlayerId().intValue());
        assertTrue(messageView.getContent().contains(amount.toString()));
    }

    @Test
    public void testAddBalanceToMainWallet() {
        BigDecimal amount = new BigDecimal("100");
        BigDecimal oldBalance = walletService.getMainWalletBalance(playerTestId_1);

        walletService.addBalanceToMainWallet(playerTestId_1, amount, operatorUsername, operatorId);

        BigDecimal newBalance = walletService.getMainWalletBalance(playerTestId_1);
        assertEquals(oldBalance.add(amount).longValue(), newBalance.longValue());

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(playerTestId_1);
        queryForm.setType(WalletTransactionTypeEnum.ADD_BALANCE.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(datas.size() - 1);
        assertTrue(amount.compareTo(theTransation.getAmount()) == 0);
        assertEquals(operatorUsername, theTransation.getOperatorUsername());
        assertEquals(String.format("This transaction was made by %s.", operatorUsername), theTransation.getNote());

    }

    @Test
    public void testDeductBalanceFromMainWallet() {
        BigDecimal oldBalance = walletService.getMainWalletBalance(playerTestId_1);
        BigDecimal amount = new BigDecimal("100");

        walletService.deductBalanceFromMainWallet(playerTestId_1, amount, operatorUsername, operatorId);

        BigDecimal newBalance = walletService.getMainWalletBalance(playerTestId_1);
        assertEquals(oldBalance.subtract(amount).longValue(), newBalance.longValue());

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(playerTestId_1);
        queryForm.setType(WalletTransactionTypeEnum.DEDUCT_BALANCE.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(datas.size() - 1);
        assertTrue(amount.compareTo(theTransation.getAmount()) == 0);
        assertEquals(operatorUsername, theTransation.getOperatorUsername());
        assertEquals(String.format("This transaction was made by %s.", operatorUsername), theTransation.getNote());
    }
}
