package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.automation.AutomationJob;
import com.tripleonetech.sbe.automation.AutomationJobMapper;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayerTagService;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PlayerTagServiceTest extends BaseTest {

    @Autowired
    private PlayerTagService playerTagService;

    @Autowired
    private PlayerTagMapper playerTagMapper;

    @Autowired
    private AutomationJobMapper automationJobMapper;

    private int testTagId = 6;

    @Test
    public void testDeletePlayerTag() {
        PlayerTag originalTag = playerTagService.getTag(testTagId);

        playerTagService.deletePlayerTag(testTagId);
        assertNull("tag should be deleted", playerTagMapper.get(testTagId, false));
        AutomationJob automationJob = automationJobMapper.get(originalTag.getAutomationJobId());
        assertEquals(true, automationJob.isDeleted());
    }

    @Test
    public void testFailDeleteSystemDefaultGroupTag() {
        Boolean result = playerTagService.deletePlayerTag(playerTagMapper.getDefaultGroupId());
        assertFalse("Test failed, system default group tag should not be allowed to delete", result);
        assertNotNull("Test failed, system default group tag should not be allowed to delete",
                playerTagMapper.get(playerTagMapper.getDefaultGroupId(), true));
    }

    @Test
    public void testCreateGroup() {
        PlayerTag sampleGroup = playerTagMapper.get(1, true);

        String groupName = "test create group";
        sampleGroup.setName(groupName);
        Integer tagId = playerTagService.createGroup(sampleGroup);

        List<PlayerTag> playerGroups = playerTagMapper.listByNameLike(groupName);
        PlayerTag playerTag = playerGroups.get(playerGroups.size() - 1);
        assertEquals(groupName, playerTag.getName());
        assertEquals(tagId, playerTag.getId());
        assertTrue(playerTag.isPlayerGroup());
    }

    @Test
    public void testCreateTag() {
        PlayerTag sampleTag = playerTagMapper.get(6, false);

        String name = "test insert";
        sampleTag.setName(name);
        sampleTag.setPlayerGroup(false);
        Integer tagId = playerTagService.createTag(sampleTag);

        List<PlayerTag> playerTags = playerTagMapper.listByNameLike(name);
        assertNotNull("tag should be inserted", playerTags);
        assertTrue(playerTags.size() > 0);
        assertEquals(tagId, playerTags.get(playerTags.size() - 1).getId());
    }

    @Test
    public void testListAllTags() {
        List<PlayerTag> tags = playerTagService.listAllTags();
        assertNotNull("tag should be queried", tags);
        assertTrue(tags.size() > 0);
    }

    @Test
    public void testGetTag() {
        String tagName = "TestTag";
        PlayerTag tag = playerTagService.getTag(testTagId);
        assertNotNull("tag should be queried", tag);
        assertEquals("tag name should be " + tagName, tagName, tag.getName());
    }

    @Test
    public void testUpdate() {
        String tagName = "test update";
        PlayerTag originalTag = playerTagService.getTag(testTagId);
        PlayerTag tag = new PlayerTag();
        BeanUtils.copyProperties(originalTag, tag); // creates a copy of original tag
        tag.setName(tagName);
        tag.setAutomationJobId(2);
        playerTagService.updatePlayerTag(tag);

        List<PlayerTag> playerTags = playerTagMapper.listByNameLike(tagName);
        assertNotNull("tag should be updated", playerTags);
        assertTrue(playerTags.size() > 0);

        PlayerTag updatedTag = playerTagService.getTag(testTagId);
        assertEquals(tagName, updatedTag.getName());
        assertEquals(2, updatedTag.getAutomationJobId().intValue());
        AutomationJob automationJob = automationJobMapper.get(originalTag.getAutomationJobId());
        assertEquals(true, automationJob.isDeleted());
    }

    @Test
    public void testUpdate_JobIdIsNull() {
        PlayerTag originalTag = playerTagService.getTag(testTagId);
        PlayerTag tag = new PlayerTag();
        BeanUtils.copyProperties(originalTag, tag); // creates a copy of original tag
        tag.setAutomationJobId(null);
        playerTagService.updatePlayerTag(tag);

        PlayerTag updatedTag = playerTagService.getTag(testTagId);
        assertNull(updatedTag.getAutomationJobId());
        AutomationJob automationJob = automationJobMapper.get(originalTag.getAutomationJobId());
        assertEquals(true, automationJob.isDeleted());
    }

    @Test
    public void testDeletePlayerTagAutomation() {
        PlayerTag tag = playerTagService.getTag(testTagId);
        Integer originalAutomationJobId = tag.getAutomationJobId();

        playerTagService.deletePlayerTagAutomationJob(tag.getId());

        PlayerTag updatedTag = playerTagMapper.get(tag.getId(), tag.isPlayerGroup());
        assertNull(updatedTag.getAutomationJobId());
        AutomationJob job = automationJobMapper.get(originalAutomationJobId);
        assertTrue("The related job should be deleted", job.isDeleted());
    }

    @Test
    public void testSetDefaultTag() {
        int newDefaultGroupTagId = playerTagService.listAllGroups().get(0).getId();
        Boolean result = playerTagService.setDefaultTag(newDefaultGroupTagId);
        assertTrue("Failed setting default group tag", result);
        assertEquals("Failed matching the updated default group tag id", newDefaultGroupTagId,
                playerTagMapper.getDefaultGroupId().intValue());
    }

    @Test
    public void testFailUpdateDefaultGroupTagWithNonGroupTag() {
        int newDefaultGroupTagId = playerTagService.listAllTags().get(0).getId();
        Boolean result = playerTagService.setDefaultTag(newDefaultGroupTagId);
        assertFalse("Test failed! Non group tag should not able to set as the system default group tag", result);
        assertNotEquals("Test failed! Non group tag should not able to set as the system default group tag",
                newDefaultGroupTagId, playerTagMapper.getDefaultGroupId().intValue());
    }

}
