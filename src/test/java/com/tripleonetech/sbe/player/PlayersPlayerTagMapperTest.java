package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PlayersPlayerTagMapperTest extends BaseTest {

    private final boolean playerGroup = false;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;

    private int playerId = 1;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testListByPlayerId() {
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertNotNull("playersPlayerTags should be queried", playersPlayerTags);
        assertTrue(playersPlayerTags.size() > 0);
    }

    @Test
    public void testListSystemTagByPlayerIds() {
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listGroupByPlayerIds(Collections.singletonList(playerId));
        assertNotNull("playersPlayerTags should be queried", playersPlayerTags);
        assertTrue(playersPlayerTags.size() > 0);
    }

    @Test
    public void testListByPlayerTagId() {
        int playerTagId = 1;
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerTagId(playerTagId);
        assertNotNull("playersPlayerTags should be queried", playersPlayerTags);
        assertTrue(playersPlayerTags.size() > 0);
    }

    @Test
    public void testInsertMultiTagsWithOnePlayer() {
        Integer playerId = 2;
        Integer playerTagId = 7;

        // Before
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertFalse(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));

        // After
        playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, Collections.singletonList(playerTagId));
        playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));
    }

    @Test
    public void testInsertMultiPlayersWithOneTag() {
        Integer playerId = 2;
        Integer playerTagId = 7;

        // Before
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertFalse(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));

        // After
        playersPlayerTagMapper.insertMultiPlayersWithOneTag(Collections.singletonList(playerId), playerTagId);
        playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));
    }

    @Test
    public void testDelete() {
        Integer playerId = 2;
        Integer playerTagId = 6;

        // Before
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));

        // After
        playersPlayerTagMapper.delete(playerId, Collections.singletonList(playerTagId));
        playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertFalse(playersPlayerTags.stream().anyMatch(existing -> existing.getPlayerTagId().equals(playerTagId)));
    }

    @Test
    public void testGetPlayerIdHasAnyTagOf() {
        List<Integer> tagIds = Arrays.asList(6, 8);
        List<Integer> playerIds = playersPlayerTagMapper.getPlayerIdHasAnyTagOf(tagIds);

        // expected id = 1, 2 (4 was removed due to it being USD)
        Arrays.asList(1, 2).forEach(i -> {
            assertTrue(playerIds.contains(i));
        });
    }

    @Test
    public void testGetPlayerIdHasAllTagOf() {
        List<Integer> tagIds = Arrays.asList(6, 8);
        List<Integer> playerIds = playersPlayerTagMapper.getPlayerIdHasAllTagOf(tagIds);

        // expected id = 1, 2
        Arrays.asList(1, 2).forEach(i -> {
            assertTrue(playerIds.contains(i));
        });
    }

    @Test
    public void testGetPlayerIdNotAnyTagOf() {
        List<Integer> tagIds = Arrays.asList(6, 8);
        List<Integer> playerIds = playersPlayerTagMapper.getPlayerIdNotAnyTagOf(tagIds);

        // expected id = NOT 1, 2, 4
        Arrays.asList(1, 2, 4).forEach(i -> {
            assertFalse(playerIds.contains(i));
        });
    }
}
