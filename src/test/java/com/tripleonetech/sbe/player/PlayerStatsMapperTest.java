package com.tripleonetech.sbe.player;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlayerStatsMapperTest extends BaseTest {
    @Autowired
    private PlayerStatsMapper playerStatsMapper;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;

    private Integer playerId = 2;
    private List<Integer> playerIds = Arrays.asList(1, 2, 3);
    private WalletTransaction walletTransaction;
    private PromoBonus promoBonus;

    @Before
    public void setUp() throws Exception {
        walletTransaction = new WalletTransaction();
        walletTransaction.setAmount(new BigDecimal("11"));
        walletTransaction.setPlayerId(playerId);

        promoBonus = new PromoBonus();
        promoBonus.setBonusAmount(new BigDecimal("22"));
        promoBonus.setBonusDate(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS));
        promoBonus.setPlayerId(playerId);
        promoBonus.setRuleId(2);
        promoBonus.setStatus(BonusStatusEnum.APPROVED);
    }

    @Test
    public void testGetByPlayerId() {
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertEquals(playerId, playerStats.getPlayerId());
    }

    @Test
    public void testUpsertDirty() {
        playerStatsMapper.upsertDirty(playerIds, PlayerStatsTypeEnum.DEPOSIT_AMOUNT);
        playerStatsMapper.upsertDirty(playerIds, PlayerStatsTypeEnum.CASHBACK_BONUS);

        playerIds.forEach(playerId -> {
            PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
            assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.DEPOSIT_AMOUNT.getCode()) > 0);
            assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CASHBACK_BONUS.getCode()) > 0);
        });
    }

    @Test
    public void testCaculateDirtyWalletAmount_Deposit() throws InterruptedException {
        PlayerStats originalPlayerStats = playerStatsMapper.getByPlayerId(playerId);
        walletTransaction.setType(WalletTransactionTypeEnum.DEPOSIT);
        walletTransactionMapper.insert(walletTransaction);
        Thread.sleep(1000);

        PlayerStatsTypeEnum playerStatsType = PlayerStatsTypeEnum.DEPOSIT_AMOUNT;
        playerStatsMapper.calculateDirtyWalletAmount(playerStatsType);
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & playerStatsType.getCode()) == 0);
        assertTrue(Integer.compare(playerStats.getTotalDepositCount(),
                originalPlayerStats.getTotalDepositCount() + 1) == 0);
        assertTrue(playerStats.getTotalDepositAmount()
                .compareTo(originalPlayerStats.getTotalDepositAmount().add(walletTransaction.getAmount())) == 0);
        assertTrue(playerStats.getLastDepositDate().isAfter(originalPlayerStats.getLastDepositDate()));
    }

    @Test
    public void testCaculateDirtyWalletAmount_Withdraw() throws InterruptedException {
        PlayerStats originalPlayerStats = playerStatsMapper.getByPlayerId(playerId);
        walletTransaction.setType(WalletTransactionTypeEnum.WITHDRAWAL);
        walletTransactionMapper.insert(walletTransaction);
        Thread.sleep(1000);

        PlayerStatsTypeEnum playerStatsType = PlayerStatsTypeEnum.WITHDRAW_AMOUNT;
        playerStatsMapper.calculateDirtyWalletAmount(playerStatsType);
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & playerStatsType.getCode()) == 0);
        assertTrue(Integer.compare(playerStats.getTotalWithdrawCount(),
                originalPlayerStats.getTotalWithdrawCount() + 1) == 0);
        assertTrue(playerStats.getTotalWithdrawAmount()
                .compareTo(originalPlayerStats.getTotalWithdrawAmount().add(walletTransaction.getAmount())) == 0);
        assertTrue(playerStats.getLastWithdrawDate().isAfter(originalPlayerStats.getLastWithdrawDate()));
    }

    @Test
    public void testCaculateDirtyBonusAmount_Cashback() throws InterruptedException {
        PlayerStats originalPlayerStats = playerStatsMapper.getByPlayerId(playerId);
        promoBonus.setPromoType(PromoTypeEnum.CASHBACK);
        promoBonusMapper.insert(promoBonus);
        Thread.sleep(1000);

        PlayerStatsTypeEnum playerStatsType = PlayerStatsTypeEnum.CASHBACK_BONUS;
        playerStatsMapper.calculateDirtyBonusAmount(playerStatsType);
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & playerStatsType.getCode()) == 0);
        assertTrue(playerStats.getTotalCashbackBonus()
                .compareTo(originalPlayerStats.getTotalCashbackBonus().add(promoBonus.getBonusAmount())) == 0);
    }

    @Test
    public void testCaculateDirtyBonusAmount_Referral() throws InterruptedException {
        PlayerStats originalPlayerStats = playerStatsMapper.getByPlayerId(playerId);
        promoBonus.setPromoType(PromoTypeEnum.REFERRAL);
        promoBonusMapper.insert(promoBonus);
        Thread.sleep(1000);

        PlayerStatsTypeEnum playerStatsType = PlayerStatsTypeEnum.REFERRAL_BONUS;
        playerStatsMapper.calculateDirtyBonusAmount(playerStatsType);
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & playerStatsType.getCode()) == 0);
        assertTrue(playerStats.getTotalReferralBonus()
                .compareTo(originalPlayerStats.getTotalReferralBonus().add(promoBonus.getBonusAmount())) == 0);
    }

    @Test
    public void testCaculateDirtyBonusAmount_Campaign() throws InterruptedException {
        PlayerStats originalPlayerStats = playerStatsMapper.getByPlayerId(playerId);
        promoBonus.setPromoType(PromoTypeEnum.CAMPAIGN_TASK);
        promoBonusMapper.insert(promoBonus);
        Thread.sleep(1000);

        PlayerStatsTypeEnum playerStatsType = PlayerStatsTypeEnum.CAMPAIGN_BONUS;
        playerStatsMapper.calculateDirtyBonusAmount(playerStatsType);
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & playerStatsType.getCode()) == 0);
        assertTrue(playerStats.getTotalCampaignBonus()
                .compareTo(originalPlayerStats.getTotalCampaignBonus().add(promoBonus.getBonusAmount())) == 0);
    }

}
