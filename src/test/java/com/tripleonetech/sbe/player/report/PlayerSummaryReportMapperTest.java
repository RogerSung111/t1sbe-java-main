package com.tripleonetech.sbe.player.report;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PlayerSummaryReportMapperTest extends BaseTest {

    @Autowired
    private PlayerSummaryReportMapper mapper;

    @Test
    public void testList() {
        PlayerSummaryReportForm form = new PlayerSummaryReportForm();
        form.setDateStart(LocalDate.now().minusDays(2));
        form.setDateEnd(LocalDate.now().minusDays(1));
        form.setByDevice(true);
        List<PlayerSummaryReportView> list = mapper.list(form, form.getPage(), form.getLimit());

        assertEquals(10, list.size());

        Map<LocalDate, Map<DeviceTypeEnum, List<PlayerSummaryReportView>>> collect = list
                .stream()
                .collect(Collectors.groupingBy(PlayerSummaryReportView::getDate,
                        Collectors.groupingBy(PlayerSummaryReportView::getDevice)));

        assertEquals(2, collect.size());
        assertEquals(5, collect.get(LocalDate.now().minusDays(2)).size());
        assertEquals(5, collect.get(LocalDate.now().minusDays(1)).size());

    }

    @Test
    public void testGetLastTotalCount() {
        Integer lastTotalCount = mapper.getLastTotalCount(DeviceTypeEnum.COMPUTER);
        assertNotNull(lastTotalCount);
    }

    @Test
    public void testAddOneCount() {
        LocalDateTime timeHour = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);

        int count = mapper.addOneCount(timeHour, 5, DeviceTypeEnum.MOBILE);
        assertEquals(1, count);
    }

    @Test
    public void testAddCounts() {
        int lastHourTotalSum = 5;
        int registers = 3;
        LocalDateTime firstTestHour = LocalDateTime.now().withHour(12).truncatedTo(ChronoUnit.HOURS);

        for (int i = 0; i < registers; i++) {
            mapper.addOneCount(firstTestHour, lastHourTotalSum, DeviceTypeEnum.MOBILE);
        }

        PlayerSummaryReportForm form = new PlayerSummaryReportForm();
        form.setDateStart(firstTestHour.toLocalDate());
        form.setDateEnd(firstTestHour.toLocalDate());
        form.setTimezoneOffset(0);
        form.setByDevice(true);
        List<PlayerSummaryReportView> list = mapper.list(form, form.getPage(), form.getLimit());
        // lastTotalCount + 1 = 5, and insert 3times, lastTotalCount + 3 = 7
        assertEquals(7, list.get(0).getTotalCount());
    }

}
