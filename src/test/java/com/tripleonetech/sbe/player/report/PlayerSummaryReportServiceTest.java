package com.tripleonetech.sbe.player.report;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.player.PlayerCredential;

public class PlayerSummaryReportServiceTest extends BaseTest {

    @Autowired
    private PlayerSummaryReportService service;
    @Autowired
    private PlayerSummaryReportMapper mapper;

    @Test
    public void testUpsertCount() {
        PlayerCredential playerCredential = new PlayerCredential();
        int computerRegisters = 3;
        int mobileRegisters = 4;
        // add 1 day for isolate from V3 mock data
        LocalDateTime firstTestHour = LocalDateTime.now().plusDays(1).withHour(12);

        playerCredential.setRegisterDevice(DeviceTypeEnum.COMPUTER);
        playerCredential.setCreatedAt(firstTestHour);
        // 12 o'clock of yesterday, android, 4 players
        for (int i = 0; i < computerRegisters; i++) {
            service.upsertCount(playerCredential);
        }
        playerCredential.setCreatedAt(firstTestHour.plusHours(23));
        // 12 o'clock of today, android, 4 players
        for (int i = 0; i < computerRegisters; i++) {
            service.upsertCount(playerCredential);
        }
        playerCredential.setRegisterDevice(DeviceTypeEnum.MOBILE);
        // 12 o'clock of today, iOS, 3 players
        for (int i = 0; i < mobileRegisters; i++) {
            service.upsertCount(playerCredential);
        }
        playerCredential.setRegisterDevice(DeviceTypeEnum.COMPUTER);
        playerCredential.setCreatedAt(firstTestHour.plusHours(25));
        // 13 o'clock of today, android, 4 players
        for (int i = 0; i < computerRegisters; i++) {
            service.upsertCount(playerCredential);
        }

        // V3 data
        // last total_count of player_summary_report where device = 2
        int computerBaseTotalSum = 152;
        // last total_count of player_summary_report where device = 2
        int mobileBaseTotalSum = 147;
        PlayerSummaryReportForm form = new PlayerSummaryReportForm();
        // UTC
        form.setDateStart(firstTestHour.toLocalDate());
        form.setDateEnd(firstTestHour.toLocalDate().plusDays(2));
        form.setTimezoneOffset(0);
        form.setAsc(true);
        form.setByDevice(true);
        List<PlayerSummaryReportView> list = mapper.list(form, form.getPage(), form.getLimit());
        assertEquals(3, list.size());
        String firstDay = form.getDateStart().toString();
        String secondDay = form.getDateStart().plusDays(1).toString();
        assertEquals(firstDay, list.get(0).getDate().toString());
        assertEquals(computerBaseTotalSum + computerRegisters, list.get(0).getTotalCount());
        assertEquals(computerRegisters, list.get(0).getNewCount());
        assertEquals(secondDay, list.get(1).getDate().toString());
        assertEquals(computerBaseTotalSum + computerRegisters * 3, list.get(1).getTotalCount());
        assertEquals(computerRegisters * 2, list.get(1).getNewCount());
        assertEquals(secondDay, list.get(2).getDate().toString());
        assertEquals(mobileBaseTotalSum + mobileRegisters, list.get(2).getTotalCount());
        assertEquals(mobileRegisters, list.get(2).getNewCount());

        // +11:00
        String thirdDay = form.getDateStart().plusDays(2).toString();
        form.setTimezoneOffset(11);
        list = mapper.list(form, form.getPage(), form.getLimit());
        assertEquals(4, list.size());
        assertEquals(firstDay, list.get(0).getDate().toString());
        assertEquals(computerBaseTotalSum + computerRegisters, list.get(0).getTotalCount());
        assertEquals(computerRegisters, list.get(0).getNewCount());
        assertEquals(secondDay, list.get(1).getDate().toString());
        assertEquals(computerBaseTotalSum + computerRegisters * 2, list.get(1).getTotalCount());
        assertEquals(computerRegisters, list.get(1).getNewCount());
        assertEquals(secondDay, list.get(2).getDate().toString());
        assertEquals(mobileBaseTotalSum + mobileRegisters, list.get(2).getTotalCount());
        assertEquals(mobileRegisters, list.get(2).getNewCount());
        assertEquals(thirdDay, list.get(3).getDate().toString());
        assertEquals(computerBaseTotalSum + computerRegisters * 3, list.get(3).getTotalCount());
        assertEquals(computerRegisters, list.get(3).getNewCount());
        assertEquals(DeviceTypeEnum.COMPUTER, list.get(3).getDevice());
    }

}
