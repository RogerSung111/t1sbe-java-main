package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PlayerAnnouncementServiceTest extends BaseTest {
    @Autowired
    private PlayerAnnouncementService playerAnnouncementService;
    @Autowired
    private PlayerAnnouncementMapper playerAnnouncementMapper;
    @Autowired
    private PlayerMapper playerMapper;

    private static final String TEST_TITLE = "Unit test subject";
    private static final String TEST_ANNOUNCEMENT = "Unit test announcement";

    @Test
    public void testAddAndQueryAnnouncement() {
        PlayerAnnouncementForm announcementForm = new PlayerAnnouncementForm();
        announcementForm.setTitle(TEST_TITLE);
        announcementForm.setContent(TEST_ANNOUNCEMENT);
        announcementForm.setStartAt(ZonedDateTime.now());
        announcementForm.setEndAt(ZonedDateTime.now().plusDays(1));
        announcementForm.setPlayerGroupIds(Arrays.asList(1, 2, 3));
        int newId = playerAnnouncementService.create(announcementForm);

        PlayerAnnouncementDetailView newAnnouncement = playerAnnouncementMapper.getDetail(newId);
        assertNotNull(newAnnouncement);
        assertEquals(TEST_TITLE, newAnnouncement.getTitle());
        assertEquals(TEST_ANNOUNCEMENT, newAnnouncement.getContent());
        assertEquals(3, newAnnouncement.getPlayerGroups().size());
        assertEquals(0, newAnnouncement.getPlayerTags().size());
    }

    @Test
    public void testUpdateAndFindAnnouncement() {
        int testAnnouncementId = 1;

        PlayerAnnouncementForm announcementForm = new PlayerAnnouncementForm();
        announcementForm.setTitle(TEST_TITLE);
        announcementForm.setContent(TEST_ANNOUNCEMENT);
        announcementForm.setId(testAnnouncementId);
        announcementForm.setPlayerGroupIds(Arrays.asList(1, 2, 3));

        playerAnnouncementService.update(announcementForm);

        PlayerAnnouncementDetailView updatedAnnouncement = playerAnnouncementMapper.getDetail(testAnnouncementId);
        assertEquals(TEST_TITLE, updatedAnnouncement.getTitle());
        assertEquals(TEST_ANNOUNCEMENT, updatedAnnouncement.getContent());
        assertEquals(3, updatedAnnouncement.getPlayerGroups().size());
        assertEquals(0, updatedAnnouncement.getPlayerTags().size());
    }

    @Test
    public void testGetActiveAnnouncement() {
        Player player = playerMapper.get(2); // test002

        List<PlayerAnnouncementPlayerView> announcements = playerAnnouncementService.queryAvailable(player, new PageQueryForm());

        assertEquals("There should be exactly 3 announcements applicable to test002", 3, announcements.size());

    }

}
