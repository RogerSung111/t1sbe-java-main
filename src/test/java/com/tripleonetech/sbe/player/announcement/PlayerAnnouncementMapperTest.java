package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.tag.PlayerTagLogicalOperator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PlayerAnnouncementMapperTest extends BaseTest {
    @Autowired
    private PlayerAnnouncementMapper mapper;

    private PlayerAnnouncement announcement;

    @Before
    public void init() {
        announcement = new PlayerAnnouncement();
        announcement.setTitle("Unit test subject");
        announcement.setContent("Unit test announcement");
    }

    @Test
    public void testGet() {
        PlayerAnnouncementView announcement = mapper.getDetail(1);
        assertNotNull(announcement);
        assertEquals(2, announcement.getPlayerGroups().size());
    }

    @Test
    public void testInsert() {
        PlayerAnnouncementForm form = new PlayerAnnouncementForm();
        form.setTitle("Unit test subject");
        form.setContent("Unit test announcement");
        form.setStartAt(ZonedDateTime.now());

        PlayerAnnouncement announcement = PlayerAnnouncement.from(form);
        assertTrue(mapper.insert(announcement) > 0);
        PlayerAnnouncementQueryForm queryForm = new PlayerAnnouncementQueryForm();
        queryForm.setContent(announcement.getContent());
        PlayerAnnouncementView insertedItem =
                mapper.query(queryForm,
                        queryForm.getPage(),
                        queryForm.getLimit()
                ).get(0);
        assertNotNull("resultList should not be empty", insertedItem);
        assertNotNull(insertedItem.getId());
        assertEquals(insertedItem.getTitle(), announcement.getTitle());
    }


    @Test
    public void testUpdate() {
        int testAnnouncementId = 1;

        announcement.setId(testAnnouncementId);
        assertEquals(1, mapper.update(announcement));

        PlayerAnnouncementDetailView updatedAnnouncement = mapper
                .getDetail(testAnnouncementId);
        assertEquals(announcement.getTitle(), updatedAnnouncement.getTitle());
        assertEquals(announcement.getContent(), updatedAnnouncement.getContent());
    }

    @Test
    public void testQueryAvailable() {
        List<PlayerAnnouncementTagFiltered> announcementIds =
                mapper.queryAvailable(LocalDateTime.now().minusHours(2));
        assertTrue("Should have more than 1 announcements relevant", announcementIds.size() > 1);
        assertEquals(PlayerTagLogicalOperator.ANY, announcementIds.get(0).getPlayerTagOperator());
    }

}
