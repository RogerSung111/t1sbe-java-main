package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PlayerProfileMapperTest extends BaseTest {
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;

    @Test
    public void testGetProfileView() {
        PlayerMultiCurrencyProfileView profile = playerProfileMapper.getProfileViewByCredentialId(2);
        assertNotNull(profile);
        assertTrue(profile.getProfiles().size() > 1);
        assertTrue(profile.getBalances().size() > 1);
        assertEquals("test001", profile.getReferer().getUsername());

        PlayerMultiCurrency.ProfileView aProfile = profile.getProfiles().get(0);
        assertNotNull(aProfile.getLine());
        assertNotNull(aProfile.getSkype());
        assertNotNull(aProfile.getQq());
        assertNotNull(aProfile.getWechat());
        assertNotNull(aProfile.getCurrency());
        assertNotNull(aProfile.getStatus());
        assertNotNull(aProfile.isCashbackEnabled());
        assertNotNull(aProfile.isCampaignEnabled());
        assertNotNull(aProfile.getRisk());
        assertNotNull(aProfile.getCreatedAt());
        assertNotNull(aProfile.getGroup());
        assertNotNull(aProfile.getTags());
        assertTrue(aProfile.getTags().size() > 0);
    }

    @Test
    public void testGetPlayerProfile() {
        int playerId = 1;
        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(playerId);
        assertNotNull(playerProfile);
        assertNotNull(playerProfile.getId());
        assertEquals("Player IDs should be matched", 1, playerProfile.getId());
    }

    @Test(expected = DuplicateKeyException.class)
    public void testInsertPlayerProfileFail() {
        int playerId = 2; // Duplicated player id
        PlayerProfile playerProfile = new PlayerProfile();
        playerProfile.setPlayerId(playerId);
        playerProfile.setFirstName("testInsertFirstName");
        playerProfile.setLastName("testInsertLastName");
        playerProfileMapper.insert(playerProfile);
    }

    @Test
    public void testInsertPlayerProfile() {
        PlayerCredential modkCredential = new PlayerCredential();
        modkCredential.setUsername("test002test002");
        modkCredential.setPassword("123456");
        modkCredential.setReferralCode("1234567");
        modkCredential.setEmailVerified(false);
        playerCredentialMapper.insert(modkCredential);

        Player mockPlayer = new Player();
        mockPlayer.setUsername(modkCredential.getUsername());
        mockPlayer.setCurrency(SiteCurrencyEnum.IDR);
        mockPlayer.setStatus(PlayerStatusEnum.ACTIVE);

        playerMapper.insert(mockPlayer);
        int playerId = mockPlayer.getId();

        PlayerProfile playerProfile = new PlayerProfile();

        playerProfile.setPlayerId(playerId);
        playerProfile.setFirstName("testInsertFirstName");
        playerProfile.setLastName("testInsertLastName");
        playerProfile.setBirthday(LocalDate.now());
        playerProfile.setCountryCode("TWN");
        playerProfile.setLanguage("testInsertLanguage");
        playerProfile.setGender("M");
        playerProfile.setAddress("testInsertAddress");
        playerProfile.setCity("testInsertCity");
        playerProfile.setCountryPhoneCode("+866");
        playerProfile.setLine("testInsertLineMessenger");
        playerProfile.setSkype("testInsertSkypeMessenger");
        playerProfile.setQq("testInsertQqMessenger");
        playerProfile.setWechat("testInsertWechatMessenger");
        playerProfile.setPhoneNumber("123546789");
        playerProfileMapper.insert(playerProfile);

        playerProfile = playerProfileMapper.getByPlayerId(playerId);
        assertEquals("Player IDs should be matched", playerId, playerProfile.getPlayerId());
        assertEquals("player FirstName should be matched", "testInsertFirstName", playerProfile.getFirstName());
        assertTrue("Player should be inserted as a new record", playerProfile.getId() > 1);
    }

    @Test
    public void testUpdate() {
        int playerId = 2;

        PlayerProfile playerProfile = new PlayerProfile();
        playerProfile.setPlayerId(playerId);
        playerProfile.setFirstName("testFirstNameUpdated");
        playerProfile.setLastName("testLastNameUpdated");
        playerProfile.setBirthday(LocalDate.now());
        playerProfile.setCountryCode("TWN");
        playerProfile.setLanguage("testLanguageUpdated");
        playerProfile.setGender("M");
        playerProfile.setAddress("testAddressUpdated");
        playerProfile.setCity("testCityUpdated");
        playerProfile.setCountryPhoneCode("+866");
        playerProfile.setLine("testLineMessengerUpdated");
        playerProfile.setSkype("testSkypeMessengerUpdated");
        playerProfile.setQq("testQqMessengerUpdated");
        playerProfile.setWechat("testWechatMessengerUpdated");
        playerProfile.setPhoneNumber("123546789");
        int count = playerProfileMapper.update(playerProfile);
        assertEquals("Player should be updated", 1, count);

        PlayerProfile updatedProfile = playerProfileMapper.getByPlayerId(playerId);
        assertEquals("First name field should be updated", "testFirstNameUpdated", updatedProfile.getFirstName());
        assertNotNull("Last name field should not be updated or set null", updatedProfile.getLastName());
    }

    @Test
    public void testGetPlayerInfo() {
        int playerId = 2;
        PlayerInfoView playerInfo = playerProfileMapper.getPlayerInfo(playerId);

        assertNotNull(playerInfo);
        assertNotNull(playerInfo.getCurrency());
        assertNotNull(playerInfo.getGroup());
        assertNotNull(playerInfo.getGroup().getName());
        assertTrue(playerInfo.isWithdrawPasswordExists());

        List<Player> newPlayers = playerMapper.listByUsername("testfornewplayer");
        newPlayers.forEach(p -> {
            PlayerInfoView playerInfo2 = playerProfileMapper.getPlayerInfo(p.getId());
            assertNotNull(playerInfo2);
        });
    }
}
