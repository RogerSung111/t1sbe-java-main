package com.tripleonetech.sbe.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.AlphanumericGenUtil;

public class PlayerCredentialMapperTest extends BaseTest {

    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGet() {
        String username = "test002";
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(username);

        assertEquals(username, playerCredential.getUsername());
        assertTrue(playerCredential.validatePassword("123456"));
    }

    @Test
    public void testInsert() {
        int referralCodeLength = 7;
        
        String referralCode = AlphanumericGenUtil.generateAlphanumericCode(referralCodeLength);
        PlayerCredential mockCredential = new PlayerCredential();
        mockCredential.setUsername("testAccountForUnitTest");
        mockCredential.setPassword("123456");
        mockCredential.setReferralCode(referralCode);
        mockCredential.setEmailVerified(false);
        playerCredentialMapper.insert(mockCredential);

        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(mockCredential.getUsername());
        assertEquals(mockCredential.getUsername(), playerCredential.getUsername());
        assertTrue(playerCredential.validatePassword(mockCredential.getPassword()));
        assertEquals(playerCredential.getReferralCode(), referralCode);
    }

    @Test
    public void testUpdatePasswordEncrypt() {
        String mockUsername = "test002";
        String mockPassword = "testPasswordForUnitTest";

        // Before update
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(mockUsername);
        assertTrue(playerCredential.validatePassword("123456"));

        // After update
        playerCredential.setPassword(mockPassword);
        playerCredentialMapper.updatePasswordEncrypt(mockUsername, playerCredential.getPasswordEncrypt());
        playerCredential = playerCredentialMapper.getByUsername(mockUsername);
        assertTrue(playerCredential.validatePassword(mockPassword));

    }

    @Test
    public void testUpdateVerificationInfo() {
        String mockUsername = "test002";
        PlayerVerificationQuestionForm form = new PlayerVerificationQuestionForm();
        form.setVerificationQuestionId(1);
        form.setVerificationAnswer("都喜歡");
        playerCredentialMapper.updateVerificationInfo(mockUsername, form);

        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(mockUsername);
        assertEquals(form.getVerificationQuestionId(), playerCredential.getVerificationQuestionId());
        assertEquals(form.getVerificationAnswer(), playerCredential.getVerificationAnswer());
    }

}
