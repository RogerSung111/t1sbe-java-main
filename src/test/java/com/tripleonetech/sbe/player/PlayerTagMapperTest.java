package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PlayerTagMapperTest extends BaseTest {

    private final boolean playerGroup = false;

    @Autowired
    private PlayerTagMapper playerTagMapper;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;

    private PlayerTag newTag;

    private int testTagId = 6;

    @Before
    public void setUp() throws Exception {
        newTag = new PlayerTag();
        newTag.setName("Sample tag");
        newTag.setAutomationJobId(1);
    }

    @Test
    public void testDelete() {
        Boolean result = playerTagMapper.delete(testTagId);
        assertTrue("Failed deleting tag", result);

        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerTagId(testTagId);
        assertEquals("playersPlayerTags with tag id is " + testTagId + " should be deleted, too",
                0, playersPlayerTags.size());
    }

    @Test
    public void testInsert() {
        newTag.setName("test insert");
        int count = playerTagMapper.insert(newTag);
        assertEquals("tag should be inserted", 1, count);
    }

    @Test
    public void testGetGroupByPlayerId() {
        int playerId = 1;
        PlayerTag playerTag = playerTagMapper.getGroupByPlayerId(playerId);
        assertNotNull("tag should be queried", playerTag);
        assertEquals("Automation Job ID exists for this group", 4, playerTag.getAutomationJobId().intValue());
    }

    @Test
    public void testListAll() {
        List<PlayerTag> tags = playerTagMapper.listAll(playerGroup);
        assertNotNull("tag should be queried", tags);
        assertTrue(tags.size() > 0);
    }

    @Test
    public void testListPlayerTagsByIds() {
        List<PlayerTag> tags = playerTagMapper.listPlayerTagsByIds(Collections.singletonList(testTagId));
        assertNotNull("tag should be queried", tags);
        assertTrue(tags.size() > 0);
    }

    @Test
    public void testListByNameLike() {
        String queryName = "silver";
        List<PlayerTag> tags = playerTagMapper.listByNameLike(queryName);
        assertNotNull("tag should be queried", tags);
        assertTrue("tag should be queried", tags.size() > 0);
        assertTrue("tag should be start with '" + queryName + "'",
                tags.get(0).getName().regionMatches(true, 0, queryName, 0, queryName.length()));
    }

    @Test
    public void testGet() {
        String tagName = "TestTag";
        PlayerTag tag = playerTagMapper.get(testTagId, playerGroup);
        assertNotNull("tag should be queried", tag);
        assertEquals("tag name should be " + tagName, tagName, tag.getName());
        assertNotNull("Should have automation job id", tag.getAutomationJobId());
    }

    @Test
    public void testUpdate() {
        newTag.setId(testTagId);
        newTag.setName("test update");
        Boolean result = playerTagMapper.update(newTag);
        assertEquals("Tag should be updated", true, result);
    }

    @Test
    public void testDeleteAutomationJob() {
        int count = playerTagMapper.deleteAutomationJob(testTagId);
        assertEquals("Tag should be updated", 1, count);

        PlayerTag updatedTag = playerTagMapper.get(testTagId);
        assertNull("Automation Job ID should have been deleted for this given tag", updatedTag.getAutomationJobId());
    }

    @Test
    public void testGetDefaultTagId() {
        int defaultTagId = playerTagMapper.getDefaultGroupId();
        assertEquals("Default tag id should be: 5", 5, defaultTagId);
    }
}
