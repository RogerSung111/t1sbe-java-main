package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PlayerBankAccountMapperTest extends BaseTest{

    private PlayerBankAccount playerBankAccount;
    @Autowired
    private PlayerBankAccountMapper playerBankAccountMapper;
    
    @Before
    public void init(){
        playerBankAccount = new PlayerBankAccount();

        playerBankAccount.setBankId(1);
        playerBankAccount.setPlayerId(1);
        playerBankAccount.setDefaultAccount(true);
        playerBankAccount.setAccountNumber("0000111122225555");
        playerBankAccount.setBankBranch("test");
    }

    @Test
    public void testInsert(){
        playerBankAccountMapper.insert(playerBankAccount);
        assertNotNull(playerBankAccount.getId());
    }

    @Test
    public void testSetDefault(){
        playerBankAccountMapper.setDefault(2, playerBankAccount.getPlayerId());
        assertFalse(playerBankAccountMapper.get(1).isDefaultAccount());
        assertTrue(playerBankAccountMapper.get(2).isDefaultAccount());
    }

    @Test
    public void testDelete(){
        playerBankAccountMapper.delete(1);
        assertNotNull("Deleted account should still be accessible via direct id", playerBankAccountMapper.get(1));
    }

    @Test
    public void testListByPlayerId(){
        List<PlayerBankAccount> playerAccounts = playerBankAccountMapper.listByPlayerId(2);
        assertEquals("Player id = 2 should have 3 bank accounts", 3, playerAccounts.size());
    }

    @Test
    public void testListByPlayerCredential() {
        List<PlayerBankAccount> playerAccounts = playerBankAccountMapper.listByPlayerCredential(2, SiteCurrencyEnum.CNY);
        assertNotNull(playerAccounts);
        assertEquals("Player id = 2 - CNY should have 3 bank accounts",3, playerAccounts.size());

        PlayerBankAccount account = playerAccounts.get(0);
        assertNotNull(account.getId());
        assertNotNull(account.getPlayerId());
        assertNotNull(account.getBankId());
        assertNotNull(account.getAccountNumber());
        assertNotNull(account.isDefaultAccount());
    }

    @Test
    public void testGetForPlayerCredential() {
        PlayerBankAccount account =
                playerBankAccountMapper.getForPlayerCredential(2, SiteCurrencyEnum.CNY, 2);
        assertNotNull(account);
        assertNotNull(account.getId());
        assertNotNull(account.getPlayerId());
        assertNotNull(account.getBankId());
        assertNotNull(account.getAccountNumber());
        assertNotNull(account.isDefaultAccount());

        account =
                playerBankAccountMapper.getForPlayerCredential(2, SiteCurrencyEnum.USD, 2);
        assertNull(account);
    }
}
