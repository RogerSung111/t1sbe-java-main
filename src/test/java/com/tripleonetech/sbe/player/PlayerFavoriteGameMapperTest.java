package com.tripleonetech.sbe.player;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class PlayerFavoriteGameMapperTest extends BaseTest {

    @Autowired
    private PlayerFavoriteGameMapper playerFavoriteGameMapper;

    @Test
    public void testInsert() {
        PlayerFavoriteGame obj = new PlayerFavoriteGame();
        obj.setPlayerId(2);
        obj.setGameApiId(5);
        obj.setGameCode("BO2P");
        playerFavoriteGameMapper.insert(obj);

        List<PlayerFavoriteGame> list = playerFavoriteGameMapper.listByPlayer(2);
        assertTrue(list.stream().anyMatch(
                e -> (obj.getGameApiId().equals(e.getGameApiId()) && obj.getGameCode().equals(e.getGameCode()))));
    }

    @Test
    public void testDelete() {
        PlayerFavoriteGame obj = new PlayerFavoriteGame();
        obj.setPlayerId(2);
        obj.setGameApiId(5);
        obj.setGameCode("AV01");
        playerFavoriteGameMapper.delete(obj);

        List<PlayerFavoriteGame> list = playerFavoriteGameMapper.listByPlayer(2);
        assertFalse(list.stream().anyMatch(
                e -> (obj.getGameApiId().equals(e.getGameApiId()) && obj.getGameCode().equals(e.getGameCode()))));
    }

    @Test
    public void testListByPlayer() {
        List<PlayerFavoriteGame> list = playerFavoriteGameMapper.listByPlayer(2);
        assertEquals(26, list.size());
        list.forEach(obj -> assertEquals(2, obj.getPlayerId().intValue()));
    }

}
