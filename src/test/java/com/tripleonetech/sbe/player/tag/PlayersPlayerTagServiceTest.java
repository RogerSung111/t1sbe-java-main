package com.tripleonetech.sbe.player.tag;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.tripleonetech.sbe.BaseTest;

public class PlayersPlayerTagServiceTest extends BaseTest {

    @Autowired
    private PlayersPlayerTagService playersPlayerTagService;
    
    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;
    
    private final static int PLAYER_ID_1 = 1;

    @Test
    public void testUpdatePlayersPlayerTag() {
        List<Integer> newTagIds = new ArrayList<>(Arrays.asList(6, 7));
        playersPlayerTagService.updatePlayersPlayerTag(newTagIds, PLAYER_ID_1);
        
        List<PlayersPlayerTag> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(PLAYER_ID_1, false);
        assertTrue(playerTags.size() == newTagIds.size());
        
        List<Integer> currentTagIds = playerTags.stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());
        assertTrue(currentTagIds.equals(newTagIds));
    }
    
    @Test
    public void testUpdatePlayersPlayerTagShouldBeRemovedAll() {
        List<Integer> newTagIds = null;
        playersPlayerTagService.updatePlayersPlayerTag(newTagIds, PLAYER_ID_1);
        
        List<PlayersPlayerTag> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(PLAYER_ID_1, false);
        assertTrue("Player tags should be empty", CollectionUtils.isEmpty(playerTags));
        
    }
    
    @Test
    public void testUpdatePlayersPlayerTagWrongTagId() {
        List<Integer> newTagIds = Arrays.asList(1);
        List<PlayersPlayerTag> originalPlayersPlayTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(PLAYER_ID_1, false);
        playersPlayerTagService.updatePlayersPlayerTag(newTagIds, PLAYER_ID_1);
        
        List<PlayersPlayerTag> newPlayersPlayTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(PLAYER_ID_1, false);
        assertTrue("Player tags should not be updated", newPlayersPlayTags.equals(originalPlayersPlayTags));
        
    }
    
    
    @Test
    public void testUpdatePlayersPlayerGroup() {
        int newGroupId = 2;
        playersPlayerTagService.updatePlayersPlayerGroup(newGroupId, PLAYER_ID_1);
        PlayersPlayerTag group = playersPlayerTagMapper.listGroupByPlayerIds(Arrays.asList(PLAYER_ID_1)).get(0);
        
        assertNotNull(group);
        assertTrue(group.getPlayerTagId().equals(newGroupId));
    }
    
    @Test
    public void testUpdatePlayersPlayerGroupWrongGroupId() {
        PlayersPlayerTag originalPlayerGroup = playersPlayerTagMapper.listGroupByPlayerIds(Arrays.asList(PLAYER_ID_1)).get(0);
        assertNotNull(originalPlayerGroup);
        
        int newGroupId = 7;
        playersPlayerTagService.updatePlayersPlayerGroup(newGroupId, PLAYER_ID_1);
        PlayersPlayerTag group = playersPlayerTagMapper.listGroupByPlayerIds(Arrays.asList(PLAYER_ID_1)).get(0);
        
        assertNotNull(group);
        assertTrue("Player group should not be updated", originalPlayerGroup.getPlayerTagId().equals(group.getPlayerTagId()));
    }
}
