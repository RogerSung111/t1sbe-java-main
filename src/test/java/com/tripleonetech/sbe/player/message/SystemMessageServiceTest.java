package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Deposit.Properties.*;
import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.DEPOSIT;
import static org.junit.Assert.assertEquals;

public class SystemMessageServiceTest extends BaseTest {
    private int playerId;
    private String expectedContent;
    private String testSubject;
    private PlayerMessageTemplate.Type testType;
    private Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params;

    @Autowired
    private PlayerMessageMapper playerMessageMapper;

    @Autowired
    private SystemMessageService systemMessageService;

    @Before
    public void init() {
        playerId = 2;
        expectedContent = "Deposit template, deposit amount is 1000";
        testSubject = "Dear test002, Your Deposit request";
        testType = DEPOSIT;

        params = new HashMap<>();
        params.put(AMOUNT, 1000);
        params.put(PLAYER_NAME, "test002");
    }

    @Test
    public void testSendSystemMessage() {
        systemMessageService.sendSystemMessage(testType, params, Collections.singletonList(playerId));

        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        queryForm.setSubject(testSubject);

        List<PlayerMessageView> playerMessageViews = playerMessageMapper.query(queryForm, 1, 10);
        assertEquals("playerMessageViews size should be only 1", 1, playerMessageViews.size());

        String actualContent = playerMessageViews.get(0).getContent();
        assertEquals(expectedContent, actualContent);
    }

}
