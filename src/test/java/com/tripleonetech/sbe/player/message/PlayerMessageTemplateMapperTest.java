package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class PlayerMessageTemplateMapperTest extends BaseTest {

    @Autowired
    private PlayerMessageTemplateSettingMapper playerMessageTemplateSettingMapper;

    private PlayerMessageTemplate.Type testType;

    @Before
    public void init() {
        testType = PlayerMessageTemplate.Type.DEPOSIT;
    }

    @Test
    public void testGetByType() {
        PlayerMessageTemplateSetting playerMessageTemplateSetting = playerMessageTemplateSettingMapper.getByType(testType);
        assertNotNull(playerMessageTemplateSetting);
        assertEquals("Deposit template, deposit amount is [AMOUNT]", playerMessageTemplateSetting.getContent());
    }

    @Test
    public void testListAll() {
        List<PlayerMessageTemplateSetting> playerMessageTemplateSettings = playerMessageTemplateSettingMapper.listAll();
        assertNotNull(playerMessageTemplateSettings);
        assertTrue(playerMessageTemplateSettings.size() > 0);
    }

    @Test
    public void testUpdate() {
        String testContent = "test update content";
        String testSubject = "test update subject";

        PlayerMessageTemplateSetting playerMessageTemplateSetting = playerMessageTemplateSettingMapper.getByType(testType);
        playerMessageTemplateSetting.setSubject(testSubject);
        playerMessageTemplateSetting.setContent(testContent);
        int update = playerMessageTemplateSettingMapper.update(playerMessageTemplateSetting);
        assertEquals(1, update);

        playerMessageTemplateSetting = playerMessageTemplateSettingMapper.getByType(testType);
        assertEquals("subject should be " + testSubject , testSubject, playerMessageTemplateSetting.getSubject());
        assertEquals("content should be " + testContent , testContent, playerMessageTemplateSetting.getContent());
    }
}
