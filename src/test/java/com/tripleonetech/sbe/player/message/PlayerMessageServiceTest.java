package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;

public class PlayerMessageServiceTest extends BaseTest {
    @Autowired
    private PlayerMessageService playerMessageService;

    private static final Integer PLAYER_ID = 2;
    private static final Integer OPERATOR_ID = 2;
    private static final Integer TEST_MESSAGE_ID = 1;
    private static final String TEST_SUBJECT = "Unit test subject";
    private static final String TEST_MESSAGE = "Unit test message";

    @Test
    public void testSendAndListMessage() {
        PlayerMessage message = new PlayerMessage();
        message.setOperatorId(OPERATOR_ID);
        message.setPlayerId(PLAYER_ID);
        message.setContent(TEST_MESSAGE);
        message.setSubject(TEST_SUBJECT);

        playerMessageService.sendMessage(message);
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setOperatorId(OPERATOR_ID);
        form.setPlayerId(PLAYER_ID);
        form.setSubject(TEST_SUBJECT);
        PlayerMessageView playerMessageView = playerMessageService.listMessage(form).get(0);

        assertNotNull(playerMessageView);
        assertEquals(OPERATOR_ID, playerMessageView.getOperatorId());
        assertEquals(PLAYER_ID, playerMessageView.getPlayerId());
        assertEquals(TEST_SUBJECT, playerMessageView.getSubject());
        assertEquals(TEST_MESSAGE, playerMessageView.getContent());
        assertFalse("Messages between player and operator should have system flag set to false", playerMessageView.isSystem());
    }

    @Test
    public void testDeleteAndListMessage() {
        playerMessageService.deleteMessage(TEST_MESSAGE_ID);
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setId(TEST_MESSAGE_ID);
        PlayerMessageView playerMessageView = playerMessageService.listMessage(form).get(0);
        assertTrue(playerMessageView.isSystem());
    }

    @Test
    public void testBatchDeleteAndListMessage() {
        playerMessageService.batchDeleteMessage(Collections.singletonList(TEST_MESSAGE_ID));
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setId(TEST_MESSAGE_ID);
        PlayerMessageView playerMessageView = playerMessageService.listMessage(form).get(0);
        assertTrue(playerMessageView.isDeleted());
    }
}
