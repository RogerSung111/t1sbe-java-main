package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlayerMessageMapperTest extends BaseTest {
    @Autowired
    private PlayerMessageMapper playerMessageMapper;
    private PlayerMessage playerMessage = new PlayerMessage();

    @Before
    public void init() {
        playerMessage.setPlayerId(2);
        playerMessage.setSubject("PlayerMessageMapperTest Subject");
        playerMessage.setContent("PlayerMessageMapperTest Content");
        playerMessage.setSystem(false);
        playerMessage.setSender((byte) 1);
    }

    @Test
    public void testGetUnreadCount() {
        int unreadCount = playerMessageMapper.getUnreadCount(2);
        assertEquals(4, unreadCount);
    }

    @Test
    public void queryThreads() {
        PlayerMessageThreadListQueryForm queryForm = new PlayerMessageThreadListQueryForm();
        queryForm.setCreatedAtStart(ZonedDateTime.now().minusDays(2));
        queryForm.setCreatedAtEnd(ZonedDateTime.now());
        queryForm.setOperatorId(1);

        List<PlayerMessageThreadListView> threads = playerMessageMapper.queryThreads(queryForm,
                queryForm.getPage(), queryForm.getLimit());
        assertNotNull(threads);
        assertTrue(threads.size() > 0);

        PlayerMessageThreadListView aThread = threads.get(0);
        assertNotNull(aThread.getSubject());
        assertNotNull(aThread.getPlayer());
        assertNotNull(aThread.getOperator());
        assertEquals(1, aThread.getOperator().getId());
    }

    @Test
    public void queryThreadsWithMessages() {
        PlayerMessageThreadQueryForm queryForm = new PlayerMessageThreadQueryForm();
        queryForm.setCreatedAtStart(ZonedDateTime.now().minusDays(2));
        queryForm.setCreatedAtEnd(ZonedDateTime.now());
        queryForm.setPlayerId(2);

        List<PlayerMessageThreadView> threads = playerMessageMapper.queryThreadsWithMessages(queryForm);
        assertNotNull(threads);
        assertTrue(threads.size() > 0);

        PlayerMessageThreadView aThread = threads.get(0);
        assertTrue(aThread.isUnread());
        assertNotNull(aThread.getSubject());
        assertTrue(aThread.getMessages().size() > 0);
    }

    @Test
    public void getThread() {
        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        queryForm.setThreadId(2);

        List<PlayerMessageView> messages = playerMessageMapper.query(queryForm, 1, 99);
        assertNotNull(messages);
        assertTrue(messages.size() > 0);

        PlayerMessageView aMessage = messages.get(0);
        assertFalse(aMessage.isRead());
    }

    @Test
    public void testInsertAndQuery() {
        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        assertTrue(playerMessageMapper.insert(playerMessage) > 0);
        queryForm.setSubject("PlayerMessageMapperTest");
        PlayerMessageView insertedItem = playerMessageMapper.query(queryForm, queryForm.getPage(),
                queryForm.getLimit()).get(0);
        assertTrue("resultList should not be empty", insertedItem != null);
        assertEquals((int) insertedItem.getPlayerId(), 2);
        assertEquals(insertedItem.getSubject(), "PlayerMessageMapperTest Subject");
    }

    @Test
    public void testDelete() {
        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        assertEquals("Deleting the message whose ID = 1 should succeed", 1, playerMessageMapper.setDeleted(1));
        queryForm.setId(1);
        PlayerMessageView deletedMessage = playerMessageMapper
                .query(queryForm, queryForm.getPage(), queryForm.getLimit())
                .get(0);
        assertEquals(deletedMessage.isDeleted(), true);
    }
}
