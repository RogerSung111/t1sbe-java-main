package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.privilege.SiteConfigs;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PlayerServiceTest extends BaseTest {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private SiteConfigs siteConfigs;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;

    private Player newPlayer;
    private final String referralCodeOfPlayerTest001 = "abc1def";

    @Before
    public void setUp() throws Exception {
        newPlayer = new Player();
        newPlayer.setUsername("test002");
        newPlayer.setPassword("123456");
        newPlayer.setCurrency(SiteCurrencyEnum.CNY);
        newPlayer.setReferralCode(referralCodeOfPlayerTest001);
    }

    @Test
    public void testRegister() {
        
        PlayerCredential referrer = playerCredentialMapper.getByReferrerCode(referralCodeOfPlayerTest001);
        
        newPlayer.setUsername("test002-noDuplicate");

        // Before
        PlayerCredential credential = playerCredentialMapper.getByUsername(newPlayer.getUsername());
        assertNull(credential);

        // After
        PlayerNewForm form = new PlayerNewForm();
        form.setUsername(newPlayer.getUsername());
        form.setCurrency(newPlayer.getCurrency());
        form.setReferralCode(newPlayer.getReferralCode());
        Integer credentialId = playerService.register(form, newPlayer.getPassword(), DeviceTypeEnum.MOBILE, "127.0.0.1");
        PlayerCredential newCredential = playerCredentialMapper.getByUsername(newPlayer.getUsername());
        assertNotNull(newCredential.getReferralCode());
        assertEquals(newCredential.getReferralPlayerCredentialId(), referrer.getId());
        assertEquals(DeviceTypeEnum.MOBILE, newCredential.getRegisterDevice());
        assertEquals("127.0.0.1", newCredential.getRegisterIp());
        assertEquals(credentialId, newCredential.getId());
        
        List<SiteCurrencyEnum> siteCurrencies = siteConfigs.getSiteCurrencies();
        for (SiteCurrencyEnum siteCurrency : siteCurrencies) {
            Player player = playerMapper.getByUniqueKey(newPlayer.getUsername(), siteCurrency);
            assertEquals(newPlayer.getUsername(), player.getUsername());
            assertEquals(siteCurrency, player.getCurrency());
            assertTrue(newPlayer.validatePassword(player.getPassword()));
        }
    }

    @Test
    public void testRecordLogin() {
        ZonedDateTime currentDate = ZonedDateTime.now();

        // Before
        Player player = playerMapper.getByUniqueKey(newPlayer.getUsername(), newPlayer.getCurrency());
        assertTrue(currentDate.isAfter(player.getLastLoginTime()));

        // After
        playerService.recordLogin(player);
        player = playerMapper.getByUniqueKey(newPlayer.getUsername(), newPlayer.getCurrency());
        assertTrue(currentDate.isBefore(player.getLastLoginTime()));
    }

    @Test
    public void testUpdateStatus() {
        PlayerStatusEnum mockStatus = PlayerStatusEnum.BLOCKED;

        // Before
        Player player = playerMapper.getByUniqueKey(newPlayer.getUsername(), newPlayer.getCurrency());
        assertNotEquals(mockStatus, player.getStatus());

        // After
        playerService.updateStatus(player.getId(), mockStatus);
        player = playerMapper.getByUniqueKey(newPlayer.getUsername(), newPlayer.getCurrency());
        assertEquals(mockStatus, player.getStatus());
    }

    @Test
    public void testUserExists_Exists_Success() {
        boolean hasUsername = playerService.userExists(newPlayer.getUsername());
        assertTrue(hasUsername);
    }

    @Test
    public void testUserExists_NotExists_Success() {
        boolean hasUsername = playerService.userExists("Username_NotExists");
        assertFalse(hasUsername);
    }
}
