package com.tripleonetech.sbe.player;

import com.google.common.collect.ImmutableList;
import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;
import static org.junit.Assert.*;

public class PlayerMapperTest extends BaseTest {

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private Constant constant;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void validateEncryptionConstants() {
        assertEquals("Validating salt is 'og' in application-test.properties constant", "og",
                constant.getPlayer().getEncryptionSalt());
        assertEquals("Validating password is 'dcrajUg01' in application-test.properties constant", "dcrajUg01",
                constant.getPlayer().getEncryptionPassword());
    }

    private void assertPlayer(Player player) {
        assertNotNull(player);
        assertEquals("Player decrypted password should match original", "123456", player.getPassword());
    }

    private void assertPlayerView(PlayerIdView playerView) {
        assertNotNull(playerView);
        assertNotNull(playerView.getCurrency());
        assertNotNull(playerView.getId());
        assertNotNull(playerView.getUsername());
    }

    @Test
    public void testGetPlayer() {
        int playerId = 1;
        Player player = playerMapper.get(playerId);
        assertPlayer(player);
    }

    @Test
    public void testGetByCredentialId() {
        int credentialId = 2;
        List<Player> players = playerMapper.listByCredentialId(credentialId, null);
        players.forEach(this::assertPlayer);
    }

    @Test
    public void testGetPlayerIdByCredentialId() {
        Integer playerId = playerMapper.getPlayerIdByCredentialId(2, SiteCurrencyEnum.CNY);
        assertEquals(2, playerId.intValue());
    }

    @Test
    public void testGetPlayerIdsByCredentialIds() {
        List<Integer> credentialIds = Arrays.asList(1,2,3,4,5);
        List<Integer> playerIds = playerMapper.getPlayerIdsByCredentialIds(credentialIds, SiteCurrencyEnum.CNY);
        // expected playerIds: 1,2,3,5,6
        assertNotNull(playerIds);
        assertEquals(5, playerIds.size());
        assertTrue(playerIds.containsAll(Arrays.asList(1,2,3,5,6)));
    }

    @Test
    public void testGetPlayerIdByUniqueKey() {
        Integer playerId = playerMapper.getPlayerIdByUniqueKey("test002", SiteCurrencyEnum.CNY);
        assertEquals(2, playerId.intValue());
    }

    @Test
    public void testListByPlayerIds() {
        List<Integer> playerIds = ImmutableList.of(2, 4, 5);
        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        players.forEach(this::assertPlayer);
    }

    @Test
    public void testListByEmptyPlayerIds() {
        List<Integer> playerIds = new ArrayList<>();
        List<Player> players = playerMapper.listByPlayerIds(playerIds);
        assertEquals("Empty player IDs should return empty player list", 0, players.size());
    }

    @Test
    public void testGetReferralPlayerByPlayerIds() {
        List<Integer> playerIds = ImmutableList.of(2, 4);
        List<ReferrerPlayer> referralPlayer = playerMapper.getReferralPlayerByPlayerIds(playerIds);
        Map<Integer, ReferrerPlayer> referrerPlayerMap = referralPlayer.stream()
                .collect(toMap(ReferrerPlayer::getReferralPlayerId, Function.identity()));
        Set<Integer> playerSet = referrerPlayerMap.keySet();
        for (Integer playerId : playerIds) {
            assertTrue(playerSet.contains(playerId));
            ReferrerPlayer referrerPlayer = referrerPlayerMap.get(playerId);
            assertNotNull(referrerPlayer);
            assertNotNull(referrerPlayer.getPlayerId());
        }
    }

    @Test
    public void testGetNoReferralPlayerByPlayerIds() {
        List<Integer> playerIds = ImmutableList.of(1, 3);
        List<ReferrerPlayer> referralPlayer = playerMapper.getReferralPlayerByPlayerIds(playerIds);
        Map<Integer, ReferrerPlayer> referrerPlayerMap = referralPlayer.stream()
                .collect(toMap(ReferrerPlayer::getReferralPlayerId, Function.identity()));
        Set<Integer> playerSet = referrerPlayerMap.keySet();
        for (Integer playerId : playerIds) {
            assertTrue(playerSet.contains(playerId));
            ReferrerPlayer referrerPlayer = referrerPlayerMap.get(playerId);
            assertNotNull(referrerPlayer);
            assertNull(referrerPlayer.getPlayerId());
        }
    }

    @Test
    public void testGetReferralPlayerByEmptyPlayerIds() {
        List<Integer> playerIds = new ArrayList<>();
        List<ReferrerPlayer> referralPlayer = playerMapper.getReferralPlayerByPlayerIds(playerIds);
        assertEquals("Empty player IDs should return empty player list", 0, referralPlayer.size());
    }

    @Test
    public void testListViewByPlayerIds() {
        List<Integer> playerIds = ImmutableList.of(2, 4, 5);
        List<PlayerIdView> players = playerMapper.listViewByPlayerIds(playerIds, 1, 20);
        players.forEach(this::assertPlayerView);
    }

    @Test
    public void testGetByUniqueKey() {
        Player player = playerMapper.getByUniqueKey("test002", SiteCurrencyEnum.USD);
        assertPlayer(player);
    }

    @Test
    public void testInsertPlayer() {
        PlayerCredential modkCredential = new PlayerCredential();
        modkCredential.setUsername("test002test002");
        modkCredential.setPassword("123456");
        modkCredential.setReferralCode("1234567");
        modkCredential.setEmailVerified(false);
        playerCredentialMapper.insert(modkCredential);

        Player mockPlayer = new Player();
        mockPlayer.setUsername(modkCredential.getUsername());
        mockPlayer.setCurrency(SiteCurrencyEnum.IDR);
        mockPlayer.setStatus(PlayerStatusEnum.ACTIVE);

        // Before insert
        Player player = playerMapper.getByUniqueKey(mockPlayer.getUsername(), mockPlayer.getCurrency());
        assertNull(player);

        // After insert
        playerMapper.insert(mockPlayer);
        player = playerMapper.getByUniqueKey(mockPlayer.getUsername(), mockPlayer.getCurrency());
        assertEquals(mockPlayer.getCurrency(), player.getCurrency());
        assertTrue("Player should be inserted as a new record", player.getId() > 2);
        assertEquals("Player decrypted password should match original", "123456", player.getPassword());
    }

    @Test
    public void testUpdateLastLoginDate() {
        Integer mockPlayerId = 2;
        LocalDateTime mockDate = LocalDateTime.now();

        // Before update
        Player player = playerMapper.get(mockPlayerId);
        assertTrue(DateUtils.localToZoned(mockDate).isAfter(player.getLastLoginTime()));

        // After update
        player.setLastLoginTime(mockDate);
        player.setLastLoginDevice(DeviceTypeEnum.MOBILE);
        player.setLastLoginIp("127.0.0.1");
        playerMapper.updateLastLoginInfo(player);
        player = playerMapper.get(mockPlayerId);
        assertEquals(0, ChronoUnit.SECONDS.between(mockDate, player.getLastLoginTime()));
        assertEquals(DeviceTypeEnum.MOBILE, player.getLastLoginDevice());
        assertEquals("127.0.0.1", player.getLastLoginIp());
    }

    @Test
    public void testSetPassword() {
        int playerId = 2;
        String newPasswordEncrypt = "ZXCASD";

        playerMapper.updatePassword(playerId, newPasswordEncrypt);

        Player player = playerMapper.get(playerId);
        assertEquals(newPasswordEncrypt, player.getPasswordEncrypt());
    }
}
