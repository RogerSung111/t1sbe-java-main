package com.tripleonetech.sbe.player;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;

import static org.junit.Assert.assertTrue;

public class PlayerStatsDirtyAspectTest extends BaseTest {
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PlayerStatsMapper playerStatsMapper;
    private Integer playerId = 2;

    @Test
    public void testSetWalletAmountToDirty() {
        PlayerStats playerStats = null;
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.setType(WalletTransactionTypeEnum.DEPOSIT);
        walletTransaction.setAmount(new BigDecimal("100"));
        walletTransaction.setPlayerId(playerId);

        // deposit type
        walletTransaction.setType(WalletTransactionTypeEnum.DEPOSIT);
        walletTransactionMapper.insert(walletTransaction);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.DEPOSIT_AMOUNT.getCode()) > 0);

        // withdraw type
        walletTransaction.setType(WalletTransactionTypeEnum.WITHDRAWAL);
        walletTransactionMapper.insert(walletTransaction);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.WITHDRAW_AMOUNT.getCode()) > 0);
    }

    @Test
    public void testSetBonusAmountToDirtyWhenInsert() {
        PlayerStats playerStats = null;
        PromoBonus promoBonus = new PromoBonus();
        promoBonus.setBonusAmount(new BigDecimal("100"));
        promoBonus.setBonusDate(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS));
        promoBonus.setPlayerId(playerId);
        promoBonus.setRuleId(2);
        promoBonus.setStatus(BonusStatusEnum.PENDING_APPROVAL);

        // cashback type
        promoBonus.setPromoType(PromoTypeEnum.CASHBACK);
        promoBonusMapper.insert(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CASHBACK_BONUS.getCode()) > 0);

        // referral type
        promoBonus.setPromoType(PromoTypeEnum.REFERRAL);
        promoBonusMapper.insert(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.REFERRAL_BONUS.getCode()) > 0);

        // campaign type
        promoBonus.setPromoType(PromoTypeEnum.CAMPAIGN_DEPOSIT);
        promoBonusMapper.insert(promoBonus);
        promoBonus.setPromoType(PromoTypeEnum.CAMPAIGN_RESCUE);
        promoBonusMapper.insert(promoBonus);
        promoBonus.setPromoType(PromoTypeEnum.CAMPAIGN_TASK);
        promoBonusMapper.insert(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CAMPAIGN_BONUS.getCode()) > 0);
    }

    @Test
    public void testSetBonusAmountToDirtyWhenUpdate() {
        PlayerStats playerStats = null;
        PromoBonus promoBonus = null;

        // cashback type
        promoBonus = promoBonusMapper.queryById(4);
        promoBonusMapper.update(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CASHBACK_BONUS.getCode()) > 0);

        // referral type
        promoBonus = promoBonusMapper.queryById(12);
        promoBonusMapper.update(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.REFERRAL_BONUS.getCode()) > 0);

        // campaign type
        promoBonus = promoBonusMapper.queryById(38);
        promoBonusMapper.update(promoBonus);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CAMPAIGN_BONUS.getCode()) > 0);
    }

    @Test
    public void testSetBonusAmountToDirtyWhenUpdateStatus() {
        PlayerStats playerStats = null;
        PromoBonus promoBonus = null;

        // cashback type
        promoBonus = promoBonusMapper.queryById(4);
        promoBonusMapper.updateStatus(promoBonus.getId(), BonusStatusEnum.APPROVED);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CASHBACK_BONUS.getCode()) > 0);

        // referral type
        promoBonus = promoBonusMapper.queryById(12);
        promoBonusMapper.updateStatus(promoBonus.getId(), BonusStatusEnum.APPROVED);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.REFERRAL_BONUS.getCode()) > 0);

        // campaign type
        promoBonus = promoBonusMapper.queryById(38);
        promoBonusMapper.updateStatus(promoBonus.getId(), BonusStatusEnum.APPROVED);
        playerStats = playerStatsMapper.getByPlayerId(playerId);
        assertTrue((playerStats.getDirty() & PlayerStatsTypeEnum.CAMPAIGN_BONUS.getCode()) > 0);
    }

}
