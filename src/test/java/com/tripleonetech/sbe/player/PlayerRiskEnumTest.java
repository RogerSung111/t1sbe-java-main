package com.tripleonetech.sbe.player;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlayerRiskEnumTest {

    @Test
    public void testIncrease() {
        assertEquals(PlayerRiskEnum.MEDIUM, PlayerRiskEnum.MILD.increase());
    }

    @Test
    public void testDecrease() {
        assertEquals(PlayerRiskEnum.LOW, PlayerRiskEnum.MILD.decrease());
    }
}
