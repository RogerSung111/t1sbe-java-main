package com.tripleonetech.sbe.oauth;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.operator.*;

public class OauthServiceTest extends BaseTest {
    
    @Autowired
    private OauthService oauthService;
    
    @Autowired
    private OperatorMapper operatorMapper;
    
    @Autowired
    private OperatorOauthMapper operatorOauthMapper;
    
    private final static String SUPERADMIN = "superadmin";
    
    @Test
    public void testCreateToken() {
        Operator operator = operatorMapper.getByUsername(SUPERADMIN);
        OperatorDetails operatorDetail = new OperatorDetails(operator);
        OAuth2AccessToken oauth2Token = oauthService.createOauth2Token(operatorDetail);
        assertNotNull(oauth2Token);

    }
    
    @Test
    public void testFindOauth2TokenUsingWrongTokenResultShouldBeNull() {
        String accessToken = "40bfb0f8-abcf-42c4-a253-45c380093b38";
        assertNull(oauthService.findOauth2Token(accessToken));
    }
    
    @Test
    public void testFindOauth2Token() {
        Operator operator = operatorMapper.getByUsername(SUPERADMIN);
        OperatorDetails operatorDetail = new OperatorDetails(operator);
        OAuth2AccessToken oauth2Token = oauthService.createOauth2Token(operatorDetail);
        
        assertNotNull(oauthService.findOauth2Token(oauth2Token.getValue()));
    }
    
    @Test
    public void testRefreshExpiredOperatorRefreshToken() {
        Operator operator = operatorMapper.getByUsername(SUPERADMIN);
        OperatorOauth operatorOauth = operatorOauthMapper.get(operator.getId());
        assertNull(operatorOauth);
        oauthService.refreshExpiredOperatorRefreshToken(SUPERADMIN);
        
        OperatorOauth newOperatorOauth = operatorOauthMapper.get(operator.getId());
        assertNotNull(newOperatorOauth);
    }

}
