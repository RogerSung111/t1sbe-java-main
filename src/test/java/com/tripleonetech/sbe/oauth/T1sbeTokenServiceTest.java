package com.tripleonetech.sbe.oauth;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerUserDetails;

public class T1sbeTokenServiceTest extends BaseTest {

    @Autowired
    private T1sbeTokenService T1sbeTokenService;
    @Autowired
    private DefaultTokenServices defaultTokenServices;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private Constant constant;
    @Autowired
    private PlayerMapper playerMapper;

    @Test
    public void testRevokeTokens() {
        String username = "test001";
        List<Player> players = playerMapper.listByUsername(username);
        Player player = players.get(0);
        generateToken(player);
        generateToken(player);
        generateToken(player);
        Collection<OAuth2AccessToken> tokens = tokenStore
                .findTokensByClientIdAndUserName(constant.getOauthPlayer().getClient(), username);
        assertTrue(tokens.size() == 3);

        T1sbeTokenService.revokePlayerTokens(username, false);
        tokens = tokenStore
                .findTokensByClientIdAndUserName(constant.getOauthPlayer().getClient(), username);
        assertTrue(tokens.size() == 0);
    }

    @Test
    public void testListPlayerTokens() {
        String username = "test001";
        List<Player> players = playerMapper.listByUsername(username);
        Player player = players.get(0);
        generateToken(player);
        generateToken(player);
        generateToken(player);
        Collection<OAuth2AccessToken> tokens = T1sbeTokenService.listPlayerTokens(username);
        assertTrue(tokens.size() == 3);
    }

    private void generateToken(Player player) {
        PlayerUserDetails playerDetails = new PlayerUserDetails(player);

        String clientId = constant.getOauthPlayer().getClient();
        Collection<? extends GrantedAuthority> authorities = playerDetails.getAuthorities();

        OAuth2Request oAuth2Request = new OAuth2Request(null, clientId,
                authorities, true, null, null, null, null, null);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                playerDetails,
                null, authorities);
        OAuth2Authentication oauth = new OAuth2Authentication(oAuth2Request, authenticationToken);
        defaultTokenServices.createAccessToken(oauth);
    }

}
