package com.tripleonetech.sbe;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * On top of BaseTest, this class provides backend api profile for unit tests.
 * 
 * @author yunfei
 *         Created on 9 Jan 2018
 */
@SpringBootTest(properties = { "spring.profiles.active=main,test,db-test", "scheduling.enabled=false", "redis.enabled=false" }, classes = T1SBEApplication.class)
public abstract class BaseMainTest extends BaseMvcTest {
    private final Logger logger = LoggerFactory.getLogger(BaseMainTest.class);
    private String operatorUsername = "superadmin";
    private String operatorPassword = "abcdef";

    // Obtains access token with default admin username and password
    @Override
    protected String obtainAccessToken() {
        String client = "T1SBE";
        String secret = "T1SBE-rocks";
        String content;
        try {
            content = super.getMockMvc()
                    .perform(
                            post("/oauth/token")
                                    .param("username", this.operatorUsername)
                                    .param("password", this.operatorPassword)
                                    .param("grant_type", "password")
                                    .param("scope", "read write")
                                    .param("client_id", client)
                                    .param("client_secret", secret))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
        } catch (Exception e) {
            logger.error("Fail to get token");
            e.printStackTrace();
            return null;
        }
        logger.debug("OAuth token response :{}", content);

        Pattern pattern = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            logger.error("Unable to obtain access token");
            return null;
        }
    }

    protected String getOperatorUsername() {
        return operatorUsername;
    }

    protected void setOperatorUsername(String operatorUsername) {
        this.operatorUsername = operatorUsername;
    }

    protected String getOperatorPassword() {
        return operatorPassword;
    }

    protected void setOperatorPassword(String operatorPassword) {
        this.operatorPassword = operatorPassword;
    }
}
