package com.tripleonetech.sbe;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.iprule.country.GeoIpServiceViaApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * On top of BaseTest, this class provides player api profile for unit tests.
 * 
 * Note: For some reason, unit tests that switches profile sometimes doesn't refresh the propertyResolver,
 * resulting in api profile properties not read by the Environment. Hence we use @DirtiesContext here to
 * force a context reload each time we test with api profile.
 * 
 * @author Yunfei<yunfei.dev@tripleonetech.net>
 *         Created on 1 Nov 2018
 */
@SpringBootTest(properties = { "spring.profiles.active=api,test,db-test", "scheduling.enabled=false", "redis.enabled=false" }, classes = T1SBEApplication.class)
public abstract class BaseApiTest extends BaseMvcTest {
    private final Logger logger = LoggerFactory.getLogger(BaseApiTest.class);
    @Autowired
    private Constant constant;
    @Autowired
    GeoIpServiceViaApi geoIpService;

    protected int getLoginPlayerId() {
        if (this.getClass().getName().endsWith("Test2")) {
            return 101;
        }
        return 2; // test002
    }

    protected String getLoginPlayerUsername() {
        if (this.getClass().getName().endsWith("Test2")) {
            return "testfornewplayer";
        }
        return "test002";
    }

    protected String getLoginPlayerCurrency() {
        return "CNY";
    }

    // Obtains access token with default player username and password
    @Override
    protected String obtainAccessToken() {
        String client = constant.getOauthPlayer().getClient();
        String secret = constant.getOauthPlayer().getSecret();
        String content;
        try {
            content = super.getMockMvc()
                    .perform(
                            post("/oauth/token")
                                    .param("username", getLoginPlayerUsername())
                                    .param("password", "123456")
                                    .param("currency", getLoginPlayerCurrency())
                                    .param("grant_type", "password")
                                    .param("scope", "read write")
                                    .param("client_id", client)
                                    .param("client_secret", secret))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
        } catch (Exception e) {
            logger.error("Fail to get api token");
            e.printStackTrace();
            return null;
        }
        logger.debug("OAuth token response :{}", content);

        Pattern pattern = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            logger.error("Unable to obtain access token");
            return null;
        }
    }
}
