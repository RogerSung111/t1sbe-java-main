package com.tripleonetech.sbe.web.common;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigView;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameTypeView;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.game.log.GameApiIdNameView;
import com.tripleonetech.sbe.payment.PaymentApiFactory;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigView;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.privilege.SiteCurrencyView;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import com.tripleonetech.sbe.privilege.SiteTemplateView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class SiteControllerTest extends BaseMainTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Test
    public void getSiteCurrencyNotEmptyTest() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/currencies"));
        assertNotNull(result.get("data"));
        String data = result.get("data").toString();
        List<SiteCurrencyView> currencies = JsonUtils.jsonToObjectList(data, SiteCurrencyView.class);

        assertTrue(currencies.size() > 0);
        assertNotNull(currencies.get(0).isActive());
        assertNotNull(currencies.get(0).getCurrency());

    }

    @Test
    public void getSiteTemplateNotEmptyTest() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/templates"));
        assertNotNull(result.get("data"));
        String data = result.get("data").toString();
        List<SiteTemplateView> templates = JsonUtils.jsonToObjectList(data, SiteTemplateView.class);

        assertTrue(templates.size() > 0);
        assertNotNull(templates.get(0).isActive());
        assertNotNull(templates.get(0).getTemplate());
    }

    @Test
    public void getConfigSystemTimeZoneNotNullTest() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/time-zone"));
        String timeZone = result.get("data").asText();
        assertTrue(StringUtils.isNotBlank(timeZone));
    }

    @Test
    public void getSiteLanguagesNotEmptyTest() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/languages"));
        JsonNode languages = result.get("data");
        assertTrue(languages.isArray());
        assertTrue(languages.size() > 0);
        assertEquals("en-US", languages.get(0).textValue());
        assertEquals("hi-IN", languages.get(1).textValue());
        assertEquals("th-TH", languages.get(2).textValue());
        assertEquals("vi-VN", languages.get(3).textValue());
        assertEquals("zh-CN", languages.get(4).textValue());
    }

    private void initGameApi() {
        int testGameApiId1 = 1;
        gameApiConfigMapper.delete(testGameApiId1);
    }
    /**
     * @purpose: The test is for drop-down list(should show all game_apis even game_api is deleted)
     */
    @Test
    @WithUserDetails("superadmin")
    public void testGetSiteGameApis() throws Exception {
        initMock();
        boolean showDeletedApi = true;
        List<String> gameApisCodes = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.GAME_API);
        List<GameApiConfig> allGameApiConfigs = gameApiConfigMapper.listGameApiConfigByCode(gameApisCodes, showDeletedApi);
        //delete 1 game_api
        initGameApi();

        //loop api return mockGame
        Mockito.<GameApi<? extends ApiGameLog>>when(mockApiFactory.getById(Mockito.anyInt())).thenReturn(mockGameApiMockGame);
        Mockito.when(mockGameApiMockGame.isOnline()).thenReturn(true);

        String result = mockMvc.perform(get("/site-config/game-apis")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        JsonNode resultNode = mapper.readTree(result);
        List<ApiConfigView> apiConfigView = mapper.readValue(resultNode.get("data").toString(),
                new TypeReference<List<ApiConfigView>>() {});

        assertOk(resultNode);
        assertTrue(apiConfigView.size() == allGameApiConfigs.size());
        for(ApiConfigView item : apiConfigView) {
            assertNotNull(item.getCurrency());
            assertTrue(StringUtils.isNotBlank(item.getName()));
            assertTrue(item.getId() > 0);
            for(IdNameView gameType : item.getGameTypes()) {
                assertTrue(gameType.getId() > 0);
                assertTrue(StringUtils.isNotBlank(gameType.getName()));
            }
        }

    }


    @Test
    public void getSiteAvailableGameApiNotEmpty() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/available-game-apis"));
        JsonNode data = result.get("data");
        assertTrue("Should return available game apis", data.size() > 0);
    }

    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private SiteController siteController;
    @Mock
    private GameApiFactory mockApiFactory;
    @Mock
    private GameApi mockGameApiMockGame;
    @Mock
    private PaymentApiFactory mockPaymentApiFactory;
    @Mock
    private PaymentApi mockPaymentApiMockPay;

    @Autowired
    private WebApplicationContext webApplicationContext;

    public void initMock() throws Exception {
        MockitoAnnotations.initMocks(this);
        SiteController unwrappedController = (SiteController) unwrapProxy(siteController);
        ReflectionTestUtils.setField(unwrappedController, "gameApiFactory", mockApiFactory);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @Test
    @WithUserDetails("superadmin")
    public void getSitePaymentApiNotEmpty() throws Exception {
        initMock();
        Mockito.when(mockPaymentApiFactory.getById(Mockito.anyInt())).thenReturn(mockPaymentApiMockPay);
        Mockito.when(mockPaymentApiMockPay.isOnline()).thenReturn(true);

        String result = mockMvc.perform(get("/site-config/payment-apis")
                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse().getContentAsString();
        JsonNode resultNode = mapper.readTree(result);
        List<PaymentApiConfigView> apiConfigView = mapper.readValue(resultNode.get("data").toString(),
                new TypeReference<List<PaymentApiConfigView>>() {});

        assertOk(resultNode);
        assertTrue(apiConfigView.size() > 0);
        assertTrue(apiConfigView.get(0).isOnline());
        assertNotNull(apiConfigView.get(0).getDailyMaxDeposit());
    }

    @Test
    public void getSiteGameTypeNotEmpty() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/game-types"));
        String data = result.get("data").toString();
        List<GameTypeView> gameTypes = mapper.readValue(data, new TypeReference<List<GameTypeView>>() {});
        assertTrue(gameTypes.size() > 0);
        for(GameTypeView item : gameTypes) {
            assertTrue(StringUtils.isNotBlank(item.getName()));
            assertNotNull(item.getId());
            for(GameApiIdNameView gameApi : item.getGameApis()) {
                assertTrue(gameApi.getId() > 0);
                assertTrue(StringUtils.isNotBlank(gameApi.getCode()));
                assertTrue(StringUtils.isNotBlank(gameApi.getName()));
            }
        }
    }

    @Test
    public void getSiteCreditPoint() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/credit-point"));
        String data = result.get("data").toString();
        BigDecimal balance = mapper.readValue(data, BigDecimal.class);
        assertEquals(balance, new BigDecimal("6666.66"));

    }

    @Test
    public void getGamelogReportSearchLimits() throws Exception {
        JsonNode result = mockMvcPerform(get("/site-config/game-log-search-limit"));
        String data = result.get("data").toString();
        Map<String, Integer> gamelogSearchLimits = mapper.readValue(data,
                new TypeReference<Map<String, Integer>>() {
                });
        assertEquals(31, gamelogSearchLimits.get("gamelogReportSearchLimit").intValue());
        assertEquals(183, gamelogSearchLimits.get("gamelogSummaryReportSearchLimit").intValue());

    }
}
