package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.player.report.PlayerSummaryReportView;

public class ReportPlayerSummaryControllerTest extends BaseMainTest {
    @Test
    public void testQueryPlayerSummaryReportWithNoDeviceDetail() throws Exception {
        LocalDate threeDaysAgo = LocalDate.now().minusDays(3);

        JsonNode resultNode = mockMvcPerform(get("/reports/player-summary")
                .param("dateStart", threeDaysAgo.toString())
                .param("dateEnd", LocalDate.now().toString())
                // .param("byDevice", "false") // This is default
                .param("timezoneOffset", "0"));
        assertOk(resultNode);
        JsonNode data = resultNode.get("data");
        // test response info
        assertEquals(3, data.get("total").intValue());
        assertEquals(20, data.get("pageSize").intValue());
        assertEquals(1, data.get("pages").intValue());

        JsonNode jsonArray = data.get("list");
        assertTrue(jsonArray.isArray());
        assertEquals(3, jsonArray.size());
        // spot test data
        String validateDateString = LocalDate.now().minusDays(2).toString();
        assertEquals(validateDateString, jsonArray.get(1).get("date").textValue());
        /*
         * select sum(device_new_count) from
         * (
         * SELECT sum(new_count) device_new_count FROM player_summary_report
         * where time_hour >= [2 days ago 00:00 GMT+0] and time_hour < [yesterday 00:00 GMT+0]
         * group by device
         * ) count_by_device
         */
        // The last of mock data is yesterday, so get(1) is data of 2 days ago
        // And so on, get(0) for yesterday, and get(2) for 3 days ago
        // There is no get(3), unless change the parameter "dateStart" before 3 days ago
        assertEquals(74, jsonArray.get(1).get("newCount").intValue());
        /*
         * select sum(device_total_count) from
         * (
         * SELECT max(total_count) device_total_count FROM player_summary_report
         * where time_hour < [yesterday 00:00 GMT+0]
         * group by device
         * ) count_by_device
         */
        assertEquals(686, jsonArray.get(1).get("totalCount").intValue());
        assertNull(jsonArray.get(1).get("device"));
        assertEquals(validateDateString + "-0", jsonArray.get(1).get("id").textValue());

    }

    @Test
    public void testQueryPlayerSummaryReport() throws Exception {

        LocalDate threeDaysAgo = LocalDate.now().minusDays(3);

        JsonNode resultNode = mockMvcPerform(get("/reports/player-summary")
                .param("dateStart", threeDaysAgo.toString())
                .param("dateEnd", LocalDate.now().toString())
                .param("byDevice", "true")
                .param("timezoneOffset", "0"));

        assertOk(resultNode);
        JsonNode data = resultNode.get("data");
        // test response info
        assertEquals(15, data.get("total").intValue());
        assertEquals(20, data.get("pageSize").intValue());
        assertEquals(1, data.get("pages").intValue());

        JsonNode jsonArray = data.get("list");
        assertTrue(jsonArray.isArray());
        assertEquals(15, jsonArray.size());
        // spot test data
        String validateDateString = LocalDate.now().minusDays(1).toString();
        assertEquals(validateDateString, jsonArray.get(0).get("date").textValue());
        /*
         * SELECT sum(new_count) count FROM player_summary_report
         * where time_hour >= [yesterday 00:00 GMT+0] and time_hour < [today 00:00 GMT+0]
         * and device = 6
         * group by device
         */
        assertEquals(19, jsonArray.get(0).get("newCount").intValue());
        // As above, sum(new_count) -> max(total_count)
        assertEquals(157, jsonArray.get(0).get("totalCount").intValue());
        assertEquals(6, jsonArray.get(0).get("device").intValue());
        assertEquals(validateDateString + "-6", jsonArray.get(0).get("id").textValue());

        // confirm record numbers of each device and date
        String jsonString = JsonUtils.objectToJson(jsonArray);
        List<PlayerSummaryReportView> list = JsonUtils.jsonToObjectList(jsonString, PlayerSummaryReportView.class);

        assertEquals(15, list.size());
        list.forEach(v -> assertEquals(v.getDate().toString() + "-" + v.getDevice().getCode(), v.getId()));

        Map<LocalDate, Map<DeviceTypeEnum, List<PlayerSummaryReportView>>> collect = list.stream()
                .collect(Collectors.groupingBy(PlayerSummaryReportView::getDate,
                        Collectors.groupingBy(PlayerSummaryReportView::getDevice)));
        assertEquals(3, collect.size());
        assertEquals(5, collect.get(threeDaysAgo).size());
        assertEquals(5, collect.get(threeDaysAgo.plusDays(1)).size());
        assertEquals(5, collect.get(threeDaysAgo.plusDays(2)).size());
    }
}
