package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.PlayerBankAccount;
import com.tripleonetech.sbe.player.PlayerBankAccountForm;
import com.tripleonetech.sbe.player.PlayerBankAccountMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerBankAccountControllerTest extends BaseMainTest {
    @Autowired
    private PlayerBankAccountMapper mapper;

    @Test
    public void testGetBankAccount() throws Exception {
        JsonNode node = mockMvcPerform(get("/players/2/bank-accounts/CNY"));
        assertOk(node);

        JsonNode data = node.get("data");
        assertTrue(data.size() > 0);

        JsonNode accountNode = data.get(0);
        assertFalse(accountNode.get("id").isNull());
        assertFalse(accountNode.get("bankId").isNull());
        assertFalse(accountNode.get("accountNumber").isNull());
        assertFalse(accountNode.get("accountHolderName").isNull());
    }

    @Test
    public void testSetDefault() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/players/2/bank-accounts/CNY/2/set-default")
        );

        assertOk(node);
        assertDataSuccess(node.get("data"));

        node = mockMvcPerform(
                post("/players/2/bank-accounts/CNY/100/set-default")
                        .with(csrf())
        );
        assertDataFail(node.get("data"));
    }

    @Test
    public void testCreate() throws Exception {
        PlayerBankAccountForm playerBankAccount = new PlayerBankAccountForm();
        playerBankAccount.setBankId(1);
        playerBankAccount.setAccountNumber("3456-7812-2325-1231");
        playerBankAccount.setBankBranch("TEST");
        playerBankAccount.setAccountHolderName("Jack");

        String requestBody = JsonUtils.objectToJson(playerBankAccount);
        JsonNode node = mockMvcPerform(
                post("/players/2/bank-accounts/CNY/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));
        assertTrue(node.get("data").get("id").asInt() > 1);
    }

    @Test
    public void testCreateFail() throws Exception {
        PlayerBankAccountForm playerBankAccount = new PlayerBankAccountForm();
        playerBankAccount.setBankId(1);
        playerBankAccount.setAccountHolderName("Jack");

        String requestBody = JsonUtils.objectToJson(playerBankAccount);
        JsonNode node = mockMvcPerform(
                post("/players/2/bank-accounts/CNY/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
        );

        assertInvalidParameter(node);
    }

    @Test
    public void testUpdate() throws Exception {
        PlayerBankAccountForm playerBankAccount = new PlayerBankAccountForm();
        playerBankAccount.setBankId(5);
        playerBankAccount.setAccountNumber("3456-7812-2325-1231");
        playerBankAccount.setBankBranch("TEST");
        playerBankAccount.setAccountHolderName("Jack");

        String requestBody = JsonUtils.objectToJson(playerBankAccount);
        JsonNode node = mockMvcPerform(
                post("/players/2/bank-accounts/CNY/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));

        PlayerBankAccount updatedAccount = mapper.get(2);
        assertEquals(playerBankAccount.getBankId(), updatedAccount.getBankId());
        assertEquals(playerBankAccount.getAccountHolderName(), updatedAccount.getAccountHolderName());
        assertEquals(playerBankAccount.getAccountNumber(), updatedAccount.getAccountNumber());
    }

}
