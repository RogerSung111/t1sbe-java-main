package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.storage.ResourceUpload;
import com.tripleonetech.sbe.common.storage.ResourceUploadMapper;
import com.tripleonetech.sbe.common.storage.StorageService;
import com.tripleonetech.sbe.web.opbackend.ImageController.ImageUpdateForm;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ImageControllerTest extends BaseMainTest {

    @Autowired
    private StorageService storageService;
    @Autowired
    private  ResourceUploadMapper resourceUploadMapper;
    @Autowired
    private Constant constant;

    private String originalFilename = "test.gif";
    private String contentType = "image/gif";
    private String filePath;
    private Integer resourceUploadId;

    @After
    public void tearDown() throws Exception {
        // revert
        if (filePath != null && !filePath.isEmpty()) {
            storageService.delete(filePath);
        }
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/images")
                .param("name", "test")
                .param("sort", "width ASC"));
        assertOk(resultNode);

        assertEquals(3, resultNode.get("data").get("total").asInt());
        int width = 0;
        Iterator<JsonNode> elements = resultNode.get("data").get("list").elements();
        while (elements.hasNext()) {
            JsonNode data = elements.next();
            assertTrue(width <= data.get("width").intValue());
            width = data.get("width").intValue();
            assertFalse(data.get("createdAt").isNull());
        }
    }

    @Test
    public void testList_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/images"));
        assertOk(resultNode);

        assertEquals(4, resultNode.get("data").get("total").asInt());
        JsonNode data = resultNode.get("data").get("list").get(0);
        assertTrue(data.get("filePath").asText().startsWith(constant.getStorage().getStaticParentUrl()));
        assertTrue(data.hasNonNull("fileName"));
        assertTrue(data.hasNonNull("name"));
        assertTrue(data.hasNonNull("size"));
        assertTrue(data.hasNonNull("width"));
        assertTrue(data.hasNonNull("height"));
    }

    @Test
    public void testUploadFile() throws Exception {
        JsonNode resultNode = this.uploadImageFile();

        ResourceUpload resourceUpload = resourceUploadMapper.get(this.resourceUploadId);
        this.filePath = resourceUpload.getFilePath();
        assertTrue(this.filePath.endsWith("gif"));
        assertTrue(resourceUpload.getSize().compareTo(2120L) == 0);
        assertTrue(
                resultNode.get("data").get("filePath").asText().startsWith(constant.getStorage().getStaticParentUrl()));
        assertEquals("my_image.gif", resourceUpload.getName());
        assertEquals(119, resourceUpload.getWidth().intValue());
        assertEquals(43, resourceUpload.getHeight().intValue());
    }

    private JsonNode uploadImageFile() throws IOException, Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", originalFilename, contentType,
                new ClassPathResource("static/logo.gif").getInputStream());
        JsonNode resultNode = mockMvcPerform(multipart("/images/create")
                .file(multipartFile)
                .param("name", "my_image.gif"));
        assertOk(resultNode);

        this.resourceUploadId = resultNode.get("data").get("id").asInt();
        ResourceUpload resourceUpload = resourceUploadMapper.get(this.resourceUploadId);
        this.filePath = resourceUpload.getFilePath();
        return resultNode;
    }

    @Ignore // TODO Limit file size, uploading with swagger is successful, but unit test is failed.
    @Test
    public void testUploadFile_MaxUploadSizeExceeded_Failed() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", originalFilename, contentType, new byte[1024 * 1024 * 11]);
        JsonNode resultNode = mockMvcPerform(multipart("/images/create").file(multipartFile));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().startsWith("The field file exceeds its maximum permitted size of"));
    }

    @Test
    public void testUploadFile_InvalidFileType_Failed() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.exe", contentType, new byte[1024 * 1024 * 11]);
        JsonNode resultNode = mockMvcPerform(multipart("/images/create")
                .file(multipartFile)
                .param("name", "my_image.gif"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().startsWith("Failed to store this file type exe"));
    }

    @Test
    public void testLinkFile() throws Exception {
        // Prepare the file to be tested
        this.uploadImageFile();

        ResourceUpload resourceUpload = resourceUploadMapper.get(this.resourceUploadId);
        byte[] downloadBytes = mockMvcPerformDownloadFile(get("/resources/{filePath}", resourceUpload.getFilePath()));
        assertEquals(2120, downloadBytes.length);
    }

    @Test
    public void testDelete() throws Exception {
        // Prepare the file to be tested
        this.uploadImageFile();
        Resource resource = storageService.loadAsResource(filePath);
        assertTrue(resource.exists());

        JsonNode resultNode = mockMvcPerform(post("/images/{id}/delete", this.resourceUploadId));
        assertOk(resultNode);

        ResourceUpload resourceUpload = resourceUploadMapper.get(this.resourceUploadId);
        assertNull(resourceUpload);
        assertFalse(resource.exists());
    }

    @Test
    public void testEdit() throws Exception {
        ImageUpdateForm params = new ImageUpdateForm();
        params.setName("edit_name_for_unit_test.gif");
        JsonNode resultNode = mockMvcPerform(post("/images/{id}/edit", 1).content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        ResourceUpload resourceUpload = resourceUploadMapper.get(1);
        assertEquals(params.getName(), resourceUpload.getName());
    }

}
