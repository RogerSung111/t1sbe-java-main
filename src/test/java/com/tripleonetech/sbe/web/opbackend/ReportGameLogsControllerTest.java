package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.Constant;

public class ReportGameLogsControllerTest extends BaseMainTest {
    @Autowired
    private Constant constant;

    @Test
    public void testQueryReport_OnlyRequiredParams() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs/")
                .param("betTimeStart", now.plusDays(-29).toString())
                .param("betTimeEnd", now.toString()));
        assertOk(resultNode);
    }

    @Test
    public void testQueryReportWithUsernames() throws Exception {

        ZonedDateTime now = ZonedDateTime.now();
        String[] usernames = { "test002", "test003", "test004" };
        MockHttpServletRequestBuilder builder = get("/reports/game-logs/")
                .param("betTimeStart", now.plusDays(-29).toString())
                .param("betTimeEnd", now.toString())
                .param("currency", "CNY")
                .param("page", "2")
                .param("limit", "10");
        for (String username : usernames) {
            builder.param("usernames", username);
        }
        JsonNode resultNode = mockMvcPerform(builder);
        assertOk(resultNode);
        // list
        assertEquals(2, resultNode.get("data").get("list").get("pageNum").asInt());
        assertEquals(126, resultNode.get("data").get("list").get("total").asInt());
    }

    @Test
    public void testQueryReport() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs/")
                .param("betTimeStart", now.plusDays(-29).toString())
                .param("betTimeEnd", now.toString())
                .param("currency", "CNY")
                .param("page", "2")
                .param("limit", "10"));
        assertOk(resultNode);

        // list
        assertEquals(2, resultNode.get("data").get("list").get("pageNum").asInt());
        Set<Integer> gameApiIdSet = new HashSet<>();
        Iterator<JsonNode> list = resultNode.get("data").get("list").get("list").elements();
        while (list.hasNext()) {
            JsonNode node = list.next();

            gameApiIdSet.add(Integer.valueOf(node.get("gameApiId").asInt()));
            assertNotNull(node.get("gameApiCode"));
            assertNotNull(node.get("gameCode"));
            assertNotNull(node.get("playerUsername"));
            assertNotNull(node.get("externalUid"));
            assertNotNull(node.get("betDetails"));
            assertFalse(node.get("gameName").isNull());
            assertEquals("CNY", node.get("currency").asText());
            ZonedDateTime betTime = ZonedDateTime.parse(node.get("betTime").asText());
            assertTrue(betTime.compareTo(now.plusDays(-29)) >= 0);
            assertTrue(betTime.compareTo(now) <= 0);
        }

        // summary
        assertTrue(Integer.compare(resultNode.get("data").get("summary").size(), gameApiIdSet.size()) >= 0);
        Iterator<JsonNode> summary = resultNode.get("data").get("summary").elements();
        while (summary.hasNext()) {
            JsonNode node = summary.next();
            assertNotNull(node.get("gameApiName"));
            assertNotNull(node.get("bet"));
            assertNotNull(node.get("payout"));
            assertFalse(node.get("currency").isNull());
        }
    }

    @Test
    public void testList_NullDateCondition_Failed() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs/"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("betTimeStart"));
        assertTrue(resultNode.get("data").get("message").asText().contains("betTimeEnd"));
        assertTrue(resultNode.get("data").get("message").asText().contains("must not be null"));
    }

    @Test
    public void testList_ExceededLimitDays_Failed() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs/")
                .param("betTimeStart", now.plusDays(-200).toString())
                .param("betTimeEnd", now.toString()));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains(
                String.format("Date range should be limited to %d days", constant.getReport().getGameLogDayRange())));
    }
}
