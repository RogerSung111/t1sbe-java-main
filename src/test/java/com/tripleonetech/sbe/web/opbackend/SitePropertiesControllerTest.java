package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.site.SitePropertyKey.LiveChatInfoEnum;
import com.tripleonetech.sbe.site.SitePropertyMapper;
import com.tripleonetech.sbe.web.common.SitePropertiesReadController;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.tripleonetech.sbe.site.SitePropertyKey.SiteInfoEnum.LOGO;
import static com.tripleonetech.sbe.site.SitePropertyKey.SiteInfoEnum.SLOGAN;
import static com.tripleonetech.sbe.site.SitePropertyKey.SiteInfoEnum.TITLE;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class SitePropertiesControllerTest extends BaseMainTest {
    @Autowired
    private SitePropertyMapper sitePropertyMapper;

    @Test
    public void testGet() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/site-properties/live-chat"));
        assertOk(resultNode);

        assertEquals("http://tripleonetech.net", resultNode.get("data").get("liveChatUrl").asText());
        assertEquals("test-code", resultNode.get("data").get("liveChatCode").asText());
    }

    @Test
    public void testUpdate() throws Exception {
        SitePropertiesReadController.LiveChatInfoForm params = new SitePropertiesReadController.LiveChatInfoForm();
        params.setLiveChatUrl("http://test.com.net");
        params.setLiveChatCode("code-for-unit-test");
        JsonNode resultNode = mockMvcPerform(post("/site-properties/live-chat").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        assertEquals(params.getLiveChatUrl(), sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_URL).getValue());
        assertEquals(params.getLiveChatCode(), sitePropertyMapper.get(LiveChatInfoEnum.LIVE_CHAT_CODE).getValue());
    }

    @Test
    public void testGetSiteInfo() throws Exception {
        String testSlogan = "demo slogan";
        String testTitle = "demo site";
        String testLogo = "/image/logo.png";

        JsonNode resultNode = mockMvcPerform(get("/site-properties/site-info"));
        assertOk(resultNode);

        assertEquals(testSlogan, resultNode.get("data").get("slogan").asText());
        assertEquals(testTitle, resultNode.get("data").get("title").asText());
        assertEquals(testLogo, resultNode.get("data").get("logo").asText());
    }

    @Test
    public void testUpdateSiteInfo() throws Exception {
        SitePropertiesReadController.SiteInfoForm params = new SitePropertiesReadController.SiteInfoForm();
        params.setLogo("test_for_upload.png");
        params.setSlogan("unit test slogan");
        params.setTitle("unit test title");

        JsonNode resultNode = mockMvcPerform(post("/site-properties/site-info")
                .content(JsonUtils.objectToJson(params)));

        assertOk(resultNode);

        assertEquals(params.getSlogan(), sitePropertyMapper.get(SLOGAN).getValue());
        assertEquals(params.getTitle(), sitePropertyMapper.get(TITLE).getValue());
        assertEquals(params.getLogo(), sitePropertyMapper.get(LOGO).getValue());
    }

}
