package com.tripleonetech.sbe.web.opbackend;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.config.GameApiConfigView;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class GameApiConfigControllerTest extends BaseMainTest {

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Autowired
    private ObjectMapper mapper;
    private int GAME_CONFIG_API_ID;

    @Before
    public void setUp() throws Exception {
        GAME_CONFIG_API_ID = 1;
    }

    private void initGameApi() {
        gameApiConfigMapper.delete(GAME_CONFIG_API_ID);
    }
    @Test
    public void testListGameApiAndExcludeDeletedGameApi() throws Exception {

        initGameApi();
        boolean showDeletedApi = false;
        List<String> gameApisCodes = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.GAME_API);
        List<GameApiConfig> excludeDeletedGameApiConfigs = gameApiConfigMapper.listGameApiConfigByCode(gameApisCodes, showDeletedApi);

        JsonNode resultNode = mockMvcPerform(
                get("/system/game-api").param("showDeletedApi", String.valueOf(showDeletedApi)));
        List<GameApiConfigView> gameApiConfigs = mapper.readValue(resultNode.get("data").toString(),
                new TypeReference<List<GameApiConfigView>>() {});
        assertOk(resultNode);
        assertTrue(gameApiConfigs.size() == excludeDeletedGameApiConfigs.size());
    }

    @Test
    public void testListGameApiWithoutParam() throws Exception {

        initGameApi();

        JsonNode resultNode = mockMvcPerform(get("/system/game-api"));
        List<GameApiConfigView> gameApiConfigs = mapper.readValue(resultNode.get("data").toString(),
                new TypeReference<List<GameApiConfigView>>() {});
        assertOk(resultNode);
        assertTrue(gameApiConfigs.size() > 0);
    }

    @Test
    public void testListAllGameApis() throws Exception {
        //delete 1 game_api
        initGameApi();
        boolean showDeletedApi = false;
        List<String> gameApisCodes = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.GAME_API);
        List<GameApiConfig> excludeDeletedGameApiConfigs = gameApiConfigMapper.listGameApiConfigByCode(gameApisCodes, showDeletedApi);

        showDeletedApi = true;
        JsonNode resultNode = mockMvcPerform(
                get("/system/game-api")
                .param("showDeletedApi", String.valueOf(showDeletedApi)));

        List<GameApiConfigView> gameApiConfigs = mapper.readValue(resultNode.get("data").toString(),
                new TypeReference<List<GameApiConfigView>>() {});

        assertOk(resultNode);
        assertTrue(gameApiConfigs.size() > excludeDeletedGameApiConfigs.size());
        
        //Validate game_api_config
        Optional<GameApiConfigView> gameApiConfig = gameApiConfigs.stream()
                .filter(apiConfig -> apiConfig.getId() == GAME_CONFIG_API_ID).findAny();
        assertTrue(gameApiConfig.isPresent());
        
        //Validate game types
        assertTrue(gameApiConfig.get().getGameTypes().size() > 0);
        gameApiConfig.get().getGameTypes().forEach(gameType -> {
            assertTrue(gameType.getId() > 0);
            assertTrue(StringUtils.isNotBlank(gameType.getName()));
        });
    }

    @Test
    public void testListSingleApi() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/system/game-api/" + this.GAME_CONFIG_API_ID));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("MockGame", node.get("code").asText());
        assertEquals("CNY", node.get("currency").asText());
        assertEquals("MockGame CNY", node.get("name").asText());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/system/game-api/" + this.GAME_CONFIG_API_ID + "/status")
                .content("{\"status\":0}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }
    
    @Test
    public void testEnableGameLogSync() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/system/game-api/" + this.GAME_CONFIG_API_ID + "/game-log-sync/enabled")
                .content("{\"enabled\":1}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        
        GameApiConfig gameApiConfig = gameApiConfigMapper.get(this.GAME_CONFIG_API_ID);
        assertTrue("SyncEnabled should be true", gameApiConfig.isSyncEnabled());
    }

    @Test
    public void testTestConnectivity() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/system/game-api/" + this.GAME_CONFIG_API_ID + "/test"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }
}
