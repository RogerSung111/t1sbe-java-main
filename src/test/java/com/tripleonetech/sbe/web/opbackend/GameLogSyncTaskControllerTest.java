package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.ZonedDateTime;
import java.util.HashMap;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTask;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTaskMapper;

public class GameLogSyncTaskControllerTest extends BaseMainTest {

    @Autowired
    private GameLogSyncTaskMapper gameLogSyncTaskMapper;
    
    @Test
    public void testList() throws Exception {
        JsonNode node = mockMvcPerform(get("/game-log-sync-tasks")
                .param("createdAtStart", ZonedDateTime.now().minusHours(2).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString()));
        assertOk(node);
        assertNotNull(node.get("data"));
    }

    @Test
    public void testCreateSyncTask() throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("gameApiId", 1);
        params.put("syncStart", "2020-06-08T10:37:16.899Z");
        params.put("syncEnd", "2020-06-08T10:47:16.901Z");

        JsonNode resultNode = mockMvcPerform(post("/game-log-sync-tasks")
                .content(JsonUtils.objectToJson(params)));
        assertDataSuccess(resultNode.get("data"));

        GameLogSyncTask task = gameLogSyncTaskMapper.get(resultNode.get("data").get("id").asInt());
        assertEquals(params.get("gameApiId"), task.getGameApiId());
        assertEquals(params.get("syncStart").toString().substring(0, 23), task.getSyncStart().substring(0, 23));
        assertEquals(params.get("syncEnd").toString().substring(0, 23), task.getSyncEnd().substring(0, 23));
    }

}
