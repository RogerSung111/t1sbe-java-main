package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportSummaryControllerTest extends BaseMainTest {

    @Test
    public void testQuerySummaryReport() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/summary/CNY")
                .param("dateStart", LocalDate.now().minusDays(2).toString())
                .param("dateEnd", LocalDate.now().plusDays(1).toString())
                .param("page", "1")
                .param("limit", "5"));

        assertOk(resultNode);
        JsonNode data = resultNode.get("data").get("list");
        for(JsonNode item : data) {
            assertNotNull(item.get("date").asText());
            assertEquals(item.get("date").asText(), item.get("id").asText());
            assertTrue(item.get("playerCount").asInt() >= 0);
            assertTrue(item.get("depositAmount").decimalValue().compareTo(BigDecimal.ZERO) >= 0);
            assertTrue(item.get("depositCount").asInt() >= 0);
            assertTrue(item.get("withdrawalCount").asInt() >= 0);
            assertTrue(item.get("withdrawalAmount").decimalValue().compareTo(BigDecimal.ZERO) >= 0);
            assertTrue(item.get("betCount").asInt() >= 0);
            assertTrue(item.get("bet").decimalValue().compareTo(BigDecimal.ZERO) >= 0);
            assertTrue(item.get("payout").decimalValue().compareTo(BigDecimal.ZERO) >= 0);
        }
    }
}
