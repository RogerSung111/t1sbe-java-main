package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.withdraw.condition.WithdrawCondition;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class WithdrawConditionControllerTest extends BaseMainTest {

    @Autowired
    private WithdrawConditionMapper mapper;

    int playerId_2 = 2;

    @Test
    public void listTest() throws Exception {

        JsonNode resultNode = mockMvcPerform(get("/players/withdraw-condition")
                .contentType(MediaType.APPLICATION_JSON)
                .param("playerId", "2")
                .param("currency", "CNY"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data").get("list");
        assertNotNull(node);
        assertEquals("There should be 2 withdraw conditions returned", 2, node.size());
    }

    @Test
    public void listTestWithEmptyUsername() throws Exception {

        JsonNode resultNode = mockMvcPerform(get("/players/withdraw-condition")
                .param("Username", "")
                .contentType(MediaType.APPLICATION_JSON));

        assertInvalidParameter(resultNode);
    }

    @Test
    public void listTestWithoutParam() throws Exception {

        JsonNode resultNode = mockMvcPerform(get("/players/withdraw-condition")
                .contentType(MediaType.APPLICATION_JSON));

        assertInvalidParameter(resultNode);
    }

    @Test
    public void cancelTest() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/players/withdraw-condition/1/cancel"));

        assertOk(resultNode);

        List<WithdrawCondition> unsettledConditions = mapper.getAllUnsettledConditions();
        assertEquals(1, unsettledConditions.size());
    }

}
