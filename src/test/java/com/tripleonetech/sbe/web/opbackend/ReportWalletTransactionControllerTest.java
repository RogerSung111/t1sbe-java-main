package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportWalletTransactionControllerTest extends BaseMainTest {

    @Test
    public void testQuery() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/wallet-transaction")
                .param("createdAtStart", ZonedDateTime.now().plusDays(-2).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString())
                .param("currency", "CNY")
                .param("limit", "10")
                .param("type", Integer.toString(WalletTransactionTypeEnum.DEPOSIT.getCode()))
                .param("page", "1")
                .param("sort", "createdAt")
                .param("asc", "false"));
        assertOk(resultNode);
        
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue(node.get("total").asInt() > 0);

        JsonNode row = node.get("list").get(0);
        assertTrue(row.get("balanceBefore").asDouble() > 0);
        assertTrue(row.get("balanceAfter").asDouble() > 0);

        for (JsonNode aRow : node.get("list")) {
            assertEquals(WalletTransactionTypeEnum.DEPOSIT.getCode(), aRow.get("type").asInt());
        }

        resultNode = mockMvcPerform(get("/reports/wallet-transaction"));
        assertOk(resultNode);

        node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue(node.get("total").asInt() > 0);
    }

    @Test
    public void testQuery_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/wallet-transaction"));
        assertOk(resultNode);
    }
}
