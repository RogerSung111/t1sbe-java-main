package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.payment.PaymentMethod;
import com.tripleonetech.sbe.payment.PaymentMethodForm;
import com.tripleonetech.sbe.payment.PaymentMethodMapper;
import com.tripleonetech.sbe.payment.PaymentMethodTypeEnum;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PaymentMethodControllerTest extends BaseMainTest {
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;

    @Test
    public void testList() throws Exception {
        JsonNode node = mockMvcPerform(get("/payment-methods"));
        assertOk(node);

        JsonNode data = node.get("data");
        assertEquals("There should be 14 payment methods total", 14, data.size());

        IntStream.range(0, data.size()).forEach(i -> {
            assertFalse(data.get(i).get("name").isNull());
            assertFalse(data.get(i).get("type").isNull());
            assertTrue(data.get(i).get("dailyMaxDeposit").isNumber());
            assertTrue(data.get(i).get("minDepositPerTrans").isNumber());
            assertTrue(data.get(i).get("maxDepositPerTrans").isNumber());
            assertTrue(data.get(i).get("chargeRatio").isNumber());
            if(!data.get(i).get("paymentApi").isNull()) {
                assertTrue(data.get(i).get("paymentApi").has("id"));
                assertTrue(data.get(i).get("paymentApi").has("name"));
            }
        });

        assertTrue(data.get(0).get("playerGroups").size() == 3);
        JsonNode playerGroup = data.get(0).get("playerGroups").get(0);
        assertFalse(playerGroup.get("id").isNull());
        assertFalse(playerGroup.get("name").isNull());

        assertTrue(data.get(0).get("playerTags").size() == 1);
        JsonNode playerTag = data.get(0).get("playerTags").get(0);
        assertFalse(playerTag.get("id").isNull());
        assertFalse(playerTag.get("name").isNull());
    }

    @Test
    public void testSearch() throws Exception {
        JsonNode node = mockMvcPerform(get("/payment-methods")
            .param("currency", "CNY")
            .param("name", "人工转帐")
            .param("type", "1")
            .param("playerTagIds", "6")
            .param("playerGroupIds", "1")
            .param("playerBearPaymentCharge", "false")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertEquals("There should be 1 payment methods total", 1, data.size());
    }

    @Test
    public void testDetail() throws Exception {
        JsonNode result = mockMvcPerform(get("/payment-methods/2"));
        assertOk(result);

        JsonNode node = result.get("data");
        assertEquals(0, node.get("players").size());
        assertTrue(node.get("playerGroups").size() > 0);
        assertFalse(node.get("priority").isNull());
        assertTrue(node.get("dailyMaxDeposit").isNumber());
        assertTrue(node.get("minDepositPerTrans").isNumber());
        assertTrue(node.get("maxDepositPerTrans").isNumber());
        assertTrue(node.get("chargeRatio").isNumber());
        assertTrue(node.get("fixedCharge").isNumber());

        JsonNode paymentApiNode = node.get("paymentApi");
        assertFalse(paymentApiNode.get("id").isNull());
        assertFalse(paymentApiNode.get("name").isNull());

        JsonNode playerGroupNode = node.get("playerGroups").get(0);
        assertFalse(playerGroupNode.get("id").isNull());
    }

    @Test
    public void testCreate() throws Exception {
        PaymentMethodForm newPaymentMethod = new PaymentMethodForm();
        newPaymentMethod.setName("BOC");
        newPaymentMethod.setType(PaymentMethodTypeEnum.BANK_TRANSFER.getCode());
        newPaymentMethod.setDailyMaxDeposit(new BigDecimal(10000));
        newPaymentMethod.setMinDepositPerTrans(new BigDecimal(10));
        newPaymentMethod.setMaxDepositPerTrans(new BigDecimal(1000));
        newPaymentMethod.setCurrency(SiteCurrencyEnum.CNY);
        newPaymentMethod.setChargeRatio(new BigDecimal(1));
        newPaymentMethod.setFixedCharge(new BigDecimal(10));
        newPaymentMethod.setPlayerBearPaymentCharge(false);
        newPaymentMethod.setNote("测试note");
        newPaymentMethod.setPriority(3);
        newPaymentMethod.setPlayerIds(Arrays.asList(2,3,4,5));
        newPaymentMethod.setPlayerTagIds(Arrays.asList(6));
        newPaymentMethod.setPlayerGroupIds(Arrays.asList(1,3));

        String requestBody = JsonUtils.objectToJson(newPaymentMethod);

        JsonNode result = mockMvcPerform(post("/payment-methods/create")
                .content(requestBody));
        assertOk(result);
        assertDataSuccess(result.get("data"));
        assertNotNull(result.get("data").get("id"));

        int newId = result.get("data").get("id").asInt();
        PaymentMethod paymentMethod = paymentMethodMapper.get(newId);
        assertEquals(3, paymentMethod.getPlayerTagIds().size());
        assertEquals(4, paymentMethod.getPlayerIds().size());
        assertEquals(newPaymentMethod.getPriority(), paymentMethod.getPriority());
    }

    @Test
    public void testUpdate() throws Exception {
        int paymentMethodId = 1;
        String newPaymentMethodName = "New payment method";
        
        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentMethodId);
        PaymentMethodForm form = new PaymentMethodForm();
        BeanUtils.copyProperties(paymentMethod, form);
        form.setType(paymentMethod.getType().getCode());
        form.setName(newPaymentMethodName);
        form.setPlayerBearPaymentCharge(true);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode result = mockMvcPerform(post("/payment-methods/" + paymentMethodId)
                .content(requestBody));
        assertDataSuccess(result.get("data"));
        
        paymentMethod = paymentMethodMapper.get(paymentMethodId);
        assertTrue(paymentMethod.getName().equals(newPaymentMethodName));
    }

    @Test
    public void testUpdateEnabled() throws Exception {
        int paymentMethodId = 1;
        HashMap<String, Object> params = new HashMap<>();
        params.put("enabled", 1);
        JsonNode result = mockMvcPerform(post("/payment-methods/" + paymentMethodId + "/enabled").content(JsonUtils.objectToJson(params)));
        assertDataSuccess(result.get("data"));
        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentMethodId);
        assertTrue(paymentMethod.isEnabled());
    }

    @Test
    public void testDelete() throws Exception {
        // there is a constraint issue, so if there is any deposit_request existed, the payment_method cannot be
        // deleted.
        int paymentMethodId = 4;
        JsonNode result = mockMvcPerform(post("/payment-methods/" + paymentMethodId + "/delete"));
        this.assertDataSuccess(result.get("data"));

        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentMethodId);
        assertTrue(paymentMethod.isDeleted());
    }

}
