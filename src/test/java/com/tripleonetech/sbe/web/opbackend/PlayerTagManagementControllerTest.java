package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerTagManagementControllerTest extends BaseMainTest {
    private final boolean playerGroup = false;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;


    @Test
    public void testSetTags() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(existingPlayerTags.size() > 1);// 選至少兩個以上的做測試

        List<Integer> addTagIds = Arrays.asList(6, 7);

        JsonNode node = mockMvcPerform(post("/players/{playerId}/tags", playerId)
                .content("{\"tagIds\": " + addTagIds.toString() + "}"));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(addTagIds.size(), updatedPlayerTags.size());
        for (PlayersPlayerTag playersPlayerTag : updatedPlayerTags) {
            Integer playerTagId = playersPlayerTag.getPlayerTagId();
            assertTrue(addTagIds.stream().anyMatch(playerTagId::equals));
            assertTrue(existingPlayerTags.stream().skip(1).noneMatch(playerTagId::equals));
        }
    }

    @Test
    public void testSetTags_EmptyTagId() throws Exception {
        int playerId = 2;

        JsonNode node = mockMvcPerform(post("/players/{playerId}/tags", playerId)
                .content("{\"tagIds\": []}"));
        assertDataFail(node.get("data"));
    }

    @Test
    public void testSetTags_OriginalIsEmpty() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        List<Integer> deleteTagIds = existingPlayerTags.stream()
                .map(PlayersPlayerTag::getPlayerTagId)
                .collect(Collectors.toList());
        playersPlayerTagMapper.delete(playerId, deleteTagIds);
        existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(CollectionUtils.isEmpty(existingPlayerTags));

        List<Integer> addTagIds = Arrays.asList(6, 7, 8);
        JsonNode node = mockMvcPerform(post("/players/{playerId}/tags", playerId)
                .content("{\"tagIds\": " + addTagIds.toString() + " }"));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(addTagIds.size(), updatedPlayerTags.size());
        for (PlayersPlayerTag playerTag : updatedPlayerTags) {
            assertTrue(addTagIds.contains(playerTag.getPlayerTagId()));
        }
    }

    @Test
    public void testAddTag() throws Exception {
        Integer toAddTagId = 7;
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(existingPlayerTags.stream().noneMatch(toAddTagId::equals));

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/tags/add", playerId)
                        .content("{\"tagIds\": [" + toAddTagId + "]}"));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(existingPlayerTags.size() + 1, updatedPlayerTags.size());
        for (PlayersPlayerTag playerTag : updatedPlayerTags) {
            Integer playerTagId = playerTag.getPlayerTagId();
            assertTrue(playerTagId.equals(toAddTagId) || existingPlayerTags.stream().anyMatch(existing -> playerTagId.equals(existing.getPlayerTagId())));
        }
    }

    @Test
    public void testDeleteTag() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(existingPlayerTags.size() > 1);// 選至少兩個以上的做測試

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/tags/{tagId}/delete", playerId, existingPlayerTags.get(0).getPlayerTagId()));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(existingPlayerTags.size() - 1, updatedPlayerTags.size());
        for (PlayersPlayerTag playerTag : updatedPlayerTags) {
            Integer playerTagId = playerTag.getPlayerTagId();
            assertTrue(existingPlayerTags.stream().skip(1).noneMatch(playerTagId::equals));
            assertNotEquals(playerTagId, existingPlayerTags.get(0).getPlayerTagId());
        }
    }


}
