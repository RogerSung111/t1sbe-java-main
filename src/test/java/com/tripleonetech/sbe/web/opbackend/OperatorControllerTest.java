package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.operator.OperatorForm;
import com.tripleonetech.sbe.operator.OperatorMapper;
import com.tripleonetech.sbe.operator.OperatorStatusEnum;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class OperatorControllerTest extends BaseMainTest {
    @Autowired
    private OperatorMapper operatorMapper;
    private Integer operatorId;
    private Operator operator;
    private Operator loginOperator;

    @Before
    public void setUp() {
        operatorId = 2;
        operator = operatorMapper.get(operatorId); // admin
        loginOperator = operatorMapper.get(1); // superadmin
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/operators")
                .param("page", "1")
                .param("limit", "10")
                .param("username", "admin")
                .param("status", "1")
                .param("roleId", "2") // the role for 'admin'
                .param("sort", "role DESC"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data").get("list").get(0);
        assertFalse(data.has("oldPassword"));
        assertFalse(data.has("newPasswrod"));
        assertFalse(data.has("retypePassword"));
        assertEquals(operator.getId().intValue(), data.get("id").asInt());
        assertEquals(operator.getUsername(), data.get("username").asText());
        assertEquals(operator.getStatus(), data.get("status").asInt());
        assertNotNull(data.get("createdAt").asText());
    }

    @Test
    public void testListWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/operators"));
        assertOk(resultNode);

        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/operators/" + operator.getId()));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertFalse(data.has("oldPassword"));
        assertFalse(data.has("password"));
        assertFalse(data.has("confirmPassword"));
        assertEquals(operator.getId().intValue(), data.get("id").asInt());
        assertEquals(operator.getUsername(), data.get("username").asText());
        assertEquals(operator.getStatus(), data.get("status").asInt());
        assertEquals(operator.getRole().getId(), data.get("role").get("id").asInt());
    }

    @Test
    public void testDetailForCurrentOperator() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/operator"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertFalse(data.has("password"));
        assertEquals(loginOperator.getId().intValue(), data.get("id").asInt());
        assertEquals(loginOperator.getUsername(), data.get("username").asText());
        assertEquals(loginOperator.getStatus(), data.get("status").asInt());
        assertEquals(loginOperator.getRole().getId(), data.get("role").get("id").asInt());
    }

    @Test
    public void testResetPassword() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/operators/{operatorId}/password", operatorId));
        assertDataSuccess(resultNode.get("data"));

        Operator operator = operatorMapper.get(operatorId);
        assertTrue(operator.validatePassword(resultNode.get("data").get("password").asText()));
    }

    @Test
    public void testResetPassword_RoleIsNotAdmin_Failed() throws Exception {
        setOperatorUsername("finance"); // same password
        JsonNode resultNode = mockMvcPerform(post("/operators/{operatorId}/password", operatorId));

        // Query should not succeed as default operator username 'finance' does not have operator account write permission
        assertAccessForbidden(resultNode);
    }

    @Test
    public void testChangePassword() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operator/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ " +
                                "\"originalPassword\" : \"abcdef\"," +
                                "\"newPassword\" : \"aaa123\" " +
                                "}")
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Operator operator = operatorMapper.get(1);
        assertTrue(operator.validatePassword("aaa123"));
    }

    @Test
    public void testChangePasswordFail() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operator/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ " +
                                "\"originalPassword\" : \"wrong-password\"," +
                                "\"newPassword\" : \"aaa123\" " +
                                "}")
                        .with(csrf()));
        assertInvalidPassword(resultNode);
    }

    @Test
    public void testUpdateRole() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operators/" + operatorId + "/role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"roleId\" : \"5\"" +
                                "}")
                        .with(csrf()));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Operator operator = operatorMapper.get(operatorId);
        assertEquals(5, operator.getRole().getId());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operators/" + operatorId + "/status")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"status\" : \"0\"" +
                                "}")
                        .with(csrf()));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Operator operator = operatorMapper.get(operatorId);
        assertEquals(OperatorStatusEnum.DISABLED.getCode(), operator.getStatus());
    }

    @Test
    public void testCreate() throws Exception {
        OperatorForm operatorForm = this.prepareOperatorForm();
        operatorForm.setUsername("testcreate");

        JsonNode resultNode = mockMvcPerform(
                post("/operators/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.objectToJson(operatorForm))
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Operator operator = operatorMapper.getByUsername(operatorForm.getUsername());
        assertEquals(operator.getId().intValue(), resultNode.get("data").get("id").asInt());
        assertEquals(operatorForm.getUsername(), operator.getUsername());
        assertEquals(operatorForm.getStatus().intValue(), operator.getStatus());
        assertTrue(operator.validatePassword(operatorForm.getPassword()));
        // There is no role after creation
        //assertEquals(operatorForm.getRoleId().intValue(), operator.getRole().getId());
    }

    @Test
    public void testCreateDuplicateUsername() throws Exception {
        OperatorForm operatorForm = this.prepareOperatorForm();
        operatorForm.setUsername("admin");

        JsonNode resultNode = mockMvcPerform(
                post("/operators/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.objectToJson(operatorForm))
                        .with(csrf()));
        assertUsernameAlreadyExist(resultNode);
        assertDataFail(resultNode.get("data"));
    }

    @Test
    public void testCreateInvalidPassword() throws Exception {
        OperatorForm operatorForm = this.prepareOperatorForm();
        String newPassword = "zxcv";
        operatorForm.setPassword(newPassword);
        operatorForm.setConfirmPassword(newPassword);

        JsonNode resultNode = mockMvcPerform(
                post("/operators/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.objectToJson(operatorForm))
                        .with(csrf()));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testUsernameExists() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/operators/username-available/abcdef")
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        resultNode = mockMvcPerform(
                get("/operators/username-available/superadmin")
                        .with(csrf()));
        assertUsernameAlreadyExist(resultNode);
    }

    private OperatorForm prepareOperatorForm() {
        OperatorForm operatorForm = new OperatorForm();
        operatorForm.setOldPassword("abcdef");
        operatorForm.setPassword("123456");
        operatorForm.setConfirmPassword(operatorForm.getPassword());
        operatorForm.setStatus(OperatorStatusEnum.ENABLED.getCode());
        operatorForm.setRoleId(2); // admin
        return operatorForm;
    }
}
