package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportDepositSummaryControllerTest extends BaseMainTest {
    @Test
    public void testQueryDepositSummaryReport() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/deposit-summary")
                .param("dateStart", LocalDate.now().minusDays(7).toString())
                .param("dateEnd", LocalDate.now().plusDays(1).toString()));

        assertOk(resultNode);
        JsonNode data = resultNode.get("data").get("list");
        for(JsonNode item : data) {
            assertNotNull(item.get("date").asText());
            assertTrue(item.get("depositCount").asInt() > 0);
            assertTrue(item.get("depositorCount").asInt() > 0);
            assertTrue(new BigDecimal(item.get("depositAmount").asText()).compareTo(BigDecimal.ZERO) > 0);
            assertNotNull(item.get("id").asText());
        }
    }
}
