package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.automation.AutomationJob;
import com.tripleonetech.sbe.automation.AutomationJobMapper;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.web.RestResponseCodeEnum;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerTagControllerTest extends BaseMainTest {
    @Autowired
    private PlayerTagMapper playerTagMapper;
    @Autowired
    private AutomationJobMapper automationJobMapper;

    @Test
    public void testListAllTags() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/tags"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.size() > 0);
    }

    @Test
    public void testGet() throws Exception {
        int tagId = 6; // Test Tag
        JsonNode resultNode = mockMvcPerform(get("/tags/{tagId}", tagId));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("tag name should be 'TestTag'", "TestTag", node.get("name").asText());
    }

    @Test
    public void testCreate() throws Exception {
        PlayerTag sampleTag = playerTagMapper.get(6, false);
        sampleTag.setName("testCreate");

        String sampleTagJson = JsonUtils.objectToJson(sampleTag);
        JsonNode resultNode = mockMvcPerform(post("/tags/create").content(sampleTagJson));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        assertFalse(resultNode.get("data").get("id").isNull());

        List<PlayerTag> tags = playerTagMapper.listByNameLike("testCreate");
        assertTrue("there should be one tag named \"testCreate\"", tags.size() > 0);
    }

    @Test
    public void testSave() throws Exception {
        int tagId = 6; // Test Tag
        HashMap<String, String> params = new HashMap<>();
        params.put("name", "testUpdate");
        params.put("automationJobId", "3");
        JsonNode resultNode = mockMvcPerform(post("/tags/{tagId}", tagId).content(JsonUtils.objectToJson(params)));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> tags = playerTagMapper.listByNameLike(params.get("name"));
        assertTrue("there should be one tag named " + params.get("name"), tags.size() > 0);

        tags = playerTagMapper.listByNameLike("Test Tag");
        assertEquals("there should be no tag named Test Tag", 0, tags.size());

        PlayerTag updatedTag = playerTagMapper.get(tagId);
        assertEquals(params.get("name"), updatedTag.getName());
        assertEquals(params.get("automationJobId"), updatedTag.getAutomationJobId().toString());
        AutomationJob automationJob = automationJobMapper.get(1); // get original automation job
        assertEquals("The original automation job should be marked as deleted", true, automationJob.isDeleted());
    }

    @Test
    public void testSaveDeleteAutomation() throws Exception {
        int tagId = 6; // Test Tag, original automation job id = 1
        HashMap<String, String> params = new HashMap<>();
        params.put("name", "testUpdate");
        params.put("automationJobId", null);
        JsonNode resultNode = mockMvcPerform(post("/tags/{tagId}", tagId).content(JsonUtils.objectToJson(params)));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> tags = playerTagMapper.listByNameLike(params.get("name"));
        assertTrue("there should be one tag named " + params.get("name"), tags.size() > 0);

        PlayerTag updatedTag = tags.get(0);
        assertNull("Updated tag should become a manual tag without automation job", updatedTag.getAutomationJobId());

        AutomationJob automationJob = automationJobMapper.get(1); // get original automation job
        assertEquals("The original automation job should be marked as deleted", true, automationJob.isDeleted());
    }

    @Test
    public void testDelete() throws Exception {
        int tagId = 6; // Test Tag
        JsonNode resultNode = mockMvcPerform(post("/tags/{tagId}/delete", tagId));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> tags = playerTagMapper.listByNameLike("Test Tag");
        assertEquals("there should be no tag named Test Tag", 0, tags.size());

        AutomationJob automationJob = automationJobMapper.get(1);
        assertEquals(true, automationJob.isDeleted());
    }

    @Test
    public void testDelete_DefaultTag_Failed() throws Exception {
        int tagId = 5; // Test Tag
        JsonNode resultNode = mockMvcPerform(post("/tags/{tagId}/delete", tagId));
        assertNotEquals(RestResponseCodeEnum.OK.getCode(), resultNode.get("code").asInt());
    }
}
