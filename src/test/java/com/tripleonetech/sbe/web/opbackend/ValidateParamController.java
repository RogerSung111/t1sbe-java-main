package com.tripleonetech.sbe.web.opbackend;

import java.text.ParseException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/test/validation")
public class ValidateParamController {
    private static final Logger logger = LoggerFactory.getLogger(ValidateParamController.class);

    // With @Validated, we can validate request parameters and path variables.
    @GetMapping("/req-param")
    public ValidationTestForm validateReqParam(@NotBlank @RequestParam("name") String name) throws ParseException {
        logger.debug("@RequestParam name[{}]", name);
        return new ValidationTestForm();
    }

    // With @Valid, we can validate content of java bean.
    @PostMapping("/json")
    public ValidationTestForm validateJson(@Valid @RequestBody ValidationTestForm myForm) throws ParseException {
        logger.debug("@RequestBody username[{}]", myForm.username);
        return myForm;
    }

    public static class ValidationTestForm {
        @NotBlank
        private String username;
        @NotBlank
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
