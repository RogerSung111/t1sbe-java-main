package com.tripleonetech.sbe.web.opbackend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.GameNameMapper;
import com.tripleonetech.sbe.game.GameNameQueryForm;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.tripleonetech.sbe.web.opbackend.GameNameController.GameNameSortForm;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class GameNameControllerTest extends BaseMainTest {
    @Autowired
    private GameNameMapper gameNameMapper;

    @Test
    public void testList() throws Exception {
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameApiCode("T1GPT");
        formObj.setPage(1);
        formObj.setLimit(10);

        JsonNode resultNode = mockMvcPerform(get("/games")
                .param("gameApiCode", formObj.getGameApiCode())
                .param("gameName", formObj.getGameName())
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString())
                .param("sort", "gameTypeName")
        );

        assertOk(resultNode);
        assertTrue(resultNode.size() > 0);

        JsonNode node = resultNode.get("data");
        assertTrue("Must return a non-empty list", node.get("total").asInt() > 0);
        JsonNode data0 = node.get("list").get(0);
        assertEquals(formObj.getGameApiCode(), data0.get("gameApiCode").asText());
        assertTrue(data0.has("gameImgUrl"));
        assertTrue(data0.has("featured"));
        assertTrue(data0.has("releasedDate"));
        assertTrue(data0.has("sort"));
    }

    @Test
    public void testListWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/games"));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testList2() throws Exception {
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameTypeId(2);
        formObj.setPage(1);
        formObj.setLimit(10);

        JsonNode resultNode = mockMvcPerform(get("/games")
                .param("gameTypeId", Integer.toString(formObj.getGameTypeId()))
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString()));
        assertOk(resultNode);
        assertTrue(resultNode.size() > 0);

        JsonNode node = resultNode.get("data");
        assertTrue(node.get("total").asInt() > 0);
        assertEquals(formObj.getGameTypeId().toString(), node.get("list").get(0).get("gameTypeId").asText());
    }

    @Test
    public void testBatchUpdateSort() throws Exception {
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameApiCode("T1GPT");
        formObj.setPage(1);
        formObj.setLimit(5);

        JsonNode resultNode = mockMvcPerform(get("/games")
                .param("gameApiCode", formObj.getGameApiCode())
                .param("gameName", formObj.getGameName())
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString()));
        JsonNode listNode = resultNode.get("data").get("list");

        List<GameNameSortForm> sortForm = new ArrayList<>();

        GameNameSortForm f1 = new GameNameSortForm();
        JsonNode expectFirst = listNode.get(listNode.size() - 2);
        f1.setGameNameId(expectFirst.get("gameNameId").asInt());
        f1.setSort(999);
        sortForm.add(f1);

        GameNameSortForm f2 = new GameNameSortForm();
        JsonNode expectSecond = listNode.get(listNode.size() - 1);
        f2.setGameNameId(expectSecond.get("gameNameId").asInt());
        f2.setSort(99);
        sortForm.add(f2);

        JsonNode result = mockMvcPerform(post("/games/update-sort").content(JsonUtils.objectToJson(sortForm)));
        assertOk(result);

        resultNode = mockMvcPerform(get("/games")
                .param("gameApiCode", formObj.getGameApiCode())
                .param("gameName", formObj.getGameName())
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString()));

        listNode = resultNode.get("data").get("list");
        assertEquals(expectFirst.get("gameNameId"), listNode.get(0).get("gameNameId"));
        assertEquals(expectSecond.get("gameNameId"), listNode.get(1).get("gameNameId"));
    }

    @Test
    public void testBatchUpdateSortWithWrongParam() throws Exception {
        GameNameSortForm f1 = new GameNameSortForm();
        f1.setGameNameId(4792);

        JsonNode result = mockMvcPerform(post("/games/update-sort")
                .content(JsonUtils.objectToJson(Collections.singletonList(f1))));
        assertInvalidParameter(result);

        GameNameSortForm f2 = new GameNameSortForm();
        f1.setSort(999);

        result = mockMvcPerform(post("/games/update-sort")
                .content(JsonUtils.objectToJson(Collections.singletonList(f2))));
        assertInvalidParameter(result);
    }

    @Test
    public void testDisable() throws Exception {
        int limit = 5;
        GameNameQueryForm formObj = new GameNameQueryForm();
        formObj.setGameApiCode("T1GPT");
        formObj.setPage(1);
        formObj.setLimit(limit);

        JsonNode resultNode = mockMvcPerform(get("/games")
                .param("gameApiCode", formObj.getGameApiCode())
                .param("gameName", formObj.getGameName())
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString()));

        JsonNode list = resultNode.get("data").get("list");
        List<Integer> data = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            data.add(list.get(i).get("gameNameId").asInt());
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("gameNameIds", data);
        params.put("enabled", false);
        JsonNode result = mockMvcPerform(post("/games/enabled").content(JsonUtils.objectToJson(params)));
        assertOk(result);

        resultNode = mockMvcPerform(get("/games")
                .param("gameApiCode", formObj.getGameApiCode())
                .param("gameName", formObj.getGameName())
                .param("page", formObj.getPage().toString())
                .param("limit", formObj.getLimit().toString()));
        assertFalse(resultNode.get("data").get("list").get(0).get("userEnabled").asBoolean());
    }

    @Test
    public void testAssignGameType() throws Exception {
        GameName condition = new GameName();
        condition.setGameTypeId(4);
        List<GameName> gameNames = gameNameMapper.list(condition);
        List<Integer> gameNameIds = gameNames.stream().map(GameName::getId).collect(Collectors.toList());

        Integer gameTypeId = 2;
        HashMap<String, Object> params = new HashMap<>();
        params.put("gameNameIds", gameNameIds);
        params.put("gameTypeId", gameTypeId);
        JsonNode result = mockMvcPerform(post("/games/assign").content(JsonUtils.objectToJson(params)));
        assertOk(result);

        condition.setGameTypeId(gameTypeId);
        List<GameName> updatedGameNames = gameNameMapper.list(condition);
        gameNameIds.forEach(id -> assertTrue(updatedGameNames.stream().anyMatch(updated -> id.equals(updated.getId()))));
    }
}
