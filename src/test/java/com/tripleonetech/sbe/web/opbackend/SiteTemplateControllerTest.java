package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.web.RestResponseCodeEnum;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class SiteTemplateControllerTest extends BaseMainTest {

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @Test
    public void setTemplateActiveTest() throws Exception {
        String testTemplate = "new2";
        JsonNode resultNode = mockMvcPerform(post("/site-templates/{templateName}/active", testTemplate));

        assertOk(resultNode);

        List<SitePrivilege> templates = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_TEMPLATE);
        Optional<SitePrivilege> currentTemplate = templates.stream().filter(SitePrivilege::isActive).findFirst();
        assertTrue(currentTemplate.isPresent());
        assertEquals(testTemplate, currentTemplate.get().getValue());
    }

    @Test
    public void setNoneExistTemplateActiveTest() throws Exception {
        String testTemplate = "none-exist";
        JsonNode resultNode = mockMvcPerform(post("/site-templates/{templateName}/active", testTemplate));
        assertDataFail(resultNode.get("data"));
        assertEquals(RestResponseCodeEnum.TEMPLATE_NOT_FOUND.getCode(), resultNode.get("code").asInt());
    }

}
