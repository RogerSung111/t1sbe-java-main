package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSetting;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSettingMapper;
import com.tripleonetech.sbe.promo.referral.ReferralTypeEnum;
import com.tripleonetech.sbe.web.opbackend.PromoReferralController.PromoPlayerReferralSettingView;
import com.tripleonetech.sbe.web.opbackend.PromoReferralController.ReferralSettingForm;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PromoReferralControllerTest extends BaseMainTest {
    @Autowired
    private PromoPlayerReferralSettingMapper promoPlayerReferralSettingMapper;

    private final int mockDataSize = 6;

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/referral/list"));
        JsonNode node = resultNode.get("data");

        assertEquals(mockDataSize, node.size());
        JsonNode jsonNode = node.get(0);
        assertTrue(jsonNode.get("playerId").isNull());
    }

    @Test
    public void testListByCredentialId() throws Exception {
        int playerId = 1;
        JsonNode resultNode = mockMvcPerform(get("/promotions/referral/list/{playerId}", playerId));
        JsonNode node = resultNode.get("data");

        assertEquals(1, node.size());
        JsonNode jsonNode = node.get(0);

        assertEquals(playerId, jsonNode.get("playerId").asInt());
        assertEquals(ReferralTypeEnum.DEPOSIT_REVENUE.getCode(), jsonNode.get("referralType").asInt());
        assertEquals(SiteCurrencyEnum.CNY.toString(), jsonNode.get("currency").asText());
        assertTrue(jsonNode.get("enabled").asBoolean());
    }

    @Test
    public void testListByDefaultCredentialId() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/referral/list/{playerId}", 0));
        JsonNode node = resultNode.get("data");

        assertEquals(3, node.size());
        JsonNode jsonNode = node.get(0);

        assertTrue(jsonNode.get("playerId").isNull());
        assertEquals(ReferralTypeEnum.BET_REVENUE.getCode(), jsonNode.get("referralType").asInt());
        assertEquals(SiteCurrencyEnum.CNY.toString(), jsonNode.get("currency").asText());
        assertTrue(jsonNode.get("enabled").asBoolean());
    }

    @Test
    public void testDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/referral/{id}", 1));
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("name"));
        assertNotNull(node.get("referralType"));
        assertNotNull(node.get("currency"));
        assertNotNull(node.get("playerId"));
    }

    @Test
    public void testCreate() throws Exception {
        ReferralSettingForm newRule = new ReferralSettingForm();
        newRule.setName("test");
        newRule.setCurrency(SiteCurrencyEnum.USD);
        newRule.setReferralType(ReferralTypeEnum.DEPOSIT_REVENUE.getCode());
        newRule.setBonusPercentage(new BigDecimal("10"));
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);
        newRule.setWithdrawConditionMultiplier(1);

        String requestBody = JsonUtils.objectToJson(newRule);
        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        PromoPlayerReferralSetting playerReferralSetting = promoPlayerReferralSettingMapper.get(mockDataSize + 1);
        assertEquals(newRule.getName(), playerReferralSetting.getName());
        assertEquals(newRule.getCurrency(), playerReferralSetting.getCurrency());
        assertEquals(newRule.getReferralType(), playerReferralSetting.getReferralType());
        assertEquals(newRule.getPlayerId(), playerReferralSetting.getPlayerCredentialId());
        assertEquals(playerReferralSetting.getId().intValue(), resultNode.get("data").get("id").asInt());
    }

    @Test
    public void testCreateWithNoRequiredParam() throws Exception {
        ReferralSettingForm newRule = new ReferralSettingForm();
        newRule.setCurrency(SiteCurrencyEnum.USD);
        newRule.setReferralType(ReferralTypeEnum.DEPOSIT_REVENUE.getCode());
        newRule.setBonusPercentage(new BigDecimal("10"));
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);
        newRule.setWithdrawConditionMultiplier(1);

        // no name
        String requestBody = JsonUtils.objectToJson(newRule);
        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/create").contentType(MediaType.APPLICATION_JSON).content(requestBody)
        );
        assertInvalidParameter(resultNode);

        newRule = new ReferralSettingForm();
        newRule.setName("test");
        newRule.setReferralType(ReferralTypeEnum.DEPOSIT_REVENUE.getCode());
        newRule.setBonusPercentage(new BigDecimal("10"));
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);
        newRule.setWithdrawConditionMultiplier(1);

        // no currency
        requestBody = JsonUtils.objectToJson(newRule);
        resultNode = mockMvcPerform(
                post("/promotions/referral/create").contentType(MediaType.APPLICATION_JSON).content(requestBody)
        );
        assertInvalidParameter(resultNode);

        newRule = new ReferralSettingForm();
        newRule.setName("test");
        newRule.setCurrency(SiteCurrencyEnum.USD);
        newRule.setBonusPercentage(new BigDecimal("10"));
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);
        newRule.setWithdrawConditionMultiplier(1);

        // no referralType
        requestBody = JsonUtils.objectToJson(newRule);
        resultNode = mockMvcPerform(
                post("/promotions/referral/create").contentType(MediaType.APPLICATION_JSON).content(requestBody)
        );
        assertInvalidParameter(resultNode);

        newRule = new ReferralSettingForm();
        newRule.setName("test");
        newRule.setCurrency(SiteCurrencyEnum.USD);
        newRule.setReferralType(ReferralTypeEnum.DEPOSIT_REVENUE.getCode());
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);
        newRule.setWithdrawConditionMultiplier(1);

        // no bonusPercentage
        requestBody = JsonUtils.objectToJson(newRule);
        resultNode = mockMvcPerform(
                post("/promotions/referral/create").contentType(MediaType.APPLICATION_JSON).content(requestBody)
        );
        assertInvalidParameter(resultNode);

        newRule = new ReferralSettingForm();
        newRule.setName("test");
        newRule.setCurrency(SiteCurrencyEnum.USD);
        newRule.setReferralType(ReferralTypeEnum.DEPOSIT_REVENUE.getCode());
        newRule.setBonusPercentage(new BigDecimal("10"));
        newRule.setMaxBonusAmount(new BigDecimal("100"));
        newRule.setMinBonusAmount(new BigDecimal("1"));
        newRule.setBonusAutoRelease(true);
        newRule.setPlayerId(1);

        // no withdrawConditionMultiplier
        requestBody = JsonUtils.objectToJson(newRule);
        resultNode = mockMvcPerform(
                post("/promotions/referral/create").contentType(MediaType.APPLICATION_JSON).content(requestBody)
        );
        assertInvalidParameter(resultNode);

    }

    @Test
    public void testUpdate() throws Exception {
        int id = 1;
        String name = "test";
        PromoPlayerReferralSettingView referralSetting = new PromoPlayerReferralSettingView();
        referralSetting.setId(id);
        referralSetting.setName(name);
        referralSetting.setBonusAutoRelease(true);
        String requestBody = JsonUtils.objectToJson(referralSetting);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        PromoPlayerReferralSetting playerReferralSetting = promoPlayerReferralSettingMapper.get(id);

        assertEquals(name, playerReferralSetting.getName());
        assertTrue(playerReferralSetting.isBonusAutoRelease());
    }

    @Test
    public void testUpdateWithOutOfRangeParam() throws Exception {
        int id = 1;
        PromoPlayerReferralSettingView referralSetting = new PromoPlayerReferralSettingView();
        referralSetting.setBonusPercentage(new BigDecimal(100000));
        String requestBody = JsonUtils.objectToJson(referralSetting);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testUpdateEnabled() throws Exception {
        int id = 1;
        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/{id}/enabled", id)
                        .content("{\"enabled\":0}"));
        assertOk(resultNode);

        PromoPlayerReferralSetting playerReferralSetting = promoPlayerReferralSettingMapper.get(id);
        assertFalse(playerReferralSetting.isEnabled());
    }

    @Test
    public void testDelete() throws Exception {
        int id = 1;
        JsonNode resultNode = mockMvcPerform(
                post("/promotions/referral/{id}/delete", id));
        assertOk(resultNode);
        assertNull(promoPlayerReferralSettingMapper.get(id));
    }
}
