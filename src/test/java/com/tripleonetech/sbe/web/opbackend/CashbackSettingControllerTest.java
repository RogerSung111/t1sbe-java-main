package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.model.MultiCurrencyAmount;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.cashback.CashbackExecutionTypeEnum;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSetting;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingForm;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class CashbackSettingControllerTest extends BaseMainTest {
    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;

    @Test
    public void testGetCashbackSettings() throws Exception {
        JsonNode result = mockMvcPerform(get("/promotions/cashback-setting"));
        assertOk(result);
        JsonNode data = result.get("data");
        assertFalse(data.has("id"));
        assertTrue(data.get("executionType").isInt());
        assertTrue(data.get("minBonusAmount").isArray());
        assertTrue(data.get("maxBonusAmount").isArray());
        assertTrue(data.has("withdrawConditionMultiplier"));
        assertTrue(data.has("dailyCashbackStartTime"));
        assertTrue(data.has("nextCalculationTime"));
    }

    @Test
    public void testUpdateCashbackSetting() throws Exception {
        PromoCashbackSettingForm form = setupParamForm();

        JsonNode result = mockMvcPerform(post("/promotions/cashback-setting").content(JsonUtils.objectToJson(form)));
        assertOk(result);

        // Cashback setting should be updated successfully
        PromoCashbackSetting setting = promoCashbackSettingMapper.get();
        assertEquals(CashbackExecutionTypeEnum.REALTIME, setting.getExecutionTypeEnum());
        assertEquals(form.getEnabled(), setting.isEnabled());
        assertEquals(100.00, setting.getMinBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(), 0.01);
        assertEquals(1000.00, setting.getMaxBonusAmount().getAmount(SiteCurrencyEnum.CNY).floatValue(), 0.01);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUpdateCashbackSetting_EmptyAmountList_Failed() throws Exception {
        PromoCashbackSettingForm form = setupParamForm();
        form.setMinBonusAmount(new MultiCurrencyAmount(
                Collections.EMPTY_LIST));
        form.setMaxBonusAmount(new MultiCurrencyAmount(
                Collections.EMPTY_LIST));

        JsonNode result = mockMvcPerform(post("/promotions/cashback-setting").content(JsonUtils.objectToJson(form)));
        assertInvalidParameter(result);
        assertTrue(result.get("data").get("message").asText().contains("must not be empty"));
    }

    @Test
    public void testUpdateCashbackSetting_NullAmount_Failed() throws Exception {
        PromoCashbackSettingForm form = setupParamForm();
        form.setMinBonusAmount(new MultiCurrencyAmount(
                Collections.singletonList(
                        new MultiCurrencyAmount.CurrencyAmount())));
        form.setMaxBonusAmount(new MultiCurrencyAmount(
                Collections.singletonList(
                        new MultiCurrencyAmount.CurrencyAmount())));

        JsonNode result = mockMvcPerform(post("/promotions/cashback-setting").content(JsonUtils.objectToJson(form)));
        assertInvalidParameter(result);
        assertTrue(result.get("data").get("message").asText().contains("must not be null"));
    }

    private PromoCashbackSettingForm setupParamForm() {
        PromoCashbackSettingForm form = new PromoCashbackSettingForm();
        form.setExecutionType(CashbackExecutionTypeEnum.REALTIME.getCode());
        form.setMinBonusAmount(new MultiCurrencyAmount(
                Collections.singletonList(
                        new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.CNY, "100.00"))));
        form.setMaxBonusAmount(new MultiCurrencyAmount(
                Collections.singletonList(
                        new MultiCurrencyAmount.CurrencyAmount(SiteCurrencyEnum.CNY, "1000.00"))));
        form.setEnabled(true);
        return form;
    }
}
