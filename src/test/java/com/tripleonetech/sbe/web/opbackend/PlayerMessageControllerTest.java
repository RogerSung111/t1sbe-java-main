package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerMessageControllerTest extends BaseMainTest {

    @Autowired
    private PlayerMessageService playerMessageService;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;
    @Autowired
    private PlayerMapper playerMapper;

    @Autowired
    private PlayerMessageTemplateSettingMapper playerMessageTemplateSettingMapper;

    private static final int TEST_MESSAGE_ID = 2;
    private static final int PLAYER_ID = 2;
    private static final String TEST_SUBJECT = "Unit test subject";
    private static final String TEST_MESSAGE = "Unit test message";
    private static List<PlayerMessageView> resultView;

    @Test
    public void testQueryMessage() throws Exception {
        String playerUsername = playerMapper.get(PLAYER_ID).getUsername();
        JsonNode resultNode = mockMvcPerform(get("/messages")
                .param("playerUsername", playerUsername));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals("Failed asserting that playerUsername is: " + playerUsername,
                node.get("list").get(0).get("playerUsername").asText(), playerUsername);
        assertFalse(node.get("list").get(0).get("read").asBoolean());
    }

    @Test
    public void testQueryMessageWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void testQueryThread() throws Exception {
        String playerUsername = playerMapper.get(PLAYER_ID).getUsername();
        JsonNode resultNode = mockMvcPerform(get("/messages/threads")
                .param("playerId", "" + PLAYER_ID)
                .param("createdAtStart", ZonedDateTime.now().minusDays(1).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals("Player username should be: " + playerUsername,
                playerUsername, node.get("list").get(0).get("player").get("name").asText());
    }

    @Test
    public void testQueryThreadWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages/threads"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void testGetThread() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages/threads/2"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Should return 2 messages under thread[2]", 2, node.size());
    }

    @Test
    public void testSendMessage() throws Exception {
        PlayerMessageController.MessageForm form = new PlayerMessageController.MessageForm();
        form.setSubject(TEST_SUBJECT);
        form.setContent(TEST_MESSAGE);
        form.setPlayerId(PLAYER_ID);
        form.setCurrency(SiteCurrencyEnum.CNY);
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/messages/send")
                .content(requestBodyStr));

        assertOk(resultNode);

        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();
        queryForm.setPlayerId(PLAYER_ID);
        queryForm.setSubject(TEST_SUBJECT);
        resultView = playerMessageService.listMessage(queryForm);

        PlayerMessageView message = resultView.get(0);
        assertEquals("The content of player message should be: " + TEST_MESSAGE,
                TEST_MESSAGE, message.getContent());
        assertEquals("The message should start a new thread (id = thread id)",
                message.getId(),
                message.getThreadId());
        assertEquals(message.getId().intValue(), resultNode.get("data").get("id").asInt());
        assertFalse("message should be unread", message.isRead());
    }

    @Test
    public void testSendMessageWithoutRequireParam() throws Exception {
        PlayerMessageController.MessageForm form = new PlayerMessageController.MessageForm();
        form.setContent(TEST_MESSAGE);
        form.setPlayerId(PLAYER_ID);
        form.setCurrency(SiteCurrencyEnum.CNY);
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/messages/send").content(requestBodyStr));
        assertInvalidParameter(resultNode);


        form = new PlayerMessageController.MessageForm();
        form.setSubject(TEST_SUBJECT);
        form.setPlayerId(PLAYER_ID);
        form.setCurrency(SiteCurrencyEnum.CNY);
        requestBodyStr = JsonUtils.objectToJson(form);

        resultNode = mockMvcPerform(post("/messages/send").content(requestBodyStr));
        assertInvalidParameter(resultNode);


        form = new PlayerMessageController.MessageForm();
        form.setSubject(TEST_SUBJECT);
        form.setContent(TEST_MESSAGE);
        form.setCurrency(SiteCurrencyEnum.CNY);
        requestBodyStr = JsonUtils.objectToJson(form);

        resultNode = mockMvcPerform(post("/messages/send").content(requestBodyStr));
        assertInvalidParameter(resultNode);


        form = new PlayerMessageController.MessageForm();
        form.setSubject(TEST_SUBJECT);
        form.setContent(TEST_MESSAGE);
        form.setPlayerId(PLAYER_ID);
        requestBodyStr = JsonUtils.objectToJson(form);

        resultNode = mockMvcPerform(post("/messages/send").content(requestBodyStr));
        assertInvalidParameter(resultNode);


        form = new PlayerMessageController.MessageForm();
        requestBodyStr = JsonUtils.objectToJson(form);

        resultNode = mockMvcPerform(post("/messages/send").content(requestBodyStr));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testReplyMessage() throws Exception {
        PlayerMessageController.MessageContentForm message = new PlayerMessageController.MessageContentForm();
        message.setContent("Unit test reply message");
        String requestBodyStr = JsonUtils.objectToJson(message);

        // Replying to message id = 2
        JsonNode resultNode = mockMvcPerform(post("/messages/{id}/reply", TEST_MESSAGE_ID)
                .content(requestBodyStr));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);

        PlayerMessageThreadQueryForm form = new PlayerMessageThreadQueryForm();
        form.setPlayerId(2);
        PlayerMessageThreadView playerMessageView = playerMessageMapper.queryThreadsWithMessages(form).get(0);
        assertEquals("The thread should now contain 3 messages",
                3, playerMessageView.getMessages().size());

        PlayerMessageView latestMessage = playerMessageView.getMessages().get(0);
        assertEquals("Latest message should have the same content as the message we just added",
                message.getContent(), latestMessage.getContent());
        assertEquals(latestMessage.getId().intValue(), resultNode.get("data").get("id").asInt());
        assertFalse("Latest message should be unread", latestMessage.isRead());
        assertNotNull("Latest message should have operatorId", latestMessage.getOperatorId());
    }

    @Test
    public void testDeleteMessage() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/messages/{id}/delete", TEST_MESSAGE_ID));

        assertOk(resultNode);
        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();

        queryForm.setId(TEST_MESSAGE_ID);
        resultView = playerMessageService.listMessage(queryForm);

        assertTrue("Test fail! The message should be flagged as deleted!", resultView.get(0).isDeleted());
    }

    @Test
    public void testBatchDeleteMessage() throws Exception {
        List<Integer> deleteMsgIds = Arrays.asList(1, 2);

        JsonNode resultNode = mockMvcPerform(post("/messages/delete-batch")
                .content("{\"messageIds\": " + deleteMsgIds.toString() + "}"));

        assertOk(resultNode);
        PlayerMessageQueryForm queryForm = new PlayerMessageQueryForm();

        queryForm.setDeleted(true);
        List<PlayerMessageView> result = playerMessageService.listMessage(queryForm);

        assertTrue(result.stream().anyMatch(m -> m.getId() == 1));
        assertTrue(result.stream().anyMatch(m -> m.getId() == 2));
    }

    @Test
    public void testBatchDeleteMessageWithEmptyParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/messages/delete-batch")
                .content("{\"messageIds\": []}"));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testListTemplates() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages/templates"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.size() > 0);
    }

    @Test
    public void testUpdateContent() throws Exception {
        int templateId = 1;
        String testContent = "test update content";
        String testSubject = "test update subject";

        JsonNode resultNode = mockMvcPerform(post("/messages/templates/{templateId}", templateId)
                .content("{\"content\":\"" + testContent + "\", \"subject\":\"" + testSubject + "\"}"));

        assertOk(resultNode);

        PlayerMessageTemplateSetting playerMessageTemplateSetting = playerMessageTemplateSettingMapper.get(templateId);
        assertEquals("subject should be " + testSubject, testSubject, playerMessageTemplateSetting.getSubject());
        assertEquals("content should be " + testContent, testContent, playerMessageTemplateSetting.getContent());
    }
}
