package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopDepositCategoryEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopPopularGameCategoryEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopWithdrawCategoryEnum;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class DashboardControllerTest extends BaseMainTest {

    @Test
    public void testSummaryDaily() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/dashboard")
                .param("currency", SiteCurrencyEnum.CNY.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull("Failed asserting that totalPlayerCount is not null",
                node.findValue("totalPlayerCount").asInt());
    }

    @Test
    public void testSummaryBetGorssProfitStatus() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/dashboard/betgrossprofit")
                .param("currency", SiteCurrencyEnum.CNY.toString())
                .param("period", SummaryPeriodTypeEnum.LAST_WEEK.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 8, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("betAmount"));
    }

    @Test
    public void testSummaryPlayerStatus() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/playerstatus")
                        .param("period", SummaryPeriodTypeEnum.LAST_WEEK.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 8, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("newPlayerWithoutDeposit").asLong());
    }

    @Test
    public void testSummaryNetProfit() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/netprofit")
                        .param("period", SummaryPeriodTypeEnum.LAST_WEEK.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 8, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("cashback").asLong());
    }

    @Test
    public void testSummaryTopDeposit() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/topdeposit")
                        .param("period", SummaryPeriodTypeEnum.LAST_7_DAYS.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString())
                        .param("category", SummaryTopDepositCategoryEnum.BY_COUNT.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("playerUsername").asText());
        assertNotNull(node.findValue("depositCount").asLong());
        assertNotNull(node.findValue("depositMethod").asLong());
    }

    @Test
    public void testSummaryTopWithdrawal() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/topwithdrawal")
                        .param("period", SummaryPeriodTypeEnum.LAST_7_DAYS.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString())
                        .param("category", SummaryTopWithdrawCategoryEnum.BY_COUNT.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("playerUsername").asText());
        assertNotNull(node.findValue("withdrawalCount").asLong());
        assertNotNull(node.findValue("withdrawalAmount").asLong());
    }

    @Test
    public void testSummaryTopWinner() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/topwinner")
                        .param("period", SummaryPeriodTypeEnum.LAST_7_DAYS.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("playerUsername").asText());
        assertNotNull(node.findValue("playerWin").asLong());
    }

    @Test
    public void testSummaryTopPopularGame() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/toppopulargame")
                        .param("period", SummaryPeriodTypeEnum.LAST_7_DAYS.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString())
                        .param("category", SummaryTopPopularGameCategoryEnum.BY_PLAYER.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("gameApiCode").asText());
        assertNotNull(node.findValue("playerCount").asLong());
    }

    @Test
    public void testSummaryTopPopularGameWithLocale() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/toppopulargame")
                        .param("period", SummaryPeriodTypeEnum.LAST_7_DAYS.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString())
                        .param("category", SummaryTopPopularGameCategoryEnum.BY_PLAYER.toString())
                        .param("locale", "zh-CN"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("gameApiCode").asText());
        assertNotNull(node.findValue("playerCount").asLong());
    }

    @Test
    public void testSummaryGameProviderInfo() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/gameproviderinfo")
                        .param("period", SummaryPeriodTypeEnum.TODAY.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString()));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("gameApiId").asText());
        assertNotNull(node.findValue("win").asLong());
    }

    @Test
    public void testSummaryGameProviderInfoWithLocale() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/dashboard/gameproviderinfo")
                        .param("period", SummaryPeriodTypeEnum.TODAY.toString())
                        .param("currency", SiteCurrencyEnum.CNY.toString())
                        .param("locale", "zh-CN"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("Failed!", 10, node.size());
        node = node.get(0);
        assertNotNull(node.findValue("gameApiId").asText());
        assertNotNull(node.findValue("win").asLong());
    }
}
