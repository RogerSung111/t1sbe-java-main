package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestApprovalForm;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.PaymentRequestForm.OperatorPaymentRequestForm;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class DepositRequestControllerTest extends BaseMainTest {
    @Autowired
    private DepositRequestMapper depositRequestMapper;

    private final long DEPOSIT_REQUEST_ID = 100002;
    private final long EXPIRED_DEPOSIT_REQUEST_ID = 100001;

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/deposit-requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("requestedDateStart", ZonedDateTime.now().minusDays(5).toString())
                        .param("requestedDateEnd", ZonedDateTime.now().toString())
                        .param("username", "test")
                        .param("currency", "CNY")
                        .param("paymentApiIds", "1", "2", "3", "10")
                        .param("page", "1")
                        .param("limit", "20")
                        .param("sort", "requested_date"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue( node.get("total").asInt() >= 1);

        JsonNode items =node.get("list");
        assertNotNull(items);

        JsonNode aRequest = items.get(0);
        assertTrue(aRequest.get("paymentMethod").get("paymentApiId").asInt() > 0);
        assertFalse(aRequest.get("currency").isNull());

        for(int i = 0; i < node.get("total").asInt(); i++) {
            JsonNode depositRequest = items.get(i);
            assertNotEquals("Result should not contain expired deposit requests",
                    DepositStatusEnum.EXPIRED.getCode(), depositRequest.get("status").asInt());
        }
    }

    @Test
    public void testListExpired() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/deposit-requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("status", DepositStatusEnum.EXPIRED.getCode() + "")
                        .param("page", "1")
                        .param("limit", "20")
                        .param("sort", "requested_date"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue( node.get("total").asInt() >= 1);
        JsonNode items =node.get("list");
        assertNotNull(items);

        for(int i = 0; i < node.get("total").asInt(); i++) {
            JsonNode depositRequest = items.get(i);
            assertEquals("Result should only contain expired deposit requests",
                    DepositStatusEnum.EXPIRED.getCode(), depositRequest.get("status").asInt());
        }
    }

    @Test
    public void testListNoParam() throws Exception {

        JsonNode resultNode = mockMvcPerform(
                get("/deposit-requests")
                        .contentType(MediaType.APPLICATION_JSON));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue( node.get("total").asInt() > 1);

        JsonNode items =node.get("list");
        assertNotNull(items);

        assertTrue(items.size() > 0);
    }

    @Test
    public void testGetDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/deposit-requests/" + this.DEPOSIT_REQUEST_ID));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(this.DEPOSIT_REQUEST_ID, node.findValue("id").asInt());
        assertFalse(node.get("username").isNull());
        assertEquals("This payment request status is open", DepositStatusEnum.OPEN.getCode(), node.get("status").asInt());
    }

    @Test
    public void testSubmitPaymentRequest() throws Exception {
        OperatorPaymentRequestForm form = new OperatorPaymentRequestForm();
        form.setPaymentMethodId(1);
        form.setAmount(new BigDecimal(100));
        form.setPlayerId(1);

        String paymentStr = JsonUtils.objectToJson(form);
        JsonNode resultNode = mockMvcPerform(post("/deposit-requests/payment-requests")
                .contentType(MediaType.APPLICATION_JSON).content(paymentStr));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(100, node.get("params").get("amount").asInt());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        DepositRequestApprovalForm form = new DepositRequestApprovalForm();
        form.setStatus(DepositStatusEnum.REJECTED.getCode());
        form.setComment("payment receipt is rejected");
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/deposit-requests/" + this.DEPOSIT_REQUEST_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertOk(resultNode);
        DepositRequest depositRequest = depositRequestMapper.get(this.DEPOSIT_REQUEST_ID);
        assertTrue("The status of deposit_request should be [REJECT] ", depositRequest.getStatus() == DepositStatusEnum.REJECTED);
    }

    @Test
    public void testUpdateExpiredStatusFail() throws Exception {

        DepositRequestApprovalForm form = new DepositRequestApprovalForm();
        form.setStatus(DepositStatusEnum.REJECTED.getCode());
        form.setComment("payment receipt is rejected");
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/deposit-requests/" + this.EXPIRED_DEPOSIT_REQUEST_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertInvalidStatus(resultNode);
    }


    @Test
    public void testUpdateApproveStatusWithoutOtherParam() throws Exception {

        DepositRequestApprovalForm form = new DepositRequestApprovalForm();
        form.setStatus(DepositStatusEnum.APPROVED.getCode());
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/deposit-requests/" + this.DEPOSIT_REQUEST_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertOk(resultNode);
        DepositRequest depositRequest = depositRequestMapper.get(this.DEPOSIT_REQUEST_ID);
        assertTrue("The status of deposit_request should be [Approved]", depositRequest.getStatus() == DepositStatusEnum.APPROVED);
    }

    @Test
    public void testGetComments() throws Exception {
        long depositRequestId = 100001;
        JsonNode resultNode = mockMvcPerform(get("/deposit-requests/" + depositRequestId + "/request-history"));
        assertOk(resultNode);

        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("comment").asText()));
        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("createdBy").get("name").asText()));
        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("updatedBy").get("name").asText()));
    }

    @Test
    public void testGetActionRequiredCount() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/deposit-requests/action-required-count"));
        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
    }
}
