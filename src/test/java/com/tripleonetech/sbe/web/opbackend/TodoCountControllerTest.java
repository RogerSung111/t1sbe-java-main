package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;

public class TodoCountControllerTest extends BaseMainTest {

    @Test
    public void testGetTodoCount() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/todo"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertEquals(9, data.get("cashbackPendingCount").asInt());
        assertEquals(7, data.get("depositRequestPendingCount").asInt());
        assertEquals(0, data.get("withdrawRequestPendingCount").asInt());
        assertEquals(2, data.get("campaignPendingCount").asInt());
        assertEquals(4, data.get("bonusPendingCount").asInt());
        assertEquals(2, data.get("playerMessagePendingCount").asInt());
    }

}
