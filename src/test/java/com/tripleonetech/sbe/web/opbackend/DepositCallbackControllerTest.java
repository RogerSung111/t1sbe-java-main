package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.payment.*;

public class DepositCallbackControllerTest extends BaseMainTest {
    
    private static final int CALLBACK_PAYMENT_API_ID = 10;
    private static final int PAYMENT_METHOD_ID = 2;
    private static final int PLAYER_ID = 1;
    private static final String SUCCESS_TRUE_JSON = "OK";

    @Autowired
    private DepositRequestMapper depositRequestMapper;
    

    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    private void initCallback() {
        //create a mock deposit_request which should be matched with [mock sign]
        String requestDate = "2017-04-20T19:52:57";
        PaymentMethod paymentMethod = paymentMethodMapper.get(PAYMENT_METHOD_ID);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal depositAmount = new BigDecimal(100.00);
        
        DepositRequest depositRequest = new DepositRequest();
        depositRequest.setId(4245368915748504L);
        depositRequest.setAmount(depositAmount);
        depositRequest.setPlayerId(PLAYER_ID);
        depositRequest.setPaymentMethodId(PAYMENT_METHOD_ID);
        depositRequest.setRequestedDate(LocalDateTime.parse(requestDate));
        depositRequest.setExpirationDate(LocalDateTime.parse(requestDate).plusHours(3));
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        depositRequest.setPaymentCharge(depositAmount.multiply(chargeRatio).add(paymentMethod.getFixedCharge()));
        depositRequestMapper.insert(depositRequest);
    }


    @Test
    public void testCallbackShouldBeSuccessByMockOnepay() throws Exception {
        initCallback();
        // the following parameters from payment-api document
        String mockSign = "6B4B676F6B72444562582F615278723571344F50735A725A495634666E666353754B6E5A786C74723"
                + "642755A7439686B707754366F535A787079612B4C455A704F395970695A6B46387478720A4E7764344D2F7"
                + "6556453475330326E70744C754F6B55706B764171426C444C305531374A49775535303573327658484E7438"
                + "4F7032746D7975632F393677766250456D742F44363750366B670A4D2B57532F41374E636C48657671385A4747553D";
        Map<String, String> callbackParameters = new HashMap<>();
        callbackParameters.put("amountFee", "1");
        callbackParameters.put("merchantId", "752");
        callbackParameters.put("tradeStatus", "PS_PAYMENT_SUCCESS");
        callbackParameters.put("sign", mockSign);
        callbackParameters.put("signType", "RSA");
        callbackParameters.put("merchantTradeId", "4245368915748504");
        callbackParameters.put("payEndTime", "2017-04-20 20:52:57");
        callbackParameters.put("currency", "JPY");
        callbackParameters.put("version", "1.0");
        callbackParameters.put("pwTradeId", "174124007522");

        String requestBody = JsonUtils.objectToJson(callbackParameters);

        MvcResult mvcResult = getMockMvc().perform(post("/payment-callback/"+CALLBACK_PAYMENT_API_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)).andReturn();

       assertEquals(SUCCESS_TRUE_JSON, mvcResult.getResponse().getContentAsString());
    }


    @Test
    public void testCallbackByUrlEncodedShouldBeSuccessByMockOnepay() throws Exception {
        initCallback();
        // the following parameters from payment-api document
        String mockSign = "6B4B676F6B72444562582F615278723571344F50735A725A495634666E666353754B6E5A786C74723"
                + "642755A7439686B707754366F535A787079612B4C455A704F395970695A6B46387478720A4E7764344D2F7"
                + "6556453475330326E70744C754F6B55706B764171426C444C305531374A49775535303573327658484E7438"
                + "4F7032746D7975632F393677766250456D742F44363750366B670A4D2B57532F41374E636C48657671385A4747553D";

        MvcResult mvcResult = getMockMvc().perform(post("/payment-callback/"+CALLBACK_PAYMENT_API_ID)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("amountFee", "1")
                .param("merchantId", "752")
                .param("tradeStatus", "PS_PAYMENT_SUCCESS")
                .param("sign", mockSign)
                .param("signType", "RSA")
                .param("merchantTradeId", "4245368915748504")
                .param("payEndTime", "2017-04-20 20:52:57")
                .param("currency", "JPY")
                .param("version","1.0")
                .param("pwTradeId", "174124007522")).andReturn();

       assertEquals(SUCCESS_TRUE_JSON, mvcResult.getResponse().getContentAsString());
    }
}
