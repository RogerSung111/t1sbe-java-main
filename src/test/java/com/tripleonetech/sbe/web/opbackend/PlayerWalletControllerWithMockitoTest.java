package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame2;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.common.web.ControllerExceptionHandlers;

public class PlayerWalletControllerWithMockitoTest extends BaseMainTest {

    private String url;

    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private PlayerWalletController playerWalletController;
    @MockBean
    private WalletService mockWalletService;

    @Before
    public void setUp() throws Exception {
        GameApiMockGame.playerAccount.put("test004", new BigDecimal("150"));
        GameApiMockGame2.playerAccount.put("test004", new BigDecimal("130"));

        String currency = "CNY";
        int playerId = 4;
        String walletListUrl = "/players/" + playerId + "/wallets/" + currency;
        url = walletListUrl + "/collect-all-balances";

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(playerWalletController)
                .setControllerAdvice(new ControllerExceptionHandlers()).build();
    }

    @Test
    @WithUserDetails("superadmin")
    public void testSubsAllToMainForMultiStatus() throws Exception {
        // only api 1 is BAD_REQUEST
        Mockito.when(mockWalletService.withdrawGame(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(BigDecimal.class), Mockito.any()))
                .thenReturn(new GameApiResult(ApiResponseEnum.OK));
        Mockito.when(mockWalletService.withdrawGame(Mockito.anyInt(), eq(1), Mockito.any(BigDecimal.class), Mockito.any()))
                .thenReturn(new GameApiResult(ApiResponseEnum.BAD_REQUEST));

        MvcResult mvcResult = mockMvc.perform(post(url).accept(MediaType.APPLICATION_JSON)).andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode resultNode = objectMapper.readTree(result);
        assertMultiStatus(resultNode);

        JsonNode data = resultNode.get("data");
        assertTrue(data.isArray());
        for(JsonNode node : data) {
            if (node.get("id").asInt() == 1) {
                assertEquals("BAD_REQUEST", node.get("message").asText());
            } else {
                assertTrue(node.get("success").asBoolean());
            }
        }
    }

    @Test
    @WithUserDetails("superadmin")
    public void testSubsAllToMainForOperationFailed() throws Exception {
        // all Api Result is BAD_REQUEST
        Mockito.when(mockWalletService.withdrawGame(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(BigDecimal.class), Mockito.any()))
                .thenReturn(new GameApiResult(ApiResponseEnum.BAD_REQUEST));

        MvcResult mvcResult = mockMvc.perform(post(url).accept(MediaType.APPLICATION_JSON)).andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode resultNode = objectMapper.readTree(result);
        assertOperationFailed(resultNode);

        JsonNode data = resultNode.get("data");
        assertTrue(data.isArray());
        data.forEach(node -> assertEquals("BAD_REQUEST", node.get("message").asText()));
    }
}
