package com.tripleonetech.sbe.web.opbackend;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.web.opbackend.ValidateParamController.ValidationTestForm;

public class ValidateParamControllerTest extends BaseMainTest {
    @Test
    public void testValidateReqParam() throws Exception {
        String url = "/test/validation/req-param?name="; // blank name parameter should trigger invalid param
        JsonNode resultNode = mockMvcPerform(get(url));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testValidateJson() throws Exception {
        String url = "/test/validation/json";
        // blank form should trigger invalid param
        String inputJson = JsonUtils.objectToJson(new ValidationTestForm());
        JsonNode resultNode = mockMvcPerform(
                post(url).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson));
        assertInvalidParameter(resultNode);
    }

}
