package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryQueryForm;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportGameLogSummaryControllerTest extends BaseMainTest {
    @Autowired
    private Constant constant;

    @Test
    public void testList_OnlyRequiredParams() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString()));
        assertOk(resultNode);
    }

    @Test
    public void testList_GroupByGamePlatform() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString())
                // groupBy is not required, default is GamePlatform
                // .param("groupBy", GroupByEnum.GamePlatform.name())
                .param("page", "2")
                .param("limit", "10"));
        assertOk(resultNode);

        assertEquals(2, resultNode.get("data").get("list").get("pageNum").asInt());
        Iterator<JsonNode> list = resultNode.get("data").get("list").get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();
            assertEquals(SiteCurrencyEnum.CNY.name(), data.get("currency").asText());
            assertFalse(data.get("gameApi").get("code").isNull());
            assertTrue(data.get("ggr").asDouble() > 0);
            assertTrue(data.get("margin").asDouble() > 0);
        }
    }

    @Test
    public void testList_GroupByGameType() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString())
                .param("groupBy", "" + ReportGameLogSummaryQueryForm.GroupByEnum.GAME_TYPE.getCode())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        Iterator<JsonNode> list = resultNode.get("data").get("list").get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();
            assertNotNull(data.get("gameType"));
            assertNotNull(data.get("gameType").get("name"));
        }
    }

    @Test
    public void testListByUsernameGroupByGameType() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.plusDays(1).toString())
                .param("username", "007")
                .param("groupBy", "" + ReportGameLogSummaryQueryForm.GroupByEnum.GAME_TYPE.getCode())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        JsonNode listNode = resultNode.get("data").get("list");
        assertEquals(4, listNode.get("size").asInt());
        Iterator<JsonNode> list = listNode.get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();
            assertNotNull(data.get("gameType"));
            assertNotNull(data.get("gameType").get("name"));
            assertFalse(data.get("id").isNull());
        }
    }

    @Test
    public void testListByPlayerIdGroupByGameType() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.plusDays(1).toString())
                .param("playerId", "7")
                .param("groupBy", "" + ReportGameLogSummaryQueryForm.GroupByEnum.GAME_TYPE.getCode())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        JsonNode listNode = resultNode.get("data").get("list");
        assertEquals(4, listNode.get("size").asInt());
        Iterator<JsonNode> list = listNode.get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();
            assertNotNull(data.get("gameType"));
            assertNotNull(data.get("gameType").get("name"));
            assertFalse(data.get("id").isNull());
        }
    }

    @Test
    public void testList_GroupByPlayer() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString())
                .param("groupBy", "" + ReportGameLogSummaryQueryForm.GroupByEnum.PLAYER.getCode())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        Iterator<JsonNode> list = resultNode.get("data").get("list").get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();
            assertFalse(data.get("player").get("username").isNull());
            assertFalse(data.get("playerGroup").get("name").isNull());
        }
    }

    @Test
    public void testList_NullDateCondition_Failed() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateSTART", now.plusDays(-200).toString())
                // .param("dateEnd", now.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("dateEnd"));
        assertTrue(resultNode.get("data").get("message").asText().contains("must not be null"));
    }

    @Test
    public void testList_ExceededLimitDays_Failed() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-200).toString())
                .param("dateEnd", now.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains(
                String.format("Date range should be limited to %d days", constant.getReport().getGameLogSummaryDayRange())));
    }

    @Test
    public void testList_SingleCurrency() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data").get("summary");
        assertEquals(1, node.size());
        assertEquals(SiteCurrencyEnum.CNY.name(), node.get(0).get("currency").asText());
        assertTrue(node.get(0).get("playerCount").asLong() > 0);
        assertEquals(1, node.get(0).get("bet").decimalValue().compareTo(BigDecimal.ZERO));
    }

    @Test
    public void testList_MultiCurrency() throws Exception {
        LocalDate now = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("dateStart", now.plusDays(-180).toString())
                .param("dateEnd", now.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        assertTrue(resultNode.get("data").get("summary").size() > 1);
        JsonNode node = resultNode.get("data").get("summary").get(0);
        assertFalse(node.get("currency").isNull());
        assertTrue(node.get("playerCount").asLong() > 0);
        assertEquals(1, node.get("bet").decimalValue().compareTo(BigDecimal.ZERO));
    }


    @Test
    public void testListSummaryShouldBeSameWithSumOfListView() throws Exception {
        LocalDate now = LocalDate.now();
        Integer limit = 999;
        JsonNode resultNode = mockMvcPerform(get("/reports/game-log-summary")
                .param("currency", SiteCurrencyEnum.CNY.name())
                .param("dateStart", now.plusDays(-10).toString())
                .param("dateEnd", now.toString())
                .param("groupBy", "" + ReportGameLogSummaryQueryForm.GroupByEnum.PLAYER.getCode())
                .param("page", "1")
                .param("limit", String.valueOf(limit)));
        assertOk(resultNode);

        Integer totalBetCount = 0;
        BigDecimal totalBet = new BigDecimal(0);
        BigDecimal totalWin = new BigDecimal(0);
        BigDecimal totalLoss = new BigDecimal(0);
        BigDecimal totalPayout = new BigDecimal(0);
        JsonNode listData  = resultNode.get("data").get("list").get("list");
        assertTrue(listData.size() < limit);
        for(JsonNode node : listData) {
            totalBetCount += node.get("betCount").asInt();
            totalBet = totalBet.add(node.get("bet").decimalValue());
            totalWin = totalWin.add(node.get("win").decimalValue());
            totalLoss = totalLoss.add(node.get("loss").decimalValue());
            totalPayout = totalPayout.add(node.get("payout").decimalValue());
        }
        JsonNode summary = resultNode.get("data").get("summary");
        assertTrue(summary.get(0).get("bet").decimalValue().compareTo(totalBet) == 0);
        assertTrue(summary.get(0).get("win").decimalValue().compareTo(totalWin) == 0);
        assertTrue(summary.get(0).get("loss").decimalValue().compareTo(totalLoss) == 0);
        assertTrue(summary.get(0).get("payout").decimalValue().compareTo(totalPayout) == 0);

    }
}
