package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.iprule.IpRuleConfigSiteEnum;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class IpRuleControllerTest extends BaseMainTest {
    @Autowired
    private MockMvc mockMvc;

    private int MOCK_IP_RULE_ID = 1; // 0.0.0.0/0 BACK_OFFICE whitelist

    @Test
    public void testEmptyListBackOfficeIpRule() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.BACK_OFFICE.getCode())
                        .param("ip", "not-exist"));
        // Query that returns no result should succeed
        assertOk(resultNode);
    }

    @Test
    public void testListBackOfficeIpRule() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.BACK_OFFICE.getCode())
                        .param("ip", "10.1.0.1"));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testListFrontOfficeIpRule() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.FRONT_OFFICE.getCode()));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testListCountry() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/firewall-rules/countries"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data").get("list");
        assertEquals(241, node.size());
    }

    @Test
    public void testListCountryEnabledOnly() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/firewall-rules/countries/enabled-only")
        );
        assertOk(resultNode);

        JsonNode node = resultNode.get("data").get("list");
        assertEquals(6, node.size());
    }

    @Test
    public void testSetCountry() throws Exception {
        IpRuleController.CountryListForm countryListForm = new IpRuleController.CountryListForm();
        countryListForm.setCountryCodes(Arrays.asList("SG", "TW"));
        JsonNode resultNode = mockMvcPerform(
                post("/firewall-rules/countries")
                        .content(JsonUtils.objectToJson(countryListForm))
        );
        assertOk(resultNode);

        JsonNode resultNodeUpdated = mockMvcPerform(
                get("/firewall-rules/countries/enabled-only")
        );
        assertOk(resultNodeUpdated);

        JsonNode node = resultNodeUpdated.get("data").get("list");
        assertEquals(2, node.size());
    }

    @Test
    @Ignore("Irrelevant now, backoffice is now whitelist")
    public void testDeleteBackOfficeBlackListAndGetBlocked() throws Exception {
        Map<String, Object> formObj = new HashMap<>();
        formObj.put("ip", "127.0.0.1/32");
        formObj.put("status", 1);
        formObj.put("remark", "testaddingblacklistIP");

        JsonNode resultNode = mockMvcPerform(post("/firewall-rules/{site}", IpRuleConfigSiteEnum.BACK_OFFICE.getCode())
                .content(JsonUtils.objectToJson(formObj)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        assertTrue(resultNode.get("data").get("id").asInt() > 1);

        MvcResult mvcResult = mockMvc.perform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.FRONT_OFFICE.getCode())
                        .header("Authorization", "Bearer " + obtainAccessToken())
                        .header("Content-Type", "application/json")
                        .with(csrf())
                        .header("Accept-Language", "en-US"))
                .andExpect(status().isForbidden()).andReturn();
        String result = mvcResult.getResponse().getContentAsString();

        assertTrue(result.contains("Your IP has been restricted"));
    }

    @Test
    public void testUpdateIpRule() throws Exception {
        Map<String, Object> formObj = new HashMap<>();
        formObj.put("ip", "updatedIP");
        formObj.put("remark", "testupdatingIP");

        JsonNode resultNode = mockMvcPerform(post("/firewall-rules/" + MOCK_IP_RULE_ID + "/update")
                .content(JsonUtils.objectToJson(formObj)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        JsonNode uodatedResultNode = mockMvcPerform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.BACK_OFFICE.getCode())
                        .param("ip", "updatedIP"));

        assertOk(uodatedResultNode);

        JsonNode node = uodatedResultNode.get("data").get("list").get(0);
        assertEquals("updatedIP", node.get("ip").asText());
        assertEquals("testupdatingIP", node.get("remark").asText());
    }

    @Test
    public void testDeleteIpRule() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/firewall-rules/" + MOCK_IP_RULE_ID + "/delete"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        JsonNode uodatedResultNode = mockMvcPerform(
                get("/firewall-rules/{site}", IpRuleConfigSiteEnum.BACK_OFFICE.getCode())
                        .param("ip", "0.0.0.0/0"));

        assertOk(uodatedResultNode);
        JsonNode node = uodatedResultNode.get("data").get("list");
        assertTrue(node.size() == 0);
    }
}
