package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.web.StatusForm;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfig;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfigMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WithdrawApiConfigControllerTest extends BaseMainTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    WithdrawApiConfigMapper withdrawApiConfigMapper;


    @Test
    public void testList() throws Exception {
        JsonNode node = mockMvcPerform(get("/system/withdraw-api"));
        assertOk(node);

        JsonNode apiConfigs = node.get("data");
        assertNotNull(apiConfigs);
        assertTrue(apiConfigs.size() > 1);
    }

    @Test
    public void testEditSubmit() throws Exception {
        int withdrawApiConfigId = 3;
        String newApiName = "TEST_WITHDRAW_API";

        WithdrawApiConfig withdrawApiConfig = withdrawApiConfigMapper.get(withdrawApiConfigId);
        withdrawApiConfig.setName(newApiName);
        byte[] requestBody = objectMapper.writeValueAsBytes(withdrawApiConfig);

        JsonNode node = mockMvcPerform(
                post("/system/withdraw-api/" + withdrawApiConfigId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);

        withdrawApiConfig = withdrawApiConfigMapper.get(withdrawApiConfigId);
        assertEquals(newApiName, withdrawApiConfig.getName());

    }

    @Test
    public void testCreateSubmit() throws Exception {
        WithdrawApiConfig apiConfig = new WithdrawApiConfig();
        apiConfig.setName("testCreateSubmit");
        apiConfig.setCode("MockWithdraw");
        apiConfig.setCurrency(SiteCurrencyEnum.CNY);
        byte[] requestBody = objectMapper.writeValueAsBytes(apiConfig);

        JsonNode node = mockMvcPerform(
                post("/system/withdraw-api/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        mockMvc.perform(
                post("/system/withdraw-api/create")
                        .header("Authorization", "Bearer " + obtainAccessToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();
        assertOk(node);
        assertDataSuccess(node.get("data"));
        assertTrue(node.get("data").get("id").asInt() > 1);
    }

    @Test
    public void testUpdateStatus() throws Exception {

        StatusForm form = new StatusForm();
        form.setStatus(ApiConfigStatusEnum.ENABLED.getCode());
        byte[] requestBody = objectMapper.writeValueAsBytes(form);

        JsonNode node = mockMvcPerform(
                post("/system/withdraw-api/1/status")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));
    }

}
