package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.domain.DomainConfig;
import com.tripleonetech.sbe.domain.DomainConfigForm;
import com.tripleonetech.sbe.domain.DomainConfigMapper;
import com.tripleonetech.sbe.web.opbackend.DomainConfigController.DomainConfigSimpleForm;

public class DomainConfigControllerTest extends BaseMainTest {

    @Autowired
    private DomainConfigMapper domainConfigMapper;

    @Test
    public void listDomainConfigsTest() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/domain-configs"));

        assertOk(resultNode);
        assertTrue(resultNode.get("data").size() > 0);
        JsonNode items = resultNode.get("data");
        items.forEach((JsonNode item) -> {
            assertNotNull(item.get("id"));
            assertNotNull(item.get("name"));
            assertNotNull(item.get("enabled"));
            assertNotNull(item.get("site"));
        });
        
    }

    @Test
    public void batchInsertDomainConfigTest() throws Exception {
        List<DomainConfigForm> requestForm = new ArrayList<>();

        DomainConfigForm newDomain1 = new DomainConfigForm();
        newDomain1.setName("www.xpj.totonestop.com");
        newDomain1.setEnabled(true);
        newDomain1.setSite(1);

        DomainConfigForm newDomain2 = new DomainConfigForm();
        newDomain2.setName("admin.xpj.totonestop.com");
        newDomain2.setEnabled(true);
        newDomain2.setSite(0);
        requestForm.add(newDomain1);
        requestForm.add(newDomain2);
        
        String requestBody = JsonUtils.objectToJson(requestForm);

        JsonNode resultNode = mockMvcPerform(
                post("/domain-configs/create").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        assertOk(resultNode);
        JsonNode items = resultNode.get("data");
        items.forEach((JsonNode item) -> {
            assertTrue(item.get("success").asBoolean());
            assertNotNull(item.get("id"));
        });
    }

    
    @Test
    public void batchUpdateDomainConfigTest() throws Exception {
        List<DomainConfigForm> requestForm = new ArrayList<>();

        String newDomainName1 = "admin.demo.totonestop.com";
        Integer domainConfigId1 = 1;
        DomainConfig existingDomain = domainConfigMapper.get(domainConfigId1);
        DomainConfigForm existingDomainForm = new DomainConfigForm();
        existingDomainForm.setId(existingDomain.getId());
        existingDomainForm.setEnabled(!existingDomain.getEnabled());
        existingDomainForm.setName(newDomainName1);
        existingDomainForm.setSite(existingDomain.getSite());
        
        Integer domainConfigId2 = 2;
        String newDomainName2 = "www.demo.totonestop.com";
        DomainConfig existingDomai2 = domainConfigMapper.get(domainConfigId2);
        DomainConfigForm existingDomainForm2 = new DomainConfigForm();
        existingDomainForm2.setId(existingDomai2.getId());
        existingDomainForm2.setEnabled(!existingDomai2.getEnabled());
        existingDomainForm2.setName(newDomainName2);
        existingDomainForm2.setSite(existingDomai2.getSite());
        
        requestForm.add(existingDomainForm);
        requestForm.add(existingDomainForm2);

        String requestBody = JsonUtils.objectToJson(requestForm);

        JsonNode resultNode = mockMvcPerform(
                post("/domain-configs").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        assertOk(resultNode);
        JsonNode items = resultNode.get("data");
        items.forEach((JsonNode item) -> {
            assertTrue(item.get("success").asBoolean());
            assertNotNull(item.get("id"));
        });
        
        DomainConfig updatedDomainConfig1 = domainConfigMapper.get(domainConfigId1);
        assertEquals(newDomainName1, updatedDomainConfig1.getName());
        
        DomainConfig updatedDomainConfig2 = domainConfigMapper.get(domainConfigId2);
        assertEquals(newDomainName2, updatedDomainConfig2.getName());

    }

    @Test
    public void deleteDomainConfigTest() throws Exception {
        Integer domainConfigIds[] = { 1, 2 };
        String requestBody = JsonUtils.objectToJson(domainConfigIds);
        JsonNode resultNode = mockMvcPerform(
                post("/domain-configs/delete-batch").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        assertOk(resultNode);
    }

    @Test
    public void enableDomainConfigTest() throws Exception {
        List<DomainConfigSimpleForm> requestForm = new ArrayList<>();
        DomainConfigSimpleForm domainConfig1 = new DomainConfigSimpleForm();
        domainConfig1.setId(1);
        domainConfig1.setEnabled(false);

        DomainConfigSimpleForm domainConfig2 = new DomainConfigSimpleForm();
        domainConfig2.setId(2);
        domainConfig2.setEnabled(false);
        requestForm.add(domainConfig1);
        requestForm.add(domainConfig2);
        String requestBody = JsonUtils.objectToJson(requestForm);
        JsonNode resultNode = mockMvcPerform(
                post("/domain-configs/enable-batch").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        assertOk(resultNode);
        assertTrue(!domainConfigMapper.get(1).getEnabled());
        assertTrue(!domainConfigMapper.get(2).getEnabled());

    }
    
    @Test
    public void batchUpdateDomainConfigTestNotCompletedParameter() throws Exception {
        List<DomainConfigForm> requestForm = new ArrayList<>();
        //Information is not enough
        DomainConfigForm newDomain1 = new DomainConfigForm();
        newDomain1.setName("www.xpj.totonestop.com");

        DomainConfigForm newDomain2 = new DomainConfigForm();
        newDomain2.setName("admin.xpj.totonestop.com");
        newDomain2.setEnabled(true);
        newDomain2.setSite(0);
        requestForm.add(newDomain1);
        requestForm.add(newDomain2);
        
        String requestBody = JsonUtils.objectToJson(requestForm);

        JsonNode resultNode = mockMvcPerform(
                post("/domain-configs/create").contentType(MediaType.APPLICATION_JSON).content(requestBody));

        assertInvalidParameter(resultNode);
                
    }

}
