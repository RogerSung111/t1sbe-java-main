package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportEmailControllerTest extends BaseMainTest {
    @Test
    public void testListEmailVerificationHistory_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/email-otp"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.get("total").asInt() > 0);
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void testListEmailVerificationHistory() throws Exception {

        String url = "/reports/email-otp";
        JsonNode resultNode = mockMvcPerform(get(url)
                .param("createdAtStart", ZonedDateTime.now().minusHours(1).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString()));

        assertOk(resultNode);
        assertTrue(resultNode.size() > 0);
        assertTrue(resultNode.get("data").get("list").get(0).hasNonNull("currency"));
    }

    @Test
    public void testListEmailHistory_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/email-history"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.get("total").asInt() > 0);
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void testListEmailHistory() throws Exception {

        String url = "/reports/email-history";
        JsonNode resultNode = mockMvcPerform(get(url)
                .param("createdAtStart", ZonedDateTime.now().minusHours(1).toString())
                .param("createdAtEnd", ZonedDateTime.now().plusMinutes(5).toString()));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue(resultNode.size() > 0);
        assertTrue(resultNode.get("data").get("list").get(0).hasNonNull("currency"));
    }
}
