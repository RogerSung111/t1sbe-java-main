package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;

public class ReportWithdrawSummaryControllerTest extends BaseMainTest {

    @Test
    public void testQueryWithdrawSummaryReport() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/withdraw-summary")
                .param("dateStart", LocalDate.now().minusDays(7).toString())
                .param("dateEnd", LocalDate.now().plusDays(1).toString())
                .param("timezoneOffset", "0"));

        assertOk(resultNode);
        JsonNode data = resultNode.get("data").get("list");
        for (JsonNode item : data) {
            assertNotNull(item.get("date").asText());
            assertTrue(item.get("withdrawalCount").asInt() > 0);
            assertTrue(item.get("withdrawerCount").asInt() > 0);
            assertTrue(new BigDecimal(item.get("withdrawalAmount").asText()).compareTo(BigDecimal.ZERO) > 0);
            assertNotNull(item.get("id").asText());
        }
    }
}
