package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayerTagService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerGroupControllerTest extends BaseMainTest {

    @Autowired
    private PlayerTagMapper playerTagMapper;
    @Autowired
    private PlayerTagService playerTagService;

    private static final String SET_DEFAULT_TAG_ERROR_MSG = "No group found with id";

    @Test
    public void testListAllGroups() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/groups"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.size() > 0);
    }

    @Test
    public void testGet() throws Exception {
        int groupId = 3; // Gold Nova III
        JsonNode resultNode = mockMvcPerform(get("/groups/{groupId}", groupId));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("group name should be 'Gold Nova III'", "Gold Nova III", node.get("name").asText());
    }

    @Test
    public void testCreate() throws Exception {
        PlayerTag sampleGroup = playerTagMapper.get(1, true);
        sampleGroup.setName("testCreate");
        sampleGroup.setAutomationJobId(2);

        JsonNode resultNode = mockMvcPerform(post("/groups/create").content(JsonUtils.objectToJson(sampleGroup)));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> groups = playerTagMapper.listByNameLike(sampleGroup.getName());
        assertEquals("There should be one group named " + sampleGroup.getName(), 1, groups.size());
        assertEquals("The group should have corresponding automation job", 2, sampleGroup.getAutomationJobId().intValue());
        assertEquals(groups.get(groups.size() - 1).getId().intValue(), resultNode.get("data").get("id").asInt());
    }

    @Test
    public void testSave() throws Exception {
        int groupId = 3; // Gold Nova III
        HashMap<String, String> params = new HashMap<>();
        params.put("name", "testUpdate");
        params.put("automationJobId", "6");
        JsonNode resultNode = mockMvcPerform(
                post("/groups/{groupId}", groupId).content(JsonUtils.objectToJson(params)));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> groups = playerTagMapper.listByNameLike(params.get("name"));
        assertEquals("there should be one group named " + params.get("name"),1,  groups.size());
        PlayerTag group = groups.get(0);
        assertEquals("The group should have corresponding automation job", 6, group.getAutomationJobId().intValue());

        groups = playerTagMapper.listByNameLike("Gold Nova III");
        assertEquals("there should be no group named 'Gold Nova III'", 0, groups.size());
    }

    @Test
    public void testDelete() throws Exception {
        int groupId = 3; // Gold Nova III
        JsonNode resultNode = mockMvcPerform(post("/groups/{groupId}/delete", groupId));

        assertDataSuccess(resultNode.get("data"));

        List<PlayerTag> groups = playerTagMapper.listByNameLike("Gold Nova III");
        assertEquals("there should be no group named 'Gold Nova III'", 0, groups.size());
    }

    @Test
    public void testFailDeleteDefaultGroup() throws Exception {
        Integer systemDefaultGroupId = playerTagMapper.getDefaultGroupId(); // Get current system default group tag id
        JsonNode resultNode = mockMvcPerform(post("/groups/{groupId}/delete", systemDefaultGroupId));

        assertOperationFailed(resultNode);

        PlayerTag group = playerTagMapper.get(systemDefaultGroupId, true);
        assertTrue("Default system group should not be deleted", group.getId().equals(systemDefaultGroupId) );
    }

    @Test
    public void testSetDefaultTagWithGroupId() throws Exception {
        Integer newSystemDefaultTagId = playerTagService.listAllGroups().get(0).getId(); // Get any of the existing player group id
        JsonNode resultNode = mockMvcPerform(post("/groups/{groupId}/set-default", newSystemDefaultTagId));

        assertDataSuccess(resultNode.get("data"));

        Integer updatedDefaultTagId = playerTagMapper.getDefaultGroupId();
        assertEquals("Failed matching updatedDefaultTagId to the newSystemDefaultTagId", updatedDefaultTagId, newSystemDefaultTagId);
    }

    @Test
    public void testFailToSetDefaultTagWithTagId() throws Exception {
        Integer newSystemDefaultTagId = playerTagService.listAllTags().get(0).getId(); // Get any of the existing player tag id
        JsonNode resultNode = mockMvcPerform(post("/groups/{groupId}/set-default", newSystemDefaultTagId));

        assertDataFail(resultNode.get("data"));
        assertTrue(resultNode.get("data").get("message").toString().contains(SET_DEFAULT_TAG_ERROR_MSG));
    }
}
