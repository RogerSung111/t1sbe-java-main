package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportWalletHistoryControllerTest extends BaseMainTest {

    @Test
    public void testQuery() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime twoDaysAgo = now.plusDays(-2);
        JsonNode resultNode = mockMvcPerform(get("/reports/wallet-history")
                .param("username", "test001")
                .param("createdAtStart", twoDaysAgo.toString())
                .param("createdAtEnd", now.toString())
                .param("currency", "CNY")
                .param("gameApiIds", "1")
                .param("gameApiIds", "2")
                .contentType(MediaType.APPLICATION_JSON));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");

        assertNotNull(node.get("list"));
        assertTrue(node.get("list").size() > 0);
        assertNotNull(node.get("list").get(0).get("createdAt"));
        assertNotNull(node.get("list").get(0).get("username"));
        assertNotNull(node.get("list").get(0).get("gameApiId"));
        assertNotNull(node.get("list").get(0).get("balanceBefore"));
        assertNotNull(node.get("list").get(0).get("balanceAfter"));
        assertNotNull(node.get("list").get(0).get("currency"));
    }

    @Test
    public void testQuery_OnlyRequiredParams() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        JsonNode resultNode = mockMvcPerform(get("/reports/wallet-history")
                .param("createdAtStart", now.plusDays(-7).format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
                .param("createdAtEnd", now.format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
                .param("username", "001"));
        assertOk(resultNode);
    }

    @Test
    public void testQueryResultSameWithUsernameLike() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        String createdAt = now.plusDays(-7).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
        String createdAtEnd = now.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);

        JsonNode resultNode1 = mockMvcPerform(get("/reports/wallet-history")
                .param("createdAtStart", createdAt)
                .param("createdAtEnd", createdAtEnd)
                .param("username", "test001"));
        assertOk(resultNode1);

        assertTrue(resultNode1.get("data").get("total").asInt() > 0);
    }

    @Test
    public void testQuery_NullDateCondition_Failed() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/wallet-history")
                .param("createdAtEnd", ZonedDateTime.now().format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
                .param("username", "test001"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("createdAtStart"));

        resultNode = mockMvcPerform(get("/reports/wallet-history")
                .param("createdAtStart", ZonedDateTime.now().format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
                .param("username", "test001"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("createdAtEnd"));
    }
}
