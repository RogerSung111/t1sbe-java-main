package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportActivityControllerTest extends BaseMainTest {

    @Test
    public void testShowPlayerActivityReport() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime twoDaysAgo = now.plusDays(-2);
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity")
                .param("creationTimeFrom", twoDaysAgo.toString())
                .param("creationTimeTo", now.toString())
                .param("currency", "CNY")
                .param("username", "test")
                .param("playerCredentialId", "2")
                //.param("invoke", "") // There is no value in test data of invoke
                .param("returnCode", "20000")
                .param("device", "" + DeviceTypeEnum.COMPUTER.getCode())
                .param("ip", "45")); // ip is wildcard);
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        JsonNode list = node.get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);

        JsonNode jsonNode = list.get(0);
        assertNotNull(jsonNode.get("createdAt"));
        assertNotNull(jsonNode.get("username"));
        assertEquals(20000, jsonNode.get("returnCode").asInt());
        assertEquals("GET", jsonNode.get("httpMethod").asText());
        assertNotNull(jsonNode.get("requestUri"));
        assertEquals(2, jsonNode.get("playerId").asInt());
        assertNotNull(jsonNode.get("action"));
        assertNotNull(jsonNode.get("currency"));
        assertNotNull(jsonNode.get("device"));
        assertNotNull(jsonNode.get("ip"));
    }

    @Test
    public void testList_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity").param("page", "2").param("limit", "3"));
        assertOk(resultNode);
    }

    @Test
    public void testFilterByRequestUri() throws Exception {
        String testUri = "verification-question";
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity")
                .param("username", "test002")
                .param("requestUri", testUri));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals(1, node.get("list").size());
        assertThat(node.get("list").get(0).get("requestUri").asText(), containsString(testUri));
    }

    @Test
    public void testFilterByInvoke() throws Exception {
        String testControllerName = "ApiPlayerController";
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity")
                .param("username", "test002")
                .param("invoke", testControllerName));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals(4, node.get("list").size());
        assertThat(node.get("list").get(0).get("invoke").asText(), containsString(testControllerName));
    }

    @Test
    public void testFilterByHttpMethod() throws Exception {
        String testHttpMethod = "GET";
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity")
                .param("username", "test002")
                .param("httpMethod", testHttpMethod));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals(2, node.get("list").size());
        assertEquals(testHttpMethod, node.get("list").get(0).get("httpMethod").asText());
    }

    @Test
    public void testFilterByDevice() throws Exception {
        int testDevice = 3;
        JsonNode resultNode = mockMvcPerform(get("/reports/player-activity")
                .param("currency", "CNY")
                .param("device", "" + testDevice));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertEquals(1, resultNode.get("data").get("list").size());
        assertEquals(testDevice, node.get("list").get(0).get("device").asInt());

    }

    @Test
    public void testShowOperatorActivityReport() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime twoDaysAgo = now.plusDays(-2);
        JsonNode resultNode = mockMvcPerform(get(
                "/reports/operator-activity")
                .param("creationTimeFrom", twoDaysAgo.toString())
                .param("creationTimeTo", now.toString()));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue(node.get("list").size() > 0);
        assertFalse(node.get("list").get(0).get("createdAt").isNull());
        assertFalse(node.get("list").get(0).get("username").isNull());
        assertFalse(node.get("list").get(0).get("device").isNull());
        assertFalse(node.get("list").get(0).get("ip").isNull());
    }
}
