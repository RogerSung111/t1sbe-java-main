package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.promo.bonus.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PromoBonusControllerTest extends BaseMainTest {
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private BonusService bonusService;

    @Test
    public void testList() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-1);
        String mockUsername = "001";

        JsonNode resultNode = mockMvcPerform(get("/promo-bonus")
                .param("playerId", "2")
                .param("currency", "CNY")
                .param("status", "" + BonusStatusEnum.APPROVED.getCode())
                .param("promoType", "11")
                .param("ruleId", "1")
                .param("promoName", "充")
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertEquals(1, list.size());
        JsonNode item = list.get(0);
        assertEquals(BonusStatusEnum.APPROVED.getCode(), item.get("status").asInt());
        assertEquals("CNY", item.get("currency").asText());

        resultNode = mockMvcPerform(get("/promo-bonus")
                .param("currency", "USD")
                .param("status", "" + BonusStatusEnum.PENDING_APPROVAL.getCode())
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
        for (JsonNode data : list) {
            assertEquals("USD", data.get("currency").asText());
        }

        resultNode = mockMvcPerform(get("/promo-bonus")
                .param("username", "test002")
                .param("page", "1")
                .param("limit", "5")
                .param("sort", "id DESC"));
        assertOk(resultNode);
        list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() == 5);
        for (int i = 0; i < 4; i++) {
            assertTrue(list.get(i).get("id").asInt() > list.get(i + 1).get("id").asInt());
            assertEquals("test002", list.get(i).get("username").asText());
        }
    }

    @Test
    public void testListOnlyRequired() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promo-bonus"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
    }

    @Test
    public void testListCampaignOnlyRequired() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-3);

        JsonNode resultNode = mockMvcPerform(get("/promo-bonus/campaigns")
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString()));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
    }

    @Test
    public void testListCampaign() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-3);

        JsonNode resultNode = mockMvcPerform(get("/promo-bonus/campaigns")
                .param("playerId", "2")
                .param("currency", "CNY")
                .param("ruleId", "1")
                .param("promoTypes", "11")
                .param("campaignName", "充")
                .param("status", "" + BonusStatusEnum.APPROVED.getCode())
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString()));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
        for (JsonNode data : list) {
            assertEquals(BonusStatusEnum.APPROVED.getCode(), data.get("status").asInt());
            assertEquals(BonusStatusEnum.APPROVED.name(), data.get("statusEnum").asText());
            assertEquals("CNY", data.get("currency").asText());
        }

        resultNode = mockMvcPerform(get("/promo-bonus/campaigns")
                .param("currency", "USD")
                .param("status", "" + BonusStatusEnum.APPROVED.getCode()));
        assertOk(resultNode);

        list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue("There should be no campaign bonuses under USD", list.size() == 0);

        resultNode = mockMvcPerform(get("/promo-bonus/campaigns")
                .param("username", "test002")
                .param("page", "1")
                .param("limit", "2")
                .param("sort", "bonusAmount DESC"));
        assertOk(resultNode);
        list = resultNode.get("data").get("list");
        assertTrue(list.size() == 2);
        assertTrue(list.get(0).get("bonusAmount").bigIntegerValue()
                .compareTo(list.get(1).get("bonusAmount").bigIntegerValue()) > 0);
        for (JsonNode data : list) {
            assertEquals("test002", data.get("username").asText());
            assertTrue(data.get("promoType").asInt() > 10);
        }
    }

    @Test
    public void testApprove() throws Exception {
        Integer mockId = 3;
        BonusStatusEnum mockStatus = BonusStatusEnum.APPROVED;
        PromoBonus originalPromoBonus = promoBonusMapper.queryById(mockId);
        BigDecimal originalMainWalletBalance = walletService.getMainWalletBalance(originalPromoBonus.getPlayerId());

        // Before
        assertNotEquals(mockStatus, originalPromoBonus.getStatus());

        JsonNode resultNode = mockMvcPerform(post("/promo-bonus/" + mockId + "/approve"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        // After
        PromoBonus promoBonus = promoBonusMapper.queryById(mockId);
        BigDecimal mainWalletBalance = walletService.getMainWalletBalance(originalPromoBonus.getPlayerId());
        assertEquals(mockStatus, promoBonus.getStatus());
        assertEquals((originalMainWalletBalance.add(originalPromoBonus.getBonusAmount())), mainWalletBalance);
        assertNull("Bonus should have null comment as approving does not require reason", promoBonus.getComment());
    }

    @Test
    public void testReject() throws Exception {
        Integer mockId = 7;
        BonusStatusEnum mockStatus = BonusStatusEnum.REJECTED;
        PromoBonus originalPromoBonus = promoBonusMapper.queryById(mockId);
        BigDecimal originalMainWalletBalance = walletService.getMainWalletBalance(originalPromoBonus.getPlayerId());

        // Before
        assertNotEquals(mockStatus, originalPromoBonus.getStatus());



        JsonNode resultNode = mockMvcPerform(
                post("/promo-bonus/" + mockId + "/reject")
                        .content("{\"reason\":\"unit test\"}")
        );
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        // After
        PromoBonus promoBonus = promoBonusMapper.queryById(mockId);
        BigDecimal mainWalletBalance = walletService.getMainWalletBalance(originalPromoBonus.getPlayerId());
        assertEquals(mockStatus, promoBonus.getStatus());
        assertEquals(originalMainWalletBalance, mainWalletBalance);
        assertEquals("Bonus comment should be filled with reject reason", "unit test", promoBonus.getComment());
    }

    @Test
    public void testBatchApprove() throws Exception {
        PromoBonusQueryForm queryCondition = new PromoBonusQueryForm();
        queryCondition.setStatus(BonusStatusEnum.PENDING_APPROVAL.getCode());
        List<BonusView> bonusViews = bonusService.query(queryCondition);
        List<Integer> mockIds = bonusViews.stream()
                .map(cashback -> cashback.getId())
                .collect(Collectors.toList());

        // Before
        mockIds.forEach(mockId -> {
            PromoBonus originalPromoBonus = promoBonusMapper.queryById(mockId);
            assertEquals(BonusStatusEnum.PENDING_APPROVAL, originalPromoBonus.getStatus());
        });

        List<String> idStrArray = mockIds.stream().map(id -> id.toString()).collect(Collectors.toList());
        Map<String, Object> params = new HashMap<>();
        params.put("ids", idStrArray);
        JsonNode resultNode = mockMvcPerform(post("/promo-bonus/approve-batch").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        // After
        mockIds.forEach(mockId -> {
            PromoBonus promoBonus = promoBonusMapper.queryById(mockId);
            assertEquals(BonusStatusEnum.APPROVED, promoBonus.getStatus());
        });
    }

    @Test
    public void testBatchApprove_WithoutId_Fail() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("ids", null);
        JsonNode resultNode = mockMvcPerform(post("/promo-bonus/approve-batch").content(JsonUtils.objectToJson(params)));
        assertDataFail(resultNode.get("data"));

    }

    @Test
    public void testBatchReject() throws Exception {
        PromoBonusQueryForm queryCondition = new PromoBonusQueryForm();
        queryCondition.setStatus(BonusStatusEnum.PENDING_APPROVAL.getCode());
        List<BonusView> bonusViews = bonusService.query(queryCondition);
        List<Integer> mockIds = bonusViews.stream()
                .map(cashback -> cashback.getId())
                .collect(Collectors.toList());

        // Before
        mockIds.forEach(mockId -> {
            PromoBonus originalPromoBonus = promoBonusMapper.queryById(mockId);
            assertEquals(BonusStatusEnum.PENDING_APPROVAL, originalPromoBonus.getStatus());
        });

        List<String> idStrArray = mockIds.stream().map(id -> id.toString()).collect(Collectors.toList());
        Map<String, Object> params = new HashMap<>();
        params.put("ids", idStrArray);
        JsonNode resultNode = mockMvcPerform(post("/promo-bonus/reject-batch").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        // After
        mockIds.forEach(mockId -> {
            PromoBonus promoBonus = promoBonusMapper.queryById(mockId);
            assertEquals(BonusStatusEnum.REJECTED, promoBonus.getStatus());
        });
    }

    @Test
    public void testBatchReject_WithoutId_Fail() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("ids", null);
        JsonNode resultNode = mockMvcPerform(post("/promo-bonus/approve-batch").content(JsonUtils.objectToJson(params)));
        assertDataFail(resultNode.get("data"));

    }
}
