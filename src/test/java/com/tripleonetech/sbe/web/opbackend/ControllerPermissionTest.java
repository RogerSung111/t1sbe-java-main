package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ControllerPermissionTest extends BaseMainTest {

    @Test
    public void testAdminPermissionPlayerList() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "test002")
                        .param("page", "1")
                        .param("limit", "5"));
        // Query should succeed as default operator username 'superadmin' has player list permission
        assertOk(resultNode);
    }

    @Test
    public void testNoPermissionPlayerList() throws Exception {
        setOperatorUsername("finance"); // same password

        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "test002")
                        .param("page", "1")
                        .param("limit", "5"));
        // Query should not succeed as default operator username 'finance' does not have player list permission
        assertAccessForbidden(resultNode);
    }
}
