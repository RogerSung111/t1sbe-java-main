package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerGroupManagementControllerTest extends BaseMainTest {
    private final boolean playerGroup = true;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;


    @Test
    public void testSetTags() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(existingPlayerTags.size() > 0);// 選至少一個以上的做測試

        Integer addGroupId = 2;
        JsonNode node = mockMvcPerform(post("/players/{playerId}/groups", playerId)
                .content("{\"groupId\": " + addGroupId + "}"));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(addGroupId, updatedPlayerTags.get(0).getPlayerTagId());
    }

    @Test
    public void testSetTags_EmptyTagId() throws Exception {
        int playerId = 2;

        JsonNode node = mockMvcPerform(post("/players/{playerId}/groups", playerId)
                .content("{\"groupId\": ''}"));
        assertDataFail(node.get("data"));
    }

    @Test
    public void testSetTags_OriginalIsEmpty() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        List<Integer> deleteGroupIds = existingPlayerTags.stream()
                .map(PlayersPlayerTag::getPlayerTagId)
                .collect(Collectors.toList());
        playersPlayerTagMapper.delete(playerId, deleteGroupIds);
        existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(CollectionUtils.isEmpty(existingPlayerTags));

        Integer addGroupId = 2;
        JsonNode node = mockMvcPerform(post("/players/{playerId}/groups", playerId)
                .content("{\"groupId\": " + addGroupId + "}"));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(addGroupId, updatedPlayerTags.get(0).getPlayerTagId());
    }

    @Test
    public void testAddTag() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        List<Integer> deleteGroupIds = existingPlayerTags.stream()
                .map(PlayersPlayerTag::getPlayerTagId)
                .collect(Collectors.toList());
        playersPlayerTagMapper.delete(playerId, deleteGroupIds);

        Integer addGroupId = 2;

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/groups/add", playerId)
                        .content("{\"groupId\": " + addGroupId + "}"));

        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(addGroupId, updatedPlayerTags.get(0).getPlayerTagId());
    }

    @Test
    public void testAddTagFail() throws Exception {
        Integer addGroupId = 2;
        int playerId = 2;

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/groups/add", playerId)
                        .content("{\"groupId\": " + addGroupId + "}"));
        assertDataFail(node.get("data"));

    }

    @Test
    public void testDeleteTag() throws Exception {
        int playerId = 2;
        List<PlayersPlayerTag> existingPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertTrue(existingPlayerTags.size() > 0);// 選至少一個以上的做測試

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/groups/{groupId}/delete", playerId, existingPlayerTags.get(0).getPlayerTagId()));
        assertDataSuccess(node.get("data"));

        List<PlayersPlayerTag> updatedPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        assertEquals(0, updatedPlayerTags.size());
    }


}
