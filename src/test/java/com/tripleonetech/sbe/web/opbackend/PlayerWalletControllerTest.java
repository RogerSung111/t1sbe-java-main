package com.tripleonetech.sbe.web.opbackend;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame2;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerNewForm;
import com.tripleonetech.sbe.player.PlayerService;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionQueryForm;
import com.tripleonetech.sbe.web.api.ApiReportController.WalletTransactionView;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerWalletControllerTest extends BaseMainTest {

    private String walletListUrl;
    private Integer playerId;
    private String currency;
    private int gameApiId;
    private int disabledGameApiId = 4;

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    
    @Before
    public void setUp() throws Exception {
        playerId = 1;
        currency = "CNY";
        gameApiId = 1;
        walletListUrl = "/players/" + playerId + "/wallets/" + currency;
    }
    
    private void disableGameApi(Integer disabledGameApiId) {
        GameApiConfig apiConfig = gameApiConfigMapper.getIgnoreDeleted(disabledGameApiId);
        apiConfig.setStatus(ApiConfigStatusEnum.DISABLED);
        gameApiConfigMapper.updateStatus(apiConfig);
    }
    @Test
    public void testList() throws Exception {
        String url = walletListUrl;

        JsonNode resultNode = mockMvcPerform(get(url).contentType(MediaType.APPLICATION_JSON));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        JsonNode walletsNode = node.get("wallets");

        assertTrue("There should be wallets", walletsNode.size() > 0);
        assertTrue("The first wallet should always be main wallet",
                walletsNode.get(0).get("gameApiId").isNull());
        assertFalse("The first sub-wallet should exist",
                walletsNode.get(1).get("gameApiId").isNull());
        assertFalse("The status of first sub-wallet should exist",
                walletsNode.get(1).get("status").isNull());
        assertFalse("Must contain player info", node.get("player").isNull());
        assertEquals("Result must contain playerId", this.playerId.intValue(), node.get("player").get("id").asInt());
        assertEquals("Username must be test001", "test001", node.get("player").get("username").asText());
        assertEquals("Result must contain currency", this.currency, node.get("player").get("currency").asText());
        assertFalse("The first sub-wallet should not allow decimal transfer", walletsNode.get(1).get("allowDecimalTransfer").asBoolean());
    }

    @Test
    public void testListForNewPlayer() throws Exception {
        PlayerNewForm newPlayerForm = new PlayerNewForm();
        newPlayerForm.setCurrency(SiteCurrencyEnum.CNY);
        newPlayerForm.setUsername("unitTestNew");
        newPlayerForm.setEmail("test@test.com");
        newPlayerForm.setReferralCode("abcdefg");
        playerService.register(newPlayerForm, "abcdef", DeviceTypeEnum.MANUAL, "1.1.1.1");

        JsonNode resultNode = mockMvcPerform(get(this.walletListUrl).contentType(MediaType.APPLICATION_JSON));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        JsonNode walletsNode = node.get("wallets");
        assertTrue("There should be wallets", walletsNode.size() > 0);

        for (int i = 0; i < walletsNode.size(); i++) {
            if (i == 0) {
                assertTrue("The first wallet should always be main wallet",
                        walletsNode.get(i).get("gameApiId").isNull());
            } else {
                assertFalse("Every sub-wallet should exist", walletsNode.get(i).get("gameApiId").isNull());
                assertEquals("Each sub-wallet should be CNY as player was created in CNY", "CNY", walletsNode.get(i).get("currency").asText());
            }
        }
    }

    @Test
    public void testAddBalance() throws Exception {
        Player player = playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY).get(0);
        Wallet mainWalletBefore = walletMapper.getMainWallet(player.getId());

        String url = walletListUrl + "/add-balance";
        JsonNode resultNode = mockMvcPerform(post(url).content("{\"amount\":100}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(player.getId());
        queryForm.setType(WalletTransactionTypeEnum.ADD_BALANCE.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(0);
        assertTrue(new BigDecimal("100").compareTo(theTransation.getAmount()) == 0);
        assertEquals(this.getOperatorUsername(), theTransation.getOperatorUsername());
        assertEquals(String.format("This transaction was made by %s.", this.getOperatorUsername()),
                theTransation.getNote());
        assertEquals(getOperatorUsername(), theTransation.getOperatorUsername());

        Wallet mainWalletAfter = walletMapper.getMainWallet(player.getId());
        assertEquals("Balance should increase by 100",
                mainWalletBefore.getBalance().add(BigDecimal.valueOf(100L)).longValue(),
                mainWalletAfter.getBalance().longValue());
    }

    @Test
    public void testDeductBalance() throws Exception {
        Player player = playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY).get(0);
        Wallet mainWalletBefore = walletMapper.getMainWallet(player.getId());

        String url = walletListUrl + "/deduct-balance";
        JsonNode resultNode = mockMvcPerform(post(url).content("{\"amount\":100}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(player.getId());
        queryForm.setType(WalletTransactionTypeEnum.DEDUCT_BALANCE.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(0);
        assertTrue(new BigDecimal("100").compareTo(theTransation.getAmount()) == 0);
        assertEquals(this.getOperatorUsername(), theTransation.getOperatorUsername());
        assertEquals(String.format("This transaction was made by %s.", this.getOperatorUsername()),
                theTransation.getNote());
        assertEquals(getOperatorUsername(), theTransation.getOperatorUsername());

        Wallet mainWalletAfter = walletMapper.getMainWallet(player.getId());
        assertEquals("Balance should be deducted by 100",
                mainWalletBefore.getBalance().subtract(BigDecimal.valueOf(100L)).longValue(),
                mainWalletAfter.getBalance().longValue());
    }

    @Test
    public void testMainWithdrawWithInsufficientBalance() throws Exception {
        String url = walletListUrl + "/deduct-balance";
        JsonNode resultNode = mockMvcPerform(post(url).content("{\"amount\":10000}"));
        assertInsufficientBalance(resultNode);

        String message = resultNode.get("data").get("message").asText();
        assertThat(message, containsString("Insufficient balance"));
    }

    @Test
    public void testMainToSub() throws Exception {
        String url = walletListUrl + "/{gameApiId}/transfer-in";
        JsonNode resultNode = mockMvcPerform(post(url, gameApiId).content("{\"amount\":100}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY).get(0).getId());
        queryForm.setType(WalletTransactionTypeEnum.TRANSFER_OUT.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(0);
        assertEquals(getOperatorUsername(), theTransation.getOperatorUsername());
    }

    @Test
    public void testSubToMain() throws Exception {
        String url = walletListUrl + "/{gameApiId}/transfer-out";
        JsonNode resultNode = mockMvcPerform(post(url, gameApiId).content("{\"amount\":100}"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY).get(0).getId());
        queryForm.setType(WalletTransactionTypeEnum.TRANSFER_IN.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(0);
        assertEquals(getOperatorUsername(), theTransation.getOperatorUsername());
    }
    
    @Test
    public void testSubToMainByUsingDisabledAPI() throws Exception {
        disableGameApi(disabledGameApiId);
        String url = walletListUrl + "/{gameApiId}/transfer-out";
        JsonNode resultNode = mockMvcPerform(post(url, disabledGameApiId).content("{\"amount\":100}"));
        assertOperationFailed(resultNode);
    }

    
    @Test
    public void testMainAllToSub() throws Exception {
        String url = walletListUrl + "/{gameApiId}/transfer-in-all";
        JsonNode node = mockMvcPerform(post(url, gameApiId).with(csrf()));
        assertOk(node);
        assertDataSuccess(node.get("data"));
    }

    @Test
    public void testSubAllToMain() throws Exception {
        String url = walletListUrl + "/{gameApiId}/transfer-out-all";
        JsonNode node = mockMvcPerform(post(url, gameApiId).with(csrf()));
        assertOk(node);
        assertDataSuccess(node.get("data"));

        WalletTransactionQueryForm queryForm = new WalletTransactionQueryForm();
        queryForm.setPlayerId(playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY).get(0).getId());
        queryForm.setType(WalletTransactionTypeEnum.TRANSFER_IN.getCode());
        List<WalletTransactionView> datas = walletTransactionMapper.list(queryForm, 1, 100);
        WalletTransactionView theTransation = datas.get(0);
        assertEquals(getOperatorUsername(), theTransation.getOperatorUsername());
    }
    
    @Test
    public void testSubAllToMainByUsingDisabledAPI() throws Exception {
        disableGameApi(disabledGameApiId);

        String url = walletListUrl + "/{gameApiId}/transfer-out-all";
        JsonNode node = mockMvcPerform(post(url, disabledGameApiId).with(csrf()));
        assertOperationFailed(node);

    }

    @Test
    public void testGetCurrentBalanceMainWallet() throws Exception {
        String url = walletListUrl + "/0/get-balance";
        JsonNode node = mockMvcPerform(get(url));
        assertOk(node);
        JsonNode resultNode = node.get("data");
        assertTrue(resultNode.get("balance").asInt() > 0);
        assertTrue("Main wallet balance should not have gameApiId property", resultNode.get("gameApiId").isNull());

    }

    @Test
    public void testGetCurrentBalance() throws Exception {
        String url = walletListUrl + "/{gameApiId}/get-balance";
        JsonNode node = mockMvcPerform(get(url, gameApiId));
        assertOk(node);
        JsonNode resultNode = node.get("data");
        assertEquals("The gameApiId should match", gameApiId, resultNode.get("gameApiId").asInt());
        assertNotNull("The gameApiName should not be null", resultNode.get("gameApiName"));
        assertFalse("Right after get-balance, game wallet dirty status should be false", resultNode.get("dirty").asBoolean());
        assertFalse("Wallet should not allow decimal transfer", resultNode.get("allowDecimalTransfer").asBoolean());
    }

    @Test
    public void testSubsAllToMain() throws Exception {
        int id = 5;
        playerId = 4;
        walletListUrl = "/players/" + playerId + "/wallets/" + currency;
        String url = walletListUrl + "/collect-all-balances";

        BigDecimal playerMockGameWalletBalance = new BigDecimal("150");
        BigDecimal playerMockGame2WalletBalance = new BigDecimal("130");
        GameApiMockGame.playerAccount.put("test004", playerMockGameWalletBalance);
        GameApiMockGame2.playerAccount.put("test004", playerMockGame2WalletBalance);

        BigDecimal beforeMainWalletBalance = walletMapper.getMainWallet(id).getBalance();
        BigDecimal totalBalance = beforeMainWalletBalance.add(playerMockGameWalletBalance)
                .add(playerMockGame2WalletBalance);

        JsonNode result = mockMvcPerform(post(url));
        assertOk(result);

        Player player = playerMapper.get(id);
        Wallet wallet = walletMapper.get(id, gameApiId);

        Wallet mainWallet = walletMapper.getMainWallet(id);
        assertEquals("Player balance should be " + totalBalance, totalBalance.floatValue(),
                mainWallet.getBalance().floatValue(), 0.001);
        walletService.getCurrentBalance(player, wallet);
        assertEquals("Player's gameApi 1 should be " + BigDecimal.ZERO.floatValue(), BigDecimal.ZERO.floatValue(),
                wallet.getBalance().floatValue(), 0.001);

    }
}
