package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.operator.OperatorRole;
import com.tripleonetech.sbe.operator.OperatorRoleMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class OperatorRoleControllerTest extends BaseMainTest {

    private int roleId;

    @Autowired
    private OperatorRoleMapper operatorRoleMapper;

    @Before
    public void setUp() {
        roleId = 2;
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/operator-roles"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data").get(1);
        assertTrue("Listed operator role should have permission info", data.has("permission"));
        assertFalse("Listed operator role should not contain operatorPermission object", data.has("operatorPermission"));
    }

    @Test
    public void testCreate() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operator-roles/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\" : \"testCreate\", \"permission\" : \"[]\" }")
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<OperatorRole> operatorRoles = operatorRoleMapper.list();
        assertEquals("After creation, there should be 6 roles", 6, operatorRoles.size());
    }

    @Test
    public void testUpdate() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operator-roles/" + roleId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\" : \"testUpdate\", \"permission\" : \"[]\" }")
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        OperatorRole operatorRole = operatorRoleMapper.get(roleId);
        assertEquals("testUpdate", operatorRole.getName());
    }

    @Test
    public void testDelete() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/operator-roles/" + roleId + "/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(csrf()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        OperatorRole operatorRole = operatorRoleMapper.get(roleId);
        assertNull(operatorRole);
    }
}
