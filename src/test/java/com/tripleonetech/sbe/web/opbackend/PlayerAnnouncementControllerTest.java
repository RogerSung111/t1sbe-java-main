package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.announcement.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PlayerAnnouncementControllerTest extends BaseMainTest {
    @Autowired
    private PlayerAnnouncementMapper playerAnnouncementMapper;

    private static final int TEST_ANNOUNCEMENT_ID = 1;
    private static final String PRE_URL = "/player/announcement";
    private static final String TEST_TITLE = "Unit test title";
    private static final String TEST_CONTENT = "Unit test content";

    @Test
    public void testQuery_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get(PRE_URL));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertFalse(node.get("list").isNull());
        assertFalse(node.get("size").isNull());
        assertTrue(node.get("size").asInt() > 0);
        assertFalse(node.get("list").get(0).has("content"));
    }

    @Test
    public void testQueryAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(get(PRE_URL)
                .param("title", "Announcement")
                .param("messageForDate", ZonedDateTime.now().minusHours(1).toString())
        );
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertTrue("Announcement title should contain 'Announcement' String",
                node.get("list").get(0).get("title").asText().contains("Announcement"));
        assertFalse(node.get("list").get(0).has("content"));
    }

    @Test
    public void testGetAnnouncement() throws Exception {
        int testId = 1;
        JsonNode resultNode = mockMvcPerform(get(PRE_URL + "/{id}", "" + testId));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals(testId, node.get("id").asInt());
        assertEquals("announcement content", node.get("content").asText());
    }

    @Test
    public void testCreateAnnouncement() throws Exception {
        PlayerAnnouncementForm announcement = new PlayerAnnouncementForm();
        announcement.setTitle(TEST_TITLE);
        announcement.setContent(TEST_CONTENT);
        announcement.setPlayerGroupIds(Arrays.asList(1,2,3));
        announcement.setPlayerTagIds(Arrays.asList(6,7));
        String requestBodyStr = JsonUtils.objectToJson(announcement);

        JsonNode resultNode = mockMvcPerform(post(PRE_URL + "/create")
                .content(requestBodyStr));

        assertOk(resultNode);
        assertFalse(resultNode.get("data").get("id").isNull());
        int newAnnouncementId = resultNode.get("data").get("id").asInt();

        PlayerAnnouncementDetailView newAnnouncement = playerAnnouncementMapper.getDetail(newAnnouncementId);
        assertEquals("The content of player announncement should be: " + TEST_CONTENT,
                newAnnouncement.getContent(), TEST_CONTENT);
        assertEquals(2, newAnnouncement.getPlayerTags().size());
        assertEquals(3, newAnnouncement.getPlayerGroups().size());
    }

    @Test
    public void testCreateAnnouncementNoTags() throws Exception {
        PlayerAnnouncementForm announcement = new PlayerAnnouncementForm();
        announcement.setTitle(TEST_TITLE);
        announcement.setContent(TEST_CONTENT);
        String requestBodyStr = JsonUtils.objectToJson(announcement);

        JsonNode resultNode = mockMvcPerform(post(PRE_URL + "/create")
                .content(requestBodyStr));

        assertOk(resultNode);
        assertFalse(resultNode.get("data").get("id").isNull());
        int newAnnouncementId = resultNode.get("data").get("id").asInt();

        PlayerAnnouncementDetailView newAnnouncement = playerAnnouncementMapper.getDetail(newAnnouncementId);
        assertEquals("The content of player announncement should be: " + TEST_CONTENT,
                newAnnouncement.getContent(), TEST_CONTENT);
    }

    @Test
    public void testUpdateAnnouncement() throws Exception {
        PlayerAnnouncementForm announcement = new PlayerAnnouncementForm();
        announcement.setTitle(TEST_TITLE);
        announcement.setContent(TEST_CONTENT);
        announcement.setPlayerGroupIds(Arrays.asList(1,2,3));
        announcement.setPlayerTagIds(Arrays.asList(6,7));
        String requestBodyStr = JsonUtils.objectToJson(announcement);
        JsonNode resultNode = mockMvcPerform(post(PRE_URL + '/' + TEST_ANNOUNCEMENT_ID)
                .content(requestBodyStr));

        assertOk(resultNode);

        PlayerAnnouncementQueryForm queryForm = new PlayerAnnouncementQueryForm();
        queryForm.setId(TEST_ANNOUNCEMENT_ID);
        PlayerAnnouncementDetailView updatedAnnouncement = playerAnnouncementMapper.getDetail(TEST_ANNOUNCEMENT_ID);
        assertEquals("The content of player announncement should be: " + TEST_CONTENT,
                updatedAnnouncement.getContent(), TEST_CONTENT);
        assertEquals(2, updatedAnnouncement.getPlayerTags().size());
        assertEquals(3, updatedAnnouncement.getPlayerGroups().size());
    }

    @Test
    public void testDeleteAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(post(PRE_URL + '/' + TEST_ANNOUNCEMENT_ID + "/delete"));
        assertOk(resultNode);

        PlayerAnnouncementView announcementDeleted = playerAnnouncementMapper.getDetail(TEST_ANNOUNCEMENT_ID);
        assertNull(announcementDeleted);
    }
}
