package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableList;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletAmountForm;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import com.tripleonetech.sbe.web.opbackend.PlayerController.CurrencyBooleanForm;
import com.tripleonetech.sbe.web.opbackend.PlayerController.CurrencyForm;
import com.tripleonetech.sbe.web.opbackend.PlayerController.CurrencyStatusForm;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


public class PlayerControllerTest extends BaseMainTest {
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;

    private final int playerId = 2;

    @Test
    public void testEmptyPlayerList() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "not-exist")
                        .param("page", "1")
                        .param("limit", "5"));
        // Query that returns no result should succeed
        assertOk(resultNode);
    }

    @Test
    public void testPlayerList() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("page", "2")
                        .param("limit", "5"));

        // Query without any criteria should succeed
        assertOk(resultNode);

        resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "test")
                        .param("sort", "username ASC")
                        .param("status", "1")
                        .param("lastLoginDevice", "2")
                        .param("lastLoginIp", "114.32.45.146")
                        .param("page", "1")
                        .param("limit", "5"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");

        JsonNode playersNode = node.get("list");
        assertEquals("Should return 5 players' info", 5, playersNode.size());
        JsonNode playerNode = playersNode.get(2); // test002
        assertFalse("Should have email", playerNode.get("email").isNull());
        assertFalse("Player should be returned with its tags info",
                playerNode.get("profiles").get(0).get("tags").isNull());
        assertTrue("Tag IDs must be returned as array", playerNode.get("profiles").get(0).get("tags").isArray());
        assertEquals("This player should have global status = 1", 1, playerNode.get("status").asInt());
        String lastLoginTime = playerNode.get("activities").get(0).get("lastLoginTime").asText();
        assertTrue("Last Login Time must be in ISO format", lastLoginTime.contains("T"));
        assertFalse("Last login IP should have value", playerNode.get("activities").get(0).get("lastLoginIp").isNull());
        int lastLoginDevice = playerNode.get("activities").get(0).get("lastLoginDevice").asInt();
        assertTrue("Must contain valid last login device code", lastLoginDevice > 0);
        JsonNode balanceNode = playerNode.get("balances");
        assertTrue("Should contain balances as an array", balanceNode.isArray());
        JsonNode cnyBalance = balanceNode.get(0);
        assertEquals("Should be CNY balance", "CNY", cnyBalance.get("currency").asText());
        assertTrue("Should non-zero CNY balance", cnyBalance.get("total").asDouble() > 0.0);
        JsonNode profileNode = playerNode.get("profiles").get(0);
        assertNotNull("Should contain at least one profile", profileNode);
        assertFalse("Should contain status in profile", profileNode.get("status").isNull());
        assertFalse("Should contain cashbackEnabled in profile", profileNode.get("cashbackEnabled").isNull());
        assertFalse("Should contain campaignEnabled in profile", profileNode.get("campaignEnabled").isNull());
        assertFalse("Should contain withdrawEnabled in profile", profileNode.get("withdrawEnabled").isNull());
        assertFalse("Should contain risk in profile", profileNode.get("risk").isNull());
        playerNode.get("profiles").forEach(profile -> assertNotEquals(PlayerStatusEnum.CURRENCY_INACTIVE.getCode(), profile.get("status").asInt()));
    }

    @Test
    public void testPlayerListWithStatus() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "test018"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        JsonNode playersNode = node.get("list");
        JsonNode playerNode = playersNode.get(0); // test017 itself
        assertEquals("Player test018 should have status = 0", 0, playerNode.get("status").asInt());
        assertEquals("Should only return 1 set of profile as only 1 currency is active for test018", 1, playerNode.get("profiles").size());
    }

    @Test
    public void testPlayerListWithTag() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("username", "test")
                        .param("sort", "lastLoginTime DESC")
                        .param("status", "1")
                        .param("page", "1")
                        .param("limit", "20")
                        .param("tags", "1")
                        .param("tags", "7"));
        // query players with BOTH tags 1 and 7
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(
                "There should be a total of 2 players (player id = 1,5) having both tags 1 and 7",
                2, node.get("total").asInt());
        JsonNode playersNode = node.get("list");
        JsonNode playerNode = playersNode.get(1);
        assertEquals(1, playerNode.get("profiles").get(0).get("group").asInt());
    }

    @Test
    public void testPlayerListWithGroup() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players")
                        .param("group", "3")
        );
        // query players with group tag = 3 (Gold Nova III)
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(
                "There should be a total of 5 players under player group Gold Nova III",
                5, node.get("total").asInt());
        JsonNode playersNode = node.get("list");
        JsonNode playerNode = playersNode.get(1);
        assertEquals(3, playerNode.get("profiles").get(0).get("group").asInt());
    }

    @Test
    public void testPlayerSuggestion() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players/suggestion")
                        .param("usernameSearch", "002")
                        .param("currency", "CNY")
        );
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertFalse(data.get("list").isNull());
        assertTrue(data.get("list").size() > 0);
    }

    @Test
    public void testGetReferPlayers() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/players/{playerId}/refer-players", playerId));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertTrue("The test player must have at least one refer player", data.size() > 0);
        assertNotNull(data.get(0).get("id"));
        assertNotNull(data.get(0).get("username"));
    }

    @Test
    public void testAddBalanceToMainWallet() throws Exception {
        // ID of test001, test002
        ImmutableList<Integer> playerIds = ImmutableList.of(1, 2);
        List<Wallet> originalWallets = walletMapper.getMainWallets(playerIds);

        ImmutableList<String> usernames = ImmutableList.of("test001", "test002");
        List<WalletAmountForm> params = usernames.stream().map(username -> {
            WalletAmountForm form = new WalletAmountForm();
            form.setUsername(username);
            form.setCurrency(SiteCurrencyEnum.CNY);
            form.setAmount(new BigDecimal("-100"));
            return form;
        }).collect(Collectors.toList());

        JsonNode resultNode = mockMvcPerform(post("/players/batch-adjust-balance").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<Wallet> updatedWallets = walletMapper.getMainWallets(playerIds);
        assertEquals("balance_after should be the same with sum of originalBalance and amount",
                originalWallets.get(0).getBalance().add(new BigDecimal("-100")), updatedWallets.get(0).getBalance());
        assertEquals("balance_after should be the same with sum of originalBalance and amount",
                originalWallets.get(1).getBalance().add(new BigDecimal("-100")), updatedWallets.get(1).getBalance());
    }

    @Test
    public void testAddBalanceToMainWallet_MultiStatus() throws Exception {
        // ID of test001, test002
        ImmutableList<Integer> playerIds = ImmutableList.of(1, 2);
        List<Wallet> originalWallets = walletMapper.getMainWallets(playerIds);

        // test001 greater than 1000, test002 less than 1000, testqqq not exists
        ImmutableList<String> usernames = ImmutableList.of("test001", "test002", "testqqq");
        List<WalletAmountForm> params = usernames.stream().map(username -> {
            WalletAmountForm form = new WalletAmountForm();
            form.setUsername(username);
            form.setCurrency(SiteCurrencyEnum.CNY);
            form.setAmount(new BigDecimal("-1000"));
            form.setNote("testBatchDepositMainWallet");
            return form;
        }).collect(Collectors.toList());

        JsonNode resultNode = mockMvcPerform(post("/players/batch-adjust-balance").content(JsonUtils.objectToJson(params)));
        assertMultiStatus(resultNode);

        assertInsufficientBalance(resultNode.get("data").get(0));
        assertPlayerNotFound(resultNode.get("data").get(1));
        assertOk(resultNode.get("data").get(2));
        List<Wallet> updatedWallets = walletMapper.getMainWallets(playerIds);
        assertEquals("balance_after should be the same with sum of originalBalance and amount",
                originalWallets.get(0).getBalance().add(new BigDecimal("-1000")), updatedWallets.get(0).getBalance());
        assertEquals("balance_after should be the same with sum of originalBalance and amount",
                originalWallets.get(1).getBalance(), updatedWallets.get(1).getBalance());
    }

    @Test
    public void testAddBalanceToMainWallet_InvalidParams_Failed() throws Exception {
        ImmutableList<String> usernames = ImmutableList.of("test001", "test002");
        List<WalletAmountForm> params = usernames.stream().map(username -> {
            WalletAmountForm form = new WalletAmountForm();
            form.setUsername(username);
            form.setCurrency(SiteCurrencyEnum.CNY);
            form.setAmount(null);
            return form;
        }).collect(Collectors.toList());

        JsonNode resultNode = mockMvcPerform(post("/players/batch-adjust-balance").content(JsonUtils.objectToJson(params)));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testUpdateGlobalStatus() throws Exception {
        CurrencyStatusForm form = new CurrencyStatusForm();
        form.setStatus(PlayerStatusEnum.BLOCKED.getCode());

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/status", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        PlayerCredential player = playerCredentialMapper.get(playerId);
        assertEquals(PlayerStatusEnum.BLOCKED, player.getGlobalStatus());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        CurrencyStatusForm form = new CurrencyStatusForm();
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setStatus(PlayerStatusEnum.BLOCKED.getCode());

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/status", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertEquals(form.getStatus().intValue(), updatedPlayer.getStatus().getCode());
    }

    @Test
    public void testEnableCurrency() throws Exception {
        CurrencyForm form = new CurrencyForm();
        form.setCurrency(SiteCurrencyEnum.IDR);

        Player player = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertEquals(PlayerStatusEnum.CURRENCY_INACTIVE, player.getStatus());

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/activate-currency", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertEquals("After activating currency, player status under the given currency should be ACTIVE",
                PlayerStatusEnum.ACTIVE, updatedPlayer.getStatus());
    }

    @Test
    public void testEnableCurrency_CurrentStatusNotCurrencyInactive() throws Exception {
        CurrencyStatusForm form = new CurrencyStatusForm();
        form.setCurrency(SiteCurrencyEnum.IDR);
        form.setStatus(1);

        Player player = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        playerMapper.updateStatus(player.getId(), PlayerStatusEnum.BLOCKED);

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/activate-currency", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertEquals(PlayerStatusEnum.BLOCKED, updatedPlayer.getStatus());
    }

    @Test
    public void testUpdateCashbackStatus() throws Exception {
        CurrencyBooleanForm form = new CurrencyBooleanForm();
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setEnabled(false);

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/cashback-enabled", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertFalse(updatedPlayer.getCashbackEnabled());
    }

    @Test
    public void testUpdatePromoStatus() throws Exception {
        CurrencyBooleanForm form = new CurrencyBooleanForm();
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setEnabled(false);

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/campaign-enabled", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertFalse(updatedPlayer.getCampaignEnabled());
    }

    @Test
    public void testUpdateWithdrawEnabled() throws Exception {
        CurrencyBooleanForm form = new CurrencyBooleanForm();
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setEnabled(false);

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/withdraw-enabled", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        Player updatedPlayer = playerMapper.listByCredentialId(playerId, form.getCurrency()).get(0);
        assertFalse(updatedPlayer.getWithdrawEnabled());
    }

    @Test
    public void testResetPassword() throws Exception {
        List<Player> players = playerMapper.listByCredentialId(playerId, null);
        String originalPassword = players.get(0).getPassword();

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/password", playerId));
        assertOk(resultNode);

        String newPassword = resultNode.get("data").get("password").asText();
        Player updatedPlayer = playerMapper.get(players.get(0).getId());
        assertEquals(newPassword, updatedPlayer.getPassword());
        assertNotEquals(originalPassword, updatedPlayer.getPassword());
        assertEquals(true, updatedPlayer.getPasswordPending());
        assertTrue(updatedPlayer.isEnabled());

        Player linkedPlayer = playerMapper.get(players.get(1).getId());
        assertEquals("Set password should also affect the player account with same username under different currency",
                newPassword, linkedPlayer.getPassword());
        assertEquals(true, linkedPlayer.getPasswordPending());
        assertTrue(updatedPlayer.isEnabled());
    }

    @Test
    public void testResetWithdrawPassword() throws Exception {
        List<Player> players = playerMapper.listByCredentialId(playerId, null);
        String originalWithdrawPasswordHash = players.get(0).getWithdrawPasswordHash();

        JsonNode resultNode = mockMvcPerform(
                post("/players/{playerId}/withdraw-password", playerId));
        assertOk(resultNode);

        String responseNewWithdrawPassword = resultNode.get("data").get("password").asText();
        Player updatedPlayer = playerMapper.get(players.get(0).getId());
        String newWithdrawPasswordHash = updatedPlayer.getWithdrawPasswordHash();

        assertNotNull(responseNewWithdrawPassword);
        assertNotEquals(originalWithdrawPasswordHash, newWithdrawPasswordHash);
    }

    @Test
    public void testCreate() throws Exception {
        PlayerNewForm form = new PlayerNewForm();
        form.setUsername("test777");
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setEmail("testemail@tripleonetech.com");

        JsonNode resultNode = mockMvcPerform(post("/players/create").content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        assertFalse(resultNode.get("data").get("password").isNull());

        List<SitePrivilege> privileges = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_CURRENCY);
        for (SitePrivilege privilege : privileges) {
            SiteCurrencyEnum currency = SiteCurrencyEnum.valueOf(privilege.getValue());
            Player newPlayer = playerMapper.getByUniqueKey(form.getUsername(), currency);
            assertNotNull(newPlayer);
            assertEquals(true, newPlayer.getPasswordPending());
            assertEquals(form.getEmail(), newPlayer.getEmail());
            assertEquals(newPlayer.getCredentialId().intValue(), resultNode.get("data").get("id").asInt());
            if (currency == form.getCurrency()) {
                assertNotEquals(PlayerStatusEnum.CURRENCY_INACTIVE, newPlayer.getStatus());
            } else {
                assertEquals(PlayerStatusEnum.CURRENCY_INACTIVE, newPlayer.getStatus());
            }
        }
    }

    @Test
    public void testUsernameAvailable() throws Exception {
        String availableUsername = "availableUsername";
        String unavailableUsername = "test002";

        JsonNode resultNode = mockMvcPerform(
                get("/players/username-available/" + availableUsername));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        resultNode = mockMvcPerform(
                get("/players/username-available/" + unavailableUsername));
        assertUsernameAlreadyExist(resultNode);
    }

    @Test
    public void testQueryPlayerProfile() throws Exception {
        JsonNode node = mockMvcPerform(get("/players/{playerId}/profile", playerId));
        JsonNode data = node.get("data");
        assertEquals(playerId, data.get("id").asInt());
        assertEquals("test002", data.get("username").asText());
        assertFalse("Should contain non-null passwordPending value", data.get("passwordPending").isNull());
        assertEquals("Should have global status enabled", 1, data.get("status").asInt());

        JsonNode balanceNode = data.get("balances");
        assertTrue("Should contain balances as an array", balanceNode.isArray());
        assertEquals("Should only contain balance for three currencies", 3, balanceNode.size());
        JsonNode cnyBalance = balanceNode.get(0);
        assertEquals("Should be CNY balance", "CNY", cnyBalance.get("currency").asText());
        assertTrue("Should non-zero CNY balance", cnyBalance.get("total").asDouble() > 0.0);
        assertTrue("depositCount should be returned as number", cnyBalance.get("depositCount").isNumber());
        assertTrue("withdrawCount should be returned as number", cnyBalance.get("withdrawCount").isNumber());
        assertFalse("pendingBonus field should not be null", cnyBalance.get("pendingBonus").isNull());

        JsonNode profileNode = data.get("profiles");
        assertTrue("Should contain profiles as an array", profileNode.isArray());
        assertEquals("Should only contain profile for three currencies", 3, profileNode.size());
        JsonNode cnyProfile = profileNode.get(0);
        assertEquals("Should be CNY profile", "CNY", cnyProfile.get("currency").asText());
        assertFalse("Should contain one group", cnyProfile.get("group").isNull());
        assertTrue("Should contain more than one tag", cnyProfile.get("tags").size() > 0);
        assertFalse("Should contain language", cnyProfile.get("language").isNull());
        assertEquals("T1devLine", cnyProfile.get("line").asText());
        assertEquals("T1DevSkype", cnyProfile.get("skype").asText());
        assertEquals("1231234123", cnyProfile.get("qq").asText());
        assertEquals("T1DevWechat", cnyProfile.get("wechat").asText());
        assertEquals(2, cnyProfile.get("contactPreferenceList").size());
        assertEquals(1, cnyProfile.get("contactPreferenceList").get(0).asInt());
        assertEquals(2, cnyProfile.get("contactPreferenceList").get(1).asInt());

        JsonNode identityNode = data.get("identities");
        assertTrue("Should contain profiles as an array", identityNode.isArray());
        assertEquals("Should only contain profile for three currencies", 3, identityNode.size());
        JsonNode cnyIdentity = identityNode.get(0);
        assertEquals("Should be CNY identity", "CNY", cnyIdentity.get("currency").asText());
        assertFalse(cnyIdentity.get("firstName").isNull());
        assertFalse(cnyIdentity.get("lastName").isNull());
        assertFalse(cnyIdentity.get("gender").isNull());
        assertFalse(cnyIdentity.get("birthday").isNull());
        assertFalse(cnyIdentity.get("address").isNull());
        assertFalse(cnyIdentity.get("city").isNull());
        assertFalse(cnyIdentity.get("countryCode").isNull());
        assertFalse(cnyIdentity.get("countryPhoneCode").isNull());
        assertFalse(cnyIdentity.get("phoneNumber").isNull());

        JsonNode refererNode = data.get("referer");
        assertEquals(1, refererNode.get("id").asInt());
        assertEquals("test001", refererNode.get("username").asText());
    }

    @Test
    public void testPlayerProfileWithStatus() throws Exception {
        // global status = disabled and some player status = CURRENCY_INACTIVE
        int playerId = 18;
        JsonNode node = mockMvcPerform(get("/players/{playerId}/profile", playerId));
        JsonNode data = node.get("data");
        assertEquals(playerId, data.get("id").asInt());
        assertEquals("test018", data.get("username").asText());
        assertEquals("Should have global status disabled", 0, data.get("status").asInt());
        assertEquals("Should only return 1 set of profile as only 1 currency is active for test018", 1, data.get("profiles").size());
    }

    @Test
    public void testUpdateIdentity() throws Exception {
        PlayerIdentityForm form = new PlayerIdentityForm();
        form.setFirstName("testEditSubmit");
        form.setLastName("testEditSubmit");
        form.setPhoneNumber("testEdit123456");
        form.setAddress("testEditAddress");
        form.setCity("testEditCity");
        form.setBirthday(LocalDate.now());
        form.setCountryCode("CNH");
        form.setCountryPhoneCode("65");
        form.setGender("F");

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/identity/CNY", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertDataSuccess(node.get("data"));

        List<Player> players = playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY);
        PlayerMultiCurrency.IdentityView updatedIdentity = playerProfileMapper.getIdentityViewByPlayerId(players.get(0).getId());
        assertEquals(form.getFirstName(), updatedIdentity.getFirstName());
        assertEquals(form.getLastName(), updatedIdentity.getLastName());
        assertEquals(form.getGender(), updatedIdentity.getGender());
        assertEquals(form.getBirthday(), updatedIdentity.getBirthday());
        assertEquals(form.getAddress(), updatedIdentity.getAddress());
        assertEquals(form.getCity(), updatedIdentity.getCity());
        assertEquals(form.getCountryCode(), updatedIdentity.getCountryCode());
        assertEquals(form.getCountryPhoneCode(), updatedIdentity.getCountryPhoneCode());
        assertEquals(form.getPhoneNumber(), updatedIdentity.getPhoneNumber());
    }

    @Test
    public void testUpdateProfile() throws Exception {
        List<Integer> newTagIds = new ArrayList<>(Collections.singletonList(8));
        Integer groupId = 2;
        PlayerProfileForm form = new PlayerProfileForm();
        form.setQq("1234");
        form.setSkype("testEditSkype");
        form.setLine("testEditLine");
        form.setWechat("testEditWeChat");
        form.setLanguage("zh-HK");
        form.setGroup(groupId);
        form.setTags(newTagIds);
        form.setContactPreferenceList(Arrays.asList(1, 4));

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/profile/CNY", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertDataSuccess(node.get("data"));

        List<Player> players = playerMapper.listByCredentialId(playerId, SiteCurrencyEnum.CNY);
        Integer playerId = players.get(0).getId();
        PlayerProfile updatedProfile = playerProfileMapper.getByPlayerId(playerId);
        assertEquals(form.getLanguage(), updatedProfile.getLanguage());
        assertEquals(form.getLine(), updatedProfile.getLine());
        assertEquals(form.getSkype(), updatedProfile.getSkype());
        assertEquals(form.getQq(), updatedProfile.getQq());
        assertEquals(form.getWechat(), updatedProfile.getWechat());
        assertEquals(5, updatedProfile.getContactPreferences().intValue());

        List<Integer> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, false)
                .stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());
        assertEquals(1, playerTags.size());
        assertEquals(newTagIds, playerTags);
        List<PlayersPlayerTag> playerGroup = playersPlayerTagMapper.listGroupByPlayerIds(new ArrayList<>(Collections.singletonList(playerId)));
        assertNotNull(playerGroup);
        assertEquals(groupId, playerGroup.get(0).getPlayerTagId());
    }

    @Test
    public void testUpdateProfilePartialFail() throws Exception {
        List<Integer> newTagIds = new ArrayList<>(Collections.singletonList(4)); // invalid tag id as it's a group
        Integer groupId = 2;
        PlayerProfileForm form = new PlayerProfileForm();
        form.setQq("1234");
        form.setSkype("testEditSkype");
        form.setLine("testEditLine");
        form.setWechat("testEditWeChat");
        form.setLanguage("zh-HK");
        form.setGroup(groupId);
        form.setTags(newTagIds);

        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/profile/CNY", playerId)
                        .content(JsonUtils.objectToJson(form)));
        assertMultiStatus(node);
    }

    @Test
    public void testUpdatePlayerGroupWhenNoGroup() throws Exception {
        Integer groupId = 2;
        PlayerProfileForm form = new PlayerProfileForm();
        form.setGroup(groupId);
        form.setLanguage("zh-CN");

        // assign group id to player who does not have group id before
        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/profile/CNY", 12)
                        .content(JsonUtils.objectToJson(form)));
        assertDataSuccess(node.get("data"));

        List<Player> players = playerMapper.listByCredentialId(12, SiteCurrencyEnum.CNY);
        Integer playerId = players.get(0).getId();
        List<Integer> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, true)
                .stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());
        assertEquals(1, playerTags.size());
        assertEquals(groupId, playerTags.get(0));
    }

    @Test
    public void testDeleteGroup() throws Exception {
        PlayerProfileForm form = new PlayerProfileForm();
        form.setLanguage("zh-CN");
        form.setGroup(null);

        // allow to delete group id
        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/profile/CNY", 2)
                        .content(JsonUtils.objectToJson(form)));
        assertDataSuccess(node.get("data"));
        List<Player> players = playerMapper.listByCredentialId(2, SiteCurrencyEnum.CNY);
        Integer playerId = players.get(0).getId();
        List<Integer> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, true)
                .stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());
        assertEquals(0, playerTags.size());
    }

    @Test
    public void testDeleteTag() throws Exception {
        PlayerProfileForm form = new PlayerProfileForm();
        form.setTags(new ArrayList<>());
        form.setLanguage("zh-CN");

        // allow to delete all tags
        JsonNode node = mockMvcPerform(
                post("/players/{playerId}/profile/CNY", 2)
                        .content(JsonUtils.objectToJson(form)));
        assertDataSuccess(node.get("data"));
        List<Player> players = playerMapper.listByCredentialId(2, SiteCurrencyEnum.CNY);
        Integer playerId = players.get(0).getId();
        List<Integer> playerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, false)
                .stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());
        assertEquals(0, playerTags.size());
    }

    @Test
    public void testAccountTransactionSummary() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/players/{playerId}/account-transaction-summary", playerId));

        assertOk(resultNode);
        JsonNode data = resultNode.get("data");

        JsonNode usdSummary = data.get(0);
        assertEquals("Should be CNY balance", "CNY", usdSummary.get("currency").asText());
        assertTrue("Should non-zero CNY withdrawal Amount", usdSummary.get("withdrawalTotalAmount").asDouble() > 0.0);
        assertTrue("Should non-zero CNY bonus Amount", usdSummary.get("bonusTotalAmount").asDouble() > 0.0);
    }

    @Test
    public void testGetEmail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/players/{playerId}/email", playerId));
        assertOk(resultNode);

        assertEquals("tripleonetechsg@gmail.com", resultNode.get("data").get("email").asText());
    }

    @Test
    public void testUpdateEmail() throws Exception {
        Map<String, String> mockData = new HashMap<>();
        mockData.put("email", "testemail@tripleonetech.com");

        JsonNode resultNode = mockMvcPerform(post("/players/{playerId}/email", playerId)
                .content(JsonUtils.objectToJson(mockData)));
        assertOk(resultNode);

        PlayerCredential playerCredential = playerCredentialMapper.get(playerId);
        assertEquals(mockData.get("email"), playerCredential.getEmail());
    }

    @Test
    public void testUpdateEmail_InvalidPattern_Failed() throws Exception {
        Map<String, String> mockData = new HashMap<>();
        mockData.put("email", "testemailtripleonetech.com");

        JsonNode resultNode = mockMvcPerform(post("/players/{playerId}/email", playerId)
                .content(JsonUtils.objectToJson(mockData)));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("must be a well-formed email address"));
    }

    @Test
    public void testAvailablePaymentMethods() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/players/{playerId}/payment-methods/{currency}", 2, "CNY"));
        assertOk(resultNode);

        JsonNode dataNode = resultNode.get("data");
        assertEquals("There are 3 payment methods available to player test002 CNY",
                3, dataNode.size());
        IntStream.range(0, dataNode.size()).forEach(i ->
                assertEquals("CNY", dataNode.get(i).get("currency").asText())
        );
    }
}
