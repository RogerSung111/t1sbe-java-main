package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.cashback.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class PromoCashbackControllerTest extends BaseMainTest {
    @Autowired
    private PromoCashbackMapper promoCashbackMapper;

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback"));
        JsonNode node = resultNode.get("data");

        assertEquals(6, node.size());
        assertTrue(node.get(0).get("gameTypes").isArray());
    }

    @Test
    public void testListByGroupId() throws Exception {
        int groupId = 1;
        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}", groupId));
        JsonNode node = resultNode.get("data");

        assertEquals(2, node.size());
        JsonNode jsonNode = node.get(0);
        assertTrue(jsonNode.get("gameTypes").isArray());
        assertEquals(groupId, jsonNode.get("groupId").asInt());
    }

    @Test
    public void testListByDefaultGroupId() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}", 0));
        JsonNode node = resultNode.get("data");

        assertEquals(2, node.size());
        JsonNode jsonNode = node.get(0);
        assertTrue(jsonNode.get("gameTypes").isArray());
        assertEquals(0, jsonNode.get("groupId").asInt());
    }

    @Test
    public void testDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/{id}", 1));
        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("name"));
        assertNotNull(node.get("groupId"));
        assertNotNull(node.get("gameTypes"));
        assertNotNull(node.get("settings"));
    }

    @Test
    public void testCreate() throws Exception {
        CashbackForm form = new CashbackForm();
        initCommonFormParams(form);
        form.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/create").content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);

        // new record is 7, read it back to ensure all properties are saved to DB
        resultNode = mockMvcPerform(get("/promotions/cashback/{id}", resultNode.get("data").get("id").asInt()));
        JsonNode cashbackNode = resultNode.get("data");
        assertCommonFormTest(form, cashbackNode);
        assertEquals(form.getCurrency().name(), cashbackNode.get("currency").asText());
    }

    @Test
    public void testCreate_OnlyRequiredParams() throws Exception {
        CashbackForm form = new CashbackForm();
        initCommonFormParams(form);
        form.setCurrency(SiteCurrencyEnum.USD);
        form.setGroupId(null);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/create").content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
    }

    @Test
    public void testCreate_ExceedNumberBetRange_Failed() throws Exception {
        CashbackForm form = new CashbackForm();
        initCommonFormParams(form);
        form.setCurrency(SiteCurrencyEnum.USD);
        // set empty percentage
        PromoCashbackBetRange betRange = new PromoCashbackBetRange();
        betRange.setLimit(new BigDecimal("-100"));
        betRange.setCashbackPercentage(new BigDecimal("100.01"));
        form.setSettings(Arrays.asList(betRange));

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/create").content(JsonUtils.objectToJson(form)));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("must be greater than or equal to 0"));
        assertTrue(resultNode.get("data").get("message").asText().contains("must be less than or equal to 100"));
    }

    @Test
    public void testUpdate() throws Exception {
        CashbackForm form = new CashbackForm();
        initCommonFormParams(form);
        form.setEnabled(false);
        form.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/1")
                .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);

        resultNode = mockMvcPerform(get("/promotions/cashback/1"));
        JsonNode cashbackNode = resultNode.get("data");
        assertCommonFormTest(form, cashbackNode);
        assertEquals(form.getEnabled(), cashbackNode.get("enabled").asBoolean());
        assertEquals(form.getCurrency().toString(), cashbackNode.get("currency").textValue());
    }

    @Test
    public void testUpsertBatch() throws Exception {
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();

        Integer testGroupId = 3;
        int testCashbackId = 1;

        CashbackForm.CashbackBatchUpdateForm updateForm = new CashbackForm.CashbackBatchUpdateForm();
        initCommonFormParams(updateForm);
        updateForm.setId(testCashbackId);
        updateForm.setEnabled(false);
        updateForm.setCurrency(SiteCurrencyEnum.USD);

        CashbackForm insertForm = new CashbackForm();
        initCommonFormParams(insertForm);
        insertForm.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/group/{groupId}", testGroupId)
                .content(JsonUtils.objectToJson(Arrays.asList(updateForm, insertForm))));
        assertOk(resultNode);

        Cashback cashback = promoCashbackMapper.get(testCashbackId);
        assertFalse(cashback.isEnabled());
        assertEquals(testGroupId, cashback.getGroupId());
        assertEquals(updateForm.getCurrency(), cashback.getCurrency());

        List<CashbackView> group3 = promoCashbackMapper.listViewByGroup(testGroupId, locale, defaultLocale);
        assertEquals(2 , group3.size());

        CashbackForm form = insertForm;
        for (CashbackView cashbackView : group3) {
            resultNode = mockMvcPerform(get("/promotions/cashback/" + cashbackView.getId()));
            JsonNode cashbackNode = resultNode.get("data");
            if (cashbackView.getId() == testCashbackId) {
                form = updateForm;
            }
            assertCommonFormTest(form, cashbackNode);
        }
    }

    @Test
    public void testUpsertBatchOnlyInsert() throws Exception {
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();

        Integer testGroupId = 3;

        CashbackForm insertForm = new CashbackForm();
        initCommonFormParams(insertForm);
        insertForm.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/group/{groupId}", testGroupId)
                .content(JsonUtils.objectToJson(Collections.singletonList(insertForm))));
        assertOk(resultNode);

        List<CashbackView> group3 = promoCashbackMapper.listViewByGroup(testGroupId, locale, defaultLocale);
        assertEquals(1 , group3.size());

        for (CashbackView cashbackView : group3) {
            resultNode = mockMvcPerform(get("/promotions/cashback/" + cashbackView.getId()));
            JsonNode cashbackNode = resultNode.get("data");
            assertCommonFormTest(insertForm, cashbackNode);
        }
    }

    @Test
    public void testUpsertBatchOnlyUpdate() throws Exception {
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();

        Integer testGroupId = 3;
        int testCashbackId = 1;

        CashbackForm.CashbackBatchUpdateForm updateForm = new CashbackForm.CashbackBatchUpdateForm();
        initCommonFormParams(updateForm);
        updateForm.setId(testCashbackId);
        updateForm.setEnabled(false);
        updateForm.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/group/{groupId}", testGroupId)
                .content(JsonUtils.objectToJson(Collections.singletonList(updateForm))));
        assertOk(resultNode);

        Cashback cashback = promoCashbackMapper.get(testCashbackId);
        assertFalse(cashback.isEnabled());
        assertEquals(testGroupId, cashback.getGroupId());
        assertEquals(updateForm.getCurrency(), cashback.getCurrency());

        List<CashbackView> group3 = promoCashbackMapper.listViewByGroup(testGroupId, locale, defaultLocale);
        assertEquals(1 , group3.size());

        for (CashbackView cashbackView : group3) {
            resultNode = mockMvcPerform(get("/promotions/cashback/" + cashbackView.getId()));
            JsonNode cashbackNode = resultNode.get("data");
            assertCommonFormTest(updateForm, cashbackNode);
        }
    }

    @Test
    public void testUpdate_OnlyRequiredParams() throws Exception {
        CashbackForm form = new CashbackForm();
        initCommonFormParams(form);
        form.setEnabled(false);
        form.setGroupId(null);
        form.setCurrency(SiteCurrencyEnum.USD);

        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/1")
                .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);

        resultNode = mockMvcPerform(get("/promotions/cashback/1"));
        JsonNode cashbackNode = resultNode.get("data");
        assertCommonFormTest(form, cashbackNode);
        assertEquals(form.getEnabled(), cashbackNode.get("enabled").asBoolean());
        assertEquals(form.getCurrency().toString(), cashbackNode.get("currency").textValue());
    }

    private void initCommonFormParams(CashbackForm form) {
        form.setName("test");
        form.setBonusAutoRelease(true);
        form.setGroupId(3);
        form.setEnabled(true);
        List<PromoCashbackBetRange> betRanges = Arrays.stream(new String[]{"0.000", "111.000"}).map(source -> {
            PromoCashbackBetRange target = new PromoCashbackBetRange();
            target.setLimit(new BigDecimal(source));
            target.setCashbackPercentage(new BigDecimal("12"));
            return target;
        }).collect(Collectors.toList());
        form.setSettings(betRanges);
        List<CashbackGameType> gameTypes = Arrays.stream(new String[]{"1", "2", "3"}).map(source -> {
            CashbackGameType target = new CashbackGameType();
            target.setGameApiId(5);
            target.setGameTypeId(Integer.valueOf(source));
            return target;
        }).collect(Collectors.toList());
        form.setGameTypes(gameTypes);
    }

    private void assertCommonFormTest(CashbackForm form, JsonNode node) {
        assertEquals(form.getName(), node.get("name").asText());
        assertEquals(form.getBonusAutoRelease(), node.get("bonusAutoRelease").asBoolean());
        if (form.getGroupId() != null) {
            assertEquals(form.getGroupId().intValue(), node.get("groupId").asInt());
        } else {
            assertTrue(node.get("groupId").isNull());
        }
        assertEquals(form.getSettings().isEmpty() ? 1 : form.getSettings().size(), node.get("settings").size());
        assertEquals(form.getGameTypes().isEmpty() ? 1 : form.getGameTypes().size(), node.get("gameTypes").size());
        IntStream.range(0, form.getSettings().size()).forEach(idx -> {
            assertTrue(form.getSettings().get(idx).getLimit()
                    .compareTo(new BigDecimal(node.get("settings").get(idx).get("limit").asText())) == 0);
            assertTrue(form.getSettings().get(idx).getCashbackPercentage()
                    .compareTo(new BigDecimal(node.get("settings").get(idx).get("cashbackPercentage").asText())) == 0);
        });
        IntStream.range(0, form.getGameTypes().size()).forEach(idx -> {
            assertEquals(form.getGameTypes().get(idx).getGameApiId().intValue(),
                    node.get("gameTypes").get(idx).get("gameApiId").asInt());
            assertEquals(form.getGameTypes().get(idx).getGameTypeId().intValue(),
                    node.get("gameTypes").get(idx).get("gameTypeId").asInt());
        });
    }

    @Test
    public void testUpdateEnabled() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/{id}/enabled", 1)
                .content("{\"enabled\":0}"));
        assertOk(resultNode);
        Cashback cashback = promoCashbackMapper.get(1);
        assertEquals(Boolean.FALSE, cashback.isEnabled());
    }

    @Test
    public void testDelete() throws Exception {
        Cashback cashback = promoCashbackMapper.get(1);
        assertFalse(cashback.isDeleted());
        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/{id}/delete", 1));
        assertOk(resultNode);
        cashback = promoCashbackMapper.get(1);
        assertNull(cashback);
    }

    @Test
    public void testDeleteBatch() throws Exception {
        Cashback cashback = promoCashbackMapper.get(1);
        assertFalse(cashback.isDeleted());
        JsonNode resultNode = mockMvcPerform(post("/promotions/cashback/group/{groupId}/delete", 1));
        assertOk(resultNode);
        cashback = promoCashbackMapper.get(1);
        assertNull(cashback);
    }

    @Test
    public void testGetUsableGameTypes_ByGroupId() throws Exception {
        Integer groupId = 1; // rule id = 1 & 2

        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}/usable-game-types", groupId));
        assertOk(resultNode);

        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        List<CashbackGameType> gameTypesOfRule1 = promoCashbackMapper.getGameTypes(1, locale);
        List<CashbackGameType> gameTypesOfRule2 = promoCashbackMapper.getGameTypes(2, locale);

        List<CashbackGameType> existedGameTypes = new ArrayList<>();
        existedGameTypes.addAll(gameTypesOfRule1);
        existedGameTypes.addAll(gameTypesOfRule2);

        for (JsonNode data : resultNode.get("data")) {
            boolean hasExistedValue = existedGameTypes.stream()
                    .anyMatch(value -> value.getGameApiId() == data.get("gameApiId").asInt()
                            && value.getGameTypeId() == data.get("gameTypeId").asInt());
            assertFalse(hasExistedValue);
            assertNotNull(data.get("currency").textValue());
        }
    }

    @Test
    public void testGetUsableGameTypesWithCurrency() throws Exception {
        Integer groupId = 1; // rule id = 1 & 2

        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}/usable-game-types", groupId));
        assertOk(resultNode);
        int allUsableCount = resultNode.get("data").size();

        resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}/usable-game-types", groupId).param("currency", "USD"));
        assertOk(resultNode);
        assertNotEquals(allUsableCount, resultNode.get("data").size());

        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        List<CashbackGameType> gameTypesOfRule1 = promoCashbackMapper.getGameTypes(1, locale);
        List<CashbackGameType> gameTypesOfRule2 = promoCashbackMapper.getGameTypes(2, locale);

        List<CashbackGameType> existedGameTypes = new ArrayList<>();
        existedGameTypes.addAll(gameTypesOfRule1);
        existedGameTypes.addAll(gameTypesOfRule2);

        for (JsonNode data : resultNode.get("data")) {
            boolean hasExistedValue = existedGameTypes.stream()
                    .anyMatch(value -> value.getGameApiId() == data.get("gameApiId").asInt()
                            && value.getGameTypeId() == data.get("gameTypeId").asInt());
            assertFalse(hasExistedValue);
            assertNotNull(data.get("currency").textValue());
        }
    }

    @Test
    public void testGetUsableGameTypesWithoutCurrency() throws Exception {
        Integer groupId = 1; // rule id = 1 & 2

        JsonNode resultNode = mockMvcPerform(get("/promotions/cashback/group/{groupId}/usable-game-types", groupId));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").size() > 1);
    }

    @Test
    @Ignore("Preview function has been taken down temporarily")
    public void testPreview() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/promotions/cashback/preview"));
        assertOk(resultNode);

        JsonNode dataNode = resultNode.get("data");
        assertTrue(dataNode.get("allLimits").size() > 0);
        assertTrue(dataNode.get("existingGameTypes").size() > 0);
        assertTrue(dataNode.get("percentages").size() > 0);
    }
}
