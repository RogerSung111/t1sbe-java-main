package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.payment.PaymentMethod;
import com.tripleonetech.sbe.payment.PaymentMethodMapper;
import com.tripleonetech.sbe.payment.config.PaymentApiConfig;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;

public class PaymentApiConfigControllerTest extends BaseMainTest {
    
    @Autowired
    private PaymentApiConfigMapper paymentApiConfigMapper;
    
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    
    private int newPaymentApiConfigId;
    String newApiConfigName;

    @Before
    public void setUp() throws Exception {
        newPaymentApiConfigId = 3;
        newApiConfigName = "TEST_PAYMNET_API";
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/system/payment-api"));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        JsonNode paymentApi = node.get(0);

        assertEquals("MockPay", paymentApi.get("code").asText());
        assertFalse(paymentApi.get("dailyMaxDeposit").isNull());
        assertFalse(paymentApi.get("minDepositPerTrans").isNull());
        assertFalse(paymentApi.get("maxDepositPerTrans").isNull());
        assertFalse(paymentApi.get("chargeRatio").isNull());
        assertFalse(paymentApi.get("fixedCharge").isNull());
    }

    @Test
    public void testListSingleApi() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/system/payment-api/" + this.newPaymentApiConfigId));

        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals("MockPay", node.get("code").asText());
        assertEquals("THB", node.get("currency").asText());
    }

    @Test
    public void testEditSubmit() throws Exception {
        PaymentApiConfig paymentApiConfig = paymentApiConfigMapper.get(this.newPaymentApiConfigId);
        paymentApiConfig.setName(newApiConfigName);
        paymentApiConfig.setDailyMaxDeposit(new BigDecimal("123.45"));
        paymentApiConfig.setMaxDepositPerTrans(new BigDecimal("123.46"));
        paymentApiConfig.setFixedCharge(new BigDecimal("123.47"));
        // simulate the case where not all fields are given
        paymentApiConfig.setCode(null);
        paymentApiConfig.setMeta(null);

        String paymentApiConfigStr = JsonUtils.objectToJson(paymentApiConfig);
        JsonNode result = mockMvcPerform(
                post("/system/payment-api/" + this.newPaymentApiConfigId).content(paymentApiConfigStr));

        assertOk(result);

        PaymentApiConfig updatedPaymentApiConfig = paymentApiConfigMapper.get(newPaymentApiConfigId);
        assertEquals(newApiConfigName, updatedPaymentApiConfig.getName());
        assertEquals(0, paymentApiConfig.getDailyMaxDeposit().compareTo(updatedPaymentApiConfig.getDailyMaxDeposit()));
        assertEquals(0, paymentApiConfig.getDailyMaxDeposit().compareTo(updatedPaymentApiConfig.getDailyMaxDeposit()));
        assertEquals(0, paymentApiConfig.getMaxDepositPerTrans().compareTo(updatedPaymentApiConfig.getMaxDepositPerTrans()));
    }
    
    @Test
    public void testEditPaymentApiConfigAndAmountsShouldBeUpdated() throws Exception {
        
        BigDecimal expectedMinDepositPerTrans = new BigDecimal("100");
        BigDecimal expectedMaxDepositPerTrans = new BigDecimal("2000");
        BigDecimal expectedDailyMaxDeposit = new BigDecimal("30000");

        PaymentApiConfig paymentApiConfig = paymentApiConfigMapper.get(this.newPaymentApiConfigId);
        paymentApiConfig.setName(newApiConfigName);
        paymentApiConfig.setMinDepositPerTrans(expectedMinDepositPerTrans);
        paymentApiConfig.setMaxDepositPerTrans(expectedMaxDepositPerTrans);
        paymentApiConfig.setDailyMaxDeposit(expectedDailyMaxDeposit);
        paymentApiConfig.setFixedCharge(new BigDecimal("123.47"));
        // simulate the case where not all fields are given
        paymentApiConfig.setCode(null);
        paymentApiConfig.setMeta(null);

        String paymentApiConfigStr = JsonUtils.objectToJson(paymentApiConfig);
        JsonNode result = mockMvcPerform(
                post("/system/payment-api/" + this.newPaymentApiConfigId).content(paymentApiConfigStr));

        assertOk(result);
        assertTrue(result.get("data").size() > 0);
        
        JsonNode paymentMethodSimpleView = result.get("data");
        paymentMethodSimpleView.forEach(p -> {
            PaymentMethod paymentMethod = paymentMethodMapper.get(p.get("id").asInt());
            assertTrue("MinDepositPerTrans amount should be "+ expectedMinDepositPerTrans, paymentMethod.getMinDepositPerTrans().compareTo(expectedMinDepositPerTrans) == 0);
            assertTrue("MaxDepositPerTrans amount should be "+ expectedMaxDepositPerTrans, paymentMethod.getMaxDepositPerTrans().compareTo(expectedMaxDepositPerTrans) == 0);
            assertTrue("DailyMaxDeposit amount should be "+ expectedDailyMaxDeposit, paymentMethod.getDailyMaxDeposit().compareTo(expectedDailyMaxDeposit) == 0);

        });
    }

    @Test
    public void testCreateSubmit() throws Exception {
        PaymentApiConfig paymentApiConfig = new PaymentApiConfig();
        paymentApiConfig.setName("testCreateSubmit");
        paymentApiConfig.setCode("MockPay");
        paymentApiConfig.setDailyMaxDeposit(new BigDecimal("123.45"));
        paymentApiConfig.setMaxDepositPerTrans(new BigDecimal("123.46"));
        paymentApiConfig.setFixedCharge(new BigDecimal("123.47"));
        paymentApiConfig.setCurrency(SiteCurrencyEnum.CNY);
        String apiConfigStr = JsonUtils.objectToJson(paymentApiConfig);
        JsonNode result = mockMvcPerform(post("/system/payment-api/create").content(apiConfigStr));

        assertOk(result);
        assertEquals(16, result.get("data").get("id").asInt());
    }

    @Test
    public void testUpdateStatus() throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("status", 0);
        JsonNode resultNode = mockMvcPerform(post("/system/payment-api/" + this.newPaymentApiConfigId + "/status")
                .content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void testDeleteSubmit() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/system/payment-api/" + this.newPaymentApiConfigId + "/delete"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void testTestConnectivity() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/system/payment-api/" + this.newPaymentApiConfigId + "/test"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

}
