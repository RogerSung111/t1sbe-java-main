package com.tripleonetech.sbe.web.opbackend;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.BooleanForm;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;
import com.tripleonetech.sbe.promo.campaign.CampaignApprovalStatusEnum;
import com.tripleonetech.sbe.promo.campaign.CampaignRunningStatusEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignListQueryForm;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDeposit;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDepositForm;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDepositMapper;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLogin;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginForm;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginMapper;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginRuleForm;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTask;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskForm;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskMapper;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskTypeEnum;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PromoCampaignControllerTest extends BaseMainTest {
    @Autowired
    private PromoCampaignDepositMapper depositMapper;
    @Autowired
    private PromoCampaignTaskMapper taskMapper;
    @Autowired
    private PromoCampaignLoginMapper loginMapper;

    @Test
    public void testQuery() throws Exception {
        PromoCampaignListQueryForm form = new PromoCampaignListQueryForm();
        form.setType(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        form.setStatus(Arrays.asList(CampaignApprovalStatusEnum.PENDING_APPROVAL.getCode()));
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setAutoJoin(false);

        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns")
                .param("type", "" + form.getType())
                .param("status", "" + CampaignApprovalStatusEnum.PENDING_APPROVAL.getCode())
                .param("currency", form.getCurrency().toString())
                .param("autoJoin", form.isAutoJoin() ? "true":"false")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);

        JsonNode list = data.get("list");
        //assertEquals(1, node.get("data").get("list").size());
        assertTrue(list.size() >= 1);

        JsonNode item = list.get(0);
        assertFalse("Campaign should have uid field", item.get("uid").isNull());
        assertFalse("Campaign should have currency field", item.get("currency").isNull());
        assertEquals("Campaign should have type field matching PromoTypeEnum",
                PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode(), item.get("type").asInt());
        assertFalse("Campaign should have auto-join set to false", item.get("autoJoin").asBoolean());
        assertFalse("Campaign should have effectiveStartTime", item.get("effectiveStartTime").isNull());
        assertEquals(CampaignRunningStatusEnum.PENDING.getCode(), item.get("runningStatus").asInt());
    }

    @Test
    public void testQueryNoParam() throws Exception {
        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns")
        );
        assertOk(node);
        assertNotNull(node.get("data"));
        assertTrue(node.get("data").get("list").size() >= 6);
    }

    @Test
    public void testGetDepositCampaign() throws Exception {
        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns/deposit/2")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);
        assertEquals(CampaignApprovalStatusEnum.APPROVED.getCode(), data.get("status").asInt());
        assertEquals(BonusReceiveCycleEnum.ONCE.getCode(), data.get("bonusReceiveCycle").asInt());
        assertEquals(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode(), data.get("type").asInt());
        assertFalse(data.get("effectiveStartTime").isNull());
        assertTrue(data.get("bonusCount").asInt() > 0);
        assertTrue(data.get("pendingBonusCount").asInt() > 0);
        assertTrue(data.get("bonusPlayerCount").asInt() > 0);
    }

    @Test
    public void testGetLoginCampaign() throws Exception {
        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns/login/2"));
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);
        assertEquals(CampaignApprovalStatusEnum.APPROVED.getCode(), data.get("status").asInt());
    }

    @Test
    @Ignore("Pending merging enabled into status")
    public void testDisable() throws Exception {
        BooleanForm form = new BooleanForm();
        form.setEnabled(false);
        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns/task/1")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);
        assertEquals(CampaignApprovalStatusEnum.APPROVED.getCode(), data.get("status").asInt());
        assertEquals(BonusReceiveCycleEnum.ONCE.getCode(), data.get("bonusReceiveCycle").asInt());
        assertEquals(PromoTypeEnum.CAMPAIGN_TASK.getCode(), data.get("type").asInt());
        assertFalse(data.get("effectiveStartTime").isNull());
    }

    @Test
    public void testGetCampaignBonuses() throws Exception {
        PageQueryForm queryForm = new PageQueryForm();
        queryForm.setPage(1);
        queryForm.setLimit(100);

        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns/deposit/2/bonuses")
                        .param("page", queryForm.getPage().toString())
                        .param("limit", queryForm.getLimit().toString())
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);

        assertTrue("Should return more than 1 bonuses", data.get("size").asInt() > 0);

        JsonNode bonus = data.get("list").get(0);
        assertEquals(PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode(), bonus.get("promoType").asInt());
    }

    @Test
    public void testGetCampaignPlayers() throws Exception {
        PageQueryForm queryForm = new PageQueryForm();
        queryForm.setPage(1);
        queryForm.setLimit(100);
        //queryForm.setSort("id DESC");

        JsonNode node = mockMvcPerform(
                get("/promotions/campaigns/deposit/2/players")
                        .param("page", queryForm.getPage().toString())
                        .param("limit", queryForm.getLimit().toString())
                        .param("sort", "id DESC")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);

        assertTrue("Should return more than 1 players", data.get("size").asInt() > 0);

        JsonNode player = data.get("list").get(0);
        assertEquals(19, player.get("id").asInt());
    }

    @Test
    public void testDelete() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/4/delete")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(4);
        assertTrue(deposit.isDeleted());
    }

    @Test
    public void testDeleteFail() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/1/delete")
        );
        assertInvalidStatus(node);
    }

    @Test
    public void testSubmit() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/4/submit")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(4);
        assertEquals(CampaignApprovalStatusEnum.PENDING_APPROVAL, deposit.getStatus());
    }

    @Test
    public void testReject() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/3/reject")
                .content("{\"reason\":\"test reason\"}")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(3);
        assertEquals(CampaignApprovalStatusEnum.REJECTED, deposit.getStatus());
        assertEquals("test reason", deposit.getComment());
    }

    @Test
    public void testApprove() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/3/approve")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(3);
        assertEquals(CampaignApprovalStatusEnum.APPROVED, deposit.getStatus());
        assertTrue("The effective start time should have been set to now",
                LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
            - deposit.getEffectiveStartTime().toEpochSecond(ZoneOffset.UTC) < 1);
    }

    @Test
    public void testStart() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/6/start")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(6);
        assertNotNull(deposit.getEffectiveStartTime());
        assertEquals(CampaignRunningStatusEnum.RUNNING, deposit.getRunningStatus());
    }

    @Test
    public void testTerminate() throws Exception {
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/2/terminate")
        );
        assertOk(node);

        PromoCampaignDeposit deposit = depositMapper.get(2);
        assertNotNull(deposit.getEffectiveEndTime());
        assertEquals(CampaignRunningStatusEnum.FINISHED, deposit.getRunningStatus());
    }

    @Test
    public void testDeleteLoginCampaign() throws Exception {
        BooleanForm form = new BooleanForm();
        form.setEnabled(true);
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/login/1/delete")
                        .content(JsonUtils.objectToJson(form)));
        assertOk(node);

        PromoCampaignLogin loginCampaign = loginMapper.get(1);
        assertTrue(loginCampaign.isDeleted());
    }

    @Test
    public void testCreateNewDepositCampaign() throws Exception {
        PromoCampaignDepositForm form = new PromoCampaignDepositForm();
        form.setMinDeposit(BigDecimal.TEN);
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setBonusAutoRelease(true);
        form.setName("zh-CN", "test");
        form.setStartTime(LocalDateTime.now());
        form.setBonusReceiveCycle(BonusReceiveCycleEnum.ONCE.getCode());
        form.setWithdrawConditionMultiplier(10);
        form.setFixedBonus(new BigDecimal("38.00"));
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));
        assertFalse(node.get("data").get("id").isNull());
    }

    @Test
    public void testCreateNewDepositCampaignWithoutRequiredField() throws Exception {
        PromoCampaignDepositForm form = new PromoCampaignDepositForm();
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setBonusAutoRelease(true);
        form.setMaxBonusAmount(new BigDecimal(1000.00));
        form.setName("zh-CN", "test");
        form.setStartTime(LocalDateTime.now());
        form.setBonusReceiveCycle(BonusReceiveCycleEnum.ONCE.getCode());
        //form.setWithdrawConditionMultiplier(10);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );

        assertInvalidParameter(node);
    }

    @Test
    public void testCreateNewTaskCampaign() throws Exception {
        PromoCampaignTaskForm form = new PromoCampaignTaskForm();
        form.setName("zh-CN", "test save promo task");
        form.setContent("zh-CN", "test save promo task");
        form.setFixedBonus(new BigDecimal(8.000));
        form.setBonusAutoRelease(true);
        form.setAutoJoin(true);
        form.setCurrency(SiteCurrencyEnum.CNY);
        form.setBonusReceiveCycle(BonusReceiveCycleEnum.ONCE.getCode());
        form.setTaskType(PromoCampaignTaskTypeEnum.IdentityVerificationBonus.getCode());
        form.setWithdrawConditionMultiplier(10);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/campaigns/task/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);
        assertFalse(node.get("id").isNull());
    }

    @Test
//    @Ignore("TODO: SBEJ-1429")
    public void testCreateNewLoginCampaign() throws Exception {
        PromoCampaignLoginForm form = new PromoCampaignLoginForm();
        form.setName("zh-CN", "test save promo login");
        form.setContent("zh-CN", "test save promo login");
        form.setBonusAutoRelease(true);
        form.setAutoJoin(true);
        form.setCurrency(SiteCurrencyEnum.CNY);
//        form.setBonusReceiveCycle(BonusReceiveCycleEnum.ONCE.getCode());
        form.setWithdrawConditionMultiplier(6);

        List<PromoCampaignLoginRuleForm> rules = new ArrayList<>();
        rules.add(new PromoCampaignLoginRuleForm((byte) 2, new BigDecimal("28.00")));

        form.setRules(rules);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/campaigns/login/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);
        assertFalse(node.get("id").isNull());
    }

    @Test
    public void testCreateNewLoginCampaign_WithoutRules_Fail() throws Exception {
        PromoCampaignLoginForm form = new PromoCampaignLoginForm();
        form.setName("zh-CN", "test save promo login");
        form.setContent("zh-CN", "test save promo login");
        form.setBonusAutoRelease(true);
        form.setAutoJoin(true);
        form.setCurrency(SiteCurrencyEnum.CNY);
//        form.setBonusReceiveCycle(BonusReceiveCycleEnum.ONCE.getCode());
        form.setWithdrawConditionMultiplier(5);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/campaigns/login/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOperationFailed(resultNode);
    }

    @Test
    public void testUpdateDepositCampaign() throws Exception {
        PromoCampaignDepositForm form = new PromoCampaignDepositForm();
        form.setName("zh-CN", "test");
        form.setContent("zh-CN", "test");
        form.setPercentageBonus(BigDecimal.TEN);
        String requestBody = JsonUtils.objectToJson(form);

        // Deposit campaign id = 1 has a fixed bonus
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));

        PromoCampaignDeposit updatedCampaign = depositMapper.get(3);
        assertEquals(form.getName("zh-CN"), updatedCampaign.getName("zh-CN"));
        assertNull("Updated campaign should have its fixed bonus value set to null", updatedCampaign.getFixedBonus());
        assertEquals(0,BigDecimal.TEN.compareTo(updatedCampaign.getPercentageBonus()));
        assertEquals("After editing, campaign should go back to DRAFT status", CampaignApprovalStatusEnum.DRAFT, updatedCampaign.getStatus());
    }

    @Test
    public void testUpdateDepositCampaignUnsetEndTime() throws Exception {
        PromoCampaignDepositForm form = new PromoCampaignDepositForm();
        form.setEndTime(null);
        form.setPercentageBonus(BigDecimal.TEN);
        String requestBody = JsonUtils.objectToJson(form);

        // Deposit campaign id = 1 has an end_time
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));

        PromoCampaignDeposit updatedCampaign = depositMapper.get(3);
        assertNull("Updated campaign should have its end_time cleared", updatedCampaign.getEndTime());
    }

    @Test
    public void testUpdateDepositCampaignWrongStatus() throws Exception {
        PromoCampaignDepositForm form = new PromoCampaignDepositForm();
        form.setName("zh-CN", "test");
        form.setContent("zh-CN", "test");
        form.setPercentageBonus(BigDecimal.TEN);
        String requestBody = JsonUtils.objectToJson(form);

        // Deposit campaign id = 1 is approved and cannot be edited
        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/deposit/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertInvalidStatus(node);
    }

    @Test
    public void testUpdateTaskCampaign() throws Exception {
        PromoCampaignTaskForm form = new PromoCampaignTaskForm();
        form.setName("zh-CN", "test");
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/task/4")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf())
        );
        assertOk(node);
        assertDataSuccess(node.get("data"));

        PromoCampaignTask updatedCampaign = taskMapper.get(4);
        assertEquals(form.getName("zh-CN"), updatedCampaign.getName("zh-CN"));
        assertEquals("After editing, campaign should go back to DRAFT status", CampaignApprovalStatusEnum.DRAFT, updatedCampaign.getStatus());
    }

    @Test
//    @Ignore("TODO: SBEJ-1429")
    public void testUpdateLoginCampaign() throws Exception {
        PromoCampaignLoginForm form = new PromoCampaignLoginForm();
        form.setName("zh-CN", "test");

        String requestBody = JsonUtils.objectToJson(form);

        JsonNode node = mockMvcPerform(
                post("/promotions/campaigns/login/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .with(csrf()));
        assertOk(node);
        assertDataSuccess(node.get("data"));

        PromoCampaignLogin updatedCampaign = loginMapper.get(1);
        //TODO:(blocked by SBEJ-1472) test updated campaign name with i18n pattern
//        assertEquals(form.getName("zh-CN"), updatedCampaign.getName());
    }

    @Test
    public void testAddRequiredLoginDayRule() throws Exception {
        PromoCampaignLoginRuleForm ruleForm = new PromoCampaignLoginRuleForm();
        ruleForm.setLoginDay((byte) 5);
        ruleForm.setFixedBonus(new BigDecimal("88.000"));

        PromoCampaignLoginRuleForm ruleForm2 = new PromoCampaignLoginRuleForm();
        ruleForm2.setLoginDay((byte) 6);
        ruleForm2.setFixedBonus(new BigDecimal("108.000"));

        List<PromoCampaignLoginRuleForm> ruleForms = new ArrayList<>();
        ruleForms.add(ruleForm);
        ruleForms.add(ruleForm2);

        String requestBody = JsonUtils.objectToJson(ruleForms);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/campaigns/login/1/days/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);
    }

    @Test
    public void testDeleteRequiredLoginDayRule() throws Exception {
        PromoCampaignController.LoginDayBatchDeleteForm form = new PromoCampaignController.LoginDayBatchDeleteForm();
        List<Byte> requriedLoginDays = new ArrayList<>();
        requriedLoginDays.add((byte) 2);
        requriedLoginDays.add((byte) 3);
        form.setLoginDays(requriedLoginDays);
        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(
                post("/promotions/campaigns/login/1/days/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertMultiStatus(resultNode);

        JsonNode node0 = resultNode.get("data").get(0);
        assertDataSuccess(node0);
    }
}
