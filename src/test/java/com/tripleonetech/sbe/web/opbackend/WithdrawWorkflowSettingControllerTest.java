package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSetting;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingForm;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static com.tripleonetech.sbe.withdraw.WithdrawStatusEnum.REVIEW1;
import static com.tripleonetech.sbe.withdraw.WithdrawStatusEnum.REVIEW2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class WithdrawWorkflowSettingControllerTest extends BaseMainTest {

    @Autowired
    private WithdrawWorkflowSettingMapper workflowSettingMapper;

    @Test
    public void testGetWorkflowSettings() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-workflow"));
        assertOk(resultNode);
        assertEquals("WITHDRAW workflow step should be 12", 12, resultNode.get("data").size());
    }

    @Test
    public void testUpdateWorkflowSettings() throws Exception {
        List<WithdrawWorkflowSettingForm> form = new ArrayList<>();

        String name = "Financial checking";
        WithdrawWorkflowSettingForm item1 = new WithdrawWorkflowSettingForm();
        item1.setId((REVIEW1));
        item1.setName(name);
        item1.setEnabled(false);

        WithdrawWorkflowSettingForm item2 = new WithdrawWorkflowSettingForm();
        item2.setId(REVIEW2);
        item2.setEnabled(true);

        form.add(item1);
        form.add(item2);

        String requestBody = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/withdraw-workflow")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf()));
        assertOk(resultNode);

        WithdrawWorkflowSetting review1 = workflowSettingMapper.getWorkflowSettingById(REVIEW1);
        assertEquals("Step REVIEW1 should be re-defined name as Financial checking", name, review1.getName());

        WithdrawWorkflowSetting review2 = workflowSettingMapper.getWorkflowSettingById(REVIEW2);
        assertTrue("Step REVIEW2 should be enabled", review2.getEnabled());
    }
}
