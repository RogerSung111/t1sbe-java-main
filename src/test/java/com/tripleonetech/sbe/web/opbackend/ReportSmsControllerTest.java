package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ReportSmsControllerTest extends BaseMainTest {
    @Test
    public void testList_OnlyRequiredParams() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/sms-otp"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertTrue(node.get("total").asInt() > 0);
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/sms-otp")
                .param("creationTimeFrom", ZonedDateTime.now().minusHours(1).toString())
                .param("creationTimeTo", ZonedDateTime.now().toString()));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue(node.get("list").size() > 0);
        assertTrue(node.get("list").get(0).hasNonNull("currency"));
    }

}
