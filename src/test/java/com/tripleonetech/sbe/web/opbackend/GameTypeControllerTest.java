package com.tripleonetech.sbe.web.opbackend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.game.GameType;
import com.tripleonetech.sbe.game.GameTypeI18n;
import com.tripleonetech.sbe.game.GameTypeI18nMapper;
import com.tripleonetech.sbe.game.GameTypeMapper;

public class GameTypeControllerTest extends BaseMainTest {

    @Autowired
    private GameTypeMapper gameTypeMapper;
    @Autowired
    private GameTypeI18nMapper gameTypeI18nMapper;

    private GameType mockGameType;

    @Before
    public void setup() {
        mockGameType = new GameType();
        mockGameType.setId(1);
        List<GameTypeI18n> gameTypeI18nList = gameTypeI18nMapper.listByGameType(mockGameType.getId());
        mockGameType.setGameTypeI18n(gameTypeI18nList);
        // avoid these dates get converted in JSON and causing error
        gameTypeI18nList.forEach(item -> {
            item.setCreatedAt(null);
            item.setUpdatedAt(null);
        });
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/game-types"));
        assertOk(resultNode);

        assertEquals("There should be 10 game types returned", 10, resultNode.get("data").size());
        JsonNode node = resultNode.get("data").get(0);
        assertEquals(1, node.get("id").asInt());
        assertEquals("Slot Game", node.get("name").asText());
        assertTrue(node.get("gameCount").asInt() > 0);
        assertTrue(node.get("gameApis").size() > 0);
        JsonNode gameApisNode = node.get("gameApis");
        assertTrue("gameApis.size should greater than 0", gameApisNode.size() > 0);
        for(JsonNode gameApi : gameApisNode) {
            assertTrue(gameApi.get("id").asInt() > 0);
            assertNotNull(gameApi.get("code").asText());
            assertNotNull(gameApi.get("name").asText());
        }
    }

    @Test
    public void testGetDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/game-types/" + mockGameType.getId()));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue(node.get("i18nNames").size() > 0);
        assertEquals(mockGameType.getId().intValue(), node.get("id").asInt());
    }

    @Test
    public void testAdd() throws Exception {
        GameTypeI18n i18nModified = mockGameType.getGameTypeI18n().get(0);
        i18nModified.setName("測試遊戲名 for uniTest");

        Map<String, Object> formObj = new HashMap<>();
        formObj.put("i18nNames", mockGameType.getGameTypeI18n());

        JsonNode resultNode = mockMvcPerform(post("/game-types")
                .content(JsonUtils.objectToJson(formObj)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        int newGameTypeId = resultNode.get("data").get("id").asInt();
        assertTrue("new game type ID should be greater than 10", newGameTypeId > 10);
        List<GameTypeI18n> gameTypeI18nList = gameTypeI18nMapper.listByGameType(newGameTypeId);
        GameTypeI18n i18nFiltered = gameTypeI18nList.stream()
                .filter(i18n -> i18n.getLocale().equals(i18nModified.getLocale())).findFirst()
                .orElse(null);
        assertEquals(i18nModified.getName(), i18nFiltered.getName());

        resultNode = mockMvcPerform(get("/game-types"));
        assertOk(resultNode);
        assertEquals("There should be 11 game types in total", 11, resultNode.get("data").size());
    }

    @Test
    public void testUpdate() throws Exception {
        GameTypeI18n i18nModified = mockGameType.getGameTypeI18n().get(0);
        i18nModified.setName("測試遊戲名 for uniTest");

        Map<String, Object> formObj = new HashMap<>();
        formObj.put("id", mockGameType.getId()); // has id, means update
        formObj.put("i18nNames", mockGameType.getGameTypeI18n());

        JsonNode resultNode = mockMvcPerform(
                post("/game-types")
                .content(JsonUtils.objectToJson(formObj))
        );
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<GameTypeI18n> gameTypeI18nList = gameTypeI18nMapper.listByGameType(mockGameType.getId());
        GameTypeI18n i18nFiltered = gameTypeI18nList.stream()
                .filter(i18n -> i18n.getLocale().equals(i18nModified.getLocale())).findFirst()
                .orElse(null);
        assertEquals(i18nModified.getName(), i18nFiltered.getName());
    }

    @Test
    public void testDelete() throws Exception {
        Integer mockGameTypeId = 10;
        GameType gameType = gameTypeMapper.getGameTypeById(mockGameTypeId, null, null);
        assertTrue(gameType.isEnabled());

        JsonNode resultNode = mockMvcPerform(post("/game-types/" + mockGameTypeId + "/delete"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        gameType = gameTypeMapper.getGameTypeById(mockGameTypeId, null, null);
        assertNull(gameType);
    }

    @Test
    public void testFailedDelete() throws Exception {
        Integer mockGameTypeId = 1;

        JsonNode resultNode = mockMvcPerform(post("/game-types/" + mockGameTypeId + "/delete"));
        assertOperationFailed(resultNode);
        assertDataFail(resultNode.get("data"));

        GameType gameType = gameTypeMapper.getGameTypeById(mockGameTypeId, null, null);
        assertTrue(gameType.isEnabled());
    }
}
