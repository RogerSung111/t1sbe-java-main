package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.automation.*;
import com.tripleonetech.sbe.common.JsonUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class AutomationControllerTest extends BaseMainTest {
    @Autowired
    private AutomationJobMapper jobMapper;

    @Test
    public void testGetJob() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/automation/jobs/1"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertEquals(1, data.get("id").asInt());
        assertFalse(data.get("filters").isNull());
        assertTrue(data.get("filters").isArray());
        assertFalse(data.get("actions").isNull());
        assertTrue(data.get("actions").isArray());
        assertTrue(data.get("runCount").isNumber());
        assertTrue(data.get("startAt").asText().contains("Z"));
    }

    @Test
    public void testListJobs() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/automation/jobs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("sort", "updatedAt DESC"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertEquals(4, data.size());
    }

    @Test
    public void testGetPlayers() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/automation/jobs/1/players"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        JsonNode playersNode = data.get("list");
        assertTrue("Should return more than 1 player", playersNode.size() > 1);
        JsonNode playerNode = playersNode.get(0);
        assertEquals(1, playerNode.get("id").asInt());
        assertEquals("CNY", playerNode.get("currency").asText());
        assertEquals("test001", playerNode.get("username").asText());
    }

    @Test
    public void testCreateJob() throws Exception {
        AutomationJobForm form = new AutomationJobForm();
        form.setName("A sample automation job");
        form.setMaxRun(3);
        form.setFiltersJson("[[{\"id\": 2, \"value\": [\"CNY\", 5999], \"operator\": \">\"}, {\"id\": 3, \"value\": [\"CNY\", 100], \"operator\": \"<=\"}]]");
        form.setActionsJson("[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}]");
        form.setStartAt(ZonedDateTime.now());
        form.setRunFrequency((byte) AutomationJobRunFrequencyEnum.EVERY_DAY.getCode());

        JsonNode resultNode = mockMvcPerform(post("/automation/jobs/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        assertTrue(resultNode.get("data").get("id").isInt());
    }

    @Test
    public void testCreateJobFailEmptyFilter() throws Exception {
        AutomationJobForm form = new AutomationJobForm();
        form.setName("A sample automation job");
        form.setMaxRun(3);
        // Empty filters - should return invalid parameter
        //form.setFiltersJson("[[{\"id\": 2, \"value\": [\"CNY\", 5999], \"operator\": \">\"}, {\"id\": 3, \"value\": [\"CNY\", 100], \"operator\": \"<=\"}]]");
        form.setActionsJson("[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}]");
        form.setStartAt(ZonedDateTime.now());
        form.setRunFrequency((byte) AutomationJobRunFrequencyEnum.EVERY_DAY.getCode());

        JsonNode resultNode = mockMvcPerform(post("/automation/jobs/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.objectToJson(form)));

        assertInvalidParameter(resultNode);
    }

    @Test
    public void testCreateJobWrongGroupIdFailed() throws Exception {
        AutomationJobForm form = new AutomationJobForm();
        form.setName("A sample automation job");
        form.setMaxRun(3);
        form.setFiltersJson("[[{\"id\": 0, \"value\": [1,2,6], \"operator\": \">\"}]]");
        form.setActionsJson("[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}]");
        form.setStartAt(ZonedDateTime.now());
        form.setRunFrequency((byte) AutomationJobRunFrequencyEnum.EVERY_DAY.getCode());

        JsonNode resultNode = mockMvcPerform(post("/automation/jobs/create")
                .content(JsonUtils.objectToJson(form)));
        assertInvalidParameter(resultNode);
        assertEquals("Tag id [6] is not a player group id", resultNode.get("data").get("message").asText());
    }

    @Test
    public void testUpdateJob() throws Exception {
        AutomationJobForm form = new AutomationJobForm();
        form.setName("A sample automation job");
        form.setMaxRun(3);
        form.setFiltersJson("[[{\"id\": 2, \"value\": [\"CNY\", 5999], \"operator\": \">\"}, {\"id\": 3, \"value\": [\"CNY\", 100], \"operator\": \"<=\"}]]");
        form.setActionsJson("[{\"id\": 1, \"value\": [43]}, {\"id\": 1, \"value\": [8]}]");
        form.setStartAt(ZonedDateTime.now());
        form.setRunFrequency((byte) AutomationJobRunFrequencyEnum.EVERY_DAY.getCode());

        JsonNode resultNode = mockMvcPerform(post("/automation/jobs/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.objectToJson(form)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        AutomationJob updatedJob = jobMapper.get(1);
        assertEquals(form.getName(), updatedJob.getName());
        assertEquals(form.getActions().size(), updatedJob.getActions().size());
        assertEquals(form.getFilters().size(), updatedJob.getFilters().size());
        assertEquals(form.getStartAt().toEpochSecond(), updatedJob.getStartAt().toEpochSecond());
    }

    @Test
    public void testListRecords() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/automation/jobs/1/records")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("status", "" + AutomationRecordStatusEnum.COMPLETED.getCode())
                        .param("sort", "updatedAt DESC"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        JsonNode recordsNode = data.get("list");
        assertEquals("There should be only 2 records matching the query", 2, recordsNode.size());
        assertEquals("The matching record should have the jobId equals to that of query", 1, recordsNode.get(0).get("jobId").asInt());
        assertFalse(recordsNode.get(0).has("affectedPlayerIds"));
        assertTrue(recordsNode.get(0).has("affectedPlayerCount"));
    }

    @Test
    public void testListRecordsNoParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/automation/jobs/1/records")
        );
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        JsonNode recordsNode = data.get("list");
        assertEquals("Should return the total 3 records", 3, recordsNode.size());
    }

    @Test
    public void testRunJob() throws Exception {
        AutomationJob job = jobMapper.get(4);
        JsonNode resultNode = mockMvcPerform(post("/automation/jobs/{id}/run", job.getId()));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        AutomationJob updatedJob = jobMapper.get(job.getId());
        assertTrue(updatedJob.getRunCount().compareTo(job.getRunCount() + 1) == 0);
    }
}
