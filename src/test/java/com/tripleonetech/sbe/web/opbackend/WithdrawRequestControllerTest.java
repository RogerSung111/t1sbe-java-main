package com.tripleonetech.sbe.web.opbackend;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseMainTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestForm.OperatorWithdrawRequestForm;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class WithdrawRequestControllerTest extends BaseMainTest {
    private final int apiId = 1;
    private final long withdrawRequestId = 100003;
    private final int status = 1;
    private final int PLAYER_ID = 1;

    @Autowired
    private WalletMapper walletMapper;

    public OperatorWithdrawRequestForm initForm() {
        OperatorWithdrawRequestForm form = new OperatorWithdrawRequestForm();
        form.setAmount(BigDecimal.TEN);
        form.setBankBranch("中国工商北京分部");
        form.setBankId(1);
        form.setAccountNumber("4444-3424-2323-2323");
        form.setWithdrawPassword("abcdef");
        form.setAccountHolderName("李晨");
        form.setPlayerId(PLAYER_ID);
        return form;
    }

    @Test
    public void testCreateWithdrawRequest() throws Exception {
        String withdrawalStr = JsonUtils.objectToJson(initForm());
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertOk(result);
        assertTrue(result.get("data").get("id").asInt() > 1);
    }

    @Test
    public void testCreateWithdrawRequestMissingRequiredField() throws Exception {
        OperatorWithdrawRequestForm form = initForm();
        form.setBankId(null);
        String withdrawalStr = JsonUtils.objectToJson(form);
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertInvalidParameter(result);
    }

    @Test
    public void listTestWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/withdraw-requests").contentType(MediaType.APPLICATION_JSON));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue(node.get("list").size() > 0);
    }

    @Test
    public void listTest() throws Exception {
        LocalDate currentDate = LocalDate.now();
        int testStatus = WithdrawStatusEnum.PAID.getCode();

        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests")
                .contentType(MediaType.APPLICATION_JSON)
                .param("username", "test004")
                .param("playerId", "4")
                .param("requestedDateStart", currentDate.minusDays(1).toString() + "T00:00:00+00:00")
                .param("requestedDateEnd", currentDate.toString() + "T23:59:59+00:00")
                .param("currency", "CNY")
                .param("status", "" + testStatus)
                .param("page", "1")
                .param("limit", "10")
                .param("sort", "requestedDate")
                .param("asc", "1"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertNotNull(node.get("total"));
        assertEquals(1, node.get("total").asInt());
        assertNotNull(node.findValue("username"));
        assertEquals("test004", node.get("list").get(0).get("username").asText());
        assertEquals(testStatus, node.get("list").get(0).get("status").asInt());
        assertEquals("CNY", node.get("list").get(0).get("currency").asText());
        assertNotNull(node.get("list").get(0).get("updatedAt").asText());
    }

    @Test
    public void detailTest() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests/" + this.withdrawRequestId));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(this.withdrawRequestId, node.findValue("id").asInt());
        assertNotNull(node.findValue("username"));
        assertNotEquals("null", node.get("updatedAt").asText());
    }

    @Test
    public void updateStatusTest() throws Exception {
        // create corresponding pending withdraw amount

        Map<String, Object> payload = new HashMap<>();
        payload.put("nextStatus", WithdrawStatusEnum.REVIEW2);
        payload.put("comment", "Approval by review2");
        String requestBody = JsonUtils.objectToJson(payload);

        walletMapper.moveMainBalanceToPending(PLAYER_ID, new BigDecimal(100));
        JsonNode resultNode = mockMvcPerform(post("/withdraw-requests/" + this.withdrawRequestId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void updateStatusFailedTest() throws Exception {

        Map<String, Object> payload = new HashMap<>();
        String requestBody = JsonUtils.objectToJson(payload);

        walletMapper.moveMainBalanceToPending(PLAYER_ID, new BigDecimal(100));
        JsonNode resultNode = mockMvcPerform(post("/withdraw-requests/" + this.withdrawRequestId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void submitWithdrawRequestTest() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/withdraw-requests/submit-to-api/" + apiId + "/" + this.withdrawRequestId));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void thirdPartyApiCallbackTest() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                post("/withdraw-requests/callback/" + apiId + "/" + this.withdrawRequestId + "/" + status));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void testGetAvailableWorkflowSettingsOfWithdraw() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/withdraw-requests/" + this.withdrawRequestId + "/workflow"));
        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
    }

    @Test
    public void testGetComments() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests/" + this.withdrawRequestId + "/request-history"));

        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));

        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("comment").asText()));
        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("createdBy").get("name").asText()));
        assertTrue(StringUtils.isNotBlank(resultNode.get("data").get(0).get("updatedBy").get("name").asText()));

    }

    @Test
    public void testGetActionRequiredCount() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests/action-required-count"));

        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
    }
}
