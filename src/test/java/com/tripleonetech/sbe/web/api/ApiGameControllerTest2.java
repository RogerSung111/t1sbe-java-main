package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiGameControllerTest2 extends ApiGameControllerTest {
    @Test
    public void testGameList_OnlyFavorite() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = mockMvcPerform(get(uri)
                .param("gameApiId", "5")
                .param("favorite", "true")
                .param("sort", "released_date"));
        assertOk(resultNode);
    }
}
