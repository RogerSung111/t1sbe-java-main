package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.CryptoUtils;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.player.PlayerNewForm.PlayerRegisterForm;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiPlayerRegisterControllerTest extends BaseApiTest {

    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    
    private PlayerRegisterForm formObj;

    @Before
    public void setUp() {
        formObj = new PlayerRegisterForm();
        formObj.setUsername("test002");
        formObj.setPassword("123456");
        formObj.setCurrency(SiteCurrencyEnum.CNY);
        formObj.setEmail("testemail@tripleonetech.com");
    }

    @Test
    public void testRegister() throws Exception {
        formObj.setUsername("test002-noDuplicate");

        JsonNode resultNode = mockMvcPerformNoLogin(post("/player/register")
                .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36")
                .content(JsonUtils.objectToJson(formObj)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(formObj.getUsername());
        assertEquals(DeviceTypeEnum.COMPUTER, playerCredential.getRegisterDevice());
        List<SitePrivilege> sitePrivileges = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_CURRENCY);
        for (SitePrivilege sitePrivilege : sitePrivileges) {
            SiteCurrencyEnum currency = SiteCurrencyEnum.valueOf(sitePrivilege.getValue());
            Player player = playerMapper.getByUniqueKey(formObj.getUsername(), currency);
            assertEquals(formObj.getUsername(), player.getUsername());
            assertEquals(formObj.getPassword(), CryptoUtils.decryptPlayerPassword(player.getPasswordEncrypt()));
            assertEquals(formObj.getEmail(), player.getEmail());
            assertEquals(player.getCredentialId().intValue(), resultNode.get("data").get("id").asInt());
            if (currency == formObj.getCurrency()) {
                assertNotEquals(PlayerStatusEnum.CURRENCY_INACTIVE, player.getStatus());
            } else {
                assertEquals(PlayerStatusEnum.CURRENCY_INACTIVE, player.getStatus());
            }
        }
    }

    @Test
    public void testRegister_DuplicateUsername_Failed() throws Exception {
        JsonNode resultNode = mockMvcPerformNoLogin(post("/player/register")
                .content(JsonUtils.objectToJson(formObj)));

        assertUsernameAlreadyExist(resultNode);
    }

}
