package com.tripleonetech.sbe.web.api;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.PlayerCredentialMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestForm;

public class ApiWithdrawalControllerTest extends BaseApiTest {

    protected WithdrawRequestForm form;
    
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    
    @Before
    public void init() {
        form = new WithdrawRequestForm();
        form.setAmount(BigDecimal.TEN);
        form.setBankBranch("中国工商北京分部");
        form.setBankId(1);
        form.setAccountNumber("4444-3424-2323-2323");
        form.setWithdrawPassword("abcdef");
        form.setAccountHolderName("李晨");
    }

    @Test
    public void testCreateWithdrawRequest() throws Exception {
        String withdrawalStr = JsonUtils.objectToJson(form);
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertOk(result);
        assertTrue(result.get("data").get("id").asInt() > 1);
        
    }
    
    @Test
    public void testCreateWithdrawRequestMissingRequiredField() throws Exception {
        form.setBankId(null);
        String withdrawalStr = JsonUtils.objectToJson(form);
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertInvalidParameter(result);

    }

    @Test
    public void testCreateWithdrawRequestByUsingWrongWithdrawPassword() throws Exception {
        //Change withdraw password to wrong withdraw password
        form.setWithdrawPassword("654321");
        String withdrawalStr = JsonUtils.objectToJson(form);
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertInvalidPassword(result);
    }
    
    @Test
    public void testCreateWithdrawRequestNoMaintainWithdrawPassword() throws Exception {
        playerCredentialMapper.updateWithdrawPasswordHash(this.getLoginPlayerUsername(), null);
        String withdrawalStr = JsonUtils.objectToJson(form);
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(withdrawalStr));
        assertOperationFailed(result);
    }
    
}
