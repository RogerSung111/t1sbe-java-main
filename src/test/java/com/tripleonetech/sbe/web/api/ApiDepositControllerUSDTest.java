package com.tripleonetech.sbe.web.api;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiDepositControllerUSDTest extends BaseApiTest {
    // This test runs under the context of USD
    @Override
    protected String getLoginPlayerCurrency() {
        return "USD";
    }

    @Test
    public void testListAvailablePayment() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-methods"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertEquals("test002 USD should have 1 manual payment method available (no payment API with USD is enabled)",
                1, data.size());

        JsonNode aPaymentMethod = data.get(0);
        assertFalse(aPaymentMethod.get("id").isNull());
        assertFalse(aPaymentMethod.get("name").isNull());
        assertFalse(aPaymentMethod.get("minDeposit").isNull());
        assertFalse(aPaymentMethod.get("maxDeposit").isNull());
    }
}
