package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiAnnouncementControllerTest2 extends ApiAnnouncementControllerTest {
    @Override
    @Test
    public void testListAvailableAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/announcements"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        int numAnnouncements = node.get("size").asInt();
        assertEquals("Should have exactly 2 announcements visible after login",2, numAnnouncements);
    }

}
