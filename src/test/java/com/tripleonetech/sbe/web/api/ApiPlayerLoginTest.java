package com.tripleonetech.sbe.web.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.Constant;

public class ApiPlayerLoginTest extends BaseApiTest {
    @Autowired
    private Constant constant;

    @Test
    public void testLogin() throws Exception {
        JsonNode resultNode = this.login(this.getLoginPlayerUsername(), "123456");
        assertTrue(resultNode.hasNonNull("access_token"));
    }

    @Test
    public void testLogin_NotExistsUsername_Failed() throws Exception {
        JsonNode resultNode = this.login("test002-notexists", "123456");
        assertEquals("invalid_grant", resultNode.get("error").asText());
        assertEquals("Bad credentials", resultNode.get("error_description").asText());
    }

    @Test
    public void testLogin_WrongPassword_Failed() throws Exception {
        JsonNode resultNode = this.login(this.getLoginPlayerUsername(), "wrongpassword");
        assertEquals("invalid_grant", resultNode.get("error").asText());
        assertEquals("Bad credentials", resultNode.get("error_description").asText());
    }

    private JsonNode login(String username, String password) throws Exception {
        return mockMvcPerformNoLogin(post("/oauth/token")
                .param("username", username)
                .param("password", password)
                .param("currency", "CNY")
                .param("grant_type", "password")
                .param("scope", "read write")
                .param("client_id", constant.getOauthPlayer().getClient())
                .param("client_secret", constant.getOauthPlayer().getSecret()));
    }

    @Override
    protected JsonNode mockMvcPerformNoLogin(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        MvcResult mvcResult = getMockMvc().perform(
                requestBuilder
                        .with(csrf())
                        .header("Accept-Language", "en-US"))
                .andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(result);
    }
}
