package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.CampaignApprovalStatusEnum;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiCampaignControllerTest extends BaseApiTest {

    private String baseUrl = "/campaigns";

    @Test
    @Ignore("TODO: test against new campaign list data structure")
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get(baseUrl + ""));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertEquals(JsonNodeType.ARRAY, node.get("joinedCampaigns").getNodeType());
        assertEquals(1, ((ArrayNode) node.get("joinedCampaigns")).size());
        assertEquals(JsonNodeType.ARRAY, node.get("availableCampaigns").getNodeType());
        assertEquals(4, ((ArrayNode) node.get("availableCampaigns")).size());
    }

    @Test
    @Ignore("TODO: should not return non-approved campaigns")
    public void testGet() throws Exception {
        JsonNode node = mockMvcPerform(
                get(baseUrl + "/deposit/3")
        );
        assertOk(node);

        JsonNode data = node.get("data");
        assertNotNull(data);
        TestCase.assertEquals(CampaignApprovalStatusEnum.PENDING_APPROVAL.getCode(), data.get("status").asInt());
    }

    @Test
    @Ignore("TODO: test against new campaign list data structure")
    public void testAdd() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode array = mapper.createArrayNode();
        ObjectNode node = mapper.createObjectNode();
        node.put("promoType", PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        node.put("campaignId", "1");
        array.add(node);
        String requestBody = mapper.writeValueAsString(array);

        JsonNode resultNode = mockMvcPerform(post(baseUrl
                + "/join")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
        assertOk(resultNode);


        JsonNode confirmNode = mockMvcPerform(get(baseUrl + ""));
        assertOk(confirmNode);

        JsonNode data = confirmNode.get("data");
        assertNotNull(data);
        assertEquals(JsonNodeType.ARRAY, data.get("joinedCampaigns").getNodeType());
        assertEquals(1, ((ArrayNode) data.get("joinedCampaigns")).size());
        assertEquals(JsonNodeType.ARRAY, data.get("availableCampaigns").getNodeType());
        assertEquals(4, ((ArrayNode) data.get("availableCampaigns")).size());
    }

    @Test
    @Ignore("TODO: test against new campaign list data structure")
    public void testJoinAndList() throws Exception {

        JsonNode beforeResultNode = mockMvcPerform(get(baseUrl + ""));
        assertOk(beforeResultNode);

        JsonNode beforeNode = beforeResultNode.get("data");
        assertNotNull(beforeNode);
        assertEquals(JsonNodeType.ARRAY, beforeNode.get("joinedCampaigns").getNodeType());
        assertEquals(1, beforeNode.get("joinedCampaigns").size());
        assertEquals(JsonNodeType.ARRAY, beforeNode.get("availableCampaigns").getNodeType());
        assertEquals(4, beforeNode.get("availableCampaigns").size());

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode array = mapper.createArrayNode();
        ObjectNode node = mapper.createObjectNode();
        node.put("promoType", PromoTypeEnum.CAMPAIGN_DEPOSIT.getCode());
        node.put("campaignId", "2");
        array.add(node);
        String requestBody = mapper.writeValueAsString(array);

        JsonNode resultNode = mockMvcPerform(post(baseUrl
                + "/join")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody));
        assertOk(resultNode);

        JsonNode confirmNode = mockMvcPerform(get(baseUrl + ""));
        assertOk(confirmNode);

        JsonNode data = confirmNode.get("data");
        assertNotNull(data);
        assertEquals(JsonNodeType.ARRAY, data.get("joinedCampaigns").getNodeType());
        assertEquals(beforeNode.get("joinedCampaigns").size() + 1, data.get("joinedCampaigns").size());
        assertEquals(JsonNodeType.ARRAY, data.get("availableCampaigns").getNodeType());
        assertEquals(beforeNode.get("availableCampaigns").size() - 1, data.get("availableCampaigns").size());
    }

}
