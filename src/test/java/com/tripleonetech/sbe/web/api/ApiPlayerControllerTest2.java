package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.player.PlayerBankAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiPlayerControllerTest2 extends ApiPlayerControllerTest {

    @Autowired
    private PlayerBankAccountMapper playerBankAccountMapper;

    @Override
    protected String loginPlayerEmail() {
        return "testemail@tripleonetech.com";
    }

    @Override
    public void testGetPlayerStats() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/stats"));
        assertOk(resultNode);
    }

    @Override
    public void testViewProfile() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/profile"));
        assertOk(resultNode);
    }

    @Override
    public void testGetContactPreferences() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/contact-preferences"));
        assertOk(resultNode);
    }

    @Override
    public void testSwitchWithNewActivePlayer() throws Exception {
        String switchCurrency = "USD";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        assertOk(resultNode); // testnewplayer has USD and status = 1
    }

    @Override
    public void testSwitchCurrencyWithBlockedPlayer() throws Exception {
        String switchCurrency = "THB";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        assertOk(resultNode); // testnewplayer has THB and status = 1
    }

    @Override
    public void testViewIdentity() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/identity"));
        assertOk(resultNode);
    }

    @Override
    public void testSetDefaultPlayerBankAccount() throws Exception {
        int playerBankAccountId = 2; // for testnewplayer (id = 101), there is no bankaccount that belongs to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/set-default"));
        assertPlayerBankAccountNotFound(resultNode);
    }

    @Override
    public void testDeletePlayerBankAccount() throws Exception {
        int playerBankAccountId = 2; // for player test002 (id = 2), get a bank account id that belongs to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/delete"));
        assertPlayerBankAccountNotFound(resultNode);
    }

    // lastLogin is null
    @Override
    public void testGetPlayerInfo() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/info"));
        assertOk(resultNode);
    }

    @Override
    public void testGetPersonalBankAccounts() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/bank-accounts"));
        assertOk(resultNode);
    }

    @Override
    public void testViewReferPlayers() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/refer-players"));
        assertOk(resultNode);
    }


}
