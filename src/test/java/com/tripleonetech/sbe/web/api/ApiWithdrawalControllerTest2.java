package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.common.JsonUtils;

public class ApiWithdrawalControllerTest2 extends ApiWithdrawalControllerTest {

    @Override
    public void testCreateWithdrawRequestByUsingWrongWithdrawPassword() throws Exception {
        form.setWithdrawPassword("654321");
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(JsonUtils.objectToJson(form)));
        assertOperationFailed(result);
    }

    @Override
    public void testCreateWithdrawRequest() throws Exception {
        JsonNode result = mockMvcPerform(post("/withdraw-requests/create").content(JsonUtils.objectToJson(form)));
        assertOperationFailed(result);
    }
}
