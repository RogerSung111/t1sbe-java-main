package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.LocalDate;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;

public class ApiWithdrawRequestControllerTest2 extends ApiWithdrawRequestControllerTest {
    @Override
    public void testList() throws Exception {
        LocalDate currentDate = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests")
                .contentType(MediaType.APPLICATION_JSON)
                .param("requestedDateStart", currentDate.toString() + "T00:00:00+00:00")
                .param("requestedDateEnd", currentDate.toString() + "T23:59:59+00:00")
                .param("status", "" + WithdrawStatusEnum.CREATED.getCode())
                .param("page", "1")
                .param("limit", "10")
                .param("sort", "requestedDate")
                .param("asc", "1"));
        assertOk(resultNode);
    }

    @Override
    public void testListNoParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests")
                .contentType(MediaType.APPLICATION_JSON));
        assertOk(resultNode);
    }

    @Test
    public void testGetPendingRequestAmount() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests/pending-amount"));
        assertOk(resultNode);
    }
}
