package com.tripleonetech.sbe.web.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.game.GameApiService;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame;
import com.tripleonetech.sbe.game.integration.impl.GameApiMockGame2;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.player.wallet.WalletService;

public class ApiWalletControllerTest extends BaseApiTest {

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private GameApiService gameApiService;
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    protected int gameApiId1;
    protected BigDecimal amount;

    private BigDecimal playerMockGameWalletAddAmount;
    private BigDecimal playerMockGameWallet1DeductAmount;
    private static BigDecimal playerMockGameWalletBalance;
    private static BigDecimal playerMockGame2WalletBalance;

    @Before
    public void setUp() throws Exception {
        
        playerMockGameWalletBalance = new BigDecimal("150");
        playerMockGame2WalletBalance = new BigDecimal("130");

        gameApiId1 = 1; // MockGameApi
        amount = new BigDecimal("66");
        playerMockGameWalletAddAmount = playerMockGameWalletBalance.add(amount);
        playerMockGameWallet1DeductAmount = playerMockGameWalletBalance.subtract(amount);

        GameApiMockGame.playerAccount.put("test002", playerMockGameWalletBalance);
        GameApiMockGame2.playerAccount.put("test002", playerMockGame2WalletBalance);
        
    }

    @Test
    public void testList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/wallets"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");

        assertTrue("There should be at least 1 wallet", node.size() > 0);
        assertTrue("The first wallet should always be main wallet",
                node.get(0).get("gameApiId").isNull());
        assertTrue("The first sub-wallet should always be the one with smallest ID",
                !node.get(1).get("gameApiId").isNull());
        assertTrue("The status of first sub-wallet should not be null",
                !node.get(1).get("status").isNull());
        assertFalse("Wallet must contain allowDecimalTransfer property", node.get(1).get("allowDecimalTransfer").isNull());
        // TODO: Fill in unit test for wallet balances
        // JsonNode mainWallet = node.findValue("main");
        // assertEquals("Main wallet should be " + playerMainWalletBalance, playerMainWalletBalance.intValue(),
        // mainWallet.get("balance").intValue());
        //
        // // The sub wallets is origin dirty records in database
        // JsonNode subWallet1 = node.findValue("gameWallets").get(0);
        // assertEquals("Balance of sub wallet 1 should be " + playerMockGameWalletBalance.intValue(),
        // playerMockGameWalletBalance.intValue(), subWallet1.get("balance").intValue());
        // assertEquals("Sub wallet 1 should be dirty", true, subWallet1.get("dirty").booleanValue());
        // JsonNode subWallet2 = node.findValue("gameWallets").get(1);
        // assertEquals("Balance of sub wallet 2 should be " + playerMockGame2WalletBalance.intValue(),
        // playerMockGame2WalletBalance.intValue(), subWallet2.get("balance").intValue());
        // assertEquals("Sub wallet 2 should be dirty", true, subWallet2.get("dirty").booleanValue());
    }

    @Test
    public void testMainToSub() throws Exception {
        Player player = playerMapper.get(getLoginPlayerId());
        Wallet wallet = walletMapper.get(getLoginPlayerId(), gameApiId1);
        Wallet mainWallet = walletMapper.getMainWallet(getLoginPlayerId());
        BigDecimal beforeBalance = mainWallet.getBalance();

        JsonNode result = mockMvcPerform(
                post("/wallets/" + gameApiId1 + "/deposit").param("amount", amount.toString()));
        assertOk(result);

        mainWallet = walletMapper.getMainWallet(getLoginPlayerId());
        BigDecimal afterBalance = mainWallet.getBalance();
        assertEquals("Player balance should be " + beforeBalance.subtract(amount),
                beforeBalance.subtract(amount).floatValue(), afterBalance.floatValue(), 0.01);

        walletService.getCurrentBalance(player, wallet);
        assertEquals("Player's gameApi_1 wallet should be " + playerMockGameWalletAddAmount,
                playerMockGameWalletAddAmount,
                wallet.getBalance());
    }

    @Test
    public void testMainToSubByUsingDisabledGameApi() throws Exception {
        GameApiConfig gameApiConfig = gameApiConfigMapper.get(gameApiId1);
        gameApiConfig.setStatus(ApiConfigStatusEnum.DISABLED);
        gameApiConfigMapper.updateStatus(gameApiConfig);
        
        JsonNode resultNode = mockMvcPerform(
                post("/wallets/" + gameApiId1 + "/deposit").param("amount", amount.toString()));
        this.assertOperationFailed(resultNode);

    }
    
    /**
     * The test is for withdrawing an specific amount from game_api to main_wallet
     */
    @Test
    public void testSubToMain() throws Exception {
        Player player = playerMapper.get(getLoginPlayerId());
        Wallet wallet = walletMapper.get(getLoginPlayerId(), gameApiId1);

        BigDecimal beforeMainBalance = walletMapper.getMainWallet(getLoginPlayerId()).getBalance();
        JsonNode result = mockMvcPerform(
                post("/wallets/" + gameApiId1 + "/withdraw").param("amount", amount.toString()));
        assertOk(result);

        BigDecimal afterMainBalance = walletMapper.getMainWallet(getLoginPlayerId()).getBalance();
        assertEquals("Player balance should be " + beforeMainBalance.add(amount).floatValue(),
                beforeMainBalance.add(amount).floatValue(),
                afterMainBalance.floatValue(), 0.01);
        walletService.getCurrentBalance(player, wallet);
        assertEquals("Player's gameApi 1 should be " + playerMockGameWallet1DeductAmount,
                playerMockGameWallet1DeductAmount,
                wallet.getBalance());
    }

    // added real api wallet to test002(playerId:2), blocked by GameApiT1Test
    @Test
    @Ignore
    public void testSubsAllToMain() throws Exception {
        Player player = playerMapper.get(getLoginPlayerId());
        Wallet wallet = walletMapper.get(getLoginPlayerId(), gameApiId1);

        BigDecimal beforeMainBalance = walletMapper.getMainWallet(getLoginPlayerId()).getBalance();
        JsonNode result = mockMvcPerform(post("/wallets/collect-all-balances"));
        assertOk(result);

        BigDecimal totalBalance = beforeMainBalance.add(playerMockGameWalletBalance).add(playerMockGame2WalletBalance);
        Wallet afterWallet = walletMapper.getMainWallet(getLoginPlayerId());
        assertEquals("Player balance should be " + totalBalance, totalBalance.floatValue(),
                afterWallet.getBalance().floatValue(), 0.001);
        walletService.getCurrentBalance(player, wallet);
        assertEquals("Player's gameApi 1 should be " + BigDecimal.ZERO.floatValue(), BigDecimal.ZERO.floatValue(),
                wallet.getBalance().floatValue(), 0.001);
    }

    /**
     * refresh the game wallet
     */
    @Test
    public void testGetCurrentBalance() throws Exception {
        JsonNode result = mockMvcPerform(get("/wallets/" + gameApiId1));
        assertOk(result);
    }

    /**
     * The test is for depositing all balance to game_api <br>
     */
    @Test
    public void testMainAllToSub() throws Exception {

        BigDecimal beforeBalance = walletMapper.getMainWallet(getLoginPlayerId()).getBalance();
        JsonNode result = mockMvcPerform(post("/wallets/" + gameApiId1 + "/deposit"));
        assertOk(result);

        float afterBalance = walletMapper.getMainWallet(getLoginPlayerId()).getBalance().floatValue();

        float apiBalance = gameApiService.syncBalance(gameApiId1, playerMapper.get(getLoginPlayerId())).getBalance()
                .floatValue();
        assertEquals("Player's main wallet should be" + BigDecimal.ZERO, BigDecimal.ZERO.floatValue(), afterBalance,
                0.001);
        assertEquals("Player's game wallet should be " + beforeBalance.add(playerMockGameWalletBalance).floatValue(),
                beforeBalance.add(playerMockGameWalletBalance).floatValue(), apiBalance, 0.001);
    }

    /**
     * The test is for withdrawing [all amount] from game_api to main_wallet
     */
    @Test
    public void testSubAllToMain() throws Exception {
        BigDecimal beforeMainWallet = walletMapper.getMainWallet(getLoginPlayerId()).getBalance();
        assertTrue("Player's main wallet should NOT be empty", beforeMainWallet.floatValue() > 0.0f);

        JsonNode result = mockMvcPerform(post("/wallets/" + gameApiId1 + "/withdraw"));
        assertOk(result);

        float afterMainWallet = walletMapper.getMainWallet(getLoginPlayerId()).getBalance().floatValue();
        BigDecimal walletAddGame = beforeMainWallet.add(playerMockGameWalletBalance);
        float fWalletAddGame = walletAddGame.floatValue();
        float apiBalance = gameApiService.syncBalance(gameApiId1, playerMapper.get(getLoginPlayerId())).getBalance()
                .floatValue();
        assertEquals("Player's wallet should be " + walletAddGame, fWalletAddGame, afterMainWallet, 0.001);
        assertEquals("Player's game wallet should be " + BigDecimal.ZERO, BigDecimal.ZERO.floatValue(), apiBalance,
                0.001);
    }

}
