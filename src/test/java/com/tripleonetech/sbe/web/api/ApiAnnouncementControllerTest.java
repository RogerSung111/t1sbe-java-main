package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiAnnouncementControllerTest extends BaseApiTest {

    @Test
    public void testListAvailableAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/announcements"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        int numAnnouncements = node.get("size").asInt();
        assertEquals("Should have exactly 3 announcements visible after login", 3, numAnnouncements);
        assertFalse(node.has("content"));
    }

    @Test
    public void testListAvailableAnnouncementWithoutLogin() throws Exception {
        JsonNode resultNode = mockMvcPerformNoLogin(get("/announcements"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        int numAnnouncements = node.get("size").asInt();
        assertEquals("Should have only 2 announcements visible after login", 2, numAnnouncements);
    }

    @Test
    public void testGetAvailableAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/announcements/2"));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals(2, node.get("id").asInt());
        assertNotNull(node.get("content"));
    }

    @Test
    public void testGetUnavailableAnnouncement() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/announcements/1"));
        assertPlayerAnnouncementNotFound(resultNode);
    }
}
