package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.LocalDate;

import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;

public class ApiDepositRequestControllerTest2 extends ApiDepositRequestControllerTest {

    @Override
    public void testListDepositRequests() throws Exception {
        LocalDate currentDate = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(
                get("/payment-requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("requestedDateStart", currentDate.toString() + "T00:00:00Z")
                        .param("requestedDateEnd", currentDate.toString() + "T23:59:59Z")
                        .param("currency", "CNY")
                        .param("paymentApiIds", "1", "2", "3", "10")
                        .param("page", "1")
                        .param("limit", "20")
                        .param("sort", "requested_date"));
        assertOk(resultNode);
    }

    @Override
    public void testListDepositRequestsNoParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(
                get("/payment-requests")
                        .contentType(MediaType.APPLICATION_JSON));
        assertOk(resultNode);
    }
    
    @Override
    public void testGetPendingRequestAmount() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-requests/pending-amount"));
        assertOk(resultNode);
    }
}
