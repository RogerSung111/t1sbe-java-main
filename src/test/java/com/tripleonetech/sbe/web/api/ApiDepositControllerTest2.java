package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.fasterxml.jackson.databind.JsonNode;

public class ApiDepositControllerTest2 extends ApiDepositControllerTest {

    @Override
    public void testListAvailablePayment() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-methods"));
        assertOk(resultNode);
    }

    @Override
    public void testGetDepositRequestDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-requests/100006"));
        assertEntityNotFound(resultNode);
    }
}
