package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiReportControllerTest extends BaseApiTest {
    @Autowired
    private Constant constant;

    @Test
    public void testWalletTransactionList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/transactions")
                .param("createdAtStart", ZonedDateTime.now().minusHours(26).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString())
                .param("type", Integer.toString(WalletTransactionTypeEnum.WITHDRAWAL.getCode())));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        JsonNode firstItem = node.get("list").get(0);
        assertTrue(node.get("total").asInt() > 0);
        assertEquals(2, firstItem.get("playerId").asInt());
        assertEquals(WalletTransactionTypeEnum.WITHDRAWAL.getCode(), firstItem.get("type").asInt());
        assertEquals(NullNode.getInstance(), firstItem.get("gameApiId"));
        assertTrue(firstItem.has("operatorId"));
        assertTrue(firstItem.has("operatorUsername"));
    }

    @Test
    public void testWalletTransactionListWithoutUnnecessaryParameters() throws Exception {
        
        JsonNode resultNode = mockMvcPerform(get("/reports/transactions")
                .param("createdAtStart", ZonedDateTime.now().minusHours(24).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString()));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testGameLogsList() throws Exception {
        Integer mockGameApiId = 9;
        ZonedDateTime mockBetTimeEnd = ZonedDateTime.now().plusHours(-6);
        ZonedDateTime mockBetTimeStart = mockBetTimeEnd.plusDays(-1);

        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs")
                .param("gameApiIds", mockGameApiId.toString())
                .param("betTimeStart", mockBetTimeStart.toString())
                .param("betTimeEnd", mockBetTimeEnd.toString())
                .param("page", "2")
                .param("limit", "10"));
        assertOk(resultNode);

        // list
        assertEquals(2, resultNode.get("data").get("list").get("pageNum").asInt());
        Iterator<JsonNode> list = resultNode.get("data").get("list").get("list").elements();
        while (list.hasNext()) {
            JsonNode data = list.next();

            assertEquals("test002", data.get("playerUsername").asText());
            assertEquals("T1GPT CNY", data.get("gameApiName").asText());
            assertFalse(data.get("gameName").isNull());
            ZonedDateTime betTime = ZonedDateTime.parse(data.get("betTime").asText());
            assertTrue(betTime.compareTo(mockBetTimeStart) >= 0);
            assertTrue(betTime.compareTo(mockBetTimeEnd) <= 0);
        }

        // summary
        Iterator<JsonNode> summary = resultNode.get("data").get("summary").elements();
        while (summary.hasNext()) {
            JsonNode node = summary.next();

            assertFalse(node.get("gameApiName").isNull());
            assertFalse(node.get("currency").isNull());
            assertTrue(node.get("bet").asDouble() > 0);
            assertTrue(node.get("payout").asDouble() > 0);
        }
    }

    @Test
    public void testList_NullDateCondition_Failed() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs")
                .param("page", "1")
                .param("limit", "10"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("betTimeStart"));
        assertTrue(resultNode.get("data").get("message").asText().contains("betTimeEnd"));
        assertTrue(resultNode.get("data").get("message").asText().contains("must not be null"));
    }

    @Test
    public void testList_ExceededLimitDays_Failed() throws Exception {
        ZonedDateTime now = ZonedDateTime.now();

        JsonNode resultNode = mockMvcPerform(get("/reports/game-logs")
                .param("betTimeStart", now.plusDays(-32).toString())
                .param("betTimeEnd", now.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains(
                String.format("Date range should be limited to %d days", constant.getReport().getApiGameLogDayRange())));
    }
}
