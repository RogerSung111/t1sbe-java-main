package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;

public class ApiBonusControllerTest2 extends ApiBonusControllerTest {
    @Override
    public void testListCampaign() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now().plusDays(1);
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-2);

        JsonNode resultNode = mockMvcPerform(get("/bonuses/campaign")
                .param("status", "" + BonusStatusEnum.PENDING_APPROVAL.getCode())
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);
    }

    @Override
    public void testListCashbackWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/cashback"));
        assertOk(resultNode);
    }

    @Override
    public void testListCampaignWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/campaign"));
        assertOk(resultNode);
    }
    
    @Override
    public void testListReferralWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/referral"));
        assertOk(resultNode);
    }
    
    @Override
    public void testListReferral() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.minusDays(7);

        JsonNode resultNode = mockMvcPerform(get("/bonuses/referral")
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);
    }
}
