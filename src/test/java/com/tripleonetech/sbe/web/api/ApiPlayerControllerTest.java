package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;
import com.tripleonetech.sbe.common.web.ChangePasswordForm;
import com.tripleonetech.sbe.email.EmailHelperT1Notification;
import com.tripleonetech.sbe.email.EmailService;
import com.tripleonetech.sbe.email.EmailVerificationHistory;
import com.tripleonetech.sbe.email.EmailVerificationHistoryMapper;
import com.tripleonetech.sbe.payment.Bank;
import com.tripleonetech.sbe.payment.BankMapper;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTask;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskMapper;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskTypeEnum;
import com.tripleonetech.sbe.sms.SmsApiConfigMapper;
import com.tripleonetech.sbe.sms.history.SmsOtpHistory;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryMapper;
import com.tripleonetech.sbe.sms.integration.SmsApiFactory;
import com.tripleonetech.sbe.sms.integration.impl.SmsApiT1Notification;
import com.tripleonetech.sbe.sms.service.SmsService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


public class ApiPlayerControllerTest extends BaseApiTest {
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private BankMapper bankMapper;
    @Autowired
    private PlayerBankAccountMapper playerBankAccountMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PromoCampaignTaskMapper promoCampaignTaskMapper;

    private Bank bank;
    private PromoCampaignTask MOCK_PHONE_CAMPAIGN;

    protected String loginPlayerEmail() {
        return "tripleonetechsg@gmail.com";
    }

    @Before
    public void init() {
        bank = bankMapper.get(1);
        MOCK_PHONE_CAMPAIGN = promoCampaignTaskMapper
                .queryAvailableRule(PromoCampaignTaskTypeEnum.SmsVerificationBonus.getCode(), SiteCurrencyEnum.CNY);
    }

    @Test
    public void testGetPlayerInfo() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/info"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertEquals(getLoginPlayerUsername(), node.get("username").asText());
        assertFalse(node.get("lastLogin").isNull());
        assertFalse(node.get("lastLogin").get("ip").isNull());
        assertFalse(node.get("lastLogin").get("time").isNull());
        assertFalse(node.get("passwordPending").isNull());
        assertFalse(node.get("group").get("id").isNull());
        assertFalse(node.get("group").get("name").isNull());
        assertFalse(node.get("emailVerified").isNull());
        assertFalse(node.get("phoneVerified").isNull());
        assertFalse(node.get("withdrawPasswordExists").isNull());
        assertTrue(node.get("withdrawPasswordExists").asBoolean());
        assertFalse(node.get("unreadMessageCount").isNull());
        assertTrue(node.get("unreadMessageCount").asInt() > 0);
        if (this.getLoginPlayerUsername().equals("test002")) {
            assertTrue(node.get("passwordQuestionExists").asBoolean());
        } else {
            assertFalse(node.get("passwordQuestionExists").asBoolean());
        }
    }

    @Test
    public void testSwitchCurrency() throws Exception {
        String switchCurrency = "USD";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        assertOk(resultNode);
        JsonNode node = resultNode.get("data");
        assertEquals(getLoginPlayerUsername(), node.get("username").asText());
        assertNotEquals(getLoginPlayerId(), node.get("id").asInt());
        assertEquals(switchCurrency, node.get("currency").asText());
    }

    @Test
    public void testSwitchNotAvailableCurrency() throws Exception {
        String switchCurrency = "TWD";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        assertCurrencyNotAvailable(resultNode);
    }

    @Test
    public void testSwitchCurrencyWithBlockedPlayer() throws Exception {
        String switchCurrency = "THB";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        assertPlayerBlocked(resultNode);
    }

    @Test
    public void testSwitchWithNewActivePlayer() throws Exception {
        String switchCurrency = "USD";

        JsonNode resultNode = mockMvcPerform(post("/player/switch-currency")
                .param("currency", switchCurrency));
        JsonNode node = resultNode.get("data");
        assertEquals(getLoginPlayerUsername(), node.get("username").asText());
        assertNotEquals(getLoginPlayerId(), node.get("id").asInt());
        assertEquals(switchCurrency, node.get("currency").asText());
    }

    @Test
    public void testViewReferPlayers() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/refer-players"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertTrue("The test player must have at least one refer player", data.size() > 0);
        assertNotNull(data.get(0).get("id"));
        assertNotNull(data.get(0).get("username"));
    }

    @Test
    public void testViewEmail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/email"));
        assertOk(resultNode);

        assertEquals(loginPlayerEmail(), resultNode.get("data").get("email").asText());
    }

    @Test
    public void testUpdateEmail() throws Exception {
        Map<String, String> mockData = new HashMap<>();
        mockData.put("email", "testemail@tripleonetech.com");

        JsonNode resultNode = mockMvcPerform(post("/player/email").content(JsonUtils.objectToJson(mockData)));
        assertOk(resultNode);

        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(getLoginPlayerUsername());
        assertEquals(mockData.get("email"), playerCredential.getEmail());
    }

    @Test
    public void testUpdateEmail_InvalidPattern_Failed() throws Exception {
        Map<String, String> mockData = new HashMap<>();
        mockData.put("email", "testemailtripleonetech.com");

        JsonNode resultNode = mockMvcPerform(post("/player/email").content(JsonUtils.objectToJson(mockData)));
        assertInvalidParameter(resultNode);
        assertTrue(resultNode.get("data").get("message").asText().contains("must be a well-formed email address"));
    }

    @Test
    public void testViewIdentity() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/identity"));
        assertOk(resultNode);

        JsonNode identityNode = resultNode.get("data");
        assertNotNull(identityNode);

        assertFalse(identityNode.get("currency").isNull());
        assertFalse(identityNode.get("firstName").isNull());
        assertFalse(identityNode.get("lastName").isNull());
        assertFalse(identityNode.get("gender").isNull());
        assertFalse(identityNode.get("birthday").isNull());
        assertFalse(identityNode.get("address").isNull());
        assertFalse(identityNode.get("city").isNull());
        assertFalse(identityNode.get("countryCode").isNull());
        assertFalse(identityNode.get("countryPhoneCode").isNull());
        assertFalse(identityNode.get("phoneNumber").isNull());
    }

    @Test
    public void testEditIdentity() throws Exception {
        PlayerIdentityForm form = new PlayerIdentityForm();
        form.setFirstName("testEditSubmit");
        form.setLastName("testEditSubmit");
        form.setPhoneNumber("testEdit123456");
        form.setAddress("testEditAddress");
        form.setBirthday(LocalDate.of(2020, 1, 1));
        form.setCity("testEditCity");
        form.setCountryCode("CHN");
        form.setCountryPhoneCode("65");
        form.setGender("F");

        String requestBody = JsonUtils.objectToJson(form);
        JsonNode resultNode = mockMvcPerform(post("/player/identity")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
        assertOk(resultNode);

        PlayerProfile updatedIdentity = playerProfileMapper.getByPlayerId(getLoginPlayerId());
        assertEquals(form.getFirstName(), updatedIdentity.getFirstName());
        assertEquals(form.getLastName(), updatedIdentity.getLastName());
        assertEquals(form.getGender(), updatedIdentity.getGender());
        assertEquals(form.getBirthday(), updatedIdentity.getBirthday());
        assertEquals(form.getAddress(), updatedIdentity.getAddress());
        assertEquals(form.getCity(), updatedIdentity.getCity());
        assertEquals(form.getCountryCode(), updatedIdentity.getCountryCode());
        assertEquals(form.getCountryPhoneCode(), updatedIdentity.getCountryPhoneCode());
        assertEquals(form.getPhoneNumber(), updatedIdentity.getPhoneNumber());
    }

    @Test
    public void testViewProfile() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/profile"));
        assertOk(resultNode);

        JsonNode profileNode = resultNode.get("data");
        assertNotNull(profileNode);
        assertFalse(profileNode.get("line").isNull());
        assertFalse(profileNode.get("skype").isNull());
        assertFalse(profileNode.get("qq").isNull());
        assertFalse(profileNode.get("wechat").isNull());
        assertFalse(profileNode.get("currency").isNull());
        assertFalse(profileNode.get("status").isNull());
        assertFalse(profileNode.get("cashbackEnabled").isNull());
        assertTrue(profileNode.get("cashbackEnabled").asBoolean());
        assertFalse(profileNode.get("campaignEnabled").isNull());
        assertTrue(profileNode.get("campaignEnabled").asBoolean());
        assertFalse(profileNode.get("createdAt").isNull());
        assertFalse(profileNode.get("language").isNull());
        assertFalse(profileNode.get("group").isNull());
        assertFalse(profileNode.get("tags").isNull());
        assertTrue(profileNode.get("tags").size() > 1);

        assertFalse(profileNode.get("firstName").isNull());
        assertFalse(profileNode.get("lastName").isNull());
        assertFalse(profileNode.get("gender").isNull());
        assertFalse(profileNode.get("birthday").isNull());
        assertFalse(profileNode.get("address").isNull());
        assertFalse(profileNode.get("city").isNull());
        assertFalse(profileNode.get("countryCode").isNull());
        assertFalse(profileNode.get("countryPhoneCode").isNull());
        assertFalse(profileNode.get("phoneNumber").isNull());
        assertFalse(profileNode.get("referralCode").isNull());

        assertEquals("test001", profileNode.get("referer").get("username").asText());
    }

    @Test
    public void testEditProfile() throws Exception {
        ApiPlayerProfileForm form = new ApiPlayerProfileForm();
        form.setLanguage("en-US");
        form.setQq("1234");
        form.setSkype("testEditSkype");
        form.setLine("testEditLine");
        form.setWechat("testEditWeChat");
        form.setFirstName("testEditSubmit");
        form.setLastName("testEditSubmit");
        form.setPhoneNumber("testEdit123456");
        form.setAddress("testEditAddress");
        form.setBirthday(LocalDate.of(2020, 1, 1));
        form.setLanguage("zh-CN");
        form.setCity("testEditCity");
        form.setCountryCode("CHN");
        form.setCountryPhoneCode("65");
        form.setGender("F");

        String requestBody = JsonUtils.objectToJson(form);
        JsonNode resultNode = mockMvcPerform(post("/player/profile")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
        assertOk(resultNode);

        PlayerProfile updatedProfile = playerProfileMapper.getByPlayerId(getLoginPlayerId());
        assertEquals(form.getLanguage(), updatedProfile.getLanguage());
        assertEquals(form.getLine(), updatedProfile.getLine());
        assertEquals(form.getSkype(), updatedProfile.getSkype());
        assertEquals(form.getQq(), updatedProfile.getQq());
        assertEquals(form.getWechat(), updatedProfile.getWechat());
        assertEquals(form.getFirstName(), updatedProfile.getFirstName());
        assertEquals(form.getLastName(), updatedProfile.getLastName());
        assertEquals(form.getPhoneNumber(), updatedProfile.getPhoneNumber());
        assertEquals(form.getAddress(), updatedProfile.getAddress());
        assertEquals(form.getBirthday(), updatedProfile.getBirthday());
        assertEquals(form.getLanguage(), updatedProfile.getLanguage());
        assertEquals(form.getCity(), updatedProfile.getCity());
        assertEquals(form.getCountryCode(), updatedProfile.getCountryCode());
        assertEquals(form.getCountryPhoneCode(), updatedProfile.getCountryPhoneCode());
        assertEquals(form.getGender(), updatedProfile.getGender());
    }

    @Test
    public void testGetPersonalBankAccounts() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/bank-accounts"));
        assertOk(resultNode);
        assertTrue("The test player must have at least one bank account", resultNode.get("data").size() > 0);

        assertNotNull(resultNode.get("data").get(0).get("id"));
        assertNotNull(resultNode.get("data").get(0).get("accountHolderName"));

    }

    @Test
    public void testCreatePlayerBankAccount() throws Exception {
        PlayerBankAccount playerBankAccount = new PlayerBankAccount();
        playerBankAccount.setPlayerId(1);
        playerBankAccount.setBankId(bank.getId());
        playerBankAccount.setAccountNumber("1111-2222-3333-4444");
        playerBankAccount.setBankBranch("Manila branch");
        playerBankAccount.setAccountHolderName("test002");
        playerBankAccount.setDefaultAccount(true);

        String requestBody = JsonUtils.objectToJson(playerBankAccount);
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        assertFalse(resultNode.get("data").get("id").isNull());

        PlayerBankAccount newPlayerBankAccount = playerBankAccountMapper.get(resultNode.get("data").get("id").asInt());
        assertEquals("test002", newPlayerBankAccount.getAccountHolderName());

    }

    @Test
    public void testCreateDuplicatedPlayerBankAccount() throws Exception {
        PlayerBankAccount playerBankAccount = new PlayerBankAccount();
        playerBankAccount.setPlayerId(1);
        playerBankAccount.setBankId(bank.getId());
        playerBankAccount.setAccountNumber("3333111122223333");
        playerBankAccount.setBankBranch("Manila branch");
        playerBankAccount.setAccountHolderName("test002");
        playerBankAccount.setDefaultAccount(true);

        String requestBody = JsonUtils.objectToJson(playerBankAccount);
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
        assertFalse(resultNode.get("data").get("id").isNull());

        PlayerBankAccount newPlayerBankAccount = playerBankAccountMapper.get(resultNode.get("data").get("id").asInt());
        assertEquals("test002", newPlayerBankAccount.getAccountHolderName());

    }

    @Test
    public void testEditPlayerBankAccount() throws Exception {
        String defaultAccount = "1111-1111-1111-1111";
        String newAccountHolderName = "test test";
        PlayerBankAccount playerBankAccount = playerBankAccountMapper.get(1);
        playerBankAccount.setDefaultAccount(true);
        playerBankAccount.setAccountNumber(defaultAccount);
        playerBankAccount.setAccountHolderName(newAccountHolderName);
        String requestBody = JsonUtils.objectToJson(playerBankAccount);

        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        PlayerBankAccount updatedPlayerBankAccount = playerBankAccountMapper.get(1);
        assertEquals(updatedPlayerBankAccount.getAccountNumber(), defaultAccount);
        assertTrue(updatedPlayerBankAccount.isDefaultAccount());
        assertEquals(newAccountHolderName, updatedPlayerBankAccount.getAccountHolderName());
    }

    @Test
    public void testSetDefaultPlayerBankAccount() throws Exception {
        int playerBankAccountId = 2; // for player test002 (id = 2), get a bank account id that belongs to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/set-default"));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        PlayerBankAccount updatedPlayerBankAccount = playerBankAccountMapper.get(playerBankAccountId);
        assertTrue(updatedPlayerBankAccount.isDefaultAccount());
    }

    @Test
    public void testSetDefaultOtherPlayerBankAccount() throws Exception {
        int playerBankAccountId = 1; // for player test002 (id = 2), get a bank account id that belongs to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/set-default"));
        assertPlayerBankAccountNotFound(resultNode);
    }

    @Test
    public void testDeletePlayerBankAccount() throws Exception {
        int playerBankAccountId = 2; // for player test002 (id = 2), get a bank account id that belongs to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/delete"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void testDeleteOtherPlayerBankAccount() throws Exception {
        int playerBankAccountId = 1; // for player test002 (id = 2), get a bank account id that does not belong to him
        JsonNode resultNode = mockMvcPerform(post("/player/bank-accounts/" + playerBankAccountId + "/delete"));
        assertPlayerBankAccountNotFound(resultNode);
    }

    @Test
    public void testUpdatePassword() throws Exception {
        playerCredentialMapper.updatePasswordPending(getLoginPlayerUsername(), true);

        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword("123456");
        form.setNewPassword("new-pass");
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/player/password")
                .content(requestBodyStr));

        assertOk(resultNode);

        Player player = playerMapper.get(getLoginPlayerId());
        assertTrue(player.validatePassword(form.getNewPassword()));
        assertFalse(player.getPasswordPending());

    }

    @Test
    public void testUpdateWithdrawPassword() throws Exception {

        ApiPlayerController.ChangeWithdrawPasswordForm form = new ApiPlayerController.ChangeWithdrawPasswordForm();
        form.setLoginPassword("123456");
        form.setNewWithdrawPassword("654321");
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/player/withdraw-password")
                .content(requestBodyStr));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Test
    public void testUpdatePassword_WrongOriginalPassword_Failed() throws Exception {
        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword("123456-wrong");
        form.setNewPassword("new-pass");
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/player/password")
                .content(requestBodyStr));

        assertInvalidPassword(resultNode);
    }

    @Autowired
    private SmsService smsService;
    @Autowired
    private SmsApiT1Notification smsApi;
    @Autowired
    private SmsApiConfigMapper smsApiConfigMapper;
    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;
    @Autowired
    private EmailService emailService;
    @Autowired
    private EmailHelperT1Notification emailHelperT1Notification;
    @Autowired
    private EmailVerificationHistoryMapper emailVerificationHistoryMapper;
    @Autowired
    private T1NotificationApi notificationApi;
    private String MOCK_PHONE = "886-917272945";
    private String MOCK_EMAIL = "test.apiplayer@gmail.com";
    private static String mockSuccessResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
    }

    private void initMockApi() {
        // for send sms
        smsApi.setConfig(smsApiConfigMapper.get(3));
        SmsApiFactory apiFactory = Mockito.mock(SmsApiFactory.class);
        smsService.setApiFactory(apiFactory);
        when(apiFactory.getEnabledSmsApi()).thenReturn(smsApi);

        // for send email
        emailService.setEmailHelper(emailHelperT1Notification);
    }

    @Test
    public void testSendOtp() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));

        Map<String, Object> params = new HashMap<>();
        params.put("phoneNumber", MOCK_PHONE);
        JsonNode resultNode = mockMvcPerform(post("/player/phone-verification/send").content(JsonUtils.objectToJson(params)));
        assertDataSuccess(resultNode.get("data"));

        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(getLoginPlayerId(), MOCK_PHONE);
        assertEquals(this.getLoginPlayerId(), smsOtpHistory.getPlayerId().intValue());
        assertFalse(smsOtpHistory.isVerified());
        assertTrue(smsOtpHistory.isDelivered());
        assertTrue(smsOtpHistory.getExpirationTime().isAfter(LocalDateTime.now()));
    }

    @Test
    public void testSendOtp_InvalidPhone_Failed() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));

        Map<String, Object> params = new HashMap<>();
        params.put("phoneNumber", "1234567890");
        JsonNode resultNode = mockMvcPerform(post("/player/phone-verification/send").content(JsonUtils.objectToJson(params)));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testValidateOtp() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        Map<String, Object> params = new HashMap<>();
        params.put("phoneNumber", MOCK_PHONE);
        mockMvcPerform(post("/player/phone-verification/send").content(JsonUtils.objectToJson(params)));

        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(getLoginPlayerId(), MOCK_PHONE);
        params.put("otpCode", smsOtpHistory.getOtpCode());
        JsonNode resultNode = mockMvcPerform(post("/player/phone-verification").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(getLoginPlayerId());
        assertTrue(playerProfile.isPhoneVerified());
    }

    @Test
    @Ignore("This unit test is no longer valid as task campaign is an async event now")
    public void testFailJoinPhoneCampaignOneTimeConstraint() throws Exception {
//        insertMockOtp();
        MOCK_PHONE_CAMPAIGN.setFixedBonus(BigDecimal.valueOf(38));

        promoCampaignTaskMapper.update(MOCK_PHONE_CAMPAIGN);
        JsonNode resultNode = mockMvcPerform(post("/player/phone-verification")
                .param("otpCode", "332211")
                .param("phoneNumber", MOCK_PHONE));
        assertOk(resultNode);

        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(getLoginPlayerId());

        PromoBonus expectedRecordQuery = new PromoBonus(getLoginPlayerId(), MOCK_PHONE_CAMPAIGN.getId(),
                PromoTypeEnum.CAMPAIGN_TASK, null, null, null, null);
        List<PromoBonus> record = promoBonusMapper.query(expectedRecordQuery);

        assertTrue(playerProfile.isPhoneVerified());

        assertEquals(record.size(), 1);
        assertNotEquals(record.get(0).getBonusAmount(), MOCK_PHONE_CAMPAIGN.getFixedBonus());
        assertNull(record.get(0).getReferenceAmount());
    }

    @Test
    public void testFailedToVerifyAndSavePhone() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        Map<String, Object> params = new HashMap<>();
        params.put("phoneNumber", "MOCK_PHONE");
        mockMvcPerform(post("/player/phone-verification/send").content(JsonUtils.objectToJson(params)));

        params.put("otpCode", "123456");
        JsonNode resultNode = mockMvcPerform(post("/player/phone-verification").content(JsonUtils.objectToJson(params)));
        this.assertOperationFailed(resultNode);
    }

    @Test
    @Ignore("Ignoring as this requires external service")
    public void testSendVerificationEmail() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        Map<String, Object> params = new HashMap<>();
        params.put("email", MOCK_EMAIL);

        JsonNode resultNode = mockMvcPerform(post("/player/email-verification/send").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(getLoginPlayerId(), MOCK_EMAIL);
        assertEquals(getLoginPlayerId(), emailVerificationHistory.getPlayerId().intValue());
        assertEquals(MOCK_EMAIL, emailVerificationHistory.getEmail());
        assertFalse(emailVerificationHistory.getVerified());
        assertTrue(emailVerificationHistory.getExpirationTime().isAfter(LocalDateTime.now()));
    }

    @Test
    public void testVerifyAndSaveEmail() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        Map<String, Object> params = new HashMap<>();
        params.put("email", MOCK_EMAIL);
        mockMvcPerform(post("/player/email-verification/send").content(JsonUtils.objectToJson(params)));

        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper
                .getByPlayerIdAndEmail(getLoginPlayerId(), MOCK_EMAIL);
        params.put("otpCode", emailVerificationHistory.getOtpCode());
        JsonNode resultNode = mockMvcPerform(post("/player/email-verification").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        PlayerCredential playerCredential = playerCredentialMapper.getByPlayerId(getLoginPlayerId());
        assertTrue(playerCredential.isEmailVerified());
    }

    @Test
    public void testFailedVerifyAndSaveEmail() throws Exception {
        this.initMockApi();
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        Map<String, Object> params = new HashMap<>();
        params.put("email", MOCK_EMAIL);
        mockMvcPerform(post("/player/email-verification/send").content(JsonUtils.objectToJson(params)));

        params.put("otpCode", "123456");
        JsonNode resultNode = mockMvcPerform(post("/player/email-verification").content(JsonUtils.objectToJson(params)));
        this.assertOperationFailed(resultNode);
    }

    @Test
    public void testGetPlayerStats() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/stats"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertTrue(data.hasNonNull("totalDepositCount"));
        assertTrue(data.hasNonNull("totalDepositAmount"));
        assertTrue(data.hasNonNull("firstDepositDate"));
        assertTrue(data.hasNonNull("lastDepositDate"));
        assertTrue(data.hasNonNull("totalWithdrawCount"));
        assertTrue(data.hasNonNull("totalWithdrawAmount"));
        assertTrue(data.hasNonNull("firstWithdrawDate"));
        assertTrue(data.hasNonNull("lastWithdrawDate"));
        assertTrue(data.hasNonNull("totalCashbackBonus"));
        assertTrue(data.hasNonNull("totalReferralBonus"));
        assertTrue(data.hasNonNull("totalCampaignBonus"));
    }

    @Test
    public void testListVerificationQuestions() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/verification-questions"));
        assertOk(resultNode);

        assertEquals(5, resultNode.get("data").size());
        assertTrue(resultNode.get("data").get(0).hasNonNull("questionId"));
        assertTrue(resultNode.get("data").get(0).hasNonNull("question"));
    }

    @Test
    public void testUpdateVerificationQuestion() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("verificationQuestionId", 1);
        params.put("verificationAnswer", "都好吃");

        JsonNode resultNode = mockMvcPerform(post("/player/verification-question")
                .content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(getLoginPlayerUsername());
        assertEquals(params.get("verificationQuestionId"), playerCredential.getVerificationQuestionId());
        assertEquals(params.get("verificationAnswer"), playerCredential.getVerificationAnswer());
    }

    @Test
    public void testUpdateVerificationQuestion_SizeLess3_Failed() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("verificationQuestionId", 1);
        params.put("verificationAnswer", "都好");

        JsonNode resultNode = mockMvcPerform(post("/player/verification-question")
                .content(JsonUtils.objectToJson(params)));
        assertInvalidParameter(resultNode);
    }

    @Test
    public void testGetVerificationQuestion() throws Exception {
        JsonNode resultNode = mockMvcPerformNoLogin(get("/player/verification-question")
                .param("username", "test002")
                .param("currency", "CNY"));
        assertOk(resultNode);

        assertEquals("What is your favorite food?", resultNode.get("data").get("question").asText());
    }

    @Test
    public void testForgetPassword() throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("username", "test002");
        params.put("verificationAnswer", "testVerificationAnswer");
        params.put("newPassword", "1234567890");

        JsonNode resultNode = mockMvcPerformNoLogin(post("/player/forget-password")
                .content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        Player updatedPlayer = playerMapper.get(2);
        assertTrue(updatedPlayer.validatePassword(params.get("newPassword")));
        assertFalse(updatedPlayer.getPasswordPending());
    }

    @Test
    public void testForgetPassword_IncorrectAnswer_Failed() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("username", "test002");
        params.put("verificationAnswer", "都好吃a");
        params.put("newPassword", "1234567890");

        JsonNode resultNode = mockMvcPerformNoLogin(post("/player/forget-password")
                .content(JsonUtils.objectToJson(params)));
        assertOperationFailed(resultNode);
        assertDataFail(resultNode.get("data"));
    }

    @Test
    public void testGetContactPreferences() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/contact-preferences"));
        assertOk(resultNode);

        assertEquals(2, resultNode.get("data").get("contactPreferenceList").size());
        assertEquals(1, resultNode.get("data").get("contactPreferenceList").get(0).asInt());
        assertEquals(2, resultNode.get("data").get("contactPreferenceList").get(1).asInt());
    }

    @Test
    public void testUpdateContactPreferences() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("contactPreferenceList", Arrays.asList(1, 4));

        JsonNode resultNode = mockMvcPerform(post("/player/contact-preferences")
                .content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        PlayerProfile updatedProfile = playerProfileMapper.getByPlayerId(getLoginPlayerId());
        assertEquals(5, updatedProfile.getContactPreferences().intValue());
    }

    @Test
    public void testUpdateContactPreferences_EmptyValues() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("contactPreferenceList", null);

        JsonNode resultNode = mockMvcPerform(post("/player/contact-preferences")
                .content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        PlayerProfile updatedProfile = playerProfileMapper.getByPlayerId(getLoginPlayerId());
        assertEquals(0, updatedProfile.getContactPreferences().intValue());
    }

    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private Constant constant;

    @Test
    public void testRevokeOtherTokens() throws Exception {
        obtainAccessToken();
        obtainAccessToken();
        Collection<OAuth2AccessToken> tokens = tokenStore
                .findTokensByClientIdAndUserName(constant.getOauthPlayer().getClient(), getLoginPlayerUsername());
        assertEquals(2, tokens.size());
        JsonNode resultNode = mockMvcPerform(get("/player/clean-token"));
        assertOk(resultNode);

        tokens = tokenStore
                .findTokensByClientIdAndUserName(constant.getOauthPlayer().getClient(), getLoginPlayerUsername());
        assertEquals(1, tokens.size());
    }
}
