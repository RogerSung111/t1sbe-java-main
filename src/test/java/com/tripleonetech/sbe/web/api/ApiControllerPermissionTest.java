package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.web.ChangePasswordForm;
import com.tripleonetech.sbe.player.PlayerCredentialMapper;
import com.tripleonetech.sbe.player.PlayerProfileForm;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiControllerPermissionTest extends BaseApiTest {
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;

    @Test
    public void testGetPlayerInfo_NotPasswordPendding_Allow() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/player/info"));
        assertOk(resultNode);
    }

    @Test
    public void testGetPlayerInfo_PasswordPendding_Deny() throws Exception {
        playerCredentialMapper.updatePasswordPending(getLoginPlayerUsername(), true);

        JsonNode resultNode = mockMvcPerform(get("/player/profile"));
        // Query should not succeed as default player username 'test002' was be updated status to PasswordPendding
        assertAccessForbidden(resultNode);
    }

    @Test
    public void testUpdatePassword_PasswordPending_Allow() throws Exception {
        playerCredentialMapper.updatePasswordPending(getLoginPlayerUsername(), true);

        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword("123456");
        form.setNewPassword("new-pass");
        String requestBodyStr = JsonUtils.objectToJson(form);

        JsonNode resultNode = mockMvcPerform(post("/player/password")
                .content(requestBodyStr));

        assertOk(resultNode);
    }

    @Test
    public void testViewProfile_PasswordPending_Allow() throws Exception {
        playerCredentialMapper.updatePasswordPending(getLoginPlayerUsername(), true);

        JsonNode resultNode = mockMvcPerform(get("/player/info"));
        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
    }

    @Test
    public void testEditProfile_PasswordPendding_Deny() throws Exception {
        playerCredentialMapper.updatePasswordPending(getLoginPlayerUsername(), true);

        PlayerProfileForm form = new PlayerProfileForm();
        form.setQq("testEditSubmit");
        JsonNode resultNode = mockMvcPerform(post("/player/profile")
                .content(JsonUtils.objectToJson(form)));
        assertAccessForbidden(resultNode);
    }
}
