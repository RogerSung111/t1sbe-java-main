package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.player.PlayerFavoriteGame;
import com.tripleonetech.sbe.player.PlayerFavoriteGameMapper;
import com.tripleonetech.sbe.web.api.ApiGameController.FavoriteGameForm;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiGameControllerTest extends BaseApiTest {

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    @Autowired
    private PlayerFavoriteGameMapper playerFavoriteGameMapper;
    private int gameApiId;
    private String gameCode;
    private int playerId;

    @Before
    public void setup() {
        gameApiId = 1;
        gameCode = "FD1";
        playerId = getLoginPlayerId();
    }

    @Test
    public void testGameList() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = mockMvcPerform(get(uri)
                .param("gameTypeId", "2")
                .param("web", "1")
                .param("gameApiId", "5")
                .param("sort", "gameNameId"));
        assertOk(resultNode);

        int rowCount = resultNode.get("data").get("total").asInt();
        assertEquals(14, rowCount);

        // verify favorite game
        List<String> favoriteGameCodes = Arrays.asList("13", "220", "AV01", "BJ", "DT", "DTA0", "DTAB", "DTAF");
        resultNode.get("data").get("list").forEach(node -> {
            if (this.getLoginPlayerId() == 2 && favoriteGameCodes.contains(node.get("gameCode").asText())) {
                assertTrue(node.get("favorite").asBoolean());
            } else {
                assertFalse(node.get("favorite").asBoolean());
            }
        });
        assertEquals("http://FanTan/img/url", resultNode.get("data").get("list").get(0).get("gameImgUrl").textValue());
    }

    @Test
    public void testGameList_OnlyFavorite() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = mockMvcPerform(get(uri)
                .param("gameApiId", "5")
                .param("favorite", "true"));
        assertOk(resultNode);

        resultNode.get("data").get("list").forEach(node -> {
            assertTrue(node.get("favorite").asBoolean());
        });
    }

    @Test
    public void testGameListAndFuzzySearchByUsingGameName() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = mockMvcPerform(get(uri)
                .param("gameName", "Legend of"));
        assertOk(resultNode);

        int rowCount = resultNode.get("data").get("total").asInt();
        assertEquals(5, rowCount);
    }

    @Test
    public void testGameList_NoLogin() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = this.mockMvcPerformNoLogin(get(uri)
                .param("gameTypeId", "2")
                .param("web", "1")
                .param("gameApiId", "5")
                .param("sort", "gameCode"));
        assertOk(resultNode);

        assertTrue(resultNode.get("data").get("total").asInt() > 0);
        // verify favorite game
        resultNode.get("data").get("list").forEach(node -> {
            assertFalse(node.get("favorite").asBoolean());
        });
    }

    @Test
    public void testGameListWithoutParam() throws Exception {
        String uri = "/games/CNY";
        JsonNode resultNode = mockMvcPerform(get(uri));
        assertOk(resultNode);

        assertTrue(resultNode.get("data").get("list").size() > 0);
    }

    @Test
    public void testLaunchGame() throws Exception {
        String uri = "/launch-game?gameApiId=" + gameApiId + "&gameCode=" + gameCode;
        JsonNode resultNode = mockMvcPerform(get(uri));
        String gameUrl = gameApiConfigMapper.get(gameApiId).get(MetaFieldTypeEnum.URL.toString());
        String url = resultNode.get("data").asText();
        assertThat(url, both(startsWith(gameUrl)).and(endsWith("/" + gameCode + "/" + playerId)));
    }

    @Test
    public void testAddFavoriteGame() throws Exception {
        FavoriteGameForm params = new FavoriteGameForm();
        params.setGameApiId(5);
        params.setGameCode("BO2P");
        JsonNode resultNode = mockMvcPerform(post("/favorite-game").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        List<PlayerFavoriteGame> list = playerFavoriteGameMapper.listByPlayer(getLoginPlayerId());
        assertTrue(list.stream().anyMatch(
                e -> (params.getGameApiId().equals(e.getGameApiId()) && params.getGameCode().equals(e.getGameCode()))));
    }

    @Test
    public void testRemoveFavoriteGame() throws Exception {
        FavoriteGameForm params = new FavoriteGameForm();
        params.setGameApiId(5);
        params.setGameCode("AGIN");
        JsonNode resultNode = mockMvcPerform(post("/favorite-game/delete").content(JsonUtils.objectToJson(params)));
        assertOk(resultNode);

        List<PlayerFavoriteGame> list = playerFavoriteGameMapper.listByPlayer(getLoginPlayerId());
        assertFalse(list.stream().anyMatch(
                e -> (params.getGameApiId().equals(e.getGameApiId()) && params.getGameCode().equals(e.getGameCode()))));
    }
}
