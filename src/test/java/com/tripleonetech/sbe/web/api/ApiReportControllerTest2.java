package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;

public class ApiReportControllerTest2 extends ApiReportControllerTest {

    @Override
    public void testWalletTransactionListWithoutUnnecessaryParameters() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/transactions")
                .param("createdAtStart", ZonedDateTime.now().minusHours(24).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString()));
        assertOk(resultNode);
    }

    @Override
    public void testWalletTransactionList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/reports/transactions")
                .param("createdAtStart", ZonedDateTime.now().minusHours(24).toString())
                .param("createdAtEnd", ZonedDateTime.now().toString())
                .param("type", Integer.toString(WalletTransactionTypeEnum.WITHDRAWAL.getCode())));
        assertOk(resultNode);
    }

}
