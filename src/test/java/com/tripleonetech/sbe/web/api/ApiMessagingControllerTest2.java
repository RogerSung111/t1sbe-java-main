package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.message.PlayerMessageMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiMessagingControllerTest2 extends ApiMessagingControllerTest {

    @Autowired
    private PlayerMessageMapper playerMessageMapper;

    @Override
    public void testListPlayerMessage() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages"));
        assertOk(resultNode);
    }

    @Override
    public void testListPlayerMessageWithParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages")
                .param("createdAtEnd", ZonedDateTime.now().toString())
                .param("createdAtStart", ZonedDateTime.now().minusMonths(2).toString())
                .param("threadId", "2"));
        assertOk(resultNode);
    }

    @Override
    public void testSendMessage() throws Exception {
        ApiMessagingController.MessageForm message = new ApiMessagingController.MessageForm();
        message.setSubject("Unit test subject");
        message.setContent("Unit test message");
        JsonNode resultNode = mockMvcPerform(post("/messages/send")
                .content(JsonUtils.objectToJson(message)));
        assertOk(resultNode);
    }

    @Override
    public void testSetSystemMessageRead() throws Exception {
        JsonNode resultNode = mockMvcPerform(post("/messages/system-read"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));
    }

    @Override
    public void testSetMessageRead() throws Exception {
        Integer messageId = 2;
        JsonNode resultNode = mockMvcPerform(post("/messages/{id}/read", messageId));
        assertOk(resultNode);
    }

    @Test
    @Override
    public void testSetMessageBatchRead() throws Exception {
        int unreadCount = playerMessageMapper.getUnreadCount(getLoginPlayerId());
        assertEquals(0, unreadCount);

        JsonNode resultNode = mockMvcPerform(post("/messages/all-read"));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        unreadCount = playerMessageMapper.getUnreadCount(getLoginPlayerId());
        assertEquals(0, unreadCount);
    }
}
