package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiWithdrawConditionControllerTest2 extends ApiWithdrawConditionControllerTest {

    @Override
    public void testWithdrawConditionList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-conditions"));
        assertOk(resultNode);
    }

    @Test
    public void testWithdrawConditionBalance() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-conditions/balance"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertEquals("There should be 2 properties returned", 2, node.size());
    }
}
