package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiWithdrawConditionControllerTest extends BaseApiTest {
    @Test
    public void testWithdrawConditionList() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-conditions"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertEquals("There should be 2 withdraw conditions returned", 2, node.size());
    }

    @Test
    public void testWithdrawConditionBalance() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-conditions/balance"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertEquals("There should be 2 properties returned", 2, node.size());

        JsonNode availableNode = node.get("available");
        assertNotNull(availableNode);
        assertTrue("Expecting non-zero available balance", availableNode.asDouble() > 0);
        JsonNode lockedNode = node.get("lockedOn");
        assertNotNull(lockedNode);
        assertTrue("Expecting non-zero locked on balance", lockedNode.asDouble() > 0);
    }
}
