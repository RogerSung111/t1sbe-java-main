package com.tripleonetech.sbe.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.JsonNode;

public class ApiWalletControllerTest2 extends ApiWalletControllerTest {

    @Override
    public void testSubAllToMain() throws Exception {
        JsonNode result = mockMvcPerform(post("/wallets/" + gameApiId1 + "/withdraw"));
        assertBadWalletRequest(result);
    }

    @Override
    public void testSubToMain() throws Exception {
        JsonNode result = mockMvcPerform(
                post("/wallets/" + gameApiId1 + "/withdraw").param("amount", amount.toString()));
        assertBadWalletRequest(result);
    }

    @Override
    public void testMainToSub() throws Exception {
        JsonNode result = mockMvcPerform(
                post("/wallets/" + gameApiId1 + "/deposit").param("amount", amount.toString()));
        assertBadWalletRequest(result);
    }

    @Override
    public void testMainAllToSub() throws Exception {
        JsonNode result = mockMvcPerform(post("/wallets/" + gameApiId1 + "/deposit"));
        assertOk(result);
    }
}
