package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiWithdrawRequestControllerTest extends BaseApiTest {
    @Test
    public void testList() throws Exception {
        LocalDate currentDate = LocalDate.now().minusDays(2);
        int testStatus = WithdrawStatusEnum.PAID.getCode();

        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests")
                .contentType(MediaType.APPLICATION_JSON)
                .param("requestedDateStart", currentDate.toString() + "T00:00:00+00:00")
                .param("requestedDateEnd", currentDate.toString() + "T23:59:59+00:00")
                .param("status", "" + testStatus)
                .param("page", "1")
                .param("limit", "10")
                .param("sort", "requestedDate")
                .param("asc", "1"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals("There should be 5 withdraw requests for [" + getLoginPlayerUsername() + "] for date [" + currentDate + "] and status is PAID",
                5, node.get("total").asInt());

        node.get("list").forEach(withdrawRequestNode -> {
            assertEquals("Result must be under current login player", getLoginPlayerUsername(), withdrawRequestNode.get("username").asText());
            assertEquals("Result must be the expected status", testStatus, withdrawRequestNode.get("status").asInt());
            assertEquals("Result must be the current player's currency", "CNY", withdrawRequestNode.get("currency").asText());
            assertTrue("Result must all be of the current date",
                    ZonedDateTime.parse(withdrawRequestNode.get("requestedDate").asText()).toLocalDate().isEqual(currentDate));
        });
    }

    @Test
    public void testListNoParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests")
                .contentType(MediaType.APPLICATION_JSON));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node.get("list"));
        assertNotNull(node.get("total"));
        assertTrue(node.get("total").asInt() > 0);
        assertNotNull(node.findValue("username"));
        assertEquals("test002", node.get("list").get(0).get("username").asText());
        assertEquals("CNY", node.get("list").get(0).get("currency").asText());
        assertNotNull(node.get("list").get(0).get("updatedAt").asText());
    }

    @Test
    public void testGetPendingRequestAmount() throws Exception {
        BigDecimal expectedUnapprovedRequestAmount = new BigDecimal(300);
        JsonNode resultNode = mockMvcPerform(get("/withdraw-requests/pending-amount"));
        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
        assertTrue(expectedUnapprovedRequestAmount.compareTo(new BigDecimal(resultNode.get("data").asText())) == 0);
    }
}
