package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiMessagingControllerTest extends BaseApiTest {
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerMessageService playerMessageService;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;

    @Test
    public void testListPlayerMessage() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertTrue(node.size() > 0);

        JsonNode threadNode = node.get(0);
        JsonNode messageNode = threadNode.get("messages").get(0);
        String playerUsername = playerMapper.get(getLoginPlayerId()).getUsername();
        assertEquals("Failed asserting that playerUsername is: " + playerUsername,
                messageNode.get("playerUsername").asText(), playerUsername);
        assertFalse(messageNode.get("read").asBoolean());

        // make sure none of the result messages contains deleted message
        node.forEach(aThreadNode -> {
            aThreadNode.get("messages").forEach(m -> {
                assertNotEquals("Message ID = 6 is deleted and should not appear in result",
                        6, m.get("id").asInt());
                assertFalse("None of the message should be deleted", m.get("deleted").asBoolean());
            });
        });
    }

    @Test
    public void testListPlayerMessageWithParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/messages")
                .param("createdAtEnd", ZonedDateTime.now().toString())
                .param("createdAtStart", ZonedDateTime.now().minusDays(2).toString())
                .param("threadId", "2"));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertNotNull(node);
        assertTrue(node.size() > 0);

        node.forEach(threadNode -> {
            assertTrue("Each thread must contain at least 1 message", threadNode.get("messages").size() > 0);
            threadNode.get("messages").forEach(messageNode -> {
                assertEquals("Each message should be for current login player", getLoginPlayerUsername(), messageNode.get("playerUsername").asText());
                assertFalse("Each message should have thread ID", messageNode.get("threadId").isNull());
                assertEquals("Each message should have thread ID = 2", 2, messageNode.get("threadId").asInt());
                assertTrue("createdAt must match search criteria",
                        ZonedDateTime.parse(messageNode.get("createdAt").asText()).isAfter(ZonedDateTime.now().minusDays(2)));
            });
        });
    }

    @Test
    public void testSendMessage() throws Exception {
        String testSubject = "Unit test subject";
        String testContent = "Unit test message";

        ApiMessagingController.MessageForm message = new ApiMessagingController.MessageForm();
        message.setSubject(testSubject);
        message.setContent(testContent);
        String requestBodyStr = JsonUtils.objectToJson(message);

        JsonNode resultNode = mockMvcPerform(post("/messages/send")
                .content(requestBodyStr));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);

        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setPlayerId(getLoginPlayerId());
        form.setSubject(testSubject);
        PlayerMessageView playerMessageView = playerMessageService.listMessage(form).get(0);
        assertEquals("The content of player_subject should be: " + testSubject,
                testSubject, playerMessageView.getSubject());
        assertEquals("The content of player_message should be: " + testContent,
                testContent, playerMessageView.getContent());
        assertEquals("This API will create a new thread", playerMessageView.getId(), playerMessageView.getThreadId());
        assertEquals(playerMessageView.getId().intValue(), resultNode.get("data").get("id").asInt());
        assertTrue("message should be read", playerMessageView.isRead());
    }

    @Test
    public void testReplyMessage() throws Exception {
        ApiMessagingController.MessageContentForm message = new ApiMessagingController.MessageContentForm();
        message.setContent("Unit test reply message");
        String requestBodyStr = JsonUtils.objectToJson(message);

        // Replying to message id = 2
        JsonNode resultNode = mockMvcPerform(post("/messages/2/reply")
                .content(requestBodyStr));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertDataSuccess(node);

        PlayerMessageThreadQueryForm form = new PlayerMessageThreadQueryForm();
        form.setPlayerId(2);
        PlayerMessageThreadView playerMessageView = playerMessageMapper.queryThreadsWithMessages(form).get(0);
        assertEquals("The thread should now contain 3 messages",
                3, playerMessageView.getMessages().size());

        PlayerMessageView latestMessage = playerMessageView.getMessages().get(0);
        assertEquals("Latest message should have the same content as the message we just added",
                message.getContent(), latestMessage.getContent());
        assertEquals(latestMessage.getId().intValue(), resultNode.get("data").get("id").asInt());
        assertTrue("Latest message should be read", latestMessage.isRead());
    }

    @Test
    public void testSetSystemMessageRead() throws Exception {
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setPlayerId(this.getLoginPlayerId());
        form.setSystem(true);
        List<PlayerMessageView> messages = playerMessageMapper.query(form, 1, 10);
        assertEquals("There should be 2 system messages for player " + this.getLoginPlayerId(), 2, messages.size());
        assertFalse("1st system message was unread before update", messages.get(0).isRead());

        JsonNode resultNode = mockMvcPerform(post("/messages/system-read"));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        List<PlayerMessageView> readMessages = playerMessageMapper.query(form, 1, 10);
        for (PlayerMessageView message : readMessages) {
            assertTrue("All queried messages should be system message", message.isSystem());
            assertTrue("All system messages should be read after update", message.isRead());
        }
    }

    @Test
    public void testSetMessageRead() throws Exception {
        Integer messageId = 2;
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setId(messageId);
        List<PlayerMessageView> messages = playerMessageMapper.query(form, 1, 10);
        assertFalse("Message was unread before update", messages.get(0).isRead());

        JsonNode resultNode = mockMvcPerform(post("/messages/{id}/read", messageId));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        messages = playerMessageMapper.query(form, 1, 10);
        assertTrue("Message is read after update", messages.get(0).isRead());
    }

    @Test
    public void testSetMessageBatchRead() throws Exception {
        int unreadCount = playerMessageMapper.getUnreadCount(getLoginPlayerId());
        assertTrue(unreadCount > 0);

        JsonNode resultNode = mockMvcPerform(post("/messages/all-read"));

        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        unreadCount = playerMessageMapper.getUnreadCount(getLoginPlayerId());
        assertEquals(0, unreadCount);
    }

    @Test
    public void testSetThreadRead() throws Exception {
        Integer threadId = 2;
        Integer messageId = 5;
        PlayerMessageQueryForm form = new PlayerMessageQueryForm();
        form.setId(messageId);
        List<PlayerMessageView> messages = playerMessageMapper.query(form, 1, 10);
        assertFalse("Message was unread before update", messages.get(0).isRead());

        JsonNode resultNode = mockMvcPerform(post("/messages/{id}/thread-read", threadId));
        assertOk(resultNode);
        assertDataSuccess(resultNode.get("data"));

        messages = playerMessageMapper.query(form, 1, 10);
        assertTrue("Message is read after update", messages.get(0).isRead());
    }

}
