package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.payment.DepositRequestView;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiDepositRequestControllerTest extends BaseApiTest {
    @Test
    public void testListDepositRequests() throws Exception {
        LocalDate startDate = LocalDate.now().minusDays(1);
        LocalDate endDate = LocalDate.now();
        JsonNode resultNode = mockMvcPerform(
                get("/payment-requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("requestedDateStart", startDate.toString() + "T00:00:00Z")
                        .param("requestedDateEnd", endDate.toString() + "T23:59:59Z")
                        .param("currency", "CNY")
                        .param("paymentApiIds", "1", "2", "3", "10")
                        .param("page", "1")
                        .param("limit", "20")
                        .param("sort", "requestedDate"));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals("There should be 3 active (status = 0) deposit requests for [" + getLoginPlayerUsername() +
                        "] for date [" + startDate + "] ~ [" + endDate + "]",
                3, node.get("total").asInt());
        assertEquals(3, node.get("list").size());

        node.get("list").forEach(depositRequestNode -> {
            assertEquals("Result must be under current login player", getLoginPlayerUsername(), depositRequestNode.get("username").asText());
            assertEquals("Result must be the current player's currency", "CNY", depositRequestNode.get("currency").asText());
            assertFalse("Result must contain valid payment method", depositRequestNode.get("paymentMethod").isNull());
            assertFalse("Result must fall in search date range",
                    ZonedDateTime.parse(depositRequestNode.get("requestedDate").asText()).toLocalDate().isBefore(startDate));
            assertFalse("Result must fall in search date range",
                    ZonedDateTime.parse(depositRequestNode.get("requestedDate").asText()).toLocalDate().isAfter(endDate));
        });
    }

    @Test
    public void testListDepositRequestsExpiredRequestShouldNotShowUp() throws Exception {
        int expiredDepositRequest = 131145;
        JsonNode resultNode = mockMvcPerform(
                get("/payment-requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", "1")
                        .param("limit", "20")
                        .param("sort", "requestedDate"));

        assertOk(resultNode);
        List<DepositRequestView> view = JsonUtils.jsonToObjectList(resultNode.get("data").get("list").toString(), DepositRequestView.class);
        List<DepositRequestView> notExpectedResult = view.stream()
                .filter(depositRequest -> depositRequest.getId() == expiredDepositRequest)
                        .collect(Collectors.toList());
        assertTrue(notExpectedResult.size() == 0);
    }

    @Test
    public void testListDepositRequestsNoParam() throws Exception {

        JsonNode resultNode = mockMvcPerform(
                get("/payment-requests")
                        .contentType(MediaType.APPLICATION_JSON));

        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertTrue( node.get("total").asInt() > 1);

        JsonNode items =node.get("list");
        assertNotNull(items);

        JsonNode aRequest = items.get(0);
        assertFalse(aRequest.get("paymentMethod").isNull());
    }
    
    @Test
    public void testGetPendingRequestAmount() throws Exception {
        BigDecimal expectedUnapprovedAmount = new BigDecimal(700);
        JsonNode resultNode = mockMvcPerform(get("/payment-requests/pending-amount"));
        assertOk(resultNode);
        assertNotNull(resultNode.get("data"));
        assertTrue(expectedUnapprovedAmount.compareTo(new BigDecimal(resultNode.get("data").asText())) == 0);
    }
}
