package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ApiBonusControllerTest extends BaseApiTest {

    @Test
    public void testListCashback() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-1);

        JsonNode resultNode = mockMvcPerform(get("/bonuses/cashback")
                .param("status", "" + BonusStatusEnum.PENDING_APPROVAL.getCode())
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10")
                .param("sort", "id DESC"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        for (int i = 0; i < list.size() - 1; i++) {
            assertEquals(2, list.get(i).get("playerId").asInt());
            assertTrue(list.get(i).get("referenceAmount").asInt() > 10);
            assertFalse(list.get(i).get("bonusDate").isNull());
            assertTrue(list.get(i).get("id").asInt() > list.get(i + 1).get("id").asInt());
        }
    }

    @Test
    public void testListCampaign() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now().plusDays(1);
        ZonedDateTime mockDateStart = mockDateEnd.plusDays(-30);

        JsonNode resultNode = mockMvcPerform(get("/bonuses/campaign")
                .param("status", "" + BonusStatusEnum.APPROVED.getCode())
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("ruleId", "3")
                .param("promoTypes", "12")
                .param("page", "1")
                .param("limit", "10")
                .param("sort", "bonusAmount desc"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertEquals("There should be exactly 2 task[3] bonuses approved under player test002 CNY", 2, list.size());
        for (int i = 0; i < list.size() - 1; i++) {
            assertEquals(2, list.get(i).get("playerId").asInt());
            assertFalse(list.get(i).get("bonusDate").isNull());
            assertEquals("Reference amount should be 0 for task type", 0, list.get(i).get("referenceAmount").asInt());
            assertTrue(list.get(i).get("bonusAmount").bigIntegerValue()
                    .compareTo(list.get(i + 1).get("bonusAmount").bigIntegerValue()) >= 0);
        }
    }

    @Test
    public void testListReferral() throws Exception {
        ZonedDateTime mockDateEnd = ZonedDateTime.now();
        ZonedDateTime mockDateStart = mockDateEnd.minusDays(7);

        JsonNode resultNode = mockMvcPerform(get("/bonuses/referral")
                .param("bonusDateStart", mockDateStart.toString())
                .param("bonusDateEnd", mockDateEnd.toString())
                .param("page", "1")
                .param("limit", "10"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertEquals("There's exactly 1 referral bonus pending approval", 1, list.size());
        JsonNode data = list.get(0);
        assertEquals(2, data.get("playerId").asInt());
        assertEquals(29000, data.get("referenceAmount").asInt());
    }

    @Test
    public void testListCashbackWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/cashback"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
    }

    @Test
    public void testListCampaignWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/campaign"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
    }

    @Test
    public void testListReferralWithoutParam() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/bonuses/referral"));
        assertOk(resultNode);

        JsonNode list = resultNode.get("data").get("list");
        assertNotNull(list);
        assertTrue(list.size() > 0);
    }
}
