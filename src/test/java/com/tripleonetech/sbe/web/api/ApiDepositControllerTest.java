package com.tripleonetech.sbe.web.api;

import java.math.BigDecimal;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.PaymentRequestForm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class ApiDepositControllerTest extends BaseApiTest {
    private static final int PAYMENT_METHOD_ID_MANUAL = 1;

    @Test
    public void testListAvailablePayment() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-methods"));
        assertOk(resultNode);

        JsonNode data = resultNode.get("data");
        assertEquals("test002 should have 2 payment methods available", 2, data.size());

        JsonNode aPaymentMethod = data.get(0);
        assertFalse(aPaymentMethod.get("id").isNull());
        assertFalse(aPaymentMethod.get("name").isNull());
        assertFalse(aPaymentMethod.get("minDeposit").isNull());
        assertFalse(aPaymentMethod.get("maxDeposit").isNull());
    }

    @Test
    public void testGetDepositRequestDetail() throws Exception {
        JsonNode resultNode = mockMvcPerform(get("/payment-requests/100006"));
        assertOk(resultNode);
        assertTrue(resultNode.get("data").size() > 0);
        assertEquals(DepositStatusEnum.CANCELED.getCode(), resultNode.get("data").get("status").asInt());
        assertNotNull(resultNode.get("data").get("updatedAt").asText());
        assertNotEquals("null", resultNode.get("data").get("updatedAt").asText());

        resultNode = mockMvcPerform(get("/payment-requests/100001")); // not under test002
        assertEntityNotFound(resultNode);
    }

    @Test
    public void testSubmitPaymentRequestManual() throws Exception {
        PaymentRequestForm paymentApiFormView = new PaymentRequestForm();
        paymentApiFormView.setPaymentMethodId(PAYMENT_METHOD_ID_MANUAL);
        paymentApiFormView.setAmount(new BigDecimal(100));

        String paymentStr = JsonUtils.objectToJson(paymentApiFormView);
        JsonNode resultNode = mockMvcPerform(post("/payment-requests")
                .contentType(MediaType.APPLICATION_JSON).content(paymentStr));
        assertOk(resultNode);

        JsonNode node = resultNode.get("data");
        assertEquals(100, node.get("params").get("amount").asInt());
    }

    @Test
    public void testSubmitPaymentRequestManualWithoutParams() throws Exception {
        PaymentRequestForm paymentApiNewForm = new PaymentRequestForm();
        paymentApiNewForm.setPaymentMethodId(PAYMENT_METHOD_ID_MANUAL);

        String paymentStr = JsonUtils.objectToJson(paymentApiNewForm);
        JsonNode resultNode = mockMvcPerform(post("/payment-requests")
                .contentType(MediaType.APPLICATION_JSON).content(paymentStr));
        this.assertInvalidParameter(resultNode);

    }


}
