package com.tripleonetech.sbe.creditpoint;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class CreditPointMapperTest extends BaseTest {

    @Autowired
    CreditPointMapper mapper;

    @Test
    public void testGet() {
        BigDecimal balance = mapper.get();
        assertEquals(balance, new BigDecimal("6666.660"));
    }

    @Test
    public void testAdd() {
        mapper.add(BigDecimal.TEN);
        assertEquals(mapper.get(), new BigDecimal("6676.660"));
    }

    @Test
    public void testSubtract() {
        mapper.subtract(BigDecimal.TEN);
        assertEquals(mapper.get(), new BigDecimal("6656.660"));
    }

}
