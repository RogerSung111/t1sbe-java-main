package com.tripleonetech.sbe.creditpoint;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseTest;

public class CreditPointServiceTest extends BaseTest {
    
    @Autowired
    private CreditPointService creditPointService;
    
    @Autowired
    private CreditPointMapper creditPointMapper;
  
    private BigDecimal originalCreditPoint;
    
    @Before
    public void setup() {
        originalCreditPoint = creditPointMapper.get();
    }

    @Test
    public void testSubtractCreditPoint() {
        creditPointService.subtractCreditPoint(originalCreditPoint);
        BigDecimal currentCreditPoint = creditPointMapper.get();
        assertTrue(BigDecimal.ZERO.compareTo(currentCreditPoint) == 0);

    }
    
    @Test
    public void testSubtractExcessCreditPoint() {
        boolean result = creditPointService.subtractCreditPoint(originalCreditPoint.add(BigDecimal.ONE));
        assertTrue(!result);
        BigDecimal currentCreditPoint = creditPointMapper.get();
        assertTrue(currentCreditPoint.compareTo(originalCreditPoint) == 0);

    }
}
