package com.tripleonetech.sbe.common.web;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeRangeFormTest {
    @Test
    public void testTimezoneOffsetString() {
        TimeRangeForm form = new TimeRangeForm();
        form.setTimezoneOffset(8);
        assertEquals("+08:00", form.getTimezoneOffsetString());

        form.setTimezoneOffset(10);
        assertEquals("+10:00", form.getTimezoneOffsetString());

        form.setTimezoneOffset(-8);
        assertEquals("-08:00", form.getTimezoneOffsetString());

        form.setTimezoneOffset(-10);
        assertEquals("-10:00", form.getTimezoneOffsetString());
    }
}
