package com.tripleonetech.sbe.common.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.tripleonetech.sbe.BaseTest;
import com.tripleonetech.sbe.common.integration.ApiConfig;

public class ApiConfigTest extends BaseTest {
    private ApiConfig apiConfig;

    @Before
    public void setUp() throws Exception {
        apiConfig = new ApiConfig();
    }

    @Test
    public void testParseMetaSuccess() {
        apiConfig.setMeta("{\r\n" +
                "   \"url\": \"http://api.mg.com/service\",\r\n" +
                "   \"username\": \"test\",\r\n" +
                "   \"key\": \"123abc\"\r\n" +
                " }");
        assertEquals("http://api.mg.com/service", apiConfig.get("url"));
        assertEquals("test", apiConfig.get("username"));
        assertEquals("123abc", apiConfig.get("key"));
    }
}
