package com.tripleonetech.sbe.common.notification;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.BaseApiTest;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MockApiHttpServiceFactory;
import com.tripleonetech.sbe.common.notification.T1BatchNotification;
import com.tripleonetech.sbe.common.notification.T1Notification;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;

public class T1NotificationApiTest extends BaseApiTest {
    @Autowired
    private T1NotificationApi notificationApi;

    private String recipient = "java@tripleonetech.net";
    private String subject = "notification subject for unit test";
    private String content = "Hi, this is notification content for unit test.";
    private String template = "Hi {{playerUsername}}, this is content template for unit test.";

    @Test
    public void testSend_SMS() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        String phone = "886-917123456";
        T1Notification notification = new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.SMS)
                .content(content)
                .addRecipients(phone);
        ApiResult apiResult = notificationApi.send(notification);

        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
    }

    @Test
    public void testSend_Email() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockSuccessResponse }));
        T1Notification notification = new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.MAIL_TXT)
                .subject(subject)
                .content(content)
                .addRecipients(recipient);
        ApiResult apiResult = notificationApi.send(notification);

        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
    }

    @Test
    public void testSend_Email_Failed() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockFailedResponse }));
        T1Notification notification = new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.MAIL_TXT)
                .subject(subject)
                .content(content)
                .addRecipients(recipient);
        ApiResult apiResult = notificationApi.send(notification);

        assertEquals(ApiResponseEnum.ACTION_UNSUCCESSFUL, apiResult.getResponseEnum());
    }

    @Test
    public void testSend_Email_InvalidFailed() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockInvalidParamResponse }));
        T1Notification notification = new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.MAIL_TXT)
                .subject(subject)
                .content(content)
                .addRecipients(recipient);
        ApiResult apiResult = notificationApi.send(notification);

        assertEquals(ApiResponseEnum.BAD_REQUEST, apiResult.getResponseEnum());
    }

    @Test
    public void testSendBroadcast_Email() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockBroadcastResponse }));
        T1Notification notification = new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.MAIL_TXT)
                .subject(subject)
                .content(content)
                .addRecipients(recipient);
        ApiResult apiResult = notificationApi.sendBroadcast(notification);

        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
    }

    @Test
    public void testSendBatch_Email() {
        notificationApi.setHttpService(MockApiHttpServiceFactory.get(new String[] { mockBatchResponse }));
        T1BatchNotification notification = new T1BatchNotification()
                .type(T1CommonNotification.MessageTypeEnum.MAIL_TXT)
                .template(template)
                .addListElements(
                        new T1BatchNotification.ListContent()
                                .subject(subject)
                                .recipient(recipient)
                                .putData("playerUsername", "George"),
                        new T1BatchNotification.ListContent()
                                .subject(subject)
                                .recipient(recipient)
                                .putData("playerUsername", "Mary"));
        ApiResult apiResult = notificationApi.sendBatch(notification);

        assertEquals(ApiResponseEnum.OK, apiResult.getResponseEnum());
    }

    private static String mockSuccessResponse;
    private static String mockFailedResponse;
    private static String mockInvalidParamResponse;
    private static String mockBroadcastResponse;
    private static String mockBatchResponse;
    static {
        mockSuccessResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":30}}";
        mockFailedResponse = "{\"so_id\":2451,\"notification\":{\"id\":2459,\"status\":90}}";
        mockInvalidParamResponse = "{\"code\":\"ValidationFailed\",\"message\":\"资料验证失败\",\"error\":{\"code\":\"ValidationFailed\",\"message\":\"资料验证失败\"},\"success\":false}";
        mockBroadcastResponse = "{\"so_id\": 169, \"notifications\": [{\"id\": 263, \"status\": 30}, {\"id\": 264, \"status\": 30 }] }";
        mockBatchResponse = "{\"so_id\": 170 }";
    }
}
