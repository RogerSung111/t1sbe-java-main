package com.tripleonetech.sbe.common;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConstantTest extends BaseTest {
    @Autowired
    private Constant constant;

    @Test
    public void testValueOfTimezone() {
        assertEquals("GMT+8", constant.getTimezone());
        assertEquals(Constants.get("timezone"), constant.getTimezone());
    }

    @Test
    public void testValueOfPlayerPrefix() {
        assertEquals("dev", constant.getGame().getPlayerPrefix());
        assertEquals(Constants.get("game.player-prefix"), constant.getGame().getPlayerPrefix());
    }

    @Test
    public void testFirewallOff() {
        assertTrue("Firewall should be OFF in test environment", constant.getFrontend().isFirewallOff());
        assertTrue("Firewall should be OFF in test environment", constant.getBackend().isFirewallOff());
    }

    @Test
    public void testSingleton() {
        assertEquals(this.constant, Constant.getInstance());
    }
}
