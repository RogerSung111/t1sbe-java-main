package com.tripleonetech.sbe.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.tripleonetech.sbe.BaseTest;

public class ConstantsTest extends BaseTest {
    @Test
    public void testConstants() {
        assertEquals("Test getting value of 'constant.test' property from application-test.properties", "TEST_CONSTANT",
                Constants.get("test"));
    }

    @Test
    public void testBackendDefaultLocaleConstant() {
        assertTrue("Test getting value of 'constant.backend.default-locale' property from application.properties",
                !StringUtils.isEmpty(Constants.get("backend.default-locale")));
    }

    @Test
    public void testGameConstant() {
        assertEquals("GMT+8", Constants.get("game.T1GAGIN.timezone"));
    }
}
