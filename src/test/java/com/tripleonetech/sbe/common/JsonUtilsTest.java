package com.tripleonetech.sbe.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

// Note: This unit test does not require spring
public class JsonUtilsTest /* extends BaseTest */ {
    // private static final Logger logger = LoggerFactory.getLogger(JsonUtilsTest.class);

    @Test
    public void testJsonToMap() {
        Map<String, String> stringMap = JsonUtils.jsonToMap("{ \"name\":\"John\", \"age\":30, \"car\":null }");
        assertEquals("John", stringMap.get("name"));
        assertEquals("30", stringMap.get("age"));
        assertTrue(stringMap.containsKey("car"));
        assertNull(stringMap.get("car"));
    }

    @Test
    public void testMapToJson() {
        Map<String, Object> stringMap = new HashMap<String, Object>();
        stringMap.put("name", "John");
        stringMap.put("age", 30);
        stringMap.put("car", null);
        String jsonString = JsonUtils.objectToJson(stringMap);
        assertNotNull(jsonString);
    }

    @Test(expected = JsonException.class)
    public void testInvalidJsonToMap() {
        // missing last curly braces
        JsonUtils.jsonToMap("{ \"name\":\"John\", \"age\":30, \"car\":null, \"address\":\"<>?\"\" ");
    }

}

