package com.tripleonetech.sbe.common.storage;

import com.tripleonetech.sbe.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class FileSystemStorageServiceTest extends BaseTest {

    @Autowired
    private FileSystemStorageService storageService;

    private String subFolder = "testfolder";
    private String originalFilename = "test_for_upload.txt";
    private String fileContent;
    private MockMultipartFile multipartFile;
    private String filePath;

    @Before
    public void setUp() throws Exception {
        fileContent = "test file upload at ".concat(LocalDateTime.now().toString());
        multipartFile = new MockMultipartFile("file", originalFilename, "text/plain", fileContent.getBytes());
    }

    @After
    public void tearDown() {
        // revert
        if (filePath != null && !filePath.isEmpty()) {
            storageService.delete(filePath);
        }

        try(Stream<Path> str = Files.list(Paths.get(storageService.getRootLocation().toString(), subFolder))) {
            str.forEach(path -> {
                try {
                    Files.deleteIfExists(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStore() throws Exception {
        filePath = storageService.store(multipartFile);

        Path uploadedFile = storageService.load(filePath);
        assertEquals(fileContent, new String(Files.readAllBytes(uploadedFile)));
    }

    @Test
    public void testStore_SpecifyFilename() throws Exception {
        filePath = storageService.store(multipartFile, originalFilename);

        Path uploadedFile = storageService.load(filePath);
        assertEquals(originalFilename, filePath);
        assertEquals(fileContent, new String(Files.readAllBytes(uploadedFile)));
    }

    @Test
    public void testStore_SpecifyFilenameWithSubFolder() throws Exception {
        filePath = storageService.store(multipartFile, subFolder, originalFilename);

        Path uploadedFile = storageService.load(filePath);

        assertEquals(String.join(File.separator, subFolder, originalFilename), filePath);
        assertEquals(fileContent, new String(Files.readAllBytes(uploadedFile)));
    }

    @Test
    public void testStore_SpecifySpecialSymbolName() throws Exception {
        String specialSymbolName = "#-$%^".concat(originalFilename.substring(0, 2)).concat("~！")
                .concat(originalFilename.substring(2));
        filePath = storageService.store(multipartFile, specialSymbolName);

        assertNotEquals(specialSymbolName, filePath);
        String regEx = "[^a-zA-Z0-9-_.]";
        assertFalse(Pattern.compile(regEx).matcher(filePath).find());
        Path uploadedFile = storageService.load(filePath);
        assertEquals(fileContent, new String(Files.readAllBytes(uploadedFile)));
    }

    @Test
    public void testLoadAll() {
        Stream<Path> allFilePaths = storageService.loadAll();
        List<String> filenames = allFilePaths.map(Path::toString).collect(Collectors.toList());

        File folder = new File(storageService.getRootLocation().toString());
        File[] files = folder.listFiles();
        for (File file : files) {
            assertTrue(filenames.contains(file.getName()));
        }
    }

    @Test
    public void testLoadAll_SpecifySubFolder() throws Exception {
        // Prepare the file to be tested
        filePath = storageService.store(multipartFile, subFolder, originalFilename);

        Stream<Path> allFilePaths = storageService.loadAll(subFolder);
        assertEquals(originalFilename, allFilePaths.findFirst().get().getFileName().toString());
    }

    @Test
    public void testLoad() throws Exception {
        // Prepare the file to be tested
        filePath = storageService.store(multipartFile, originalFilename);
        Path resource = storageService.load(filePath);

        assertEquals(fileContent, new String(Files.readAllBytes(resource)));
    }

    @Test
    public void testLoadAsResource() throws Exception {
        // Prepare the file to be tested
        filePath = storageService.store(multipartFile, originalFilename);
        Resource resource = storageService.loadAsResource(filePath);

        byte[] downloadBytes = new byte[fileContent.getBytes().length];
        try (InputStream inStream = resource.getInputStream()) {
            inStream.read(downloadBytes);
            assertEquals(fileContent, new String(downloadBytes));
        }
    }

    @Test
    public void testDelete() throws Exception {
        // Prepare the file to be tested
        filePath = storageService.store(multipartFile, originalFilename);
        Resource resource = storageService.loadAsResource(filePath);
        assertTrue(resource.exists());

        storageService.delete(filePath);
        assertFalse(resource.exists());
    }

}
