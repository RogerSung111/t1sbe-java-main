package com.tripleonetech.sbe.common.integration;

import com.tripleonetech.sbe.BaseApiTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertEquals;

public class ClientHttpInfoUtilsTest extends BaseApiTest {
    @Autowired
    private DeviceResolverHandlerInterceptor interceptor;
    private MockHttpServletRequest request;

    @Before
    public void setUp() throws Exception {
        request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

//    @Test
//    public void testGetDeviceType_APP_IOS() throws Exception {
//        request.addHeader("User-Agent", "T1sbeApp/1.0.0 iPhone iOS/10_1 CFNetwork/808.3 Darwin/16.3.0");
//        interceptor.preHandle(request, null, null);
//        assertEquals(DeviceTypeEnum.APP_IOS, ClientHttpInfoUtils.getDeviceType());
//    }
//
//    @Test
//    public void testGetDeviceType_APP_ANDROID() throws Exception {
//        request.addHeader("User-Agent",
//                "T1sbeApp/1.0.0 Dalvik/2.1.0 (Linux; U; Android 6.0.1; vivo 1610 Build/MMB29M)");
//        interceptor.preHandle(request, null, null);
//        assertEquals(DeviceTypeEnum.APP_ANDROID, ClientHttpInfoUtils.getDeviceType());
//    }

    @Test
    public void testGetDeviceType_MOBILE_HTML() throws Exception {
        request.addHeader("User-Agent",
                "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1");
        interceptor.preHandle(request, null, null);
        assertEquals(DeviceTypeEnum.MOBILE, ClientHttpInfoUtils.getDeviceType());
    }

    @Test
    public void testGetDeviceType_COMPUTER_HTML() throws Exception {
        request.addHeader("User-Agent",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
        interceptor.preHandle(request, null, null);
        assertEquals(DeviceTypeEnum.COMPUTER, ClientHttpInfoUtils.getDeviceType());
    }

    @Test
    public void testGetDeviceType_TABLET_HTML() throws Exception {
        request.addHeader("User-Agent",
                "Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1");
        interceptor.preHandle(request, null, null);
        assertEquals(DeviceTypeEnum.TABLET, ClientHttpInfoUtils.getDeviceType());
    }

}
