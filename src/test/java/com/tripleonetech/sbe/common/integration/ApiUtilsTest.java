package com.tripleonetech.sbe.common.integration;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class ApiUtilsTest extends BaseTest {
    @Test
    public void testGetMetaFieldsForPayment() {
        Map<String, List<ApiMetaField>> metaFields = ApiUtils.getMetaFieldsByApiType(ApiTypeEnum.PAYMENT);
        assertTrue("We should get metaFields for payment APIs", metaFields.size() > 0);
        assertTrue("We should get metaFields for MockPay", metaFields.containsKey("MockPay"));
        assertTrue("MetaFields should exist for MockPay", metaFields.get("MockPay").size() > 0);
    }
}
