package com.tripleonetech.sbe.common.integration;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseFactory;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.message.BasicStatusLine;
import org.mockito.Mockito;

public class MockApiHttpServiceFactory {
    /**
     * Creates a mock ApiHttpService instance which returns all responses passed in as parameter in order.
     * 1st call returns responses[0], 2nd call returns responses[1], etc.
     * The last response will be repeated.
     * 
     * @param responses
     *            Mock responses of each api call
     */
    public static ApiHttpService get(String[] responses) {
        HttpResponseFactory responseFactory = new DefaultHttpResponseFactory();
        List<HttpResponse> responseList = new ArrayList<>();

        for (String response : responses) {
            HttpResponse r = responseFactory.newHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, null), null);
            r.setEntity(EntityBuilder.create().setText(response).build());
            responseList.add(r);
        }
        
        ApiHttpService mockService = Mockito.mock(ApiHttpService.class);
        if (responses.length == 1) {
        Mockito.when(mockService.postAsFormFields(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(responseList.get(0));
        Mockito.when(mockService.postAsJsonData(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(responseList.get(0));
        Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any()))
                    .thenReturn(responseList.get(0));
        Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenReturn(responseList.get(0));
        } else if (responses.length > 1) {
            HttpResponse firstResponse = responseList.get(0);
            responseList.remove(0);
            HttpResponse[] restOfResponses = responseList.toArray(new HttpResponse[0]);
            Mockito.when(mockService.postAsFormFields(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(firstResponse, restOfResponses);
            Mockito.when(mockService.postAsJsonData(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(firstResponse, restOfResponses);
            Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any()))
                    .thenReturn(firstResponse, restOfResponses);
            Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any(), Mockito.any()))
            .thenReturn(responseList.get(0));
        } else {
            Mockito.when(mockService.postAsFormFields(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(null);
            Mockito.when(mockService.postAsJsonData(Mockito.anyString(), Mockito.anyMap()))
                    .thenReturn(null);
            Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any()))
                    .thenReturn(null);
            Mockito.when(mockService.submitGet(Mockito.anyString(), Mockito.any(), Mockito.any()))
            .thenReturn(responseList.get(0));
        }

        return mockService;
    }

}
