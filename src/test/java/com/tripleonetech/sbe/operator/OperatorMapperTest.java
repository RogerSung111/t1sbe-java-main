package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class OperatorMapperTest extends BaseTest {
    @Autowired
    private OperatorMapper operatorMapper;

    @Test
    public void testGetUser() {
        int userId = 1;
        Operator operator = operatorMapper.get(userId);
        assertNotNull("Should get the user whose ID = 1", operator);
        assertEquals("Username should be superadmin", "superadmin", operator.getUsername());
        assertTrue("User password should be abcdef", operator.validatePassword("abcdef"));
        assertNotNull("Should contain created at", operator.getCreatedAt());
        assertNotNull("Result operator should contain a role", operator.getRole());
    }

    @Test
    public void testNewUser() {
        Operator operator = new Operator();
        operator.setUsername("qaadmin");
        operator.setPassword("abcdef");
        operator.setStatusEnum(OperatorStatusEnum.ENABLED);
        operatorMapper.insert(operator);
        assertTrue("User should be inserted successfully and updated with a positive ID", operator.getId() > 0);
    }
    
    @Test
    public void testUpdateUser() {
        Operator operator = new Operator();
        operator.setUsername("qaadmin");
        operator.setPassword("abcdef");
        operator.setStatusEnum(OperatorStatusEnum.ENABLED);
        operatorMapper.insert(operator);
    	operator.setPassword("123456");
    	operatorMapper.update(operator);

        // Query back updated object
    	Operator updatedOperator = operatorMapper.get(operator.getId());
    	assertEquals(operator.getPasswordHash(), updatedOperator.getPasswordHash());
    }
}
