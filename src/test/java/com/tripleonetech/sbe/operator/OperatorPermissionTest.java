package com.tripleonetech.sbe.operator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class OperatorPermissionTest {

    @Test
    public void testParse(){
        OperatorPermission permission = new OperatorPermission();
        permission.setValue("[\"0\",\"0\",\"13101011122212\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]");
        assertNotNull(permission.getValues());
        assertEquals(14, permission.getValues().size());
        assertEquals("13101011122212".length(), permission.getValues().get(2).size());
        assertEquals(3, permission.getValues().get(2).get(1).intValue());
    }

}
