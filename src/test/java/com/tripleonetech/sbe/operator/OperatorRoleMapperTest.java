package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OperatorRoleMapperTest extends BaseTest {
    @Autowired
    private OperatorRoleMapper operatorRoleMapper;
    @Autowired
    private OperatorMapper operatorMapper;

    private int roleId = 2;

    @Test
    public void testGetOperatorRole() {
        OperatorRole role = operatorRoleMapper.get(roleId);

        assertNotNull(role);
        assertEquals("Query should return OPERATOR role", "OPERATOR", role.getName().toUpperCase());
        assertTrue("Query should return permission string", role.getPermission().length() > 0);
    }

    @Test
    public void testListOperatorRole() {
        List<OperatorRole> roles = operatorRoleMapper.list();

        assertEquals("Query should return a total of 5 pre-defined roles", 5, roles.size());
    }

    @Test
    public void testInsertOperatorRole() {
        OperatorRole role = operatorRoleMapper.get(roleId);
        role.setName("New Role");
        operatorRoleMapper.insert(role);
        assertNotEquals("After insert, object should be given new ID", roleId, role.getId());

        OperatorRole roleUpdated = operatorRoleMapper.get(role.getId());
        assertNotNull("Should be able to query new role", roleUpdated);
        assertEquals("New role name validation", "New Role", roleUpdated.getName());
    }

    @Test
    public void testUpdateOperatorRole() {
        OperatorRole role = operatorRoleMapper.get(roleId);
        role.setName("Updated_Role");
        operatorRoleMapper.update(role);

        OperatorRole roleUpdated = operatorRoleMapper.get(roleId);
        assertEquals("Query should return role with updated name", "Updated_Role", roleUpdated.getName());
    }

    @Test
    public void testDeleteOperatorRole() {
        Operator operator = operatorMapper.get(2);
        assertEquals("Query operator whose role is Operator", roleId, operator.getRole().getId());

        operatorRoleMapper.delete(roleId);

        OperatorRole roleDeleted = operatorRoleMapper.get(roleId);
        assertNull("Should not be able to query role", roleDeleted);

        operator = operatorMapper.get(2);
        assertEquals("Operator with \'Operator\' role should have its role removed", 0, operator.getRole().getId());
        assertNull("Operator with \'Operator\' role should have its role removed", operator.getRole().getName());
    }

}
