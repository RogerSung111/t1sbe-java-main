# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: t1sbe-test
# Generation Time: 2019-12-20 10:16:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

#IP rule countries
INSERT IGNORE INTO `ip_rule_country` (`id`, `country_code`) VALUES
(1,'AD'),(2,'AE'),(3,'AF'),(4,'AG'),(5,'AI'),(6,'AL'),(7,'AM'),(8,'AO'),(9,'AQ'),(10,'AR'),(11,'AS'),(12,'AT'),(13,'AU'),(14,'AW'),(15,'AX'),(16,'AZ'),(17,'BA'),(18,'BB'),(19,'BD'),(20,'BE'),(21,'BF'),(22,'BG'),(23,'BH'),(24,'BI'),(25,'BJ'),(26,'BL'),(27,'BM'),(28,'BN'),(29,'BO'),(30,'BQ'),(31,'BR'),(32,'BS'),(33,'BT'),(34,'BW'),(35,'BY'),(36,'BZ'),(37,'CA'),(38,'CD'),(39,'CF'),(40,'CG'),(41,'CH'),(42,'CI'),(43,'CK'),(44,'CL'),(45,'CM'),(46,'CN'),(47,'CO'),(48,'CR'),(49,'CU'),(50,'CV'),(51,'CW'),(52,'CY'),(53,'CZ'),(54,'DE'),(55,'DJ'),(56,'DK'),(57,'DM'),(58,'DO'),(59,'DZ'),(60,'EC'),(61,'EE'),(62,'EG'),(63,'ER'),(64,'ES'),(65,'ET'),(66,'FI'),(67,'FJ'),(68,'FK'),(69,'FM'),(70,'FO'),(71,'FR'),(72,'GA'),(73,'GB'),(74,'GD'),(75,'GE'),(76,'GF'),(77,'GG'),(78,'GH'),(79,'GI'),(80,'GL'),(81,'GM'),(82,'GN'),(83,'GP'),(84,'GQ'),(85,'GR'),(86,'GT'),(87,'GU'),(88,'GW'),(89,'GY'),(90,'HK'),(91,'HN'),(92,'HR'),(93,'HT'),(94,'HU'),(95,'ID'),(96,'IE'),(97,'IL'),(98,'IM'),(99,'IN'),(100,'IO'),(101,'IQ'),(102,'IR'),(103,'IS'),(104,'IT'),(105,'JE'),(106,'JM'),(107,'JO'),(108,'JP'),(109,'KE'),(110,'KG'),(111,'KH'),(112,'KI'),(113,'KM'),(114,'KN'),(115,'KP'),(116,'KR'),(117,'KW'),(118,'KY'),(119,'KZ'),(120,'LA'),(121,'LB'),(122,'LC'),(123,'LI'),(124,'LK'),(125,'LR'),(126,'LS'),(127,'LT'),(128,'LU'),(129,'LV'),(130,'LY'),(131,'MA'),(132,'MC'),(133,'MD'),(134,'ME'),(135,'MF'),(136,'MG'),(137,'MH'),(138,'MK'),(139,'ML'),(140,'MM'),(141,'MN'),(142,'MO'),(143,'MP'),(144,'MQ'),(145,'MR'),(146,'MS'),(147,'MT'),(148,'MU'),(149,'MV'),(150,'MW'),(151,'MX'),(152,'MY'),(153,'MZ'),(154,'NA'),(155,'NC'),(156,'NE'),(157,'NF'),(158,'NG'),(159,'NI'),(160,'NL'),(161,'NO'),(162,'NP'),(163,'NR'),(164,'NU'),(165,'NZ'),(166,'OM'),(167,'PA'),(168,'PE'),(169,'PF'),(170,'PG'),(171,'PH'),(172,'PK'),(173,'PL'),(174,'PM'),(175,'PR'),(176,'PS'),(177,'PT'),(178,'PW'),(179,'PY'),(180,'QA'),(181,'RE'),(182,'RO'),(183,'RS'),(184,'RU'),(185,'RW'),(186,'SA'),(187,'SB'),(188,'SC'),(189,'SD'),(190,'SE'),(191,'SG'),(192,'SH'),(193,'SI'),(194,'SJ'),(195,'SK'),(196,'SL'),(197,'SM'),(198,'SN'),(199,'SO'),(200,'SR'),(201,'SS'),(202,'ST'),(203,'SV'),(204,'SX'),(205,'SY'),(206,'SZ'),(207,'TC'),(208,'TD'),(209,'TG'),(210,'TH'),(211,'TJ'),(212,'TK'),(213,'TL'),(214,'TM'),(215,'TN'),(216,'TO'),(217,'TR'),(218,'TT'),(219,'TV'),(220,'TW'),(221,'TZ'),(222,'UA'),(223,'UG'),(224,'UM'),(225,'US'),(226,'UY'),(227,'UZ'),(228,'VA'),(229,'VC'),(230,'VE'),(231,'VG'),(232,'VI'),(233,'VN'),(234,'VU'),(235,'WF'),(236,'WS'),(237,'YE'),(238,'YT'),(239,'ZA'),(240,'ZM'),(241,'ZW');

INSERT IGNORE INTO `ip_rule_country_i18n` (`country_id`, `locale`, `name`) VALUES
(1,'zh-CN','安道尔'),(2,'zh-CN','阿联酋'),(3,'zh-CN','阿富汗'),(4,'zh-CN','安提瓜和巴布达'),(5,'zh-CN','安圭拉'),(6,'zh-CN','阿尔巴尼亚'),(7,'zh-CN','亚美尼亚'),(8,'zh-CN','安哥拉'),(9,'zh-CN','南极洲'),(10,'zh-CN','阿根廷'),(11,'zh-CN','美属萨摩亚'),(12,'zh-CN','奥地利'),(13,'zh-CN','澳大利亚'),(14,'zh-CN','阿鲁巴'),(15,'zh-CN','奥兰'),(16,'zh-CN','阿塞拜疆'),(17,'zh-CN','波黑'),(18,'zh-CN','巴巴多斯'),(19,'zh-CN','孟加拉国孟加拉国'),(20,'zh-CN','比利时'),(21,'zh-CN','布基纳法索'),(22,'zh-CN','保加利亚'),(23,'zh-CN','巴林'),(24,'zh-CN','布隆迪'),(25,'zh-CN','贝宁'),(26,'zh-CN','圣巴泰勒米'),(27,'zh-CN','百慕大'),(28,'zh-CN','文莱'),(29,'zh-CN','玻利维亚'),(30,'zh-CN','荷兰加勒比区'),(31,'zh-CN','巴西'),(32,'zh-CN','巴哈马'),(33,'zh-CN','不丹'),(34,'zh-CN','博茨瓦纳'),(35,'zh-CN','白俄罗斯'),(36,'zh-CN','伯利兹'),(37,'zh-CN','加拿大'),(38,'zh-CN','刚果民主共和国'),(39,'zh-CN','中非'),(40,'zh-CN','刚果共和国'),(41,'zh-CN','瑞士'),(42,'zh-CN','科特迪瓦'),(43,'zh-CN','库克群岛'),(44,'zh-CN','智利'),(45,'zh-CN','喀麦隆'),(46,'zh-CN','中国'),(47,'zh-CN','哥伦比亚'),(48,'zh-CN','哥斯达黎加'),(49,'zh-CN','古巴'),(50,'zh-CN','佛得角'),(51,'zh-CN','库拉索'),(52,'zh-CN','塞浦路斯'),(53,'zh-CN','捷克'),(54,'zh-CN','德国'),(55,'zh-CN','吉布提'),(56,'zh-CN','丹麦'),(57,'zh-CN','多米尼克'),(58,'zh-CN','多米尼加'),(59,'zh-CN','阿尔及利亚'),(60,'zh-CN','厄瓜多尔'),(61,'zh-CN','爱沙尼亚'),(62,'zh-CN','埃及'),(63,'zh-CN','厄立特里亚'),(64,'zh-CN','西班牙'),(65,'zh-CN','埃塞俄比亚'),(66,'zh-CN','芬兰'),(67,'zh-CN','斐济'),(68,'zh-CN','福克兰群岛'),(69,'zh-CN','密克罗尼西亚联邦'),(70,'zh-CN','法罗群岛'),(71,'zh-CN','法国'),(72,'zh-CN','加蓬'),(73,'zh-CN','英国'),(74,'zh-CN','格林纳达'),(75,'zh-CN','格鲁吉亚'),(76,'zh-CN','法属圭亚那'),(77,'zh-CN','根西'),(78,'zh-CN','加纳'),(79,'zh-CN','直布罗陀'),(80,'zh-CN','格陵兰'),(81,'zh-CN','冈比亚'),(82,'zh-CN','几内亚'),(83,'zh-CN','瓜德罗普'),(84,'zh-CN','赤道几内亚'),(85,'zh-CN','希腊'),(86,'zh-CN','危地马拉'),(87,'zh-CN','关岛'),(88,'zh-CN','几内亚比绍'),(89,'zh-CN','圭亚那'),(90,'zh-CN','香港'),(91,'zh-CN','洪都拉斯'),(92,'zh-CN','克罗地亚'),(93,'zh-CN','海地'),(94,'zh-CN','匈牙利'),(95,'zh-CN','印尼'),(96,'zh-CN','爱尔兰'),(97,'zh-CN','以色列'),(98,'zh-CN','马恩岛'),(99,'zh-CN','印度'),(100,'zh-CN','英属印度洋领地'),(101,'zh-CN','伊拉克'),(102,'zh-CN','伊朗'),(103,'zh-CN','冰岛'),(104,'zh-CN','意大利'),(105,'zh-CN','泽西'),(106,'zh-CN','牙买加'),(107,'zh-CN','约旦'),(108,'zh-CN','日本'),(109,'zh-CN','肯尼亚'),(110,'zh-CN','吉尔吉斯斯坦'),(111,'zh-CN','柬埔寨'),(112,'zh-CN','基里巴斯'),(113,'zh-CN','科摩罗'),(114,'zh-CN','圣基茨和尼维斯'),(115,'zh-CN','朝鲜'),(116,'zh-CN','韩国'),(117,'zh-CN','科威特'),(118,'zh-CN','开曼群岛'),(119,'zh-CN','哈萨克斯坦'),(120,'zh-CN','老挝'),(121,'zh-CN','黎巴嫩'),(122,'zh-CN','圣卢西亚'),(123,'zh-CN','列支敦士登'),(124,'zh-CN','斯里兰卡'),(125,'zh-CN','利比里亚'),(126,'zh-CN','莱索托'),(127,'zh-CN','立陶宛'),(128,'zh-CN','卢森堡'),(129,'zh-CN','拉脱维亚'),(130,'zh-CN','利比亚'),(131,'zh-CN','摩洛哥'),(132,'zh-CN','摩纳哥'),(133,'zh-CN','摩尔多瓦'),(134,'zh-CN','黑山'),(135,'zh-CN','法属圣马丁'),(136,'zh-CN','马达加斯加'),(137,'zh-CN','马绍尔群岛'),(138,'zh-CN','北马其顿'),(139,'zh-CN','马里'),(140,'zh-CN','缅甸'),(141,'zh-CN','蒙古国'),(142,'zh-CN','澳门'),(143,'zh-CN','北马里亚纳群岛'),(144,'zh-CN','马提尼克'),(145,'zh-CN','毛里塔尼亚'),(146,'zh-CN','蒙特塞拉特'),(147,'zh-CN','马耳他'),(148,'zh-CN','毛里求斯'),(149,'zh-CN','马尔代夫'),(150,'zh-CN','马拉维'),(151,'zh-CN','墨西哥'),(152,'zh-CN','马来西亚'),(153,'zh-CN','莫桑比克'),(154,'zh-CN','纳米比亚'),(155,'zh-CN','新喀里多尼亚'),(156,'zh-CN','尼日尔'),(157,'zh-CN','诺福克岛'),(158,'zh-CN','尼日利亚'),(159,'zh-CN','尼加拉瓜'),(160,'zh-CN','荷兰'),(161,'zh-CN','挪威'),(162,'zh-CN','尼泊尔'),(163,'zh-CN','瑙鲁'),(164,'zh-CN','纽埃'),(165,'zh-CN','新西兰'),(166,'zh-CN','阿曼'),(167,'zh-CN','巴拿马'),(168,'zh-CN','秘鲁'),(169,'zh-CN','法属波利尼西亚'),(170,'zh-CN','巴布亚新几内亚'),(171,'zh-CN','菲律宾'),(172,'zh-CN','巴基斯坦'),(173,'zh-CN','波兰'),(174,'zh-CN','圣皮埃尔和密克隆'),(175,'zh-CN','波多黎各'),(176,'zh-CN','巴勒斯坦'),(177,'zh-CN','葡萄牙'),(178,'zh-CN','帕劳'),(179,'zh-CN','巴拉圭'),(180,'zh-CN','卡塔尔'),(181,'zh-CN','留尼汪'),(182,'zh-CN','罗马尼亚'),(183,'zh-CN','塞尔维亚'),(184,'zh-CN','俄罗斯'),(185,'zh-CN','卢旺达'),(186,'zh-CN','沙特阿拉伯'),(187,'zh-CN','所罗门群岛'),(188,'zh-CN','塞舌尔'),(189,'zh-CN','苏丹'),(190,'zh-CN','瑞典'),(191,'zh-CN','新加坡'),(192,'zh-CN','圣赫勒拿、阿森松和特里斯坦-达库尼亚'),(193,'zh-CN','斯洛文尼亚'),(194,'zh-CN','斯瓦尔巴和扬马延'),(195,'zh-CN','斯洛伐克'),(196,'zh-CN','塞拉利昂'),(197,'zh-CN','圣马力诺'),(198,'zh-CN','塞内加尔'),(199,'zh-CN','索马里'),(200,'zh-CN','苏里南'),(201,'zh-CN','南苏丹'),(202,'zh-CN','圣多美和普林西比'),(203,'zh-CN','萨尔瓦多'),(204,'zh-CN','荷属圣马丁'),(205,'zh-CN','叙利亚'),(206,'zh-CN','斯威士兰'),(207,'zh-CN','特克斯和凯科斯群岛'),(208,'zh-CN','乍得'),(209,'zh-CN','多哥'),(210,'zh-CN','泰国'),(211,'zh-CN','塔吉克斯坦'),(212,'zh-CN','托克劳'),(213,'zh-CN','东帝汶'),(214,'zh-CN','土库曼斯坦'),(215,'zh-CN','突尼斯'),(216,'zh-CN','汤加'),(217,'zh-CN','土耳其'),(218,'zh-CN','特立尼达和多巴哥'),(219,'zh-CN','图瓦卢'),(220,'zh-CN','台湾台湾'),(221,'zh-CN','坦桑尼亚'),(222,'zh-CN','乌克兰'),(223,'zh-CN','乌干达'),(224,'zh-CN','美国本土外小岛屿'),(225,'zh-CN','美国'),(226,'zh-CN','乌拉圭'),(227,'zh-CN','乌兹别克斯坦'),(228,'zh-CN','梵蒂冈'),(229,'zh-CN','圣文森特和格林纳丁斯'),(230,'zh-CN','委内瑞拉'),(231,'zh-CN','英属维尔京群岛'),(232,'zh-CN','美属维尔京群岛'),(233,'zh-CN','越南'),(234,'zh-CN','瓦努阿图'),(235,'zh-CN','瓦利斯和富图纳'),(236,'zh-CN','萨摩亚'),(237,'zh-CN','也门'),(238,'zh-CN','马约特'),(239,'zh-CN','南非'),(240,'zh-CN','赞比亚'),(241,'zh-CN','津巴布韦'),
(1,'en-US','Andorra'),(2,'en-US','United Arab Emirates'),(3,'en-US','Afghanistan'),(4,'en-US','Antigua and Barbuda'),(5,'en-US','Anguilla'),(6,'en-US','Albania'),(7,'en-US','Armenia'),(8,'en-US','Angola'),(9,'en-US','Antarctica'),(10,'en-US','Argentina'),(11,'en-US','American Samoa'),(12,'en-US','Austria'),(13,'en-US','Australia'),(14,'en-US','Aruba'),(15,'en-US','Aland Islands'),(16,'en-US','Azerbaijan'),(17,'en-US','Bosnia and Herzegovina'),(18,'en-US','Barbados'),(19,'en-US','Bangladesh'),(20,'en-US','Belgium'),(21,'en-US','Burkina Faso'),(22,'en-US','Bulgaria'),(23,'en-US','Bahrain'),(24,'en-US','Burundi'),(25,'en-US','Benin'),(26,'en-US','Saint Barthelemy'),(27,'en-US','Bermuda'),(28,'en-US','Brunei Darussalam'),(29,'en-US','Bolivia \(Plurinational State of\)'),(30,'en-US','Bonaire, Sint Eustatius and Saba'),(31,'en-US','Brazil'),(32,'en-US','Bahamas'),(33,'en-US','Bhutan'),(34,'en-US','Botswana'),(35,'en-US','Belarus'),(36,'en-US','Belize'),(37,'en-US','Canada'),(38,'en-US','Congo \(Democratic Republic of the\)'),(39,'en-US','Central African Republic'),(40,'en-US','Congo'),(41,'en-US','Switzerland'),(42,'en-US','Cote D\'ivoire'),(43,'en-US','Cook Islands'),(44,'en-US','Chile'),(45,'en-US','Cameroon'),(46,'en-US','China'),(47,'en-US','Colombia'),(48,'en-US','Costa Rica'),(49,'en-US','Cuba'),(50,'en-US','Cabo Verde'),(51,'en-US','Curacao'),(52,'en-US','Cyprus'),(53,'en-US','Czechia'),(54,'en-US','Germany'),(55,'en-US','Djibouti'),(56,'en-US','Denmark'),(57,'en-US','Dominica'),(58,'en-US','Dominican Republic'),(59,'en-US','Algeria'),(60,'en-US','Ecuador'),(61,'en-US','Estonia'),(62,'en-US','Egypt'),(63,'en-US','Eritrea'),(64,'en-US','Spain'),(65,'en-US','Ethiopia'),(66,'en-US','Finland'),(67,'en-US','Fiji'),(68,'en-US','Falkland Islands \(Malvinas\)'),(69,'en-US','Micronesia \(Federated States of\)'),(70,'en-US','Faroe Islands'),(71,'en-US','France'),(72,'en-US','Gabon'),(73,'en-US','United Kingdom of Great Britain and Northern Ireland'),(74,'en-US','Grenada'),(75,'en-US','Georgia'),(76,'en-US','French Guiana'),(77,'en-US','Guernsey'),(78,'en-US','Ghana'),(79,'en-US','Gibraltar'),(80,'en-US','Greenland'),(81,'en-US','Gambia'),(82,'en-US','Guinea'),(83,'en-US','Guadeloupe'),(84,'en-US','Equatorial Guinea'),(85,'en-US','Greece'),(86,'en-US','Guatemala'),(87,'en-US','Guam'),(88,'en-US','Guinea-Bissau'),(89,'en-US','Guyana'),(90,'en-US','Hong Kong'),(91,'en-US','Honduras'),(92,'en-US','Croatia'),(93,'en-US','Haiti'),(94,'en-US','Hungary'),(95,'en-US','Indonesia'),(96,'en-US','Ireland'),(97,'en-US','Israel'),(98,'en-US','Isle of Man'),(99,'en-US','India'),(100,'en-US','British Indian Ocean Territory'),(101,'en-US','Iraq'),(102,'en-US','Iran \(Islamic Republic of\)'),(103,'en-US','Iceland'),(104,'en-US','Italy'),(105,'en-US','Jersey'),(106,'en-US','Jamaica'),(107,'en-US','Jordan'),(108,'en-US','Japan'),(109,'en-US','Kenya'),(110,'en-US','Kyrgyzstan'),(111,'en-US','Cambodia'),(112,'en-US','Kiribati'),(113,'en-US','Comoros'),(114,'en-US','Saint Kitts and Nevis'),(115,'en-US','Korea \(Democratic People\'s Republic of\)'),(116,'en-US','Korea \(Republic of\)'),(117,'en-US','Kuwait'),(118,'en-US','Cayman Islands'),(119,'en-US','Kazakhstan'),(120,'en-US','Lao People\'s Democratic Republic'),(121,'en-US','Lebanon'),(122,'en-US','Saint Lucia'),(123,'en-US','Liechtenstein'),(124,'en-US','Sri Lanka'),(125,'en-US','Liberia'),(126,'en-US','Lesotho'),(127,'en-US','Lithuania'),(128,'en-US','Luxembourg'),(129,'en-US','Latvia'),(130,'en-US','Libya'),(131,'en-US','Morocco'),(132,'en-US','Monaco'),(133,'en-US','Moldova \(Republic of\)'),(134,'en-US','Montenegro'),(135,'en-US','Saint Martin \(French Part\)'),(136,'en-US','Madagascar'),(137,'en-US','Marshall Islands'),(138,'en-US','North Macedonia'),(139,'en-US','Mali'),(140,'en-US','Myanmar'),(141,'en-US','Mongolia'),(142,'en-US','Macao'),(143,'en-US','Northern Mariana Islands'),(144,'en-US','Martinique'),(145,'en-US','Mauritania'),(146,'en-US','Montserrat'),(147,'en-US','Malta'),(148,'en-US','Mauritius'),(149,'en-US','Maldives'),(150,'en-US','Malawi'),(151,'en-US','Mexico'),(152,'en-US','Malaysia'),(153,'en-US','Mozambique'),(154,'en-US','Namibia'),(155,'en-US','New Caledonia'),(156,'en-US','Niger'),(157,'en-US','Norfolk Island'),(158,'en-US','Nigeria'),(159,'en-US','Nicaragua'),(160,'en-US','Netherlands'),(161,'en-US','Norway'),(162,'en-US','Nepal'),(163,'en-US','Nauru'),(164,'en-US','Niue'),(165,'en-US','New Zealand'),(166,'en-US','Oman'),(167,'en-US','Panama'),(168,'en-US','Peru'),(169,'en-US','French Polynesia'),(170,'en-US','Papua New Guinea'),(171,'en-US','Philippines'),(172,'en-US','Pakistan'),(173,'en-US','Poland'),(174,'en-US','Saint Pierre and Miquelon'),(175,'en-US','Puerto Rico'),(176,'en-US','Palestine, State of'),(177,'en-US','Portugal'),(178,'en-US','Palau'),(179,'en-US','Paraguay'),(180,'en-US','Qatar'),(181,'en-US','Reunion'),(182,'en-US','Romania'),(183,'en-US','Serbia'),(184,'en-US','Russian Federation'),(185,'en-US','Rwanda'),(186,'en-US','Saudi Arabia'),(187,'en-US','Solomon Islands'),(188,'en-US','Seychelles'),(189,'en-US','Sudan'),(190,'en-US','Sweden'),(191,'en-US','Singapore'),(192,'en-US','Saint Helena, Ascension and Tristan Da Cunha'),(193,'en-US','Slovenia'),(194,'en-US','Svalbard and Jan Mayen'),(195,'en-US','Slovakia'),(196,'en-US','Sierra Leone'),(197,'en-US','San Marino'),(198,'en-US','Senegal'),(199,'en-US','Somalia'),(200,'en-US','Suriname'),(201,'en-US','South Sudan'),(202,'en-US','Sao Tome and Principe'),(203,'en-US','El Salvador'),(204,'en-US','Sint Maarten \(Dutch Part\)'),(205,'en-US','Syrian Arab Republic'),(206,'en-US','Eswatini'),(207,'en-US','Turks and Caicos Islands'),(208,'en-US','Chad'),(209,'en-US','Togo'),(210,'en-US','Thailand'),(211,'en-US','Tajikistan'),(212,'en-US','Tokelau'),(213,'en-US','Timor-Leste'),(214,'en-US','Turkmenistan'),(215,'en-US','Tunisia'),(216,'en-US','Tonga'),(217,'en-US','Turkey'),(218,'en-US','Trinidad and Tobago'),(219,'en-US','Tuvalu'),(220,'en-US','Taiwan \(Province of China\)'),(221,'en-US','Tanzania, United Republic of'),(222,'en-US','Ukraine'),(223,'en-US','Uganda'),(224,'en-US','United States Minor Outlying Islands'),(225,'en-US','United States of America'),(226,'en-US','Uruguay'),(227,'en-US','Uzbekistan'),(228,'en-US','Holy See'),(229,'en-US','Saint Vincent and The Grenadines'),(230,'en-US','Venezuela \(Bolivarian Republic of\)'),(231,'en-US','Virgin Islands \(British\)'),(232,'en-US','Virgin Islands \(U.S.\)'),(233,'en-US','Viet Nam'),(234,'en-US','Vanuatu'),(235,'en-US','Wallis and Futuna'),(236,'en-US','Samoa'),(237,'en-US','Yemen'),(238,'en-US','Mayotte'),(239,'en-US','South Africa'),(240,'en-US','Zambia'),(241,'en-US','Zimbabwe');

# Payment bank list
INSERT IGNORE INTO `bank` (`name`, `code`)
VALUES
('中国工商银行', 'ICBC'),
('中国农业银行', 'ABC'),
('中国银行', 'BOC'),
('中国建设银行', 'CCB'),
('中国邮政储蓄银行', 'PSBC'),
('交通银行', 'COMM'),
('招商银行', 'CMB'),
('上海浦东发展银行', 'SPDB'),
('兴业银行', 'CIB'),
('华夏银行', 'HXBANK'),
('广东发展银行', 'GDB'),
('中国民生银行', 'CMBC'),
('中信银行', 'CITIC'),
('中国光大银行', 'CEB'),
('恒丰银行', 'EGBANK'),
('浙商银行', 'CZBANK'),
('渤海银行', 'BOHAIB'),
('平安银行', 'SPABANK'),
('上海农村商业银行', 'SHRCB'),
('国家开发银行', 'CDB'),
('玉溪市商业银行', 'YXCCB'),
('尧都农商行', 'YDRCB'),
('北京银行', 'BJBANK'),
('上海银行', 'SHBANK'),
('江苏银行', 'JSBANK'),
('杭州银行', 'HZCB'),
('南京银行', 'NJCB'),
('宁波银行', 'NBBANK'),
('徽商银行', 'HSBANK'),
('长沙银行', 'CSCB'),
('成都银行', 'CDCB'),
('重庆银行', 'CQBANK'),
('大连银行', 'DLB'),
('南昌银行', 'NCB'),
('福建海峡银行', 'FJHXBC'),
('汉口银行', 'HKB'),
('温州银行', 'WZCB'),
('青岛银行', 'QDCCB'),
('台州银行', 'TZCB'),
('嘉兴银行', 'JXBANK'),
('常熟农村商业银行', 'CSRCB'),
('南海农村信用联社', 'NHB'),
('常州农村信用联社', 'CZRCB'),
('内蒙古银行', 'H3CB'),
('绍兴银行', 'SXCB'),
('顺德农商银行', 'SDEB'),
('吴江农商银行', 'WJRCB'),
('齐商银行', 'ZBCB'),
('贵阳市商业银行', 'GYCB'),
('遵义市商业银行', 'ZYCBANK'),
('湖州市商业银行', 'HZCCB'),
('龙江银行', 'DAQINGB'),
('晋城银行JCBANK', 'JINCHB'),
('浙江泰隆商业银行', 'ZJTLCB'),
('广东省农村信用社联合社', 'GDRCC'),
('东莞农村商业银行', 'DRCBCL'),
('浙江民泰商业银行', 'MTBANK'),
('广州银行', 'GCB'),
('辽阳市商业银行', 'LYCB'),
('江苏省农村信用联合社', 'JSRCU'),
('廊坊银行', 'LANGFB'),
('浙江稠州商业银行', 'CZCB'),
('德阳商业银行', 'DYCB'),
('晋中市商业银行', 'JZBANK'),
('苏州银行', 'BOSZ'),
('桂林银行', 'GLBANK'),
('乌鲁木齐市商业银行', 'URMQCCB'),
('成都农商银行', 'CDRCB'),
('张家港农村商业银行', 'ZRCBANK'),
('东莞银行', 'BOD'),
('莱商银行', 'LSBANK'),
('北京农村商业银行', 'BJRCB'),
('天津农商银行', 'TRCB'),
('上饶银行', 'SRBANK'),
('富滇银行', 'FDB'),
('重庆农村商业银行', 'CRCBANK'),
('鞍山银行', 'ASCB'),
('宁夏银行', 'NXBANK'),
('河北银行', 'BHB'),
('华融湘江银行', 'HRXJB'),
('自贡市商业银行', 'ZGCCB'),
('云南省农村信用社', 'YNRCC'),
('吉林银行', 'JLBANK'),
('东营市商业银行', 'DYCCB'),
('昆仑银行', 'KLB'),
('鄂尔多斯银行', 'ORBANK'),
('邢台银行', 'XTB'),
('晋商银行', 'JSB'),
('天津银行', 'TCCB'),
('营口银行', 'BOYK'),
('吉林农信', 'JLRCU'),
('山东农信', 'SDRCU'),
('西安银行', 'XABANK'),
('河北省农村信用社', 'HBRCU'),
('宁夏黄河农村商业银行', 'NXRCU'),
('贵州省农村信用社', 'GZRCU'),
('阜新银行', 'FXCB'),
('湖北银行黄石分行', 'HBHSBANK'),
('浙江省农村信用社联合社', 'ZJNX'),
('新乡银行', 'XXBANK'),
('湖北银行宜昌分行', 'HBYCBANK'),
('乐山市商业银行', 'LSCCB'),
('江苏太仓农村商业银行', 'TCRCB'),
('驻马店银行', 'BZMD'),
('赣州银行', 'GZB'),
('无锡农村商业银行', 'WRCB'),
('广西北部湾银行', 'BGB'),
('广州农商银行', 'GRCB'),
('江苏江阴农村商业银行', 'JRCB'),
('平顶山银行', 'BOP'),
('泰安市商业银行', 'TACCB'),
('南充市商业银行', 'CGNB'),
('重庆三峡银行', 'CCQTGB'),
('中山小榄村镇银行', 'XLBANK'),
('邯郸银行', 'HDBANK'),
('库尔勒市商业银行', 'KORLABANK'),
('锦州银行', 'BOJZ'),
('齐鲁银行', 'QLBANK'),
('青海银行', 'BOQH'),
('阳泉银行', 'YQCCB'),
('盛京银行', 'SJBANK'),
('抚顺银行', 'FSCB'),
('郑州银行', 'ZZBANK'),
('深圳农村商业银行', 'SRCB'),
('潍坊银行', 'BANKWF'),
('九江银行', 'JJBANK'),
('江西省农村信用', 'JXRCU'),
('河南省农村信用', 'HNRCU'),
('甘肃省农村信用', 'GSRCU'),
('四川省农村信用', 'SCRCU'),
('广西省农村信用', 'GXRCU'),
('陕西信合', 'SXRCCU'),
('武汉农村商业银行', 'WHRCB'),
('宜宾市商业银行', 'YBCCB'),
('昆山农村商业银行', 'KSRB'),
('石嘴山银行', 'SZSBK'),
('衡水银行', 'HSBK'),
('信阳银行', 'XYBANK'),
('鄞州银行', 'NBYZ'),
('张家口市商业银行', 'ZJKCCB'),
('许昌银行', 'XCYH'),
('济宁银行', 'JNBANK'),
('开封市商业银行', 'CBKF'),
('威海市商业银行', 'WHCCB'),
('湖北银行', 'HBC'),
('承德银行', 'BOCD'),
('丹东银行', 'BODD'),
('金华银行', 'JHBANK'),
('朝阳银行', 'BOCY'),
('临商银行', 'LSBC'),
('包商银行', 'BSB'),
('兰州银行', 'LZYH'),
('周口银行', 'BOZK'),
('德州银行', 'DZBANK'),
('三门峡银行', 'SCCB'),
('安阳银行', 'AYCB'),
('安徽省农村信用社', 'ARCU'),
('湖北省农村信用社', 'HURCB'),
('湖南省农村信用社', 'HNRCC'),
('广东南粤银行', 'NYNB'),
('洛阳银行', 'LYBANK'),
('农信银清算中心', 'NHQS'),
('城市商业银行资金清算中心', 'CBBQS');

/* 固定提供六個步驟給客戶制定 */
INSERT INTO withdraw_workflow_setting (id, name, enabled, required) VALUES
	(1, 'Created', 1, 1),
	(2, 'Pending Review', 1, 1),
	(3, '', 0, 0),
	(4, '', 0, 0),
	(5, '', 0, 0),
	(6, '', 0, 0),
	(7, '', 0, 0),
	(8, '', 0, 0),
	(9, 'Paying', 1, 1),
	(10, 'Paid', 1, 1),
	(11, 'Rejected', 1, 1),
	(12, 'Cancel', 1, 1);

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
