/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Dump of table game_name
# ------------------------------------------------------------
LOCK TABLES `game_name` WRITE;
/*!40000 ALTER TABLE `game_name` DISABLE KEYS */;

INSERT INTO `game_name` (`id`, `game_api_code`, `game_code`, `game_type_id`, `featured`, `released_date`, `web`, `mobile`, `enabled`, `demo_link`, `created_at`, `updated_at`)
VALUES
	(12380,'T1GAGIN','FT',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12381,'T1GAGIN','ZJH',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12382,'T1GAGIN','NN',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12383,'T1GAGIN','DT',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12384,'T1GAGIN','BAC',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12385,'T1GAGIN','BJ',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12386,'T1GAGIN','SHB',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12387,'T1GAGIN','LBAC',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12388,'T1GAGIN','ULPK',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12389,'T1GAGIN','CBAC',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12390,'T1GAGIN','ROU',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12391,'T1GAGIN','SBAC',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12392,'T1GAGIN','LINK',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(15526,'T1GAGIN','BF',2,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12262,'T1GAGIN','SLM3',1,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(12368,'T1GAGIN','SB45',1,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(14707,'T1GISB','5616',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(24863,'T1GISB','905616',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(27002,'T1GMGPLUS','SMG_LegendOftheMoonLovers',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(14733,'T1GKYCARD','9109101',3,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(20713,'T1GCQ9','1',1,1,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(24868,'T1GISB','909307',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(30645,'T1GMGPLUS','SMG_ladiesNite2TurnWild',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(4684,'T1GPT','gos',1,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(4615,'T1GPT','bj_mh5',3,NULL,NULL,1,0,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514'),
	(4792,'T1GPT','ba',3,NULL,NULL,1,1,1,NULL,'2020-08-03 17:11:03.514','2020-08-03 17:11:03.514');

/*!40000 ALTER TABLE `game_name` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table game_name_i18n
# ------------------------------------------------------------
LOCK TABLES `game_name_i18n` WRITE;
/*!40000 ALTER TABLE `game_name_i18n` DISABLE KEYS */;

INSERT INTO `game_name_i18n` (`id`, `game_name_id`, `locale`, `name`, `created_at`, `updated_at`)
VALUES
	(12380,12380,'en-US','Fan Tan','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12381,12381,'en-US','Win Three Cards','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12382,12382,'en-US','Bull Bull','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12383,12383,'en-US','Dragon Tiger','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12384,12384,'en-US','Baccarat','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12385,12385,'en-US','Blackjack','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12386,12386,'en-US','Sicbo','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12387,12387,'en-US','Bid Baccarat','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12388,12388,'en-US','Ultimate Holdem','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12389,12389,'en-US','VIP Baccarat','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12390,12390,'en-US','Roulette','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12391,12391,'en-US','Insurance Bacarrat','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12392,12392,'en-US','Multiple platform','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(15526,15526,'en-US','Bull Fight','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12262,12262,'en-US','Legend of Warriors','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(12368,12368,'en-US','Legend of the Dragon','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(14707,14707,'en-US','Legend of Loki (Html5)','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(24862,24863,'en-US','Legend of Loki (Html5)','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(27001,27002,'en-US','Legend of the Moon Lovers','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(4684,4684,'en-US','Golden Tour','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(14733,14733,'en-US','Baccarat Fresh Room','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(20712,20713,'en-US','FruitKing','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(24867,24868,'en-US','Scrolls of Ra HD (Html5)','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(30644,30645,'en-US','Ladies Nite 2 Turn Wild','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(4615,4615,'en-US','Blackjack','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151'),
	(4792,4792,'en-US','Baccarat','2020-08-03 17:11:05.151','2020-08-03 17:11:05.151');

/*!40000 ALTER TABLE `game_name_i18n` ENABLE KEYS */;
UNLOCK TABLES;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


# Loads test data into DB
# Test site specifications: Language = en-US, zh-CN, th; Currency = CNY, USD, THB; Game = MockGame, MockGame2, T1

INSERT IGNORE INTO `game_api` (`id`, `code`, `name`, `currency`, `meta`, `md5`, `remark`, `sync_enabled`, `status`, `next_sync_from`)
VALUES
	(1,'MockGame','MockGame CNY','CNY','{\n  \"url\": \"http://api.mockgame.com/service\",\n  \"merchantId\": \"mock_cny\",\n  \"key\": \"123abc\"\n}', '03f11c8823e2cef723a2ebaf21d3a11a', 'Mock Game CNY remark', 0, 0, now()),
    (2,'MockGame2','MockGame2 USD','USD','{\n  \"url\": \"http://api.mockgame.com/service\",\n  \"merchantId\": \"mock_usd\",\n  \"key\": \"123abc\"\n}', 'ee154027bece23ab68f9df1af02ff218', 'Mock Game 2 USD remark', 0, 0, now()),
    (3,'MockGame','BBIN THB','THB','{\n  \"url\": \"http://api.mockgame.com/service\",\n  \"merchantId\": \"mock_thb\",\n  \"key\": \"123abc\"\n}', '50c5d0e8d8d6ec603b5b32d7c8ff2ef4', 'Mock Game BBIN THB remark', 0, 0, now()),
    (4,'MockGame2','PT CNY','CNY','{\n  \"url\": \"http://api.mockgame.com/service\",\n  \"merchantId\": \"mock_thb\",\n  \"key\": \"123abc\"\n}', '882ba13a3fb242a7424cb3d3b89b469a', 'Mock Game PT CNY remark', 0, 0, now()),
    (5,'T1GAGIN','T1GAGIN CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\"\n }', '05ad8cb38d0182095be88be831052a48', NULL, 0, 0, now()),
    (6,'T1GBBIN','T1GBBIN CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\"\n }', '1b55513d6f2d35ba19e05bc1afe3b338', NULL, 0, 0, now()),
    (7,'T1GPRAGMATICPLAY','T1GPRAGMATICPLAY CNY','CNY','{\n   \"url\": \"http://admin.gamegateway-shadow.t1t.in/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', 'e45c6cfc711d5cf13e3c7859a68dfcb0', NULL, 1, 1, now()),
    (8,'T1GKYCARD','T1GKYCARD CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 1, 1, now()),
    (9,'T1GPT','T1GPT CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 1, 1, now()),
    (10,'T1GMGPLUS','T1GMG CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 1, 1, now()),
    (11,'T1GAB','T1GAB CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 0, 0, now()),
    (12,'T1GEBET','T1GEBET CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 0, 0, now()),
    (13,'T1GISB','T1GISB CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n  }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 1, 1, Date_Sub(now(), INTERVAL 10 MINUTE)),
    (14,'T1GONEWORKS','T1GONEWORKS CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 0, 0, now()),
    (15,'T1GCQ9','T1GCQ9 CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\", \"syncSpin\": \"60\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 1, 1, now()),
    (16,'T1GTF','T1GTF CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 0, 0, now()),
    (17,'T1GLOTTERY','T1GLOTTERY CNY','CNY','{\n   \"url\": \"http://admin.gamegateway.t1t.games/gamegateway\",\n   \"merchantCode\": \"testmerchant\",\n   \"securedKey\": \"511fb01b2f801e9c9f89a61f88152641\",\n   \"signKey\": \"813067f02aa371611ce5cd1c409b9387\",\n   \"prefixForUsername\": \"dev\"\n }', '40b01e8e0576d9e93a47ff22887547e4', NULL, 0, 0, now()),
    (18,'T1LOTTERY','T1LOTTERY CNY','CNY','{\n   \"url\": \"https://open-api.lottery-demo.tripleone.tech/gameapi/v1\",\n   \"gameUrl\": \"https://game.lottery-demo.tripleone.tech\",\n   \"merchantCode\": \"d20d236b38d8402ab280ba8c0fbd2c0d\",\n   \"securedKey\": \"04c5ed1cc3c844dc8e4d9eb2e457e229\",\n   \"signKey\": \"ae605debd2b845f9b42137b115a50416\",\n   \"prefixForUsername\": \"\",\n   \"homeLink\": \"\",\n   \"needClose\": \"true\",\n   \"lotteryType\": \"\"\n }', '40b01e8e0576d9e93a47ff22887547e4', 'Direct T1 LOTTERY', 0, 0, now())

ON DUPLICATE KEY UPDATE  `status` = VALUES(`status`);

INSERT IGNORE INTO `game_api_player_account` (`id`, `game_api_id`, `player_id`, `username`, `password_encrypt`, `created_at`, `updated_at`)
VALUES
    (1, 1, 2, 'cnytest002', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '2018-11-08 00:00:00.000', '2018-11-08 06:05:27.289'),
    (2, 2, 3, 'usdtest003', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '2018-11-08 01:00:00.000', '2018-11-08 06:08:17.679'),
    (3, 2, 2, 'cnytest002', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (4, 5, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (5, 6, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (6, 7, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (7, 8, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (8, 9, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (9, 10, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (10, 11, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (11, 12, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (12, 13, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
	(13, 14, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (14, 15, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
    (15, 16, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
	(16, 17, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031'),
	(17, 18, 2, 'test002', '$2a$10$rS17fG/OqbehR1Zl5Adh1euM8LxQ.13aJ6fQR4AuaNr.tkukpzaKy', '2018-11-08 01:00:00.000', '2018-11-08 06:21:42.031');


INSERT IGNORE INTO `payment_api` (`id`, `code`, `name`, `currency`, `meta`, `status`, `daily_max_deposit`, `min_deposit_per_trans`, `max_deposit_per_trans`, `charge_ratio`, `fixed_charge`)
VALUES
  	(1,'MockPay','支付宝','CNY' ,
  		'{\n  \"url\": \"http://api.mockpay.com/service\",\n  \"merchantId\": \"mock_cny\",\n  \"key\": \"123abc\"\n}',
  		0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (2,'MockPay','微信支付','USD' ,
    	'{\n  \"url\": \"http://api.mockpay.com/service\",\n  \"merchantId\": \"mock_usd\",\n  \"key\": \"123abc\"\n}', 
    	0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (3,'MockPay','网银快捷支付','THB' 
    	,'{\n  \"url\": \"http://api.mockpay.com/service\",\n  \"merchantId\": \"mock_thb\",\n  \"key\": \"123abc\"\n}', 
    	0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (4,'PaySecWeChat','PAYSEC微信','CNY' ,
        '{"merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/4",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "WECHAT",
            "requestTokenUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"}', 
            0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (5,'PaySecQuickpay','PAYSEC快捷支付','CNY' ,
        '{"merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/5",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "BANK_TRANSFER",
            "bankCode":"QUICKPAY",
            "requestTokenUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"}', 
            0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (6,'PaySecBankTransfer','PAYSEC银行转账','CNY' ,
        '{
            "merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/6",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "BANK_TRANSFER",
            "requestTokenUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"
        }', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
     (7,'PaySecAlipay','PAYSEC支付宝','CNY' ,
        '{
            "merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/7",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "ALIPAY",
            "requestTokenUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"
        }', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (8,'PaySecQqpay','PAYSEC财付通','CNY' ,
        '{
            "merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/8",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "QQPAY",
            "requestTokenUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"
        }', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (9,'PaySecUnionPayQr','PAYSEC银联扫码','CNY' ,
        '{
            "merchantCode": "838bccf9-5d94-41bc-af9a-76541f46140d",
            "key": "$2a$12$24WTvCAFJSjsT4vc6HUcZO",
            "version": "3.0",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/9",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-requests/return",
            "currency": "CNY",
            "channelCode": "UNIONPAY_QR",
            "requestTokenUrlL":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/requestToken",
            "sendTokenFormUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenForm",
            "sendTokenJsonUrl":"https://pg-staging.paysec.com/Intrapay/paysec/v1/payIn/sendTokenJSON"
        }', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (10,'OnePayBankTransferPay','ONEPAY银行转账','CNY' ,
        '{
            "merchantId": "3683",
            "currency": "CNY",
            "privateKey": "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAK5ZLp/8VOMkGZEZC9fNxIQvFmbteTD2g+f/EoR11vy4l5VJ++NDyhKCxcSD59yEfJbktCxHm+YthECy8ng+111vfA53qQiV2jgJe1E4Wyr8cCGdOSC66j6pfeqk5Z2LxX1PbDu82YH88tRQrlxzNfBD2P227VNirmP6ctAJwtHJAgMBAAECgYBtDyZGN05WQ0em5tbsqC9MTDQkMFoF0b5TSdAogZn4vfM8FGp3D/mAOMNDGQvZehqBPRCjPiv8AO7glc9sfkqygIC9pN53V6yGccK3aWLLRq83dEdDCrSIjCaHl0HO8J0/NzBl1dJNG++ffjSqIKT2PHeu9gkvMnsi5n5M7OcuvQJBAPsNkZFqGLp2UL60RAb6BEEyrT83sKjMvQmIYtAkJQWuaapnfeAFf1aMGJ4jwrXUNXxe2xEOf7aHLlvFfNbSKm8CQQCxyK28L/X+n5ukd/lqV4/FGOjwg3GFDQs4w/sVfEW+6/Im2PXNCnINpvnCCiiA26BOLmW62GfndoB25bSYgkNHAkEAmfPHKbIzOd0mRNwMv26AP28ROKwxBEKRRhBB8DvKDELZf2r5kPAuF2fQDOIHHDPnHL6afkoy3T7mmdZwaNf2twJAc1hYH/ieCY1UVejhEXWt+ZqnxyQAyuojlbjRdqciOTSr7zhkgZt9VA45jQM6NCBKaW8A4bD1+N6RipKOEUSykQJAO7ZCxMNUvA7/7dUUdt0CZRqhk3hH7epGT7LCawx9/w4es269fguFGuw/kDja8RFv7DgrFzwrX+rYEJC/1GmhEQ==",
            "publicKey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDmwwFxxkClvsBzZUtE0CN4S7P0QWZxnpxn2De0zlqbjY6Put/8738SXYkGsuBIb5QZU3tDb/0hmON3zQ84BLexksP2iNqY1q1VSeY2NkV/QxrCUefUedTFsDU+ZcIB5JJ02m4fqpYtzYowtf5JrgjYHcyrO1IaX3NVITm9EPOMHQIDAQAB",
            "requestUrl": "https://api.onepay.solutions/payment/v3/checkOut.html",
            "returnUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/10",
            "notifyUrl": "http://www.t1sbe-1.t1t.games/api/api/payment-callback/10"
        }', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
    (11,'OnePayEwalletQrPay','ONEPAY扫码','CNY' ,
        '{
			"merchantId": "3683",
			"currency": "CNY",
			"privateKey": "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAK5ZLp/8VOMkGZEZC9fNxIQvFmbteTD2g+f/EoR11vy4l5VJ++NDyhKCxcSD59yEfJbktCxHm+YthECy8ng+111vfA53qQiV2jgJe1E4Wyr8cCGdOSC66j6pfeqk5Z2LxX1PbDu82YH88tRQrlxzNfBD2P227VNirmP6ctAJwtHJAgMBAAECgYBtDyZGN05WQ0em5tbsqC9MTDQkMFoF0b5TSdAogZn4vfM8FGp3D/mAOMNDGQvZehqBPRCjPiv8AO7glc9sfkqygIC9pN53V6yGccK3aWLLRq83dEdDCrSIjCaHl0HO8J0/NzBl1dJNG++ffjSqIKT2PHeu9gkvMnsi5n5M7OcuvQJBAPsNkZFqGLp2UL60RAb6BEEyrT83sKjMvQmIYtAkJQWuaapnfeAFf1aMGJ4jwrXUNXxe2xEOf7aHLlvFfNbSKm8CQQCxyK28L/X+n5ukd/lqV4/FGOjwg3GFDQs4w/sVfEW+6/Im2PXNCnINpvnCCiiA26BOLmW62GfndoB25bSYgkNHAkEAmfPHKbIzOd0mRNwMv26AP28ROKwxBEKRRhBB8DvKDELZf2r5kPAuF2fQDOIHHDPnHL6afkoy3T7mmdZwaNf2twJAc1hYH/ieCY1UVejhEXWt+ZqnxyQAyuojlbjRdqciOTSr7zhkgZt9VA45jQM6NCBKaW8A4bD1+N6RipKOEUSykQJAO7ZCxMNUvA7/7dUUdt0CZRqhk3hH7epGT7LCawx9/w4es269fguFGuw/kDja8RFv7DgrFzwrX+rYEJC/1GmhEQ==",
			"publicKey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDmwwFxxkClvsBzZUtE0CN4S7P0QWZxnpxn2De0zlqbjY6Put/8738SXYkGsuBIb5QZU3tDb/0hmON3zQ84BLexksP2iNqY1q1VSeY2NkV/QxrCUefUedTFsDU+ZcIB5JJ02m4fqpYtzYowtf5JrgjYHcyrO1IaX3NVITm9EPOMHQIDAQAB",
			"requestUrl": "https://api.onepay.solutions/payment/otoSoft/v3/getQrCode.html"
		}', 0, 10000.00, 10.00, 1000.00, 1.5, 10),
	(12,'PayBus','Paybus-HiPay-支付宝扫码1','CNY' ,
        '{
			"merchantCode": "0356727fbc6241368785dc2ea74304ae",
			"signKey": "adf3bbdff3434d0d835723048777f64c",
			"requestUrl": "http://35.194.151.151:7701",
			"notifyUrl": "http://www.t1sbe-1.t1t.in/payment-callback/12",
			"channel": "ALI_PAY",
			"platformId": "86",
			"subChannel": "A"
		}', 1, 10000.00, 10.00, 1000.00, 1.5, 10),
	(13,'PayBus','Paybus-HiPay-支付宝扫码2','CNY' ,
        '{
			"merchantCode": "0356727fbc6241368785dc2ea74304ae",
			"signKey": "adf3bbdff3434d0d835723048777f64c",
			"requestUrl": "http://35.194.151.151:7701",
			"notifyUrl": "http://www.t1sbe-1.t1t.in/payment-callback/13",
			"platformId": "86",
			"channel": "ALI_PAY",
			"subChannel": "A"
		}', 1, 10000.00, 10.00, 1000.00, 1.5, 10),
	(14,'PayBus','Paybus-HiPay-網銀','CNY' ,
    '{
		"merchantCode": "0356727fbc6241368785dc2ea74304ae",
		"signKey": "adf3bbdff3434d0d835723048777f64c",
		"requestUrl": "http://35.194.151.151:7701",
		"notifyUrl": "http://www.t1sbe-1.t1t.in/payment-callback/14",
		"platformId": "86",
		"channel": "EBANK",
		"subChannel": "IMB"
	}', 1, 10000.00, 10.00, 1000.00, 1.5, 10),
	(15,'PayBus','Paybus-HiPay-支付宝转卡','CNY' ,
    '{
		"merchantCode": "0356727fbc6241368785dc2ea74304ae",
		"signKey": "adf3bbdff3434d0d835723048777f64c",
		"requestUrl": "http://35.194.151.151:7701",
		"notifyUrl": "http://www.t1sbe-1.t1t.in/payment-callback/15",
		"platformId": "86",
		"channel": "ATB"
	}', 1, 10000.00, 10.00, 1000.00, 1.5, 10);

INSERT IGNORE INTO `withdraw_api` (`id`, `code`, `name`, `currency`, `meta`, `status`)
VALUES
  	(1,'MockWithdraw','Wechat Pay','CNY','{\n  \"url\": \"http://api.mockwithdraw.com/service\",\n  \"notifyUrl\": \"http://admin.t1sbe.local:8000/callback/mockwithdraw\",\n  \"merchantId\": \"mock_cny\",\n  \"key\": \"123abc\"\n}', 1),
    (2,'MockWithdraw','MockWithdraw USD','USD','{\n  \"url\": \"http://api.mockwithdraw.com/service\",\n  \"notifyUrl\": \"http://admin.t1sbe.local:8000/callback/mockwithdraw\",\n  \"merchantId\": \"mock_usd\",\n  \"key\": \"123abc\"\n}', 1),
    (3,'MockWithdraw','MockWithdraw THB','THB','{\n  \"url\": \"http://api.mockwithdraw.com/service\",\n  \"notifyUrl\": \"http://admin.t1sbe.local:8000/callback/mockwithdraw\",\n  \"merchantId\": \"mock_thb\",\n  \"key\": \"123abc\"\n}', 1);

INSERT IGNORE INTO `operator_role` (`id`, `name`, `permission`)
VALUES
    (1, 'ADMIN', '[\"3333333\",\"33\",\"13111111122212\",\"1311111022222\",\"011332333\",\"11\",\"1111\",\"211333333\",\"337\",\"3733\",\"70\",\"17253\",\"37\",\"30\"]'),
    (2, 'OPERATOR', '[\"13\",\"0\",\"13101011122212\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]'),
    (3, 'HELP_DESK', '[\"00\",\"0\",\"13101011122212\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]'),
    (4, 'MARKETING', '[\"00\",\"0\",\"13101011122212\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]'),
    (5, 'FINANCE', '[\"00\",\"0\",\"13101011122212\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]');

# All password = abcdef
INSERT IGNORE INTO `operator` (`id`, `username`, `password_hash`, `role_id`)
VALUES
(1,'superadmin','$2a$10$Xi8srfpb0W.uv76cBzpA8.V.ohGey3kIPA6Mrv66.mJDQePSfcBrS',1),
(2,'admin','$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',2),
(3,'helpdesk','$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',3),
(4,'marketing','$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',4),
(5,'finance','$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',5);

# player_tag
INSERT IGNORE INTO `player_tag` (`id`, `player_group`, `default_tag`, `name`, `automation_job_id`) VALUES
('1', '1', false,'Gold Nova I', 4),
('2', '1', false,'Gold Nova II', NULL),
('3', '1', false,'Gold Nova III', NULL),
('4', '1', false,'Silver I', NULL),
('5', '1', true,'Silver II', NULL),
('6', '0', false,'TestTag', 1),
('7', '0', false,'TestTag2', 2),
('8', '0', false,'TestTag3', NULL);

# Both password = 123456
INSERT IGNORE INTO `player_credential` (`username`, `password_encrypt`, `withdraw_password_hash`,  `referral_code`, `referral_player_credential_id`, `email`, `register_device`, `register_ip`, `verification_question_id`, `verification_answer`, `created_at`, `updated_at`)
VALUES
('test001', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', 'abc1def', NULL, 'test001@tripleonetech.net', 2, '114.32.45.146', 1, 'testVerificationAnswer', now(), now()),
('test002', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', 'ber12e3', 1, 'tripleonetechsg@gmail.com', 2, '114.32.45.146', 1, 'testVerificationAnswer', now(), now()),
('test003', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',  'abc1eee', NULL,'MarilynWBowens@rhyta.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test004', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',  'abc1de1', 3, NULL, 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test005', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',  'a231def', 2, NULL, 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test006', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',  'abc234f', 1, 'admin@tripletwotech.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test007', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK',  'cdc2def', 6, NULL, 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test008', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', 'dbc2def', 6, 'superadmin@tripletwotech.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('test009', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', 'ebc3def', 6, 'msloan@comcast.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('test010', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', 'ggc4def', 3, 'dkasak@outlook.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('test011', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', '12355de', 2, 'mschilli@yahoo.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test012', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', '$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK', '456d2ef', 1, 'donev@att.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test013', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '789d2ef', NULL, 'mddallara@verizon.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test014', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '654d3ef', NULL, 'ajlitt@yahoo.ca', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test015', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '876d3ef', NULL, 'dodong@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test016', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '1114def', NULL, 'mbrown@att.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test017', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '2222def', NULL, 'jmmuller@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test018', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', NULL, '3332def', NULL, 'bescoto@yahoo.ca', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test019', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '4443def', NULL, 'a@a.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test020', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '55s5def', NULL, 'conteb@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test021', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', NULL, '66s6def', NULL, 'conteb@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test022', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '77e7def', NULL, 'conteb@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test023', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '88e8def', NULL, 'conteb@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test024', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, '9e99def', NULL, 'bescoto@yahoo.ca', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('test025', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'de1f111', NULL, 'bescoto@yahoo.ca', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test026', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'abc1222', NULL, 'bescoto@yahoo.ca', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test027', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab4c333', NULL, 'jmmuller@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('test028', 'eb151df7ce96baed55e55b69001216231dfa7b569fb5f8704904d7dbd08f71d4', NULL, 'a6bc444', NULL, 'jmmuller@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('test029', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'a7bc555', NULL, 'jmmuller@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('abc030', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'abc4666', NULL, 'mbrown@att.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
('abc031', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab2c777', NULL, 'mbrown@att.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', now(), now()),
('abc032', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab2cbe2', NULL, 'mbrown@att.net', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('abc033', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab2c3ew', NULL, 'dodong@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('abc034', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab2c3e2', NULL, 'dodong@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
('superadmin', '2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e', NULL, 'ab32c34', NULL, 'dodong@mac.com', 2, '114.32.45.146', 2, 'testVerificationAnswer', DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour));

-- Set one player with global status = Disabled
UPDATE player_credential SET global_status = 0 WHERE username = 'test018';

INSERT IGNORE INTO `player` (`id`, `username`, `status`, `currency`, `last_login_time`, `last_login_device`, `last_login_ip`, `risk`, `created_at`, `updated_at`)
VALUES
(1, 'test001', 1, 'CNY', now(), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(2, 'test002', 1, 'CNY', now(), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(3, 'test003', 1, 'CNY', now(), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(4, 'test002', 1, 'USD', now(), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(5, 'test004', 1, 'CNY', now(), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(6, 'test005', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), DATE_ADD(now(), interval 8 hour)),
(7, 'test006', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), DATE_ADD(now(), interval 8 hour)),
(8, 'test007', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), DATE_ADD(now(), interval 8 hour)),
(9, 'test008', 1, 'CNY', DATE_ADD(now(), interval -8 hour), 2, '114.32.45.146', 5, DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
(10, 'test009', 1, 'USD', DATE_ADD(now(), interval -8 hour), 2, '114.32.45.146', 5, DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
(11, 'test010', 1, 'USD', DATE_ADD(now(), interval -8 hour), 2, '114.32.45.146', 1, DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval -8 hour)),
(12, 'test011', 1, 'USD', now(), 2, '114.32.45.146', 1, now(), now()),
(13, 'test012', 1, 'USD', now(), 2, '114.32.45.146', 9, now(), now()),
(14, 'test013', 1, 'CNY', now(), 2, '114.32.45.146', 9, now(), now()),
(15, 'test014', 1, 'USD', now(), 2, '114.32.45.146', 9, now(), now()),
(16, 'test015', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
(17, 'test016', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
(18, 'test017', 1, 'CNY', DATE_ADD(now(), interval 8 hour), 2, '114.32.45.146', 1, DATE_ADD(now(), interval 8 hour), DATE_ADD(now(), interval 8 hour)),
(19, 'test018', 1, 'CNY', now(), 2, '114.32.45.146', 1, now(), now()),
(20, 'test019', 1, 'CNY', now(), 2, '114.32.45.146', 1, now(), now()),
(74,'test001',1 ,'USD', DATE_ADD(now(), interval 1 hour), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(75,'test001',1 ,'THB', DATE_ADD(now(), interval 2 hour), 2, '114.32.45.146', 1, DATE_SUB(now(), INTERVAL 30 DAY), now()),
(22,'test019', 1, 'THB', now(), 2, '114.32.45.146', 1, now(), now());

# create player with all currency but status is CURRENCY_INACTIVE:9
INSERT INTO `player` (`id`,`username`,`currency`,`status`,`cashback_enabled`,`campaign_enabled`) VALUES
(21,'test019','USD',9,1,1)
,(23,'test019','IDR',9,1,1)
,(24,'test018','USD',9,1,1)
,(25,'test018','THB',9,1,1)
,(26,'test018','IDR',9,1,1)
,(27,'test017','USD',9,1,1)
,(28,'test017','THB',9,1,1)
,(29,'test017','IDR',9,1,1)
,(30,'test016','USD',9,1,1)
,(31,'test016','THB',9,1,1)
,(32,'test016','IDR',9,1,1)
,(33,'test015','USD',9,1,1)
,(34,'test015','THB',9,1,1)
,(35,'test015','IDR',9,1,1)
,(36,'test014','CNY',9,1,1)
,(37,'test014','THB',9,1,1)
,(38,'test014','IDR',9,1,1)
,(39,'test013','USD',9,1,1)
,(40,'test013','THB',9,1,1)
,(41,'test013','IDR',9,1,1)
,(42,'test012','CNY',9,1,1)
,(43,'test012','THB',9,1,1)
,(44,'test012','IDR',9,1,1)
,(45,'test011','CNY',9,1,1)
,(46,'test011','THB',9,1,1)
,(47,'test011','IDR',9,1,1)
,(48,'test010','CNY',9,1,1)
,(49,'test010','THB',9,1,1)
,(50,'test010','IDR',9,1,1)
,(51,'test009','CNY',9,1,1)
,(52,'test009','THB',9,1,1)
,(53,'test009','IDR',9,1,1)
,(54,'test008','USD',9,1,1)
,(55,'test008','THB',9,1,1)
,(56,'test008','IDR',9,1,1)
,(57,'test007','USD',9,1,1)
,(58,'test007','THB',9,1,1)
,(59,'test007','IDR',9,1,1)
,(60,'test006','USD',9,1,1)
,(61,'test006','THB',9,1,1)
,(62,'test006','IDR',9,1,1)
,(63,'test005','USD',9,1,1)
,(64,'test005','THB',9,1,1)
,(65,'test005','IDR',9,1,1)
,(66,'test004','USD',9,1,1)
,(67,'test004','THB',9,1,1)
,(68,'test004','IDR',9,1,1)
,(69,'test003','USD',9,1,1)
,(70,'test003','THB',9,1,1)
,(71,'test003','IDR',9,1,1)
,(72,'test002','THB',0,1,1)
,(73,'test002','IDR',9,1,1)
,(76,'test001','IDR',9,1,1);

# players_player_tag
INSERT IGNORE INTO players_player_tag(player_id, player_tag_id) values
(1, 1), (1, 6), (1, 7), (1, 8),
(2, 1), (2, 6), (2, 8),
(3, 1),
(4, 1), (4, 8),
(5, 1), (5, 7),
(6, 1),
(7, 1),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 1),
(19, 1);

#Player Profile
INSERT IGNORE INTO `player_profile` (`id`, `player_id`, `first_name`, `last_name`, `birthday`, `language`, `gender`, `address`, `city`, `country_code`, `country_phone_code`, `line`, `skype`, `qq`, `wechat`, `phone_number`, `contact_preferences`)
VALUES
    (1, 1, '三儿', '张', '1980-01-01', 'zh-CN', 'M', '颐和园路5号', '北京', 'CHN', '63',  'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', '98765432', 1),
    (2, 2, '晨', '李', '1981-02-02', 'zh-CN', 'M', '皮条胡同老拉家', '北京', 'CHN', '63', 'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', '13806000000', 3),
    (3, 8, '思睿', '张', '1983-03-03', 'zh-CN', 'M', '于家大鸡窝路', '大兴', 'CHN', '86', 'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', '865134518235', 1),
    (4, 6, '洪涛', '刘', '1993-03-03', 'zh-CN', 'M', '天安门广场', '北京', 'CHN', '86', 'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', '865134512342', 1),
    (5, 4, 'John', 'Green', '1992-02-02', 'en-US', 'M', 'Bandar Udara I Gusti Ngurah Rai, Tuban, Kuta, Tuban, Kuta, Kabupaten Badung', 'Bali', 'IDN', '62', 'T1devLineUSD', 'T1DevSkypeUSD', '3823871732', 'T1DevWechatUSD', null, 1),
    (6, 5, 'John', 'Wilson', '1991-02-02', 'en-US', 'M', 'Bandar Udara I Gusti Ngurah Rai', 'Bali', 'IDN', '62', 'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', null, 1),
    (7, 7, 'John', 'Davis', '1990-01-02', 'en-US', 'M', 'Jimbaran, South Kuta, Badung Regency', 'Bali', 'IDN', '62', 'T1devLine', 'T1DevSkype', '1231234123', 'T1DevWechat', null, 3);

#Player Profile (DEMO)
INSERT INTO player_profile (player_id,first_name,last_name,birthday,`language`,gender,address,city,country_code,country_phone_code,phone_number) VALUES
('3','Joseph','Hawkins','1970-08-27','en-US','M','268 Phetchaburi Rd, Thanon Phetchaburi','Bangkok','THA','66','281-555-7019'),
('9','三儿','张','1985-10-30','zh-CN','M','Unit 3-5 Caldevale Works, River St, Brighouse, HD6 1NL','Bangkok','THA','66','203-610-1399'),
('10','六郎','黄','1995-04-18','zh-CN','M','Ebenezer Hall, Cedar Rd, Cobham, KT11 2AA','Bangkok','THA','66','303-475-5490'),
('11','四','赵','1983-04-21','zh-CN','M','Holt Rd, Sheringham, NR26 8TU ','Wevelbeek','VNM','84','757-855-3417'),
('12','二','王','1995-03-20','zh-CN','M','83 Featherstall Rd North, Oldham, OL9 6QB','Laagport','VNM','84','210-778-2695'),
('13','Connor','Stenson','1989-06-02','en-US','M','89 Park Rd, Cowes, PO31 7NE','Beaulon','VNM','84','256-646-0373'),
('14','Tony','Baker','1993-10-04','en-US','M','14 California Close, Sutton, SM2 6DQ','Vierroux','VNM','84','540-589-6815'),
('15','Mason','Briggs','1983-07-23','en-US','M','Wakefield Rd, Barnsley, S71 1NH','Cloppenland','VNM','84','845-493-8174'),
('16','Leland','Duffy','1993-04-16','en-US','M','12 Trevanion Road, London, W14 9BJ','Penzrode','VNM','84','850-232-2077'),
('17','Aidan','Nelson','1995-12-24','en-US','M','Butts Works, Billingshurst, RH14 0BN','Bangkok','THA','66','740-721-8547'),
('18','Harrison','Stenson','1990-04-26','en-US','M','14-15 Bethel Square, BRECON, LD3 7JP','Bangkok','THA','66','940-435-6561'),
('19','Caroline','Middleton','1985-03-13','en-US','F','Prestige House, 12, Bowlalley Lane, Hull, HU1 1XR','Bangkok','THA','66','732-790-9342'),
('20','Monique','Graham','1987-06-02','en-US','F','Brynford Hill, Holywell, CH8 8AD','Bangkok','THA','66','630-887-7717');

# create player with all currency but status is CURRENCY_INACTIVE:9
INSERT INTO `player_profile` (`player_id`) VALUES
(21),(22),(23),
(24),(25),(26),
(27),(28),(29),
(30),(31),(32),
(33),(34),(35),
(36),(37),(38),
(39),(40),(41),
(42),(43),(44),
(45),(46),(47),
(48),(49),(50),
(51),(52),(53),
(54),(55),(56),
(57),(58),(59),
(60),(61),(62),
(63),(64),(65),
(66),(67),(68),
(69),(70),(71),
(72),(73),
(74),(75),(76);

INSERT INTO player_verification_question_i18n(`question_id`, `locale`, `question`) VALUES
(1, 'en-US', 'What is your favorite food?'),(1, 'zh-CN', '最喜歡的食物？'),
(2, 'en-US', 'What is your favorite color?'),(2, 'zh-CN', '最喜歡的顏色？'),
(3, 'en-US', 'What is your favorite song?'),(3, 'zh-CN', '最喜歡的歌？'),
(4, 'en-US', 'What is your favorite sport?'),
(5, 'en-US', 'What is your favorite country?'),(5, 'zh-CN', '最喜歡的國家？');

INSERT IGNORE INTO `wallet` (`player_id`, `game_api_id`, `balance`)
VALUES
    (1, NULL, 9834),
    (1, 1, 330),
    (1, 4, 593),
    (1, 8, 0),
    (1, 9, 0),
    (1, 10, 0),
    (1, 11, 0),
    (1, 12, 0),
    (1, 13, 0),
    (1, 14, 0),

    (2, NULL, 327),
    (2, 1, 150), (2, 2, 0), (2, 3, 0), (2, 4, 0),
    (2, 5, 0), (2, 6, 0), (2, 7, 0), (2, 8, 0),
    (2, 9, 0), (2, 10, 0), (2, 11, 0),
    (2, 12, 0), (2, 13, 0), (2, 14, 0), (2, 15, 0),
	(2, 16, 0), (2, 17, 0), (2, 18, 0),


    (3, NULL, 3827),
    (3, 9, 0),
    (3, 13, 0),
    (3, 15, 0),

    (4, NULL, 6739),

    (5, NULL, 500),
    (5, 1, 150),
    (5, 2, 130),
    (6, NULL, 600),
    (7, NULL, 700),
    (8, NULL, 800),
    (9, NULL, 500),
    (10, NULL, 500),
    (11, NULL, 500),
    (12, NULL, 400),
    (13, NULL, 300),
    (14, NULL, 400),
    (15, NULL, 500),
    (16, NULL, 600),
    (17, NULL, 700),
    (18, NULL, 800),
    (19, NULL, 300),
    (20, NULL, 500),
    (60, NULL, 700);

# Matches the pending amount of withdraw request
UPDATE `wallet` SET `pending_wallet` = 300 WHERE player_id = 2 AND game_api_id IS NULL;

INSERT INTO `wallet` (`player_id`,`game_api_id`,`balance`,`pending_wallet`) VALUES

(36,NULL,0.000,0.000), (36,1,0.000,0.000), (36,4,0.000,0.000), (36,8,0.000,0.000), (36,9,0.000,0.000), (36,10,0.000,0.000), (36,11,0.000,0.000), (36,12,0.000,0.000), (36,13,0.000,0.000), (36,14,0.000,0.000),
(42,NULL,0.000,0.000), (42,1,0.000,0.000), (42,4,0.000,0.000), (42,8,0.000,0.000), (42,9,0.000,0.000), (42,10,0.000,0.000), (42,11,0.000,0.000), (42,12,0.000,0.000), (42,13,0.000,0.000), (42,14,0.000,0.000),
(45,NULL,0.000,0.000), (45,1,0.000,0.000), (45,4,0.000,0.000), (45,8,0.000,0.000), (45,9,0.000,0.000), (45,10,0.000,0.000), (45,11,0.000,0.000), (45,12,0.000,0.000), (45,13,0.000,0.000), (45,14,0.000,0.000),
(48,NULL,0.000,0.000), (48,1,0.000,0.000), (48,4,0.000,0.000), (48,8,0.000,0.000), (48,9,0.000,0.000), (48,10,0.000,0.000), (48,11,0.000,0.000), (48,12,0.000,0.000), (48,13,0.000,0.000), (48,14,0.000,0.000),
(51,NULL,0.000,0.000), (51,1,0.000,0.000), (51,4,0.000,0.000), (51,8,0.000,0.000), (51,9,0.000,0.000), (51,10,0.000,0.000), (51,11,0.000,0.000), (51,12,0.000,0.000), (51,13,0.000,0.000), (51,14,0.000,0.000);


UPDATE `wallet` SET clean_date = DATE_SUB(NOW(), INTERVAL RAND() * 3600 SECOND);

INSERT IGNORE INTO `payment_method` (`id`, `name`, `type`, `currency`, `daily_max_deposit`, `min_deposit_per_trans`, `max_deposit_per_trans`, 
	`enabled`, `payment_api_id`, `note`, `charge_ratio`, `fixed_charge`, `player_bear_payment_charge`) VALUES
    (1, '人工转帐', 1, 'CNY', 10000, 10, 1000, 1, NULL, '转帐银行： 中国工商银行上海市分行 \n转帐帐户： 1540-3241-1524-0063\n收款人：王二\n\n转帐完毕之后，请将订单编号附加于转帐备注', 0, 0, 0),
    (2, '网银支付', 1, 'CNY', 10000, 10, 1000, 1, 10, NULL, 0, 0, 0),
    (3, '银联扫码', 2, 'CNY', 10000, 10, 1000, 1, 10, NULL, 0, 3, 0),
    (4, '微信扫码', 3, 'CNY', 10000, 10, 1000, 1, 10, NULL, 1.5, 10, 1),
    (5, '支付宝', 4, 'CNY', 10000, 10, 1000, 1, 12, NULL, 1, 10, 1),
   	(12, '网银支付', 1, 'CNY', 10000, 10, 1000, 1, 14, NULL, 0, 15, 0),

    (6, 'Manual Deposit', 1, 'USD', 10000, 10, 1000, 1, NULL, '转帐银行： 中国工商银行上海市分行 \n转帐帐户： 1540-3241-1524-0063\n收款人：王二\n\n转帐完毕之后，请将订单编号附加于转帐备注', 0, 1, 0),
    (7, 'ALIPAY', 4, 'USD', 10000, 10, 1000, 1, 2, NULL, 1.5, 10, 0),
    (8, 'ONLINE BANKING', 1, 'USD', 10000, 10, 1000, 1, 2, NULL, 0, 0, 0),
    (9, 'WECHAT PAY', 3, 'USD', 10000, 10, 1000, 1, 2, NULL, 1.5, 10, 1),
    
    (10, 'Manual Deposit', 1, 'THB', 10000, 10, 1000, 1, NULL, '转帐银行： 中国工商银行上海市分行 \n转帐帐户： 1540-3241-1524-0063\n收款人：王二\n\n转帐完毕之后，请将订单编号附加于转帐备注', 0, 5, 0),
    (11, 'ALIPAY', 4, 'THB', 10000, 10, 1000, 1, 3, NULL, 1.5, 10, 0),
    (13, 'WECHAT PAY', 3, 'THB', 10000, 10, 1000, 1, 3, NULL, 1.5, 10, 1),
    
    (14, 'Manual Deposit', 1, 'IDR', 10000, 10, 1000, 1, NULL, '转帐银行： 中国工商银行上海市分行 \n转帐帐户： 1540-3241-1524-0063\n收款人：王二\n\n转帐完毕之后，请将订单编号附加于转帐备注', 0, 1, 0);

# id = 1: groups and tags; id = 2: groups only; id = 3: tags only; id = 4: tags but not effective due to player id
INSERT IGNORE INTO `payment_method_player_tag` (`payment_method_id`, `player_tag_id`) VALUES
    (1, 1), (1, 2), (1, 3), (1, 6),
    (2, 1), (2, 2), (2, 3), (2, 4),
    (3, 6), (3, 7);

# id = 4,5,12: players only
INSERT IGNORE INTO `payment_method_player` (`payment_method_id`, `player_id`) VALUES
    (12, 2), (12, 1), 
    (5, 2), (5, 1), 
    (4, 5);

# deposit_request (Demo data is provided for CNY, USD, THB ONLY)
	#CNY : Payment_method_id (1=人工转帐, 2=网银支付, 3=银联扫码)
INSERT IGNORE INTO deposit_request (id, player_id, payment_method_id, amount, `status`, expiration_date, requested_date, external_uid)
VALUES
    (100001 , 1, 2, 1000, 0, '2018-09-20 17:33:00', '2018-09-20 14:33:00', 'onlinebank-external-uid-123456'),
    (100002 , 1, 1, 100000, 0, DATE_ADD(now(), interval 1 hour), now(), 'large-order-external-uid-999000'),
    (100003 , 1, 3, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), 'unionpay-external-uid-123456'),
    (100004 , 1, 2, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), 'onlinebank-external-uid-123457'),
    (100005 , 2, 1, 500, 0, DATE_ADD(now(), interval 3 hour), DATE_SUB(now(), INTERVAL 2 HOUR), NULL),
    (100006 , 2, 3, 200, 11, DATE_ADD(now(), interval 3 hour), now(), 'unionpay-external-uid-123457'),
    (100007 , 3, 1, 100, 0, DATE_ADD(now(), interval 3 hour), now(), NULL),
    (100008 , 3, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY), NULL),
    (100009 , 5, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY), NULL),
    (100010 , 6, 3, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 36 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 2 DAY), 'unionpay-external-uid-123455'),
    (100011 , 7, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY), NULL),
    (100012 , 6, 1, 100, 1, DATE_ADD(now(), interval 3 hour), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY), NULL),
    (111132 , 1, 12, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 1 DAY), 'onlinebank-external-uid-123458'),
    #expired case
    (131145 , 2, 12, 200, 0, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:30:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:30:00'), INTERVAL 1 DAY), 'onlinebank-external-uid-123458'),
    #not expired case 
    (131146 , 2, 12, 200, 0, DATE_ADD(now(), interval 1 hour), DATE_SUB(now(), interval 1 hour),'onlinebank-external-uid-123458'),
    #old case
    (131147 , 2, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 4 DAY), NULL),
    (131148 , 2, 3, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 36 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 5 DAY), 'unionpay-external-uid-123555'),

    (111133 , 3, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY), NULL),
    (111134 , 5, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 3 DAY), NULL),
    (111135 , 6, 3, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 36 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 3 DAY), 'unionpay-external-uid-123455'),
    (111136 , 7, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), NULL),
    (111137 , 3, 1, 200, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:30:00'), INTERVAL 3 DAY), NULL),
    (111138 , 3, 1, 300, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:40:00'), INTERVAL 3 DAY), NULL),

    (111139 , 3, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY), NULL),
    (111140 , 5, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 4 DAY), NULL),
    (111141 , 6, 3, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 36 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:30:00'), INTERVAL 4 DAY), 'unionpay-external-uid-123455'),
    (111142 , 7, 1, 100, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), NULL),
    (111143 , 3, 1, 200, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:30:00'), INTERVAL 4 DAY), NULL),
    (111144 , 3, 1, 300, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:40:00'), INTERVAL 4 DAY), NULL);


    #USD :
INSERT IGNORE INTO deposit_request (player_id, payment_method_id, amount, status, expiration_date, requested_date, external_uid)
VALUES
	#test001 Payment_method_id (7 = ALIPAY - USD)
	(74, 7, 100, 10, DATE_ADD(now(), INTERVAL 1 DAY), DATE_SUB(now(), INTERVAL 1 DAY), 'alipay-external-uid-11111'),
    (74, 7, 100, 10, DATE_ADD(now(), INTERVAL 6 HOUR), DATE_SUB(now(), INTERVAL 6 HOUR), 'alipay-external-uid-22222'),
    (74, 7, 100, 0, DATE_ADD(now(), INTERVAL 3 HOUR), DATE_SUB(now(), INTERVAL 3 HOUR), 'alipay-external-uid-33333'),

    #test002 Payment_method_id (8 = ONLINE-BANK - USD)
	(4, 8, 100, 10, DATE_ADD(now(), INTERVAL 1 DAY), DATE_SUB(now(), INTERVAL 1 DAY), 'bank-external-uid-211111'),
    (4, 8, 200, 10, DATE_ADD(now(), INTERVAL 6 HOUR), DATE_SUB(now(), INTERVAL 6 HOUR), 'bank-external-uid-322222'),
    (4, 8, 100, 0, DATE_ADD(now(), INTERVAL 3 HOUR), DATE_SUB(now(), INTERVAL 3 HOUR), 'bank-external-uid-433333');

#THB :
INSERT IGNORE INTO deposit_request (player_id, payment_method_id, amount, status, expiration_date, requested_date, external_uid)
VALUES
	#test001 Payment_method_id  (13 = ONLIN-BANK PAY - THB)
	(75, 12, 1000, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 1 DAY), 'bank-external-uid-11111'),
    (75, 12, 300, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:30:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:30:00'), INTERVAL 1 DAY), 'bank-external-uid-22222'),
    (75, 12, 500, 0, DATE_ADD(now(), INTERVAL 3 HOUR), DATE_SUB(now(), INTERVAL 3 HOUR), 'bank-external-uid-33333'),

    #test019 Payment_method_id  (10 = Manual - THB)
	(22, 10, 200, 1, DATE_ADD(now(), INTERVAL 1 DAY), DATE_SUB(now(), INTERVAL 1 DAY), 'atm-external-uid-111119'),
    (22, 10, 300, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 36 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY), 'atm-external-uid-222229'),
    (22, 10, 500, 1, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 12 HOUR), DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 1 DAY), 'atm-external-uid-333339');


INSERT INTO deposit_summary_report (time_hour, currency, deposit_count, depositor_count, deposit_amount, dirty) VALUES
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100.000, 1),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), 'CNY', 2, 1, 200.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 200.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 2 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 1 DAY), 'THB', 2, 1, 1300.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY), 'THB', 1, 1, 300.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 1 DAY), 'THB', 1, 1, 500.000, 0),

	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 100.000, 1),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY), 'CNY', 1, 3, 600.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 100.000, 0),

	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 100.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 100.000, 1),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY), 'CNY', 1, 3, 600.000, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 100.000, 0);

INSERT INTO `player_summary_report` (`time_hour`,`total_count`,`new_count`,`device`) VALUES
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 10 DAY),2,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 10 DAY),2,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 10 DAY),1,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 10 DAY),2,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 10 DAY),1,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 10 DAY),4,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 10 DAY),4,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 10 DAY),5,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 10 DAY),6,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 10 DAY),4,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 10 DAY),2,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 10 DAY),4,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 10 DAY),3,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 10 DAY),7,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 10 DAY),4,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 10 DAY),8,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 10 DAY),8,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 10 DAY),5,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 10 DAY),6,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 10 DAY),9,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 10 DAY),5,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 10 DAY),7,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 10 DAY),7,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 10 DAY),5,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 10 DAY),9,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 10 DAY),9,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 10 DAY),6,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 10 DAY),10,4,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 10 DAY),11,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 10 DAY),10,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 10 DAY),10,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 10 DAY),11,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 10 DAY),12,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 10 DAY),12,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 10 DAY),13,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 10 DAY),12,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 10 DAY),13,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 10 DAY),8,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 10 DAY),14,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 10 DAY),13,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 10 DAY),15,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 10 DAY),11,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 10 DAY),14,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 10 DAY),11,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 10 DAY),13,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 10 DAY),16,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 10 DAY),16,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 10 DAY),14,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 9 DAY),17,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 9 DAY),14,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 9 DAY),16,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 9 DAY),16,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 9 DAY),14,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 9 DAY),18,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 9 DAY),17,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 9 DAY),16,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 9 DAY),18,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 9 DAY),17,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 9 DAY),19,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 9 DAY),18,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 9 DAY),19,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 9 DAY),20,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 9 DAY),18,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 9 DAY),19,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 9 DAY),20,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 9 DAY),21,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 9 DAY),20,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 9 DAY),22,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 9 DAY),21,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 9 DAY),23,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 9 DAY),22,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 9 DAY),21,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 9 DAY),20,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 9 DAY),25,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 9 DAY),23,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 9 DAY),24,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 9 DAY),22,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 9 DAY),24,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 9 DAY),26,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 9 DAY),20,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 9 DAY),27,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 9 DAY),26,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 9 DAY),22,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 9 DAY),23,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 9 DAY),28,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 9 DAY),26,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 9 DAY),27,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 9 DAY),28,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 9 DAY),29,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 9 DAY),23,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 9 DAY),26,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 9 DAY),30,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 9 DAY),31,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 9 DAY),29,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 9 DAY),28,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 9 DAY),29,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 9 DAY),31,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 9 DAY),24,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 9 DAY),31,3,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 9 DAY),31,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 9 DAY),33,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 9 DAY),33,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 9 DAY),32,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 8 DAY),25,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 8 DAY),32,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 8 DAY),33,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 8 DAY),34,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 8 DAY),26,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 8 DAY),35,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 8 DAY),34,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 8 DAY),36,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 8 DAY),37,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 8 DAY),37,4,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 8 DAY),37,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 8 DAY),35,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 8 DAY),27,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 8 DAY),29,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 8 DAY),38,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 8 DAY),37,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 8 DAY),38,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 8 DAY),38,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 8 DAY),38,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 8 DAY),40,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 8 DAY),39,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 8 DAY),41,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 8 DAY),30,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 8 DAY),33,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 8 DAY),40,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 8 DAY),41,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 8 DAY),43,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 8 DAY),39,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 8 DAY),44,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 8 DAY),42,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 8 DAY),45,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 8 DAY),41,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 8 DAY),40,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 8 DAY),41,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 8 DAY),43,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 8 DAY),43,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 8 DAY),46,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 8 DAY),45,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 8 DAY),38,5,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 8 DAY),47,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 8 DAY),43,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 8 DAY),46,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 8 DAY),46,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 8 DAY),39,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 8 DAY),48,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 8 DAY),46,3,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 8 DAY),47,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 8 DAY),49,3,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 8 DAY),49,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 8 DAY),40,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 8 DAY),48,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 8 DAY),50,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 8 DAY),50,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 8 DAY),48,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 8 DAY),41,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 8 DAY),50,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 8 DAY),51,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 7 DAY),42,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 7 DAY),51,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 7 DAY),51,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 7 DAY),52,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 7 DAY),52,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 7 DAY),53,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 7 DAY),43,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 7 DAY),52,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 7 DAY),44,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 7 DAY),53,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 7 DAY),54,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 7 DAY),54,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 7 DAY),51,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 7 DAY),52,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 7 DAY),54,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 7 DAY),55,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 7 DAY),54,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 7 DAY),56,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 7 DAY),45,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 7 DAY),55,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 7 DAY),48,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 7 DAY),57,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 7 DAY),58,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 7 DAY),55,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 7 DAY),56,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 7 DAY),57,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 7 DAY),59,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 7 DAY),56,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 7 DAY),49,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 7 DAY),59,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 7 DAY),50,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 7 DAY),60,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 7 DAY),57,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 7 DAY),60,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 7 DAY),51,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 7 DAY),57,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 7 DAY),52,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 7 DAY),61,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 7 DAY),58,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 7 DAY),58,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 7 DAY),53,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 7 DAY),54,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 7 DAY),64,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 7 DAY),60,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 7 DAY),61,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 7 DAY),65,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 7 DAY),59,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 7 DAY),55,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 7 DAY),61,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 7 DAY),61,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 7 DAY),57,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 7 DAY),62,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 7 DAY),63,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 7 DAY),63,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 7 DAY),66,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 7 DAY),60,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 7 DAY),62,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 7 DAY),67,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 7 DAY),65,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 7 DAY),64,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 7 DAY),67,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 7 DAY),63,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 7 DAY),63,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 7 DAY),68,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 7 DAY),65,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 6 DAY),66,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 6 DAY),64,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 6 DAY),65,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 6 DAY),70,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 6 DAY),69,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 6 DAY),71,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 6 DAY),72,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 6 DAY),70,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 6 DAY),71,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 6 DAY),65,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 6 DAY),73,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 6 DAY),72,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 6 DAY),66,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 6 DAY),68,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 6 DAY),74,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 6 DAY),73,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 6 DAY),66,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 6 DAY),67,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 6 DAY),76,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 6 DAY),67,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 6 DAY),77,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 6 DAY),69,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 6 DAY),75,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 6 DAY),69,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 6 DAY),70,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 6 DAY),80,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 6 DAY),68,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 6 DAY),70,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 6 DAY),76,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 6 DAY),69,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 6 DAY),72,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 6 DAY),81,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 6 DAY),71,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 6 DAY),77,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 6 DAY),73,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 6 DAY),70,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 6 DAY),82,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 6 DAY),74,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 6 DAY),73,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 6 DAY),83,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 6 DAY),74,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 6 DAY),72,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 6 DAY),84,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 6 DAY),74,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 6 DAY),77,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 6 DAY),85,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 6 DAY),78,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 6 DAY),75,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 5 DAY),80,5,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 5 DAY),88,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 5 DAY),79,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 5 DAY),75,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 5 DAY),80,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 5 DAY),82,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 5 DAY),81,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 5 DAY),78,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 5 DAY),83,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 5 DAY),89,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 5 DAY),79,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 5 DAY),84,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 5 DAY),76,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 5 DAY),91,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 5 DAY),85,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 5 DAY),86,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 5 DAY),92,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 5 DAY),77,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 5 DAY),87,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 5 DAY),96,4,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 5 DAY),80,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 5 DAY),98,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 5 DAY),78,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 5 DAY),88,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 5 DAY),83,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 5 DAY),89,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 5 DAY),79,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 5 DAY),90,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 5 DAY),81,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 5 DAY),83,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 5 DAY),92,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 5 DAY),85,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 5 DAY),85,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 5 DAY),86,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 5 DAY),100,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 5 DAY),82,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 5 DAY),86,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 5 DAY),87,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 5 DAY),83,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 5 DAY),89,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 5 DAY),102,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 5 DAY),87,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 5 DAY),84,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 5 DAY),93,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 5 DAY),88,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 4 DAY),95,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 4 DAY),91,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 4 DAY),91,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 4 DAY),96,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 4 DAY),92,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 4 DAY),86,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 4 DAY),103,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 4 DAY),98,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 4 DAY),106,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 4 DAY),87,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 4 DAY),107,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 4 DAY),91,4,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 4 DAY),92,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 4 DAY),93,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 4 DAY),92,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 4 DAY),100,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 4 DAY),108,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 4 DAY),101,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY),93,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY),94,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 4 DAY),95,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 4 DAY),93,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY),104,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY),109,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY),96,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 4 DAY),97,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 4 DAY),95,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 4 DAY),95,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 4 DAY),96,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 4 DAY),105,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 4 DAY),106,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 4 DAY),110,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 4 DAY),108,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 4 DAY),98,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 4 DAY),98,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 4 DAY),100,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 4 DAY),99,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 4 DAY),96,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 4 DAY),111,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY),101,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY),112,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY),109,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY),98,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY),101,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 4 DAY),99,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 4 DAY),113,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 4 DAY),111,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 4 DAY),100,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 4 DAY),115,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 4 DAY),102,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 4 DAY),102,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 3 DAY),117,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 3 DAY),103,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 3 DAY),103,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 3 DAY),105,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 3 DAY),112,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 3 DAY),104,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 3 DAY),113,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 3 DAY),118,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 3 DAY),106,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 3 DAY),119,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 3 DAY),114,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 3 DAY),115,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 3 DAY),107,3,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 3 DAY),107,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 3 DAY),120,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 3 DAY),108,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 3 DAY),110,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 3 DAY),110,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 3 DAY),121,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 3 DAY),111,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 3 DAY),116,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 3 DAY),123,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 3 DAY),125,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 3 DAY),112,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 3 DAY),112,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 3 DAY),101,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY),114,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY),117,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 3 DAY),116,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 3 DAY),126,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 3 DAY),102,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 3 DAY),106,4,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 3 DAY),117,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 3 DAY),118,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 3 DAY),128,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 3 DAY),109,3,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 3 DAY),119,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 3 DAY),118,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 3 DAY),113,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 3 DAY),110,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 3 DAY),120,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY),131,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY),121,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY),111,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY),119,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 3 DAY),121,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 3 DAY),114,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 3 DAY),115,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 3 DAY),116,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 21:00:00'), INTERVAL 3 DAY),123,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 3 DAY),133,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 3 DAY),113,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 3 DAY),118,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 3 DAY),124,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 3 DAY),115,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 3 DAY),134,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 2 DAY),122,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 2 DAY),135,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 2 DAY),116,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 2 DAY),125,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 2 DAY),117,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 02:00:00'), INTERVAL 2 DAY),126,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 2 DAY),127,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 2 DAY),120,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY),118,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY),122,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY),136,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 2 DAY),128,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 2 DAY),123,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 2 DAY),137,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 2 DAY),123,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 2 DAY),119,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 2 DAY),139,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 2 DAY),124,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 2 DAY),124,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 2 DAY),140,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 2 DAY),120,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 2 DAY),125,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 08:00:00'), INTERVAL 2 DAY),127,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 2 DAY),141,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 2 DAY),127,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 2 DAY),121,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 2 DAY),129,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 2 DAY),142,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 2 DAY),129,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 2 DAY),128,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY),122,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY),129,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY),130,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY),130,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 2 DAY),131,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 2 DAY),130,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 2 DAY),143,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 2 DAY),132,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 2 DAY),133,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 2 DAY),124,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 2 DAY),132,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 2 DAY),145,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 2 DAY),132,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 2 DAY),133,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 2 DAY),134,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 2 DAY),135,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 2 DAY),134,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 2 DAY),136,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 2 DAY),126,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 2 DAY),134,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 2 DAY),136,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 2 DAY),129,3,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 2 DAY),146,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 2 DAY),137,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY),138,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY),138,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY),130,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 1 DAY),149,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 1 DAY),140,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00'), INTERVAL 1 DAY),135,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 1 DAY),142,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 01:00:00'), INTERVAL 1 DAY),131,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 03:00:00'), INTERVAL 1 DAY),150,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 1 DAY),145,3,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 1 DAY),139,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 04:00:00'), INTERVAL 1 DAY),151,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 1 DAY),146,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 05:00:00'), INTERVAL 1 DAY),136,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 1 DAY),133,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 06:00:00'), INTERVAL 1 DAY),147,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 1 DAY),148,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 1 DAY),137,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 1 DAY),152,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 07:00:00'), INTERVAL 1 DAY),134,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY),135,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY),149,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY),150,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY),136,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY),139,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY),140,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 10:00:00'), INTERVAL 1 DAY),153,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY),154,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY),151,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY),137,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 1 DAY),141,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 1 DAY),138,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 1 DAY),152,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 1 DAY),156,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY),142,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 1 DAY),157,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 1 DAY),143,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 1 DAY),139,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 1 DAY),159,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 1 DAY),140,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 16:00:00'), INTERVAL 1 DAY),153,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 1 DAY),161,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 1 DAY),141,2,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 1 DAY),141,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00'), INTERVAL 1 DAY),154,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY),142,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY),146,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY),162,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY),142,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 1 DAY),163,1,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY),166,3,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY),144,2,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY),155,1,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY),148,2,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY),143,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 1 DAY),149,1,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 1 DAY),144,1,0),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 22:00:00'), INTERVAL 1 DAY),145,1,3),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 1 DAY),157,2,6),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 1 DAY),152,3,2),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 1 DAY),168,2,1),
(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 1 DAY),147,2,3);


# withdraw_request
INSERT IGNORE INTO withdraw_request (id, player_id, bank_id, account_number, account_holder_name, amount, status, requested_date, external_uid, bank_branch)
VALUES
    (100001 , 1, 1, '1111000011112222', 'Chang San', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY), null, '中国工商银行広東分行'),
    (100002 , 1, 1, '1111000011112222', 'Chang San', 100, 11, DATE_SUB(NOW(), INTERVAL 1 DAY), null, '中国工商银行広東分行'),
    (100003 , 1, 1, '1111000011112222', 'Chang San', 50, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 1 DAY), 'test-external-uid-123456', '中国工商银行広東分行'),
    (100004 , 1, 1, '1111000011112222', 'Chang San', 100, 11, NOW(), null, '中国工商银行広東分行'),
    
    (100005 , 2, 3, '2222000033332222', 'Li Cheng', 300, 2, DATE_SUB(NOW(), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100006 , 2, 3, '2222000033332222', 'Li Cheng', 500, 12, DATE_SUB(NOW(), INTERVAL 1 DAY), null, '中国银行広東分行'),
    (100007 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), null, '中国银行広東分行'),
    (100008 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), null, '中国银行広東分行'),
    
    (100009 , 3, 7, '5555000011112222', 'Joseph Hawkins', 300, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 1 DAY), null, '招商银行上海分行'),
    (100010 , 2, 3, '2222000033332222', 'Li Cheng', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100011 , 2, 3, '2222000033332222', 'Li Chengn', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100012 , 5, 1, '5555000011112222', 'John Wilson', 100, 11, DATE_SUB(NOW(), INTERVAL 1 DAY), null, '中国工商银行深圳分行'),
    (100013 , 5, 1, '5555000011112222', 'John Wilson', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 1 DAY), null, '中国工商银行深圳分行'),
    (100014 , 6, 2, '6666336665550000', '刘 洪涛', 300, 11, DATE_SUB(NOW(), INTERVAL 1 DAY), null, '中国农业银行深圳分行'),
    (100015 , 6, 2, '6666336665550000', '刘 洪涛', 600, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 1 DAY), null, '中国农业银行深圳分行'),
    (100016 , 7, 2, '7993336665554441', 'Jhon Davis', 700, 11, DATE_SUB(NOW(), INTERVAL 30 MINUTE), null, '中国农业银行深圳分行'),
    
    (100017 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100018 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100019 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), null, '中国银行広東分行'),
    (100020 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY), null, '中国银行広東分行'),
    (100021 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY), null, '中国银行広東分行'),
    (100024 , 2, 3, '2222000033332222', 'Li Cheng', 300, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY), null, '中国银行広東分行'),
    (100022 , 2, 3, '2222000033332222', 'Li Cheng', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), null, '中国银行広東分行'),
    (100023 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 3 DAY), null, '中国银行広東分行'),
    (100025 , 2, 3, '2222000033332222', 'Li Cheng', 400, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY), null, '中国银行広東分行'),
    (100026 , 2, 3, '2222000033332222', 'Li Cheng', 500, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 3 DAY), null, '中国银行広東分行'),

    (100027 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), null, '中国银行広東分行'),
    (100028 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 4 DAY), null, '中国银行広東分行'),
    (100029 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY), null, '中国银行広東分行'),
    (100030 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 4 DAY), null, '中国银行広東分行'),
    (100031 , 2, 3, '2222000033332222', 'Li Cheng', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY), null, '中国银行広東分行'),

    (100032 , 6, 2, '6666336665550000', '刘 洪涛', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), null, '中国农业银行深圳分行'),
	(100033 , 7, 2, '7993336665554441', 'Jhon Davis', 500, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), null, '中国农业银行深圳分行'),
	(100034 , 6, 2, '6666336665550000', '刘 洪涛', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), null, '中国农业银行深圳分行'),
	(100035 , 7, 2, '7993336665554441', 'Jhon Davis', 200, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), null, '中国农业银行深圳分行'),

	(100036 , 6, 2, '6666336665550000', '刘 洪涛', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), null, '中国农业银行深圳分行'),
	(100037 , 7, 2, '7993336665554441', 'Jhon Davis', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), null, '中国农业银行深圳分行'),
	(100038 , 6, 2, '6666336665550000', '刘 洪涛', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), null, '中国农业银行深圳分行'),
	(100039 , 7, 2, '7993336665554441', 'Jhon Davis', 100, 10, DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), null, '中国农业银行深圳分行');

INSERT INTO withdraw_summary_report(time_hour, currency, withdrawal_count, withdrawer_count, withdrawal_amount, dirty) VALUES
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 2 DAY), 'CNY', 5, 3, 600, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 23:00:00'), INTERVAL 2 DAY), 'USD', 2, 1, 200, 0),
	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 09:00:00'), INTERVAL 1 DAY), 'CNY', 2, 1, 400, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 50, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 14:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 100, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 600, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 300, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 200, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 1 DAY), 'CNY', 1, 1, 200, 0),

    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 300, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 200, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 3 DAY), 'CNY', 3, 3, 500, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 400, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 3 DAY), 'CNY', 1, 1, 500, 0),

	(DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 200, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 12:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 200, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00'), INTERVAL 4 DAY), 'CNY', 3, 3, 900, 1),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 18:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 200, 0),
    (DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-%d 20:00:00'), INTERVAL 4 DAY), 'CNY', 1, 1, 200, 0);

#player_bank_account
INSERT IGNORE INTO player_bank_account (player_id, bank_id, account_number, account_holder_name, bank_branch, default_account, deleted) VALUES
    (1, 1, '1111000011112222', 'Chang San', '中国工商银行広東分行', 1, 0),
    (2, 3, '2222000033332222', 'Li Cheng', '中国银行広東分行', 1, 0),
    (2, 4, '3333111122223333', 'Li Cheng', '中国建设银行北京分行', 0, 0),
    (2, 6, '4444222255556666', 'Li Cheng', '交通银行上海分行', 0, 0),
    (3, 7, '5555000011112222', 'Joseph Hawkins', '招商银行上海分行', 1, 0),
    (3, 4, '6666336665554440', 'Joseph Hawkins', '中国建设银行深圳分行', 0, 0),
    (5, 1, '5555000011112222', 'John Wilson', '中国工商银行深圳分行', 1, 0),
    (6, 2, '6666336665550000', '刘 洪涛', '中国农业银行深圳分行', 1, 0),
    (7, 2, '7993336665554441', 'John Davis', '中国农业银行深圳分行', 1, 0);
    
# game_type
INSERT IGNORE INTO game_type (id, enabled) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1);

# game_type_i18n
INSERT IGNORE INTO game_type_i18n (game_type_id, locale, `name`) VALUES
(1, 'en-US', 'Slot Game'),
(1, 'zh-CN', '老虎机'),
(2, 'en-US', 'Live Casino'),
(2, 'zh-CN', '真人游戏'),
(3, 'en-US', 'Casino'),
(3, 'zh-CN', '赌场游戏'),
(4, 'en-US', 'Lottery'),
(4, 'zh-CN', '彩票'),
(5, 'en-US', 'Fishing'),
(5, 'zh-CN', '钓鱼'),
(6, 'en-US', 'Sports'),
(6, 'zh-CN', '体育'),
(7, 'en-US', 'E-Sports'),
(7, 'zh-CN', '电子竞技'),
(8, 'en-US', 'Chess'),
(8, 'zh-CN', '棋牌游戏'),
(9, 'en-US', 'Finance Game'),
(9, 'zh-CN', '金融'),
(10, 'en-US', 'Other'),
(10, 'zh-CN', '其他');

# game_name
# gameApiId : 1  MockGame = BBIN
INSERT IGNORE INTO game_name (id, game_api_code, game_code, game_type_id, enabled, featured, web, mobile, released_date) VALUES
(1, 'MockGame', 'FD1', 1, 1, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 1 DAY)),
(2, 'MockGame', 'BB', 1, 1, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 2 DAY)),
(3, 'MockGame', 'LIVE99', 2, 1, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 2 DAY)),
(4, 'MockGame', 'ES', 1, 1, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 3 DAY)),
(5, 'MockGame', 'CF', 1, 1, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 3 DAY)),
(6, 'MockGame', 'CK', 1, 1, 1, 0, 1, DATE_SUB(current_date() , INTERVAL 4 DAY)),
(7, 'MockGame', 'DT', 1, 1, 0, 0, 1, DATE_SUB(current_date() , INTERVAL 4 DAY)),
(8, 'MockGame', 'JQW', 1, 1, 0, 0, 1, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(9, 'MockGame', 'SP', 1, 1, 0, 0, 1, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(10, 'MockGame', 'FM', 1, 1, 0, 0, 1, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(11, 'MockGame', 'ZCJB', 1, 1, 0, 1, 0, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(12, 'MockGame', 'HF', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 1 DAY)),
(13, 'MockGame', 'KK', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 2 DAY)),
(14, 'MockGame', 'VRCR', 4, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 3 DAY)),
(15, 'MockGame2', 'PTFD', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(16, 'MockGame2', 'PTBB', 1, 1, 1, 1, 1, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(17, 'MockGame2', 'PTCF', 2, 1, 0, 1, 0, DATE_SUB(current_date() , INTERVAL 5 DAY)),
(18, 'MockGame', 'DUMMY', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 3 DAY)),
(19, 'MockGame2', 'DUMMY', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 3 DAY)),
(20, 'T1GAGIN', 'DUMMY', 1, 1, 0, 1, 1, DATE_SUB(current_date() , INTERVAL 3 DAY));

# game_name_i18n
INSERT IGNORE INTO game_name_i18n (id, game_name_id, locale, `name`) values
(1, 1, 'en-US', 'Fortune Day'),
(2, 1, 'zh-CN', '幸運日1'),
(3, 2, 'en-US', 'Bonus Bears'),
(4, 2, 'zh-CN', '熊之舞'),
(5, 3, 'en-US', 'Baccarat Live 999 Seat'),
(6, 3, 'zh-CN', '百家乐999'),
(7, 4, 'en-US', 'Esmeralda'),
(9, 5, 'en-US', 'Crazy fruit'),
(10, 5, 'zh-CN', '水果狂'),
(11, 6, 'en-US', 'Chinese Kitchen'),
(12, 7, 'en-US', 'Desert Treasure'),
(13, 8, 'en-US', 'Jin Qian Wa'),
(14, 9, 'en-US', 'Sweet Party'),
(15, 10, 'en-US', 'Funky Monkey'),
(16, 11, 'en-US', 'ZHAO CAI JIN BAO'),
(17, 11, 'zh-CN', '招财进宝'),
(18, 12, 'en-US', 'HALLOWEEN FORTUNE'),
(19, 13, 'en-US', 'King Kong'),
(20, 15, 'en-US', 'Super Fortune Day'),
(21, 16, 'en-US', 'Super Bonus Bears'),
(22, 17, 'en-US', 'Super Crazy fruit'),
(23, 14, 'en-US', 'VR Car Racing'),
(24, 14, 'zh-CN', 'VR 賽車');

# For testing game type activeGameCount (with user_enabled = false)
INSERT IGNORE INTO game_name (game_api_code, game_code, game_type_id, enabled, user_enabled, featured, web, mobile, released_date) VALUES
('MockGame', 'slot', 1, 1, 0, 1, 1, 0, DATE_SUB(current_date() , INTERVAL 1 DAY));

#game_log_hourly_report
INSERT IGNORE INTO `game_log_hourly_report` (`game_api_id`, `game_api_code`, `game_code`, `player_id`, `player_username`, `bet_count`, `bet`, `win`, `loss`, `payout`, `bet_time_hour`, `created_at`, `updated_at`) VALUES
(1, 'MockGame', 'FD1', '1', 'test001', 20, '100.000','100', '200', 0.000, '2018-09-20 14:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'FD1', '1', 'test001', 18, '100.000','200', '300', 0.000, '2018-09-20 15:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'FD1', '2', 'test002', 5, '50.000','0', '50', 0.000, '2018-09-20 13:00:00', '2018-09-20 17:36:57.970', now()),
(1, 'MockGame', 'FD1', '3', 'test003', 9, '150.000','100', '200', 0.000, '2018-09-20 12:00:00', '2018-09-20 17:36:57.971', now()),
(1, 'MockGame', 'LIVE99', '2', 'test002', 10, '100.000','100', '200', 0.000, '2018-09-20 11:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'LIVE99', '2', 'test002', 12, '100.000','100', '200', 0.000, '2018-09-20 14:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'DUMMY', '2', 'test002', 3, '50.000','100', '200', 0.000, '2018-09-20 15:00:00', '2018-09-20 17:36:57.970', now()),
(1, 'MockGame', 'DUMMY', '3', 'test003', 8, '150.000','100', '200', 0.000, '2018-09-20 18:00:00', '2018-09-20 17:36:57.971', now()),
(2, 'MockGame2', 'DUMMY', '74', 'test001', 7, '100.000','100', '200', 0.000, '2018-09-20 15:00:00', '2018-09-20 17:27:52.963', now()),
(2, 'MockGame2', 'DUMMY', '74', 'test001', 7, '100.000','100', '200', 0.000, '2018-09-20 16:00:00', '2018-09-20 17:27:52.963', now()),
(2, 'MockGame2', 'DUMMY', '4', 'test002', 10, '50.000','100', '200', 0.000, '2018-09-20 17:00:00', '2018-09-20 17:36:57.970', now()),
(2, 'MockGame2', 'DUMMY', '69', 'test003', 13, '150.000','100', '200', 0.000, '2018-09-20 18:00:00', '2018-09-20 17:36:57.971', now()),
(2, 'MockGame2', 'PTCF', '4', 'test002', 9, '100.000','100', '200', 0.000, date_sub(now(), interval 2 day), '2018-09-20 17:27:52.963', now()),
(2, 'MockGame2', 'DUMMY', '74', 'test001', 4, '50.000','100', '200', 0.000, date_sub(now(), interval 2 day), '2018-09-20 17:36:57.970', now()),
(1, 'MockGame', 'SP', '1', 'test001', 10, '100.000','100', '200', 0.000, '2018-09-20 14:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'JQW', '1', 'test001', 10, '100.000','200', '300', 0.000, '2018-09-20 15:00:00', '2018-09-20 17:27:52.963', now()),
(1, 'MockGame', 'FM', '2', 'test002', 5, '50.000','0', '50', 0.000, '2018-09-20 13:00:00', '2018-09-20 17:36:57.970', now()),
(1, 'MockGame', 'HF', '3', 'test003', 13, '150.000','100', '200', 0.000, '2018-09-20 12:00:00', '2018-09-20 17:36:57.971', now()),
(5, 'T1GAGIN', 'DUMMY', '1', 'test001', 8, '100.000','100', '200', 0.000, '2018-09-20 16:00:00', '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 2, '50.000','100', '200', 0.000, '2018-09-20 17:00:00', '2018-09-20 17:36:57.970', now()),
(5, 'T1GAGIN', 'DUMMY', '3', 'test003', 10, '150.000','100', '200', 0.000, '2018-09-20 18:00:00', '2018-09-20 17:36:57.971', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 8, '100.000','100', '200', 0.000, '2018-09-20 15:00:00', '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 8, '100.000','100', '200', 0.000, '2018-09-20 16:00:00', '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 12, '150.000','100', '200', 0.000, ADDTIME(DATE_FORMAT(now(), '%Y-%m-%d %H:00:00'), '-1 30:00:00'), '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 20, '250.000','100', '200', 0.000, ADDTIME(DATE_FORMAT(now(), '%Y-%m-%d %H:00:00'), '-1 31:00:00'), '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 31, '350.000','100', '200', 0.000, ADDTIME(DATE_FORMAT(now(), '%Y-%m-%d %H:00:00'), '-1 32:00:00'), '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '2', 'test002', 41, '450.000','100', '200', 0.000, ADDTIME(DATE_FORMAT(now(), '%Y-%m-%d %H:00:00'), '-1 33:00:00'), '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '1', 'test001', 44, '450.000','100', '200', 0.000, DATE_SUB(DATE_FORMAT(now(), '%Y-%m-%d %H:00:00'), interval 2 day), '2018-09-20 17:27:52.963', now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 1 day), '%Y-%m-%d %01:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 32, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 1 day), '%Y-%m-%d %02:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '6', 'test005', 45, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 1 day), '%Y-%m-%d %03:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '8', 'test007', 50, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 1 day), '%Y-%m-%d %03:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 23, '200.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 1 day), '%Y-%m-%d %03:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 78, '1000.000','100', '900', 800.000, DATE_FORMAT(date_add(now(), interval 8 hour), '%Y-%m-%d %01:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '8', 'test007', 33, '450.000','100', '350', 350.000, DATE_FORMAT(date_add(now(), interval 8 hour), '%Y-%m-%d %01:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '6', 'test005', 36, '450.000','100', '350', 350.000, DATE_FORMAT(now(), '%Y-%m-%d %01:00:00'), now(), now());

INSERT IGNORE INTO game_log_hourly_report (game_api_id,game_api_code,game_code,player_id,player_username, bet_count, bet,win,loss,payout,bet_time_hour,dirty,created_at,updated_at) VALUES
(9,'T1GPT','gos',1,'test001', 20,496.317, 9.787, 0, 9.787,  date_sub(now(), interval 2 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 12,424.4, 93.694, 0, 93.694,  date_sub(now(), interval 2 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 2,32.683, 20.163, 0, 20.163,  date_sub(now(), interval 2 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 1,43.712, 35.686, 0, 35.686,  date_sub(now(), interval 2 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 3,10.142, 1.106, 0, 1.106,  date_sub(now(), interval 2 day),0,now(),now()),
(9,'T1GPT','gos',1,'test001', 12,482.251, 0, 339.038, 0,  date_sub(now(), interval 3 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 23,310.248, 0, 41.839, 0,  date_sub(now(), interval 3 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 12,75.256, 61.649, 0, 61.649,  date_sub(now(), interval 3 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 9,157.481, 144.007, 0, 144.007,  date_sub(now(), interval 3 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 7,91.26, 0, 58.719, 0,  date_sub(now(), interval 3 day),0,now(),now()),
(9,'T1GPT','gos',1,'test001', 8,476.641, 312.882, 0, 312.882,  date_sub(now(), interval 4 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 10,248.967, 19.418, 0, 19.418,  date_sub(now(), interval 4 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 12,176.765, 26.524, 0, 26.524,  date_sub(now(), interval 4 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 22,127.372, 9.488, 0, 9.488,  date_sub(now(), interval 4 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 9,129.896, 0, 83.789, 0,  date_sub(now(), interval 4 day),0,now(),now()),
(9,'T1GPT','gos',1,'test001', 32,367.627, 0, 198.518, 0,  date_sub(now(), interval 5 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 31,247.781, 220.581, 0, 220.581,  date_sub(now(), interval 5 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 50,417.234, 373.206, 0, 373.206,  date_sub(now(), interval 5 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 62,450.322, 0, 3.614, 0,  date_sub(now(), interval 5 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 22,107.437, 33.476, 0, 33.476,  date_sub(now(), interval 5 day),0,now(),now()),
(9,'T1GPT','gos',1,'test001', 32,113.952, 88.521, 0, 88.521,  date_sub(now(), interval 6 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 2,11.199, 9.327, 0, 9.327,  date_sub(now(), interval 6 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 12,150.421, 0, 60.492, 0,  date_sub(now(), interval 6 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 26,198.821, 162.835, 0, 162.835,  date_sub(now(), interval 6 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 21,154.754, 0, 4.646, 0,  date_sub(now(), interval 6 day),0,now(),now()),
(9,'T1GPT','gos',1,'test001', 29,342.042, 0, 297.205, 0,  date_sub(now(), interval 7 day),0,now(),now()),
(9,'T1GPT','gos',2,'test002', 72,393.986, 150.191, 0, 150.191,  date_sub(now(), interval 7 day),0,now(),now()),
(9,'T1GPT','gos',3,'test003', 22,328.077, 293.05, 0, 293.05,  date_sub(now(), interval 7 day),0,now(),now()),
(9,'T1GPT','gos',5,'test004', 42,380.448, 77.147, 0, 77.147,  date_sub(now(), interval 7 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 18,97.53, 44.039, 0, 44.039,  date_sub(now(), interval 7 day),0,now(),now()),
(9,'T1GPT','gos',6,'test005', 21,155.752, 80.78, 0, 80.78,  date_sub(now(), interval 8 day),0,now(),now());

#for summary report
INSERT IGNORE INTO `game_log_hourly_report` (`game_api_id`, `game_api_code`, `game_code`, `player_id`, `player_username`, `bet_count`, `bet`, `win`, `loss`, `payout`, `bet_time_hour`, `created_at`, `updated_at`) VALUES
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 11:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 10:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 09:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 08:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 07:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 06:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 32, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 05:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '6', 'test005', 45, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 04:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '8', 'test007', 50, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 03:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 23, '200.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 2 day), '%Y-%m-%d 02:00:00'), now(), now()),


(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 3 day), '%Y-%m-%d 11:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 3 day), '%Y-%m-%d 10:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 3 day), '%Y-%m-%d 09:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 3 day), '%Y-%m-%d 08:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 3 day), '%Y-%m-%d 07:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 42, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 4 day), '%Y-%m-%d 06:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 32, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 4 day), '%Y-%m-%d 05:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '6', 'test005', 45, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 4 day), '%Y-%m-%d 04:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '8', 'test007', 50, '450.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 4 day), '%Y-%m-%d 03:00:00'), now(), now()),
(5, 'T1GAGIN', 'DUMMY', '7', 'test006', 23, '200.000','100', '200', 0.000, DATE_FORMAT(date_sub(curdate(), interval 4 day), '%Y-%m-%d 02:00:00'), now(), now());


INSERT IGNORE INTO `game_log_hourly_report` (`game_api_id`, `game_api_code`, `game_code`, `player_id`, `player_username`, `bet_count`, `bet`, `win`, `loss`, `payout`, `bet_time_hour`, `dirty`) VALUES
(8, 'T1GKYCARD', '9109101', 2, 'test002', 2, 20.000, 19.400, 0, 39.400, DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 2 DAY), '%Y-%m-%d 14:00:00'), 0);

#game_log_sync_task
INSERT IGNORE INTO `game_log_sync_task` (`id`,`game_api_id`,`params`,`sync_username`,`status`,`sync_result`) VALUES
(1,1, CONCAT('{ "START_TIME": "', DATE_ADD(NOW(), INTERVAL -24 HOUR), '",  "END_TIME": "', DATE_ADD(NOW(), INTERVAL -12 HOUR), '" }'), 'SyncRunner',2,'{\n  \"countOfGameLogs\": 4,\n  \"startTime\": \"2018-10-31T09:54:32.913\",\n  \"endTime\": \"2018-10-31T09:54:32.930\",\n  \"durationMs\": 17,\n  \"errorMsg\": null\n}');
INSERT IGNORE INTO `game_log_sync_task` (`id`,`game_api_id`,`params`,`sync_username`,`sync_start`,`status`,`sync_result`) VALUES
(2,2,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(3,2,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(4,2,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(5,1,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(6,3,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(7,4,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(8,5,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(9,6,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(10,7,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(11,8,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(12,9,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(13,10,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(14,11,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(15,12,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}'),
(16,13,'{\n  \"SYNC_INDEX\": 0\n}','SyncRunner','2020-03-31 11:24:00',2,'{\n  \"countOfGameLogs\": 1,\n  \"startTime\": \"2018-10-31T09:54:32.912\",\n  \"endTime\": \"2018-10-31T09:54:32.932\",\n  \"durationMs\": 20,\n  \"errorMsg\": null\n}');

#site_privilege
INSERT IGNORE INTO `site_privilege` (`type`,`value`,`active`)
    VALUES
    ('SITE_LANGUAGE', 'en-US', 1),
    ('SITE_LANGUAGE', 'zh-CN', 1),
    ('SITE_LANGUAGE', 'vi-VN', 1),
    ('SITE_LANGUAGE', 'th-TH', 1),
    ('SITE_LANGUAGE', 'hi-IN', 1),
    ('SITE_CURRENCY', 'CNY', 1),
    ('SITE_CURRENCY', 'USD', 1),
    ('SITE_CURRENCY', 'THB', 1),
    ('SITE_TEMPLATE', 'main', 1),
    ('SITE_TEMPLATE', 'new1', 0),
    ('SITE_TEMPLATE', 'new2', 0),
    ('GAME_API', 'MockGame', 1),
    ('GAME_API', 'MockGame2', 1),
    ('GAME_API', 'T1GAGIN', 1),
    ('GAME_API', 'T1GBBIN', 1),
    ('GAME_API', 'T1GPRAGMATICPLAY', 1),
    ('GAME_API', 'T1GKYCARD', 1),
    ('GAME_API', 'T1GPT', 1),
    ('GAME_API', 'T1GMGPLUS', 1),
    ('GAME_API', 'T1GAB', 1),
    ('GAME_API', 'T1GEBET', 0),
    ('GAME_API', 'T1GISB', 0),
    ('GAME_API', 'T1GONEWORKS', 0),
    ('GAME_API', 'T1GCQ9', 0),
    ('PAYMENT_API', 'MockPay', 1);

# game_log_[gameApi]
INSERT IGNORE INTO `game_log_mock_game` (`id`,`game_code`,`game_name_id`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`valid`) VALUES
(1, 'FD1',1,'1810030696387520',2,'test002',50.000,47.500,DATE_Add(now(), INTERVAL -1 DAY) ,1),
(2, 'BB',1,'1810030696387530',2,'test002',51.000,48.500,DATE_Add(now(), INTERVAL -1 DAY),1),
(3, 'LIVE999',1,'1810030696387540',2,'test002',52.000,49.500,DATE_Add(now(), INTERVAL -1 DAY),1),
(4, 'ES',1,'1810030696387550',2,'test002',53.000,40.500,DATE_Add(now(), INTERVAL -1 DAY),1),
(5, 'CF',1,'1810030696387560',2,'test002',50.000,47.500,now(),1),
(6, 'CK',1,'1810030696387570',2,'test002',51.000,48.500,now(),1),
(7, 'DT',1,'1810030696387580',2,'test002',52.000,49.500,now(),1),
(8, 'JQW',1,'1810030696387590',2,'test002',53.000,40.500,now(),1);
INSERT IGNORE INTO `game_log_mock_game2` (`id`,`game_code`,`game_name_id`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`valid`) VALUES
(1, 'PTFD',2,'47865551721',2,'test002',0.900,41.100,DATE_Add(now(), INTERVAL -1 DAY),1),
(2, 'PTBB',2,'47865551722',2,'test002',0.911,41.100,DATE_Add(now(), INTERVAL -1 DAY),1),
(3, 'PTCF',2,'47865551723',2,'test002',0.922,41.100,DATE_Add(now(), INTERVAL -1 DAY),1),
(4, 'PTFD',2,'47865551724',2,'test002',0.900,41.100,now(),1),
(5, 'PTBB',2,'47865551725',2,'test002',0.911,41.100,now(),1),
(6, 'PTCF',2,'47865551726',2,'test002',0.922,41.100,now(),1);

-- PT
INSERT IGNORE INTO `game_log_t1` (`id`,`uniqueid`, `external_uid`, `game_external_uniqueid`, `username`, `merchant_code`, `game_platform_id`, `game_code`, `game_name`, `game_finish_time`, `game_details`, `bet_time`, `payout_time`, `round_number`, `real_bet_amount`, `effective_bet_amount`, `result_amount`, `payout_amount`, `after_balance`, `bet_details`, `rent`, `md5_sum`, `ip_address`, `bet_type`, `odds_type`, `odds`, `game_status`, `detail_status`, `game_updated_at`, `created_at`, `updated_at`) VALUES
(1,67323220,'1394501354075','394501354075','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:37',NULL,'2020-01-20 11:50:37','2020-01-20 11:50:37','394501354075',0.25,0.25,-0.25,0,224.08,NULL,0,'29f589a2639a57d6ada477a0f5f860a7',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:43',now(),now()),
(2,67323219,'1394501352269','394501352269','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:35',NULL,'2020-01-20 11:50:35','2020-01-20 11:50:35','394501352269',0.25,0.25,-0.25,0,224.33,NULL,0,'c38fcda68ac10aef34b2b78547b28226',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:43',now(),now()),
(3,67323218,'1394501350415','394501350415','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:33',NULL,'2020-01-20 11:50:33','2020-01-20 11:50:33','394501350415',0.25,0.25,-0.25,0,224.58,NULL,0,'c106b96d2d15bff3661ec9cfa75661ba',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:43',now(),now()),
(4,67323217,'1394501348149','394501348149','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:31',NULL,'2020-01-20 11:50:31','2020-01-20 11:50:31','394501348149',0.25,0.25,1,1.25,224.83,NULL,0,'fd1b00bacd4da040faccff4b146ab8b5',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:43',now(),now()),
(5,67323216,'1394501345386','394501345386','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:28',NULL,'2020-01-20 11:50:28','2020-01-20 11:50:28','394501345386',0.25,0.25,0,0.25,223.83,NULL,0,'c12d22d7bacd50b1a73ecd4e991d06bd',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:43',now(),now()),
(6,67323209,'1394501332721','394501332721','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:14',NULL,'2020-01-20 11:50:14','2020-01-20 11:50:14','394501332721',0.25,0.25,-0.15,0.1,223.83,NULL,0,'6792a0f0dadc0125b503d3fee204794a',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:28',now(),now()),
(7,67323208,'1394501330937','394501330937','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:12',NULL,'2020-01-20 11:50:12','2020-01-20 11:50:12','394501330937',0.25,0.25,-0.25,0,223.98,NULL,0,'d92e97fbfd1dcf0a5f82174b95a38292',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:28',now(),now()),
(8,67323207,'1394501328905','394501328905','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:10',NULL,'2020-01-20 11:50:10','2020-01-20 11:50:10','394501328905',0.25,0.25,-0.15,0.1,224.23,NULL,0,'934753d20ac18df3726f2b3dfee07cf9',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:28',now(),now()),
(9,67323201,'1394501326556','394501326556','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:08',NULL,'2020-01-20 11:50:08','2020-01-20 11:50:08','394501326556',0.25,0.25,-0.15,0.1,224.38,NULL,0,'83164cac49e59901c2e6498d3896aefc',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(10,67323200,'1394501323707','394501323707','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:05',NULL,'2020-01-20 11:50:05','2020-01-20 11:50:05','394501323707',0.25,0.25,-0.05,0.2,224.53,NULL,0,'a0e876189fe22e865dcb91b3b26c7c9f',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(11,67323199,'1394501321399','394501321399','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:02',NULL,'2020-01-20 11:50:02','2020-01-20 11:50:02','394501321399',0.25,0.25,-0.15,0.1,224.58,NULL,0,'ef2e84c34b6b994802578f06d95c57d8',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(12,67323198,'1394501318864','394501318864','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:50:00',NULL,'2020-01-20 11:50:00','2020-01-20 11:50:00','394501318864',0.25,0.25,-0.1,0.15,224.73,NULL,0,'27c75d3b0faf6a2391965e4824c0444a',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(13,67323197,'1394501313584','394501313584','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:54',NULL,'2020-01-20 11:49:54','2020-01-20 11:49:54','394501313584',0.25,0.25,-0.25,0,224.83,NULL,0,'8a413fbb0f5c8f96f71866aa99f51834',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(14,67323196,'1394501313209','394501313209','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:54',NULL,'2020-01-20 11:49:54','2020-01-20 11:49:54','394501313209',0.25,0.25,-0.25,0,225.08,NULL,0,'847d08b645dcc2696939b7a0e46c63ae',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(15,67323195,'1394501312855','394501312855','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:54',NULL,'2020-01-20 11:49:54','2020-01-20 11:49:54','394501312855',0.25,0.25,-0.25,0,225.33,NULL,0,'e19f6a2db460cb11720ab04ccfe707ec',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(16,67323194,'1394501312194','394501312194','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:53',NULL,'2020-01-20 11:49:53','2020-01-20 11:49:53','394501312194',0.25,0.25,-0.15,0.1,225.58,NULL,0,'1881f01aee15d2619e372fd8b3ef76c2',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(17,67323193,'1394501311881','394501311881','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:53',NULL,'2020-01-20 11:49:53','2020-01-20 11:49:53','394501311881',0.25,0.25,-0.25,0,225.73,NULL,0,'209ae08e9e8e218ba210eeb7ce69cf85',NULL,NULL,0,0,'settled',1,'2020-01-20 11:51:12',now(),now()),
(18,67323192,'1394501311540','394501311540','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:52',NULL,'2020-01-20 11:49:52','2020-01-20 11:49:52','394501311540',0.25,0.25,-0.25,0,225.98,NULL,0,'07f77e7a0bc04fb85240e7ae303069ab',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(19,67323191,'1394501311177','394501311177','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:52',NULL,'2020-01-20 11:49:52','2020-01-20 11:49:52','394501311177',0.25,0.25,-0.25,0,226.23,NULL,0,'852a5165fb4b58e78ca95ab67322171b',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(20,67323190,'1394501310464','394501310464','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:51',NULL,'2020-01-20 11:49:51','2020-01-20 11:49:51','394501310464',0.25,0.25,-0.1,0.15,226.48,NULL,0,'687abd63b5d1242941a055f1884d8b57',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(21,67323189,'1394501309865','394501309865','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:50',NULL,'2020-01-20 11:49:50','2020-01-20 11:49:50','394501309865',0.25,0.25,0,0.25,226.58,NULL,0,'945e3cd4b81cc9f2776ebd2ee5cfd125',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(22,67323188,'1394501309134','394501309134','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:50',NULL,'2020-01-20 11:49:50','2020-01-20 11:49:50','394501309134',0.25,0.25,-0.15,0.1,226.58,NULL,0,'09bebaf2f971ea302fd5f30fc18d657e',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(23,67323187,'1394501308443','394501308443','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:49',NULL,'2020-01-20 11:49:49','2020-01-20 11:49:49','394501308443',0.25,0.25,-0.1,0.15,226.73,NULL,0,'e45dfdc077903b705c0d21e3538936f0',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(24,67323186,'1394501307793','394501307793','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:48',NULL,'2020-01-20 11:49:48','2020-01-20 11:49:48','394501307793',0.25,0.25,-0.15,0.1,226.83,NULL,0,'3c3092fe71ae2789bcc995ae99234cde',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(25,67323185,'1394501307428','394501307428','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:48',NULL,'2020-01-20 11:49:48','2020-01-20 11:49:48','394501307428',0.25,0.25,-0.25,0,226.98,NULL,0,'cd74def4c073ffe84fab9db6c28fd038',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(26,67323184,'1394501306836','394501306836','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:47',NULL,'2020-01-20 11:49:47','2020-01-20 11:49:47','394501306836',0.25,0.25,0,0.25,227.23,NULL,0,'a406280e55376c7d16b4d5f210a99ba7',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(27,67323183,'1394501306377','394501306377','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:47',NULL,'2020-01-20 11:49:47','2020-01-20 11:49:47','394501306377',0.25,0.25,-0.25,0,227.23,NULL,0,'f43cdde34faea0cfc85e9e2423d762aa',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(28,67323182,'1394501305594','394501305594','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:46',NULL,'2020-01-20 11:49:46','2020-01-20 11:49:46','394501305594',0.25,0.25,-0.05,0.2,227.48,NULL,0,'a0d706969a4fe7d11fa3f61c2a4626cc',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(29,67323181,'1394501305232','394501305232','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:46',NULL,'2020-01-20 11:49:46','2020-01-20 11:49:46','394501305232',0.25,0.25,-0.25,0,227.53,NULL,0,'4ca4dfe950a5ae74926946f119b8814b',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(30,67323180,'1394501304958','394501304958','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:45',NULL,'2020-01-20 11:49:45','2020-01-20 11:49:45','394501304958',0.25,0.25,-0.25,0,227.78,NULL,0,'4db615465f6710a2c539e7c1dd682924',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(31,67323179,'1394501304311','394501304311','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:45',NULL,'2020-01-20 11:49:45','2020-01-20 11:49:45','394501304311',0.25,0.25,-0.15,0.1,228.03,NULL,0,'ebe76c60c990be2cfc99582ce644f88c',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(32,67323178,'1394501303585','394501303585','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:44',NULL,'2020-01-20 11:49:44','2020-01-20 11:49:44','394501303585',0.25,0.25,0,0.25,228.18,NULL,0,'c0fb916b96371fb5231e7a3889e71be9',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(33,67323177,'1394501302929','394501302929','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:43',NULL,'2020-01-20 11:49:43','2020-01-20 11:49:43','394501302929',0.25,0.25,-0.15,0.1,228.18,NULL,0,'de37847d584fd4a264ffb0d5d6a0a518',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(34,67323176,'1394501302545','394501302545','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:43',NULL,'2020-01-20 11:49:43','2020-01-20 11:49:43','394501302545',0.25,0.25,-0.25,0,228.33,NULL,0,'8ede5039415bd8e2e59f6267c4ea5709',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(35,67323175,'1394501301944','394501301944','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:42',NULL,'2020-01-20 11:49:42','2020-01-20 11:49:42','394501301944',0.25,0.25,0.25,0.5,228.58,NULL,0,'f58210684cef03574e25ce9f5feaea24',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(36,67323174,'1394501301298','394501301298','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:42',NULL,'2020-01-20 11:49:42','2020-01-20 11:49:42','394501301298',0.25,0.25,-0.15,0.1,228.33,NULL,0,'69e5fc0f44103e2b8be078898f594437',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(37,67323173,'1394501300656','394501300656','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:41',NULL,'2020-01-20 11:49:41','2020-01-20 11:49:41','394501300656',0.25,0.25,-0.15,0.1,228.48,NULL,0,'9e21ba3b8117bdfc422b6af75829e4ea',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(38,67323172,'1394501300067','394501300067','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:40',NULL,'2020-01-20 11:49:40','2020-01-20 11:49:40','394501300067',0.25,0.25,-0.15,0.1,228.63,NULL,0,'5e3f523cc9ada13e93edfbdb2140a960',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(39,67323171,'1394501299705','394501299705','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:40',NULL,'2020-01-20 11:49:40','2020-01-20 11:49:40','394501299705',0.25,0.25,-0.25,0,228.78,NULL,0,'0fc2d032c93383eb1f246b1288746930',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(40,67323170,'1394501298997','394501298997','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:39',NULL,'2020-01-20 11:49:39','2020-01-20 11:49:39','394501298997',0.25,0.25,0.25,0.5,229.03,NULL,0,'938eab292c9a6c0f1b5ee170611b573d',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(41,67323169,'1394501298196','394501298196','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:38',NULL,'2020-01-20 11:49:38','2020-01-20 11:49:38','394501298196',0.25,0.25,-0.15,0.1,228.78,NULL,0,'1debdb1b82c3b1a9c049834670cc5a30',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:57',now(),now()),
(42,67323151,'1394501296365','394501296365','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:36',NULL,'2020-01-20 11:49:36','2020-01-20 11:49:36','394501296365',0.25,0.25,-0.25,0,228.93,NULL,0,'c018bcafcebefdcc55fc95682de0d822',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(43,67323150,'1394501294142','394501294142','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:34',NULL,'2020-01-20 11:49:34','2020-01-20 11:49:34','394501294142',0.25,0.25,0,0.25,229.18,NULL,0,'b12d6e82320bd4110d26562e1bbcf21c',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(44,67323149,'1394501291910','394501291910','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:32',NULL,'2020-01-20 11:49:32','2020-01-20 11:49:32','394501291910',0.25,0.25,-0.1,0.15,229.18,NULL,0,'15ccbafd26f0f13ce6eccf2d6e074eab',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(45,67323148,'1394501289444','394501289444','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:29',NULL,'2020-01-20 11:49:29','2020-01-20 11:49:29','394501289444',0.25,0.25,-0.15,0.1,229.28,NULL,0,'56b9ae83f1dc23bbbe302a2a8d8ac086',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(46,67323147,'1394501287278','394501287278','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:27',NULL,'2020-01-20 11:49:27','2020-01-20 11:49:27','394501287278',0.25,0.25,-0.15,0.1,229.43,NULL,0,'213a095a2a01b2025c0784d9ead2890d',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(47,67323146,'1394501285095','394501285095','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:25',NULL,'2020-01-20 11:49:25','2020-01-20 11:49:25','394501285095',0.25,0.25,0,0.25,229.58,NULL,0,'ca3c9bc4c405296c87440e2761768e33',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:42',now(),now()),
(48,67323141,'1394501280609','394501280609','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:20',NULL,'2020-01-20 11:49:20','2020-01-20 11:49:20','394501280609',12.5,12.5,12.5,25,229.58,NULL,0,'0c24aaeaca2e3047d11372d1a1b3d84e',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:27',now(),now()),
(49,67323140,'1394501278734','394501278734','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:18',NULL,'2020-01-20 11:49:18','2020-01-20 11:49:18','394501278734',12.5,12.5,-12.5,0,217.08,NULL,0,'8feaca6cf0241f1ba721f7b4d87a3e24',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:27',now(),now()),
(50,67323139,'1394501276304','394501276304','test002','testmerchant',1,'gos','_json:{\"1\":\"Golden Tour\",\"2\":\"黄金之旅\",\"3\":\"Golden Tour\",\"4\":\"Golden Tour\",\"5\":\"Golden Tour\"}','2020-01-20 11:49:15',NULL,'2020-01-20 11:49:15','2020-01-20 11:49:15','394501276304',12.5,12.5,-5,7.5,229.58,NULL,0,'36bb510d11e76f19612c404da9cb29f9',NULL,NULL,0,0,'settled',1,'2020-01-20 11:50:27',now(),now());

update `game_log_t1` set game_finish_time = DATE_Add(game_finish_time,INTERVAL  (DATEDIFF(now(), game_finish_time)-1) DAY);
update `game_log_t1` set bet_time = DATE_Add(bet_time,INTERVAL  (DATEDIFF(now(), bet_time)-1) DAY);
update `game_log_t1` set payout_time = DATE_Add(payout_time,INTERVAL  (DATEDIFF(now(), payout_time)-1) DAY);
update `game_log_t1` set game_updated_at = DATE_Add(game_updated_at,INTERVAL  (DATEDIFF(now(), game_updated_at)-1) DAY);

INSERT IGNORE INTO game_log (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`status`, `md5_sum`)
select UUID(),9, 'T1GPT', game_code,external_uid,'2',username,real_bet_amount,payout_amount,bet_time,game_finish_time,1,md5_sum from game_log_t1;

INSERT IGNORE INTO game_log (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`status`, `md5_sum`)
select UUID(),9, 'T1GPT', game_code, CONCAT('3',external_uid), 3, 'test003', real_bet_amount,payout_amount,bet_time,game_finish_time,1,md5_sum from game_log_t1;

# status is not SETTLED
INSERT IGNORE INTO game_log (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`status`, `md5_sum`) values
(UUID(),9, 'T1GPT', 'T1GPT','0123456789','2','test002',10.000,10.000,DATE_SUB(NOW(), INTERVAL 6 DAY),DATE_SUB(NOW(), INTERVAL 6 DAY),0,'0123456789'),
(UUID(),9, 'T1GPT', 'T1GPT','9876543210','2','test002',20.000,20.000,DATE_SUB(NOW(), INTERVAL 6 DAY),DATE_SUB(NOW(), INTERVAL 6 DAY),0,'9876543210');

-- CQ9
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (51,'73403638','724cq9404953130420','cq9404953130420','devtest002','testmerchant','724','1','_json:{\"1\":\"FruitKing\",\"2\":\"钻石水果王\",\"3\":\"FruitKing\",\"4\":\"FruitKing\",\"5\":\"FruitKing\"}','2020-08-21 12:27:25',NULL,'2020-08-21 12:27:25','2020-08-21 12:27:25','404953130420',1.800,1.800,-1.800,0.000,6799.580,'{\"Created At\":\"2020-08-21 12:36:43\"}',0.000,'78c677322190f616b3ce2a8d777a470b',NULL,NULL,'0',0,'normal',1,'2020-08-21 12:28:50','2020-08-21 04:28:56.989','2020-08-21 04:36:57.321');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (52,'73403639','724cq9404953130029','cq9404953130029','devtest002','testmerchant','724','1','_json:{\"1\":\"FruitKing\",\"2\":\"钻石水果王\",\"3\":\"FruitKing\",\"4\":\"FruitKing\",\"5\":\"FruitKing\"}','2020-08-21 12:27:20',NULL,'2020-08-21 12:27:20','2020-08-21 12:27:20','404953130029',0.900,0.900,-0.900,0.000,6801.380,'{\"Created At\":\"2020-08-21 12:36:43\"}',0.000,'b7f2506cc49f5a6e120b8e9506a4a829',NULL,NULL,'0',0,'normal',1,'2020-08-21 12:28:50','2020-08-21 04:28:56.989','2020-08-21 04:36:57.321');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (53,'73403640','724cq9404952989694','cq9404952989694','devtest002','testmerchant','724','1','_json:{\"1\":\"FruitKing\",\"2\":\"钻石水果王\",\"3\":\"FruitKing\",\"4\":\"FruitKing\",\"5\":\"FruitKing\"}','2020-08-21 12:27:16',NULL,'2020-08-21 12:27:16','2020-08-21 12:27:16','404952989694',0.900,0.900,-0.900,0.000,6802.280,'{\"Created At\":\"2020-08-21 12:36:43\"}',0.000,'76ae47b7f2cf57b4da6b8853573eaca6',NULL,NULL,'0',0,'normal',1,'2020-08-21 12:28:50','2020-08-21 04:28:56.989','2020-08-21 04:36:57.321');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (54,'73403641','724cq9404952988380','cq9404952988380','devtest002','testmerchant','724','1','_json:{\"1\":\"FruitKing\",\"2\":\"钻石水果王\",\"3\":\"FruitKing\",\"4\":\"FruitKing\",\"5\":\"FruitKing\"}','2020-08-21 12:27:10',NULL,'2020-08-21 12:27:10','2020-08-21 12:27:10','404952988380',18.000,18.000,52.000,70.000,6803.180,'{\"Created At\":\"2020-08-21 12:36:43\"}',0.000,'146d516407a48d7ffdd15fcb7b3e5cbc',NULL,NULL,'0',0,'normal',1,'2020-08-21 12:28:50','2020-08-21 04:28:56.989','2020-08-21 04:36:57.321');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (55,'73403642','724cq9404952988050','cq9404952988050','devtest002','testmerchant','724','1','_json:{\"1\":\"FruitKing\",\"2\":\"钻石水果王\",\"3\":\"FruitKing\",\"4\":\"FruitKing\",\"5\":\"FruitKing\"}','2020-08-21 12:26:58',NULL,'2020-08-21 12:26:58','2020-08-21 12:26:58','404952988050',18.000,18.000,-18.000,0.000,6751.180,'{\"Created At\":\"2020-08-21 12:36:43\"}',0.000,'d6af1adb671cd41186e4b2f424b22bb8',NULL,NULL,'0',0,'normal',1,'2020-08-21 12:28:50','2020-08-21 04:28:56.989','2020-08-21 04:36:57.321');

-- ISB
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (56,'73408222','561597999686820158877100932244443','1597999686820158877100932244443','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:06',NULL,'2020-08-21 16:48:06','2020-08-21 16:48:06','02279307devtest002159783200780572245',0.000,0.000,0.000,0.000,6894.580,'[]',NULL,'7852cac1c3483af2a1398e579d6ea927',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (57,'73408223','561597999686893871515468021026045','1597999686893871515468021026045','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:06',NULL,'2020-08-21 16:48:06','2020-08-21 16:48:06','02279307devtest002159783200780572245',5.000,5.000,-5.000,0.000,6894.580,'[]',NULL,'4c36865cce7e3c183396d13b303c2807',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (58,'73408224','561597999690511510781933500553022','1597999690511510781933500553022','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:10',NULL,'2020-08-21 16:48:10','2020-08-21 16:48:10','02279307devtest002159799968660905765',5.000,5.000,-5.000,0.000,6889.580,'[]',NULL,'becb30a5acf16537e8170f47676d28a3',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (59,'73408225','561597999690570872004972581039950','1597999690570872004972581039950','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:10',NULL,'2020-08-21 16:48:10','2020-08-21 16:48:10','02279307devtest002159799968660905765',0.000,0.000,10.000,10.000,6899.580,'[]',NULL,'ab81af4fb845264f7e04b662532eec7d',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (60,'73408226','561597999703364029192825649548756','1597999703364029192825649548756','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:23',NULL,'2020-08-21 16:48:23','2020-08-21 16:48:23','02279307devtest002159799969058359080',0.000,0.000,0.000,0.000,6894.580,'[]',NULL,'141ee08a69c751576f03858fdb3115fa',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (61,'73408227','561597999703819174569089430464050','1597999703819174569089430464050','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:23',NULL,'2020-08-21 16:48:23','2020-08-21 16:48:23','02279307devtest002159799969058359080',5.000,5.000,-5.000,0.000,6894.580,'[]',NULL,'4b5b3406c1e9907f9017caa250ef9931',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (62,'73408228','561597999707700292253824131416732','1597999707700292253824131416732','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:27',NULL,'2020-08-21 16:48:27','2020-08-21 16:48:27','02279307devtest002159799970347250100',5.000,5.000,-5.000,0.000,6889.580,'[]',NULL,'b074d598f72ec6c8cbf13004525d1587',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (63,'73408229','561597999717123258253000682391280','1597999717123258253000682391280','devtest002','testmerchant','56','909307','_json:{\"1\":\"Scrolls of Ra HD\",\"2\":\"埃及旋转HD\",\"3\":\"Scrolls of Ra HD\",\"4\":\"Scrolls of Ra HD\",\"5\":\"Scrolls of Ra HD\"}','2020-08-21 16:48:37',NULL,'2020-08-21 16:48:37','2020-08-21 16:48:37','02279307devtest002159799970347250100',0.000,0.000,95.000,95.000,6984.580,'[]',NULL,'fa75f1d17cff9f2087a1305318464d77',NULL,NULL,NULL,0,'normal',1,'2020-08-21 16:52:08','2020-08-21 08:57:05.146','2020-08-21 08:57:05.146');

-- MGPLUS
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (64,'73470153','521303ECE7BC007D410B00020000000127598E14','03ECE7BC007D410B00020000000127598E14','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:25:21',NULL,'2020-08-25 11:25:21','2020-08-25 11:25:21','03ECE7BC007D410B00020000000127598E14',0.250,0.250,-0.250,0.000,0.000,NULL,0.000,'01575010019bdc0122ed62621b0caf70',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (65,'73470154','521303ECE7BC007D410B00020000000127598E15','03ECE7BC007D410B00020000000127598E15','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:25:26',NULL,'2020-08-25 11:25:26','2020-08-25 11:25:26','03ECE7BC007D410B00020000000127598E15',0.250,0.250,-0.250,0.000,0.000,NULL,0.000,'b7d97f5d2118c531ade02112b3384679',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (66,'73470155','521303ECE7BC007D410B00020000000127598E16','03ECE7BC007D410B00020000000127598E16','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:25:38',NULL,'2020-08-25 11:25:38','2020-08-25 11:25:38','03ECE7BC007D410B00020000000127598E16',0.250,0.250,-0.250,0.000,0.000,NULL,0.000,'816edbd52a52d47c7b42b2f49022466d',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (67,'73470156','521303B512BD007D410B00020000000127599936','03B512BD007D410B00020000000127599936','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:25:49',NULL,'2020-08-25 11:25:49','2020-08-25 11:25:49','03B512BD007D410B00020000000127599936',0.500,0.500,-0.500,0.000,0.000,NULL,0.000,'1a6590df569a91698e667c5e95c834fc',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (68,'73470157','521303065EBD007D410B00020000000127599F50','03065EBD007D410B00020000000127599F50','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:25:54',NULL,'2020-08-25 11:25:54','2020-08-25 11:25:54','03065EBD007D410B00020000000127599F50',0.500,0.500,-0.500,0.000,0.000,NULL,0.000,'11d5d318e8c8990ec7257bdb5f633599',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (69,'73470158','521303065EBD007D410B00020000000127599F51','03065EBD007D410B00020000000127599F51','devtest002','testmerchant','5213','SMG_ladiesNite2TurnWild','_json:{\"1\":\"Ladies Nite 2 Turn Wild\",\"2\":\"淑女之夜2 狂野变身\",\"3\":\"Ladies Nite 2 Turn Wild\",\"4\":\"Ladies Nite 2 Turn Wild\",\"5\":\"Ladies Nite 2 Turn Wild\"}','2020-08-25 11:26:01',NULL,'2020-08-25 11:26:01','2020-08-25 11:26:01','03065EBD007D410B00020000000127599F51',0.250,0.250,0.150,0.400,0.000,NULL,0.000,'5dabdd0e178e245b8935e006a11c60ab',NULL,NULL,'0',0,'normal',1,'2020-08-25 11:28:15','2020-08-25 03:55:52.628','2020-08-25 03:55:52.628');

update `game_log_t1` set game_finish_time = DATE_Add(game_finish_time,INTERVAL  (DATEDIFF(now(), game_finish_time)-1) DAY) where game_platform_id in (56, 724, 5213);
update `game_log_t1` set bet_time = DATE_Add(bet_time,INTERVAL  (DATEDIFF(now(), bet_time)-1) DAY) where game_platform_id in (56, 724, 5213);
update `game_log_t1` set payout_time = DATE_Add(payout_time,INTERVAL  (DATEDIFF(now(), payout_time)-1) DAY) where game_platform_id in (56, 724, 5213);
update `game_log_t1` set game_updated_at = DATE_Add(game_updated_at,INTERVAL  (DATEDIFF(now(), game_updated_at)-1) DAY) where game_platform_id in (56, 724, 5213);


-- KYCARD
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (70,'73471444','71450-1598335476-3126803620-40','50-1598335476-3126803620-40','devtest002','testmerchant','714','9109101','_json:{\"1\":\"Baccarat Fresh Room\",\"2\":\"百家乐体验房\",\"3\":\"Baccarat Fresh Room\",\"4\":\"Baccarat Fresh Room\",\"5\":\"Baccarat Fresh Room\"}','2020-08-25 14:05:05',NULL,'2020-08-25 14:04:36','2020-08-25 14:05:05','50-1598335476-3126803620-40',12.000,12.000,11.400,23.400,0.000,NULL,0.000,'fe6182c17bf1970a154fb0542c1b8491',NULL,NULL,'0',0,'normal',1,'2020-08-25 14:05:27','2020-08-25 06:07:38.412','2020-08-25 06:07:38.412');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (71,'73471439','71450-1598335406-3126798276-50','50-1598335406-3126798276-50','devtest002','testmerchant','714','9109101','_json:{\"1\":\"Baccarat Fresh Room\",\"2\":\"百家乐体验房\",\"3\":\"Baccarat Fresh Room\",\"4\":\"Baccarat Fresh Room\",\"5\":\"Baccarat Fresh Room\"}','2020-08-25 14:03:55',NULL,'2020-08-25 14:03:26','2020-08-25 14:03:55','50-1598335406-3126798276-50',12.000,12.000,-12.000,0.000,0.000,NULL,0.000,'25274b3b47b57c0198e18aa4e8d01295',NULL,NULL,'0',0,'normal',1,'2020-08-25 14:04:27','2020-08-25 06:10:07.114','2020-08-25 06:10:07.114');
INSERT INTO `game_log_t1` (`id`,`uniqueid`,`external_uid`,`game_external_uniqueid`,`username`,`merchant_code`,`game_platform_id`,`game_code`,`game_name`,`game_finish_time`,`game_details`,`bet_time`,`payout_time`,`round_number`,`real_bet_amount`,`effective_bet_amount`,`result_amount`,`payout_amount`,`after_balance`,`bet_details`,`rent`,`md5_sum`,`ip_address`,`bet_type`,`odds_type`,`odds`,`game_status`,`detail_status`,`game_updated_at`,`created_at`,`updated_at`) VALUES (72,'73471441','71450-1598335439-3126800796-45','50-1598335439-3126800796-45','devtest002','testmerchant','714','9109101','_json:{\"1\":\"Baccarat Fresh Room\",\"2\":\"百家乐体验房\",\"3\":\"Baccarat Fresh Room\",\"4\":\"Baccarat Fresh Room\",\"5\":\"Baccarat Fresh Room\"}','2020-08-25 14:04:29',NULL,'2020-08-25 14:03:59','2020-08-25 14:04:29','50-1598335439-3126800796-45',8.000,8.000,8.000,16.000,0.000,NULL,0.000,'fe64c49c17fbd0e19c247cc67667e625',NULL,NULL,'0',0,'normal',1,'2020-08-25 14:04:57','2020-08-25 06:10:07.114','2020-08-25 06:10:07.114');

update `game_log_t1` set game_finish_time = DATE_Add(game_finish_time,INTERVAL  (DATEDIFF(now(), game_finish_time)-2) DAY) where game_platform_id in (714);
update `game_log_t1` set bet_time = DATE_Add(bet_time,INTERVAL  (DATEDIFF(now(), bet_time)-2) DAY) where game_platform_id in (714);
update `game_log_t1` set payout_time = DATE_Add(payout_time,INTERVAL  (DATEDIFF(now(), payout_time)-2) DAY) where game_platform_id in (714);
update `game_log_t1` set game_updated_at = DATE_Add(game_updated_at,INTERVAL  (DATEDIFF(now(), game_updated_at)-2) DAY) where game_platform_id in (714);


-- CQ9
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('d3eecc42-e366-11ea-9e52-0242ac110002',15,'T1GCQ9','1','724cq9404952988050',2,'test002',18.000,0.000,'2020-08-21 12:26:58','2020-08-21 12:26:58','{\"betType\": null, \"betDetails\": \"{\\\"Created At\\\":\\\"2020-08-21 12:28:50\\\"}\", \"gameDetails\": null, \"roundNumber\": \"404952988050\", \"detailStatus\": 1}',1,'1f369f299ec07e36fc07de2980a464c8','2020-08-21 04:28:57.002','2020-08-21 04:28:57.002');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('d3eecacf-e366-11ea-9e52-0242ac110002',15,'T1GCQ9','1','724cq9404952988380',2,'test002',18.000,70.000,'2020-08-21 12:27:10','2020-08-21 12:27:10','{\"betType\": null, \"betDetails\": \"{\\\"Created At\\\":\\\"2020-08-21 12:28:50\\\"}\", \"gameDetails\": null, \"roundNumber\": \"404952988380\", \"detailStatus\": 1}',1,'6aba286f9ee903232c38540b3f5c1a58','2020-08-21 04:28:57.002','2020-08-21 04:28:57.002');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('d3eec956-e366-11ea-9e52-0242ac110002',15,'T1GCQ9','1','724cq9404952989694',2,'test002',0.900,0.000,'2020-08-21 12:27:16','2020-08-21 12:27:16','{\"betType\": null, \"betDetails\": \"{\\\"Created At\\\":\\\"2020-08-21 12:28:50\\\"}\", \"gameDetails\": null, \"roundNumber\": \"404952989694\", \"detailStatus\": 1}',1,'850e890b4fc34b48f6ef708ce6ecadff','2020-08-21 04:28:57.002','2020-08-21 04:28:57.002');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('d3eec7b9-e366-11ea-9e52-0242ac110002',15,'T1GCQ9','1','724cq9404953130029',2,'test002',0.900,0.000,'2020-08-21 12:27:20','2020-08-21 12:27:20','{\"betType\": null, \"betDetails\": \"{\\\"Created At\\\":\\\"2020-08-21 12:28:50\\\"}\", \"gameDetails\": null, \"roundNumber\": \"404953130029\", \"detailStatus\": 1}',1,'b413c84761ada7ab2b3e2d4d96850b95','2020-08-21 04:28:57.002','2020-08-21 04:28:57.002');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('d3eec311-e366-11ea-9e52-0242ac110002',15,'T1GCQ9','1','724cq9404953130420',2,'test002',1.800,0.000,'2020-08-21 12:27:25','2020-08-21 12:27:25','{\"betType\": null, \"betDetails\": \"{\\\"Created At\\\":\\\"2020-08-21 12:28:50\\\"}\", \"gameDetails\": null, \"roundNumber\": \"404953130420\", \"detailStatus\": 1}',1,'4284689e31e603a48c4d72e50c2af71c','2020-08-21 04:28:57.002','2020-08-21 04:28:57.002');
-- ISB
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49396f63-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999686820158877100932244443',2,'test002',0.000,0.000,'2020-08-21 16:48:06','2020-08-21 16:48:06','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159783200780572245\", \"detailStatus\": 1}',1,'6aeb4d86357b544be0a628f2398aeeb6','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49398055-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999686893871515468021026045',2,'test002',5.000,0.000,'2020-08-21 16:48:06','2020-08-21 16:48:06','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159783200780572245\", \"detailStatus\": 1}',1,'ae8d389733de79ab90c8962c0f225071','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('4939858e-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999690511510781933500553022',2,'test002',5.000,0.000,'2020-08-21 16:48:10','2020-08-21 16:48:10','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799968660905765\", \"detailStatus\": 1}',1,'570f30be3036c1103aab7111733c6dfd','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49398b0f-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999690570872004972581039950',2,'test002',0.000,10.000,'2020-08-21 16:48:10','2020-08-21 16:48:10','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799968660905765\", \"detailStatus\": 1}',1,'e0266a497102c9602695fb38ea379e79','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49398ca3-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999703364029192825649548756',2,'test002',0.000,0.000,'2020-08-21 16:48:23','2020-08-21 16:48:23','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799969058359080\", \"detailStatus\": 1}',1,'6db5f3fc21f63869367d3443205cf4f7','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49398e3e-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999703819174569089430464050',2,'test002',5.000,0.000,'2020-08-21 16:48:23','2020-08-21 16:48:23','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799969058359080\", \"detailStatus\": 1}',1,'221efd1ed1e058842316d286d13e489a','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('49398f8d-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999707700292253824131416732',2,'test002',5.000,0.000,'2020-08-21 16:48:27','2020-08-21 16:48:27','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799970347250100\", \"detailStatus\": 1}',1,'4f96e8ef5153ed07bf84849ad326aef9','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('4939909c-e38c-11ea-9e52-0242ac110002',13,'T1GISB','909307','561597999717123258253000682391280',2,'test002',0.000,95.000,'2020-08-21 16:48:37','2020-08-21 16:48:37','{\"betType\": null, \"betDetails\": \"[]\", \"gameDetails\": null, \"roundNumber\": \"02279307devtest002159799970347250100\", \"detailStatus\": 1}',1,'f9644d186ea9e5d7b7d4fb9dffc97503','2020-08-21 08:57:05.164','2020-08-21 08:57:05.164');
-- MGPLUS
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded1376e-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303ECE7BC007D410B00020000000127598E14',2,'test002',0.250,0.000,'2020-08-25 11:25:21','2020-08-25 11:25:21','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03ECE7BC007D410B00020000000127598E14\", \"detailStatus\": 1}',1,'1cecff66281fe8cd1727fdca019a4408','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded13c0a-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303ECE7BC007D410B00020000000127598E15',2,'test002',0.250,0.000,'2020-08-25 11:25:26','2020-08-25 11:25:26','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03ECE7BC007D410B00020000000127598E15\", \"detailStatus\": 1}',1,'2da9e971fa527df3be7feb8de25d1269','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded13da4-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303ECE7BC007D410B00020000000127598E16',2,'test002',0.250,0.000,'2020-08-25 11:25:38','2020-08-25 11:25:38','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03ECE7BC007D410B00020000000127598E16\", \"detailStatus\": 1}',1,'d0e7f6e6e71171fe00f2957200095c2e','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded13f04-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303B512BD007D410B00020000000127599936',2,'test002',0.500,0.000,'2020-08-25 11:25:49','2020-08-25 11:25:49','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03B512BD007D410B00020000000127599936\", \"detailStatus\": 1}',1,'58790748fd97a3c7218aebb19c552819','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded141d2-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303065EBD007D410B00020000000127599F50',2,'test002',0.500,0.000,'2020-08-25 11:25:54','2020-08-25 11:25:54','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03065EBD007D410B00020000000127599F50\", \"detailStatus\": 1}',1,'98fcc3a9d5f59ae92e72bcae2babc455','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('ded14339-e686-11ea-9e52-0242ac110002',10,'T1GMGPLUS','SMG_ladiesNite2TurnWild','521303065EBD007D410B00020000000127599F51',2,'test002',0.250,0.400,'2020-08-25 11:26:01','2020-08-25 11:26:01','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"03065EBD007D410B00020000000127599F51\", \"detailStatus\": 1}',1,'b972b85944fc81878501a4623d6ff0fb','2020-08-25 03:55:52.647','2020-08-25 03:55:52.647');

update `game_log` set bet_time = DATE_Add(bet_time,INTERVAL  (DATEDIFF(now(), bet_time)-1) DAY) WHERE game_api_id in (10, 13, 15);
update `game_log` set settled_time = DATE_Add(settled_time,INTERVAL  (DATEDIFF(now(), settled_time)-1) DAY) WHERE game_api_id in (10, 13, 15);
update `game_log` set created_at = DATE_Add(created_at,INTERVAL  (DATEDIFF(now(), created_at)-1) DAY) WHERE game_api_id in (10, 13, 15);
update `game_log` set updated_at = DATE_Add(updated_at,INTERVAL  (DATEDIFF(now(), updated_at)-1) DAY) WHERE game_api_id in (10, 13, 15);

-- KYCARD
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('4710e9ab-e699-11ea-9e52-0242ac110002',8,'T1GKYCARD','9109101','71450-1598335476-3126803620-40',2,'test002',12.000,23.400,'2020-08-25 14:04:36','2020-08-25 14:05:05','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"50-1598335476-3126803620-40\", \"detailStatus\": 1}',1,'b45a7f2848265c525fc5df7823b0f9cc','2020-08-25 06:07:38.489','2020-08-25 06:07:38.489');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('9fa83d14-e699-11ea-9e52-0242ac110002',8,'T1GKYCARD','9109101','71450-1598335439-3126800796-45',2,'test002',8.000,16.000,'2020-08-25 14:03:59','2020-08-25 14:04:29','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"50-1598335439-3126800796-45\", \"detailStatus\": 1}',1,'90744e4495b5b6999fa0b5d679097883','2020-08-25 06:10:07.120','2020-08-25 06:10:07.120');
INSERT INTO `game_log` (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`bet_details`,`status`,`md5_sum`,`created_at`,`updated_at`) VALUES ('9fa8388e-e699-11ea-9e52-0242ac110002',8,'T1GKYCARD','9109101','71450-1598335406-3126798276-50',2,'test002',12.000,0.000,'2020-08-25 14:03:26','2020-08-25 14:03:55','{\"betType\": null, \"betDetails\": null, \"gameDetails\": null, \"roundNumber\": \"50-1598335406-3126798276-50\", \"detailStatus\": 1}',1,'abf29a057ad379507388acc99069a776','2020-08-25 06:10:07.120','2020-08-25 06:10:07.120');

update `game_log` set bet_time = DATE_Add(bet_time,INTERVAL  (DATEDIFF(now(), bet_time)-2) DAY) WHERE game_api_id in (8);
update `game_log` set settled_time = DATE_Add(settled_time,INTERVAL  (DATEDIFF(now(), settled_time)-2) DAY) WHERE game_api_id in (8);
update `game_log` set created_at = DATE_Add(created_at,INTERVAL  (DATEDIFF(now(), created_at)-2) DAY) WHERE game_api_id in (8);
update `game_log` set updated_at = DATE_Add(updated_at,INTERVAL  (DATEDIFF(now(), updated_at)-2) DAY) WHERE game_api_id in (8);


# Some ohter players unsettle game logs
INSERT IGNORE INTO game_log (`id`,`game_api_id`,`game_api_code`,`game_code`,`external_uid`,`player_id`,`player_username`,`bet`,`payout`,`bet_time`,`settled_time`,`status`, `md5_sum`) values
(UUID(),9, 'T1GPT', 'T1GPT','1123456789',3,'test003',10.000,10.000,DATE_SUB(NOW(), INTERVAL 2 DAY),DATE_SUB(NOW(), INTERVAL 2 DAY), 0,'0123456789'),
(UUID(),9, 'T1GPT', 'T1GPT','8876543210',3,'test003',20.000,20.000,DATE_SUB(NOW(), INTERVAL 2 DAY),DATE_SUB(NOW(), INTERVAL 2 DAY), 0,'9876543210');

# wallet_transaction for report
INSERT IGNORE INTO `wallet_transaction` (`type`, `player_id`,`balance_before`,`balance_after`,`amount`,`game_api_id`,`external_transaction_id`,`internal_transaction_id`,`promo_rule_id`,`note`,`operator_id`,`created_at`) VALUES
(0,1,666.000,766.000,100.000,NULL,'','',null,'note',9,'2018-11-12 01:50:22.646'),
(2,1,766.000,746.000,20.000,NULL,'','',null,'note',9,'2018-11-12 02:50:41.569'),
(3,1,746.000,716.000,30.000,9,'','',null,'note',1,'2018-11-12 08:50:54.967'),
(3,1,716.000,681.000,35.000,15,'','',null,'note',1,'2018-11-12 15:51:07.727'),
(2,1,681.000,694.000,13.000,9,'','',null,'note',1,'2018-11-12 15:51:22.038'),
(2,1,694.000,709.000,15.000,15,'','',null,'note',1,'2018-11-12 15:51:26.892'),
(3,2,1088.000,1068.000,20.000,9,'','',null,'note',1,'2018-11-12 16:07:22.315'),
(2,2,1068.000,1078.000,10.000,9,'','',null,'note',1,'2018-11-12 16:07:28.099'),
(3,2,1078.000,1018.000,60.000,15,'','',null,'note',1,'2018-11-12 23:07:39.917'),
(2,2,1018.000,1048.000,30.000,15,'','',null,'note',1,'2018-11-12 23:07:50.475'),
(1,2,1048.000,1038.000,10.000,NULL,'','',null,'note',1,'2018-11-12 23:08:01.613'),

(5, 2, 3002.340, 3154.340, 152.000, null,'','',null,'note', null, '2020-01-28 16:21:46'),
#TEST002 Deposit_Request
(0, 2, 888.000, 1388.000, 500.000, NULL,'','',null,'note',1, DATE_SUB(NOW(), INTERVAL 1 HOUR)),

(3, 2, 0.000, 2001.340, 2001.340, 15,'','',null,'note', 2, '2020-01-28 9:25:33'),
(3, 2, 0.000, 2002.340, 2002.340, 15,'','',null,'note', 2, '2020-01-28 9:33:13'),
(3, 2, 152.000, 3154.340, 3002.340, 15,'','',null,'note', 2, '2020-01-28 21:41:38'),
(3, 2, 5000.000, 5154.340, 154.340, 15,'','',null,'note', 2, '2020-01-28 22:18:15'),
(3, 2, 5000.000, 5000.340, 0.340, 15,'','',null,'note', 2, '2020-01-28 22:23:38'),
(3, 2, 0.000, 5000.340, 5000.340, 15,'','',null,'note', 2, '2020-01-28 22:25:40'),
(3, 2, 10000.000, 10000.340, 0.340, 15,'','',null,'note', 2, '2020-01-28 22:32:27'),
(3, 2, 0.000, 10000.340, 10000.340, 15,'','',null,'note', 2, '2020-01-28 22:33:34'),
(3, 2, 0.000, 10000.340, 10000.340, 15,'','',null,'note', 2, '2020-01-28 22:45:41'),
(2, 2, 2001.340, 0.000, 2001.340, 15,'','',null,'note', 2, '2020-01-28 9:25:35'),
(2, 2, 2002.340, 0.000, 2002.340, 15,'','',null,'note', 2, '2020-01-28 9:33:15'),
(2, 2, 3154.340, 0.000, 3154.340, 15,'','',null,'note', 2, '2020-01-28 21:41:41'),
(2, 2, 5154.340, 0.000, 5154.340, 15,'','',null,'note', 2, '2020-01-28 22:18:18'),
(2, 2, 5000.340, 0.000, 5000.340, 15,'','',null,'note', 2, '2020-01-28 22:23:40'),
(2, 2, 5000.340, 0.000, 5000.340, 15,'','',null,'note', 2, '2020-01-28 22:25:43'),
(2, 2, 10000.340, 0.000, 10000.340, 15,'','',null,'note', 2, '2020-01-28 22:32:29'),
(2, 2, 10000.340, 0.000, 10000.340, 15,'','',null,'note', 2, '2020-01-28 22:33:36'),
(2, 2, 10000.340, 0.000, 10000.340, 15,'','',null,'note', 2, '2020-01-28 22:45:43'),

(5, 1, 0.500, 73.500, 73.000, null,'','',null,'note', null, '2020-01-28 16:21:48'),
(5, 1, 73.500, 82.500, 9.000, null,'','',null,'note', null, '2020-01-28 16:22:08'),
(0, 1, 0.500, 3900.500, 3900.000, null,'','',null,'note', null, '2020-01-28 9:31:05'),
(0, 1, 0.500, 900.500, 900.000, null,'','',null,'note', null, '2020-01-28 14:46:35'),
(0, 1, 900.500, 3900.500, 3000.000, null,'','',null,'note', null, '2020-01-28 14:50:31'),
(0, 1, 0.500, 2000.500, 2000.000, null,'','',null,'note', null, '2020-01-28 22:26:59'),
(0, 1, 2000.500, 6000.500, 4000.000, null,'','',null,'note', null, '2020-01-28 22:32:45'),
(1, 1, 5500.500, 0.500, 5500.000, null,'','',null,'note', null, '2020-01-28 9:42:15'),
(1, 1, 6000.500, 0.500, 6000.000, null,'','',null,'note', null, '2020-01-28 14:58:41'),
(1, 1, 8000.500, 0.500, 8000.000, null,'','',null,'note', null, '2020-01-28 22:46:21'),

(3, 1, 0.500, 5500.500, 5500.000, 15,'','',null,'note', 1, '2020-01-28 9:38:48'),
(3, 1, 0.500, 6000.500, 6000.000, 15,'','',null,'note', 1, '2020-01-28 14:56:27'),
(3, 1, 0.500, 8000.500, 8000.000, 15,'','',null,'note', 1, '2020-01-28 22:43:26'),
(2, 1, 3900.500, 0.500, 3900.000, 15,'','',null,'note', 1, '2020-01-28 9:31:22'),
(2, 1, 900.500, 0.500, 900.000, 15,'','',null,'note', 1, '2020-01-28 14:46:43'),
(2, 1, 3000.500, 0.500, 3000.000, 15,'','',null,'note', 1, '2020-01-28 14:50:41'),
(2, 1, 82.500, 0.500, 82.000, 15,'','',null,'note', 1, '2020-01-28 17:35:16'),
(2, 1, 2000.500, 0.500, 2000.000, 15,'','',null,'note', 1, '2020-01-28 22:27:07'),
(2, 1, 4000.500, 0.500, 4000.000, 15,'','',null,'note', 1, '2020-01-28 22:32:59');

update `wallet_transaction` set created_at = DATE_Add(created_at,INTERVAL  (DATEDIFF(now(),created_at)-1) DAY);

# subwallet_history for report
INSERT IGNORE INTO `subwallet_history` (`type`,`player_id`,`game_api_id`,`balance_before`,`sync_success`,`balance_after`,`created_at`) VALUES
(6,1,1,99.00,1,130.00,'2018-11-12 15:50:51.344'),
(6,1,2,101.00,1,150.00,'2018-11-12 15:51:03.488'),
(6,1,1,130.00,1,160.00,'2018-11-12 15:51:18.229'),
(6,1,2,150.00,1,185.00,'2018-11-12 15:51:23.258'),
(6,2,1,99.00,1,140.00,'2018-11-12 16:07:15.860'),
(6,2,1,140.00,1,160.00,'2018-11-12 16:07:23.771'),
(6,2,2,101.00,0,101.00,'2018-11-12 16:07:33.218'),
(6,2,2,160.00,0,160.00,'2018-11-12 16:07:46.555'),
(6,2,2,101.00,1,160.00,'2018-11-12 16:07:33.218'),
(6,2,2,160.00,1,220.00,'2018-11-12 16:07:46.555'),
(2,1,1,800.00,1,130.00,'2018-11-12 02:07:46.555'),

(6,1,15,99.00,1,130.00,'2018-11-12 02:07:51.344'),
(3,1,15,800.00,1,100.00,'2018-11-12 02:50:46.555'),
(2,1,15,130.00,1,300.00,'2018-11-12 03:35:18.229'),
(2,1,15,101.00,1,150.00,'2018-11-12 03:40:03.488'),
(2,1,15,150.00,1,185.00,'2018-11-12 03:43:59.258'),
(2,1,15,50.00,1,200.00,'2018-11-12 15:45:23.235'),
(3,1,15,50.00,1,0.00,'2018-11-12 15:51:49.245'),
(2,1,15,0.00,1,150.00,'2018-11-12 16:30:58.634'),
(2,1,15,30.00,1,185.00,'2018-11-12 16:46:02.845'),
(2,1,15,30.00,1,185.00,'2018-11-12 16:46:02.845'),
(6,1,15,99.00,1,130.00,'2018-11-12 17:07:51.344'),
(3,1,15,800.00,1,100.00,'2018-11-12 17:50:46.555'),
(2,1,15,130.00,1,300.00,'2018-11-12 17:35:18.229'),
(2,1,15,101.00,1,150.00,'2018-11-12 18:40:03.488'),
(2,1,15,150.00,1,185.00,'2018-11-12 18:43:59.258'),
(2,1,15,50.00,1,200.00,'2018-11-12 18:45:23.235'),
(3,1,15,50.00,1,0.00,'2018-11-12 18:51:49.245'),
(2,1,15,0.00,1,150.00,'2018-11-12 19:30:58.634'),
(2,1,15,30.00,1,185.00,'2018-11-12 19:46:02.845'),

(2,2,15,0.00,1,140.00,'2018-11-12 06:48:15.860'),
(2,2,15,140.00,1,160.00,'2018-11-12 06:07:51.771'),
(2,2,15,20.00,1,160.00,'2018-11-12 08:34:31.771'),
(3,2,15,140.00,1,50.00,'2018-11-12 09:26:32.771'),
(2,2,15,30.00,1,160.00,'2018-11-12 09:40:45.771'),
(2,2,15,101.00,1,200.00,'2018-11-12 09:32:33.218'),
(2,2,15,160.00,1,500.00,'2018-11-12 10:41:46.555'),
(2,2,15,106.00,1,400.00,'2018-11-12 16:15:33.218'),
(2,2,15,160.00,1,700.00,'2018-11-12 16:34:46.555'),
(2,2,15,160.00,1,700.00,'2018-11-12 16:37:29.555'),
(2,2,15,240.00,1,500.00,'2018-11-12 16:48:15.860'),
(2,2,15,0.00,1,200.00,'2018-11-12 16:57:51.771'),
(6,1,15,190.00,1,190.00,'2018-11-12 17:07:51.344'),
(2,2,15,35.00,1,750.00,'2018-11-12 17:12:31.771'),
(3,2,15,400.00,1,100.00,'2018-11-12 17:26:32.771'),
(2,2,15,30.00,1,400.00,'2018-11-12 17:40:45.771'),
(2,2,15,120.00,1,500.00,'2018-11-12 17:56:33.218'),
(2,2,15,60.00,1,500.00,'2018-11-12 18:15:46.555'),
(2,2,15,140.00,1,400.00,'2018-11-12 18:41:33.218'),
(2,2,15,160.00,1,700.00,'2018-11-12 19:49:46.555'),
(2,2,15,50.00,1,350.00,'2018-11-12 19:53:39.555');

update `subwallet_history` set created_at = DATE_Add(created_at,INTERVAL (DATEDIFF(now(),created_at)-1) DAY);

# promo_bonus
INSERT IGNORE INTO promo_bonus(player_id, bonus_date, rule_id, promo_type, bonus_amount, reference_amount, status, comment) values
(1, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(1, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(1, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(2, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(2, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 1 DAY), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 2 DAY), 2, 1, 12.000, 12000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 3 DAY), 2, 1, 13.000, 13000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 4 DAY), 2, 1, 14.000, 14000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 5 DAY), 2, 1, 15.000, 15000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 6 DAY), 2, 1, 29.000, 29000.000, 1, 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 6 DAY), 2, 2, 29.000, 29000.000, 1, 'unit test'),
(2, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(3, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(3, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(3, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(4, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(4, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(4, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(5, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(5, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(5, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(6, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(6, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(6, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(7, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(7, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(7, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(8, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(8, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(8, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(9, CURDATE(), 2, 1, 11.000, 11000.000, 1, 'unit test'),
(9, CURDATE(), 2, 1, 12.000, 12000.000, 10, 'unit test'),
(9, CURDATE(), 2, 1, 10.000, 10000.000, 0, 'unit test'),
(10, date_sub(curdate(), interval 1 day), 2, 1, 10.000, 10000.000, 10, 'unit test');

INSERT IGNORE INTO promo_bonus(player_id, bonus_date, rule_id, promo_type, bonus_amount, reference_amount, `status`, `created_at`, comment) values
(3, CURDATE(), 1, 12, 10.000, 0, 0, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'unit test'),
(3, CURDATE(), 2, 12, 8.000, 0, 0, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 2 WEEK), 3, 12, 38.000, 0, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'unit test'),
(2, DATE_SUB(CURDATE(), INTERVAL 3 WEEK), 3, 12, 38.000, 0, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'unit test'),
(2, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 1, 11, 180.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'unit test'),
(2, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 0, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(2, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 0, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(2, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(3, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(5, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(6, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(3, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(5, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(6, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(7, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(8, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(9, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(14, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(16, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(17, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(18, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(19, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test'),
(20, DATE_SUB(NOW(), INTERVAL RAND() * 24 * 60 MINUTE), 2, 11, 38.000, 100.00, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 24 + 24 HOUR), 'unit test');

# bonuses for login campaign test case
INSERT IGNORE INTO promo_bonus(player_id, bonus_date, rule_id, promo_type, bonus_amount, reference_amount, login_day, `status`, `created_at`, comment) values
(2, DATE_SUB(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 8 HOUR), '%Y-%m-%d'), interval 24 + 8 hour), 2, 14, 18.000, 0, 1, 1, DATE_SUB(CURDATE(), INTERVAL RAND() * 240 + 240 HOUR), 'for login campaign unit test');

# Transaction 里面的 bonus 记录要与 bonus 里面的approved对应
INSERT INTO `wallet_transaction` (`type`,`player_id`,`balance_before`,`balance_after`,`amount`,`game_api_id`,`external_transaction_id`,`internal_transaction_id`,`promotion_type`,`promo_rule_id`,`note`,`operator_id`) VALUES
(5,2,327.000,338.000,11.000,NULL,NULL,NULL,1,2,'This transaction was made by superadmin.',1),
(4,2,338.000,376.000,38.000,NULL,NULL,NULL,11,2,'This transaction was made by superadmin.',1),
(4,2,376.000,556.000,180.000,NULL,NULL,NULL,11,1,'This transaction was made by superadmin.',1);

# Referral rule
INSERT IGNORE INTO `promo_player_referral_setting`
(`id`,`name`,`content`,`player_credential_id`,`referral_type`,`fixed_bonus`, `bonus_percentage`,`bonus_auto_release`,`min_first_deposit_amount`,`min_bonus_amount`,`max_bonus_amount`,`withdraw_condition_multiplier`,`currency`,`enabled`) VALUES
(1,'default','default CNY revenue',null,2,null,1.000,0,null,10,1000,1,'CNY',1),
(2,'default','default THB revenue',null,1,null,1.000,0,null,10,1000,1,'THB',1),
(3,'default','default USD revenue',null,0,38,0,1,100,null,null,1,'USD',1),
(4,'存款抽成','存款抽成',1,1,null,5.000,1,null,10,100,1,'CNY',1),
(5,'流水抽成','流水抽成',2,2,null,4.000,1,null,10,100,1,'CNY',1),
(6,'單次推薦獎金','單次推薦獎金',3,0,88,0,1,100,null,null,1,'CNY',1);

# Cashback
INSERT IGNORE INTO `promo_cashback_setting` (`id`, `execution_type`, `min_bonus_amount`, `max_bonus_amount`, `withdraw_condition_multiplier`, `enabled`)
VALUES
(1, 1, '[{\"amount\": 100.0, \"currency\": \"CNY\"}, {\"amount\": 20.0, \"currency\": \"USD\"}]', '[{\"amount\": 10000.0, \"currency\": \"CNY\"}, {\"amount\": 2000.0, \"currency\": \"USD\"}]', 10, 1);

INSERT IGNORE INTO `promo_cashback` (`id`,`currency`, `name`,`bonus_auto_release`,`enabled`,`group_id`) VALUES
(1,'CNY','Normal Cashback',0,1,1),
(2,'CNY','High Cashback',1,1,1),
(3,'CNY','Normal Cashback',0,1,2),
(4,'CNY','High Cashback',1,1,2),
(5,'CNY','default Normal Cashback',0,1,null),
(6,'CNY','default High Cashback',0,1,null);

INSERT IGNORE INTO `promo_cashback_game_type` (`id`,`rule_id`,`game_api_id`,`game_type_id`) VALUES
(1,1,5,1),
(2,1,5,2),
(3,1,5,3),
(4,1,5,4),
(5,2,9,1),
(6,2,9,2),
(7,2,9,3),
(8,2,9,4),
(9,5,5,1),
(10,5,5,2),
(11,5,5,3),
(12,5,5,4),
(13,6,5,1),
(14,6,5,2),
(15,6,5,3),
(16,6,5,4);

INSERT IGNORE INTO `promo_cashback_bet_range` (`id`,`rule_id`,`limit`,`cashback_percentage`) VALUES
(1,1,50,10.000),
(2,1,100,10.000),
(3,1,200,10.000),
(4,1,400,10.000),
(5,2,0,20.000),
(6,2,100,5.000),
(7,2,500,5.000),
(8,3,50,10.000),
(9,3,100,10.000),
(10,3,200,10.000),
(11,3,400,10.000),
(12,4,0,20.000),
(13,4,100,5.000),
(14,4,500,5.000),
(15,5,50000,1.000),
(16,6,50000,1.000);

# promo_campaign_deposit
INSERT IGNORE INTO `promo_campaign_deposit` (`id`, `name`, `content`, `start_time`, `effective_start_time`, `end_time`, `effective_end_time`, `fixed_bonus`, `max_bonus_amount`, `percentage_bonus`, `bonus_auto_release`, `min_deposit`, `auto_join`,`currency`, `status`, `bonus_receive_cycle`)
VALUES
(1, '首充大礼包100送180', '充值100送180，第1次充值限定', timestamp(DATE_SUB(NOW(), INTERVAL 180 MINUTE)), null, timestamp(DATE_ADD(NOW(), INTERVAL 30 DAY)), null, 180, null, null, 1, 100, 1, 'CNY', 10, 1),
(2, '充100送38', '每次充值皆回馈38', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, null, null, 38, 100, 0, 0, 100, 0, 'CNY', 10, 1),
(3, '充200送68', '每次充值皆回馈68', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, null, null, 68, 100, 0, 0, 200, 0, 'CNY', 1, 1),
(4, '充300以上送10%', '每次充值皆回馈10%', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, null, null, null, 100, 10, 0, 300, 0, 'CNY', 0, 1),
(5, '充300以上送10%', '每次充值皆回馈10%', timestamp(DATE_SUB(NOW(), INTERVAL 180 MINUTE)), null, timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, null, 100, 10, 0, 300, 0, 'CNY', 10, 1),
(6, '充300以上送10%', '每次充值皆回馈10%', timestamp(DATE_ADD(NOW(), INTERVAL 180 MINUTE)), null, null, null, null, 100, 10, 0, 300, 0, 'CNY', 10, 1);

# promo_campaign_task
INSERT IGNORE INTO `promo_campaign_task` (`task_type`, `name`, `content`, `start_time`, `effective_start_time`, `end_time`, `effective_end_time`, `fixed_bonus`, `bonus_auto_release`, `withdraw_condition_multiplier`, `status`, `bonus_receive_cycle`, `auto_join`, `currency`)
VALUES
(0,'邮件验证奖金', '成功验证邮件送18，1次限定, 一倍流水取款条件', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, null, null, 18, 1, 1, 10, 1, 0, 'CNY'),
(1,'手机验证奖金', '成功验证手机号码送28，1次限定，两倍流水取款条件', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, timestamp(DATE_ADD(NOW(), INTERVAL 30 DAY)), null, 28, 1, 2, 10, 1, 0, 'CNY'),
(2,'身份验证奖金', '成功验证身份送38，1次限定，三倍流水取款条件' , timestamp(DATE_ADD(NOW(), INTERVAL 2 DAY)), null, null, null, 38, 1, 3, 10, 3, 0, 'CNY'),
(0,'邮件验证奖金', '成功验证邮件送18，1次限定, 一倍流水取款条件', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, timestamp(DATE_ADD(NOW(), INTERVAL 30 DAY)), null, 18, 1, 1, 0, 1, 0, 'CNY'),
(1,'手机验证奖金', '成功验证手机号码送28，1次限定，两倍流水取款条件', timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)), null, timestamp(DATE_ADD(NOW(), INTERVAL 30 DAY)), null, 28, 1, 2, 0, 1, 0, 'CNY'),
(2,'身份验证奖金', '成功验证身份送38，1次限定，三倍流水取款条件' , timestamp(DATE_SUB(NOW(), INTERVAL 7 DAY)), null, timestamp(DATE_ADD(NOW(), INTERVAL 30 DAY)), null, 38, 1, 3, 1, 1, 0, 'CNY');

# promo_campaign_login
INSERT IGNORE INTO `promo_campaign_login` (`name`, `content`, `start_time`, `effective_start_time`, `end_time`, `effective_end_time`, `bonus_auto_release`, `withdraw_condition_multiplier`, `status`, `currency`)
VALUES
('登入优惠', '连续登录：1天-送38, 2天-送58, 3天-送68, 每週刷新, 3x流水取款条件' , date_sub(now(), interval 7 day), null, null, null, 1, 3, 0, 'CNY'),
('登入优惠', '连续登录：1天-送18, 2天-送28, 3天-送38, 每週刷新, 2x流水取款条件' , date_sub(now(), interval 7 day), null, null, null, 1, 2, 10, 'CNY');

# campaign_login_rule
INSERT IGNORE INTO `promo_campaign_login_rule` (`campaign_id`, `login_day`, `fixed_bonus`)
VALUES
(1, 1, 38),
(1, 2, 58),
(1, 3, 68),
(2, 1, 18),
(2, 2, 28),
(2, 3, 38);

# player_stats
INSERT IGNORE INTO `player_stats` (`player_id`, `dirty`)
select player_id, 0 from `wallet_transaction` where `type` in (0,1) group by player_id;

update `player_stats`
left join (select player_id, count(1) totalcount, sum(amount) amount, min(created_at) firstdate, max(created_at) lastdate from wallet_transaction where `type`=0 group by player_id) wg on player_stats.player_id = wg.player_id
set `total_deposit_count`=IFNULL(wg.totalcount,0), `total_deposit_amount`=IFNULL(wg.amount,0), `first_deposit_date`=wg.firstdate, `last_deposit_date`=wg.lastdate;

update `player_stats`
left join (select player_id, count(1) totalcount, sum(amount) amount, min(created_at) firstdate, max(created_at) lastdate from wallet_transaction where `type`=1 group by player_id) wg on player_stats.player_id = wg.player_id
set `total_withdraw_count`=IFNULL(wg.totalcount,0), `total_withdraw_amount`=IFNULL(wg.amount,0), `first_withdraw_date`=wg.firstdate, `last_withdraw_date`=wg.lastdate;

update `player_stats`
left join (select player_id, sum(bonus_amount) amount from promo_bonus where promo_type=1 and `status`=1 group by player_id) wg on player_stats.player_id = wg.player_id
set `total_cashback_bonus`=IFNULL(wg.amount, 0);

update `player_stats`
left join (select player_id, sum(bonus_amount) amount from promo_bonus where promo_type=2 and `status`=1 group by player_id) wg on player_stats.player_id = wg.player_id
set `total_referral_bonus`=IFNULL(wg.amount, 0);

update `player_stats`
left join (select player_id, sum(bonus_amount) amount from promo_bonus where promo_type >10 and `status`=1 group by player_id) wg on player_stats.player_id = wg.player_id
set `total_campaign_bonus`=IFNULL(wg.amount, 0);

# Announcement
INSERT IGNORE into announcement (`id`, `message`) VALUES
(1, 'Welcome to TripleOneTech ICE Demo'),
(2, 'Notice: MG Game Maintenance for 1 hour'),
(3, 'Notice: AGIN Game Maintenance coming up next week');

# Dashboard
# summary_daily
INSERT IGNORE into summary_daily (`timezone`, `currency`, `start_date`, `total_player_count`, `total_deposit_count`, `total_deposit_amount`, `total_withdrawal_count`, `total_withdrawal_amount`, `bonus`, `cashback`, `bet_amount`, `new_player_without_deposit`, `new_player_with_deposit`, `active_player_count`, `active_player_count_by_gametype`, `player_total_win`, `player_total_loss`, `created_at`, `updated_at`)
values
('UTC', 'CNY', '2018-11-11', '3', '3', '333.333', '3', '333.333', '33.333', '33.333', '333.333', '3', '3', '3', '{"Slot Game": 3}', '333.333', '333.333', now(), now()),
('UTC', 'CNY', '2019-02-04', '3', '3', '333.333', '3', '333.333', '33.333', '33.333', '333.333', '3', '3', '3', '{"Slot Game": 3}', '333.333', '333.333', now(), now()),
('UTC', 'CNY', '2019-02-11', '3', '3', '333.333', '3', '333.333', '33.333', '33.333', '333.333', '3', '3', '3', '{"Slot Game": 3}', '333.333', '333.333', now(), now()),
('UTC', 'CNY', '2019-02-18', '3', '3', '333.333', '3', '333.333', '33.333', '33.333', '333.333', '3', '3', '3', '{"Slot Game": 3}', '333.333', '333.333', now(), now()),
('UTC', 'CNY', '2019-02-25', '3', '3', '333.333', '3', '333.333', '33.333', '33.333', '333.333', '3', '3', '3', '{"Slot Game":  3}', '333.333', '333.333', now(), now()),
('UTC', 'CNY', CURRENT_DATE(), '162452', '61', '99491.440', '26', '89856.000', '27361.99', '165323.00', '22072141.05', '13', '10', '150', '{"Slot Game": 12,"Live Casino": 56, "Fishing": 7, "Lottery": 1, "Sports": 53}', '11388967.770', '11404927.120', now(), now());

# summary_bet_gross_profit_status
INSERT IGNORE into summary_bet_gross_profit_status (`period`, `timezone`, `currency`, `start_date`, `bet_amount`, `player_win`, `player_loss`)
values
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 31 DAY), '21870617.02', '11376267.48', '11757963.60'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY), '29177338.45', '14254813.67', '15589924.62'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 29 DAY), '21958550.73', '11521997.89', '11705851.65'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY), '22970100.15', '12012676.42', '11719190.61'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 27 DAY), '21867074.15', '10917380.05', '11455966.34'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 26 DAY), '22773084.53', '11588403.24', '11357410.85'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 25 DAY), '29855832.46', '15192414.06', '15424857.58'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 24 DAY), '22889418.62', '11702689.94', '11947444.23'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 23 DAY), '25638286.94', '12698343.78', '13375404.63'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 22 DAY), '32229695.25', '15914403.33', '17250699.87'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY), '29919904.39', '15960975.55', '14729068.07'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 20 DAY), '24943472.90', '12093763.99', '13103539.94'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 19 DAY), '22737710.86', '11652092.23', '12018697.02'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 18 DAY), '29833946.18', '15116779.19', '15628979.05'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 17 DAY), '27107878.18', '13953825.79', '14343633.36'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 16 DAY), '23228192.96', '11607901.97', '12013309.43'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 15 DAY), '24575637.20', '11710874.42', '12802104.51'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY), '27566335.24', '14760594.44', '13923502.07'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 13 DAY), '23563081.67', '12333998.72', '12081190.85'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 12 DAY), '35759477.07', '17085325.62', '18846812.46'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 11 DAY), '23808137.50', '12182432.01', '11827735.29'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 10 DAY), '23368084.56', '12724284.73', '12407797.34'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 9 DAY), '20673139.07', '10494675.57', '10868232.15'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '21959783.97', '10667493.85', '11866579.86'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY), '20524444.76', '10743236.25', '10527241.64'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY), '20478138.54', '10757285.52', '10690856.32'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 5 DAY), '22875877.04', '11609025.48', '12431064.57'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 4 DAY), '20833252.81', '10723922.25', '10867298.03'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 3 DAY), '18889099.40', '10047277.17', '10273394.13'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 2 DAY), '15894771.46', '8284726.72', '8326678.90'),

('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*8 DAY), '166571205.500', '84654733.550', '87035255.060'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*7 DAY), '178612782.300', '91136066.760', '92811416.290'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*6 DAY), '188249695.100', '95150993.890', '97188425.170'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*5 DAY), '156228436.00', '78986862.160', '82631435.360'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*4 DAY), '164193661.600', '82873428.010', '85465727.800'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*3 DAY), '200431530.800', '98919348.770', '102233577.600'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*2 DAY), '184847389.100', '92184356.010', '95371970.610'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7 DAY), '172836267.500', '87280708.270', '90200635.840'),

('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 12 MONTH), '817131668.060', '415330777.950', '432084294.270'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 11 MONTH), '1394629428.350', '682604965.210', '710253698.120'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 10 MONTH ), '1329476502.880', '656452086.030', '679683204.070'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 9 MONTH ), '757580445.900', '377237150.680', '393528680.410'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 8 MONTH ), '645418647.050', '344216935.840', '354961499.100'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 7 MONTH), '635042709.820', '332107574.110', '342404443.600'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 6 MONTH), '635139743.570', '324245966.910', '335095395.250'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 5 MONTH), '665129139.560', '332736540.610', '347128830.240'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 4 MONTH), '802050127.780', '406512616.550', '427494973.430'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 3 MONTH), '923790413.930', '464833932.110', '482393950.240'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 2 MONTH), '751680305.590', '382733461.350', '392300212.730'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 1 MONTH), '760693604.400', '380161567.710', '393674450.340');

# summary_player_status
INSERT IGNORE into summary_player_status (`period`, `timezone`, `currency`, `start_date`, `new_player_without_deposit`, `new_player_with_deposit`, `total_player_count`)
values
('LAST_WEEK', 'UTC', 'CNY', '2018-11-11', '3', '3', '3'),

('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 31 DAY), '156', '134', '150873'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY), '114', '120', '150583'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 29 DAY), '103', '113', '150349'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY), '66', '95', '150133'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 27 DAY), '42', '66', '149972'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 26 DAY), '55', '104', '149864'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 25 DAY), '151', '128', '149705'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 24 DAY), '76', '105', '149426'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 23 DAY), '61', '89', '149245'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 22 DAY), '75', '109', '149095'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY), '88', '111', '148911'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 20 DAY), '35', '64', '148712'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 19 DAY), '95', '100', '148613'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 18 DAY), '123', '93', '148418'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 17 DAY), '94', '113', '148202'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 16 DAY), '120', '117', '147995'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 15 DAY), '101', '88', '147758'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY), '109', '65', '147569'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 13 DAY), '14', '50', '147395'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 12 DAY), '60', '88', '147331'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 11 DAY), '106', '89', '147183'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 10 DAY), '94', '86', '146988'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 9 DAY), '125', '91', '146808'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '136', '89', '146592'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY), '89', '80', '146367'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY), '39', '50', '146198'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 5 DAY), '103', '92', '146109'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 4 DAY), '124', '74', '145914'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 3 DAY), '130', '78', '145716'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 2 DAY), '108', '84', '145508'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY), '124', '70', '145316'),

('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*8 DAY), '649', '573', '147331'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*7 DAY), '656', '626', '148613'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*6 DAY), '541', '710', '149864'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*5 DAY), '780', '804', '151448'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*4 DAY), '473', '748', '152669'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*3 DAY), '609', '722', '154000'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*2 DAY), '421', '697', '155118'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7 DAY), '546', '826', '156490'),

('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 12 MONTH), '2015', '2847', '161352'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 11 MONTH), '2348', '3269', '156490'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 10 MONTH), '2916', '2835', '150873'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 9 MONTH), '5563', '2777', '145122'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 8 MONTH), '3603', '2568', '136782'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 7 MONTH), '1812', '1811', '130611'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 6 MONTH), '1684', '1228', '126988'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 5 MONTH), '1070', '2280', '124076'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 4 MONTH), '847', '2713', '120726'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 3 MONTH), '970', '3012', '117166'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 2 MONTH), '526', '1336', '113184'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 1 MONTH), '682', '1418', '111322');

# summary_net_profit
INSERT IGNORE into summary_net_profit (`period`, `timezone`, `currency`, `start_date`, `cashback`, `bonus`, `player_loss`, `player_win`)
values
('LAST_WEEK', 'UTC', 'CNY', '2018-11-11', '33.333', '33.333', '33.333', '33.333'),

('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 31 DAY), '225308.000', '37563.850', '11757963.600', '11376267.480'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY), '166519.000', '54612.400', '15589924.620', '14254813.670'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 29 DAY), '153629.000', '30315.000', '11705851.650', '11521997.890'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY), '172941.000', '31208.260', '11719190.610', '12012676.420'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 27 DAY), '167383.000', '17186.820', '11455966.340', '10917380.050'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 26 DAY), '187682.000', '35827.000', '11357410.850', '11588403.240'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 25 DAY), '184873.000', '52847.650', '15424857.580', '15192414.060'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 24 DAY), '168985.000', '43602.700', '11947444.230', '11702689.940'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 23 DAY), '234762.000', '57892.050', '13375404.630', '12698343.780'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 22 DAY), '238754.000', '34900.040', '17250699.870', '15914403.330'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY), '235167.000', '45008.930', '14729068.070', '15960975.550'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 20 DAY), '130552.000', '59512.100', '13103539.940', '12093763.990'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 19 DAY), '215497.000', '26300.000', '12018697.020', '11652092.230'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 18 DAY), '208649.000', '47260.570', '15628979.050', '15116779.190'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 17 DAY), '174535.000', '52425.820', '14343633.360', '13953825.790'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 16 DAY), '160887.000', '39376.780', '12013309.430', '11607901.970'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 15 DAY), '184257.000', '31244.000', '12802104.510', '11710874.420'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY), '237704.000', '31651.160', '13923502.070', '14760594.440'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 13 DAY), '234078.000', '38493.400', '12081190.850', '12333998.720'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 12 DAY), '234899.000', '46297.880', '18846812.460', '17085325.620'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 11 DAY), '146194.000', '31040.900', '11827735.290', '12182432.010'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 10 DAY), '188878.000', '99995.540', '12407797.340', '12724284.730'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 9 DAY), '149896.000', '24563.000', '10868232.150', '10494675.570'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '139317.000', '30930.330', '11866579.860', '10667493.850'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY), '169446.000', '50945.840', '10527241.640', '10743236.250'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY), '172342.000', '16200.790', '10690856.320', '10757285.520'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 5 DAY), '182659.000', '20823.000', '12431064.570', '11609025.480'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 4 DAY), '139504.000', '18823.200', '10867298.030', '10723922.250'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 3 DAY), '114566.000', '29830.580', '10273394.130', '10047277.170'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 2 DAY), '158305.000', '28805.660', '8326678.900', '8284726.720'),
('TODAY', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY), '150028.000', '19062.000', '11137783.760', '11043580.020'),

('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*8 DAY), '1200972.000', '299974.280', '87035255.060', '84654733.550'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*7 DAY), '1415607.000', '266751.730', '92811416.290', '91136066.760'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*6 DAY), '1380775.000', '329590.470', '97188425.170', '95150993.890'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*5 DAY), '1141559.000', '244633.730', '82631435.360', '78986862.160'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*4 DAY), '1145870.000', '291729.800', '85465727.800', '82873428.010'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*3 DAY), '1436120.000', '333555.490', '102233577.600', '98919348.770'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7*2 DAY), '1380751.000', '78735.850', '95371970.610', '92184356.010'),
('LAST_WEEK', 'UTC', 'CNY', DATE_SUB(DATE_ADD(CURRENT_DATE, INTERVAL(1-DAYOFWEEK(CURRENT_DATE)) DAY),interval 7 DAY), '1334109.000', '214934.840', '90200635.840', '87280708.270'),

('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 12 MONTH), '6272711.220', '1010457.260', '432084294.270', '415330777.950'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 11 MONTH), '5552629.000', '992703.380', '710253698.120', '682604965.210'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 10 MONTH), '5628196.000', '1184547.250', '679683204.070', '656452086.030'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 9 MONTH), '7701714.000', '1098577.480', '393528680.410', '377237150.680'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 8 MONTH), '6387955.000', '1243451.810', '354961499.100', '344216935.840'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 7 MONTH), '5226753.000', '889881.730', '342404443.600', '332107574.110'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 6 MONTH), '5174901.000', '618030.820', '335095395.250', '324245966.910'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 5 MONTH), '4660070.000', '1808685.420', '347128830.240', '332736540.610'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 4 MONTH), '4730718.000', '1306303.470', '427494973.430', '406512616.550'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 3 MONTH), '6557991.000', '875046.240', '482393950.240', '464833932.110'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 2 MONTH), '13254923.000', '552172.160', '392300212.730', '382733461.350'),
('LAST_MONTH', 'UTC', 'CNY', DATE_SUB(DATE_FORMAT(NOW(),'%Y-%m-01'), INTERVAL 1 MONTH), '14016178.260', '457647.760', '393674450.340', '380161567.710');

# summary_top_deposit
INSERT IGNORE into summary_top_deposit (`period`, `timezone`, `currency`, `start_date`, `category`, `rank`, `player_username`, `deposit_count`, `deposit_method`, `deposit_amount`)
values
-- ('LAST_7_DAYS', 'UTC', 'CNY', '2018-11-11', 'BY_COUNT', '1', 'test002', '3', 'Bank Deposit', '333.333'),

('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '1', 'hechuan888', '27', 'Bank Deposit', '189000.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '2', 'hao454', '21', 'Bank Deposit', '23493.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '3', 'wwq5375116', '9', 'Bank Deposit', '5782.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '4', 'iphone19930820', '9', 'Bank Deposit', '1671.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '5', 'trytry88', '7', 'Bank Deposit', '27000.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '6', 'tt666887', '6', 'Bank Deposit', '3800.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '7', 'nicesori', '6', 'Bank Deposit', '61000.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '8', 'w317612', '6', 'Bank Deposit', '730.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '9', 'pokerliu', '6', 'Bank Deposit', '18000.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '10', 'gejinbo11', '5', 'Bank Deposit', '11000.000'),

('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '1', 'test001', '189', 'Bank Deposit', '1323000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '2', 'test002', '147', 'Bank Deposit', '164451'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '3', 'test003', '63', 'Bank Deposit', '40474'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '4', 'test004', '63', 'Bank Deposit', '11697'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '5', 'test005', '49', 'Bank Deposit', '189000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '6', 'test006', '42', 'Bank Deposit', '26600'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '7', 'test007', '42', 'Bank Deposit', '427000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '8', 'test008', '42', 'Bank Deposit', '5110'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '9', 'test009', '42', 'Bank Deposit', '126000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '10', 'test009', '35', 'Bank Deposit', '77000'),

('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '1', 'hechuan888', '837', 'Bank Deposit', '5859000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '2', 'hao454', '651', 'Bank Deposit', '728283'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '3', 'wwq5375116', '279', 'Bank Deposit', '179242'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '4', 'iphone19930820', '279', 'Bank Deposit', '51801'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '5', 'trytry88', '217', 'Bank Deposit', '837000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '6', 'tt666887', '186', 'Bank Deposit', '117800'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '7', 'nicesori', '186', 'Bank Deposit', '1891000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '8', 'w317612', '186', 'Bank Deposit', '22630'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '9', 'pokerliu', '186', 'Bank Deposit', '558000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '10', 'gejinbo11', '155', 'Bank Deposit', '341000');

# summary_top_withdrawal
INSERT IGNORE into summary_top_withdrawal (`period`, `timezone`, `currency`, `start_date`, `category`, `rank`, `player_username`, `withdrawal_count`, `withdrawal_amount`)
values
-- ('LAST_7_DAYS', 'UTC', 'CNY', '2018-11-11', 'BY_COUNT', '1', 'test002', '3', '333.333'),
-- ('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '1', 'test001', '3', '333.333'),
-- ('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '2', 'test002', '3', '333.333'),
-- ('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '3', 'test003', '3', '333.333'),

('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '1', 'hechuan888', '8', '150600.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '2', 'zuandian888', '7', '14000.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '3', 'trytry88', '5', '40448.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '4', 'wwq5375116', '3', '5200.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '5', 'tt666887', '3', '3034.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '6', 'jy05742246', '3', '14206.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '7', 'andy999lhl', '3', '4800.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '8', 'haoli17', '3', '9125.00'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '9', 'iphone19930820', '3', '2584.50'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_COUNT', '10', 'whttlj888', '2', '2715.00'),

('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '1', 'hechuan888', '32', '557220.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '2', 'zuandian888', '28', '51800.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '3', 'trytry88', '20', '149657.600'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '4', 'wwq5375116', '12', '19240.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '5', 'tt666887', '12', '11225.800'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '6', 'jy05742246', '12', '52562.200'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '7', 'andy999lhl', '12', '17760.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '8', 'haoli17', '12', '33762.500'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '9', 'iphone19930820', '12', '9562.650'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_COUNT', '10', 'whttlj888', '8', '10045.500'),

('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '1', 'hechuan888', '136', '1445760.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '2', 'zuandian888', '119', '134400.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '3', 'trytry88', '85', '388300.800'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '4', 'wwq5375116', '51', '49920.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '5', 'tt666887', '51', '29126.400'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '6', 'jy05742246', '51', '136377.600'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '7', 'andy999lhl', '51', '46080.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '8', 'haoli17', '51', '87600.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '9', 'iphone19930820', '51', '24811.200'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_COUNT', '10', 'whttlj888', '34', '26064.000');

# summary_top_winner
INSERT IGNORE into summary_top_winner (`period`, `timezone`, `currency`, `start_date`, `rank`, `player_username`, `player_win`)
values
-- ('LAST_7_DAYS', 'UTC', 'CNY', '2018-11-11', '1', 'test002', '333.333'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '1', 'hechuan888', '560138.075'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '2', 'qyclock108', '393951.500'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '3', 'pokerliu', '232782.750'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '4', 'jdlsn1987', '184691.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '5', 'txwwin000000', '182391.750'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '6', 'sololip', '149500.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '7', 'trytry88', '135470.050'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '8', 'w666888', '112500.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '9', 'roderick123', '109850.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '10', 'wei5168', '102500.000'),

('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '1', 'hechuan888', '3864952.718'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '2', 'qyclock108', '2718265.350'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '3', 'pokerliu', '1606200.975'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '4', 'jdlsn1987', '1274367.900'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '5', 'txwwin000000', '1258503.075'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '6', 'sololip', '1031550.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '7', 'trytry88', '934743.345'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '8', 'w666888', '776250.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '9', 'roderick123', '757965.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), '10', 'wei5168', '707250.000'),

('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '1', 'hechuan888', '10362554.388'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '2', 'qyclock108', '7288102.750'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '3', 'pokerliu', '4306480.875'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '4', 'jdlsn1987', '3416783.500'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '5', 'txwwin000000', '3374247.375'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '6', 'sololip', '2765750.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '7', 'trytry88', '2506195.925'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '8', 'w666888', '2081250.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '9', 'roderick123', '2032225.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '10', 'wei5168', '1896250.000');

# summary_top_popular_game
INSERT IGNORE into summary_top_popular_game (`period`, `timezone`, `currency`, `start_date`, `category`, `rank`, `game_code`, `player_count`, `bet_amount`, `player_win`, `player_loss`)
values
-- ('LAST_7_DAYS', 'UTC', 'CNY', '2018-11-11', 'BY_PLAYER', '1', 'FD1', '3', '333.333', '333.333', '333.333'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '1', 'Baccarat', '211', '14094312.700', '7198123.100', '7060353.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '2', 'Soccer', '114', '897871.000', '371922.290', '477372.500'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '3', 'Soccer', '108', '467454.920', '235231.400', '190132.680'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '4', 'Football', '78', '1257243.000', '531467.450', '564169.230'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '5', 'Baccarat', '45', '456517.000', '232001.100', '233250.400'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '6', 'Basketball', '36', '625173.000', '363357.020', '247314.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '7', 'XYPK10', '36', '365061.700', '248853.010', '264831.820'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '8', 'Basketball', '35', '103508.320', '39734.190', '60026.220'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '9', 'Multiple platform', '30', '100433.250', '48138.250', '52435.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, 'BY_PLAYER', '10', 'CQSSC', '24', '105586.360', '69875.190', '56695.460'),

('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '1', 'Baccarat', '688', '113430851.150', '56821367.850', '58277474.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '2', 'Multiple platform', '269', '3023306.100', '1487047.100', '1557617.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '3', 'Soccer', '224', '4236694.000', '1887457.620', '2205843.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '4', 'Soccer', '208', '1622941.630', '693240.100', '785190.340'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '5', 'Baccarat', '164', '10472965.000', '4986791.030', '5380023.400'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '6', 'Football', '121', '2333050.000', '995779.730', '1056261.700'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '7', 'HK6', '106', '974312.000', '654650.620', '670937.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '8', 'Basketball', '103', '3835033.000', '1587604.360', '2128208.000'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '9', 'XYPK10', '103', '4378039.430', '3073709.310', '3018326.040'),
('LAST_7_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY), 'BY_PLAYER', '10', 'Basketball', '98', '703666.500', '299692.240', '366635.250'),

('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '1', 'Baccarat', '1800', '469831520.950', '237073272.950', '246247049.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '2', 'Multiple platform', '721', '12833617.250', '6480581.250', '6405100.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '3', 'Soccer', '453', '14369449.000', '6377174.240', '7174675.810'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '4', 'Soccer', '440', '4993679.850', '1981367.500', '2374559.550'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '5', 'Baccarat', '435', '47701294.000', '23236819.680', '24064924.420'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '6', 'XYPK10', '295', '27244068.090', '18600605.010', '18325091.110'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '7', 'Basketball', '274', '10143687.000', '4766468.570', '4985105.400'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '8', 'XYFT', '254', '4635452.860', '3467362.440', '3493010.090'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '9', 'Basketball', '236', '3586509.000', '1587531.750', '1794623.030'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), 'BY_PLAYER', '10', 'Football', '203', '5204808.000', '2540795.930', '2204991.640');

# summary_game_provider_info
INSERT IGNORE into summary_game_provider_info (`period`, `timezone`, `currency`, `start_date`, `rank`, `game_api_id`, `game_type_id`, `bet`, `win`, `loss`)
values
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '1', '5', '2', '15050965.900', '7539610.000', '7724664.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '2', '14', '6', '1649398.000', '828614.780', '768806.170'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '3', '8', '2', '1578763.220', '685224.410', '736948.020'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '4', '13', '6', '1263802.000', '535170.810', '566794.230'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '5', '4', '2', '861791.570', '294687.190', '569304.380'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '6', '4', '4', '790532.290', '497190.000', '490625.590'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '7', '5', '6', '576677.240', '276886.670', '252361.900'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '8', '6', '2', '486343.000', '240120.650', '242392.400'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '9', '1', '2', '447304.700', '251888.700', '216961.000'),
('TODAY', 'UTC', 'CNY', CURRENT_DATE, '10', '14', '2', '337710.840', '182336.540', '159154.000'),

('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '1', '5', '2', '512669745.900', '259290761.000', '269871623.000'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '2', '1', '4', '51891584.050', '33857633.610', '32912891.210'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '3', '6', '2', '49569599.000', '23898059.020', '24757501.920'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '4', '8', '2', '37036592.190', '17430786.790', '18202044.230'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '5', '4', '2', '34516654.050', '16499096.930', '18049697.120'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '6', '1', '6', '25873631.000', '11809690.290', '12855476.940'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '7', '14', '2', '12481357.000', '5617859.900', '5969999.220'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '8', '4', '4', '8943488.840', '6417297.960', '6711103.460'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '9', '6', '6', '8886007.670', '3656129.400', '4250372.160'),
('THIS_MONTH', 'UTC', 'CNY', DATE_FORMAT(NOW(),'%Y-%m-01'), '10', '5', '5', '6220811.600', '505289.460', '844793.600'),

('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '1', '5', '2', '512669745.900', '259290761.000', '269871623.000'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '2', '1', '4', '51891584.050', '33857633.610', '32912891.210'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '3', '6', '2', '49569599.000', '23898059.020', '24757501.920'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '4', '8', '2', '37036592.190', '17430786.790', '18202044.230'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '5', '1', '2', '34516654.050', '16499096.930', '18049697.120'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '6', '2', '6', '25873631.000', '11809690.290', '12855476.940'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '7', '14', '2', '12481357.000', '5617859.900', '5969999.220'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '8', '4', '4', '8943488.840', '6417297.960', '6711103.460'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '9', '6', '6', '8886007.670', '3656129.400', '4250372.160'),
('LAST_30_DAYS', 'UTC', 'CNY', DATE_SUB(CURRENT_DATE, interval 31 DAY), '10', '5', '5', '6220811.600', '505289.460', '844793.600');

update  credit_point set `balance` = `balance` + 6666.66;

INSERT IGNORE INTO `withdraw_condition` (`player_id`, `source_type`, `source_id`, `bet_required`, `bet_amount`, `locked_on_amount`, `begin_at`, `status`)
VALUES
('2', '1', '1', '166.000', '0.000', '166.000', ADDTIME(now(), '-2 00:01:00'), 0),
('2', '1', '1', '288.000', '0.000', '288.000', ADDTIME(now(), '-2 00:01:00'), 0);

# Site message
INSERT IGNORE INTO `player_message` (`id`, `thread_id`, `system`, `player_id`, `operator_id`, `subject`, `content`, `sender`, `deleted`)
VALUES
(1, null, 1, 2, 2, 'System Message 1', 'This is a system message sent to player test002.', 0, 0),
(2, 2, 0, 2, null, 'Player Message', 'This is a player message sent by player test002 to operator admin. This is also the start of a thread.', 1, 0),
(3, 3, 0, 3, 1, 'Player Message 2', 'This is another player message sent by player test003 to operator admin.', 1, 0),
(4, null, 1, 2, 2, 'System Message 3', 'This is system message 3 sent to player test002.', 0, 0),
(5, 2, 0, 2, 2, 'Player Message', 'This is a player message replied from operator admin to player test002.', 0, 0),
(6, 2, 0, 2, 2, 'Player Message', '(Deleted) This is a player message replied from operator admin to player test002.', 0, 1);

# player_message template setting
INSERT IGNORE INTO `player_message_template_setting` (`template_type`, `subject`, `content`)
VALUES
('Deposit','Dear [PLAYER_NAME], Your Deposit request', 'Deposit template, deposit amount is [AMOUNT]'),
('Withdraw','Withdraw template', 'Withdraw template, withdraw amount is [AMOUNT]'),
('Promo','Promo template', 'Promo template');

# Player Announcement
INSERT IGNORE INTO `player_announcement` (`id`,`title`,`content`,`start_at`,`end_at`)
VALUES
(1, 'Player Announcement unit test', 'announcement content', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval 8 hour)),
(2, 'Player Announcement w/o dateRange constraints', 'announcement content', '1000-01:01 00:00:00', '9999-12-31 00:00:00'),
(3, 'Player Announcement unit test(avail for test002))', 'announcement content', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval 8 hour)),
(4, 'Player Announcement disabled', 'announcement content', DATE_ADD(now(), interval -8 hour), DATE_ADD(now(), interval 8 hour)),
(5, 'Player Announcemnt expired', 'announcement content', DATE_ADD(now(), interval -48 hour), DATE_ADD(now(), interval -40 hour));

INSERT IGNORE INTO `player_announcement_player_tag` (`announcement_id`,`player_tag_id`)
VALUES
('1', '2'),
('1', '3'),
('3', '8');

INSERT IGNORE INTO `deposit_request_process_history` (deposit_request_id, `from_status`, `to_status`, comment, updated_by, created_by) VALUES
(100001, 0, 0, 'Created deposit_request', null, null),
(100001, 0, 1, 'Confirmed by IT-desk', '2', '2'),
(100002, 0, 0, 'Created deposit_request', null, null),
(100003, 0, 0, 'Created deposit_request', null, null);

INSERT IGNORE INTO `withdraw_request_process_history` (withdraw_request_id, `from_status`, `to_status`, comment, updated_by, created_by) VALUES
(100001, 1, 1, 'Created withdraw_request', 1, 1),
(100001, 1, 1, 'Confirmed by IT-desk', 1, 1),
(100001, 1, 2, 'System review passed', 1, 1),
(100001, 2, 9, 'Ready to pay by using online bank', 1, 1),
(100001, 9, 10, 'Transferred amount by accountant', 1, 1),
(100002, 1, 1, 'Created withdraw_request', 1, 1),
(100003, 1, 1, 'Created withdraw_request', 1, 1),
(100003, 1, 2, 'System review passed', 1, 1);

INSERT IGNORE INTO `sms_api` (`code`, `name`, `status`, `meta`) VALUES
('MockSms', 'MockSms', 0, '{ \"url\": \"http://api.mocksms.com/sned-sms\"}'),
('Ihuyi', 'Ihuyi', 0, '{\"apiKey\":\"62d04297df75b277b40c7404247d35a5\", \"apiId\":\"C89408050\"}'),
('T1Notification', 'T1Notification', 1, '');

INSERT IGNORE INTO `sms_otp_history` (`sms_api_id`, `phone_number`, `player_id`, `otp_code`, `verified`, `remark`, `expiration_time`, `delivered`)
VALUES
(1, '16559181054', 1, '231242', 1, 'smsid:23123152345235', DATE_ADD(now(), interval 3 hour) , 0),
(2, '16559181234', 1, '123321', 0, '{\"soId\":\"2451\",\"notification\":{\"id\":2459,\"status\":30}}', DATE_SUB(now(), interval 3 hour) , 1);

INSERT IGNORE INTO `email_history` (`id`, `sender_address`, `recipient_address`, `subject`, `content`, `player_id`)
VALUES (1, 'tripleonetechsg@gmail.com', 'tripleonetechsg@gmail.com', 'unit test email subject', 'unit test email content', 2);

INSERT IGNORE INTO `player_activity_report` (`player_id`,`action`,`invoke`,`http_method`,`request_uuid`,`request_uri`,`request_body`,`query_string`,`return_code`,`response`,`device`,`ip`,`created_at`)
VALUES
(1,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,3,'114.32.45.146',DATE_SUB(now(), interval 6 DAY)),
(1,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,2,'114.32.45.146',DATE_SUB(now(), interval 3 DAY)),
(1,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,2,'114.32.45.146',now()),
(2,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,2,'114.32.45.146',DATE_SUB(now(), interval 5 MINUTE)),
(2,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,2,'114.32.45.146',DATE_SUB(now(), interval 4 MINUTE)),
(2,1,NULL,NULL,NULL,NULL,NULL,NULL,20000,NULL,2,'114.32.45.146',DATE_SUB(now(), interval 1 DAY)),
(2,NULL,'ApiPlayerController.editPlayerBankAccount','POST','DC1841D6-3D4B-43DE-BA18-66A66C61FA2C','/player/bank-accounts/2','{\"bankId\":0,\"accountNumber\":\"test\",\"accountHolderName\":\"test\",\"defaultAccount\":true,\"bankBranch\":\"test\"}',NULL,20000,'{\"code\":20000,\"data\":{\"success\":true,\"message\":null}}',2,'114.32.45.146',DATE_SUB(now(), interval 3 MINUTE)),
(2,NULL,'ApiPlayerController.setDefaultPlayerBankAccount','POST','2A3C18AD-BF98-4AAE-85F2-5FFBA7619A2F','/player/bank-accounts/2/set-default',NULL,NULL,20000,'{\"code\":20000,\"data\":{\"success\":true,\"message\":null}}',2,'114.32.45.146',DATE_SUB(now(), interval 2 MINUTE)),
(2,NULL,'ApiPlayerController.getPlayerBankAccounts','GET','4F365475-911C-4EB3-95C3-8D6F07AB0FA0','/player/bank-accounts',NULL,NULL,20000,'{\"code\":20000,\"data\":[{\"id\":2,\"bankId\":0,\"accountHolderName\":\"test\",\"accountNumber\":\"test\",\"bankBranch\":\"test\",\"defaultAccount\":true},{\"id\":3,\"bankId\":4,\"accountHolderName\":\"Chen XiuChuen\",\"accountNumber\":\"3333111122223333\",\"bankBranch\":\"建设银行北京分行\",\"defaultAccount\":false},{\"id\":4,\"bankId\":6,\"accountHolderName\":\"Chang Yi\",\"accountNumber\":\"4444222255556666\",\"bankBranch\":\"交通银行上海分行\",\"defaultAccount\":false}]}',2,'114.32.45.146',DATE_SUB(now(), interval 1 MINUTE)),
(2,NULL,'ApiPlayerController.getVerificationQuestion','GET','5ED589E4-22C4-4BF9-A2F7-64FA2C12F70C','/player/verification-question',NULL,'currency=CNY&username=test002',20000,'{\"code\":20000,\"data\":{\"id\":1,\"questionId\":1,\"locale\":\"en-US\",\"question\":\"What is your favorite food?\"}}',2,'114.32.45.146', now());

INSERT IGNORE INTO `operator_activity_report` (`operator_id`,`username`,`invoke`,`request_uuid`,`http_method`,`request_uri`,`request_body`,`query_string`,`return_code`,`response`,`exception`,`device`,`ip`,`created_at`)
VALUES
(1, 'superadmin', 'testClass.testMethod', UUID(), 'GET', '/test-method', NULL, NULL, 20000, NULL, NULL, 2, '114.32.45.146', now()),
(2,'admin','OperatorController.detail','1251CB6E-5BA0-480C-A7BD-1016A4E4C1EA','GET','/operator',NULL,NULL,20000,'{\"code\":20000,\"data\":{\"id\":2,\"username\":\"admin\",\"status\":1,\"role\":{\"id\":2,\"name\":\"OPERATOR\",\"permission\":\"[\\\"13\\\", \\\"0\\\", \\\"13101011122212\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\", \\\"0\\\"]\",\"createdAt\":null,\"updatedAt\":null},\"createdAt\":\"2020-09-01T12:28:13.902Z\"}}',NULL,2,'114.32.45.146', DATE_SUB(now(), interval 5 MINUTE)),
(2,'admin','OperatorRoleController.update','7A0F6C18-4893-4DAF-8443-D26F8ED6F04F','POST','/operator-roles/1','{\"name\":\"test\",\"permission\":\"test\"}',NULL,NULL,NULL,'org.springframework.dao.DataIntegrityViolationException: \r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Invalid JSON text: \"Invalid value.\" at position 1 in value for column \'operator_role.permission\'.\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: UPDATE operator_role   SET `name` = ?, `permission` = ?   WHERE `id` = ?\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Invalid JSON text: \"Invalid value.\" at position 1 in value for column \'operator_role.permission\'.\n; Data truncation: Invalid JSON text: \"Invalid value.\" at position 1 in value for column \'operator_role.permission\'.; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Invalid JSON text: \"Invalid value.\" at position 1 in value for column \'operator_role.permission\'.\r\n	at org.springframework.jdbc.support.SQLStateSQLExceptionTranslator.doTranslate(SQLStateSQLExceptionTranslator.java:104)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:72)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.$Proxy110.update(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.update(SqlSessionTemplate.java:294)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:63)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.$Proxy215.update(Unknown Source)\r\n	at com.tripleonetech.sbe.web.opbackend.OperatorRoleController.update(OperatorRoleController.java:47)\r\n	at com.tripleonetech.sbe.web.opbackend.OperatorRoleController$$FastClassBySpringCGLIB$$885d42c0.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.adapter.AfterReturningAdviceInterceptor.invoke(AfterReturningAdviceInterceptor.java:55)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.aspectj.AspectJAfterThrowingAdvice.invoke(AspectJAfterThrowingAdvice.java:62)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:93)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:688)\r\n	at com.tripleonetech.sbe.web.opbackend.OperatorRoleController$$EnhancerBySpringCGLIB$$7b8d12b5.update(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:189)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:800)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1038)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:942)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1005)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:908)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:660)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:882)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:741)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.tripleonetech.sbe.config.Slf4jMDCFilterConfiguration$Slf4jMDCFilter.doFilterInternal(Slf4jMDCFilterConfiguration.java:65)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:320)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.invoke(FilterSecurityInterceptor.java:127)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.doFilter(FilterSecurityInterceptor.java:91)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.access.ExceptionTranslationFilter.doFilter(ExceptionTranslationFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.session.SessionManagementFilter.doFilter(SessionManagementFilter.java:137)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.AnonymousAuthenticationFilter.doFilter(AnonymousAuthenticationFilter.java:111)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter.doFilter(SecurityContextHolderAwareRequestFilter.java:170)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.savedrequest.RequestCacheAwareFilter.doFilter(RequestCacheAwareFilter.java:63)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter.doFilter(OAuth2AuthenticationProcessingFilter.java:176)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.logout.LogoutFilter.doFilter(LogoutFilter.java:116)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.header.HeaderWriterFilter.doFilterInternal(HeaderWriterFilter.java:74)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.SecurityContextPersistenceFilter.doFilter(SecurityContextPersistenceFilter.java:105)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter.doFilterInternal(WebAsyncManagerIntegrationFilter.java:56)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:215)\r\n	at org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:178)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:357)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:270)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:92)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.tripleonetech.sbe.config.SimpleCORSFilter.doFilter(SimpleCORSFilter.java:47)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:199)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:490)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:139)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:408)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:834)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1417)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\r\n	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:748)\r\nCaused by: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Invalid JSON text: \"Invalid value.\" at position 1 in value for column \'operator_role.permission\'.\r\n	at com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping.translateException(SQLExceptionsMapping.java:104)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.executeInternal(ClientPreparedStatement.java:974)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.execute(ClientPreparedStatement.java:391)\r\n	at com.zaxxer.hikari.pool.ProxyPreparedStatement.execute(ProxyPreparedStatement.java:44)\r\n	at com.zaxxer.hikari.pool.HikariProxyPreparedStatement.execute(HikariProxyPreparedStatement.java)\r\n	at sun.reflect.GeneratedMethodAccessor206.invoke(Unknown Source)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.apache.ibatis.logging.jdbc.PreparedStatementLogger.invoke(PreparedStatementLogger.java:59)\r\n	at com.sun.proxy.$Proxy290.execute(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.update(PreparedStatementHandler.java:46)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.update(RoutingStatementHandler.java:74)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:50)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at org.apache.ibatis.executor.CachingExecutor.update(CachingExecutor.java:76)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.$Proxy289.update(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.$Proxy289.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:198)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 111 more\r\n',2,'114.32.45.146', DATE_SUB(now(), interval 3 MINUTE)),
(2,'admin','OperatorRoleController.delete','6AEBED6E-6E0E-4817-BFDF-FBF502D17CBF','POST','/operator-roles/1/delete',NULL,NULL,20000,'{\"code\":20000,\"data\":{\"success\":true,\"message\":null}}',NULL,2,'114.32.45.146', DATE_SUB(now(), interval 2 MINUTE)),
(2,'admin','PromoCashbackController.getUsableGameTypes','C19C1322-067E-4D85-AAA1-75AB9E081706','GET','/promotions/cashback/group/1/usable-game-types',NULL,'currency=CNY',20000,'{\"code\":20000,\"data\":[{\"gameApiId\":1,\"gameTypeId\":1,\"name\":\"MockGame CNY - Slot Game\",\"id\":\"1-1\"},{\"gameApiId\":4,\"gameTypeId\":1,\"name\":\"PT CNY - Slot Game\",\"id\":\"4-1\"},{\"gameApiId\":6,\"gameTypeId\":1,\"name\":\"T1GBBIN CNY - Slot Game\",\"id\":\"6-1\"},{\"gameApiId\":15,\"gameTypeId\":1,\"name\":\"T1GCQ9 CNY - Slot Game\",\"id\":\"15-1\"},{\"gameApiId\":13,\"gameTypeId\":1,\"name\":\"T1GISB CNY - Slot Game\",\"id\":\"13-1\"},{\"gameApiId\":10,\"gameTypeId\":1,\"name\":\"T1GMG CNY - Slot Game\",\"id\":\"10-1\"},{\"gameApiId\":7,\"gameTypeId\":1,\"name\":\"T1GPRAGMATICPLAY CNY - Slot Game\",\"id\":\"7-1\"},{\"gameApiId\":1,\"gameTypeId\":2,\"name\":\"MockGame CNY - Live Casino\",\"id\":\"1-2\"},{\"gameApiId\":4,\"gameTypeId\":2,\"name\":\"PT CNY - Live Casino\",\"id\":\"4-2\"},{\"gameApiId\":11,\"gameTypeId\":2,\"name\":\"T1GAB CNY - Live Casino\",\"id\":\"11-2\"},{\"gameApiId\":6,\"gameTypeId\":2,\"name\":\"T1GBBIN CNY - Live Casino\",\"id\":\"6-2\"},{\"gameApiId\":12,\"gameTypeId\":2,\"name\":\"T1GEBET CNY - Live Casino\",\"id\":\"12-2\"},{\"gameApiId\":10,\"gameTypeId\":2,\"name\":\"T1GMG CNY - Live Casino\",\"id\":\"10-2\"},{\"gameApiId\":15,\"gameTypeId\":3,\"name\":\"T1GCQ9 CNY - Casino\",\"id\":\"15-3\"},{\"gameApiId\":13,\"gameTypeId\":3,\"name\":\"T1GISB CNY - Casino\",\"id\":\"13-3\"},{\"gameApiId\":8,\"gameTypeId\":3,\"name\":\"T1GKYCARD CNY - Casino\",\"id\":\"8-3\"},{\"gameApiId\":10,\"gameTypeId\":3,\"name\":\"T1GMG CNY - Casino\",\"id\":\"10-3\"},{\"gameApiId\":7,\"gameTypeId\":3,\"name\":\"T1GPRAGMATICPLAY CNY - Casino\",\"id\":\"7-3\"},{\"gameApiId\":1,\"gameTypeId\":4,\"name\":\"MockGame CNY - Lottery\",\"id\":\"1-4\"},{\"gameApiId\":6,\"gameTypeId\":4,\"name\":\"T1GBBIN CNY - Lottery\",\"id\":\"6-4\"},{\"gameApiId\":17,\"gameTypeId\":4,\"name\":\"T1GLOTTERY CNY - Lottery\",\"id\":\"17-4\"},{\"gameApiId\":18,\"gameTypeId\":4,\"name\":\"T1LOTTERY CNY - Lottery\",\"id\":\"18-4\"},{\"gameApiId\":5,\"gameTypeId\":5,\"name\":\"T1GAGIN CNY - Fishing\",\"id\":\"5-5\"},{\"gameApiId\":6,\"gameTypeId\":5,\"name\":\"T1GBBIN CNY - Fishing\",\"id\":\"6-5\"},{\"gameApiId\":5,\"gameTypeId\":6,\"name\":\"T1GAGIN CNY - Sports\",\"id\":\"5-6\"},{\"gameApiId\":6,\"gameTypeId\":6,\"name\":\"T1GBBIN CNY - Sports\",\"id\":\"6-6\"},{\"gameApiId\":10,\"gameTypeId\":6,\"name\":\"T1GMG CNY - Sports\",\"id\":\"10-6\"},{\"gameApiId\":14,\"gameTypeId\":6,\"name\":\"T1GONEWORKS CNY - Sports\",\"id\":\"14-6\"}]}',NULL,2,'114.32.45.146', now());

INSERT IGNORE INTO `email_verification_history` (`id`, `email`, `otp_code`, `player_id`, `verified`, `expiration_time`)
VALUES
(1, 'tripleonetechsg@gmail.com', '231243', 2, 0, DATE_ADD(now(), interval 24 hour)),
(2, 'test@tripleonetech.net', '123321', 1, 0, DATE_SUB(now(), interval 24 hour));

INSERT IGNORE INTO `automation_job` (`id`, `name`, `filters`, `actions`, `start_at`, `max_run`, `run_frequency`, `created_at`, `updated_at`)
VALUES
(1, 'AutoJob1', '[[{"id": 1, "value": [8], "operator": "any"}], [{"id": 3, "value": ["CNY", 5000], "operator": "<"}]]', '[{"id": 1, "value": [6]}]', DATE_SUB(now(), interval 2 DAY), 0, 2,  DATE_SUB(now(), interval 2 DAY),  DATE_SUB(now(), interval 2 DAY)),
(2, 'AutoJob2', '[[{"id": 1, "value": [6, 8], "operator": "any"}], [{"id": 3, "value": ["CNY", 5000], "operator": "<"}]]', '[{"id": 1, "value": [7]}]', DATE_SUB(now(), interval 3 DAY), 10, 3, DATE_SUB(now(), interval 3 DAY), DATE_SUB(now(), interval 3 DAY)),
(3, 'AutoJob3', NULL, '[{\"id\": 1, \"value\": [6, 7]}]', DATE_SUB(now(), interval 3 DAY), 0, 4, DATE_SUB(now(), interval 3 DAY), DATE_SUB(now(), interval 3 DAY)),
(4, 'AutoJob4', '[[{"id": 1, "value": [6, 8], "operator": "any"}], [{"id": 3, "value": ["CNY", 5000], "operator": "<"}]]', '[{"id": 1, "value": [7]}]', DATE_SUB(now(), interval 3 DAY), 10, 0, DATE_SUB(now(), interval 3 DAY), DATE_SUB(now(), interval 3 DAY));

INSERT IGNORE INTO `automation_record` (`id`, `job_id`, `job_name`, `run_count`, `status`, `affected_player_ids`, `affected_player_count`, `automated`, `created_at`, `completed_at`, `updated_at`)
VALUES
(1, 1, 'AutoJob1', 1, 10, '1,2,3,4,5,6,7', 7, 1, DATE_SUB(now(), interval 3 DAY), DATE_ADD(DATE_SUB(now(), interval 3 DAY), INTERVAL 1 MINUTE), DATE_ADD(DATE_SUB(now(), interval 3 DAY), INTERVAL 1 MINUTE)),
(2, 1, 'AutoJob1', 2, 10, '1,2', 2, 1, DATE_SUB(now(), interval 2 DAY), DATE_ADD(DATE_SUB(now(), interval 2 DAY), INTERVAL 1 MINUTE), DATE_ADD(DATE_SUB(now(), interval 2 DAY), INTERVAL 1 MINUTE)),
(3, 1, 'AutoJob1', 3, 21, '1,2', 2, 1, DATE_SUB(now(), interval 1 DAY), NULL, DATE_ADD(DATE_SUB(now(), interval 1 DAY), INTERVAL 1 MINUTE));

INSERT IGNORE into `command_history` (`content`, `method_name`, `command_type`) VALUES
('[{"id":7,"playerId":2,"gameApiId":1,"gameApiCode":"MockGame","gameCode":"DUMMY","playerUsername":"test002","bet":50.000,"win":100.000,"loss":200.000,"payout":0.000,"betTimeHour":"2018-09-20T15:00:00","dirty":true,"createdAt":"2018-09-20T17:36:57.97","updatedAt":"2020-06-01T07:13:18","gameTypeId":1,"gameTypeName":null,"currency":"CNY"},{"id":8,"playerId":3,"gameApiId":1,"gameApiCode":"MockGame","gameCode":"DUMMY","playerUsername":"test003","bet":150.000,"win":100.000,"loss":200.000,"payout":0.000,"betTimeHour":"2018-09-20T18:00:00","dirty":true,"createdAt":"2018-09-20T17:36:57.971","updatedAt":"2020-06-01T07:13:18","gameTypeId":1,"gameTypeName":null,"currency":"CNY"}]', 'GAME_LOG_HOURLY_REPORT', 1),
('[{"id":1,"playerId":1,"gameApiId":1,"gameApiCode":"MockGame","gameCode":"FD1","playerUsername":"test001","bet":100.000,"win":100.000,"loss":200.000,"payout":0.000,"betTimeHour":"2018-09-20T14:00:00","dirty":true,"createdAt":"2018-09-20T17:27:52.963","updatedAt":"2020-06-01T07:13:18","gameTypeId":1,"gameTypeName":null,"currency":"CNY"},{"id":2,"playerId":1,"gameApiId":1,"gameApiCode":"MockGame","gameCode":"FD1","playerUsername":"test001","bet":100.000,"win":200.000,"loss":300.000,"payout":0.000,"betTimeHour":"2018-09-20T15:00:00","dirty":true,"createdAt":"2018-09-20T17:27:52.963","updatedAt":"2020-06-01T07:13:18","gameTypeId":1,"gameTypeName":null,"currency":"CNY"}]', 'GAME_LOG_HOURLY_REPORT', 1);

INSERT IGNORE INTO `content_store` (`key`, `type`, `content`) VALUES
('key1', 2, 'content1'),
('key2', 2, 'content2');

# player_favorite_game
INSERT IGNORE INTO `player_favorite_game` (`player_id`, `game_api_id`, `game_code`) VALUES
(2, 5, '13'),
(2, 5, '220'),
(2, 5, 'AV01'),
(2, 5, 'BJ'),
(2, 6, '3001'),
(2, 6, '3002'),
(2, 5, 'DT'),
(2, 5, 'DTA0'),
(2, 5, 'DTAB'),
(2, 5, 'DTAF'),
(2, 1, 'BB'),
(2, 1, 'CF'),
(2, 1, '3CK002'),
(2, 4, 'DUMMY'),
(2, 4, 'PTBB'),
(2, 4, 'PTFD'),
(2, 15, '1'),
(2, 15, '10'),
(2, 15, '100'),
(2, 15, '101'),
(2, 15, '102'),
(2, 15, '11'),
(2, 15, '111'),
(2, 15, '113'),
(2, 15, '12'),
(2, 15, '121'),
(3, 4, 'DUMMY'),
(3, 4, 'PTFD');

INSERT INTO resource_upload(id, `file_path`, `file_name`, `name`, `size`, `width`, `height`) VALUES
(1, "test1.png", "test1.png", "test1_for_test.png", 1000, 140, 100),
(2, "test2.png", "test2.png", "test2_for_test.png", 1000, 100, 100),
(3, "test3.png", "test3.png", "test3_for_test.png", 1000, 130, 100),
(4, "te4.png", "te4.png", "te4_for_test.png", 1000, 100, 100);

# for a whole new player start
INSERT INTO player_credential(username, password_encrypt, referral_code, referral_player_credential_id, email, email_verified, register_device, register_ip)
VALUES('testfornewplayer', 'af1172e5ca25075d547ecae3bd7fede27e59080aec78d002692decd2dfed2a92', 'bct00oc', null, 'testemail@tripleonetech.com', false, 2, '127.0.0.1');

INSERT INTO player ( id, username, `status`, currency ) values
(101, 'testfornewplayer', 1, 'CNY'),
(102, 'testfornewplayer', 1, 'THB'),
(103, 'testfornewplayer', 1, 'USD');

insert into player_profile ( player_id, phone_verified )
select id, false from player where username='testfornewplayer';

insert into wallet (player_id, game_api_id, balance)
select id, null, 0 from player where username='testfornewplayer';

insert into wallet (player_id, game_api_id, balance)
select player.id, game_api.id, 0 from player inner join game_api where player.currency=game_api.currency and username='testfornewplayer';

INSERT IGNORE INTO players_player_tag(player_id, player_tag_id)
select id, 5 from player where username='testfornewplayer';

INSERT INTO player_activity_report(player_id, action, invoke, request_uuid, return_code, `request_body`, `response`, `exception`, device, ip) VALUES
(null, null, 'ApiPlayerRegisterController.register', 'C1B1F88C-330D-4E1C-A2DD-06A354E74200', 20000, '[{"username":"testfornewplayer","currency":"CNY","referralCode":null,"email":"testemail@tripleonetech.com","password":"123456"}]',
'{"code":20000,"data":{"success":true,"message":null}}', null, 2, '127.0.0.1');
# for a whole new player end

INSERT IGNORE INTO `domain_config` (`id`, `name`, `site`, `enabled`)
VALUES(1, 'admin.demo.t1t.com', 0, 1),
      (2, 'www.demo.t1t.com', 1, 1);

INSERT IGNORE INTO `site_property` (`key`, `value`) VALUES
('LIVE_CHAT_URL', 'http://tripleonetech.net'),
('LIVE_CHAT_CODE', 'test-code'),
('TITLE', 'demo site'),
('LOGO', '/image/logo.png'),
('SLOGAN', 'demo slogan');

# Prepare accessible country / ip firewall setting. HK for company proxy, AU for BrowserStack
UPDATE ip_rule_country SET enabled = 1 WHERE country_code IN ('TW', 'NZ', 'SG', 'CN', 'HK', 'AU');

INSERT IGNORE INTO `ip_rule` (`ip`, `site`, `remark`)
VALUES('0.0.0.0/0', 0, 'UNIT_TEST_BACK_OFFICE_WHITELIST'),
      ('10.1.0.1/32', 0, 'UNIT_TEST_BACK_OFFICE_WHITELIST'),
      ('0:0:0:0:0:0:0:1', 0, 'UNIT_TEST_BACK_OFFICE_WHITELIST'),
      ('192.168.100.100/32', 1, 'UNIT_TEST_FRONT_OFFICE_BLACKLIST'),
      ('0:0:0:0:0:0:0:1', 0, 'BACK_OFFICE_WHITELIST'),
      ('127.127.127.1/32', 1, 'UNIT_TEST_FRONT_OFFICE_BLACKLIST');

INSERT IGNORE INTO ext_api_activity_report (player_id,ext_api_type,invoke,params,return_code,api_result) VALUES
(2,1,'GameApiT1GPT.launchGame','[{"id":2,"username":"test002","passwordEncrypt":"2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e","withdrawPasswordHash":"$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK","globalStatus":"ACTIVE","passwordPending":false,"referralCode":null,"referralPlayerCredentialId":null,"email":"tripleonetechsg@gmail.com","emailVerified":false,"registerDevice":"COMPUTER_HTML","registerIp":"114.32.45.146","createdAt":"2020-08-09T16:16:08Z","updatedAt":"2020-09-08T16:16:08Z","credentialId":2,"currency":"CNY","status":"ACTIVE","cashbackEnabled":true,"campaignEnabled":true,"withdrawEnabled":true,"lastLoginTime":"2020-09-08T16:41:51.212Z","lastLoginDevice":"UNKNOWN","lastLoginIp":"0:0:0:0:0:0:0:1","groupId":1,"tagIds":[6,8],"enabled":true,"lastLoginTimeLocal":"2020-09-08T16:41:51.212","createdAtLocal":"2020-08-09T16:16:08"},{"id":4788,"gameApiCode":"T1GPT","gameCode":"ash3brg","gameTypeId":3,"featured":false,"releasedDate":null,"web":true,"mobile":true,"enabled":true,"createdAt":"2020-08-03T17:11:03.514","updatedAt":"2020-08-03T17:11:03.514","gameNameI18n":{"id":4788,"gameNameId":4788,"locale":null,"name":"3 Card Brag","imgUrl":null,"createdAt":null,"updatedAt":null},"gameName":"3 Card Brag"},"pc","real","en-US"]',200,'{"responseEnum":"OK","params":{"response":{"code":0,"message":"success","detail":{"launcher":{"success":"true","url":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/1/ash3brg/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=4788&append_target_db=0"}},"apiResponse":"OK"}},"message":"OK","balance":null,"transactionId":null,"gameUrl":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/1/ash3brg/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=4788&append_target_db=0","success":true}')
,(2,1,'GameApiT1GPT.launchGame','[{"id":2,"username":"test002","passwordEncrypt":"2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e","withdrawPasswordHash":"$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK","globalStatus":"ACTIVE","passwordPending":false,"referralCode":null,"referralPlayerCredentialId":null,"email":"tripleonetechsg@gmail.com","emailVerified":false,"registerDevice":"COMPUTER_HTML","registerIp":"114.32.45.146","createdAt":"2020-08-09T16:16:08Z","updatedAt":"2020-09-08T16:41:51.232Z","credentialId":2,"currency":"CNY","status":"ACTIVE","cashbackEnabled":true,"campaignEnabled":true,"withdrawEnabled":true,"lastLoginTime":"2020-09-08T16:47:10.678Z","lastLoginDevice":"UNKNOWN","lastLoginIp":"0:0:0:0:0:0:0:1","groupId":1,"tagIds":[6,8],"enabled":true,"lastLoginTimeLocal":"2020-09-08T16:47:10.678","createdAtLocal":"2020-08-09T16:16:08"},{"id":4788,"gameApiCode":"T1GPT","gameCode":"ash3brg","gameTypeId":3,"featured":false,"releasedDate":null,"web":true,"mobile":true,"enabled":true,"createdAt":"2020-08-03T17:11:03.514","updatedAt":"2020-08-03T17:11:03.514","gameNameI18n":{"id":4788,"gameNameId":4788,"locale":null,"name":"3 Card Brag","imgUrl":null,"createdAt":null,"updatedAt":null},"gameName":"3 Card Brag"},"pc","real","en-US"]',200,'{"responseEnum":"OK","params":{"response":{"code":0,"message":"success","detail":{"launcher":{"success":"true","url":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/1/ash3brg/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=4788&append_target_db=0"}},"apiResponse":"OK"}},"message":"OK","balance":null,"transactionId":null,"gameUrl":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/1/ash3brg/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=4788&append_target_db=0","success":true}')
,(2,1,'GameApiT1GMGPLUS.launchGame','[{"id":2,"username":"test002","passwordEncrypt":"2fa5893e9542d9a7f120597ee0200c2ba21bfb76175024435d4c6ec44978da9e","withdrawPasswordHash":"$2a$10$lNFLA1TYVVpmDF1M6OTonuX/pUBoWM50lOOqj2MfpS6OVG.BQu1vK","globalStatus":"ACTIVE","passwordPending":false,"referralCode":null,"referralPlayerCredentialId":null,"email":"tripleonetechsg@gmail.com","emailVerified":false,"registerDevice":"COMPUTER_HTML","registerIp":"114.32.45.146","createdAt":"2020-08-09T16:16:08Z","updatedAt":"2020-09-08T16:41:51.232Z","credentialId":2,"currency":"CNY","status":"ACTIVE","cashbackEnabled":true,"campaignEnabled":true,"withdrawEnabled":true,"lastLoginTime":"2020-09-08T16:47:10.678Z","lastLoginDevice":"UNKNOWN","lastLoginIp":"0:0:0:0:0:0:0:1","groupId":1,"tagIds":[6,8],"enabled":true,"lastLoginTimeLocal":"2020-09-08T16:47:10.678","createdAtLocal":"2020-08-09T16:16:08"},{"id":29076,"gameApiCode":"T1GMGPLUS","gameCode":"SFB_VolcanoJourney","gameTypeId":1,"featured":false,"releasedDate":null,"web":true,"mobile":true,"enabled":true,"createdAt":"2020-08-03T17:11:03.514","updatedAt":"2020-08-03T17:11:03.514","gameNameI18n":{"id":29075,"gameNameId":29076,"locale":null,"name":"Volcano Journey","imgUrl":null,"createdAt":null,"updatedAt":null},"gameName":"Volcano Journey"},"pc","real","en-US"]',200,'{"responseEnum":"OK","params":{"response":{"code":0,"message":"success","detail":{"launcher":{"success":"true","url":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/5213/SFB_VolcanoJourney/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=29076&append_target_db=0"}},"apiResponse":"OK"}},"message":"OK","balance":null,"transactionId":null,"gameUrl":"http://player.gamegateway.t1t.games/player_center/launch_game_with_token/8f971eeeea411df806a849a810741a80/5213/SFB_VolcanoJourney/en-US/real/pc/_null/testmerchant/_null?t1_merchant_code=testmerchant&game_id=29076&append_target_db=0","success":true}');

update game_name_i18n set img_url = 'http://FanTan/img/url'
where id = 12380;

INSERT INTO time_hour_list
WITH RECURSIVE timehour_ranges (`datetime`) AS (
    SELECT DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d 00:00:00')
    UNION ALL
    SELECT `datetime` + INTERVAL 1 HOUR FROM timehour_ranges
    WHERE `datetime` + INTERVAL 1 HOUR <= DATE_ADD(CURDATE(), INTERVAL 10 DAY ))
SELECT `datetime` AS `time_hour`
FROM timehour_ranges;

