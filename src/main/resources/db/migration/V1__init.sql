# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20)
# Database: og2-test
# Generation Time: 2018-05-16 06:54:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table game_api
# ------------------------------------------------------------

# General Rule of using Decimal:
# BigInt -> Decimal(22,3); Int -> Decimal(13,3), SmallInt -> Decimal(8,3), TinyInt -> Decimal(6,3)

CREATE TABLE `game_api` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` TINYINT NOT NULL Comment '0-Disabled, 1-Enabled, 10-Inactive, 20-Deleted',
  `code` varchar(50) NOT NULL DEFAULT '',
  `currency` CHAR(3) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `sync_enabled` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `next_sync_from` varchar(255) DEFAULT NULL,
  `gamelist_last_updated_datetime` datetime(3),
  `meta` text NOT NULL,
  `md5` varchar(32) NOT NULL COMMENT 'Used to determine whether the data has been modified',
  `remark` text,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table player
# ------------------------------------------------------------

CREATE TABLE `player_credential` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_encrypt` varchar(255) NOT NULL,
  `withdraw_password_hash` varchar(255) DEFAULT NULL,
  `global_status` SMALLINT UNSIGNED NOT NULL DEFAULT '1',
  `password_pending` SMALLINT UNSIGNED NOT NULL DEFAULT '0',
  `referral_code` VARCHAR(7) NOT NULL,
  `referral_player_credential_id` INT UNSIGNED DEFAULT NULL,
  `email` varchar(255),
  `email_verified` TINYINT NOT NULL DEFAULT 0,
  `register_device` SMALLINT,
  `register_ip` varchar(20),
  `verification_question_id` TINYINT,
  `verification_answer` varchar(255),
  `last_activity_time` datetime(3),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`),
  UNIQUE KEY `idx_referral_code` (`referral_code`),
  FOREIGN KEY (referral_player_credential_id) REFERENCES player_credential (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `currency` varchar(6) NOT NULL,
  `status` SMALLINT UNSIGNED NOT NULL DEFAULT '1',
  `cashback_enabled` BOOLEAN NOT NULL DEFAULT TRUE,
  `campaign_enabled` BOOLEAN NOT NULL DEFAULT TRUE,
  `withdraw_enabled` BOOLEAN NOT NULL DEFAULT TRUE,
  `promo_enabled` BOOLEAN NOT NULL DEFAULT TRUE,
  `risk` SMALLINT UNSIGNED NOT NULL DEFAULT '1',
  `last_login_time` datetime(3),
  `last_login_device` SMALLINT UNSIGNED,
  `last_login_ip` varchar(50),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  INDEX `idx_currency` (`currency`),
  UNIQUE KEY `idx_username_currency` (`username`, `currency`),
  CONSTRAINT fk_player_credential_username FOREIGN KEY (username) REFERENCES player_credential (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table player_profile
# ------------------------------------------------------------

CREATE TABLE `player_profile` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` int NOT NULL,
  `first_name` varchar(50),
  `last_name` varchar(50),
  `birthday` date,
  `language` varchar(50),
  `gender` char(1) comment 'M/F',
  `address` varchar(255),
  `city` varchar(50),
  `country_code` char(3) comment 'ISO 3166-1 alpha-3, 3 characters. e.g. CHN, SGP',
  `country_phone_code` varchar(6) comment 'Phone country code without + sign, e.g. 86, 65',
  `line` varchar(255),
  `skype` varchar(255),
  `qq` varchar(255),
  `wechat` varchar(255),
  `phone_number` varchar(20),
  `phone_verified` TINYINT NOT NULL DEFAULT 0,
  `contact_preferences` TINYINT UNSIGNED comment 'Preferred contact methods, can be multiple selections',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table player_verification_question_i18n
#------------------------------------------------------------
CREATE TABLE `player_verification_question_i18n` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` TINYINT UNSIGNED NOT NULL,
  `locale` VARCHAR(20)  NOT NULL,
  `question` VARCHAR(200) NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_player_verification_question_id_locale` (`question_id`, `locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table player_stats
# ------------------------------------------------------------

CREATE TABLE `player_stats` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` int UNSIGNED NOT NULL,
  `total_deposit_count` int NOT NULL DEFAULT 0,
  `total_deposit_amount` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `first_deposit_date` datetime(3),
  `last_deposit_date` datetime(3),
  `total_withdraw_count` int NOT NULL DEFAULT 0,
  `total_withdraw_amount` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `first_withdraw_date` datetime(3),
  `last_withdraw_date` datetime(3),
  `total_cashback_bonus` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `total_referral_bonus` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `total_campaign_bonus` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `dirty` TINYINT UNSIGNED NOT NULL DEFAULT 127 COMMENT 'A dirty record represents that it needs to be calculated in summary.',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY (`player_id`),
  CONSTRAINT fk_player_stats_player_id FOREIGN KEY (player_id) REFERENCES player (id),
  KEY `idx_dirty` (`dirty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table game_api_player_account
# ------------------------------------------------------------

CREATE TABLE `game_api_player_account` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_api_id` int NOT NULL,
  `player_id` int NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT 'The full username registered with API',
  `password_encrypt` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_game_api_player_account_api_player_id` (`game_api_id`,`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table wallet
# ------------------------------------------------------------

CREATE TABLE `wallet` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT UNSIGNED NOT NULL,
  `game_api_id` INT UNSIGNED COMMENT 'If this column has NULL value, this row represents the player''s main wallet balance',
  `balance` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `pending_wallet` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `clean_date` datetime(3) NOT NULL DEFAULT '1970-01-01 00:00:01',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  CONSTRAINT `fk_wallet_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
   UNIQUE INDEX(player_id, game_api_id),
   PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table wallet_transaction
# ------------------------------------------------------------

CREATE TABLE `wallet_transaction` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT UNSIGNED NOT NULL DEFAULT '6' COMMENT '0:deposit, 1:withdrawal, 2:transfer in, 3:transfer out, 4:bonus, 5:cashback, 6:manual adjustment, 7:freeze ,8:withdrawal approve, 9:withdrawal cancel',
  -- `order_number` INT DEFAULT NULL COMMENT 'Request order number (if there is any)',
  `player_id` INT UNSIGNED NOT NULL,
  `balance_before` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `balance_after` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `amount` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `game_api_id` INT UNSIGNED,
  `external_transaction_id` VARCHAR(150) DEFAULT NULL,
  `internal_transaction_id` VARCHAR(150) DEFAULT NULL,
  `promotion_type` TINYINT UNSIGNED DEFAULT NULL COMMENT '1-CASHBACK, 2-REFERRAL, 11-CAMPAIGN_DEPOSIT, 12-CAMPAIGN_TASK, 13-CAMPAIGN_RESCUE',
  `promo_rule_id` INT DEFAULT NULL,
  `note` TEXT DEFAULT NULL,
  `operator_id` INT UNSIGNED,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  CONSTRAINT `fk_wallet_transaction_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wallet_transaction_game_api_id`
    FOREIGN KEY (`game_api_id`)
    REFERENCES `game_api` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY(id),
  KEY `idx_amount` (`amount`),
  KEY `idx_balance_before` (`balance_before`),
  KEY `idx_balance_after` (`balance_after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table subwallet_history
# ------------------------------------------------------------

CREATE TABLE `subwallet_history` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT UNSIGNED NOT NULL DEFAULT '6' COMMENT '0:deposit, 1:withdrawal, 2:transfer in, 3:transfer out, 4:bonus, 5:cashback, 6:manual adjustment, 7:freeze ,8:withdrawal approve, 9:withdrawal cancel',
  `player_id` INT UNSIGNED NOT NULL,
  `game_api_id` INT UNSIGNED NOT NULL,
  `balance_before` DECIMAL(13,3) NOT NULL,
  `sync_success` TINYINT UNSIGNED NOT NULL,
  `balance_after` DECIMAL(13,3) DEFAULT NULL,
  `created_by` VARCHAR(64) DEFAULT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  CONSTRAINT `fk_subwallet_history_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_subwallet_game_api_id`
    FOREIGN KEY (`game_api_id`)
    REFERENCES `game_api` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
   PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table wallet_transfer_request
#------------------------------------------------------------
CREATE TABLE `wallet_transfer_request` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `player_id` INT UNSIGNED NOT NULL,
    `game_api_id` INT UNSIGNED NOT NULL,
    `amount` DECIMAL(13,3) NOT NULL,
    `status` TINYINT UNSIGNED NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
    PRIMARY KEY (id),
    CONSTRAINT `fk_wallet_transfer_request_player_id`
      FOREIGN KEY (player_id)
      REFERENCES player (id)
      ON DELETE RESTRICT
      ON UPDATE CASCADE,
    CONSTRAINT `fk_wallet_transfer_request_game_api_id`
      FOREIGN KEY (game_api_id)
      REFERENCES game_api (id)
      ON DELETE RESTRICT
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `operator` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` SMALLINT UNSIGNED NOT NULL DEFAULT '1',
  `role_id` INT UNSIGNED,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES operator_role (id) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `operator_role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `permission` json NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_role` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table bank
# ------------------------------------------------------------

CREATE TABLE `bank` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `code` varchar(50) NOT NULL DEFAULT '',
  `icon` varchar(100) DEFAULT NULL,
  `enabled` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


# create table payment_method
# ------------------------------------------------------------

CREATE TABLE `payment_method` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `type` TINYINT NOT NULL Comment '1-BANK_TRANSFER, 2-QUICK_PAY, 3-WEXIN, 4-ALIPAY, 0-OTHERS',
  `payment_api_id` INT UNSIGNED,
  `currency` varchar(6) NOT NULL,
  `daily_max_deposit` decimal(22,3) COMMENT 'null means no limit',
  `min_deposit_per_trans` DECIMAL(8,3) COMMENT 'null or 0 means no limit',
  `max_deposit_per_trans` decimal(22,3) COMMENT 'null means no limit',
  `charge_ratio` decimal(9,6) NOT NULL DEFAULT 0 COMMENT 'charge ratio per deposit transaction. formula: deposit amount * deposit_charge_ratio(%) + deposit_fixed_charge = payment_charge',
  `fixed_charge` decimal(13,3) NOT NULL DEFAULT 0 COMMENT 'fixed charge per deposit transaction',
  `player_bear_payment_charge` BOOLEAN NOT NULL COMMENT 'True: Player bears charge',
  `player_tag_operator` VARCHAR(3) NOT NULL DEFAULT 'any' COMMENT 'This payment method should be applicable to \'any\' or \'all\' player tags. Allowed values: any|all',
  `priority` INT NOT NULL DEFAULT 0 COMMENT 'Higher number gives a higher priority. For the same payment type and payment api id, only the available payment method with highest priority should be available',
  `note` varchar(65535),
  `enabled` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `deleted` BOOLEAN NOT NULL DEFAULT '0',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `payment_method_player_tag` (
    `payment_method_id` INT UNSIGNED NOT NULL,
    `player_tag_id` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`payment_method_id`,`player_tag_id`),
    KEY `FK_payment_method_player_tag_2` (`player_tag_id`),
    CONSTRAINT `FK_payment_method_player_tag_1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_payment_method_player_tag_2` FOREIGN KEY (`player_tag_id`) REFERENCES `player_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `payment_method_player` (
    `payment_method_id` INT UNSIGNED NOT NULL,
    `player_id` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`payment_method_id`,`player_id`),
    KEY `FK_payment_method_player_2` (`player_id`),
    CONSTRAINT `FK_payment_method_player_1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_payment_method_player_2` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- -----------------------------------------------------
-- Table `player_tag`
-- -----------------------------------------------------
CREATE TABLE `player_tag` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_group` SMALLINT UNSIGNED NOT NULL DEFAULT '1',
  `default_tag` BOOLEAN NOT NULL DEFAULT FALSE,
  `name` VARCHAR(100) NOT NULL DEFAULT '',
  `automation_job_id` INT UNSIGNED,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_player_tag_name` (`name`)
)ENGINE=InnoDB DEFAULT CHARSET = utf8mb4;

#Dump of table players_player_tag
#------------------------------------------------------------
CREATE TABLE `players_player_tag` (
  `player_id` INT UNSIGNED NOT NULL,
  `player_tag_id` INT UNSIGNED NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`player_id`,`player_tag_id`),
  CONSTRAINT fk_players_player_tag_player_id FOREIGN KEY (player_id) REFERENCES player (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_players_player_tag_tag_id FOREIGN KEY (player_tag_id) REFERENCES player_tag (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table payment_api
# ------------------------------------------------------------

CREATE TABLE `payment_api` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` TINYINT NOT NULL DEFAULT 0 Comment '0-Disabled, 1-Enabled, 10-Inactive, 20-Deleted',
  `code` varchar(50) NOT NULL DEFAULT '',
  `currency` CHAR(3) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `meta` text NOT NULL,
  `daily_max_deposit` decimal(22,3) COMMENT 'null or 0 means no limit',
  `min_deposit_per_trans` DECIMAL(8,3) COMMENT 'null or 0 means no limit',
  `max_deposit_per_trans` decimal(22,3) COMMENT 'null or 0 means no limit',
  `charge_ratio` decimal(9,6) NOT NULL DEFAULT 0 COMMENT 'charge ratio per deposit transaction. formula: deposit amount * deposit_charge_ratio(%) + deposit_fixed_charge = payment_charge',
  `fixed_charge` decimal(13,3) NOT NULL DEFAULT 0 COMMENT 'fixed charge per deposit transaction',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table withdrawal_Api
# ------------------------------------------------------------

CREATE TABLE `withdraw_api` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` TINYINT NOT NULL DEFAULT 0 Comment '0-Disabled, 1-Enabled, 10-Inactive, 20-Deleted',
  `code` varchar(50) NOT NULL DEFAULT '',
  `currency` CHAR(3) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `meta` text NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


# create table game_log_sync_task
# ------------------------------------------------------------

CREATE TABLE `game_log_sync_task` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_api_id` int UNSIGNED NOT NULL,
  `params` text,
  `sync_username` varchar(50) NOT NULL,
  `sync_start` varchar(255) DEFAULT NULL,
  `sync_end` varchar(255) DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-open, 1-process, 2-success, 3-failed, 4-retry',
  `sync_result` text,
  `type` tinyint UNSIGNED NOT NULL DEFAULT '1' COMMENT '0-auto, 1-manual, 2-retry',
  `prev_task_id` bigint UNSIGNED,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `idx_find_open` (`game_api_id`,`status`, `type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `game_log` (
  `id` char(36) NOT NULL,
  `game_api_id` int UNSIGNED NOT NULL,
  `game_api_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'Columns game_api_code and game_code together uniquely identify a game_name table entry',
  `game_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'Columns game_api_code and game_code together uniquely identify a game_name table entry',
  `external_uid` varchar(64) DEFAULT '' COMMENT 'This is the ID used by game API platform to uniquely identify this record',
  `player_id` int UNSIGNED NOT NULL,
  `player_username` varchar(50) NOT NULL,
  `bet` decimal(13,3) NOT NULL,
  `payout` decimal(13,3) NOT NULL COMMENT 'Payout = Bet + Payoff',
  `bet_time` datetime NOT NULL COMMENT 'The date a bet is placed',
  `settled_time` datetime NOT NULL COMMENT 'The date of settled',
  `bet_details` JSON COMMENT 'Useful information in the original game logs table',
  `status` TINYINT UNSIGNED NOT NULL DEFAULT '1' COMMENT 'An invalid record can be unsettled or cancelled. They are displayed in reports but not counted in summary.',
  `md5_sum` varchar(32) NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`,`bet_time`), -- A PK must include all columns in the table's partitioning function
  KEY `idx_game_api_external_uid` (`game_api_id`,`external_uid`),
  KEY `idx_external_uid` (`external_uid`),
  KEY `idx_bet_time` (`bet_time` DESC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
PARTITION BY RANGE COLUMNS(bet_time) (
  PARTITION p0 VALUES LESS THAN ('2020-07-01'),
  PARTITION p1 VALUES LESS THAN ('2020-08-01'),
  PARTITION p2 VALUES LESS THAN ('2020-09-01'),
  PARTITION p3 VALUES LESS THAN ('2020-10-01'),
  PARTITION p4 VALUES LESS THAN ('2020-11-01'),-- TODO need to manually add PARTITION after this date
  PARTITION p5 VALUES LESS THAN MAXVALUE
);

CREATE TABLE `game_log_hourly_report` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_api_id` INT UNSIGNED NOT NULL,
  `game_api_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'Columns game_api_code and game_code together uniquely identify a game_name table entry',
  `game_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'Columns game_api_code and game_code together uniquely identify a game_name table entry',
  `player_id` INT UNSIGNED NOT NULL,
  `player_username` varchar(50) NOT NULL,
  `bet_count` INT UNSIGNED NOT NULL DEFAULT '0',
  `bet` decimal(13,3) NOT NULL DEFAULT '0',
  `win` decimal(13,3) NOT NULL DEFAULT '0',
  `loss` decimal(13,3) NOT NULL DEFAULT '0',
  `payout` decimal(13,3) NOT NULL DEFAULT '0' COMMENT 'Payout = Bet + Payoff',
  `bet_time_hour` datetime NOT NULL COMMENT 'The hour representation of wager date, contains data for the whole hour 00:00 ~ 23:59',
  `dirty` TINYINT NOT NULL DEFAULT 1 COMMENT 'A dirty record represents that it needs to be calculated in summary.',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_game_player_bet_time` (`game_api_id`,`game_api_code`,`game_code`,`player_id`,`bet_time_hour`),
  KEY `idx_bet_time_hour` (`bet_time_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table deposit_request
#------------------------------------------------------------
CREATE TABLE `deposit_request` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `player_id` INT UNSIGNED NOT NULL,
    `payment_method_id` INT UNSIGNED NOT NULL,
    `payment_channel` VARCHAR(20),
    `amount` DECIMAL(13,3) NOT NULL,
    `status` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-OPEN, 1-APPROVAL, 2-REJECT, 3-CANCEL',
    `requested_date` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `expiration_date` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `external_uid` varchar(64) COMMENT 'This is the ID of external api',
    `payment_charge` DECIMAL(13,3) NOT NULL DEFAULT 0 COMMENT 'Payment_charge = amount * charge_ratio(%) + fixed_charge',
    `note` text,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
    PRIMARY KEY (id),
    CONSTRAINT fk_player_id FOREIGN KEY (player_id) REFERENCES player (id),
    CONSTRAINT fk_payment_method_id FOREIGN KEY (payment_method_id) REFERENCES payment_method (id),
    INDEX `idx_requested_date` (`requested_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE deposit_request AUTO_INCREMENT=100000;

#Dump of table withdraw_request
#------------------------------------------------------------
CREATE TABLE `withdraw_request` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `player_id` INT UNSIGNED NOT NULL,
    `bank_id` INT UNSIGNED NOT NULL,
    `account_number` VARCHAR(50) NOT NULL,
    `account_holder_name` VARCHAR(50),
  	`bank_branch` VARCHAR(50) NOT NULL,
    `amount` DECIMAL(13,3) NOT NULL,
    `status` TINYINT UNSIGNED NOT NULL DEFAULT '1',
    `requested_date` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `external_uid` varchar(64) COMMENT 'This is the ID of external api',
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
    PRIMARY KEY (id),
    CONSTRAINT fk_withdraw_player_id FOREIGN KEY (player_id) REFERENCES player (id),
    CONSTRAINT fk_withdraw_bank_id FOREIGN KEY (bank_id) REFERENCES bank (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table player_bank_account
#------------------------------------------------------------

CREATE TABLE `player_bank_account` (
   `id` int unsigned NOT NULL AUTO_INCREMENT,
   `player_id` int unsigned NOT NULL,
   `bank_id` int unsigned NOT NULL,
   `account_number` varchar(50) NOT NULL DEFAULT '',
   `account_holder_name` varchar(50) DEFAULT NULL,
   `bank_branch` varchar(50) NOT NULL,
   `default_account` tinyint unsigned NOT NULL DEFAULT '0',
   `deleted` tinyint unsigned NOT NULL DEFAULT '0',
   `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
   `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
   PRIMARY KEY (`id`),
   UNIQUE KEY `uk_player_account_number` (`player_id`,`account_number`),
   CONSTRAINT `fk_bankAccount_player_id` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table game_type
#------------------------------------------------------------
CREATE TABLE `game_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `enabled` TINYINT NOT NULL DEFAULT '0',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table game_name
#------------------------------------------------------------
CREATE TABLE `game_name` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_api_code` varchar(50) NOT NULL COMMENT 'Columns game_api_code and game_code together uniquely identifies a game name entry',
  `game_code` varchar(50) NOT NULL COMMENT 'Columns game_api_code and game_code together uniquely identifies a game name entry',
  `game_type_id` int UNSIGNED DEFAULT NULL,
  `featured` tinyint DEFAULT NULL COMMENT 'Recommended game on homepage',
  `released_date` datetime(3) DEFAULT NULL,
  `web` BOOLEAN DEFAULT NULL,
  `mobile` BOOLEAN DEFAULT NULL,
  `user_enabled` BOOLEAN DEFAULT 1,
  `enabled` BOOLEAN DEFAULT 1,
  `demo_link` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sort` int DEFAULT 0,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_game_api_game_code` (`game_api_code`,`game_code`),
  KEY `fk_game_type_id` (`game_type_id`),
  CONSTRAINT `fk_game_type_id` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

#Dump of table game_type_i18n
#------------------------------------------------------------
CREATE TABLE `game_type_i18n` (
  `game_type_id` INT UNSIGNED NOT NULL,
  `locale` VARCHAR(50)  NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`game_type_id`,`locale`),
  CONSTRAINT fk_game_type_i18n_game_type_id FOREIGN KEY (game_type_id) REFERENCES game_type (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table game_name_i18n
#------------------------------------------------------------
CREATE TABLE `game_name_i18n` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_name_id` INT UNSIGNED NOT NULL,
  `locale` VARCHAR(50)  NOT NULL DEFAULT '',
  `name` varchar(500) NOT NULL DEFAULT '',
  `img_url` varchar(500),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_game_name_id_locale` (`game_name_id`, `locale`),
  CONSTRAINT fk_game_name_id FOREIGN KEY (game_name_id) REFERENCES game_name (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Referral rules
CREATE TABLE `promo_player_referral_setting` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL DEFAULT '',
  `content` TEXT,
  `player_credential_id` INT UNSIGNED,
  `referral_type` TINYINT NOT NULL COMMENT '0-one time, 1-deposit revenue, 2-withdraw revenue',
  `fixed_bonus` DECIMAL(13,3),
  `bonus_percentage` DECIMAL(6,3) NOT NULL DEFAULT 0 COMMENT 'Percentage value: 0-100',
  `bonus_auto_release` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-manual, 1-automatic',
  `min_first_deposit_amount` decimal(13,3),
  `min_bonus_amount` decimal(13,3),
  `max_bonus_amount` decimal(13,3),
  `withdraw_condition_multiplier` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `currency` char(3) NOT NULL,
  `enabled` BOOLEAN NOT NULL DEFAULT FALSE,
  `deleted` BOOLEAN NOT NULL DEFAULT FALSE,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_promo_player_referral_setting` (`player_credential_id`,`currency`),
  INDEX `idx_promo_player_referral_setting_referral_type` (`referral_type`),
  CONSTRAINT fk_promo_player_referral_setting_player_credential_id FOREIGN KEY (player_credential_id) REFERENCES player_credential (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table promo_cashback_setting
#------------------------------------------------------------
CREATE TABLE `promo_cashback_setting` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `execution_type` TINYINT NOT NULL DEFAULT 1 COMMENT 'CashbackExecutionTypeEnum: DAILY-1, REALTIME-2',
  `min_bonus_amount` JSON COMMENT 'For example: { "CNY":1000.000, "USD":100.000 }',
  `max_bonus_amount` JSON COMMENT 'For example: { "CNY":50000.000, "USD":5000.000 }',
  `withdraw_condition_multiplier` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `enabled` BOOLEAN NOT NULL DEFAULT FALSE,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Cashback rules
CREATE TABLE `promo_cashback` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `currency` char(3) NOT NULL,
  `bonus_auto_release` BOOLEAN NOT NULL DEFAULT TRUE,
  `enabled` BOOLEAN NOT NULL DEFAULT FALSE,
  `deleted` BOOLEAN NOT NULL DEFAULT FALSE,
  `group_id` INT UNSIGNED,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  CONSTRAINT fk_promo_cashback_group_id FOREIGN KEY (group_id) REFERENCES player_tag (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `promo_cashback_game_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rule_id` INT UNSIGNED NOT NULL,
  `game_api_id` INT UNSIGNED NOT NULL,
  `game_type_id` INT UNSIGNED NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  CONSTRAINT fk_promo_cashback_game_type_rule_id FOREIGN KEY (rule_id) REFERENCES promo_cashback(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_promo_cashback_game_type_game_api_id FOREIGN KEY (game_api_id) REFERENCES game_api(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_promo_cashback_game_type_game_type_id FOREIGN KEY (game_type_id) REFERENCES game_type(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `promo_cashback_bet_range` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rule_id` INT UNSIGNED NOT NULL,
  `limit` DECIMAL(10,3) NOT NULL DEFAULT 0,
  `cashback_percentage` DECIMAL(5,2) NOT NULL DEFAULT 0 COMMENT 'Percentage value: 0-100',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  CONSTRAINT fk_promo_cashback_bet_range_rule_id FOREIGN KEY (rule_id) REFERENCES promo_cashback(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table promo_bonus
#------------------------------------------------------------
CREATE TABLE `promo_bonus` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT UNSIGNED NOT NULL,
  `bonus_date` DATETIME NOT NULL,
  `rule_id` INT NOT NULL,
  `promo_type` TINYINT NOT NULL COMMENT '1-CASHBACK, 2-REFERRAL, 11-CAMPAIGN_DEPOSIT, 12-CAMPAIGN_TASK, 13-CAMPAIGN_RESCUE, 14-CAMPAIGN_LOGIN',
  `bonus_amount` decimal(13,3) NOT NULL DEFAULT 0,
  `reference_amount` DECIMAL(13,3) NOT NULL DEFAULT 0 COMMENT 'This is the amount used to generated this bonus. Bet amount for cashback, deposit amount for deposit bonus, etc.',
  `withdraw_condition_multiplier` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `login_day` INT UNSIGNED COMMENT 'For 14-CAMPAIGN_LOGIN used: Required login day',
  `status` TINYINT NOT NULL COMMENT '0-PENDING_APPROVAL, 1-APPROVED, 2-REJECTED',
  `comment` VARCHAR(4096),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  INDEX `idx_promo_request_player_id` (`player_id` DESC),
  INDEX `idx_promo_request_promo_rule_id` (`rule_id` DESC),
  INDEX `idx_promo_request_bonus_date` (`bonus_date` DESC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table promo_campaign_player
#------------------------------------------------------------
CREATE TABLE `promo_campaign_player` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT UNSIGNED NOT NULL,
  `promo_type` TINYINT UNSIGNED NOT NULL COMMENT '0-DEPOSIT',
  `campaign_id` INT UNSIGNED NOT NULL,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_player_promo` (`player_id`,`promo_type`,`campaign_id`),
  INDEX `idx_promo_campaign_player_player_id` (`player_id` DESC),
  INDEX `idx_promo_campaign_player_promotion` (`promo_type`, `campaign_id` DESC),
  CONSTRAINT fk_promo_campaign_player_player_id FOREIGN KEY (player_id) REFERENCES player (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_promo_campaign_player_campaign_id FOREIGN KEY (campaign_id) REFERENCES promo_campaign_deposit (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table promo_campaign_deposit
# ------------------------------------------------------------
CREATE TABLE `promo_campaign_deposit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `content` TEXT,
  `start_time` datetime(3) COMMENT 'This is the start time being set thru UI',
  `effective_start_time` datetime(3) COMMENT 'This is the actual time when campaign start to be effective',
  `end_time` datetime(3) COMMENT 'This is the end time being set thru UI',
  `effective_end_time` datetime(3) COMMENT 'This is the actual time when campaign ends',
  `fixed_bonus` DECIMAL(13,3),
  `percentage_bonus` decimal(9,6),
  `min_deposit` decimal(13,3),
  `max_bonus_amount` decimal(13,3),
  `bonus_auto_release` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-manual, 1-automatic',
  `bonus_receive_cycle` TINYINT NOT NULL DEFAULT '0' COMMENT '0-Unlimited, 1-Once, 2-Daily, 3-Weekly, 4-Monthly, 5-Yearly',
  `max_bonus_count_per_cycle` int DEFAULT '1',
  `currency` CHAR(3) NOT NULL,
  `withdraw_condition_multiplier` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `auto_join` BOOLEAN NOT NULL DEFAULT FALSE,
  `status` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'DRAFT(0), PENDING_APPROVAL(1), APPROVED(10), DELETED(20)',
  `comment` VARCHAR(4096),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table promo_campaign_task
# ------------------------------------------------------------
CREATE TABLE `promo_campaign_task` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_type` TINYINT NOT NULL COMMENT '0-EmailVerificationBonus, 1-SmsVerificationBonus, 2-IdentityVerificationBonus',
  `name` VARCHAR(50) NOT NULL DEFAULT '',
  `content` TEXT,
  `start_time` datetime(3) COMMENT 'This is the start time being set thru UI',
  `effective_start_time` datetime(3) COMMENT 'This is the actual time when campaign start to be effective',
  `end_time` datetime(3) COMMENT 'This is the end time being set thru UI',
  `effective_end_time` datetime(3) COMMENT 'This is the actual time when campaign ends',
  `fixed_bonus` DECIMAL(13,3),
  `bonus_auto_release` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-manual, 1-automatic',
  `bonus_receive_cycle` TINYINT COMMENT '0-Unlimited, 1-Once, 2-Daily, 3-Weekly, 4-Monthly, 5-Yearly',
  `max_bonus_count_per_cycle` INT DEFAULT '1',
  `currency` CHAR(3) NOT NULL,
  `withdraw_condition_multiplier` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `auto_join` BOOLEAN NOT NULL DEFAULT TRUE,
  `status` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'DRAFT(0), PENDING_APPROVAL(1), REJECTED(9), APPROVED(10), DELETED(20)',
  `comment` VARCHAR(4096),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table promo_campaign_login
# ------------------------------------------------------------
CREATE TABLE `promo_campaign_login` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL DEFAULT '',
  `content` TEXT,
  `start_time` datetime(3) COMMENT 'This is the start time being set thru UI',
  `effective_start_time` datetime(3) COMMENT 'This is the actual time when campaign start to be effective',
  `end_time` datetime(3) COMMENT 'This is the end time being set thru UI',
  `effective_end_time` datetime(3) COMMENT 'This is the actual time when campaign ends',
  `bonus_auto_release` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-manual, 1-automatic',
  `currency` CHAR(3) NOT NULL,
  `withdraw_condition_multiplier` INT UNSIGNED NOT NULL DEFAULT '1',
  `auto_join` BOOLEAN NOT NULL DEFAULT TRUE,
  `status` TINYINT UNSIGNED NOT NULL DEFAULT 1 COMMENT 'DRAFT(0), PENDING_APPROVAL(1), REJECTED(9), APPROVED(10), DELETED(20)',
  `comment` VARCHAR(4096),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table promo_campaign_login_rule
# ------------------------------------------------------------
CREATE TABLE `promo_campaign_login_rule` (
  `campaign_id` INT UNSIGNED NOT NULL,
  `login_day` TINYINT UNSIGNED DEFAULT 1 COMMENT 'How many days players have login consecutively',
  `fixed_bonus` DECIMAL(13,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`campaign_id`, `login_day`),
  CONSTRAINT `fk_campaign_login_rule_campaign_id` FOREIGN KEY (campaign_id) REFERENCES promo_campaign_login (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table content_store
# ------------------------------------------------------------
CREATE TABLE `content_store` (
  `key` VARCHAR(50) NOT NULL ,
  `type` INT NOT NULL ,
  `content` TEXT,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# #Dump of table promo_campaign_task_player_tag
# #------------------------------------------------------------
# CREATE TABLE `promo_campaign_task_player_tag`
# (
#   `id`            INT UNSIGNED NOT NULL AUTO_INCREMENT,
#   `player_tag_id` INT UNSIGNED NOT NULL,
#   `campaign_id`   INT UNSIGNED NOT NULL,
#   `created_at`    datetime(3)  NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
#   `updated_at`    datetime(3)  NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `idx_player_tag_rule_player_tag_id_rule_id` (player_tag_id, campaign_id),
#   CONSTRAINT fk_promo_campaign_task_player_tag_rule_id FOREIGN KEY (campaign_id) REFERENCES promo_campaign_task (id) ON UPDATE CASCADE ON DELETE CASCADE,
#   CONSTRAINT fk_promo_campaign_task_player_tag_player_tag_id FOREIGN KEY (player_tag_id) REFERENCES player_tag (id) ON UPDATE CASCADE ON DELETE CASCADE
# ) ENGINE = InnoDB
#   DEFAULT CHARSET = utf8mb4;


#Dump of table site_privilege
#------------------------------------------------------------
CREATE TABLE `site_privilege` (
  `type` VARCHAR(50) NOT NULL,
  `value` VARCHAR(50) NOT NULL,
  `active` TINYINT NOT NULL DEFAULT 0,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`type`, `value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table mock_game_log
# ------------------------------------------------------------

CREATE TABLE `game_log_mock_game` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_code` varchar(50) NOT NULL DEFAULT '',
  `game_name_id` int DEFAULT NULL,
  `external_uid` varchar(64) DEFAULT '' COMMENT 'This is the ID used by game API platform to uniquely identify this record',
  `player_id` int NOT NULL,
  `player_username` varchar(50) NOT NULL,
  `bet` DECIMAL(13,3) NOT NULL,
  `payout` DECIMAL(13,3) NOT NULL COMMENT 'Payout = Bet + Payoff',
  `bet_time` varchar(50) NOT NULL COMMENT 'The time a bet is placed',
  `valid` tinyint NOT NULL DEFAULT '1' COMMENT 'An invalid record can be unsettled or cancelled. They are displayed in reports but not counted in summary.',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_external_uid` (`external_uid`),
  KEY `idx_external_uid` (`external_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `game_log_mock_game2` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `game_code` varchar(50) NOT NULL DEFAULT '',
  `game_name_id` int DEFAULT NULL,
  `external_uid` varchar(64) DEFAULT '' COMMENT 'This is the ID used by game API platform to uniquely identify this record',
  `player_id` int NOT NULL,
  `player_username` varchar(50) NOT NULL,
  `bet` DECIMAL(13,3) NOT NULL,
  `payout` DECIMAL(13,3) NOT NULL COMMENT 'Payout = Bet + Payoff',
  `bet_time` varchar(50) NOT NULL COMMENT 'The time a bet is placed',
  `valid` tinyint NOT NULL DEFAULT '1' COMMENT 'An invalid record can be unsettled or cancelled. They are displayed in reports but not counted in summary.',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_external_uid` (`external_uid`),
  KEY `idx_external_uid` (`external_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table announcement
#------------------------------------------------------------
CREATE TABLE `announcement` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `message` VARCHAR(250) NOT NULL,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `game_log_t1` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uniqueid` VARCHAR(64),
  `external_uid` varchar(64) NOT NULL COMMENT 'Mix game_platform_id and game_external_uniqueid',
  `game_external_uniqueid` varchar(64) COMMENT 'External uniqueid from game api',
  `username` varchar(50) NOT NULL,
  `merchant_code` VARCHAR(50) NOT NULL,
  `game_platform_id` VARCHAR(50) NOT NULL,
  `game_code` varchar(50) NOT NULL DEFAULT '',
  `game_name` TEXT,
  `game_finish_time` datetime,
  `game_details` TEXT COMMENT 'depends game platform',
  `bet_time` datetime NOT NULL,
  `payout_time` datetime,
  `round_number` varchar(64),
  `real_bet_amount` DECIMAL(13,3) NOT NULL,
  `effective_bet_amount` DECIMAL(13,3) NOT NULL,
  `result_amount` DECIMAL(13,3) NOT NULL COMMENT 'positive is win, negative is loss result_amount = payout_amount – real_bet_amount',
  `payout_amount` DECIMAL(13,3) NOT NULL COMMENT '0 is loss, positive is win payout_amount= result_amount(=payoff) + real_bet_amount',
  `after_balance` DECIMAL(13,3) NOT NULL,
  `bet_details` TEXT COMMENT 'depends game platform ',
  `rent` DECIMAL(13,3) COMMENT '分成',
  `md5_sum` varchar(32),
  `ip_address` varchar(100),
  `bet_type` varchar(20) COMMENT 'Single Bet or Multiple Bet',
  `odds_type` varchar(20) COMMENT 'For sports',
  `odds` double,
  `game_status` varchar(20) COMMENT 'normal or others',
  `detail_status` int NOT NULL DEFAULT '1' COMMENT '1: settled, 2: pending, 3: accepted, 4: rejected, 5: cancelled, 6: void, 7: refund',
  `game_updated_at` datetime,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_game_log_t1_platform_external_uid` (`game_platform_id`,`external_uid`),
  KEY `idx_external_uid` (`external_uid`),
  KEY `idx_game_platform_id` (`game_platform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `game_log_t1lottery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uniqueid` bigint DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `game_platform_id` int DEFAULT NULL,
  `game_code` varchar(20) DEFAULT NULL,
  `game_name` varchar(100) DEFAULT NULL,
  `game_finish_time` int DEFAULT NULL,
  `bet_details` varchar(200) DEFAULT NULL,
  `bet_time` int NOT NULL,
  `payout_time` int DEFAULT NULL,
  `after_bet_balance` DECIMAL(13,3) NOT NULL COMMENT 'The balance after bet',
  `after_balance` DECIMAL(13,3) NOT NULL COMMENT 'The balance after payout',
  `real_bet_amount` DECIMAL(13,3) NOT NULL COMMENT '',
  `effective_bet_amount` double DEFAULT NULL,
  `result_amount` DECIMAL(13,3) NOT NULL COMMENT 'Result amount (Payout amount minus bet principal)',
  `payout_amount` DECIMAL(13,3) NOT NULL COMMENT '',
  `refund_amount` DECIMAL(13,3) NOT NULL COMMENT '',
  `remark` varchar(100) DEFAULT NULL,
  `rule_id` int DEFAULT NULL,
  `period` varchar(200) DEFAULT NULL,
  `bonus` DECIMAL(13,3) NOT NULL COMMENT '',
  `rebate` DECIMAL(13,3) NOT NULL COMMENT '',
  `mode` int DEFAULT NULL COMMENT 'The mode of bet 下注模式(分/角/元) ',
  `multiple` int DEFAULT NULL,
  `count` int DEFAULT NULL COMMENT 'The count of bet 注数',
  `status` int NOT NULL DEFAULT '0' COMMENT '1: New bet, 2: Player refund, 3: Stop bet if win in chase, 4: Win, 5: Lose, 6: Terminate, 7: Tie, 8: Lottery issue error, cancel result and return the principal',
  `open_code` varchar(255) DEFAULT NULL,
  `chase` bigint DEFAULT NULL COMMENT 'ID of chase; if this field is null, then this bet does not belong to any chase',
  `last_updated_time` int DEFAULT NULL,
  `md5_sum` varchar(64) DEFAULT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uniqueid` (`uniqueid`),
  KEY `idx_md5_sum` (`md5_sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


#Dump of table summary daily
#------------------------------------------------------------
CREATE TABLE `summary_daily` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `total_player_count` INT UNSIGNED COMMENT 'Total count of all players exist in the system',
  `total_deposit_count` INT UNSIGNED,
  `total_deposit_amount` DECIMAL(22,3),
  `total_withdrawal_count` INT UNSIGNED,
  `total_withdrawal_amount` DECIMAL(22,3),
  `bonus` DECIMAL(22,3),
  `cashback` DECIMAL(22,3),
  `bet_amount` DECIMAL(22,3),
  `new_player_without_deposit` INT UNSIGNED,
  `new_player_with_deposit` INT UNSIGNED,
  `active_player_count` INT UNSIGNED COMMENT 'only those who got wagerring records are considered as active player',
  `active_player_count_by_gametype` TEXT COMMENT 'return GametypeID with corresp. player count in JSON format',
  `player_total_win` DECIMAL(22,3) COMMENT '',
  `player_total_loss` DECIMAL(22,3) COMMENT '',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_sumnmary_daily_uid` (`timezone`,`currency`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table summary revenue
#------------------------------------------------------------
CREATE TABLE `summary_bet_gross_profit_status` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'LAST_WEEK' COMMENT 'LAST_WEEK, LAST_MONTH',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `bet_amount` DECIMAL(22,3),
  `player_win` DECIMAL(22,3),
  `player_loss` DECIMAL(22,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_bet_gross_profit_status_uid` (`period`,`timezone`,`currency`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_player_status` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_WEEK, LAST_MONTH',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `new_player_without_deposit` INT UNSIGNED,
  `new_player_with_deposit` INT UNSIGNED,
  `total_player_count` INT UNSIGNED COMMENT 'cumulative new player counts within the period',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_player_status_uid` (`period`,`timezone`,`currency`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_net_profit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_WEEK, LAST_MONTH',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `cashback` DECIMAL(22,3),
  `bonus` DECIMAL(22,3),
  `player_win` DECIMAL(22,3),
  `player_loss` DECIMAL(22,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_net_profit_uid` (`period`,`timezone`,`currency`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table summary top index
#------------------------------------------------------------
CREATE TABLE `summary_top_deposit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_7_DAYS, LAST_30_DAYS',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `category` varchar(20) NOT NULL DEFAULT 'BY_COUNT' COMMENT 'BY_COUNT(0), BY_AMOUNT(1), BY_MAX_AMOUNT(2)',
  `rank` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `player_username` varchar(50) NOT NULL DEFAULT '',
  `deposit_count` INT UNSIGNED,
  `deposit_method` VARCHAR(20),
  `deposit_amount` DECIMAL(13,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_top_deposit_uid` (`period`,`timezone`,`currency`,`start_date`,`category`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_top_withdrawal` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_7_DAYS, LAST_30_DAYS',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `category` varchar(20) NOT NULL DEFAULT 'BY_COUNT' COMMENT 'BY_COUNT(0), BY_AMOUNT(1)',
  `rank` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `player_username` varchar(50) NOT NULL DEFAULT '',
  `withdrawal_count` INT UNSIGNED,
  `withdrawal_amount` DECIMAL(22,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_top_withdrawal_uid` (`period`,`timezone`,`currency`,`start_date`,`category`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_top_winner` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_7_DAYS, LAST_30_DAYS',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `rank` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `player_username` varchar(50) NOT NULL DEFAULT '',
  `player_win` DECIMAL(13,3) NOT NULL DEFAULT '0.000',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_top_winner_uid` (`period`,`timezone`,`currency`,`start_date`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_top_popular_game` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_7_DAYS, LAST_30_DAYS',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `category` varchar(20) NOT NULL DEFAULT 'BY_PLAYER' COMMENT 'BY_PLAYER(0), BY_BET(1), BY_WIN(2), BY_LOSS(3), BY_REV(4), BY_REV_RATIO(5), BY_AVERAGE_BET(6)',
  `rank` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `game_code` varchar(50) NOT NULL COMMENT 'Columns game_api_code and game_code together uniquely identifies a game name entry',
  `player_count` INT UNSIGNED,
  `bet_amount` DECIMAL(13,3),
  `player_win` DECIMAL(22,3),
  `player_loss` DECIMAL(22,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_top_popular_game_uid` (`period`,`timezone`,`currency`,`start_date`,`category`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `summary_game_provider_info` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `period` varchar(20) NOT NULL DEFAULT 'TODAY' COMMENT 'TODAY, LAST_30_DAYS, THIS_MONTH',
  `timezone` varchar(64) NOT NULL DEFAULT 'UTC',
  `currency` CHAR(3) NOT NULL,
  `start_date` DATE COMMENT 'use for indicating DATE of the calculated data',
  `rank` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `game_api_id` INT UNSIGNED NOT NULL DEFAULT '0',
  `game_type_id` INT UNSIGNED DEFAULT NULL DEFAULT '0',
  `bet` DECIMAL(13,3),
  `win` DECIMAL(22,3),
  `loss` DECIMAL(22,3),
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_summary_game_provider_info_uid` (`period`,`timezone`,`currency`,`start_date`,`rank`),
  UNIQUE KEY `uk_game_api_type_id` (`period`,`timezone`,`currency`,`start_date`,`rank`,`game_api_id`,`game_type_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `credit_point` (
  `row_lock` char(1) PRIMARY KEY DEFAULT 'X',
  `balance` DECIMAL(13,3) NOT NULL CHECK (balance >= 0),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  CONSTRAINT row_lock_uni CHECK (row_lock = 'X')
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into credit_point (balance) values (0);

# Dump of table credit_point_history
# ------------------------------------------------------------

CREATE TABLE `credit_point_history` (
  `id` INT UNSIGNED NOT NULL,
  `from` VARCHAR(6),
  `to` VARCHAR(6),
  `status` TINYINT NOT NULL DEFAULT '0',
  `timeout` TINYINT NOT NULL DEFAULT '0',
  `credit_point` DECIMAL(13,3) NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table withdraw_condition
# ------------------------------------------------------------

CREATE TABLE `withdraw_condition` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT UNSIGNED NOT NULL,
  `source_type` TINYINT NOT NULL,
  `source_id` INT UNSIGNED NOT NULL,
  `promotion_type` TINYINT UNSIGNED DEFAULT NULL COMMENT '1-CASHBACK, 2-REFERRAL, 11-CAMPAIGN_DEPOSIT, 12-CAMPAIGN_TASK, 13-CAMPAIGN_RESCUE',
  `promotion_id` INT UNSIGNED,
  `bet_required` DECIMAL(13,3) NOT NULL COMMENT 'Amount of bet required to unlock this condition',
  `bet_amount` DECIMAL(13,3) NOT NULL DEFAULT '0' COMMENT 'Amount of bets calculated under this withdraw condition so far.',
  `locked_on_amount` DECIMAL(13,3) NOT NULL,
  `begin_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `status` TINYINT NOT NULL DEFAULT '0',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table credit_point_transaction
# ------------------------------------------------------------

CREATE TABLE `credit_point_transaction` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_type` TINYINT NOT NULL,
  `transaction_id` INT UNSIGNED,
  `credit_point` DECIMAL(13,3) NOT NULL DEFAULT 0,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
   PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table message
# ------------------------------------------------------------

CREATE TABLE `player_message` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `thread_id` INT UNSIGNED COMMENT 'This field holds the id of the 1st message of this thread, every message replied to this thread will have the same thread_id',
  `player_id` INT UNSIGNED NOT NULL,
  `operator_id` INT UNSIGNED DEFAULT NULL,
  `subject` VARCHAR(200) NOT NULL,
  `content` TEXT,
  `read` BOOLEAN NOT NULL DEFAULT FALSE,
  `sender` TINYINT UNSIGNED NOT NULL COMMENT '0: operator, 1: player',
  `deleted` BOOLEAN NOT NULL DEFAULT FALSE,
  `system` BOOLEAN NOT NULL DEFAULT FALSE,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `fk_player_id` (`player_id`),
  KEY `fk_operator_id` (operator_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `player_message_template_setting` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `template_type` varchar(20) NOT NULL,
    `subject` TEXT,
    `content` TEXT,
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_template_type` (`template_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dump of table announcement
#------------------------------------------------------------
CREATE TABLE `player_announcement` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NOT NULL DEFAULT '',
  `content` VARCHAR(250) NOT NULL,
  `start_at` DATETIME(3) NOT NULL DEFAULT '1000-01:01 00:00:00',
  `end_at` DATETIME(3) NOT NULL DEFAULT '9999-12-31 00:00:00',
  `player_tag_operator` VARCHAR(3) NOT NULL DEFAULT 'any' COMMENT 'This payment method should be applicable to \'any\' or \'all\' player tags. Allowed values: any|all',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# create table player_announcement_player_tag
# constraints of players for player_announcement
# ------------------------------------------------------------
CREATE TABLE `player_announcement_player_tag` (
  `announcement_id` INT UNSIGNED NOT NULL,
  `player_tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`announcement_id`,`player_tag_id`),
  CONSTRAINT `FK_player_announcement_player_tag_id` FOREIGN KEY (announcement_id) REFERENCES player_announcement(id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `fk_player_announcement_player_level_id` FOREIGN KEY (player_tag_id) REFERENCES player_tag(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


# Dump of table withdraw_workflow_setting
# ------------------------------------------------------------

CREATE TABLE `withdraw_workflow_setting` (
  `id` TINYINT unsigned NOT NULL,
  `name` VARCHAR(200) DEFAULT '' COMMENT 'For non-required step, client can customized name',
  `enabled` TINYINT(1) NOT NULL DEFAULT '0',
  `required` TINYINT(1) NOT NULL COMMENT 'when required is ture, the step is default by system',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table deposit_request_process_history
# ------------------------------------------------------------

CREATE TABLE `deposit_request_process_history` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `from_status` TINYINT UNSIGNED COMMENT 'Refer to DepositStatusEnum',
  `to_status` TINYINT UNSIGNED COMMENT 'Refer to DepositStatusEnum',
  `deposit_request_id` BIGINT UNSIGNED NOT NULL,
  `comment` TEXT,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `created_by` INT,
  `updated_by` INT,
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY(id),
  CONSTRAINT fk_deposit_request_id FOREIGN KEY (deposit_request_id) REFERENCES deposit_request (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table request_history
# ------------------------------------------------------------

CREATE TABLE `withdraw_request_process_history` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `from_status` TINYINT UNSIGNED COMMENT 'Refer to WithdrawStatusEnum',
  `to_status` TINYINT UNSIGNED COMMENT 'Refer to WithdrawStatusEnum',
  `withdraw_request_id` BIGINT UNSIGNED NOT NULL,
  `withdraw_workflow_id` TINYINT UNSIGNED COMMENT 'Refer to WithdrawStatusEnum',
  `comment` TEXT,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `created_by` INT,
  `updated_by` INT,
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY(id),
  CONSTRAINT fk_withdraw_request_id FOREIGN KEY (withdraw_request_id) REFERENCES withdraw_request (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table sms_api
# ------------------------------------------------------------

CREATE TABLE `sms_api` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` TINYINT NOT NULL DEFAULT 0 Comment '0-Disabled, 1-Enabled, 10-Inactive, 20-Deleted',
  `code` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `meta` text NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


# Dump of table sms_otp_history
# ------------------------------------------------------------

CREATE TABLE `sms_otp_history` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone_number` VARCHAR(20) NOT NULL,
  `otp_code` VARCHAR(6) NOT NULL,
  `sms_api_id` INT UNSIGNED NOT NULL,
  `player_id` INT UNSIGNED NOT NULL,
  `verified` TINYINT NOT NULL DEFAULT '0' COMMENT '1=Verified, 0= Not Verified',
  `delivered` TINYINT NOT NULL DEFAULT '0' COMMENT '1=Delivered, 0= Not Delivered',
  `remark` VARCHAR(300) COMMENT 'store the response of sms api',
  `expiration_time` datetime(3) NOT NULL COMMENT 'Expiry time of OTP code ',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table email_history
# ------------------------------------------------------------

CREATE TABLE `email_history` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_address` VARCHAR(255) NOT NULL,
  `recipient_address` VARCHAR(255) NOT NULL,
  `subject` VARCHAR(255) DEFAULT NULL,
  `content` VARCHAR(3000) DEFAULT NULL,
  `player_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table player_activity_report
# ------------------------------------------------------------

CREATE TABLE `player_activity_report` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT,
  `action` INT COMMENT 'For example: LOGIN=1',
  `invoke` varchar(100) COMMENT 'controller and method name',
  `http_method` CHAR(4),
  `request_uuid` CHAR(36),
  `request_uri` varchar(100),
  `request_body` text COMMENT 'Represent request body',
  `query_string` text COMMENT 'Represent request param',
  `return_code` INT COMMENT 'Represent action result',
  `response` text COMMENT 'Represent result',
  `exception` text COMMENT 'Represent exception',
  `device` SMALLINT UNSIGNED,
  `ip` varchar(50),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table ext_api_activity_report
# ------------------------------------------------------------

CREATE TABLE `ext_api_activity_report` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` INT,
  `ext_api_type` INT,
  `invoke` varchar(100) COMMENT 'controller and method name',
  `params` text COMMENT 'Represent request params',
  `return_code` INT COMMENT 'Represent action result',
  `api_result` text COMMENT 'Represent result',
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table operator_activity_report
# ------------------------------------------------------------

CREATE TABLE `operator_activity_report` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `operator_id` INT UNSIGNED,
  `username` varchar(50),
  `invoke` varchar(100) COMMENT 'controller and method name',
  `request_uuid` CHAR(36),
  `http_method` CHAR(4),
  `request_uri` varchar(100),
  `request_body` text COMMENT 'Represent request body',
  `query_string` text COMMENT 'Represent request param',
  `return_code` INT COMMENT 'Represent action result',
  `response` text COMMENT 'Represent result',
  `exception` text COMMENT 'Represent exception',
  `device` SMALLINT UNSIGNED,
  `ip` varchar(50),
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table email_verificaton_history
# ------------------------------------------------------------

CREATE TABLE `email_verification_history` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `otp_code` VARCHAR(6) NOT NULL,
  `player_id` INT UNSIGNED NOT NULL,
  `verified` BOOLEAN NOT NULL DEFAULT FALSE,
  `expiration_time` DATETIME(3) NOT NULL COMMENT 'Expiry time of email token ',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
    KEY `fk_player_id` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Automation related tables
CREATE TABLE `automation_job` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL DEFAULT '',
  `filters` JSON,
  `actions` JSON,
  `start_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'This defines the 1st time this job should be run',
  `max_run` INT UNSIGNED DEFAULT 0 COMMENT 'Defines how many times this job should run, regardless of the outcome. 0 means no limit.',
  `run_frequency` TINYINT UNSIGNED DEFAULT 0 COMMENT 'Defines how frequent this is run. NEVER(0), EVERY_MINUTE(1), EVERY_HOUR(2), EVERY_DAY(3), EVERY_WEEK(4), EVERY_MONTH(5)',
  `deleted` BOOLEAN NOT NULL DEFAULT 0,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
)ENGINE = InnoDB COMMENT='Automation jobs will be evaluated by a schedule task every minute. Jobs are by default evaluated on players.'
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `automation_record` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `job_id` INT UNSIGNED,
    `job_name` VARCHAR(100) NOT NULL DEFAULT '',
    `run_count` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT 'So far how many times this job has run',
    `status` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-Undefined, 1-In Progress, 10-Completed, 20-Cancelled, 21-Error',
    `affected_player_ids` text COMMENT 'Splitted by commas',
    `affected_player_count` INT UNSIGNED,
    `automated` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Executed by 0-Manual, 1-Automatic',
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `completed_at` DATETIME(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_job_id_run_count` (`job_id`, `run_count`)
)ENGINE = InnoDB COMMENT='Automation record will be created once a job is started by the engine'
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `command_history` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `content` text NOT NULL,
    `method_name` VARCHAR(50) NOT NULL,
    `command_type` TINYINT UNSIGNED NOT NULL COMMENT '0-RECEIVE, 1-PUBLISH',
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table operator_oauth
# ------------------------------------------------------------

CREATE TABLE `operator_oauth` (
  `operator_id` INT UNSIGNED NOT NULL,
  `access_token` varchar(256) NOT NULL,
  `refresh_token` VARCHAR(256) NOT NULL,
  `next_refresh_time` datetime(3) NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`operator_id`),
  CONSTRAINT fk_operator_id FOREIGN KEY (operator_id) REFERENCES operator (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

## Create view for merging different type of campaigns
## CAMPAIGN_DEPOSIT(11, "deposit"), CAMPAIGN_TASK(12, "task"), CAMPAIGN_LOGIN(14, "login")
CREATE VIEW `promo_campaign`
AS SELECT
       11 AS `type`,
        NULL AS `task_type`,
       `promo_campaign_deposit`.`id` AS `id`,
       `promo_campaign_deposit`.`currency` AS `currency`,
       `promo_campaign_deposit`.`name` AS `name`,
       `promo_campaign_deposit`.`content` AS `content`,
       `promo_campaign_deposit`.`start_time` AS `start_time`,
       `promo_campaign_deposit`.`effective_start_time` AS `effective_start_time`,
       `promo_campaign_deposit`.`end_time` AS `end_time`,
       `promo_campaign_deposit`.`effective_end_time` AS `effective_end_time`,
       `promo_campaign_deposit`.`status` AS `status`,
       `promo_campaign_deposit`.`bonus_receive_cycle` AS `bonus_receive_cycle`,
       `promo_campaign_deposit`.`auto_join` AS `auto_join`,
       `promo_campaign_deposit`.`created_at` AS `created_at`,
       `promo_campaign_deposit`.`updated_at` AS `updated_at`
    FROM `promo_campaign_deposit` WHERE `status` != 20
UNION
    SELECT
       12 AS `type`,
       `promo_campaign_task`.task_type,
       `promo_campaign_task`.`id` AS `id`,
       `promo_campaign_task`.`currency` AS `currency`,
       `promo_campaign_task`.`name` AS `name`,
       `promo_campaign_task`.`content` AS `content`,
       `promo_campaign_task`.`start_time` AS `start_time`,
       `promo_campaign_task`.`effective_start_time` AS `effective_start_time`,
       `promo_campaign_task`.`end_time` AS `end_time`,
       `promo_campaign_task`.`effective_end_time` AS `effective_end_time`,
       `promo_campaign_task`.`status` AS `status`,
       `promo_campaign_task`.`bonus_receive_cycle` AS `bonus_receive_cycle`,
       `promo_campaign_task`.`auto_join` AS `auto_join`,
       `promo_campaign_task`.`created_at` AS `created_at`,
       `promo_campaign_task`.`updated_at` AS `updated_at`
    FROM `promo_campaign_task` WHERE `status` != 20
UNION
    SELECT
        14 AS `type`,
        NULL AS `task_type`,
        `promo_campaign_login`.`id` AS `id`,
        `promo_campaign_login`.`currency` AS `currency`,
        `promo_campaign_login`.`name` AS `name`,
        `promo_campaign_login`.`content` AS `content`,
        `promo_campaign_login`.`start_time` AS `start_time`,
        `promo_campaign_login`.`effective_start_time` AS `effective_start_time`,
        `promo_campaign_login`.`end_time` AS `end_time`,
        `promo_campaign_login`.`effective_end_time` AS `effective_end_time`,
        `promo_campaign_login`.`status` AS `status`,
        2 AS `bonus_receive_cycle`,
        `promo_campaign_login`.`auto_join` AS `auto_join`,
        `promo_campaign_login`.`created_at` AS `created_at`,
        `promo_campaign_login`.`updated_at` AS `updated_at`
    FROM `promo_campaign_login` WHERE `status` != 20;

# Dump of table ip_rule
# ------------------------------------------------------------

CREATE TABLE `ip_rule` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` VARCHAR(30) NOT NULL DEFAULT '',
  `site` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '0-BACK_OFFICE, 1-FRONT_OFFICE',
  `remark` VARCHAR(1000) DEFAULT NULL,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table ip_rule_country
# ------------------------------------------------------------

CREATE TABLE `ip_rule_country` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_code` VARCHAR(2) NOT NULL comment 'ISO 3166-1 alpha-2, 2 characters.',
  `enabled` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_country_code` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table ip_rule_country_i18n
# ------------------------------------------------------------

CREATE TABLE `ip_rule_country_i18n` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` INT UNSIGNED NOT NULL,
  `locale` VARCHAR(20) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_country_id_locale` (`country_id`, `locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# JdbcTokenSource start
CREATE TABLE `clientdetails` (
  `appId` varchar(128) NOT NULL,
  `resourceIds` varchar(256) DEFAULT NULL,
  `appSecret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `grantTypes` varchar(256) DEFAULT NULL,
  `redirectUrl` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` INT DEFAULT NULL,
  `refresh_token_validity` INT DEFAULT NULL,
  `additionalInformation` varchar(4096) DEFAULT NULL,
  `autoApproveScopes` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`appId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_approvals` (
  `userId` varchar(256) DEFAULT NULL,
  `clientId` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `expiresAt` timestamp NULL DEFAULT NULL,
  `lastModifiedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_client_details` (
  `client_id` varchar(128) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` INT DEFAULT NULL,
  `refresh_token_validity` INT DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_client_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
# JdbcTokenSource end

# player favorite game
CREATE TABLE `player_favorite_game` (
  `player_id` INT UNSIGNED NOT NULL,
  `game_api_id` INT UNSIGNED NOT NULL,
  `game_code` varchar(50) NOT NULL,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  UNIQUE KEY `idx_player_id_game_api_id_game_code` (`player_id`, `game_api_id`, `game_code`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4;

# Dump of table domain_config
# ------------------------------------------------------------

CREATE TABLE `domain_config` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL COMMENT 'domain name example: www.t1t.com',
  `site` TINYINT UNSIGNED NOT NULL COMMENT '0-BackOffice, 1-FrontOffice',
  `enabled` BOOLEAN NOT NULL,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table site_property
# ------------------------------------------------------------
CREATE TABLE `site_property` (
  `key` VARCHAR(100) NOT NULL,
  `value` text,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  UNIQUE KEY `idx_property_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# images
CREATE TABLE `resource_upload` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_path` VARCHAR(256) NOT NULL,
  `file_name` VARCHAR(256) COMMENT 'Name of the file',
  `name` VARCHAR(256) COMMENT 'Recognized file name',
  `size` INT UNSIGNED COMMENT 'Size of a file (in bytes)',
  `width` INT UNSIGNED,
  `height` INT UNSIGNED,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4;

# Dump of table withdraw_summary_report
# ------------------------------------------------------------

CREATE TABLE `withdraw_summary_report` (
  `time_hour` DATETIME(3) NOT NULL,
  `currency` CHAR(3) NOT NULL,
  `withdrawal_count` INT UNSIGNED NOT NULL,
  `withdrawer_count` INT UNSIGNED NOT NULL,
  `withdrawal_amount` DECIMAL(13,3) NOT NULL,
  `dirty` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'A dirty record represents that it needs to be calculated.',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`time_hour`, `currency`),
  INDEX `idx_dirty` (`dirty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# Dump of table deposit_summary_report
# ------------------------------------------------------------

CREATE TABLE `deposit_summary_report` (
  `time_hour` DATETIME(3) NOT NULL,
  `currency` CHAR(3) NOT NULL,
  `deposit_count` INT UNSIGNED NOT NULL,
  `depositor_count` INT UNSIGNED NOT NULL,
  `deposit_amount` DECIMAL(13,3) NOT NULL,
  `dirty` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'A dirty record represents that it needs to be calculated.',
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`time_hour`, `currency`),
  INDEX `idx_dirty` (`dirty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

# player_summary_report
CREATE TABLE `player_summary_report` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `time_hour` datetime NOT NULL COMMENT 'The hour representation date, contains data for the whole hour 00:00 ~ 23:59',
  `total_count` INT UNSIGNED,
  `new_count` INT UNSIGNED,
  `device` INT UNSIGNED,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_player_summary_report_time_hour` (`time_hour`, `device`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4;

# player_online_report
CREATE TABLE `player_online_report` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `time_hour` datetime NOT NULL COMMENT 'The hour representation date, contains data for the whole hour 00:00 ~ 23:59',
  `max` INT UNSIGNED,
  `average` FLOAT UNSIGNED,
  `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_player_online_report_time_hour` (`time_hour`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4;

# Dump of table time_hour_list
# ------------------------------------------------------------
CREATE TABLE `time_hour_list` (
  `time_hour` datetime NOT NULL,
  PRIMARY KEY (`time_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

create view master_summary_report AS (
SELECT
	t.time_hour,
	s.value AS currency,
	SUM(game_log_hourly_report.bet_count) AS bet_count,
	SUM(game_log_hourly_report.bet) AS bet,
	SUM(game_log_hourly_report.payout) AS payout,
	deposit_summary_report.deposit_amount,
	deposit_summary_report.deposit_count,
	(SELECT
		SUM(player_summary_report.new_count) AS player_count
	FROM player_summary_report
	WHERE time_hour = t.time_hour) AS player_count,
	withdraw_summary_report.withdrawal_amount,
	withdraw_summary_report.withdrawal_count
FROM
	time_hour_list t CROSS JOIN site_privilege AS s ON s.type = 'SITE_CURRENCY'
LEFT JOIN game_log_hourly_report ON
	game_log_hourly_report.bet_time_hour = t.time_hour
INNER JOIN game_api ON
	game_api.currency = s.value
LEFT JOIN deposit_summary_report ON
	deposit_summary_report.time_hour = t.time_hour
	AND deposit_summary_report.currency = s.value
LEFT JOIN withdraw_summary_report ON
	withdraw_summary_report.time_hour = t.time_hour
	AND withdraw_summary_report.currency = s.value
GROUP BY
	t.time_hour, currency);
