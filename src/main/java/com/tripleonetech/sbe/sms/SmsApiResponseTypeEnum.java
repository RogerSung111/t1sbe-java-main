package com.tripleonetech.sbe.sms;

public enum SmsApiResponseTypeEnum {
    XML, JSON;
}
