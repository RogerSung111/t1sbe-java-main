package com.tripleonetech.sbe.sms;

public class SmsMessage {
    
    private String receiverNumber;
    private String message;
 
    public String getReceiverNumber() {
        return receiverNumber;
    }
    public void setReceiverNumber(String receiverNumber) {
        this.receiverNumber = receiverNumber;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    public static SmsMessage createSmsMessage(String phoneNumber, String message) {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage(message);
        smsMessage.setReceiverNumber(phoneNumber);
        return smsMessage;
    }
    
}
