package com.tripleonetech.sbe.sms;

import org.apache.ibatis.annotations.Mapper;

import com.tripleonetech.sbe.common.integration.ApiConfigMapper;

@Mapper
public interface SmsApiConfigMapper extends ApiConfigMapper {
    
    //Only a SMS api is enabled
    SmsApiConfig getEnabledSmsApi();
}
