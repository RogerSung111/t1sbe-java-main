package com.tripleonetech.sbe.sms.integration;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tripleonetech.sbe.common.integration.AbstractThirdPartyApi;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.sms.SmsApiResponseTypeEnum;

public abstract class AbstractSmsApi extends AbstractThirdPartyApi implements SmsApi{
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    protected abstract String getApiUrl();

    protected abstract String getBalanceUrl();
            
    protected abstract String signContent(String content);
    
    protected ApiResult handleResponse(HttpResponse httpResponse, Class<? extends SmsBaseResponse> clazz,
            SmsApiResponseTypeEnum apiResponseType) {
        
        if(httpResponse == null) {
            logger.error("Failed to process empty HttpResponse");
            return new ApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        }
        
        int responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        if (responseStatusCode != HttpStatus.SC_OK) {
            logger.error("Unexpected HTTP status: [{}]", responseStatusCode);
            return new ApiResult(ApiResponseEnum.SERVICE_UNAVAILABLE);
        }
        
        ApiResult apiResult = null;
        try {
            String response = EntityUtils.toString(httpResponse.getEntity());
            logger.debug("Api response : {}", response);
            SmsBaseResponse responseObj = null;
            switch (apiResponseType) {
                case XML:
                    responseObj = new XmlMapper().readValue(response, clazz);
                    break;
                case JSON:
                    responseObj = JsonUtils.jsonToObject(response, clazz);
            }
            apiResult = new ApiResult(responseObj.getApiResponse());
            apiResult.setMessage(responseObj.getApiResponseMessage());
            apiResult.setParam("response", responseObj);

        }catch(IOException e) {
            logger.error("Failed to parse api response - Exception message : {}", e);
        }
        return apiResult;
    }
}
