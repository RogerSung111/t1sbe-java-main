package com.tripleonetech.sbe.sms.integration.impl;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SmsApiIhuyiBalanceResponse extends SmsApiIhuyiBaseResponse {
    private BigDecimal num;

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

}
