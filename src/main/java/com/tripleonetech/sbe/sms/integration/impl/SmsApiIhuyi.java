package com.tripleonetech.sbe.sms.integration.impl;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.sms.SmsApiResponseTypeEnum;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.integration.AbstractSmsApi;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class SmsApiIhuyi extends AbstractSmsApi {
    
    private final static Logger logger = LoggerFactory.getLogger(SmsApiIhuyi.class);

    private static List<ApiMetaField> META_FIELDS;
    private final static String API_URI_SEND = "http://106.ihuyi.com/webservice/sms.php?method=Submit";
    private final static String API_URI_GET_BALANCE = "http://106.ihuyi.com/webservice/sms.php?method=GetNum";
    
    static {
        
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("apiId", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("apiKey", MetaFieldTypeEnum.TEXT, true));
            }
        };        
    }
    
    @Override
    public ApiResult getBalance() {
        Map<String, String> payload = new HashMap<>();
        payload.put("account", getConfig().getFields().get("apiId"));
        payload.put("password", getConfig().getFields().get("apiKey"));

        HttpResponse response = submitGet(getBalanceUrl(), payload);
        ApiResult apiResult = handleResponse(response, SmsApiIhuyiBalanceResponse.class, SmsApiResponseTypeEnum.XML);
        
        return apiResult;
    }

    @Override
    protected String signContent(String content) {
        return content;
    }
    
    @Override
    public ApiResult sendMessage(SmsMessage smsMessage) {
        
        Map<String, String> payload = new HashMap<>();
        payload.put("account", getConfig().getFields().get("apiId"));
        payload.put("password", getConfig().getFields().get("apiKey"));
        payload.put("mobile", smsMessage.getReceiverNumber());
        payload.put("content", smsMessage.getMessage());
        
        HttpResponse response = submitGet(getApiUrl(), payload);
        ApiResult result = handleResponse(response, SmsApiIhuyiSmsSendResponse.class, SmsApiResponseTypeEnum.XML);

        return result;
    }

    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    @Override
    protected String getApiUrl() {
        return API_URI_SEND;
    }

    @Override
    protected String getBalanceUrl() {
        return API_URI_GET_BALANCE;
    }
      
}
