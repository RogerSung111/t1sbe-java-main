package com.tripleonetech.sbe.sms.integration;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiConfigMapper;
import com.tripleonetech.sbe.common.integration.ApiFactory;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.sms.SmsApiConfig;
import com.tripleonetech.sbe.sms.SmsApiConfigMapper;

@Component
public class SmsApiFactory extends ApiFactory<SmsApi> {
    private String enabledApiConfigComparison;
    @Autowired
    private SmsApiConfigMapper configMapper;

    @Override
    protected ApiTypeEnum getApiType() {
        return ApiTypeEnum.SMS;
    }

    @Override
    protected ApiConfigMapper getConfigMapper() {
        return this.configMapper;
    }

    public SmsApi getEnabledSmsApi() {
        SmsApiConfig apiConfig = configMapper.getEnabledSmsApi();
        String enabledApiConfigComparison = StringUtils.join(
                apiConfig.getId(),
                apiConfig.getStatus(),
                apiConfig.getMeta());
        if (!enabledApiConfigComparison.equals(this.enabledApiConfigComparison)) {
            this.refreshById(apiConfig.getId());
            this.enabledApiConfigComparison = enabledApiConfigComparison;
        }

        return this.getById(configMapper.getEnabledSmsApi().getId());
    }
}
