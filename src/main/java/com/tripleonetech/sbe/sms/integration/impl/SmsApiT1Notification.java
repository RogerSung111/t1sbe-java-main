package com.tripleonetech.sbe.sms.integration.impl;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.*;
import com.tripleonetech.sbe.common.notification.T1Notification;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.notification.T1CommonNotification;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.integration.SmsApi;

@Component
@Scope("prototype")
public class SmsApiT1Notification extends AbstractThirdPartyApi implements SmsApi {
    private static List<ApiMetaField> META_FIELDS;

    @Autowired
    private T1NotificationApi notificationApi;

    static {
        META_FIELDS = Arrays.asList(new ApiMetaField("sender", MetaFieldTypeEnum.TEXT, false));
    }

    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    @Override
    public ApiResult sendMessage(SmsMessage smsMessage) throws RestResponseException {
        if (!isAvailablePhone(smsMessage.getReceiverNumber())) {
            throw new RestResponseException(RestResponse.invalidParameter("The phone number does not conform to the format"));
        }

        return notificationApi.send(new T1Notification()
                .type(T1CommonNotification.MessageTypeEnum.SMS)
                .content(smsMessage.getMessage())
                .addRecipients(smsMessage.getReceiverNumber()));
    }

    @Override
    public ApiResult getBalance() {
        ApiResult apiResult = new ApiResult(ApiResponseEnum.NOOP);
        apiResult.setMessage("Not in service");
        return apiResult;
    }

    private boolean isAvailablePhone(String phoneNumber) {
        String regex = "^[1-9]{1}[\\d]{0,2}-[1-9]{1}[\\d]{0,11}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.find();
    }

}
