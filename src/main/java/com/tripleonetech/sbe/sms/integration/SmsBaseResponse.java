package com.tripleonetech.sbe.sms.integration;

import com.tripleonetech.sbe.common.integration.ApiResponseEnum;

public interface SmsBaseResponse {
    ApiResponseEnum getApiResponse();
    String getApiResponseMessage();
}
