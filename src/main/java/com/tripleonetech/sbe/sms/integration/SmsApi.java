package com.tripleonetech.sbe.sms.integration;

import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.ThirdPartyApi;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.sms.SmsMessage;

public interface SmsApi extends ThirdPartyApi {
    
    ApiResult sendMessage(SmsMessage smsMessage) throws RestResponseException;
    
    ApiResult getBalance();
}
