package com.tripleonetech.sbe.sms.integration.impl;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.integration.AbstractSmsApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class SmsApiMockSms extends AbstractSmsApi {
    private static final Logger logger = LoggerFactory.getLogger(SmsApiMockSms.class);
    private static final String API_URI_SEND = "http://api.mock-sms.com/send-message";
    private static final String API_URI_GET_BALANCE = "http://api.mock-sms.com/get-balance";
    private static List<ApiMetaField> META_FIELDS;

    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("account", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("apiSecret", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("apiKey", MetaFieldTypeEnum.TEXT, true));

            }
        };
    }
    
    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }
    
    @Override
    public ApiResult sendMessage(SmsMessage smsMessage) {
        
        if(smsMessage == null) {
            logger.error("Failed to send SMS - Reason:Receiver and message are empty");
            return new ApiResult(ApiResponseEnum.GENERAL_ERROR);
        }
        //Mock parameters
        Map<String, Object> paramInfo = new HashMap<>();
        paramInfo.put("mobile", smsMessage.getReceiverNumber());
        paramInfo.put("message", signContent(smsMessage.getMessage()));
        paramInfo.put("account", getConfig().get("account"));

        // Start sending SMS
            //HttpResponse response = this.postAsJsonData(this.getApiUrl(), paramInfo);
        logger.info("Send message : {} to phone number : {} ", smsMessage.getMessage(), smsMessage.getReceiverNumber());
        // .....
        
        //Mock response result
        ApiResult result = new ApiResult(ApiResponseEnum.OK);
        result.setParam("response", "smsid:2123124423532");
        return result;
    }

    @Override
    public ApiResult getBalance() {
        
        Map<String, Object> paramInfo = new HashMap<>();
        paramInfo.put("account", getConfig().get("account"));
        
        //Mock account balance
            //HttpResponse response = this.postAsJsonData(this.getApiUrl(), paramInfo);
        BigDecimal mockBalance =  BigDecimal.TEN;
        logger.info("Get current balance : {}", mockBalance);
        
        //Mock response result
        ApiResult result = new ApiResult();
        result.setResponseEnum(ApiResponseEnum.OK);
        result.setParam("response", mockBalance);
        
        return result;
    }

    @Override
    public String signContent(String content) {
        String signKey = getConfig().get("apiKey");
        String signSecret = getConfig().get("apiSecret");
        //Mock encode content ....
        return signKey + content + signSecret;
    }

    @Override
    protected String getApiUrl() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getBalanceUrl() {
        // TODO Auto-generated method stub
        return null;
    }


}
