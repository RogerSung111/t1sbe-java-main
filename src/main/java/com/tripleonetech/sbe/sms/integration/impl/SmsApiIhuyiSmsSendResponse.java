package com.tripleonetech.sbe.sms.integration.impl;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SmsApiIhuyiSmsSendResponse extends SmsApiIhuyiBaseResponse {
    private String smsid;

    public String getSmsid() {
        return smsid;
    }

    public void setSmsid(String smsid) {
        this.smsid = smsid;
    }

}
