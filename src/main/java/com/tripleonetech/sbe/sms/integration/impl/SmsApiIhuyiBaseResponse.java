package com.tripleonetech.sbe.sms.integration.impl;

import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.sms.integration.SmsBaseResponse;

public abstract class SmsApiIhuyiBaseResponse implements SmsBaseResponse{
    private final static int API_RESPONSE_CODE_SUCCESS = 2;
    private int code;
    private String msg;

    @Override
    public ApiResponseEnum getApiResponse() {
        switch (getCode()) {
            case API_RESPONSE_CODE_SUCCESS:
                return ApiResponseEnum.OK;
        }
        return ApiResponseEnum.ACTION_UNSUCCESSFUL;
    }

    @Override
    public String getApiResponseMessage() {
        return this.getMsg();
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

}
