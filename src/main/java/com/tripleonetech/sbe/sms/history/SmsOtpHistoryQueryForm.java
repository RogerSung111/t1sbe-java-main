package com.tripleonetech.sbe.sms.history;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;

public class SmsOtpHistoryQueryForm extends PageQueryForm {
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value="Begin time of creation time search")
    private LocalDateTime createdAtStart;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value="End time of creation time search")
    private LocalDateTime createdAtEnd;

    @ApiModelProperty(value="Username")
    private String username;

    @ApiModelProperty(value="Mobile phone number")
    private String phoneNumber;

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
