package com.tripleonetech.sbe.sms.history;

import java.time.LocalDateTime;

public class SmsOtpHistory {
    
    private Long id;
    private Integer smsApiId;
    private String phoneNumber;
    private String otpCode;
    private Integer playerId;
    private Boolean verified;
    private Boolean delivered;
    private String remark;
    private LocalDateTime expirationTime;
    private LocalDateTime createdAt;
    
    //Association columns
    private String username;
    private String apiCode;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getSmsApiId() {
        return smsApiId;
    }
    public void setSmsApiId(Integer smsApiId) {
        this.smsApiId = smsApiId;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getOtpCode() {
        return otpCode;
    }
    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }
    public Integer getPlayerId() {
        return playerId;
    }
    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Boolean isVerified() {
        return verified;
    }
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
    public Boolean isDelivered() {
        return delivered;
    }
    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }
    public void setExpirationTime(LocalDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getApiCode() {
        return apiCode;
    }
    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }
    public static SmsOtpHistory create(Integer apiId, String phoneNumber, String otpCode, 
            Integer playerId, String remark, LocalDateTime expirationTime, Boolean delivered) {
        
        SmsOtpHistory smsOtpHistory = new SmsOtpHistory();
        smsOtpHistory.setSmsApiId(apiId);
        smsOtpHistory.setPhoneNumber(phoneNumber);
        smsOtpHistory.setOtpCode(otpCode);
        smsOtpHistory.setPlayerId(playerId);
        smsOtpHistory.setExpirationTime(expirationTime);
        smsOtpHistory.setRemark(remark);
        smsOtpHistory.setVerified(false);
        smsOtpHistory.setDelivered(delivered);
        return smsOtpHistory;
    }
    
}
