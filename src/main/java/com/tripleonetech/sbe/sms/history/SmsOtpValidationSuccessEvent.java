package com.tripleonetech.sbe.sms.history;

import org.springframework.context.ApplicationEvent;

public class SmsOtpValidationSuccessEvent extends ApplicationEvent {
    private String otpCode;
    private Integer playerId;

    public SmsOtpValidationSuccessEvent(Object source, Integer playerId, String otpCode) {
        super(source);

        this.playerId = playerId;
        this.otpCode = otpCode;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
