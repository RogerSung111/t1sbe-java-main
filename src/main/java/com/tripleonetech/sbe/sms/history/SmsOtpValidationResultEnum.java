package com.tripleonetech.sbe.sms.history;

public enum SmsOtpValidationResultEnum {
    
    PASS(true, "pass"), NOT_PASS_OTP_EXPIRED(false, "OTP code is expired"), NOT_PASS_OTP_WRONG(false, "OTP Code is wrong");
    
    private SmsOtpValidationResultEnum(Boolean pass, String message) {
        this.pass = pass;
        this.message = message;
    }
    
    private boolean pass;
    private String message;
    
    public boolean isPass() {
        return pass;
    }
    public void setPass(boolean pass) {
        this.pass = pass;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
 
}
