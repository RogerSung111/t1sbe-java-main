package com.tripleonetech.sbe.sms.history;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmsOtpHistoryService {

    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;
    
    public void saveSmsOtpHistory(Integer apiId, String phoneNumber, String otpCode, 
            Integer playerId, String remark, LocalDateTime expirationTime, Boolean delivered) {
        
        SmsOtpHistory smsOtpHistory = SmsOtpHistory.create(apiId, phoneNumber, otpCode, 
                playerId, remark, expirationTime, delivered);
        smsOtpHistoryMapper.insert(smsOtpHistory);
        
    }
}
