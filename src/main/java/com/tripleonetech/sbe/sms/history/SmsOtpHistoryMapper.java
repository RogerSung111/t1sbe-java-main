package com.tripleonetech.sbe.sms.history;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SmsOtpHistoryMapper {
    
    void insert(SmsOtpHistory smsOtpHistory);

    int updateRecordToVerified(Long id);

    SmsOtpHistory get(Long id);

    SmsOtpHistory getByPlayerIdAndPhone(@Param("playerId") Integer playerId, @Param("phoneNumber") String phoneNumber);
    
    List<SmsOtpHistoryView> listOtpHistoryByCriteria(@Param("queryForm")SmsOtpHistoryQueryForm form,
            @Param("pageNum") Integer pageNum, 
            @Param("pageSize") Integer pageSize);    
}
