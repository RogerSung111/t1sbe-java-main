package com.tripleonetech.sbe.sms.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.AlphanumericGenUtil;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.sms.SmsMessage;
import com.tripleonetech.sbe.sms.history.SmsOtpHistory;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryMapper;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryService;
import com.tripleonetech.sbe.sms.history.SmsOtpValidationResultEnum;
import com.tripleonetech.sbe.sms.integration.SmsApi;
import com.tripleonetech.sbe.sms.integration.SmsApiFactory;

@Service
public class SmsService {
    private final static Logger logger = LoggerFactory.getLogger(SmsService.class);
    private final static int OTP_CODE_LENGTH = 6;
    private final static int EXPIRY_TIME_IN_SECOND = 600;

    private SmsApiFactory apiFactory;

    @Autowired
    private SmsOtpHistoryService smsOtpHistoryService;

    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;

    @Autowired
    public void setApiFactory(SmsApiFactory apiFactory) {
        this.apiFactory = apiFactory;
    }

    public boolean sendGeneralSms(String phoneNumber, String message) throws RestResponseException {
        SmsApi api = apiFactory.getEnabledSmsApi();
        ApiResult apiResult = api.sendMessage(SmsMessage.createSmsMessage(phoneNumber, message));
        if (apiResult.getResponseEnum() != ApiResponseEnum.OK) {
            logger.error("Failed to send SMS, failure message: {}", apiResult.getMessage());
            return false;
        }
        return true;
    }

    public BigDecimal getSmsBalance() {
        SmsApi api = apiFactory.getEnabledSmsApi();
        ApiResult result = api.getBalance();
        return (BigDecimal) result.getParam("response");
    }

    public String sendOtpSms(String phoneNumber, Integer playerId) throws RestResponseException {
        String otpCode = AlphanumericGenUtil.generateRandomNumberCode(OTP_CODE_LENGTH);
        SmsApi api = apiFactory.getEnabledSmsApi();
        ApiResult result = api
                .sendMessage(SmsMessage.createSmsMessage(phoneNumber, String.format("This is your OTP: %s.", otpCode)));

        // store sms otp log
        boolean isDelivered = result.getResponseEnum() == ApiResponseEnum.OK;
        String remark = null;
        if (result.getParam("response") instanceof String) {
            remark = (String) result.getParam("response");
        } else {
            remark = JsonUtils.objectToJson(result.getParam("response"));
        }
        smsOtpHistoryService.saveSmsOtpHistory(api.getId(), phoneNumber, otpCode,
                playerId, remark, LocalDateTime.now().plusSeconds(EXPIRY_TIME_IN_SECOND), isDelivered);

        if (result.getResponseEnum() != ApiResponseEnum.OK) {
            return null;
        }
        return otpCode;
    }

    public SmsOtpValidationResultEnum validateOtpCode(Integer playerId, String otpCode, String phoneNumber) {
        SmsOtpHistory smsOtpHistory = smsOtpHistoryMapper.getByPlayerIdAndPhone(playerId, phoneNumber);
        if (smsOtpHistory == null || !otpCode.equals(smsOtpHistory.getOtpCode()) || smsOtpHistory.isVerified()) {
            return SmsOtpValidationResultEnum.NOT_PASS_OTP_WRONG;
        }
        if (LocalDateTime.now().compareTo(smsOtpHistory.getExpirationTime()) > 0) {
            return SmsOtpValidationResultEnum.NOT_PASS_OTP_EXPIRED;
        }

        smsOtpHistoryMapper.updateRecordToVerified(smsOtpHistory.getId());
        return SmsOtpValidationResultEnum.PASS;
    }

}
