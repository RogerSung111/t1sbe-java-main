package com.tripleonetech.sbe.config;

import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
@ConfigurationProperties(prefix = "config.slf4jfilter")
public class Slf4jMDCFilterConfiguration {

    public static final String UUID_TOKEN_HEADER = "X-Request-ID";
    public static final String DEFAULT_MDC_UUID_TOKEN_KEY = "Slf4jMDCFilter.UUID";

    private String mdcTokenKey = DEFAULT_MDC_UUID_TOKEN_KEY;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Bean
    public FilterRegistrationBean servletRegistrationBean() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        final Slf4jMDCFilter log4jMDCFilterFilter = new Slf4jMDCFilter(UUID_TOKEN_HEADER, mdcTokenKey);
        registrationBean.setFilter(log4jMDCFilterFilter);
        registrationBean.setOrder(2);
        return registrationBean;
    }

    public static class Slf4jMDCFilter extends OncePerRequestFilter {

        private final String headerKey;
        private final String mdcTokenKey;

        public Slf4jMDCFilter(String headerKey, String mdcTokenKey) {
            super();
            this.headerKey = headerKey;
            this.mdcTokenKey = mdcTokenKey;
        }

        @Override
        protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                        final FilterChain chain)
                throws java.io.IOException, ServletException {
            try {
                final String token;
                if (StringUtils.isNotEmpty(headerKey) && StringUtils.isNotEmpty(request.getHeader(headerKey))) {
                    token = StringUtils.substring(request.getHeader(headerKey), 0, 36);
                } else {
                    token = UUID.randomUUID().toString().toUpperCase();
                }
                MDC.put(mdcTokenKey, token);
                if (StringUtils.isNotEmpty(headerKey)) {
                    response.addHeader(headerKey, token);
                }
                insertIntoMDC(request);

                chain.doFilter(request, response);
            } finally {
                MDC.remove(mdcTokenKey);
                clearMDC();
            }
        }

        void insertIntoMDC(ServletRequest request) {
            if (request instanceof HttpServletRequest) {
                HttpServletRequest httpServletRequest = (HttpServletRequest) request;
                MDC.put("req.requestURI", httpServletRequest.getRequestURI());
                MDC.put("req.method", httpServletRequest.getMethod());
                MDC.put("req.queryString", httpServletRequest.getQueryString());
            }

        }

        void clearMDC() {
            MDC.remove("req.requestURI");
            MDC.remove("req.queryString");
            MDC.remove("req.method");
        }
    }
}
