package com.tripleonetech.sbe.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
@Profile("main")
public class ResourceServerConfigProvider extends ResourceServerConfigurerAdapter {
    @Autowired
    private UserDetailsService t1sbeUserDetailsService;

    public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(t1sbeUserDetailsService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .anonymous() // allow anonymous access
                .and()
                .authorizeRequests()
                // allow access without login
                .antMatchers(
                        // static resources
                        "/**/*.html", "/**/*.css", "/**/*.js", "/img/**", "/**/favicon.ico",
                        // required by swagger -
                        // https://github.com/springfox/springfox/issues/1996#issuecomment-335155187
                        "/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**",
                        // other URLs that does not require authentication
                        "/game-log-sync-tasks/**","/**/*callback/**","/player-level-calc/**",
                        "/site-config/**",
                        "/resources/**",
                        "/test/**"
                        )
                .permitAll()
                .antMatchers(HttpMethod.GET, "/images/**").permitAll()
                // all other access require authentication
                .antMatchers("/**").authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .csrf().disable();
    }

}
