package com.tripleonetech.sbe.config;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.iprule.IpRuleMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@Profile("main")
public class ResourceInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private IpRuleMapper ipRuleMapper;

    private static final Logger logger = LoggerFactory.getLogger(ResourceInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        if(Constant.getInstance().getBackend().isFirewallOff()) {
            logger.debug("Firewall is off");
            return true;
        }
        String requestIP = ClientHttpInfoUtils.getIp();

        // Check current IP against backoffice whitelist IPs
        List<String> authorizedIp = ipRuleMapper.listBackOfficeWhitelist();
        logger.debug("Current IP is [{}]", requestIP);
        Boolean authorized;

        logger.debug("Backoffice whitelist IPs [{}]", authorizedIp.toString());

        for (String ip : authorizedIp) {
            try {
                IpAddressMatcher ipAddressMatcher = new IpAddressMatcher(ip);
                authorized = ipAddressMatcher.matches(requestIP);
                if (authorized) {
                    logger.debug("IP Authorized!");
                    return true;
                }
            }catch(IllegalArgumentException e) {
                logger.warn("Exception parsing IP, skipping it: [{}] [{}]", ip, e.getMessage());
            }
        }
        logger.error("This IP has been restricted [{}]", requestIP);
        String res = JsonUtils.objectToJson(RestResponse.ipRestricted(requestIP));

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().print(res);
        return false;
    }
}
