package com.tripleonetech.sbe.config;

import com.google.common.base.Predicates;
import com.tripleonetech.sbe.common.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableSwagger2
@Profile("main")
public class SwaggerConfig {
    @Autowired
    private Constant constant;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tripleonetech.sbe.web"))
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .paths(Predicates.not(PathSelectors.regex("/whitelabel.*")))
                .paths(PathSelectors.any())
                .build()
                .tags(new Tag("z-Deprecated", "APIs that are deprecated or need refactoring are temporarily grouped here"), tags())
                .securitySchemes(Arrays.asList(securitySchema()))
                .securityContexts(Arrays.asList(securityContext()));
    }

    private Tag[] tags() {
        return new Tag[] {
                new Tag("API Configuration - Game", "Game API configurations"),
                new Tag("API Configuration - Payment", "Payment API configurations"),
                new Tag("API Configuration - Payment", "Payment API configurations"),
                new Tag("Automation", "Automation jobs and execution records"),
                new Tag("CMS & Templates", "Content Management Service and front-end templates"),
                new Tag("Dashboard", "Summary of site data to display on dashboard page"),
                new Tag("Deposits & Withdrawals", "Manage deposit and withdrawal requests"),
                new Tag("Deposit & Withdraw Setting", "Configure payment methods, withdraw request approval workflow"),
                new Tag("Game Log Sync", "Game log sync tasks and status"),
                new Tag("Game Management", "Manage games and game types"),
                new Tag("Message & Announcement", "Site announcements, in-site messages"),
                new Tag("Operator Management", "Manage users of site backend (i.e. operators) and their roles and permissions"),
                new Tag("Player Group & Tags", "Manage player group and player tags"),
                new Tag("Player Management", "Manage player accounts info that's not related to wallet"),
                new Tag("Player Wallet Management", "Manage player wallets and withdraw conditions"),
                new Tag("Promotion Bonus", "Manage bonus records received from promotions"),
                new Tag("Promotion - Campaign", "Manage campaigns"),
                new Tag("Promotion - Cashback", "Cashback settings"),
                new Tag("Reports", "Various reports"),
                new Tag("Resources", "Image uploads"),
                new Tag("Site Info", "Site information, usually loaded on page load. Does not require authentication."),
                new Tag("Site Properties", "Site properties allow operator to set their values")
        };
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("T1SBE Operator Backend API")
                .description(String.format("T1SBE Operator Backend API, last updated: [%s]", constant.getSwagger().getLastUpdated()))
                .version(constant.getSwagger().getBranch())
                .build();
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .clientId(constant.getOauth().getClient())
                .clientSecret(constant.getOauth().getSecret())
                .scopeSeparator(",")
                .build();
    }

    private OAuth securitySchema() {
        List<AuthorizationScope> authorizationScopeList = new ArrayList<>();
        //authorizationScopeList.add(new AuthorizationScope("read", "Read scope"));
        //authorizationScopeList.add(new AuthorizationScope("write", "Write scope"));

        List<GrantType> grantTypes = new ArrayList<>();
        grantTypes.add(new ResourceOwnerPasswordCredentialsGrant("/oauth/token"));

        OAuth oAuth = new OAuth("oauth2", authorizationScopeList, grantTypes);
        return oAuth;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth())
                .forPaths(PathSelectors.any()).build();
    }

    private List<SecurityReference> defaultAuth() {
        List<AuthorizationScope> authorizationScopeList = new ArrayList<>();
        authorizationScopeList.add(new AuthorizationScope("read", "Read scope"));
        authorizationScopeList.add(new AuthorizationScope("write", "Write scope"));
        return Arrays
                .asList(new SecurityReference("oauth2", authorizationScopeList.toArray(new AuthorizationScope[0])));
    }
}
