package com.tripleonetech.sbe.config;

import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.operator.OperatorDetails;
import com.tripleonetech.sbe.operator.OperatorRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
// custom permission evaluator to achieve operator permission
public class OperatorPermissionEvaluator implements PermissionEvaluator {
    private static final Logger logger = LoggerFactory.getLogger(OperatorPermissionEvaluator.class);

    @Override
    public boolean hasPermission(
            Authentication auth, Object targetDomainObject, Object permission) {
        if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)){
            logger.error("Wrong arguments in hasPermission call. auth:[{}], targetDomainObject:[{}], permission:[{}]",
                    auth, targetDomainObject, permission);
            return false;
        }
        String targetType = targetDomainObject.getClass().getSimpleName().toUpperCase();

        return canAccess(auth, targetType, permission.toString().toUpperCase());
    }

    @Override
    public boolean hasPermission(
            Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            logger.error("Wrong arguments in hasPermission call. auth:[{}], targetId:[{}], targetType:[{}], permission:[{}]",
                    auth, targetId, targetType, permission);
            return false;
        }
        return canAccess(auth, targetType.toUpperCase(),
                permission.toString().toUpperCase());
    }

    private boolean canAccess(Authentication auth, String targetType, String permissionExpression) {
        OperatorDetails operatorDetail = (OperatorDetails) auth.getPrincipal();
        Operator loginOperator = operatorDetail.getOperator();
        OperatorRole loginOperatorRole = loginOperator.getRole();
        logger.debug("Evaluating privilege: [{}], [{}], [{}], against permissions: [{}]", auth.getName(), targetType, permissionExpression, loginOperatorRole.getPermission());
        return loginOperatorRole.getOperatorPermission().hasPermission(permissionExpression);
    }
}