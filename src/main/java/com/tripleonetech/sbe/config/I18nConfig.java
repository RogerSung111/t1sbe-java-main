package com.tripleonetech.sbe.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Configuration
public class I18nConfig extends AcceptHeaderLocaleResolver {
    public static final Locale DEFAULT_LOCALE = new Locale("en-US");
    private List<Locale> LOCALES = Arrays.asList(
            DEFAULT_LOCALE,
            new Locale("zh-CN"),
            new Locale("id"),
            new Locale("th"),
            new Locale("ko"),
            new Locale("vi"));

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String headerLang = request.getHeader("Accept-Language");
        Locale resolvedLocale = headerLang == null || headerLang.isEmpty()
                ? DEFAULT_LOCALE
                : Locale.lookup(Locale.LanguageRange.parse(headerLang), LOCALES);
        return resolvedLocale;
    }
}