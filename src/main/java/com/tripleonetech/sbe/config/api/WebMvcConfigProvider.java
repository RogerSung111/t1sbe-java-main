package com.tripleonetech.sbe.config.api;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.converter.Converter;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Profile("api")
public class WebMvcConfigProvider implements WebMvcConfigurer {
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/", "classpath:/resources/",
            "classpath:/static/", "classpath:/public/" };

    // This will not work with OAuth enabled as OAuth filter goes first and reject request
    // See SimpleCORSFilter.java for solution
    // @Override
    // public void addCorsMappings(CorsRegistry registry) {
    // // Enables CORS from all origins
    // registry.addMapping("/**");
    // }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
    }

    @Bean
    public Converter<String, ZonedDateTime> zonedDateTimeConverter() {
        return new Converter<String, ZonedDateTime>() {
            @Override
            public ZonedDateTime convert(String source) {
                return ZonedDateTime.parse(source);
            }
        };
    }

    @Bean
    public Converter<String, LocalDateTime> localDateTimeConverter() {
        return new Converter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String source) {
                return LocalDateTime.parse(source);
            }
        };
    }

    // for spring-mobile-device
    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
        return new DeviceResolverHandlerInterceptor();
    }

    @Bean
    public com.tripleonetech.sbe.config.api.ResourceInterceptor ResourceInterceptor() {
        return new ResourceInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(deviceResolverHandlerInterceptor());
        registry.addInterceptor(ResourceInterceptor());
    }
}
