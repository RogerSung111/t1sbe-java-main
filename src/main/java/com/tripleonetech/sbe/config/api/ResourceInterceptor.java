package com.tripleonetech.sbe.config.api;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.iprule.IpRuleMapper;
import com.tripleonetech.sbe.iprule.country.GeoIpService;
import com.tripleonetech.sbe.iprule.country.IpCountryRuleCache;
import com.tripleonetech.sbe.iprule.country.IpRuleCountryMapper;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@Profile("api")
public class ResourceInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private IpRuleMapper ipRuleMapper;
    @Autowired
    private GeoIpService geoIpService;
    @Autowired
    private IpRuleCountryMapper ipRuleCountryMapper;

    private static final Logger logger = LoggerFactory
            .getLogger(com.tripleonetech.sbe.config.ResourceInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        if(Constant.getInstance().getFrontend().isFirewallOff()) {
            logger.debug("Firewall is off");
            return true;
        }

        String requestIP = ClientHttpInfoUtils.getIp();
        logger.debug("Current ip is [{}]", requestIP);

        // -- Country whitelist --
        // Judge country code exists in white list
        if (!IpCountryRuleCache.hasVisited(requestIP)) {
            String countryCode = geoIpService.getCountryCode(requestIP);
            if(countryCode == null){
                logger.error("Cannot determine country from ip [{}]", requestIP);
                writeIpRestricted(response, requestIP);
                return false;
            }
            IpCountryRuleCache.setCountry(requestIP, countryCode);
        }
        String countryCode = IpCountryRuleCache.getCountry(requestIP);
        logger.debug("IP locale is [{}]", countryCode);
        // Ignore country code judgment while it is null
        Boolean approved = ipRuleCountryMapper.getEnabled(countryCode);
        if (BooleanUtils.isNotTrue(approved)) {
            logger.debug("the countryCode is [{}], and it is not approved now", countryCode);
            writeIpRestricted(response, requestIP);
            return false;
        }

        // -- IP Blacklist --
        // Judge ip in black list
        List<String> authorizedIp = ipRuleMapper.listFrontOfficeBlacklist();
        Boolean blackListed;

        logger.debug("Blacklisted IPs: " + authorizedIp.toString());

        for (String ip : authorizedIp) {
            IpAddressMatcher ipAddressMatcher = new IpAddressMatcher(ip);
            blackListed = ipAddressMatcher.matches(requestIP);
            if (blackListed) {
                logger.error("This IP has been blacklisted: " + requestIP);
                writeIpRestricted(response, requestIP);
                return false;
            }
        }
        logger.debug("IP Authorized!");
        return true;
    }

    private void writeIpRestricted(HttpServletResponse response, String requestIP) throws IOException {
        String res = JsonUtils.objectToJson(RestResponse.ipRestricted(requestIP));
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().print(res);
    }
}
