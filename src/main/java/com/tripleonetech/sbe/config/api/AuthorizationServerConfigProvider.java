package com.tripleonetech.sbe.config.api;

import com.tripleonetech.sbe.common.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@Profile("api")
@EnableAuthorizationServer
public class AuthorizationServerConfigProvider extends AuthorizationServerConfigurerAdapter {
    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private Constant constant;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    @Qualifier("playerUserDetailsService")
    private UserDetailsService playerUserDetailsService;

    @Autowired
    private ResourceInterceptor resourceInterceptor;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // Defines the clients (API ID) accessing the authorization service
        clients.inMemory()
                .withClient(constant.getOauthPlayer().getClient())
                .secret(constant.getOauthPlayer().getSecret())
                .authorizedGrantTypes("password", "refresh_token")
                .scopes("read", "write")
                .accessTokenValiditySeconds(constant.getOauth().getAccessTokenValidMinutes() * 60)
                .refreshTokenValiditySeconds(constant.getOauth().getRefreshTokenValidMinutes() * 60);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // Used by password grant type, authenticates against player db
        endpoints.tokenStore(tokenStore)
                .addInterceptor(resourceInterceptor)
                .authenticationManager(authenticationManager)
                .userDetailsService(playerUserDetailsService)
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
                // use a new refresh token on each token refresh, invalidates old one
                .reuseRefreshTokens(false);
    }
}
