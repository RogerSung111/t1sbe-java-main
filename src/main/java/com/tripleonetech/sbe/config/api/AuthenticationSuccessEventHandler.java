package com.tripleonetech.sbe.config.api;

import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerService;
import com.tripleonetech.sbe.player.PlayerUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Component
@Profile("api")
public class AuthenticationSuccessEventHandler implements ApplicationListener<AuthenticationSuccessEvent> {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessEventHandler.class);

    @Autowired
    private PlayerService playerService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        boolean isUsernamePasswordAuth = event.getSource() instanceof UsernamePasswordAuthenticationToken;
        if(!isUsernamePasswordAuth) {
            // Do nothing if it's not a username-password authentication
            return;
        }
        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) event.getSource();

        boolean isPlayerAuth = auth.getPrincipal() instanceof PlayerUserDetails;
        if(!isPlayerAuth) {
            // Do nothing if it's not a player authentication
            return;
        }

        PlayerUserDetails playerUserDetails = (PlayerUserDetails) auth.getPrincipal();
        Player player = playerUserDetails.getPlayer();
        logger.info("Authentication successful for player [{}] @ [{}]", player.getUsername(), player.getCurrency());
        playerService.recordLogin(player);
    }
}
