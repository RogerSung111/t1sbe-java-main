package com.tripleonetech.sbe.config.api;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Profile("api")
public class AuthenticationFailureBadCredentialEventListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFailureBadCredentialEventListener.class);

    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerMapper playerMapper;

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        boolean isUsernamePasswordAuth = event.getSource() instanceof UsernamePasswordAuthenticationToken;
        if(!isUsernamePasswordAuth) {
            // Do nothing if it's not a username-password authentication
            return;
        }
        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) event.getSource();

        String playerUsername = (String) auth.getPrincipal();
        String currency = (String) ((Map) auth.getDetails()).get("currency");
        if(StringUtils.isEmpty(playerUsername) || StringUtils.isEmpty(currency)) {
            logger.warn("Failed login attempt for username [{}] and currency [{}]", playerUsername, currency);
            // Do nothing for login attempt that would not map to a user account
            return;
        }

        // Retrieve player account (again) for recording purpose
        Player player = playerMapper.getByUniqueKey(playerUsername, SiteCurrencyEnum.valueOf(currency));
        if (player == null) {
            logger.warn("Player does not exist for username [{}] and currency [{}]", playerUsername, currency);
            return;
        }
        logger.info("Authentication failed for player [{}] @ [{}]", player.getUsername(), player.getCurrency());
        playerService.recordFailedLogin(player);
    }
}
