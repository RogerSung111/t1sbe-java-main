package com.tripleonetech.sbe.config.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.web.RestResponse;

@SuppressWarnings("deprecation")
@Configuration
@Profile("api")
@EnableResourceServer
public class ResourceServerConfigProvider extends ResourceServerConfigurerAdapter {
    @Autowired
    private UserDetailsService playerUserDetailsService;

    @Autowired
    public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(playerUserDetailsService);
    }

    // Use noop password encoder, as we are managing password encoding ourselves
    @Bean
    public PasswordEncoder passwordEncoder() {
        String idForEncode = "noop";
        Map<String, PasswordEncoder> encoders = new HashMap<String, PasswordEncoder>();
        encoders.put("noop", NoOpPasswordEncoder.getInstance());
        DelegatingPasswordEncoder delegatingEncoders = new DelegatingPasswordEncoder(idForEncode, encoders);
        delegatingEncoders.setDefaultPasswordEncoderForMatches(NoOpPasswordEncoder.getInstance());
        return delegatingEncoders;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .anonymous() // allow anonymous access
                .and()
                .authorizeRequests()
                // allow access before login
                .antMatchers(
                        // permit all access to static resources
                        "/**/*.html", "/**/*.css", "/**/*.js", "/img/**", "/**/favicon.ico",
                        "/resources/**",
                        // required by swagger -
                        // https://github.com/springfox/springfox/issues/1996#issuecomment-335155187
                        "/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**",
                        // player registration, forget password
                        "/player/register", "/player/verification-question", "/player/forget-password",
                        "/games/**", "/site-config/**", "/site-properties/**", "/announcements", "/announcements/**",
                        "/**callback/**")
                .permitAll()
                // permit all access to api (need authentication)
                .antMatchers("/","/player/password").authenticated()
                .antMatchers(HttpMethod.GET, "/player/info").authenticated()
                // check other access for player status
                .anyRequest().not().hasAnyAuthority("ROLE_ANONYMOUS", "PASSWORD_PENDING")
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .csrf().disable();
    }

    @Autowired
    private DefaultWebSecurityExpressionHandler expressionHandler;

    @Bean
    public DefaultWebSecurityExpressionHandler oAuth2WebSecurityExpressionHandler(
            ApplicationContext applicationContext) {
        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
        expressionHandler.setApplicationContext(applicationContext);
        return expressionHandler;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.expressionHandler(expressionHandler);
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new AccessDeniedHandler() {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response,
                    AccessDeniedException accessDeniedException) throws IOException, ServletException {
                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                out.print(JsonUtils.objectToJson(RestResponse.accessDenied()));
                out.flush();
            }
        };
    }
}
