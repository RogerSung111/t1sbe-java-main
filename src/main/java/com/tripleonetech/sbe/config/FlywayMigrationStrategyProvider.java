package com.tripleonetech.sbe.config;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class FlywayMigrationStrategyProvider {
    private static final Logger logger = LoggerFactory.getLogger(FlywayMigrationStrategyProvider.class);

    @Value("${spring.datasource.url}")
    private String dataSourceUrl;

    // Force rebuild DB when running on test profile
    @Bean
    @Profile("test")
    public FlywayMigrationStrategy cleanMigrateStrategy() {
        FlywayMigrationStrategy strategy = new FlywayMigrationStrategy() {
            @Override
            public void migrate(Flyway flyway) {
                logger.warn("Running on *test* profile, rebuild DB [{}]",
                        dataSourceUrl);
                flyway.clean();
                flyway.migrate();
            }
        };
        return strategy;
    }
}
