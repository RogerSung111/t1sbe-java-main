package com.tripleonetech.sbe.config;

import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;

public class T1SBEAuthenticationKeyGenerator extends DefaultAuthenticationKeyGenerator {

    private static final String TIMESTAMP = "timestamp";

    @Override
    protected String generateKey(Map<String, String> values) {
        values.put(TIMESTAMP, LocalDateTime.now().toString());
        return super.generateKey(values);
    }

}
