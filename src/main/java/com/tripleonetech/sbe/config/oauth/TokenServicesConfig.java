package com.tripleonetech.sbe.config.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
public class TokenServicesConfig {

    @Autowired
    private TokenStore tokenStore;

    /**
     * For com.tripleonetech.sbe.oauth.OauthService
     * 
     * @return
     */
    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore);
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setAccessTokenValiditySeconds(1800);
        defaultTokenServices.setRefreshTokenValiditySeconds(1800);
        defaultTokenServices.setReuseRefreshToken(false);
        return defaultTokenServices;
    }

}
