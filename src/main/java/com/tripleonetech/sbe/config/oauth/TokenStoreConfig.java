package com.tripleonetech.sbe.config.oauth;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import com.tripleonetech.sbe.config.T1SBEAuthenticationKeyGenerator;

@Configuration
public class TokenStoreConfig {

    @Autowired
    DataSource ds;

    @Bean
    public TokenStore tokenStore() {
        JdbcTokenStore tokenStore = new JdbcTokenStore(ds);
        tokenStore.setAuthenticationKeyGenerator(new T1SBEAuthenticationKeyGenerator());
        tokenStore.setSelectAccessTokenAuthenticationSql(
                "select token_id, authentication from oauth_access_token where token_id = ?");
        return tokenStore;
    }

}
