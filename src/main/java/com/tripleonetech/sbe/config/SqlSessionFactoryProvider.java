package com.tripleonetech.sbe.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.tripleonetech.sbe.common.typehandlers.BaseCodeEnumTypeHandler;

@Configuration
public class SqlSessionFactoryProvider {
    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource);
        sqlSessionFactory.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
        sqlSessionFactory.setTypeHandlersPackage(BaseCodeEnumTypeHandler.class.getPackage().getName());
        return (SqlSessionFactory) sqlSessionFactory.getObject();
    }
}
