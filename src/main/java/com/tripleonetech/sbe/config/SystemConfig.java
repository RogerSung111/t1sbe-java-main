package com.tripleonetech.sbe.config;

import java.util.Arrays;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.email.EmailHelper;

@Configuration
// This class provides a place to set system settings
public class SystemConfig {
    private static final Logger logger = LoggerFactory.getLogger(SystemConfig.class);

    @Value("${constant.email.enabled-email-helper}")
    private String enabledEmailHelperName;

    @PostConstruct
    public void doConfig() {
        logger.info("Setting application timezone to UTC");
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Bean("constant")
    @ConfigurationProperties(prefix = "constant")
    public Constant getConstant() {
        logger.info("Variables not valued in Constant class:{}", StringUtils.join(Arrays.asList(
                "validation.password-regex",
                "promo.realtime-schedule",
                "promo.daily-schedule",
                "publisher.game-log-delay-milliseconds",
                "publisher.heartbeat-delay-milliseconds",
                "game.*", ", ")));
        return Constant.getInstance();
    }

    @Bean
    public EmailHelper emailHelper(BeanFactory beanFactory) {
        logger.info("The enabled email helper is {}", enabledEmailHelperName);
        return (EmailHelper) beanFactory.getBean(enabledEmailHelperName);
    }
}
