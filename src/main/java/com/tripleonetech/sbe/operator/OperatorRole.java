package com.tripleonetech.sbe.operator;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OperatorRole implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7421197041882633355L;
    private int id;
    private String name;
    private String permission;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    @JsonIgnore
    private OperatorPermission operatorPermission;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OperatorPermission getOperatorPermission() {
        if(this.operatorPermission == null && getPermission() != null){
            this.operatorPermission = new OperatorPermission();
            this.operatorPermission.setValue(getPermission());
        }
        return operatorPermission;
    }

    // transitive call to operatorPermission's hasPermission method
    public boolean hasPermission(int moduleIndex, int permissionIndex, int action) {
        if (getOperatorPermission() == null){
            return false;
        }
        return getOperatorPermission().hasPermission(moduleIndex, permissionIndex, action);
    }

    public static OperatorRole from(OperatorRoleForm formObject) {
        OperatorRole role = new OperatorRole();
        BeanUtils.copyProperties(formObject, role);
        return role;
    }
}
