package com.tripleonetech.sbe.operator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class Operator implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2573043471804449703L;
    private Integer id;
    private String username;
    private String passwordHash;
    private OperatorStatusEnum statusEnum = OperatorStatusEnum.DISABLED;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private OperatorRole role;
    private Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public boolean validatePassword(String password) {
        return BCrypt.checkpw(password, passwordHash);
    }

    public OperatorStatusEnum getStatusEnum() {
        return statusEnum;
    }

    public void setStatusEnum(OperatorStatusEnum statusEnum) {
        this.statusEnum = statusEnum;
    }

    public int getStatus() {
        return getStatusEnum().getCode();
    }

    public void setStatus(int status) {
        this.statusEnum =
                Arrays.stream(OperatorStatusEnum.values()).filter(e -> e.getCode() == status)
                        .findFirst().orElse(OperatorStatusEnum.DISABLED);

    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OperatorRole getRole() {
        return role;
    }

    public void setRole(OperatorRole role) {
        this.role = role;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
