package com.tripleonetech.sbe.operator;

import java.time.LocalDateTime;

public class OperatorOauth {
    private Integer id;
    private Integer operatorId;
    private String accessToken;
    private String refreshToken;
    private LocalDateTime nextRefreshTime;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getOperatorId() {
        return operatorId;
    }
    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }
    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getRefreshToken() {
        return refreshToken;
    }
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
    
    public LocalDateTime getNextRefreshTime() {
        return nextRefreshTime;
    }
    public void setNextRefreshTime(LocalDateTime nextRefreshTime) {
        this.nextRefreshTime = nextRefreshTime;
    }
    public LocalDateTime getCreated_at() {
        return created_at;
    }
    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }
    public LocalDateTime getUpdated_at() {
        return updated_at;
    }
    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }
    
    
}
