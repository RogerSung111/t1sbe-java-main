package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.common.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OperatorPermission {
    private static final Logger logger = LoggerFactory.getLogger(OperatorPermission.class);

    private String value;
    // parsed from the value string above, indexed by moduleIndex then permissionIndex
    // and holds an int value. We can use bit-wise AND to determine READ/WRITE/DELETE permission.
    private List<List<Integer>> values;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.parseValue();
    }

    public List<List<Integer>> getValues() {
        return this.values;
    }

    private void parseValue() {
        this.values = new ArrayList<>();
        List<String> arr = JsonUtils.jsonToObjectList(this.value, String.class);

        arr.forEach(e -> {
            List<Integer> v = new ArrayList<>();
            // convert char '1' to integer 1 assuming no exception
            e.chars().forEach(c -> v.add(c - '0'));
            this.values.add(v);
        });
    }

    /**
     * Determines whether the permission defined by the given expression is granted
     * @param expression Must be of the format 'modelIndex,permissionIndex,operation'. All values are integer.
     * @return Whether this permission is granted
     */
    public boolean hasPermission(String expression) {
        try{
            String[] expressions = expression.split(",");
            return hasPermission(
                    Integer.parseInt(expressions[0]),
                    Integer.parseInt(expressions[1]),
                    Integer.parseInt(expressions[2])
            );
        } catch(NullPointerException | IndexOutOfBoundsException | NumberFormatException ex) {
            logger.warn("Exception parsing permission expression: [{}], [{}]", expression, ex.getMessage());
        }
        return false;
    }

    public boolean hasPermission(int moduleIndex, int permissionIndex, int operation) {
        try {
            return (getValues().get(moduleIndex).get(permissionIndex) & operation) > 0;
        } catch(IndexOutOfBoundsException ex){
            logger.warn("IndexOutOfBounds when evaluating permission [{}][{}][{}]", moduleIndex, permissionIndex, operation);
            return false;
        }
    }


    // Module and permission index values
    public static final int PLAYER = 0;
    public static final int PLAYER_LIST = 0;
    public static final int PLAYER_ACCOUNT = 1;

    public static final int OPERATOR = 1;
    public static final int OPERATOR_LIST = 0;
    public static final int OPERATOR_ACCOUNT = 1;

    // permission values are defined as: 001 = read, 002 = write, 004 = delete
    public static final int READ = 1, WRITE = 2, DELETE = 4;

}
