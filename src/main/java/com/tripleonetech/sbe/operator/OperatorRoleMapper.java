package com.tripleonetech.sbe.operator;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OperatorRoleMapper {
    List<OperatorRole> list();

    OperatorRole get(int id);

    int insert(OperatorRole role);

    int update(OperatorRole role);

    int delete(int id);
}
