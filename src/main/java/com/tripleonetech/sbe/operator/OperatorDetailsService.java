package com.tripleonetech.sbe.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class OperatorDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private OperatorMapper operatorMapper;

    @Override
    public OperatorDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Operator operator = operatorMapper.getByUsername(username);
        if (operator == null) {
            // We do not want to give out the information whether username exists in our DB
            throw new UsernameNotFoundException("Wrong password or username not found");
        }

        return new OperatorDetails(operator);
    }

}
