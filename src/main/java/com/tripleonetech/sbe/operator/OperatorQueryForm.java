package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;

public class OperatorQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Operator username")
    private String username;
    @ApiModelProperty(value = "Status: DISABLED(0), ENABLED(1). (Other status to be added in future)", allowableValues = "0,1")
    private Integer status;
    @ApiModelProperty(value = "ID of operator's role")
    private Integer roleId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
