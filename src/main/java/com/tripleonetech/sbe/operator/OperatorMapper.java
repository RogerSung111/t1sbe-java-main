package com.tripleonetech.sbe.operator;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OperatorMapper {

    Operator get(int userId);

    Operator getByUsername(String username);

    List<Operator> list(@Param("query") OperatorQueryForm query,
                        @Param("pageNum") Integer pageNum,
                        @Param("pageSize") Integer pageSize);

    int insert(Operator newUser);

    int update(Operator updateUser);

    int updatePassword(@Param("id") int id, @Param("passwordHash") String passwordHash);

    int updateStatus(@Param("id") int id, @Param("status") Integer status);

    int updateRole(@Param("id") int id, @Param("roleId") Integer roleId);

    Boolean userExists(String username);
}
