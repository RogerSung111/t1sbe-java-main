package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.common.DateUtils;
import org.springframework.beans.BeanUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class OperatorView {
    private Integer id;
    private String username;
    private Integer status;
    private OperatorRole role;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public OperatorRole getRole() {
        return role;
    }

    public void setRole(OperatorRole role) {
        this.role = role;
    }

    public static OperatorView from (Operator operator){
        OperatorView view = new OperatorView();
        BeanUtils.copyProperties(operator, view);
        return view;
    }
}
