package com.tripleonetech.sbe.operator;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.validator.PropertiesPattern;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.AssertTrue;

public class OperatorForm {

    @ApiModelProperty(value = "OldPassword")
    private String oldPassword;

    @PropertiesPattern(property = "validation.password-regex", message = "{validation.password.invalid}")
    @ApiModelProperty(value = "Password. If create a new operator, this parameter is required")
    private String password;
    @ApiModelProperty(value = "Confirm Password")
    private String confirmPassword;

    @ApiModelProperty(value = "Username. If create a new operator, this parameter is required")
    private String username;
    @ApiModelProperty(value = "Status, 0 = Disabled, 1 = Enabled, default enabled")
    private Integer status;
    @ApiModelProperty(value = "ID of selected role")
    private Integer roleId;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @JsonIgnore
    @AssertTrue(message = "password validation failed")
    public boolean isValidPassword() {
        if (null == this.getPassword()) {
            return true;
        }

        return this.getPassword().equals(this.getConfirmPassword());
    }
}