package com.tripleonetech.sbe.operator;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum OperatorStatusEnum implements BaseCodeEnum {
    DISABLED(0), ENABLED(1);

    private int code;
    OperatorStatusEnum(int code){
        this.code = code;
    }

    public int getCode(){
        return this.code;
    }
}
