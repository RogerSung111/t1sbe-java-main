package com.tripleonetech.sbe.operator;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OperatorOauthMapper {
    OperatorOauth get(Integer operatorId);
    
    void upsert(OperatorOauth operatorOauth);
    
    
}
