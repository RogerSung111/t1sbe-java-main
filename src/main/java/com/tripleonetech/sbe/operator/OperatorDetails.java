package com.tripleonetech.sbe.operator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class OperatorDetails implements UserDetails {
    private static final long serialVersionUID = 3179892706129249254L;

    private Operator operator;

    public OperatorDetails(Operator operator) {
        this.operator = operator;
    }

    public Operator getOperator() {
        return this.operator;
    }

    public Integer getId() {
        return this.operator.getId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(operator.getRole().getName()));
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.operator.getPasswordHash();
    }

    @Override
    public String getUsername() {
        return this.operator.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.operator.getStatus() == OperatorStatusEnum.ENABLED.getCode();
    }
}
