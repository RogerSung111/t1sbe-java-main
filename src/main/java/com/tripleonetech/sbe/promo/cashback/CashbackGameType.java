package com.tripleonetech.sbe.promo.cashback;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Validated
public class CashbackGameType {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer id;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer ruleId;
    @NotNull
    @ApiModelProperty(value = "ID of game api.", required = true)
    private Integer gameApiId;
    @NotNull
    @ApiModelProperty(value = "ID of game type.", required = true)
    private Integer gameTypeId;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private LocalDateTime createdAt;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private String gameApiName;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private String gameTypeName;

    private SiteCurrencyEnum currency;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public Integer getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(Integer gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getGameApiName() {
        return gameApiName;
    }

    public void setGameApiName(String gameApiName) {
        this.gameApiName = gameApiName;
    }

    public String getGameTypeName() {
        return gameTypeName;
    }

    public void setGameTypeName(String gameTypeName) {
        this.gameTypeName = gameTypeName;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    @JsonProperty("id")
    public String genId() {
        return gameApiId + "-" + gameTypeId;
    }

    @JsonProperty("name")
    public String genName() {
        return gameApiName + " - " + gameTypeName;
    }

}
