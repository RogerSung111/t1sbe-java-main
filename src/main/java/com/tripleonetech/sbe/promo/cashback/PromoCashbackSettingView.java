package com.tripleonetech.sbe.promo.cashback;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;

import io.swagger.annotations.ApiModelProperty;

public class PromoCashbackSettingView extends PromoCashbackSetting {
    @ApiModelProperty("Start time of game logs calculation")
    private LocalDateTime dailyCashbackStartTime;
    @ApiModelProperty(value="Next Calculation Time")
    private LocalDateTime nextCalculationTime;
    
    public ZonedDateTime getDailyCashbackStartTime() {
        return DateUtils.localToZoned(dailyCashbackStartTime);
    }
    
    public void setDailyCashbackStartTime(LocalDateTime dailyCashbackStartTime) {
        this.dailyCashbackStartTime = dailyCashbackStartTime;
    }

    public ZonedDateTime getNextCalculationTime() {
        return DateUtils.localToZoned(nextCalculationTime);
    }

    public void setNextCalculationTime(LocalDateTime nextCalculationTime) {
        this.nextCalculationTime = nextCalculationTime;
    }
}

// Properties = 00:00Z
// API = 00:00Z
// Cashback Calculation = Properties = 00:00
