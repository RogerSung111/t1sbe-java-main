package com.tripleonetech.sbe.promo.cashback;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.model.MultiCurrencyAmount;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PromoCashbackSettingForm {
    @NotNull
    @ApiModelProperty(value = "Execution cycle, an record could be any of: DAILY(1), REALTIME(2)", example = "1", required = true)
    private Integer executionType;
    @NotEmpty
    @Valid
    @ApiModelProperty(value = "Minimum bonus amount this cashback can grant", example = "[{\"amount\": 100.00, \"currency\": \"CNY\"}, {\"amount\": 20.00, \"currency\": \"USD\"}]", required = true)
    private MultiCurrencyAmount minBonusAmount;
    @NotEmpty
    @Valid
    @ApiModelProperty(value= "Maximum bonus amount this cashback can grant", example = "[{\"amount\": 100.00, \"currency\": \"CNY\"}, {\"amount\": 20.00, \"currency\": \"USD\"}]", required = true)
    private MultiCurrencyAmount maxBonusAmount;
    @NotNull
    @ApiModelProperty(value= "0 = Disabled, 1 = Enabled", required = true)
    private Boolean enabled;

    public Integer getExecutionType() {
        return executionType;
    }

    public void setExecutionType(int executionType) {
        this.executionType = executionType;
    }

    @ApiModelProperty(value = "Derived property", hidden = true)
    public CashbackExecutionTypeEnum getExecutionTypeEnum() {
        return CashbackExecutionTypeEnum.valueOf(executionType);
    }

    public void setExecutionType(Integer executionType) {
        this.executionType = executionType;
    }

    public MultiCurrencyAmount getMinBonusAmount() {
        return minBonusAmount;
    }

    public void setMinBonusAmount(MultiCurrencyAmount minBonusAmount) {
        this.minBonusAmount = minBonusAmount;
    }

    public MultiCurrencyAmount getMaxBonusAmount() {
        return maxBonusAmount;
    }

    public void setMaxBonusAmount(MultiCurrencyAmount maxBonusAmount) {
        this.maxBonusAmount = maxBonusAmount;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMinBonusAmountJson(){
        return JsonUtils.objectToJson(minBonusAmount);
    }

    @ApiModelProperty(hidden = true)
    public void setMinBonusAmountJson(String minBonusAmountJson) {
        this.minBonusAmount = JsonUtils.jsonToObject(minBonusAmountJson, MultiCurrencyAmount.class);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMaxBonusAmountJson(){
        return JsonUtils.objectToJson(maxBonusAmount);
    }

    @ApiModelProperty(hidden = true)
    public void setMaxBonusAmountJson(String maxBonusAmountJson) {
        this.maxBonusAmount = JsonUtils.jsonToObject(maxBonusAmountJson, MultiCurrencyAmount.class);
    }

}
