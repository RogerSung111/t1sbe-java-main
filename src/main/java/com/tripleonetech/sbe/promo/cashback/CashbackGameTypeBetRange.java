package com.tripleonetech.sbe.promo.cashback;

import java.math.BigDecimal;

public class CashbackGameTypeBetRange {
    private Integer ruleId;
    private Integer gameApiId;
    private Integer gameTypeId;
    private Integer limit;
    private BigDecimal cashbackPercentage;

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public Integer getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(Integer gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public BigDecimal getCashbackPercentage() {
        return cashbackPercentage;
    }

    public void setCashbackPercentage(BigDecimal cashbackPercentage) {
        this.cashbackPercentage = cashbackPercentage;
    }
}
