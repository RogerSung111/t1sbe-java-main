package com.tripleonetech.sbe.promo.cashback;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.model.MultiCurrencyAmount;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class PromoCashbackSetting {
    @JsonIgnore
    private Integer id;
    private Integer executionType;
    private MultiCurrencyAmount minBonusAmount;
    private MultiCurrencyAmount maxBonusAmount;
    private Integer withdrawConditionMultiplier;
    private boolean enabled;
    @JsonIgnore
    private LocalDateTime createdAt;
    @JsonIgnore
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExecutionType() {
        return executionType;
    }

    public void setExecutionType(Integer executionType) {
        this.executionType = executionType;
    }

    @JsonIgnore
    public CashbackExecutionTypeEnum getExecutionTypeEnum() {
        return CashbackExecutionTypeEnum.valueOf(executionType);
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public MultiCurrencyAmount getMinBonusAmount() {
        if(minBonusAmount == null){
            minBonusAmount = new MultiCurrencyAmount();
        }
        return minBonusAmount;
    }

    public void setMinBonusAmount(MultiCurrencyAmount minBonusAmount) {
        this.minBonusAmount = minBonusAmount;
    }

    public MultiCurrencyAmount getMaxBonusAmount() {
        if(maxBonusAmount == null){
            maxBonusAmount = new MultiCurrencyAmount();
        }
        return maxBonusAmount;
    }

    public void setMaxBonusAmount(MultiCurrencyAmount maxBonusAmount) {
        this.maxBonusAmount = maxBonusAmount;
    }

    public Integer getWithdrawConditionMultiplier() {
        return withdrawConditionMultiplier;
    }

    public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    // -- Derived properties to help coding --
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMinBonusAmountJson() {
        return JsonUtils.objectToJson(getMinBonusAmount());
    }
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMaxBonusAmountJson() {
        return JsonUtils.objectToJson(getMaxBonusAmount());
    }
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public void setMinBonusAmountJson(String json) {
        if(json == null){
            return;
        }
        this.setMinBonusAmount(
                new MultiCurrencyAmount(JsonUtils.jsonToObjectList(json, MultiCurrencyAmount.CurrencyAmount.class)));
    }
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public void setMaxBonusAmountJson(String json) {
        if(json == null){
            return;
        }
        this.setMaxBonusAmount(
                new MultiCurrencyAmount(JsonUtils.jsonToObjectList(json, MultiCurrencyAmount.CurrencyAmount.class)));
    }
}
