package com.tripleonetech.sbe.promo.cashback;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Validated
public class PromoCashbackBetRange {
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer id;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer ruleId;
    @NotNull
    @PositiveOrZero
    @ApiModelProperty(value = "Limit of bet amount, must be greater than or equal to 0.", required = true)
    private BigDecimal limit;
    @NotNull
    @Min(0)
    @Max(100)
    @ApiModelProperty(value = "Cashback percentage value should be between 0.00 and 100.00", required = true)
    private BigDecimal cashbackPercentage;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    public BigDecimal getCashbackPercentage() {
        return cashbackPercentage;
    }

    public void setCashbackPercentage(BigDecimal percentage) {
        this.cashbackPercentage = percentage;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

}
