package com.tripleonetech.sbe.promo.cashback;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PromoCashbackSettingMapper {
    PromoCashbackSetting get();

    int update(PromoCashbackSettingForm form);
}
