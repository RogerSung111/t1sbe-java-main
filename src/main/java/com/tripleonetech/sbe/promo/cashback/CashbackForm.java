package com.tripleonetech.sbe.promo.cashback;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public class CashbackForm {
    @NotBlank
    @ApiModelProperty(value = "Name of current cashback", required = true)
    private String name;
    @ApiModelProperty(value = "ID of player group", required = true)
    private Integer groupId;
    @NotNull
    @ApiModelProperty(value = "Bonus auto released by system.", required = true)
    private Boolean bonusAutoRelease;

    @NotNull
    @ApiModelProperty(required = true)
    private SiteCurrencyEnum currency;

    @NotNull
    @ApiModelProperty(required = true)
    private Boolean enabled;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @NotEmpty
    @ApiModelProperty(value = "Game type settings in current cashback setting", required = true)
    private List<@Valid CashbackGameType> gameTypes;

    @NotEmpty
    @ApiModelProperty(value = "Bet range settings in current cashback setting", required = true)
    private List<@Valid PromoCashbackBetRange> settings;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Boolean getBonusAutoRelease() {
        return bonusAutoRelease;
    }

    public void setBonusAutoRelease(Boolean bonusAutoRelease) {
        this.bonusAutoRelease = bonusAutoRelease;
    }

    public List<CashbackGameType> getGameTypes() {
        return gameTypes;
    }

    public void setGameTypes(List<CashbackGameType> gameTypes) {
        this.gameTypes = gameTypes;
    }

    public List<PromoCashbackBetRange> getSettings() {
        return settings;
    }

    public void setSettings(List<PromoCashbackBetRange> settings) {
        this.settings = settings;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public static class CashbackBatchUpdateForm extends CashbackForm {

        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
