package com.tripleonetech.sbe.promo.cashback;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class CashbackView extends Cashback {
    private List<CashbackGameType> gameTypes;
    private List<PromoCashbackBetRange> settings;

    public List<CashbackGameType> getGameTypes() {
        return gameTypes;
    }

    public void setGameTypes(List<CashbackGameType> gameTypes) {
        this.gameTypes = gameTypes;
    }

    public List<PromoCashbackBetRange> getSettings() {
        return settings;
    }

    public void setSettings(List<PromoCashbackBetRange> settings) {
        this.settings = settings;
    }

    @ApiModelProperty(hidden = true)
    public boolean hasSettings() {
        return getSettings() != null && getSettings().size() > 0;
    }

    @ApiModelProperty(hidden = true)
    public boolean hasGameTypes() {
        return getGameTypes() != null && getGameTypes().size() > 0;
    }
}
