package com.tripleonetech.sbe.promo.cashback;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PromoCashbackMapper {

    List<Cashback> list();

    List<Cashback> listByIds(@Param("ids") List<Integer> ids);

    Cashback get(int id);

    CashbackView getView(@Param("id") int id, @Param("locale") String locale, @Param("defaultLocale") String defaultLocale);

    int insert(Cashback promo);

    int insertBatch(List<? extends Cashback> cashbackList);

    int update(Cashback promo);

    int updateBatch(List<? extends Cashback> cashbackList);

    int delete(int id);

    int groupDelete(int groupId);

    List<CashbackView> listView(@Param("locale") String locale, @Param("defaultLocale") String defaultLocale);

    List<CashbackView> listViewByGroup(@Param("groupId") int groupId, @Param("locale") String locale,
                                       @Param("defaultLocale") String defaultLocale);

    // -- Game Type related --
    List<CashbackGameType> getGameTypes(@Param("ruleId") Integer ruleId, @Param("locale") String locale);

    void insertGameType(CashbackGameType gameType);

    void insertBatchGameType(List<CashbackGameType> gameTypeList);

    void deleteGameTypesByRuleId(Integer ruleId);

    void deleteGameTypesByRuleIds(@Param("ruleIds") List<Integer> ruleIds);

    // -- Bet Range related --
    List<PromoCashbackBetRange> getSettings(Integer ruleId);

    void insertSetting(PromoCashbackBetRange cashbackSetting);

    void insertBatchSetting(List<PromoCashbackBetRange> cashbackSettingList);

    void deleteSettingByRuleId(Integer ruleId);

    void deleteSettingByRuleIds(@Param("ruleIds") List<Integer> ruleIds);

    // -- Used for preview --
    List<Integer> listLimits();

    List<CashbackGameTypeBetRange> listGameTypeBetRange();
}
