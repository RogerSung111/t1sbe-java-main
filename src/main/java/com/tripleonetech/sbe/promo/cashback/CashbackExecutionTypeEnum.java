package com.tripleonetech.sbe.promo.cashback;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum CashbackExecutionTypeEnum implements BaseCodeEnum {
    DAILY(1), REALTIME(2);

    private int code;

    CashbackExecutionTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static CashbackExecutionTypeEnum valueOf(Integer code){
        return code == 2 ? REALTIME : DAILY;
    }
}
