package com.tripleonetech.sbe.promo.bonus;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.PromoTypeEnum;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class BonusView {
    private Integer id;
    private Integer playerId;
    private String username;
    private LocalDateTime bonusDate;
    private Integer ruleId;
    private String promoRuleName;
    private Integer promoType;
    private BigDecimal bonusAmount;
    private BigDecimal referenceAmount;
    private Integer withdrawConditionMultiplier;
    private Integer status;
    private SiteCurrencyEnum currency;
    private Integer loginDay;
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ZonedDateTime getBonusDate() {
        return DateUtils.localToZoned(bonusDate);
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public LocalDateTime getBonusDateLocal() {
        return this.bonusDate;
    }

    public void setBonusDate(LocalDateTime bonusDate) {
        this.bonusDate = bonusDate;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getPromoRuleName() {
        return promoRuleName;
    }

    public void setPromoRuleName(String promoRuleName) {
        this.promoRuleName = promoRuleName;
    }

    public Integer getPromoType() {
        return promoType;
    }

    public void setPromoType(Integer promoType) {
        this.promoType = promoType;
    }

    public PromoTypeEnum getPromoTypeEnum() {
        return PromoTypeEnum.valueOf(getPromoType());
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BigDecimal getReferenceAmount() {
        return referenceAmount;
    }

    public void setReferenceAmount(BigDecimal referenceAmount) {
        this.referenceAmount = referenceAmount;
    }

    public Integer getWithdrawConditionMultiplier() {
        return withdrawConditionMultiplier;
    }

    public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BonusStatusEnum getStatusEnum() {
        return BonusStatusEnum.valueOf(getStatus());
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getLoginDay() {
        return loginDay;
    }

    public void setLoginDay(Integer loginDay) {
        this.loginDay = loginDay;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
