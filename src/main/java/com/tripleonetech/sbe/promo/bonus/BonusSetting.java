package com.tripleonetech.sbe.promo.bonus;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public abstract class BonusSetting {
    @NotNull
    @ApiModelProperty(required = true, value= "Whether bonus should need operator approval to release to player wallet")
    private boolean bonusAutoRelease;
    @NotNull
    @ApiModelProperty(required = true, value= "How much withdraw condition should this bonus generate, minimum 0 i.e. no withdraw condition")
    private Integer withdrawConditionMultiplier;
    @NotNull
    @ApiModelProperty(required = true, value= "Currency of bonus")
    private SiteCurrencyEnum currency;

    public boolean isBonusAutoRelease() {
        return bonusAutoRelease;
    }

    public void setBonusAutoRelease(boolean bonusAutoRelease) {
        this.bonusAutoRelease = bonusAutoRelease;
    }

    public Integer getWithdrawConditionMultiplier() {
        return withdrawConditionMultiplier;
    }

    public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }
}
