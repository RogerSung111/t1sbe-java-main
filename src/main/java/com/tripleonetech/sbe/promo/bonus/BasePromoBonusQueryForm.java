package com.tripleonetech.sbe.promo.bonus;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BasePromoBonusQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Query start date (inclusive)")
    private LocalDateTime bonusDateStart;
    @ApiModelProperty(value = "Query end date (inclusive)")
    private LocalDateTime bonusDateEnd;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Promotion rule ID")
    private Integer ruleId;
    @ApiModelProperty(value = "Promotion types: CASHBACK(1), REFERRAL(2), CAMPAIGN_DEPOSIT(11), CAMPAIGN_TASK(12), CAMPAIGN_RESCUE(13)")
    private List<Integer> promoTypes;
    @ApiModelProperty(value = "Bonus status: PENDING_APPROVAL(0), APPROVED(1), REJECTED(10)", allowableValues = "0,1,10")
    private Integer status;
    @ApiModelProperty(value = "Player's username")
    private String username;

    public LocalDateTime getBonusDateStart() {
        return bonusDateStart;
    }

    public void setBonusDateStart(ZonedDateTime bonusDateStart) {
        this.bonusDateStart = DateUtils.zonedToLocal(bonusDateStart);
    }

    public LocalDateTime getBonusDateEnd() {
        return bonusDateEnd;
    }

    public void setBonusDateEnd(ZonedDateTime bonusDateEnd) {
        this.bonusDateEnd = DateUtils.zonedToLocal(bonusDateEnd);
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Integer> getPromoTypes() {
        if(promoTypes == null){
            promoTypes = new ArrayList<>();
        }
        return promoTypes;
    }

    public void setPromoTypes(List<Integer> promoTypes) {
        this.promoTypes = promoTypes;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not appear in swagger UI")
    public BonusStatusEnum getStatusEnum() {
        return Objects.isNull(getStatus()) ? null : BonusStatusEnum.valueOf(getStatus());
    }

    @Override public String toString() {
        return "BasePromoBonusQueryForm{" +
                "bonusDateStart=" + bonusDateStart +
                ", bonusDateEnd=" + bonusDateEnd +
                ", currency=" + currency +
                ", ruleId=" + ruleId +
                ", promoTypes=" + promoTypes +
                ", status=" + status +
                "} " + super.toString();
    }
}
