package com.tripleonetech.sbe.promo.bonus;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tripleonetech.sbe.promo.PromoTypeEnum;

public class PromoBonus {
    private Integer id;

    private Integer playerId;

    private LocalDateTime bonusDate;

    private Integer ruleId;

    private PromoTypeEnum promoType;

    private BigDecimal bonusAmount;

    private BigDecimal referenceAmount;

    private Integer withdrawConditionMultiplier;

    private BonusStatusEnum status;

    private String comment;

    private Integer loginDay;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public PromoBonus() {
    }

    public PromoBonus(Integer playerId, Integer ruleId, PromoTypeEnum promoType, BigDecimal bonus,
                      BigDecimal referenceAmount, LocalDateTime bonusDate, Integer withdrawConditionMultiplier) {
        this.playerId = playerId;
        this.ruleId = ruleId;
        this.promoType = promoType;
        this.bonusAmount = bonus;
        this.referenceAmount = referenceAmount;
        this.bonusDate = bonusDate;
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
        this.status = BonusStatusEnum.PENDING_APPROVAL;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public LocalDateTime getBonusDate() {
        return bonusDate;
    }

    public void setBonusDate(LocalDateTime bonusDate) {
        this.bonusDate = bonusDate;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public PromoTypeEnum getPromoType() {
        return promoType;
    }

    public void setPromoType(PromoTypeEnum promoType) {
        this.promoType = promoType;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BigDecimal getReferenceAmount() {
        return referenceAmount;
    }

    public void setReferenceAmount(BigDecimal referenceAmount) {
        this.referenceAmount = referenceAmount;
    }

    public Integer getWithdrawConditionMultiplier() {
        return withdrawConditionMultiplier;
    }

    public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
    }

    public BonusStatusEnum getStatus() {
        return status;
    }

    public void setStatus(BonusStatusEnum status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getLoginDay() {
        return loginDay;
    }

    public void setLoginDay(Integer loginDay) {
        this.loginDay = loginDay;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "PromoBonus{" +
                "id=" + id +
                ", playerId=" + playerId +
                ", bonusDate=" + bonusDate +
                ", ruleId=" + ruleId +
                ", promoType=" + promoType +
                ", bonusAmount=" + bonusAmount +
                ", withdrawConditionMultiplier=" + withdrawConditionMultiplier +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
