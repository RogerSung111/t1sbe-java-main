package com.tripleonetech.sbe.promo.bonus;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class PromoCampaignBonusQueryForm extends BasePromoBonusQueryForm {
    @ApiModelProperty(value = "Player ID")
    private Integer playerId;
    @ApiModelProperty(value = "Promotion types: CAMPAIGN_DEPOSIT(11), CAMPAIGN_TASK(12), CAMPAIGN_RESCUE(13), CAMPAIGN_LOGIN(14)")
    private List<Integer> promoTypes;
    @ApiModelProperty(value = "Name of the campaign that's related to this bonus, fuzzy search")
    private String campaignName;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public List<Integer> getPromoTypes() {
        return promoTypes;
    }

    public void setPromoTypes(List<Integer> promoTypes) {
        this.promoTypes = promoTypes;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }
}
