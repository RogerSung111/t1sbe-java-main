package com.tripleonetech.sbe.promo.bonus;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface PromoBonusMapper {


    List<PromoBonus> listByPlayerId(Integer playerId, LocalDateTime createdAtStart,
                                    LocalDateTime createdAtEnd);

    List<PromoBonus> query(PromoBonus record);

    List<BonusView> queryByQueryForm(@Param("formObj") PromoBonusQueryForm formObj,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    List<BonusView> queryByCampaignQueryForm(@Param("formObj") PromoCampaignBonusQueryForm formObj,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    List<PlayerIdView> queryBonusPlayerByCampaign(@Param("ruleId") int ruleId,
                                                  @Param("promoType") int promoType,
                                                  @Param("query") PageQueryForm query,
                                                  @Param("pageNum") Integer pageNum,
                                                  @Param("pageSize") Integer pageSize);

    PromoBonus queryById(Integer id);

    default BonusView queryLatestExistsBonus(@Param("playerId") Integer playerId,
                                   @Param("promoType") PromoTypeEnum promoType,
                                   @Param("ruleId") Integer ruleId,
                                   @Param("loginDay") Integer loginDay){
        List<BonusView> bonusList = queryExistsBonus(playerId, promoType, ruleId, null, loginDay);
        return bonusList.size() > 0 ? bonusList.get(0) : null;
    }

    List<BonusView> queryExistsBonus(
            @Param("playerId") Integer playerId,
            @Param("promoType") PromoTypeEnum promoType,
            @Param("ruleId") Integer ruleId,
            @Param("receiveCycleStart") LocalDateTime receiveCycleStart,
            @Param("loginDay") Integer loginDay);

    int insert(PromoBonus record);

    int update(PromoBonus record);

    default int updateStatus(Integer id, BonusStatusEnum status) {
        return updateStatus(id, status, null);
    }

    int updateStatus(@Param("id") Integer id, @Param("status") BonusStatusEnum status, @Param("reason") String reason);

    int getPendingCount(@Param("isCashback") boolean isCashback);

}
