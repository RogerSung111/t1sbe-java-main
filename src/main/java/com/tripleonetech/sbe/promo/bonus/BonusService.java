package com.tripleonetech.sbe.promo.bonus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaign;
import com.tripleonetech.sbe.withdraw.condition.WithdrawCondition;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionService;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionSourceTypeEnum;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionStatusEnum;

@Service
public class BonusService {
    private static final Logger logger = LoggerFactory.getLogger(BonusService.class);

    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private WithdrawConditionService withdrawConditionService;

    public List<BonusView> query(PromoBonusQueryForm formObj) {
        List<BonusView> views = promoBonusMapper.queryByQueryForm(formObj,
                formObj.getPage(),
                formObj.getLimit());

        return views;
    }

    public List<BonusView> queryCampaign(PromoCampaignBonusQueryForm formObj) {
        List<BonusView> views = promoBonusMapper.queryByCampaignQueryForm(formObj,
                formObj.getPage(),
                formObj.getLimit());

        return views;
    }

    @Transactional
    public void systemApprove(PromoBonus promoBonus) {
        approve(promoBonus, null, null);
    }

    @Transactional
    public void approve(PromoBonus promoBonus, String operatorUsername, Integer operatorId) {
        WalletTransactionTypeEnum transactionTypeEnum;
        if (promoBonus.getStatus().equals(BonusStatusEnum.PENDING_APPROVAL)) {
            if (promoBonus.getPromoType().equals(PromoTypeEnum.CASHBACK)) {
                transactionTypeEnum = WalletTransactionTypeEnum.CASHBACK;
            } else {
                transactionTypeEnum = WalletTransactionTypeEnum.BONUS;
            }
            // Add bonus to player main wallet
            Boolean addBonusToMainWalletResult = walletService.addBonusToMainWallet(promoBonus, transactionTypeEnum,
                    operatorUsername, operatorId);
            if (addBonusToMainWalletResult) {
                promoBonusMapper.updateStatus(promoBonus.getId(), BonusStatusEnum.APPROVED, null);

                // insert withdrawal Condition
                if (promoBonus.getWithdrawConditionMultiplier() > 0) {
                    WithdrawCondition condition = new WithdrawCondition();
                    condition.setBeginAt(LocalDateTime.now());
                    condition.setBetRequired(
                            promoBonus.getBonusAmount()
                                    .multiply(new BigDecimal(promoBonus.getWithdrawConditionMultiplier())));
                    condition.setLockedOnAmount(promoBonus.getBonusAmount());
                    condition.setPlayerId(promoBonus.getPlayerId());
                    condition.setPromoType(promoBonus.getPromoType());
                    condition.setPromotionId(promoBonus.getRuleId());
                    condition.setSourceType(WithdrawConditionSourceTypeEnum.BONUS);
                    condition.setSourceId(promoBonus.getId());
                    condition.setStatus(WithdrawConditionStatusEnum.OPEN);
                    withdrawConditionService.create(condition);
                }
            }
        } else {
            logger.error("Failed approving promo bonus! ID: {}, status: {}", promoBonus.getId(),
                    promoBonus.getStatus());
        }
    }

    @Transactional
    public void reject(PromoBonus promoBonus, String reason) {
        promoBonusMapper.updateStatus(promoBonus.getId(), BonusStatusEnum.REJECTED, reason);
    }

    public boolean isJoined(Integer playerId, PromoCampaign promoCampaign) {
        LocalDateTime receiveCycleStart = null;
        if (promoCampaign.getBonusReceiveCycle() == BonusReceiveCycleEnum.UNLIMITED) {
            return false;
        }

        switch (promoCampaign.getBonusReceiveCycle()) {
        case ONCE:
            return !promoBonusMapper.queryExistsBonus(playerId, promoCampaign.getType(), promoCampaign.getId(),
                    null, null).isEmpty();
        case DAILY:
            receiveCycleStart = DateUtils.getStartOfDay();
            break;
        case WEEKLY:
            receiveCycleStart = DateUtils.getStartOfDay().minusWeeks(1).plusDays(1);
            break;
        case MONTHLY:
            receiveCycleStart = DateUtils.getStartOfDay().minusMonths(1).plusDays(1);
            break;
        case YEARLY:
            receiveCycleStart = DateUtils.getStartOfDay().minusYears(1).plusDays(1);
        default:
            break;
        }

        return promoBonusMapper.queryExistsBonus(playerId, promoCampaign.getType(), promoCampaign.getId(),
                receiveCycleStart, null).size() >= promoCampaign.getMaxBonusCountPerCycle();
    }
}
