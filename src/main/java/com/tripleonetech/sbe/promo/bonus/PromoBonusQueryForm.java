package com.tripleonetech.sbe.promo.bonus;

import io.swagger.annotations.ApiModelProperty;

public class PromoBonusQueryForm extends BasePromoBonusQueryForm {
    @ApiModelProperty(value = "Player's ID")
    private Integer playerId;
    @ApiModelProperty(value = "Name of the promotion rule that's related to this bonus, fuzzy search")
    private String promoName;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }
}
