package com.tripleonetech.sbe.promo.bonus;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum BonusStatusEnum implements BaseCodeEnum {
    PENDING_APPROVAL(0), APPROVED(1), REJECTED(10);

    private int code;

    BonusStatusEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static BonusStatusEnum valueOf(Integer code) {
        BonusStatusEnum[] enumConstants = BonusStatusEnum.values();
        for (BonusStatusEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
