package com.tripleonetech.sbe.promo.referral;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.ReferrerPlayer;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusService;
import com.tripleonetech.sbe.promo.bonus.BonusView;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;

@Service
public class PromoReferralDepositService {

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PromoPlayerReferralSettingMapper promoReferralMapper;
    @Autowired
    private BonusService bonusService;

    /**
     * 用 Approved 的 DepositRequest 來產生相對應 promo bonus, 並依設定自動加到主錢包
     */
    public void process(DepositRequest depositRequest) {
        List<ReferrerPlayer> referrerPlayers = playerMapper.getReferralPlayerByPlayerIds(
                Collections.singletonList(depositRequest.getPlayerId()));

        if (CollectionUtils.isEmpty(referrerPlayers) || referrerPlayers.get(0).getPlayerId() == null) {
            return;
        }

        ReferrerPlayer referrerPlayer = referrerPlayers.get(0);
        List<PromoPlayerReferralSetting> rules = promoReferralMapper.getByPlayerWithDefaultRules(
                null, referrerPlayer.getPlayerCredentialId(), referrerPlayer.getCurrency());

        PromoPlayerReferralSetting rule = getReferralRule(rules);
        if (rule == null) {
            return;
        }

        PromoBonus promoBonus = null;
        switch (Objects.requireNonNull(ReferralTypeEnum.valueOf(rule.getReferralType()))) {
            case ONE_TIME_REVENUE:
                promoBonus = getOneTimeRevenueBonus(referrerPlayer, rule, depositRequest.getAmount());
                break;
            case DEPOSIT_REVENUE:
                promoBonus = getDepositRevenueBonus(referrerPlayer, rule, depositRequest.getAmount());
                break;
            default:
                break;
        }

        if (promoBonus == null) {
            return;
        }
        promoBonusMapper.insert(promoBonus);

        if (rule.isBonusAutoRelease()) {
            bonusService.systemApprove(promoBonus);
        }
    }

    private PromoPlayerReferralSetting getReferralRule(List<PromoPlayerReferralSetting> rules) {
        PromoPlayerReferralSetting defaultRule = null;
        PromoPlayerReferralSetting actualRule = null;

        for (PromoPlayerReferralSetting rule : rules) {
            if (rule.getPlayerCredentialId() == null) {
                defaultRule = rule;
                continue;
            }
            actualRule = rule;
        }

        return actualRule == null ? defaultRule : actualRule;
    }

    private PromoBonus getOneTimeRevenueBonus(ReferrerPlayer referrerPlayer, PromoPlayerReferralSetting rule,
                                              BigDecimal depositAmount) {

        List<BonusView> fitBonuses = promoBonusMapper.queryExistsBonus(referrerPlayer.getPlayerId(),
                PromoTypeEnum.REFERRAL, rule.getId(), null, null);
        if (!CollectionUtils.isEmpty(fitBonuses)) {
            return null;
        }

        if (depositAmount.compareTo(rule.getMinFirstDepositAmount()) < 0) {
            return null;
        }

        PromoBonus promoBonus = new PromoBonus(referrerPlayer.getPlayerId(), rule.getId(), PromoTypeEnum.REFERRAL,
                rule.getFixedBonus(), depositAmount, DateUtils.getStartOfDay(), rule.getWithdrawConditionMultiplier());
        promoBonus.setComment("one time revenue from player id : " + referrerPlayer.getReferralPlayerId());

        return promoBonus;
    }

    private PromoBonus getDepositRevenueBonus(ReferrerPlayer referrerPlayer, PromoPlayerReferralSetting rule,
                                              BigDecimal depositAmount) {

        BigDecimal bonusAmount = BigDecimal.ZERO;
        if (Objects.nonNull(rule.getBonusPercentage())) {
            bonusAmount = depositAmount.multiply(rule.getBonusPercentage()).divide(ONE_HUNDRED);
        }
        if (Objects.nonNull(rule.getMinBonusAmount())) {
            bonusAmount = bonusAmount.max(rule.getMinBonusAmount());
        }
        if (Objects.nonNull(rule.getMaxBonusAmount())) {
            bonusAmount = bonusAmount.min(rule.getMaxBonusAmount());
        }

        PromoBonus promoBonus = new PromoBonus(referrerPlayer.getPlayerId(), rule.getId(), PromoTypeEnum.REFERRAL,
                bonusAmount, depositAmount, DateUtils.getStartOfDay(), rule.getWithdrawConditionMultiplier());
        promoBonus.setComment("deposit revenue from player id : " + referrerPlayer.getReferralPlayerId());

        return promoBonus;
    }

}
