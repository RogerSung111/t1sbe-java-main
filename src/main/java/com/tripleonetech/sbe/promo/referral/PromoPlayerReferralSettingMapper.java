package com.tripleonetech.sbe.promo.referral;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PromoPlayerReferralSettingMapper {

    List<PromoPlayerReferralSetting> list();

    PromoPlayerReferralSetting get(int id);

    int insert(PromoPlayerReferralSetting promo);

    int update(PromoPlayerReferralSetting promo);

    List<PromoPlayerReferralSetting> getByPlayerWithDefaultRules(@Param("referralType") ReferralTypeEnum referralType,
                                                 @Param("playerCredentialId") Integer playerCredentialId,
                                                 @Param("currency") SiteCurrencyEnum currency);

    List<PromoPlayerReferralSetting> listByType(@Param("referralType") ReferralTypeEnum referralType);

}
