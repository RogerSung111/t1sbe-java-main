package com.tripleonetech.sbe.promo.referral;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum ReferralTypeEnum implements BaseCodeEnum {

    ONE_TIME_REVENUE(0), DEPOSIT_REVENUE(1), BET_REVENUE(2);

    private int code;

    ReferralTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static ReferralTypeEnum valueOf(Integer code) {
        ReferralTypeEnum[] enumConstants = values();
        for (ReferralTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
