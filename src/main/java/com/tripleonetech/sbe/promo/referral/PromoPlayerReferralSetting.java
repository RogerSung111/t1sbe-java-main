package com.tripleonetech.sbe.promo.referral;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Range;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PromoPlayerReferralSetting {

    private Integer id;

    @ApiModelProperty(value = "Name of this referral")
    private String name;

    @ApiModelProperty(value = "Content (description) of this referral")
    private String content;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer playerCredentialId;

    @ApiModelProperty(value = "Referral type: ONE_TIME_REVENUE(0), DEPOSIT_REVENUE(1), BET_REVENUE(2)", allowableValues = "0,1,2")
    private Integer referralType;

    @ApiModelProperty(value = "Fixed bonus amount for this referral")
    private BigDecimal fixedBonus;

    @Range(max = 100, min = 0)
    @ApiModelProperty(value = "Percentage bonus for this referral")
    private BigDecimal bonusPercentage;

    @ApiModelProperty(value = "Whether bonus should need operator approval to release to player wallet")
    private boolean bonusAutoRelease;

    @ApiModelProperty(value = "Maximum first deposit amount this referral can grant")
    private BigDecimal minFirstDepositAmount;

    @ApiModelProperty(value = "Maximum bonus amount this referral can grant, can be empty if fixedBonus is used")
    private BigDecimal maxBonusAmount;

    @ApiModelProperty(value = "Minimum bonus amount this referral can grant, can be empty if fixedBonus is used")
    private BigDecimal minBonusAmount;

    @ApiModelProperty(value = "How much withdraw condition should this bonus generate, minimum 0 i.e. no withdraw condition")
    private Integer withdrawConditionMultiplier;

    @ApiModelProperty(required = true, value = "Currency of bonus")
    private SiteCurrencyEnum currency;

    @ApiModelProperty(hidden = true)
    private boolean enabled;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private boolean deleted;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private LocalDateTime createdAt;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    public Integer getReferralType() {
        return referralType;
    }

    public void setReferralType(Integer referralType) {
        this.referralType = referralType;
    }

    @JsonIgnore
    public ReferralTypeEnum getReferralTypeEnum() {
        return ReferralTypeEnum.valueOf(getReferralType());
    }

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public BigDecimal getBonusPercentage() {
        return bonusPercentage;
    }

    public void setBonusPercentage(BigDecimal bonusPercentage) {
        this.bonusPercentage = bonusPercentage;
    }

    public boolean isBonusAutoRelease() {
        return bonusAutoRelease;
    }

    public void setBonusAutoRelease(boolean bonusAutoRelease) {
        this.bonusAutoRelease = bonusAutoRelease;
    }

    public BigDecimal getMinFirstDepositAmount() {
        return minFirstDepositAmount;
    }

    public void setMinFirstDepositAmount(BigDecimal minFirstDepositAmount) {
        this.minFirstDepositAmount = minFirstDepositAmount;
    }

    public BigDecimal getMaxBonusAmount() {
        return maxBonusAmount;
    }

    public void setMaxBonusAmount(BigDecimal maxBonusAmount) {
        this.maxBonusAmount = maxBonusAmount;
    }

    public BigDecimal getMinBonusAmount() {
        return minBonusAmount;
    }

    public void setMinBonusAmount(BigDecimal minBonusAmount) {
        this.minBonusAmount = minBonusAmount;
    }

    public Integer getWithdrawConditionMultiplier() {
        return withdrawConditionMultiplier;
    }

    public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
        this.withdrawConditionMultiplier = withdrawConditionMultiplier;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

