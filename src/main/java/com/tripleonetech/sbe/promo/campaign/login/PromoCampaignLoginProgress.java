package com.tripleonetech.sbe.promo.campaign.login;

import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;

import java.math.BigDecimal;

public class PromoCampaignLoginProgress {
    private Byte requiredLoginDay;
    private BigDecimal bonusAmount;
    private BonusStatusEnum bonusStatusEnum;

    public Byte getRequiredLoginDay() {
        return requiredLoginDay;
    }

    public void setRequiredLoginDay(Byte requiredLoginDay) {
        this.requiredLoginDay = requiredLoginDay;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BonusStatusEnum getBonusStatusEnum() {
        return bonusStatusEnum;
    }

    public void setBonusStatusEnum(BonusStatusEnum bonusStatusEnum) {
        this.bonusStatusEnum = bonusStatusEnum;
    }
}
