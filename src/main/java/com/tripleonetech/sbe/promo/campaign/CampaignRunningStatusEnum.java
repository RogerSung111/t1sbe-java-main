package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

// This is a virtual enum
public enum CampaignRunningStatusEnum implements BaseCodeEnum {
    PENDING(0), // Waiting to run. Initial status. Return this when approval status is in DRAFT / PENDING_APPROVAL
    RUNNING(1), // Return this status if startTime / effectiveStartTime is earlier than today.
                     // Note: If effectiveStartTime is null and startTime past today, we return startTime as effectiveStartTime.
    FINISHED(2); // Return this status if endTime / effectiveEndTime is earlier than today.
                      // Note: If effectiveEndTime is null and endTime past today, we return endTime as effectiveEndTime.

    private int code;

    CampaignRunningStatusEnum(int code) {
        this.code = code;
    }

    @JsonValue
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static CampaignApprovalStatusEnum valueOf(Integer code) {
        CampaignApprovalStatusEnum[] enumConstants = CampaignApprovalStatusEnum.values();
        for (CampaignApprovalStatusEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
