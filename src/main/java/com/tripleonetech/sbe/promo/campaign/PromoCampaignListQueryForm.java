package com.tripleonetech.sbe.promo.campaign;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.ZonedDateTime;
import java.util.List;

public class PromoCampaignListQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Campaign type: CAMPAIGN_DEPOSIT(11, \"deposit\"), CAMPAIGN_TASK(12, \"task\"), CAMPAIGN_RESCUE(13, \"rescue\"), CAMPAIGN_LOGIN(14, \"login\")", allowableValues = "11,12,13,14")
    private int type;
    @ApiModelProperty(value = "Name of campaign")
    private String name;
    @ApiModelProperty(value = "Campaign status: DRAFT(0), PENDING_APPROVAL(1), REJECTED(9), APPROVED(10), DELETED(20);", allowableValues = "0,1,9,10,20")
    private List<Integer> status;
    @ApiModelProperty(value = "Whether this campaign is auto-join")
    private Boolean autoJoin;
    @ApiModelProperty(value = "Currency of this campaign")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Search start time. Search will return all campaigns ever becomes available between start time and end time")
    private ZonedDateTime startTime;
    @ApiModelProperty(value = "Search end time. Search will return all campaigns ever becomes available between start time and end time")
    private ZonedDateTime endTime;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getStatus() {
        return status;
    }

    public void setStatus(List<Integer> status) {
        this.status = status;
    }

    public Boolean isAutoJoin() {
        return autoJoin;
    }

    public void setAutoJoin(Boolean autoJoin) {
        this.autoJoin = autoJoin;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }
}
