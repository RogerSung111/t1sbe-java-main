package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.player.tag.PlayerTagIdNameView;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskTypeEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PromoCampaignBaseView {
    @ApiModelProperty(value = "For front-end to use as unique ID")
    private String uid;
    @ApiModelProperty(value = "ID of record in their original database table, not unique")
    private Integer id;
    @ApiModelProperty(value = "Promo type, CAMPAIGN_DEPOSIT(11, \"deposit\"), CAMPAIGN_TASK(12, \"task\"), CAMPAIGN_RESCUE(13, \"rescue\")")
    private PromoTypeEnum type;
    @ApiModelProperty(value = "Task type, only available for campaign task. EmailVerificationBonus(0), SmsVerificationBonus(1), IdentityVerificationBonus(2)")
    private PromoCampaignTaskTypeEnum taskType;
    @ApiModelProperty(value = "Currency of this campaign")
    private SiteCurrencyEnum currency;
    private String name;
    private String content;
    @ApiModelProperty(value = "Configured campaign start time")
    private LocalDateTime startTime;
    @ApiModelProperty(value = "Configured campaign end time")
    private LocalDateTime endTime;
    @ApiModelProperty(value = "Actual campaign start time")
    private LocalDateTime effectiveStartTime;
    @ApiModelProperty(value = "Actual campaign end time")
    private LocalDateTime effectiveEndTime;
    @ApiModelProperty(value = "Whether this campaign needs player to join manually")
    private boolean autoJoin;
    @ApiModelProperty(value = "How often bonus repeats. UNLIMITED(0), ONCE(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5)")
    private BonusReceiveCycleEnum bonusReceiveCycle;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    @JsonIgnore
    @ApiModelProperty(hidden = true, value="Player tags and player groups are accessed separately")
    private List<PlayerTagIdNameView> playerTagsAndGroups;
    private List<PlayerIdView> players;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PromoTypeEnum getType() {
        return type;
    }

    public void setType(PromoTypeEnum type) {
        this.type = type;
    }

    public PromoCampaignTaskTypeEnum getTaskType() {
        return taskType;
    }

    public void setTaskType(PromoCampaignTaskTypeEnum taskType) {
        this.taskType = taskType;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getEffectiveStartTime() {
        if(effectiveStartTime == null && startTime.isBefore(LocalDateTime.now())) {
            return startTime;
        }
        return effectiveStartTime;
    }

    public void setEffectiveStartTime(LocalDateTime effectiveStartTime) {
        this.effectiveStartTime = effectiveStartTime;
    }

    public LocalDateTime getEffectiveEndTime() {
        if(effectiveEndTime == null && endTime != null && endTime.isBefore(LocalDateTime.now())) {
            return endTime;
        }
        return effectiveEndTime;
    }

    public void setEffectiveEndTime(LocalDateTime effectiveEndTime) {
        this.effectiveEndTime = effectiveEndTime;
    }

    public boolean isAutoJoin() {
        return autoJoin;
    }

    public void setAutoJoin(boolean autoJoin) {
        this.autoJoin = autoJoin;
    }

    public BonusReceiveCycleEnum getBonusReceiveCycle() {
        return bonusReceiveCycle;
    }

    public void setBonusReceiveCycle(BonusReceiveCycleEnum bonusReceiveCycle) {
        this.bonusReceiveCycle = bonusReceiveCycle;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PlayerTagIdNameView> getPlayerTagsAndGroups() {
        // MyBatis collection will insert an empty object to the list when dealing with null left join
        if(playerTagsAndGroups == null || playerTagsAndGroups.size() == 1 && playerTagsAndGroups.get(0).getName() == null){
            playerTagsAndGroups = new ArrayList<>();
        }
        return playerTagsAndGroups;
    }

    public void setPlayerTagsAndGroups(List<PlayerTagIdNameView> playerTagsAndGroups) {
        this.playerTagsAndGroups = playerTagsAndGroups;
    }

    // Obtain player tags and groups
    public List<IdNameView> getPlayerTags() {
        return getPlayerTagsAndGroups().stream().filter(e -> !e.isGroup()).collect(Collectors.toList());
    }

    public List<IdNameView> getPlayerGroups() {
        return getPlayerTagsAndGroups().stream().filter(e -> e.isGroup()).collect(Collectors.toList());
    }

    public List<PlayerIdView> getPlayers() {
        // MyBatis collection will insert an empty object to the list when dealing with null left join
        if(players == null || players.size() == 1 && players.get(0).getUsername() == null){
            players = new ArrayList<>();
        }
        return players;
    }

    public void setPlayers(List<PlayerIdView> players) {
        this.players = players;
    }

}
