package com.tripleonetech.sbe.promo.campaign.login;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PromoCampaignLoginRule {
    private Integer campaignId;

    private Byte loginDay;

    private BigDecimal fixedBonus;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public Byte getLoginDay() {
        return loginDay;
    }

    public void setLoginDay(Byte loginDay) {
        this.loginDay = loginDay;
    }

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PromoCampaignLoginRule() {
    }

    public PromoCampaignLoginRule(Integer campaignId, Byte loginDay, BigDecimal fixedBonus) {
        this.campaignId = campaignId;
        this.loginDay = loginDay;
        this.fixedBonus = fixedBonus;
    }

    @Override
    public String toString() {
        return "PromoCampaignLoginRule{" +
                "campaignId=" + campaignId +
                ", loginDay=" + loginDay +
                ", fixedBonus=" + fixedBonus +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
