package com.tripleonetech.sbe.promo.campaign.login;

import io.swagger.annotations.ApiModelProperty;

import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaign;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PromoCampaignLogin extends PromoCampaign {
    @ApiModelProperty(value = "Define bonuses of each consecutive login days")
    private List<PromoCampaignLoginRule> rules;

    public List<PromoCampaignLoginRule> getRules() {
        return rules;
    }

    public void setRules(List<PromoCampaignLoginRule> rules) {
        this.rules = rules;
    }

    public BigDecimal getFixedBonus(Byte loginDay) {
        if (!getRules().isEmpty()) {
            Optional<PromoCampaignLoginRule> result = getRules().stream()
                    .filter(x -> x.getLoginDay() == loginDay).findFirst();
            if (result.isPresent()) {
                return result.get().getFixedBonus();
            }
        }
        return null;
    }

    @Override
    public PromoTypeEnum getType() {
        return PromoTypeEnum.CAMPAIGN_LOGIN;
    }

    public static PromoCampaignLogin from(PromoCampaignLoginForm form) {
        PromoCampaignLogin loginCampaign = new PromoCampaignLogin();
        BeanUtils.copyProperties(form, loginCampaign, "rules");

        List<PromoCampaignLoginRule> rules = new ArrayList<>();
        for (PromoCampaignLoginRuleForm rule : CollectionUtils.emptyIfNull(form.getRules())) {
            PromoCampaignLoginRule campaignLoginRule = new PromoCampaignLoginRule();
            BeanUtils.copyProperties(rule, campaignLoginRule);
            // TODO: Put campaign rules' ID
            //campaignLoginRule.setCampaignId(null != form.getId() ? form.getId() : null);
            rules.add(campaignLoginRule);
        }
        loginCampaign.setRules(rules);
        loginCampaign.setBonusReceiveCycle(form.getBonusReceiveCycleEnum());
        return loginCampaign;
    }

    @Override
    public BonusReceiveCycleEnum getBonusReceiveCycle() {
        return BonusReceiveCycleEnum.ONCE;
    }

    @Override
    public Integer getMaxBonusCountPerCycle() {
        return 1;
    }
}
