package com.tripleonetech.sbe.promo.campaign.task;

import java.time.ZoneId;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerProfileMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusService;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;
import com.tripleonetech.sbe.sms.history.SmsOtpValidationSuccessEvent;

@Component
public class PromoCampaignTaskSmsVerificationFacade {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignTaskSmsVerificationFacade.class);

    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PromoCampaignTaskMapper campaignTaskMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private PlayerMapper playerMapper;

    private static final ZoneId zoneId = ZoneId.of("UTC");

    public void calculatePromo(SmsOtpValidationSuccessEvent event) {
        Player player = playerMapper.get(event.getPlayerId());
        boolean phoneVerified = playerProfileMapper.getByPlayerId(event.getPlayerId()).isPhoneVerified();
        PromoCampaignTask promoSetting = campaignTaskMapper
                .queryAvailableRule(PromoCampaignTaskTypeEnum.SmsVerificationBonus.getCode(), player.getCurrency());

        if (promoSetting != null && promoSetting.getId() != null) {
            // SMS verification promo campaign is a one time promo
            // Check for existing PENDING_APPROVAL, APPROVED and REJECTED bonus
            boolean isJoined = bonusService.isJoined(event.getPlayerId(), promoSetting);
            PromoBonus addPromoBonus = new PromoBonus(event.getPlayerId(), promoSetting.getId(),
                    PromoTypeEnum.CAMPAIGN_TASK, promoSetting.getFixedBonus(), null,
                    DateUtils.getStartOfDay(), promoSetting.getWithdrawConditionMultiplier());

            if (phoneVerified && !isJoined) {
                // 如果 campaign_enabled 為 false 的時候，插入bonus並馬上設為rejected
                if (BooleanUtils.isFalse(player.getCampaignEnabled())) {
                    addPromoBonus.setStatus(BonusStatusEnum.REJECTED);
                    addPromoBonus.setComment("player's campaign status is inactive");
                    promoBonusMapper.insert(addPromoBonus);
                    return;
                }
                promoBonusMapper.insert(addPromoBonus);
                bonusService.systemApprove(addPromoBonus);
                logger.debug("Bonus has been successfully released to the player with playerId: {}, bonusAmount: {}", event.getPlayerId(), addPromoBonus.getBonusAmount());
            } else if (isJoined) {
                logger.debug(
                        "Bonus already existed in playerId : {}, promoType: {}, ruleId: {}, ReceiveCycle: {}, maxBonusCountPerCycle: {}",
                        event.getPlayerId(), PromoTypeEnum.CAMPAIGN_TASK, promoSetting.getId(),
                        BonusReceiveCycleEnum.ONCE, 1);
            } else {
                logger.debug("Failed inserting PromoBonus: {}, Phone Verification Status: {}",
                        addPromoBonus, phoneVerified);
            }
        } else {
            logger.debug("No available SmsVerification promo campaign found!");
        }

    }
}
