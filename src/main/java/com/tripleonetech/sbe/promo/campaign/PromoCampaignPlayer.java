package com.tripleonetech.sbe.promo.campaign;

import com.tripleonetech.sbe.promo.PromoTypeEnum;
import io.swagger.annotations.ApiModelProperty;

public class PromoCampaignPlayer {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(hidden = true)
    private Integer playerId;
    @ApiModelProperty(value = "Campaign type: CAMPAIGN_DEPOSIT(11), CAMPAIGN_TASK(12), CAMPAIGN_RESCUE(13)", required = true, allowableValues = "11,12,13")
    private Integer promoType;
    @ApiModelProperty(value = "Campaign id", required = true)
    private Integer campaignId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getPromoType() {
        return promoType;
    }

    public void setPromoType(Integer promoType) {
        this.promoType = promoType;
    }

    @ApiModelProperty(hidden = true, value="Derived property, should not appear in form")
    public PromoTypeEnum getPromoTypeEnum() {
        return PromoTypeEnum.valueOf(getPromoType());
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

}
