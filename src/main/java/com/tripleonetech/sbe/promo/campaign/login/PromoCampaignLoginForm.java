package com.tripleonetech.sbe.promo.campaign.login;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import com.tripleonetech.sbe.promo.campaign.PromoCampaignForm;

public class PromoCampaignLoginForm extends PromoCampaignForm {
    
    @ApiModelProperty(name = "rules", value = "required login day rule")
    private List<PromoCampaignLoginRuleForm> rules;

    public List<PromoCampaignLoginRuleForm> getRules() {
        return rules;
    }

    public void setRules(List<PromoCampaignLoginRuleForm> rules) {
        this.rules = rules;
    }
}
