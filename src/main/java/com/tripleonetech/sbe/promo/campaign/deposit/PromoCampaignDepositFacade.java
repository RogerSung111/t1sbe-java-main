package com.tripleonetech.sbe.promo.campaign.deposit;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusService;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignPlayer;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignPlayerMapper;

@Component
public class PromoCampaignDepositFacade {

    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignDepositFacade.class);
    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    @Autowired
    private PromoCampaignDepositMapper promoCampaignDepositMapper;
    @Autowired
    private PromoCampaignPlayerMapper promoCampaignPlayerMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private BonusService bonusService;

    /**
     * 用 Approved 的 DepositRequest 來產生相對應 promo bonus, 並依設定自動加到主錢包
     */
    public void process(DepositRequest depositRequest) {
        Player player = playerMapper.get(depositRequest.getPlayerId());
        List<PromoCampaignDeposit> rules = promoCampaignDepositMapper
                .listEffectiveRules(depositRequest.getRequestedDate(), player.getCurrency());
        List<PromoCampaignPlayer> joinedMaps = promoCampaignPlayerMapper
                .list(depositRequest.getPlayerId());
        List<Integer> joinedPromoId = joinedMaps.stream().filter(m -> m
                .getPromoTypeEnum() == PromoTypeEnum.CAMPAIGN_DEPOSIT)
                .map(PromoCampaignPlayer::getCampaignId).collect(Collectors.toList());

        for (PromoCampaignDeposit rule : rules) {
            // auto_join = 1 的 rule 不會紀錄在 promo_campaign_player 中
            if (rule.isAutoJoin() || joinedPromoId.contains(rule.getId())) {
                if (!bonusService.isJoined(depositRequest.getPlayerId(), rule)) {
                    calculatePromoRequest(rule, depositRequest, player.getCampaignEnabled());
                }
            }
        }
    }

    /**
     * @param depositRequest
     *
     */
    private void calculatePromoRequest(PromoCampaignDeposit campaignRule, DepositRequest depositRequest, boolean campaignEnabled) {
        BigDecimal depositAmount = depositRequest.getAmount();
        if (campaignRule.getMinDeposit().compareTo(depositAmount) > 0) {
            return;
        }
        BigDecimal bonusAmount = BigDecimal.ZERO;
        if (Objects.nonNull(campaignRule.getFixedBonus())) {
            bonusAmount = bonusAmount.add(campaignRule.getFixedBonus());
        }
        if (Objects.nonNull(campaignRule.getPercentageBonus())) {
            bonusAmount = bonusAmount
                    .add(depositAmount.multiply(campaignRule.getPercentageBonus()).divide(ONE_HUNDRED));
        }
        if (Objects.nonNull(campaignRule.getMaxBonusAmount())) {
            bonusAmount = bonusAmount.min(campaignRule.getMaxBonusAmount());
        }

        PromoBonus promoBonus = new PromoBonus(depositRequest.getPlayerId(), campaignRule.getId(),
                PromoTypeEnum.CAMPAIGN_DEPOSIT,
                bonusAmount, depositAmount, DateUtils.getStartOfDay(), campaignRule.getWithdrawConditionMultiplier());
        // 如果 campaign_enabled 為 false 的時候，插入bonus並馬上設為rejected
        if (BooleanUtils.isFalse(campaignEnabled)) {
            promoBonus.setStatus(BonusStatusEnum.REJECTED);
            promoBonus.setComment("player's campaign status is inactive");
        }
        promoBonusMapper.insert(promoBonus);

        if (campaignEnabled && campaignRule.isBonusAutoRelease()) {
            bonusService.systemApprove(promoBonus);
        }
    }
}
