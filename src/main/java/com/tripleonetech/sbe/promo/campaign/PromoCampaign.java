package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.web.I18nText;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.player.tag.PlayerTagIdNameView;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusSetting;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class PromoCampaign extends BonusSetting implements PromoCampaignStatusData {
    @ApiModelProperty(value = "ID of the campaign")
    private Integer id;
    @ApiModelProperty(value = "Name of the campaign, i18n")
    private I18nText name;
    @ApiModelProperty(value = "Content of the campaign, i18n")
    private I18nText content;
    @ApiModelProperty(value = "Whether player need to click join button to join this campaign")
    private boolean autoJoin;
    @ApiModelProperty(value = "How often can player receive bonus")
    private BonusReceiveCycleEnum bonusReceiveCycle;
    @ApiModelProperty(value = "How many times can player receive bonus within one bonusReceiveCycle")
    private Integer maxBonusCountPerCycle;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "Configured start time for this campaign")
    private LocalDateTime startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "Actual start time for this campaign")
    private LocalDateTime effectiveStartTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "Configured end time for this campaign")
    private LocalDateTime endTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "Actual end time for this campaign")
    private LocalDateTime effectiveEndTime;
    @ApiModelProperty(value = "Campaign (approval) status: DRAFT(0), PENDING_APPROVAL(1), APPROVED(10), DELETED(20)")
    private CampaignApprovalStatusEnum status;
    @ApiModelProperty(value = "Campaign running status: PENDING(0), RUNNING(1), FINISHED(2)")
    private CampaignRunningStatusEnum runningStatus;
    @ApiModelProperty(value = "Comment text attached to this campaign record, such as reject reason")
    private String comment;
    @ApiModelProperty("Total number of bonuses granted by this campaign, including pending and approved")
    private int bonusCount;
    @ApiModelProperty("Total number of pending bonuses under this campaign")
    private int pendingBonusCount;
    @ApiModelProperty("Total number of players receiving bonus from this campaign")
    private int bonusPlayerCount;
    @JsonIgnore
    @ApiModelProperty(hidden = true, value="Player tags and player groups are accessed separately")
    private List<PlayerTagIdNameView> playerTagsAndGroups;
    @ApiModelProperty(value="Players applicable to this campaign")
    private List<PlayerIdView> players;
    @JsonIgnore
    private LocalDateTime createdAt;
    @JsonIgnore
    private LocalDateTime updatedAt;

    public abstract PromoTypeEnum getType();

    @ApiModelProperty(value = "For front-end to use as unique ID")
    public String getUid() {
        return String.format("%s-%s", getType().getCode(), getId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public I18nText getName() {
        if(name == null) {
            name = new I18nText();
        }
        return name;
    }

    public String getName(String language) {
        I18nText.I18nTextEntry entry = getName().stream().filter(e -> e.getLanguage().equalsIgnoreCase(language)).findFirst().orElse(null);
        if(entry == null) {
            return null;
        }
        return entry.getText();
    }

    public void setName(I18nText name) {
        this.name = name;
    }

    public void setName(String language, String name) {
        getName().add(language, name);
    }

    // Temp virtual property to set i18n
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public void setI18nName(String name) {
        setName("zh-CN", name);
    }

    public I18nText getContent() {
        if(content == null){
            content = new I18nText();
        }
        return content;
    }

    public String getContent(String language) {
        I18nText.I18nTextEntry entry = getContent().stream().filter(e -> e.getLanguage().equalsIgnoreCase(language)).findFirst().orElse(null);
        if(entry == null) {
            return null;
        }
        return entry.getText();
    }

    public void setContent(I18nText content) {
        this.content = content;
    }

    public void setContent(String language, String content) {
        getContent().add(language, content);
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public void setI18nContent(String content) {
        setContent("zh-CN", content);
    }

    public boolean isAutoJoin() {
        return autoJoin;
    }

    public void setAutoJoin(boolean autoJoin) {
        this.autoJoin = autoJoin;
    }

    public BonusReceiveCycleEnum getBonusReceiveCycle() {
        return bonusReceiveCycle;
    }

    public void setBonusReceiveCycle(BonusReceiveCycleEnum bonusReceiveCycle) {
        this.bonusReceiveCycle = bonusReceiveCycle;
    }

    public Integer getMaxBonusCountPerCycle() {
        return maxBonusCountPerCycle;
    }

    public void setMaxBonusCountPerCycle(Integer maxBonusCountPerCycle) {
        this.maxBonusCountPerCycle = maxBonusCountPerCycle;
    }

    @Override
    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getEffectiveStartTime() {
        if(effectiveStartTime == null && startTime.isBefore(LocalDateTime.now())) {
            return startTime;
        }
        return effectiveStartTime;
    }

    public void setEffectiveStartTime(LocalDateTime effectiveStartTime) {
        this.effectiveStartTime = effectiveStartTime;
    }

    public LocalDateTime getEffectiveEndTime() {
        if(effectiveEndTime == null && endTime != null && endTime.isBefore(LocalDateTime.now())) {
            return endTime;
        }
        return effectiveEndTime;
    }

    public void setEffectiveEndTime(LocalDateTime effectiveEndTime) {
        this.effectiveEndTime = effectiveEndTime;
    }

    @Override
    public CampaignApprovalStatusEnum getStatus() {
        return status;
    }

    public void setStatus(CampaignApprovalStatusEnum status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return CampaignApprovalStatusEnum.DELETED == getStatus();
    }

    // Derived property: running status
    public CampaignRunningStatusEnum getRunningStatus() {
        return PromoCampaignUtils.getRunningStatus(this);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getBonusCount() {
        return bonusCount;
    }

    public void setBonusCount(int bonusCount) {
        this.bonusCount = bonusCount;
    }

    public int getPendingBonusCount() {
        return pendingBonusCount;
    }

    public void setPendingBonusCount(int pendingBonusCount) {
        this.pendingBonusCount = pendingBonusCount;
    }

    public int getBonusPlayerCount() {
        return bonusPlayerCount;
    }

    public void setBonusPlayerCount(int bonusPlayerCount) {
        this.bonusPlayerCount = bonusPlayerCount;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PlayerTagIdNameView> getPlayerTagsAndGroups() {
        // MyBatis collection will insert an empty object to the list when dealing with null left join
        if(playerTagsAndGroups == null || playerTagsAndGroups.size() == 1 && playerTagsAndGroups.get(0).getName() == null){
            playerTagsAndGroups = new ArrayList<>();
        }
        return playerTagsAndGroups;
    }

    public void setPlayerTagsAndGroups(List<PlayerTagIdNameView> playerTagsAndGroups) {
        this.playerTagsAndGroups = playerTagsAndGroups;
    }

    // Obtain player tags and groups
    public List<IdNameView> getPlayerTags() {
        return getPlayerTagsAndGroups().stream().filter(e -> !e.isGroup()).collect(Collectors.toList());
    }

    public List<IdNameView> getPlayerGroups() {
        return getPlayerTagsAndGroups().stream().filter(e -> e.isGroup()).collect(Collectors.toList());
    }

    public List<PlayerIdView> getPlayers() {
        // MyBatis collection will insert an empty object to the list when dealing with null left join
        if(players == null || players.size() == 1 && players.get(0).getUsername() == null){
            players = new ArrayList<>();
        }
        return players;
    }

    public void setPlayers(List<PlayerIdView> players) {
        this.players = players;
    }
}
