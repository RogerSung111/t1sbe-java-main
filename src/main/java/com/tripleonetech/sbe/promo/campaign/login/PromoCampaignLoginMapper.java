package com.tripleonetech.sbe.promo.campaign.login;

import org.apache.ibatis.annotations.Mapper;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignMapper;

@Mapper
public interface PromoCampaignLoginMapper extends PromoCampaignMapper<PromoCampaignLogin> {

    PromoCampaignLogin queryAvailableRule(SiteCurrencyEnum currency);
}
