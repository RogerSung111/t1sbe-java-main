package com.tripleonetech.sbe.promo.campaign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PromoCampaignPlayerService {
    @Autowired
    private PromoCampaignListMapper mapper;

    public List<PromoCampaignPlayerView> getLiveCampaignsForPlayer(int playerId) {
        List<PromoCampaignPlayerView> campaigns = mapper.queryLiveForPlayer(playerId);
        return campaigns;
    }
}
