package com.tripleonetech.sbe.promo.campaign.task;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;

import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaign;

public class PromoCampaignTask extends PromoCampaign {
    private BigDecimal fixedBonus;
    /**
     * Column: task_type
     * Remark: 0-EmailVerificationBonus, 1-SmsVerificationBonus, 2-IdentityVerificationBonus
     */
    private PromoCampaignTaskTypeEnum taskType;

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public PromoCampaignTaskTypeEnum getTaskType() {
        return taskType;
    }

    public void setTaskType(PromoCampaignTaskTypeEnum taskType) {
        this.taskType = taskType;
    }

    @Override
    public PromoTypeEnum getType() {
        return PromoTypeEnum.CAMPAIGN_TASK;
    }

    public static PromoCampaignTask from(PromoCampaignTaskForm form) {
        PromoCampaignTask taskCampaign = new PromoCampaignTask();
        BeanUtils.copyProperties(form, taskCampaign);
        taskCampaign.setBonusReceiveCycle(form.getBonusReceiveCycleEnum());
        taskCampaign.setTaskType(form.getTaskTypeEnum());
        return taskCampaign;
    }
}
