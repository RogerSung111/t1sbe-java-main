package com.tripleonetech.sbe.promo.campaign.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.player.VerifyIdentitySuccessEvent;

@Profile("listener")
@Component
public class PromoCampaignTaskIdentityVerificationListener implements ApplicationListener<VerifyIdentitySuccessEvent> {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignTaskEmailVerificationListener.class);

    @Autowired
    private PromoCampaignTaskIdentityVerificationFacade promoCampaignTaskIdentityVerificationFacade;

    @Async
    @Override
    public void onApplicationEvent(VerifyIdentitySuccessEvent event) {
        logger.info("Email verification success event：" + event.getSource());
        promoCampaignTaskIdentityVerificationFacade.calculatePromo(event);
    }
}
