package com.tripleonetech.sbe.promo.campaign;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PromoCampaignPlayerMapper {

    int insertBatch(List<PromoCampaignPlayer> list);

    int insert(PromoCampaignPlayer record);

    List<PromoCampaignPlayer> list(Integer playerId);

}
