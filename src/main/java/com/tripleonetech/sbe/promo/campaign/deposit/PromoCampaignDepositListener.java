package com.tripleonetech.sbe.promo.campaign.deposit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.payment.ApprovedDepositEvent;
import com.tripleonetech.sbe.payment.DepositRequest;

@Profile("listener")
@Component
public class PromoCampaignDepositListener {

    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignDepositListener.class);
    @Autowired
    private PromoCampaignDepositFacade promoCampaignDepositFacade;

    /**
     * 用 Approved 的 DepositRequest 來產生相對應 promo bonus
     */
    @Async
    @EventListener
    public void listen(ApprovedDepositEvent event) {
        DepositRequest depositRequest = (DepositRequest) event.getSource();
        promoCampaignDepositFacade.process(depositRequest);
    }

}