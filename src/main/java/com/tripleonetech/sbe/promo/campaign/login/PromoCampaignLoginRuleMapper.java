package com.tripleonetech.sbe.promo.campaign.login;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PromoCampaignLoginRuleMapper {
    List<PromoCampaignLoginRule> selectByCampaignId(@Param("campaignId") Integer campaignId);

    Integer insert(@Param("records")List<PromoCampaignLoginRule> records);

    Integer update(PromoCampaignLoginRule record);

    Boolean delete(Long campaignId, Byte loginDay);
}
