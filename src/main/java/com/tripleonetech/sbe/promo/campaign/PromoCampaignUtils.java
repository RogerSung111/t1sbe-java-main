package com.tripleonetech.sbe.promo.campaign;

import java.time.LocalDateTime;

public class PromoCampaignUtils {
    // Logic to determine running status based on campaign's various properties
    public static CampaignRunningStatusEnum getRunningStatus(PromoCampaignStatusData campaign) {
        switch(campaign.getStatus()) {
            case DRAFT:
            case DELETED:
            case PENDING_APPROVAL:
                return CampaignRunningStatusEnum.PENDING;

            case APPROVED:
                LocalDateTime theStartTime = campaign.getEffectiveStartTime() != null ? campaign.getEffectiveStartTime() : campaign.getStartTime();
                LocalDateTime theEndTime = campaign.getEffectiveEndTime() != null ? campaign.getEffectiveEndTime() : campaign.getEndTime();
                LocalDateTime now = LocalDateTime.now();
                if(theEndTime == null || theEndTime.isAfter(now)) {
                    if(theStartTime.isBefore(now)) {
                        return CampaignRunningStatusEnum.RUNNING;
                    } else {
                        return CampaignRunningStatusEnum.PENDING;
                    }
                } else { // theEndTime < now
                    return CampaignRunningStatusEnum.FINISHED;
                }
        }
        return null;
    }
}
