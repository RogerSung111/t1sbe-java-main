package com.tripleonetech.sbe.promo.campaign;

import io.swagger.annotations.ApiModelProperty;

// List of campaigns, used by BO, includes statistics
public class PromoCampaignListView extends PromoCampaignBaseView implements PromoCampaignStatusData {
    @ApiModelProperty("Campaign's approval status: DRAFT(0), PENDING_APPROVAL(1), REJECTED(9), APPROVED(10), DELETED(20)")
    private CampaignApprovalStatusEnum status;
    @ApiModelProperty("Total number of bonuses granted by this campaign, including pending and approved")
    private int bonusCount;
    @ApiModelProperty("Total number of pending bonuses under this campaign")
    private int pendingBonusCount;
    @ApiModelProperty("Total number of players receiving bonus from this campaign")
    private int bonusPlayerCount;

    @Override
    public CampaignApprovalStatusEnum getStatus() {
        return status;
    }

    public void setStatus(CampaignApprovalStatusEnum status) {
        this.status = status;
    }

    // Derived property: running status
    @ApiModelProperty("Campaign's running status: PENDING(0), RUNNING(1), FINISHED(2)")
    public CampaignRunningStatusEnum getRunningStatus() {
        return PromoCampaignUtils.getRunningStatus(this);
    }

    public int getBonusCount() {
        return bonusCount;
    }

    public void setBonusCount(int bonusCount) {
        this.bonusCount = bonusCount;
    }

    public int getPendingBonusCount() {
        return pendingBonusCount;
    }

    public void setPendingBonusCount(int pendingBonusCount) {
        this.pendingBonusCount = pendingBonusCount;
    }

    public int getBonusPlayerCount() {
        return bonusPlayerCount;
    }

    public void setBonusPlayerCount(int bonusPlayerCount) {
        this.bonusPlayerCount = bonusPlayerCount;
    }
}
