package com.tripleonetech.sbe.promo.campaign.task;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignForm;

public class PromoCampaignTaskForm extends PromoCampaignForm {
    @ApiModelProperty(value = "Fixed bonus amount for this campaign. This campaign only supports fixed bonus.")
    private BigDecimal fixedBonus;
    @ApiModelProperty(value = "Type of campaign task: " +
            "EmailVerificationBonus(0), " +
            "SmsVerificationBonus(1), " +
            "IdentityVerificationBonus(2)")
    private Integer taskType;

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true, value="Derived property")
    public PromoCampaignTaskTypeEnum getTaskTypeEnum() {
        if(getTaskType() == null) {
            return null;
        }
        return PromoCampaignTaskTypeEnum.valueOf(getTaskType());
    }
}
