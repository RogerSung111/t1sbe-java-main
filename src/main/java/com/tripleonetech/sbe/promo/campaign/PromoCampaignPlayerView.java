package com.tripleonetech.sbe.promo.campaign;

import io.swagger.annotations.ApiModelProperty;

// The campaign data structure to return to players. It includes addition properties.
public class PromoCampaignPlayerView extends PromoCampaignBaseView {
    @ApiModelProperty(value = "Whether current player already joined this campaign. Note if autoJoin is true, this will always be true too")
    private boolean joined;
    @ApiModelProperty(value = "Gives the id of last bonus granted (approved) from this campaign. If null, means no bonus has been granted yet.")
    private Integer lastBonusId;

    public boolean isJoined() {
        if(isAutoJoin()){
            return true;
        }
        return joined;
    }

    public void setJoined(boolean joined) {
        this.joined = joined;
    }

    public Integer getLastBonusId() {
        return lastBonusId;
    }

    public void setLastBonusId(Integer lastBonusId) {
        this.lastBonusId = lastBonusId;
    }

    @ApiModelProperty(value = "Whether there are more bonus available from this campaign.")
    public boolean getHasMoreBonus() {
        return !(getBonusReceiveCycle() == BonusReceiveCycleEnum.ONCE && getLastBonusId() != null);
    }

}
