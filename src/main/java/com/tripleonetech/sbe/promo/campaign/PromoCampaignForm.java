package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.web.I18nText;
import com.tripleonetech.sbe.promo.bonus.BonusSetting;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PromoCampaignForm extends BonusSetting {
    @ApiModelProperty(value= "Name of this campaign")
    private I18nText name;
    @ApiModelProperty(value= "Content (description) of this campaign")
    private I18nText content;
    @ApiModelProperty(value= "Whether user need to actively join this campaign")
    private boolean autoJoin;
    @ApiModelProperty(value= "Bonus receive cycle and max bonus count per cycle together defines how often bonus can be awarded." +
            "Available values: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5)")
    private Integer bonusReceiveCycle;
    @ApiModelProperty(value= "Bonus receive cycle and max bonus count per cycle together defines how often bonus can be awarded")
    private Integer maxBonusCountPerCycle;
    @ApiModelProperty(value= "Campaign start time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;
    @ApiModelProperty(value= "Campaign end time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;
    @ApiModelProperty(value = "Applicable player tag IDs")
    private List<Integer> playerTagIds;
    @ApiModelProperty(value = "Applicable player group IDs")
    private List<Integer> playerGroupIds;
    @ApiModelProperty(value = "Applicable player IDs (Note: Front-end will pass in player credential ID, will be converted to player ID upon saving)")
    private List<Integer> playerIds;

    public I18nText getName() {
        if(name == null) {
            name = new I18nText();
        }
        return name;
    }

    public String getName(String language) {
        I18nText.I18nTextEntry entry = getName().stream().filter(e -> e.getLanguage().equalsIgnoreCase(language)).findFirst().get();
        if(entry == null) {
            return null;
        }
        return entry.getText();
    }

    public void setName(I18nText name) {
        this.name = name;
    }

    public void setName(String language, String name) {
        getName().add(language, name);
    }

    public I18nText getContent() {
        if(content == null){
            content = new I18nText();
        }
        return content;
    }

    public String getContent(String language) {
        I18nText.I18nTextEntry entry = getContent().stream().filter(e -> e.getLanguage().equalsIgnoreCase(language)).findFirst().get();
        if(entry == null) {
            return null;
        }
        return entry.getText();
    }

    public void setContent(I18nText content) {
        this.content = content;
    }

    public void setContent(String language, String content) {
        getContent().add(language, content);
    }

    public boolean isAutoJoin() {
        return autoJoin;
    }

    public void setAutoJoin(boolean autoJoin) {
        this.autoJoin = autoJoin;
    }

    public Integer getBonusReceiveCycle() {
        return bonusReceiveCycle;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true, value="Derived property")
    public BonusReceiveCycleEnum getBonusReceiveCycleEnum() {
        if(getBonusReceiveCycle() == null){
            return null;
        }
        return BonusReceiveCycleEnum.valueOf(getBonusReceiveCycle());
    }

    public void setBonusReceiveCycle(Integer bonusReceiveCycle) {
        this.bonusReceiveCycle = bonusReceiveCycle;
    }

    public Integer getMaxBonusCountPerCycle() {
        return maxBonusCountPerCycle;
    }

    public void setMaxBonusCountPerCycle(Integer maxBonusCountPerCycle) {
        this.maxBonusCountPerCycle = maxBonusCountPerCycle;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<Integer> getPlayerTagIds() {
        if(playerTagIds == null){
            playerTagIds = new ArrayList<>();
        }
        return playerTagIds;
    }

    public void setPlayerTagIds(List<Integer> playerTagIds) {
        this.playerTagIds = playerTagIds;
    }

    public List<Integer> getPlayerGroupIds() {
        if(playerGroupIds == null){
            playerGroupIds = new ArrayList<>();
        }
        return playerGroupIds;
    }

    public void setPlayerGroupIds(List<Integer> playerGroupIds) {
        this.playerGroupIds = playerGroupIds;
    }

    @ApiModelProperty(hidden = true, value="Derived property, return all tags and groups IDs")
    public List<Integer> getPlayerTagIdsAndGroupIds() {
        return new ArrayList<Integer>() {{
            addAll(getPlayerTagIds());
            addAll(getPlayerGroupIds());
        }};
    }

    public List<Integer> getPlayerIds() {
        if(playerIds == null) {
            playerIds = new ArrayList<>();
        }
        return playerIds;
    }

    public void setPlayerIds(List<Integer> playerIds) {
        this.playerIds = playerIds;
    }
}
