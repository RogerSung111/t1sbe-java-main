package com.tripleonetech.sbe.promo.campaign.task;

import org.apache.ibatis.annotations.Mapper;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignMapper;

@Mapper
public interface PromoCampaignTaskMapper extends PromoCampaignMapper<PromoCampaignTask> {
    PromoCampaignTask queryAvailableRule(Integer taskType, SiteCurrencyEnum currency);
}
