package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum BonusReceiveCycleEnum implements BaseCodeEnum {
    UNLIMITED(0), ONCE(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5);

    private int code;

    BonusReceiveCycleEnum(int code) {
        this.code = code;
    }

    @JsonValue
    @Override
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static BonusReceiveCycleEnum valueOf(Integer code) {
        BonusReceiveCycleEnum[] enumConstants = BonusReceiveCycleEnum.values();
        for (BonusReceiveCycleEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
