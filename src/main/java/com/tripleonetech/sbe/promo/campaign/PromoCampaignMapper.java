package com.tripleonetech.sbe.promo.campaign;

import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;


public interface PromoCampaignMapper<T extends PromoCampaign> {
    T get(int id);

    int insert(T promo);

    int update(T promo);

    // -- approval status changes --
    int delete(int id);

    int submit(int id); // submit for approval

    int reject(@Param("id") int id, @Param("reason") String reason);

    int approve(@Param("id") int id, @Param("effectiveStartTime")LocalDateTime effectiveStartTime);

    // -- running status changes --
    int start(int id);

    int terminate(int id);
}
