package com.tripleonetech.sbe.promo.campaign.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.player.PlayerLoginSuccessEvent;

@Profile("listener")
@Component
public class PromoCampaignLoginListener {
    @Autowired
    private PromoCampaignLoginFacade promoCampaignLoginFacade;

    @Async
    @EventListener
    public void listen(PlayerLoginSuccessEvent event) {
        promoCampaignLoginFacade.calculatePromo(event);
    }
}
