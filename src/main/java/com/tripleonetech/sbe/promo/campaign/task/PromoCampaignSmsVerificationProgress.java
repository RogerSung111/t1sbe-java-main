package com.tripleonetech.sbe.promo.campaign.task;

public class PromoCampaignSmsVerificationProgress {
    private Boolean phoneVerified;

    public Boolean getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(Boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public PromoCampaignSmsVerificationProgress(Boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }
}
