package com.tripleonetech.sbe.promo.campaign;

import java.time.LocalDateTime;

public interface PromoCampaignStatusData {
    CampaignApprovalStatusEnum getStatus();
    LocalDateTime getStartTime();
    LocalDateTime getEndTime();
    LocalDateTime getEffectiveStartTime();
    LocalDateTime getEffectiveEndTime();
}
