package com.tripleonetech.sbe.promo.campaign.task;

public class PromoCampaignEmailVerificationProgress {
    private Boolean emailVerified;

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public PromoCampaignEmailVerificationProgress(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }
}
