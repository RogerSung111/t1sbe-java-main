package com.tripleonetech.sbe.promo.campaign.task;

import java.time.ZoneId;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusService;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.BonusReceiveCycleEnum;

@Component
public class PromoCampaignTaskIdentityVerificationFacade {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignTaskIdentityVerificationFacade.class);

    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PromoCampaignTaskMapper campaignTaskMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private PlayerMapper playerMapper;

    private static final ZoneId zoneId = ZoneId.of("UTC");

    public void calculatePromo(VerifyIdentitySuccessEvent event) {
        // TODO: update this snippet to adapt to uploading Doc/Identity verfication promo(Player Status is temporarily being used for determination)
        PlayerStatusEnum playerStatusEnum = PlayerStatusEnum.BLOCKED; //playerProfileMapper.getByPlayerId(event.getPlayerId()).getStatus();

        Player player = playerMapper.get(event.getPlayerId());
        PromoCampaignTask promoSetting = campaignTaskMapper
                .queryAvailableRule(PromoCampaignTaskTypeEnum.IdentityVerificationBonus.getCode(), player.getCurrency());

        if (promoSetting.getId() != null) {
            // Identity verification promo campaign is a one time promo
            // Check for existing PENDING_APPROVAL, APPROVED and REJECTED bonus
            boolean isJoined = bonusService.isJoined(event.getPlayerId(), promoSetting);

            PromoBonus addPromoBonus = new PromoBonus(event.getPlayerId(), promoSetting.getId(),
                    PromoTypeEnum.CAMPAIGN_TASK, promoSetting.getFixedBonus(), null,
                    DateUtils.getStartOfDay(), promoSetting.getWithdrawConditionMultiplier());
            if (playerStatusEnum.equals(PlayerStatusEnum.ACTIVE) && !isJoined) {
                // 如果 campaign_enabled 為 false 的時候，插入bonus並馬上設為rejected
                if (BooleanUtils.isFalse(player.getCampaignEnabled())) {
                    addPromoBonus.setStatus(BonusStatusEnum.REJECTED);
                    addPromoBonus.setComment("player's campaign status is inactive");
                    promoBonusMapper.insert(addPromoBonus);
                    return;
                }
                promoBonusMapper.insert(addPromoBonus);
                bonusService.systemApprove(addPromoBonus);
            } else if (isJoined) {
                logger.info(
                        "Bonus already existed in playerId : {}, promoType: {}, ruleId: {}, ReceiveCycle: {}, maxBonusCountPerCycle: {}",
                        event.getPlayerId(), PromoTypeEnum.CAMPAIGN_TASK, promoSetting.getId(),
                        BonusReceiveCycleEnum.ONCE, 1);
            } else {
                logger.info("Failed inserting PromoBonus: {}, Player's Status: {}",
                        addPromoBonus, playerStatusEnum);
            }
        } else {
            logger.info("No available IdentityVerification promo campaign found!");
        }

    }

}
