package com.tripleonetech.sbe.promo.campaign.deposit;

import com.tripleonetech.sbe.promo.campaign.PromoCampaignForm;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class PromoCampaignDepositForm extends PromoCampaignForm {
    @ApiModelProperty(value= "Fixed bonus for this deposit campaign")
    private BigDecimal fixedBonus;
    @ApiModelProperty(value= "Percentage bonus for this deposit campaign")
    private BigDecimal percentageBonus;
    @NotNull
    @ApiModelProperty(value= "Mininum deposit amount that triggers this campaign")
    private BigDecimal minDeposit;
    @ApiModelProperty(value= "Maximum bonus amount this campaign can grant, can be empty if fixedBonus is used")
    private BigDecimal maxBonusAmount;

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public BigDecimal getPercentageBonus() {
        return percentageBonus;
    }

    public void setPercentageBonus(BigDecimal percentageBonus) {
        this.percentageBonus = percentageBonus;
    }

    public BigDecimal getMinDeposit() {
        return minDeposit;
    }

    public void setMinDeposit(BigDecimal minDeposit) {
        this.minDeposit = minDeposit;
    }

    public BigDecimal getMaxBonusAmount() {
        return maxBonusAmount;
    }

    public void setMaxBonusAmount(BigDecimal maxBonusAmount) {
        this.maxBonusAmount = maxBonusAmount;
    }
}
