package com.tripleonetech.sbe.promo.campaign.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PromoCampaignTaskTypeEnum implements BaseCodeEnum {
    EmailVerificationBonus(0),
    SmsVerificationBonus(1),
    IdentityVerificationBonus(2);
    private int code;

    PromoCampaignTaskTypeEnum(int code) {
        this.code = code;
    }

    @JsonValue
    @Override
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static PromoCampaignTaskTypeEnum valueOf(Integer code) {
        PromoCampaignTaskTypeEnum[] enumConstants = PromoCampaignTaskTypeEnum.values();
        for (PromoCampaignTaskTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }

}
