package com.tripleonetech.sbe.promo.campaign.deposit;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignMapper;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface PromoCampaignDepositMapper extends PromoCampaignMapper<PromoCampaignDeposit> {
    List<PromoCampaignDeposit> listEffectiveRules(LocalDateTime requestedDate, SiteCurrencyEnum currency);
}
