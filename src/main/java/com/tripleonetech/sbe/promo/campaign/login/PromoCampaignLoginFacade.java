package com.tripleonetech.sbe.promo.campaign.login;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerLoginSuccessEvent;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.*;
import com.tripleonetech.sbe.report.player.PlayerActivityReportMapper;

@Component
public class PromoCampaignLoginFacade {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignLoginFacade.class);

    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PromoCampaignLoginMapper campaignLoginMapper;

    public void calculatePromo(PlayerLoginSuccessEvent event) {
        Player player = playerMapper.get(event.getPlayerId());

        PromoCampaignLogin promoSetting = campaignLoginMapper.queryAvailableRule(player.getCurrency());

        if (promoSetting != null) {

            PromoBonusQueryForm promoBonusQueryForm = new PromoBonusQueryForm();
            promoBonusQueryForm.setPlayerId(event.getPlayerId());
            List<Integer> promoTypeEnums = new ArrayList<>();
            promoTypeEnums.add(promoSetting.getType().getCode());
            promoBonusQueryForm.setPromoTypes(promoTypeEnums);
            promoBonusQueryForm.setRuleId(promoSetting.getId());

            List<PromoCampaignLoginRule> campaignLoginRules = promoSetting.getRules();

            Byte lastTierLoginDay = promoSetting.getRules().stream()
                    .sorted(Comparator.comparing(PromoCampaignLoginRule::getLoginDay).reversed()).collect(
                            Collectors.toList())
                    .get(0).getLoginDay();

            int offSetMinute = ZonedDateTime.now(ZoneId.of(Constant.getInstance().getTimezone())).getOffset()
                    .getTotalSeconds() / 60;

            Integer playerLogin = playerActivityReportMapper.getConsecutiveLoginCountByPlayerId(event.getPlayerId(),
                    promoSetting.getStartTime().toString(), offSetMinute);

            Byte requiredLoginDay;

            if (lastTierLoginDay <= playerLogin) {
                requiredLoginDay = lastTierLoginDay;
            } else {
                Optional<PromoCampaignLoginRule> rule = campaignLoginRules.stream()
                        .filter(x -> x.getLoginDay().equals(playerLogin.byteValue())).findFirst();
                if (rule.isPresent()) {
                    requiredLoginDay = rule.get().getLoginDay();
                } else {
                    requiredLoginDay = campaignLoginRules.stream().filter(x -> x.getLoginDay() > playerLogin)
                            .findFirst().map(PromoCampaignLoginRule::getLoginDay).orElse(null);
                    logger.debug(
                            "Failed inserting PromoBonus, player valid login day count: {}, required login date count: {}",
                            playerLogin, requiredLoginDay);
                    return;
                }
            }

            boolean isJoined = !promoBonusMapper
                    .queryExistsBonus(event.getPlayerId(), promoSetting.getType(), promoSetting.getId(),
                            DateUtils.getStartOfDay(), playerLogin)
                    .isEmpty();

            if (!isJoined) {
                // inserting bonus...
                PromoBonus addPromoBonus = new PromoBonus(promoBonusQueryForm.getPlayerId(), promoSetting.getId(),
                        PromoTypeEnum.CAMPAIGN_LOGIN, promoSetting.getFixedBonus(requiredLoginDay), null,
                        DateUtils.getStartOfDay(), promoSetting.getWithdrawConditionMultiplier());
                addPromoBonus.setLoginDay(playerLogin);
                String comment = String.format("Login Campaign Bonus for: [%s] to [%s]",
                        DateUtils.getStartOfDay().minusDays(playerLogin - 1), DateUtils.getStartOfDay().plusDays(1));
                addPromoBonus.setComment(comment);

                // 如果 campaign_enabled 為 false 的時候，插入bonus並馬上設為rejected
                if (BooleanUtils.isFalse(player.getCampaignEnabled())) {
                    addPromoBonus.setStatus(BonusStatusEnum.REJECTED);
                    addPromoBonus.setComment("player's campaign status is inactive");
                    promoBonusMapper.insert(addPromoBonus);
                    return;
                }

                promoBonusMapper.insert(addPromoBonus);
                bonusService.systemApprove(addPromoBonus);
                logger.debug(
                        "Successfully inserted and released bonus: {}, player valid login day count: {}, required login date count: {}",
                        addPromoBonus, playerLogin, requiredLoginDay);

            } else {
                logger.debug("Bonus already existed");
            }

        } else {
            logger.debug("No available Login Campaign found!");
        }
    }
}
