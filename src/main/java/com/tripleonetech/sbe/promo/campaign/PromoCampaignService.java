package com.tripleonetech.sbe.promo.campaign;

import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLogin;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginMapper;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginRule;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginRuleMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PromoCampaignService {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignService.class);

    @Autowired
    private PromoCampaignLoginMapper promoCampaignLoginMapper;
    @Autowired
    private PromoCampaignLoginRuleMapper campaignLoginRuleMapper;

    /**
     * update login campaign & requiredLoginDay rules
     *
     * @param promoCampaignLogin
     *
     * @return true/false (success, failure)
     * @author YU
     */
    public void updatePromoCampaignLogin(PromoCampaignLogin promoCampaignLogin) {
        promoCampaignLoginMapper.update(promoCampaignLogin);

        for (PromoCampaignLoginRule rule : CollectionUtils.emptyIfNull(promoCampaignLogin.getRules())) {
            campaignLoginRuleMapper.update(rule);
        }
    }

    /**
     * insert new login campaign & requiredLoginDay rules
     *
     * @param promoCampaignLogin
     * 
     * @return true/false (success, failure)
     * @author YU
     */
    public boolean insertPromoCampaignLoginWithRules(PromoCampaignLogin promoCampaignLogin) {
        if (!CollectionUtils.isEmpty(promoCampaignLogin.getRules())) {
            promoCampaignLoginMapper.insert(promoCampaignLogin);

            if (promoCampaignLogin.getId() > 0) {
                List<PromoCampaignLoginRule> campaignLoginRules = new ArrayList<>();
                for (PromoCampaignLoginRule rule : promoCampaignLogin.getRules()) {
                    rule.setCampaignId(promoCampaignLogin.getId());
                    campaignLoginRules.add(rule);
                }

                campaignLoginRuleMapper.insert(campaignLoginRules);
                return true;
            } else {
                logger.error("campaignId not found, insert PromoCampaignLogin Failed!");
            }
        } else {
            logger.error("requiredLoginDay rules cannot be null");
        }
        return false;
    }
}
