package com.tripleonetech.sbe.promo.campaign;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

// This mapper provides list of campaigns across different types
@Mapper
public interface PromoCampaignListMapper {
    List<PromoCampaignListView> query(@Param("query") PromoCampaignListQueryForm query,
                                      @Param("pageNum") Integer pageNum,
                                      @Param("pageSize") Integer pageSize);

    List<PromoCampaignListView> queryCompleted(@Param("query") PromoCampaignListQueryForm query,
                                      @Param("pageNum") Integer pageNum,
                                      @Param("pageSize") Integer pageSize);

    List<PromoCampaignPlayerView> queryLiveForPlayer(int playerId);

    int getPendingCount();
}
