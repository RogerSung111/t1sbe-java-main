package com.tripleonetech.sbe.promo.campaign;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum CampaignApprovalStatusEnum implements BaseCodeEnum {
    DRAFT(0), // Initial status. To PENDING_APPROVAL via 'submit-to-approval'; To DELETED via 'delete'
    PENDING_APPROVAL(1), // To APPROVED via 'approve'; To DRAFT via 'edit'; To REJECTED via 'reject'
    REJECTED(9), // To DRAFT via 'edit'; To DELETED via 'delete'
    APPROVED(10), // This is a final status
    DELETED(20); // This is a final status

    private int code;

    CampaignApprovalStatusEnum(int code) {
        this.code = code;
    }

    @JsonValue
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static CampaignApprovalStatusEnum valueOf(Integer code) {
        CampaignApprovalStatusEnum[] enumConstants = CampaignApprovalStatusEnum.values();
        for (CampaignApprovalStatusEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
