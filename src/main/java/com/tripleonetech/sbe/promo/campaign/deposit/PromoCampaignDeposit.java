package com.tripleonetech.sbe.promo.campaign.deposit;

import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.PromoCampaign;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

public class PromoCampaignDeposit extends PromoCampaign {
    private BigDecimal fixedBonus;
    private BigDecimal percentageBonus;
    private BigDecimal minDeposit;
    private BigDecimal maxBonusAmount;

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public BigDecimal getPercentageBonus() {
        return percentageBonus;
    }

    public void setPercentageBonus(BigDecimal percentageBonus) {
        this.percentageBonus = percentageBonus;
    }

    public BigDecimal getMinDeposit() {
        return minDeposit;
    }

    public void setMinDeposit(BigDecimal minDeposit) {
        this.minDeposit = minDeposit;
    }

    public BigDecimal getMaxBonusAmount() {
        return maxBonusAmount;
    }

    public void setMaxBonusAmount(BigDecimal maxBonusAmount) {
        this.maxBonusAmount = maxBonusAmount;
    }

    @Override
    public PromoTypeEnum getType() {
        return PromoTypeEnum.CAMPAIGN_DEPOSIT;
    }

    public static PromoCampaignDeposit from(PromoCampaignDepositForm form) {
        PromoCampaignDeposit depositCampaign = new PromoCampaignDeposit();
        BeanUtils.copyProperties(form, depositCampaign);
        // Form is updating bonus value
        boolean useFixedBonus = (form.getFixedBonus() != null);
        if(useFixedBonus) {
            depositCampaign.setPercentageBonus(null);
        } else {
            depositCampaign.setFixedBonus(null);
        }
        depositCampaign.setBonusReceiveCycle(form.getBonusReceiveCycleEnum());
        return depositCampaign;
    }
}
