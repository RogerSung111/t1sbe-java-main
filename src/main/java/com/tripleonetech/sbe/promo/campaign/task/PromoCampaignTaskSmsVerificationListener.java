package com.tripleonetech.sbe.promo.campaign.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.sms.history.SmsOtpValidationSuccessEvent;

@Profile("listener")
@Component
public class PromoCampaignTaskSmsVerificationListener implements ApplicationListener<SmsOtpValidationSuccessEvent> {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignTaskSmsVerificationListener.class);

    @Autowired
    private PromoCampaignTaskSmsVerificationFacade promoCampaignTaskSmsVerificationFacade;

    @Async
    @Override
    public void onApplicationEvent(SmsOtpValidationSuccessEvent event) {
        logger.info("SMS verification success event：" + event.getSource());
        promoCampaignTaskSmsVerificationFacade.calculatePromo(event);
    }
}
