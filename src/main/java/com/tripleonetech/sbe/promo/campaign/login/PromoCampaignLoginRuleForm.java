package com.tripleonetech.sbe.promo.campaign.login;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class PromoCampaignLoginRuleForm {
    @ApiModelProperty(name = "loginDay" , value = "required login day", required = true)
    private Byte loginDay;
    @ApiModelProperty(name = "fixedBonus", value = "Fixed bonus amount for specified login day", required = true)
    private BigDecimal fixedBonus;

    public Byte getLoginDay() {
        return loginDay;
    }

    public void setLoginDay(Byte loginDay) {
        this.loginDay = loginDay;
    }

    public BigDecimal getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(BigDecimal fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public PromoCampaignLoginRuleForm(Byte loginDay, BigDecimal fixedBonus) {
        this.loginDay = loginDay;
        this.fixedBonus = fixedBonus;
    }

    public PromoCampaignLoginRuleForm() {
    }
}
