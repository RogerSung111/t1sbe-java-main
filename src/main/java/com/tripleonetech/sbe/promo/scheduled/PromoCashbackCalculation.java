package com.tripleonetech.sbe.promo.scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.PlayerMessageTemplate.PlayerMessageTemplateProperty;
import com.tripleonetech.sbe.player.message.SystemMessageService;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusStatusEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.cashback.*;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Promo.Properties.AMOUNT;
import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.PROMO;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Component
public class PromoCashbackCalculation {
    private final static Logger logger = LoggerFactory.getLogger(PromoCashbackCalculation.class);

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    @Autowired
    private SystemMessageService systemMessageService;
    @Autowired
    private PromoCashbackMapper promoCashbackMapper;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    private static int CALCULATE_HOUR = Constant.getInstance().getCashback().getCalculationTime();
    private static int REALTIME_CRON_FREQUENCY_MIN = 5;
    private LocalDateTime today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @PostConstruct
    public void init() {
        // init. time information
        today = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
    }

    private PromoCashbackSetting cashbackSetting;
    // <playerId, Player>
    private Map<Integer, Player> playerMap;
    // <ruleId, Cashback>
    private Map<Integer, Cashback> configMap;
    // <gameApiId, <gameTypeId, Set<ruleId>>>
    private Map<Integer, Map<Integer, Set<Integer>>> gameTypeMap;
    // <ruleId, List<betRange>>
    private Map<Integer, List<PromoCashbackBetRange>> betRangeMap;

    public Map<Integer, Map<Integer, List<GameLogHourlyReport>>> groupRecordsByCashbackRule(List<GameLogHourlyReport> data) {
        //<Player, Records>
        Map<Integer, List<GameLogHourlyReport>> playerRecordsMap = data.stream()
                .collect(groupingBy(GameLogHourlyReport::getPlayerId, Collectors.toList()));
        //<Player, <RuleId, Records>>
        Map<Integer, Map<Integer, List<GameLogHourlyReport>>> sortedData = new HashMap<>();
        for (Map.Entry<Integer, List<GameLogHourlyReport>> playerRecords : playerRecordsMap.entrySet()) {
            Map<Integer, List<GameLogHourlyReport>> recordsMap = new HashMap<>();
            for (GameLogHourlyReport record : playerRecords.getValue()) {
                Cashback rule = getCashbackRule(record);
                if (null == rule) {
                    continue;
                }
                recordsMap.computeIfAbsent(rule.getId(), k -> new ArrayList<>()).add(record);
            }
            sortedData.put(playerRecords.getKey(), recordsMap);
        }
        return sortedData;
    }

    public void loadPromoSettings(List<GameLogHourlyReport> data) {
        data.forEach(r -> logger.debug(r.toString()));

        // Get main promotion configuration and transfer to map
        configMap = promoCashbackMapper.list().stream().collect(toMap(Cashback::getId, Function.identity()));

        // Get relative rules
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        List<CashbackGameType> cashbackGameTypes = promoCashbackMapper.getGameTypes(null, locale);
        // Transfer to map
        gameTypeMap = cashbackGameTypes.stream().collect(
                groupingBy(CashbackGameType::getGameApiId,
                        groupingBy(CashbackGameType::getGameTypeId,
                                mapping(CashbackGameType::getRuleId, Collectors.toSet())
                        )
                )
        );

        List<PromoCashbackBetRange> cashbackBetRanges = promoCashbackMapper.getSettings(null);
        betRangeMap = cashbackBetRanges.stream().collect(
                groupingBy(PromoCashbackBetRange::getRuleId,
                        collectingAndThen(Collectors.toCollection(ArrayList::new),
                                l -> {
                                    l.sort(Comparator.comparing(PromoCashbackBetRange::getLimit).reversed());
                                    return l;
                                })
                )
        );

        Set<Integer> playerIdSet = data.stream().map(GameLogHourlyReport::getPlayerId).collect(toSet());
        List<Player> players = playerMapper.listByPlayerIds(new ArrayList<>(playerIdSet));// avoid duplicate player id
        playerMap = players.stream().collect(toMap(Player::getId, Function.identity()));
    }

    public void calculate(List<GameLogHourlyReport> data) {
        //Sort data : <player, <ruleId, GameLogs>>
        Map<Integer, Map<Integer, List<GameLogHourlyReport>>> sortedData = groupRecordsByCashbackRule(data);

        //Loop : player's report <player, <ruleId, GameLogs>>
        for (Map.Entry<Integer, Map<Integer, List<GameLogHourlyReport>>> playerRecordsMap : sortedData.entrySet()) {
            Integer playerId = playerRecordsMap.getKey();
            Player player = playerMap.get(playerId);
            //Loop : < ruleId, GameLogs>
            for (Map.Entry<Integer, List<GameLogHourlyReport>> records : playerRecordsMap.getValue().entrySet()) {

                BigDecimal bonusPerRule = BigDecimal.ZERO;
                Cashback cashbackRule = configMap.get(records.getKey());
                List<PromoCashbackBetRange> betRanges = betRangeMap.get(cashbackRule.getId());

                BigDecimal totalBet = BigDecimal.ZERO;
                for (GameLogHourlyReport record : records.getValue()) {
                    BigDecimal bet = record.getBet();
                    totalBet = totalBet.add(bet);
                    logger.debug("Player:{}, Cashback Rule:{}, bet:{} ", player.getUsername(), cashbackRule.getName(), bet);
                    // bet: 4000, Cashback : { 1000-1900 1%， 2000-2999 2%，那麼如果押3000，返水是 1000 * 1% + 1000 * 2% }
                    bonusPerRule = bonusPerRule.add(preCalculateBonus(bet, betRanges));
                }

                PromoBonus addPromoBonus = new PromoBonus(playerId, cashbackRule.getId(), PromoTypeEnum.CASHBACK,
                        bonusPerRule, totalBet, endTime, cashbackSetting.getWithdrawConditionMultiplier());

                //get existing promoBonus
                List<PromoBonus> existingRequests = promoBonusMapper.query(addPromoBonus);
                BigDecimal existingBonus = existingRequests.stream().map(PromoBonus::getBonusAmount)
                        .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

                //calculate extra bonus
                BigDecimal extraBonus = calculateExtraBonus(
                        cashbackSetting.getMaxBonusAmount().getAmount(player.getCurrency()),
                        addPromoBonus.getBonusAmount(), existingBonus);
                logger.info("Player(ID):[{} ({})], Cashback rule:[{}] Expected adding bonus :[{}], Existing bonus:[{}], Should add bonus:[{}]",
                        player.getUsername(), playerId, cashbackRule.getName(), addPromoBonus
                                .getBonusAmount(), existingBonus, extraBonus);

                //add a new promoRequest or update existing promoRequest
                if (extraBonus.compareTo(BigDecimal.ZERO) > 0) {
                    addPromoBonus.setBonusAmount(extraBonus);
                    addOrUpdatePromoBonus(existingRequests, addPromoBonus);
                }

            }
        }
    }

    public BigDecimal preCalculateBonus(BigDecimal betAmount, List<PromoCashbackBetRange> settings) {
        if (CollectionUtils.isEmpty(settings)) {
            return BigDecimal.ZERO;
        }
        settings.sort(Comparator.comparing(PromoCashbackBetRange::getLimit).reversed());

        BigDecimal cashbackAmount = BigDecimal.ZERO;
        for (PromoCashbackBetRange setting : settings) {
            BigDecimal percentage = setting.getCashbackPercentage().divide(ONE_HUNDRED);
            //bet_amount does not excess limit, bet_amount = 500, Limit [ 1,000 -> 1%, 500 -> 2%, 100 -> 1% ]
            if (betAmount.compareTo(setting.getLimit()) < 0) {
                continue;
            }

            BigDecimal range = betAmount;//limit = 0
            if (BigDecimal.ZERO.compareTo(setting.getLimit()) != 0) {
                //bet = 600, limit = 500/1%, range = 600 - 500 + 1 = 101 [ bet: 500 ~ 600 should be used 1% to calculate cashback_amount ]
                range = betAmount.subtract(setting.getLimit()).add(BigDecimal.ONE);
            }

            cashbackAmount = cashbackAmount.add(range.multiply(percentage));
            logger.debug("Currnet bet:{}, range:{}, percentage:{} / range * percentage(%) = {}, Accumulated cashback:{}",
                    betAmount, range, percentage, range.multiply(percentage), cashbackAmount);
            betAmount = betAmount.subtract(range);
        }
        logger.debug("Remaining bet amount :{}", betAmount);

        return cashbackAmount;
    }

    public Cashback getCashbackRule(GameLogHourlyReport record) {
        Map<Integer, Set<Integer>> matchedGameType = gameTypeMap.get(record.getGameApiId());
        if (matchedGameType == null) {
            return null;
        }
        Set<Integer> ruleSet = matchedGameType.get(record.getGameTypeId());
        if (ruleSet == null) {
            return null;
        }

        Integer groupId = playerMap.get(record.getPlayerId()).getGroupId();

        Cashback defaultRule = null;
        Cashback actualRule = null;
        for (Integer i : ruleSet) {
            Cashback cashback = configMap.get(i);
            if (cashback == null) { // rule may be deleted
                continue;
            }
            if (cashback.getGroupId() == null) {
                defaultRule = cashback;
                continue;
            }
            if (cashback.getGroupId().equals(groupId)) {
                actualRule = cashback;
            }
        }

        return actualRule == null ? defaultRule : actualRule;
    }

    private boolean isApplicable(GameLogHourlyReport record) {
        Cashback cashback = getCashbackRule(record);
        return cashback != null && cashback.isEnabled();
    }

    private BigDecimal calculateExtraBonus(BigDecimal maxCashbackAmount, BigDecimal newBonus, BigDecimal existingBonus) {
        BigDecimal extraBonus = newBonus.subtract(existingBonus);
        if (maxCashbackAmount != null && newBonus.compareTo(maxCashbackAmount) > 0) {
            extraBonus = maxCashbackAmount.subtract(existingBonus);
        }
        return extraBonus;
    }

    private void addOrUpdatePromoBonus(List<PromoBonus> existingRequests, PromoBonus extraPromoBonus) {
        // 如果 cashback_enabled 為 false 的時候，插入bonus並馬上設為rejected、不累計
        if (!playerMap.get(extraPromoBonus.getPlayerId()).getCashbackEnabled()) {
            extraPromoBonus.setStatus(BonusStatusEnum.REJECTED);
            extraPromoBonus.setComment("player's cashback status is inactive");
            promoBonusMapper.insert(extraPromoBonus);
            return;
        }

        PromoBonus lastElement = CollectionUtils.lastElement(existingRequests);
        // The last request had deposited already, or there is no last request
        boolean newRecord = Optional.ofNullable(lastElement)
                .map(element -> BonusStatusEnum.PENDING_APPROVAL != element.getStatus()).orElse(true);
        if (newRecord) {
            promoBonusMapper.insert(extraPromoBonus);
        } else {
            //existed bonus + extra bonus
            BigDecimal bonus = lastElement.getBonusAmount().add(extraPromoBonus.getBonusAmount());
            lastElement.setBonusAmount(bonus);
            promoBonusMapper.update(lastElement);
        }

        // System message
        Map<PlayerMessageTemplateProperty, Object> params = Collections.singletonMap(AMOUNT, extraPromoBonus.getBonusAmount());
        systemMessageService.sendSystemMessage(PROMO, params, Collections.singletonList(extraPromoBonus.getPlayerId()));
    }

    private boolean isCalculatorDisabled(CashbackExecutionTypeEnum type) {
        if (cashbackSetting == null || !cashbackSetting.isEnabled()) {
            logger.info("No cashback setting defined, or cashback is not enabled, calculator not running");
            return true;
        }
        return cashbackSetting.getExecutionType() != type.getCode();
    }

    private void execute() {
        // load data depending on calculating time
        List<GameLogHourlyReport> data = gameLogHourlyReportMapper.getDailyBetSummary(startTime, endTime);
        if (CollectionUtils.isEmpty(data)) {
            return;
        }

        // load promotion settings
        loadPromoSettings(data);
        // filter improper records
        List<GameLogHourlyReport> applicableRecords = data.stream().filter(this::isApplicable).collect(toList());

        // calculate bonus of applicable records
        calculate(applicableRecords);
    }

    public void realtimeCalculate() {
        cashbackSetting = promoCashbackSettingMapper.get();
        if (isCalculatorDisabled(CashbackExecutionTypeEnum.REALTIME)) {
            return;
        }

        // to calculate real time cashback
        startTime = today.minus(Period.ofDays(1)).plusHours(CALCULATE_HOUR);
        endTime = today.plusHours(CALCULATE_HOUR).minusSeconds(1);
        if (LocalDateTime.now().minusMinutes(REALTIME_CRON_FREQUENCY_MIN).isAfter(endTime)) {
            startTime.plusDays(1);
            endTime.plusDays(1);
        }
        execute();
    }

    public void dailyCalculate() {
        cashbackSetting = promoCashbackSettingMapper.get();
        if (isCalculatorDisabled(CashbackExecutionTypeEnum.DAILY)) {
            return;
        }

        // to calculate cashback of previous day wagers (trigger hour must match the calculateHour, which is the constant.cashback.calculaiton-time)
        startTime = today.minus(Period.ofDays(1)).plusHours(CALCULATE_HOUR);
        endTime = today.plusHours(CALCULATE_HOUR).minusSeconds(1);
        execute();
    }
}
