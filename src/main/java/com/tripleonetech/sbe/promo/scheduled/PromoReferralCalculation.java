package com.tripleonetech.sbe.promo.scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.ReferrerPlayer;
import com.tripleonetech.sbe.player.message.PlayerMessageTemplate.PlayerMessageTemplateProperty;
import com.tripleonetech.sbe.player.message.SystemMessageService;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSetting;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSettingMapper;
import com.tripleonetech.sbe.promo.referral.ReferralTypeEnum;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Promo.Properties.AMOUNT;
import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.PROMO;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Component
public class PromoReferralCalculation {
    private final static Logger logger = LoggerFactory.getLogger(PromoReferralCalculation.class);

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
    @Autowired
    private SystemMessageService systemMessageService;
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PromoPlayerReferralSettingMapper promoReferralMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    private static int calculateHour = Constant.getInstance().getCashback().getCalculationTime();
    private LocalDateTime calculateDate;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @PostConstruct
    public void init() {
        // init. time information
        calculateDate = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);

        defaultRule = new HashMap<>();
    }

    // <playerId, referrerPlayer>
    private Map<Integer, ReferrerPlayer> referrerPlayerMap;
    // <playerCredentialId, <currency, PromoReferral>>
    private Map<Integer, Map<SiteCurrencyEnum, PromoPlayerReferralSetting>> groupRuleMap;

    private Map<SiteCurrencyEnum, PromoPlayerReferralSetting> defaultRule;

    private Map<Integer, Map<SiteCurrencyEnum, PromoPlayerReferralSetting>> groupRuleByPlayer(List<PromoPlayerReferralSetting> rules) {
        Map<Integer, Map<SiteCurrencyEnum, PromoPlayerReferralSetting>> sortedData = new HashMap<>();
        for (PromoPlayerReferralSetting rule : rules) {
            Integer playerCredentialId = rule.getPlayerCredentialId();
            if (playerCredentialId == null) {
                defaultRule.put(rule.getCurrency(), rule);
                continue;
            }
            sortedData.computeIfAbsent(playerCredentialId, k -> new HashMap<>()).put(rule.getCurrency(), rule);
        }
        return sortedData;
    }

    public void loadPromoSettings(List<GameLogHourlyReport> data) {
        // Get main promotion configuration and transfer to map
        List<PromoPlayerReferralSetting> rules = promoReferralMapper.listByType(ReferralTypeEnum.BET_REVENUE);
        groupRuleMap = groupRuleByPlayer(rules);

        Set<Integer> playerIdSet = data.stream().map(GameLogHourlyReport::getPlayerId).collect(toSet());
        List<ReferrerPlayer> referrerPlayerList = playerMapper.getReferralPlayerByPlayerIds(new ArrayList<>(playerIdSet));
        referrerPlayerMap = referrerPlayerList.stream()
                .collect(toMap(ReferrerPlayer::getReferralPlayerId, Function.identity()));
    }

    public void calculate(List<GameLogHourlyReport> data) {
        for (GameLogHourlyReport record : data) {
            Integer playerId = record.getPlayerId();
            BigDecimal totalBet = record.getBet();

            PromoPlayerReferralSetting referralRule = getRule(playerId, SiteCurrencyEnum.valueOf(record.getCurrency()));
            BigDecimal bonus = calculateBonus(totalBet, referralRule);
            ReferrerPlayer referrerPlayer = referrerPlayerMap.get(playerId);

            PromoBonus promoBonus = new PromoBonus(referrerPlayer.getPlayerId(), referralRule.getId(), PromoTypeEnum.REFERRAL,
                    bonus, totalBet, endTime, referralRule.getWithdrawConditionMultiplier());
            promoBonus.setComment("withdraw revenue from player id : " + referrerPlayer.getReferralPlayerId());
            promoBonusMapper.insert(promoBonus);

            // System message
            Map<PlayerMessageTemplateProperty, Object> params = Collections.singletonMap(AMOUNT, promoBonus.getBonusAmount());
            systemMessageService.sendSystemMessage(PROMO, params, Collections.singletonList(promoBonus.getPlayerId()));
        }

    }

    public BigDecimal calculateBonus(BigDecimal betAmount, PromoPlayerReferralSetting rule) {
        BigDecimal bonusAmount = BigDecimal.ZERO;
        if (Objects.nonNull(rule.getBonusPercentage())) {
            bonusAmount = betAmount.multiply(rule.getBonusPercentage()).divide(ONE_HUNDRED);
        }
        if (Objects.nonNull(rule.getMinBonusAmount())) {
            bonusAmount = bonusAmount.max(rule.getMinBonusAmount());
        }
        if (Objects.nonNull(rule.getMaxBonusAmount())) {
            bonusAmount = bonusAmount.min(rule.getMaxBonusAmount());
        }

        return bonusAmount;
    }

    public PromoPlayerReferralSetting getRule(Integer playerId, SiteCurrencyEnum currency) {
        ReferrerPlayer referrerPlayer = referrerPlayerMap.get(playerId);
        Integer referrerPlayerCredentialId = referrerPlayer.getPlayerCredentialId();
        if (referrerPlayerCredentialId == null) {
            return null;
        }

        Map<SiteCurrencyEnum, PromoPlayerReferralSetting> ruleMap = groupRuleMap.get(referrerPlayerCredentialId);
        if (ruleMap == null) {
            return defaultRule.get(currency);
        }

        return ruleMap.get(referrerPlayer.getCurrency());
    }

    private boolean isApplicable(GameLogHourlyReport record) {
        PromoPlayerReferralSetting rule = getRule(record.getPlayerId(), SiteCurrencyEnum.valueOf(record.getCurrency()));
        return rule != null && rule.isEnabled();
    }

    private void execute() {
        // load data depending on calculating time
        List<GameLogHourlyReport> data = gameLogHourlyReportMapper.getBetSumGroupByPlayer(startTime, endTime);
        if (CollectionUtils.isEmpty(data)) {
            return;
        }

        // load promotion settings
        loadPromoSettings(data);
        // filter improper records
        List<GameLogHourlyReport> applicableRecords = data.stream().filter(this::isApplicable).collect(toList());

        // calculate bonus of applicable records
        calculate(applicableRecords);
    }

    public void dailyCalculate() {
        startTime = calculateDate.minus(Period.ofDays(1)).plusHours(calculateHour);
        endTime = calculateDate.plusHours(calculateHour).minusSeconds(1);
        execute();
    }
}
