package com.tripleonetech.sbe.promo.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("scheduled")
@Component
public class PromoCashbackCalculator {

    @Autowired
    private PromoCashbackCalculation promoCashbackCalculation;

    // real-time
    @Scheduled(cron = "${constant.cashback.realtime-schedule}")
    public void realTimeScheduledTask() {
        promoCashbackCalculation.realtimeCalculate();
    }

    // daily
    @Scheduled(cron = "${constant.cashback.daily-schedule}")
    public void dailyCashbackScheduleTask() {
        promoCashbackCalculation.dailyCalculate();
    }

}
