package com.tripleonetech.sbe.promo.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("scheduled")
@Component
public class PromoReferralCalculator {

    @Autowired
    private PromoReferralCalculation promoReferralCalculation;

    // daily
    @Scheduled(cron = "${constant.referral.daily-schedule}")
    public void dailyReferralScheduleTask() {
        promoReferralCalculation.dailyCalculate();
    }

}
