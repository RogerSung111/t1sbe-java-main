package com.tripleonetech.sbe.promo;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PromoTypeEnum implements BaseCodeEnum {
    // Regular promos have code value between 1 ~ 10
    CASHBACK(1, "cashback"), REFERRAL(2, "referral"),
    // Campaigns have code value > 10
    CAMPAIGN_DEPOSIT(11, "deposit"), CAMPAIGN_TASK(12, "task"), CAMPAIGN_RESCUE(13, "rescue"), CAMPAIGN_LOGIN(14,"login");

    private int code;
    private String shortName;

    PromoTypeEnum(int code, String shortName) {
        this.code = code;
        this.shortName = shortName;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }

    public String getShortName() {
        return shortName;
    }

    @JsonCreator
    public static PromoTypeEnum valueOf(Integer code) {
        PromoTypeEnum[] enumConstants = PromoTypeEnum.values();
        for (PromoTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }

    public static PromoTypeEnum getFromShortName(String shortName) {
        PromoTypeEnum[] enumConstants = PromoTypeEnum.values();
        for (PromoTypeEnum e : enumConstants) {
            if (e.getShortName().equalsIgnoreCase(shortName)) {
                return e;
            }
        }
        return null;
    }
}
