package com.tripleonetech.sbe.withdraw.config;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WithdrawApiConfigMapper extends ApiConfigMapper {
    @Override
    List<WithdrawApiConfig> list();

    @Override
    WithdrawApiConfig get(@Param("id") Integer id);

    List<WithdrawApiConfig> listByCurrency(@Param("currency") SiteCurrencyEnum currency);

    int update(@Param("config") WithdrawApiConfig config);

    int delete(@Param("id") Integer id);

    int insert(WithdrawApiConfig config);
}
