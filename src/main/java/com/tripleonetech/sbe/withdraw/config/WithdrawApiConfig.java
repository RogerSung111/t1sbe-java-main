package com.tripleonetech.sbe.withdraw.config;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfig;

public class WithdrawApiConfig extends ApiConfig {

    private SiteCurrencyEnum currency;

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }
}
