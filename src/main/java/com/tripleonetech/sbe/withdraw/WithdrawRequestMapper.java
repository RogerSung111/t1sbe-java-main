package com.tripleonetech.sbe.withdraw;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WithdrawRequestMapper {

    int insert(WithdrawRequest withdrawRequest);

    WithdrawRequestView getView(long id);

    WithdrawRequest get(long id);

    int updateStatus(@Param("id") long id, @Param("status") WithdrawStatusEnum status);

    List<WithdrawRequestView> list(@Param("query") WithdrawRequestQueryForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    List<WithdrawRequestView> querySummary(Map<String, Object> params);

    List<WithdrawRequest> getApprovedInPeriod(
            @Param("syncStartHour") LocalDateTime syncStartHour,
            @Param("syncEndHour") LocalDateTime syncEndHour);

    long getPendingRequestCount();
    
    BigDecimal getPendingRequestAmount(Integer playerId);
}
