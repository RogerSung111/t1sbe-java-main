package com.tripleonetech.sbe.withdraw;

import com.tripleonetech.sbe.common.Utils;
import com.tripleonetech.sbe.player.PlayerProfile;
import com.tripleonetech.sbe.player.PlayerProfileMapper;
import com.tripleonetech.sbe.player.wallet.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WithdrawRequestService {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawRequestService.class);

    @Autowired
    private WalletService walletService;
    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;

    @Transactional(rollbackFor = Exception.class)
    public boolean newWithdrawal(WithdrawRequest request) {
        //freeze amount
        if (!walletService.freezeWithdrawAmount(request.getPlayerId(), request.getAmount())) {
            return false;
        }
        logger.info("Player:[{}] applied a withdraw request [{}]", request.getPlayerId(), request.toString());

        PlayerProfile profile = playerProfileMapper.getByPlayerId(request.getPlayerId());
        request.setAccountHolderName(Utils.getUserNameByLocale(profile.getFirstName(), profile.getLastName()));
        withdrawRequestMapper.insert(request);
        logger.info("Withdraw request [#{}] was created", request.getId());
        return true;
    }

    public boolean approveWithdrawRequest(WithdrawRequest withdrawRequest) {

        boolean isDeducted = walletService.deductMainBalanceFromPending(withdrawRequest.getPlayerId(), withdrawRequest.getAmount());
        if (isDeducted) {
            withdrawRequestMapper.updateStatus(withdrawRequest.getId(), WithdrawStatusEnum.PAID);
        }
        return isDeducted;
    }

    public boolean rejectWithdrawRequest(WithdrawRequest withdrawRequest) {
        boolean isReturned = walletService.returnMainBalanceFromPending(withdrawRequest.getPlayerId(), withdrawRequest.getAmount());
        if (isReturned) {
            withdrawRequestMapper.updateStatus(withdrawRequest.getId(), WithdrawStatusEnum.REJECTED);
        }
        return isReturned;
    }
}
