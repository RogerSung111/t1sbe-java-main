package com.tripleonetech.sbe.withdraw.report;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;

public class WithdrawSummaryReport {
    private LocalDateTime timeHour;
    private SiteCurrencyEnum currency;
    private Integer withdrawalCount;
    private Integer withdrawerCount;
    private BigDecimal withdrawalAmount;
    private Boolean dirty;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    
    
    public LocalDateTime getTimeHour() {
        return timeHour;
    }
    public void setTimeHour(LocalDateTime timeHour) {
        this.timeHour = timeHour;
    }
    public SiteCurrencyEnum getCurrency() {
        return currency;
    }
    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }
    public Integer getWithdrawalCount() {
        return withdrawalCount;
    }
    public void setWithdrawalCount(Integer withdrawalCount) {
        this.withdrawalCount = withdrawalCount;
    }
    public Integer getWithdrawerCount() {
        return withdrawerCount;
    }
    public void setWithdrawerCount(Integer withdrawerCount) {
        this.withdrawerCount = withdrawerCount;
    }
    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }
    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }
    public Boolean getDirty() {
        return dirty;
    }
    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    public static WithdrawSummaryReport create(WithdrawRequest withdrawRequest, SiteCurrencyEnum currency) {
        WithdrawSummaryReport withdrawSummaryReport = new WithdrawSummaryReport();
        withdrawSummaryReport.setWithdrawalAmount(withdrawRequest.getAmount());
        withdrawSummaryReport.setCurrency(currency);
        withdrawSummaryReport.setWithdrawalCount(1);
        withdrawSummaryReport.setWithdrawerCount(1);
        withdrawSummaryReport.setDirty(false);
        withdrawSummaryReport.setTimeHour(withdrawRequest.getRequestedDate().truncatedTo(ChronoUnit.HOURS));
        return withdrawSummaryReport;
    }
}
