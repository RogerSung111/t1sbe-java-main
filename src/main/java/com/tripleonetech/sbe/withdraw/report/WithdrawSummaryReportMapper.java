package com.tripleonetech.sbe.withdraw.report;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.TimeRangeForm;

@Mapper
public interface WithdrawSummaryReportMapper {
    
    void upsertDirty(WithdrawSummaryReport depositSummaryReport);
    
    void refreshDirty();
    
    WithdrawSummaryReport get(@Param("timeHour") LocalDateTime cutoffTime, @Param("currency") SiteCurrencyEnum currency);
    
    List<WithdrawSummaryReport> listDirty();

    List<WithdrawSummaryReportView> query(@Param("query") TimeRangeForm query,
                                          @Param("pageNum") Integer pageNum,
                                          @Param("pageSize") Integer pageSize);
}
