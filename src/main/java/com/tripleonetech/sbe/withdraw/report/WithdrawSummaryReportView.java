package com.tripleonetech.sbe.withdraw.report;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

public class WithdrawSummaryReportView {
    @ApiModelProperty(value = "Date of withdrawal summary")
    private LocalDate date;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Count of withdrawal requests")
    private int withdrawalCount;
    @ApiModelProperty(value = "Number of withdraw players")
    private int withdrawerCount;
    @ApiModelProperty(value = "Sum of withdrawal amount")
    private BigDecimal withdrawalAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "Unique id for this data, used by frontend")
    public String getId() {
        return String.format("%s-%s", date.toString(), currency.toString());
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public int getWithdrawalCount() {
        return withdrawalCount;
    }

    public void setWithdrawalCount(int withdrawalCount) {
        this.withdrawalCount = withdrawalCount;
    }

    public int getWithdrawerCount() {
        return withdrawerCount;
    }

    public void setWithdrawerCount(int withdrawerCount) {
        this.withdrawerCount = withdrawerCount;
    }

    public BigDecimal getWithdrawalAmount() {
        if(withdrawalAmount == null){
            return  BigDecimal.ZERO;
        }
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }
}
