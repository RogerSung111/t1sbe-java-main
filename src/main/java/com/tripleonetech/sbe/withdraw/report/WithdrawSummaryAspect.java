package com.tripleonetech.sbe.withdraw.report;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;

@Aspect
@Component
public class WithdrawSummaryAspect {
    
private static final Logger logger = LoggerFactory.getLogger(WithdrawSummaryAspect.class);
    
    @Autowired
    private WithdrawSummaryReportMapper withdrawSummaryReportMapper;
    
    @Autowired
    private PlayerMapper playerMapper;
    
    @After("execution(* com.tripleonetech.sbe.withdraw.WithdrawRequestService.approveWithdrawRequest(..)))")
    public void setAsDirty(JoinPoint joinPoint) {
        WithdrawRequest withdrawRequest = (WithdrawRequest) joinPoint.getArgs()[0];

        if (null == withdrawRequest) {
            return;
        }
        Player player = playerMapper.get(withdrawRequest.getPlayerId());
        WithdrawSummaryReport withdrawSummaryReport = WithdrawSummaryReport.create(withdrawRequest, player.getCurrency());
        withdrawSummaryReportMapper.upsertDirty(withdrawSummaryReport);
    }

}
