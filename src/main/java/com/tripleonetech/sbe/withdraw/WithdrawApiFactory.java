package com.tripleonetech.sbe.withdraw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiConfigMapper;
import com.tripleonetech.sbe.common.integration.ApiFactory;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfigMapper;
import com.tripleonetech.sbe.withdraw.integration.WithdrawApi;

@Component
public class WithdrawApiFactory extends ApiFactory<WithdrawApi> {
    @Autowired
    private WithdrawApiConfigMapper configMapper;

    @Override
    protected ApiTypeEnum getApiType() {
        return ApiTypeEnum.WITHDRAW;
    }

    @Override
    protected ApiConfigMapper getConfigMapper() {
        return this.configMapper;
    }
}
