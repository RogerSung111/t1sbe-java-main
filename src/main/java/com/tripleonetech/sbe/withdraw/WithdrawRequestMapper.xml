<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.tripleonetech.sbe.withdraw.WithdrawRequestMapper">
    <resultMap id="baseResultMapper" type="com.tripleonetech.sbe.withdraw.WithdrawRequestView">
        <id column="id" property="id" jdbcType="BIGINT" />
        <result column="bank_id" property="bankId" jdbcType="INTEGER" />
        <result column="player_id" property="playerId" jdbcType="INTEGER" />
        <result column="status" property="status" />
        <result column="amount" property="amount" jdbcType="DECIMAL" />
        <result column="requested_date" property="requestedDate" jdbcType="DATE" />
        <result column="updated_at" property="updatedAt" jdbcType="DATE" />
        <result column="external_uid" property="externalUid" jdbcType="VARCHAR" />
        <result column="username" property="username" />
        <result column="currency" property="currency" />
        <association property="bankAccount" javaType="com.tripleonetech.sbe.withdraw.WithdrawRequestView$BankAccount">
            <result column="account_holder_name" property="accountHolderName" jdbcType="VARCHAR" />
            <result column="account_number" property="accountNumber" jdbcType="VARCHAR" />
            <result column="bank_name" property="bankName" />
        </association>
    </resultMap>

    <select id="getView" resultMap="baseResultMapper">
        select w.id, w.status, w.account_number, w.account_holder_name, w.amount, w.requested_date, w.updated_at, w.external_uid, w.bank_branch,
               w.bank_id, b.name as bank_name,
               w.player_id, p.username, p.currency, p.created_at
          from withdraw_request w, bank b, player p
         where w.bank_id = b.id
           and w.player_id = p.id
           and w.id = #{id, jdbcType=BIGINT}
    </select>
    <select id="get" resultType="com.tripleonetech.sbe.withdraw.WithdrawRequest">
        select *
          from withdraw_request
         where id = #{id, jdbcType=BIGINT}
    </select>

    <select id="list" resultMap="baseResultMapper">
        SELECT
            w.id,
            p.username,
            p.currency,
            bank_id,
            b.name as bank_name,
            account_number,
            w.bank_branch,
            w.amount,
            w.requested_date,
            w.updated_at,
            w.status,
            c.id as player_id
        FROM
            withdraw_request w
            INNER JOIN bank b ON b.id = w.bank_id
            INNER JOIN player p ON p.id = w.player_id
            INNER JOIN player_credential c ON p.username = c.username
        <where>
            <if test="query.status != null">
                AND w.status = #{query.status}
            </if>
            <if test="query.username != null and query.username != ''">
                AND p.username LIKE CONCAT('%', #{query.username}, '%')
            </if>
            <if test="query.playerId != null">
                AND c.id = #{query.playerId}
            </if>
            <if test="query.currency != null">
                AND p.currency = #{query.currency}
            </if>
            <if test="query.requestedDateStart != null">
                AND w.requested_date <![CDATA[ >= ]]> #{query.requestedDateStart, jdbcType=TIMESTAMP}
            </if>
            <if test="query.requestedDateEnd != null">
                AND w.requested_date <![CDATA[ < ]]> DATE_ADD(#{query.requestedDateEnd, jdbcType=TIMESTAMP}, INTERVAL 1 SECOND)
            </if>
        </where>
        ORDER BY
        <choose>
            <when test='"amount".equals(query.sort)'>
                w.amount
            </when>
            <when test='"updatedAt".equals(query.sort)'>
                w.updated_at
            </when>
            <otherwise>
                w.requested_date
            </otherwise>
        </choose>
        <if test="!query.isAsc">
            DESC
        </if>
    </select>

    <select id="querySummary" resultMap="baseResultMapper">
        SELECT
        w.id,
        w.player_id,
        p.username,
        w.account_number,
        w.amount,
        w.requested_date,
        w.updated_at,
        w.status,
        w.bank_branch
        FROM
        withdraw_request w,
        player p
        WHERE p.id = w.player_id
        AND p.currency = #{currency}
        <if test="requestDateStart != null">
            AND w.requested_date <![CDATA[ >= ]]>
            #{requestDateStart, jdbcType=TIMESTAMP}
        </if>
        <if test="requestDateEnd != null">
            AND w.requested_date <![CDATA[ <= ]]>
            #{requestDateEnd, jdbcType=TIMESTAMP}
        </if>
    </select>

    <select id="getApprovedInPeriod" resultType="com.tripleonetech.sbe.withdraw.WithdrawRequest" >
        select *
        from withdraw_request where status = 1
        and updated_at &gt;=
        #{syncStartHour}
        and updated_at &lt; DATE_ADD(#{syncEndHour}, INTERVAL 1 HOUR)
    </select >

    <insert id="insert" useGeneratedKeys="true" keyProperty="id" parameterType="com.tripleonetech.sbe.withdraw.WithdrawRequest">
        insert into withdraw_request(id, player_id, bank_id, account_number, amount, status, bank_branch, account_holder_name)
        values(
        (select * from (select coalesce(max(id), concat( DATE_FORMAT(now(), '%Y%m%d') , '0000'))+1 from withdraw_request where created_at > curdate()) TEMP),
        #{playerId}, #{bankId}, #{accountNumber}, #{amount}, 1, #{bankBranch}, #{accountHolderName})
    </insert>

    <update id="updateStatus">
        UPDATE
            withdraw_request
        SET
            status = #{status}
        WHERE
            id = #{id}
    </update>

    <select id ="getPendingRequestCount" resultType="java.lang.Long">
        select count(*)
        from
        withdraw_request
        where
        `status` = 1
    </select>

    <!-- Status = PAID(10), REJECTED(11), CANCEL(12)  -->
    <select id="getPendingRequestAmount" resultType="java.math.BigDecimal">
    SELECT
	    IFNULL(SUM(amount), 0)
	FROM
	    withdraw_request wr
	WHERE
	    wr.status not in (10, 11, 12) and
	    wr.player_id = #{playerId};
    </select>

</mapper>
