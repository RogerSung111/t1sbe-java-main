package com.tripleonetech.sbe.withdraw;

import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WithdrawRequest {

    private Integer id;
    private Integer bankId;
    private Integer playerId;
    private WithdrawStatusEnum status;
    private BigDecimal amount;
    private String accountNumber;
    private String accountHolderName;
    private String bankBranch;
    private LocalDateTime requestedDate;
    private String externalUid;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WithdrawStatusEnum getStatus() {
        return status;
    }

    public void setStatus(WithdrawStatusEnum status) {
        this.status = status;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(LocalDateTime requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getExternalUid() {
        return externalUid;
    }

    public void setExternalUid(String externalUid) {
        this.externalUid = externalUid;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public static WithdrawRequest from(WithdrawRequestForm form) {
        WithdrawRequest withdrawRequest = new WithdrawRequest();
        BeanUtils.copyProperties(form, withdrawRequest);
        withdrawRequest.setRequestedDate(LocalDateTime.now());
        withdrawRequest.setStatus(WithdrawStatusEnum.CREATED);
        withdrawRequest.setAccountHolderName(form.getAccountHolderName());
        return withdrawRequest;
    }

    @Override
    public String toString() {
        return "WithdrawRequest [id=" + id + ", bankId=" + bankId + ", playerId=" + playerId + ", status=" + status
                + ", amount=" + amount + ", accountNumber=" + accountNumber
                + ", accountHolderName="
                + accountHolderName + ", bankBranch=" + bankBranch
                + ", requestedDate="
                + requestedDate
                + ", externalUid=" + externalUid + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }


}
