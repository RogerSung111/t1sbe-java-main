package com.tripleonetech.sbe.withdraw.workflow;

public class WithdrawWorkflow extends WithdrawWorkflowSetting {

    String withdrawRequestProcessHistoryIds;

    public String getWithdrawRequestProcessHistoryIds() {
        return withdrawRequestProcessHistoryIds;
    }

    public void setWithdrawRequestProcessHistoryIds(String withdrawRequestProcessHistoryIds) {
        this.withdrawRequestProcessHistoryIds = withdrawRequestProcessHistoryIds;
    }
}
