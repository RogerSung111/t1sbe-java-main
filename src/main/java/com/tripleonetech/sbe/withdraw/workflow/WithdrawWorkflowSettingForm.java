package com.tripleonetech.sbe.withdraw.workflow;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import io.swagger.annotations.ApiModelProperty;

public class WithdrawWorkflowSettingForm {

    @ApiModelProperty(value = "Withdraw workflow id", required = true)
    private WithdrawStatusEnum id;
    @ApiModelProperty(value = "name of workflow code", required = true)
    private String name;
    @ApiModelProperty(value = "enable/disable workflow step", required = true)
    private Boolean enabled;

    public WithdrawStatusEnum getId() {
        return id;
    }

    public void setId(WithdrawStatusEnum id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

}
