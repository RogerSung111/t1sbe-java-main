package com.tripleonetech.sbe.withdraw.workflow;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;

public class WithdrawWorkflowSettingView {
    private WithdrawStatusEnum id;
    private String name;
    private Integer step;
    private Boolean enabled;

    public WithdrawStatusEnum getId() {
        return id;
    }

    public void setId(WithdrawStatusEnum id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getStep() {
        return step;
    }
    public void setStep(Integer step) {
        this.step = step;
    }
    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public static WithdrawWorkflowSettingView from(WithdrawWorkflowSetting workflowSetting) {
        if(null == workflowSetting) {
            return null;
        }
        WithdrawWorkflowSettingView view = new WithdrawWorkflowSettingView();
        view.setId(WithdrawStatusEnum.valueOf(workflowSetting.getId()));
        view.setName(workflowSetting.getName());
        view.setStep(workflowSetting.getId());
        view.setEnabled(workflowSetting.getEnabled());
        return view;
    }

}
