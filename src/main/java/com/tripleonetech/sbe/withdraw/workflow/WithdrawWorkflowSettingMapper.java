package com.tripleonetech.sbe.withdraw.workflow;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WithdrawWorkflowSettingMapper {

    List<WithdrawWorkflowSetting> list();

    void updateWorkflowSetting(WithdrawWorkflowSetting workingSetting);

    WithdrawWorkflowSetting getWorkflowSettingById(WithdrawStatusEnum id);

    List<WithdrawWorkflow> getWorkflowWithProcessHistory(Integer withdrawRequestId);
}
