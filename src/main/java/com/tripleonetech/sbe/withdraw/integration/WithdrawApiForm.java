package com.tripleonetech.sbe.withdraw.integration;

import com.tripleonetech.sbe.player.PlayerBankAccount;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;

public class WithdrawApiForm {

    private WithdrawRequest withdrawRequest;
    private PlayerBankAccount playerBankAccount;

    public PlayerBankAccount getPlayerBankAccount() {
        return playerBankAccount;
    }

    public void setPlayerBankAccount(PlayerBankAccount playerBankAccount) {
        this.playerBankAccount = playerBankAccount;
    }

    public WithdrawRequest getWithdrawRequest() {
        return withdrawRequest;
    }

    public void setWithdrawRequest(WithdrawRequest withdrawRequest) {
        this.withdrawRequest = withdrawRequest;
    }
}
