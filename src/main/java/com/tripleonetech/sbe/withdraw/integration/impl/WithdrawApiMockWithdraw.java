package com.tripleonetech.sbe.withdraw.integration.impl;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;
import com.tripleonetech.sbe.withdraw.WithdrawRequestMapper;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import com.tripleonetech.sbe.withdraw.integration.AbstractWithdrawApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class WithdrawApiMockWithdraw extends AbstractWithdrawApi {
    // private static final Logger logger = LoggerFactory.getLogger(WithdrawApiMockWithdraw.class);
    private static List<ApiMetaField> META_FIELDS;
    private final String CALL_BACK_URL = "http://localhost:8000/payment/withdraw/callback/1";

    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;

    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("url", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("notify_url", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("merchant_id", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("key", MetaFieldTypeEnum.TEXT, true));
            }
        };
    }

    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    //Generate a withdrawal request.
    @Override
    public ApiResult withdraw(WithdrawRequest withdrawRequest) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("withdrawRequestId", Long.toString(withdrawRequest.getId()));
        param.put("amount", withdrawRequest.getAmount().toString());
        param.put("account", withdrawRequest.getAccountNumber());
        param.put("callback", CALL_BACK_URL);
        boolean success = submitRequest(param);
        return new ApiResult(success ? ApiResponseEnum.OK : ApiResponseEnum.SERVICE_UNAVAILABLE);
    }

    //submit request to third-party api
    private boolean submitRequest(Map<String, Object> param) {
        return true;
    }

    @Override
    public ApiResult callback(Map<String, ? extends Object> param) {
        int orderId = (Integer) param.get("withdrawRequestId");
        int status = (Integer) param.get("status");
        withdrawRequestMapper.updateStatus(orderId, WithdrawStatusEnum.valueOf(status));
        return new ApiResult(ApiResponseEnum.OK);
    }
}
