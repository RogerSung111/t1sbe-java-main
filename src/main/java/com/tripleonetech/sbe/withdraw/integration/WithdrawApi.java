package com.tripleonetech.sbe.withdraw.integration;

import com.tripleonetech.sbe.common.integration.ApiCallbackable;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.ThirdPartyApi;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;

public interface WithdrawApi extends ThirdPartyApi, ApiCallbackable {
    ApiResult withdraw(WithdrawRequest withdrawRequest);
}
