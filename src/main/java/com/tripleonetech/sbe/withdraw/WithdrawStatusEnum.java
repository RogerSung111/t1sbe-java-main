package com.tripleonetech.sbe.withdraw;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum WithdrawStatusEnum implements BaseCodeEnum {
    CREATED(1),
    SYSTEM_REVIEW(2),
    REVIEW1(3),
    REVIEW2(4),
    REVIEW3(5),
    REVIEW4(6),
    REVIEW5(7),
    REVIEW6(8),
    PAYING(9),
    PAID(10),
    REJECTED(11),
    CANCELED(12);

    private int code;

    WithdrawStatusEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static WithdrawStatusEnum valueOf(int status) {
        for (WithdrawStatusEnum enumInstance : WithdrawStatusEnum.values()) {
            if (status == enumInstance.code)
                return enumInstance;
        }
        return null;
    }
}
