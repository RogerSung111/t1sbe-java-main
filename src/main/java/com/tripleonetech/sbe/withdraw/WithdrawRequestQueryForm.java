package com.tripleonetech.sbe.withdraw;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

public class WithdrawRequestQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Start time")
    private LocalDateTime requestedDateStart;
    @ApiModelProperty(value = "End time")
    private LocalDateTime requestedDateEnd;
    @ApiModelProperty(value = "Withdraw request status: CREATED(1),SYSTEM_REVIEW(2),REVIEW1(3),REVIEW2(4),REVIEW3(5)," +
            "REVIEW4(6),REVIEW5(7),REVIEW6(8),PAYING(9),PAID(10),REJECTED(11),CANCELED(12)",
            allowableValues = "1,2,3,4,5,6,7,8,9,10,11,12")
    private Integer status;
    @ApiModelProperty(value = "Player username")
    private String username;
    @ApiModelProperty(value = "Player ID")
    private Integer playerId;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;

    public LocalDateTime getRequestedDateStart() {
        return requestedDateStart;
    }

    public void setRequestedDateStartLocal(LocalDateTime requestedDateStart) {
        this.requestedDateStart = requestedDateStart;
    }

    public void setRequestedDateStart(ZonedDateTime requestedDateStart) {
        this.requestedDateStart = DateUtils.zonedToLocal(requestedDateStart);
    }

    public LocalDateTime getRequestedDateEnd() {
        return requestedDateEnd;
    }

    public void setRequestedDateEndLocal(LocalDateTime requestedDateEnd) {
        this.requestedDateEnd = requestedDateEnd;
    }

    public void setRequestedDateEnd(ZonedDateTime requestedDateEnd) {
        this.requestedDateEnd = DateUtils.zonedToLocal(requestedDateEnd);
    }

    public Integer getStatus() {
        return status;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not appear in swagger UI")
    public WithdrawStatusEnum getStatusEnum() {
        return Objects.isNull(getStatus()) ? null : WithdrawStatusEnum.valueOf(getStatus());
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }
}
