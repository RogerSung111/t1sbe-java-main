package com.tripleonetech.sbe.withdraw;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

/**
 * 2019.08.14
 * @author eddie
 *
 */
public class WithdrawRequestForm {
    @NotNull
    @ApiModelProperty(value = "Bank ID", required = true)
    private Integer bankId;
    @NotNull
    @ApiModelProperty(value = "Account number", required = true)
    private String accountNumber;
    @NotNull
    @ApiModelProperty(value = "Bank branch", required = true)
    private String bankBranch;
    @NotNull
    @ApiModelProperty(value = "Withdraw amount", required = true)
    private BigDecimal amount;
    @NotNull(message = "Please input withdraw password")
    @ApiModelProperty(value = "Withdraw password", required = true)
    private String withdrawPassword;
    @NotNull
    @ApiModelProperty(value = "holder name of bank account", required = true)
    private String accountHolderName;

    public Integer getBankId() {
        return bankId;
    }
    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getBankBranch() {
        return bankBranch;
    }
    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getWithdrawPassword() {
        return withdrawPassword;
    }
    public void setWithdrawPassword(String withdrawPassword) {
        this.withdrawPassword = withdrawPassword;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }
    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }
    @Override
    public String toString() {
        return "WithdrawRequestForm [bankId=" + bankId + ", accountNumber="
                + accountNumber
                + ", bankBranch="
                + bankBranch + ", amount=" + amount + "]";
    }

    public static class OperatorWithdrawRequestForm extends WithdrawRequestForm {
        @NotNull
        @ApiModelProperty(value = "player id", required = true)
        private int playerId;

        public int getPlayerId() {
            return playerId;
        }

        public void setPlayerId(int playerId) {
            this.playerId = playerId;
        }
    }
}
