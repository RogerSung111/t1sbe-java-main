package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.promo.PromoTypeEnum;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WithdrawCondition {
    private Long id;

    private Integer playerId;

    private WithdrawConditionSourceTypeEnum sourceType;

    private Integer sourceId;

    private PromoTypeEnum promoType;

    private Integer promotionId;

    private BigDecimal betRequired;

    private BigDecimal betAmount;

    private BigDecimal lockedOnAmount;

    private LocalDateTime beginAt;

    private WithdrawConditionStatusEnum status;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public WithdrawConditionSourceTypeEnum getSourceType() {
        return sourceType;
    }

    public void setSourceType(WithdrawConditionSourceTypeEnum sourceType) {
        this.sourceType = sourceType;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public PromoTypeEnum getPromoType() {
        return promoType;
    }

    public void setPromoType(PromoTypeEnum promoType) {
        this.promoType = promoType;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public BigDecimal getBetRequired() {
        return betRequired;
    }

    public void setBetRequired(BigDecimal betRequired) {
        this.betRequired = betRequired;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getLockedOnAmount() {
        return lockedOnAmount;
    }

    public void setLockedOnAmount(BigDecimal lockedOnAmount) {
        this.lockedOnAmount = lockedOnAmount;
    }

    public LocalDateTime getBeginAt() {
        return beginAt;
    }

    public void setBeginAt(LocalDateTime beginAt) {
        this.beginAt = beginAt;
    }

    public WithdrawConditionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(WithdrawConditionStatusEnum status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
