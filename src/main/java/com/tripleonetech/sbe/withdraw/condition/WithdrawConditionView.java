package com.tripleonetech.sbe.withdraw.condition;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;

import com.tripleonetech.sbe.common.DateUtils;

public class WithdrawConditionView {

    private Integer id;

    private Integer playerId;

    private WithdrawConditionSourceTypeEnum sourceType;

    private Integer sourceId;

    private Integer promoType;

    private Integer promotionId;

    private BigDecimal betRequired;

    private BigDecimal betAmount;

    private BigDecimal lockedOnAmount;

    private LocalDateTime beginAt;

    private WithdrawConditionStatusEnum status;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public WithdrawConditionSourceTypeEnum getSourceType() {
        return sourceType;
    }

    public void setSourceType(WithdrawConditionSourceTypeEnum sourceType) {
        this.sourceType = sourceType;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getPromoType() {
        return promoType;
    }

    public void setPromoType(Integer promoType) {
        this.promoType = promoType;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public BigDecimal getBetRequired() {
        return betRequired;
    }

    public void setBetRequired(BigDecimal betRequired) {
        this.betRequired = betRequired;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getLockedOnAmount() {
        return lockedOnAmount;
    }

    public void setLockedOnAmount(BigDecimal lockedOnAmount) {
        this.lockedOnAmount = lockedOnAmount;
    }

    public LocalDateTime getBeginAt() {
        return beginAt;
    }

    public void setBeginAt(LocalDateTime beginAt) {
        this.beginAt = beginAt;
    }

    public WithdrawConditionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(WithdrawConditionStatusEnum status) {
        this.status = status;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public static List<WithdrawConditionView> createList(List<WithdrawCondition> withdrawConditions) {
        List<WithdrawConditionView> viewRows = new ArrayList<>();
        if (CollectionUtils.isEmpty(withdrawConditions)) {
            return viewRows;
        }

        for (WithdrawCondition withdrawCondition : withdrawConditions) {
            viewRows.add(create(withdrawCondition));
        }

        return viewRows;
    }

    public static WithdrawConditionView create(WithdrawCondition withdrawCondition) {
        WithdrawConditionView viewRow = new WithdrawConditionView();

        BeanUtils.copyProperties(withdrawCondition, viewRow);
        viewRow.setUpdatedAt(DateUtils.localToZoned(withdrawCondition.getUpdatedAt()));
        viewRow.setCreatedAt(DateUtils.localToZoned(withdrawCondition.getCreatedAt()));

        return viewRow;
    }

}
