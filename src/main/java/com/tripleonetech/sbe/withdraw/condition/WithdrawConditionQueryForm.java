package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class WithdrawConditionQueryForm extends PageQueryForm {
    @NotNull
    @ApiModelProperty(value = "Player ID", required = true)
    private Integer playerId;
    @NotNull
    @ApiModelProperty(value = "Currency", required = true)
    private SiteCurrencyEnum currency;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

}
