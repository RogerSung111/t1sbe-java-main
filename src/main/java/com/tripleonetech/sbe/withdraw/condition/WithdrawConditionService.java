package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.game.log.GameLogMapper;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WithdrawConditionService {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawConditionService.class);

    @Autowired
    private WithdrawConditionMapper withdrawConditionMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private GameLogHourlyReportMapper hourlyReportMapper;
    @Autowired
    private GameLogMapper gameLogMapper;

    private BigDecimal CLEAR_THRESHOLD = BigDecimal.ZERO;

    public void create(WithdrawCondition condition) {
        withdrawConditionMapper.insert(condition);
    }

    public BigDecimal getValidBalance(Integer playerId) {
        BigDecimal totalBalance = walletMapper.getBalanceSumByPlayerId(playerId);

        // Clear any pending withdraw conditions if balance falls below threshold (currently 0)
        if (CLEAR_THRESHOLD.compareTo(totalBalance) >= 0) {
            List<WithdrawCondition> unsettledConditions = withdrawConditionMapper
                    .getUnsettledConditionsForPlayer(playerId);
            List<Long> conditionIds = unsettledConditions.stream().map(WithdrawCondition::getId)
                    .collect(Collectors.toList());
            if(conditionIds.size() > 0) {
                withdrawConditionMapper.batchUpdateStatus(conditionIds, WithdrawConditionStatusEnum.CLEARED);
            }
        }

        BigDecimal totalLocked = withdrawConditionMapper.getLockedOnAmountSum(playerId);
        return totalBalance.subtract(totalLocked).max(BigDecimal.ZERO);
    }

    public void processWithdrawConditions() {
        List<WithdrawCondition> unsettledConditions = withdrawConditionMapper
                .getAllUnsettledConditions();
        BigDecimal thisHourLeftBet = null;
        BigDecimal hourlyLogBet = null;
        BigDecimal totalBet = null;
        for (WithdrawCondition record : unsettledConditions) {
            Integer playerId = record.getPlayerId();
            LocalDateTime beginAt = record.getBeginAt();
            thisHourLeftBet = gameLogMapper.thisHourLeftBet(playerId, beginAt);
            hourlyLogBet = hourlyReportMapper.totalBetSinceBeginAt(playerId, beginAt);
            totalBet = thisHourLeftBet.add(hourlyLogBet);
            withdrawConditionMapper.updateBetAmount(record.getId(), totalBet);

            if (totalBet.compareTo(record.getBetRequired()) < 0) {
                continue;
            }
            withdrawConditionMapper.updateStatus(record.getId(), WithdrawConditionStatusEnum.COMPLETED);
        }
    }

    public int cancel(Long id) {
        return withdrawConditionMapper.updateStatus(id, WithdrawConditionStatusEnum.CANCELED);
    }
}
