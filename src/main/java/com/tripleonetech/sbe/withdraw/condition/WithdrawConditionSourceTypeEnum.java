package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum WithdrawConditionSourceTypeEnum implements BaseCodeEnum {
    DEPOSIT(0), BONUS(1);

    private int code;

    WithdrawConditionSourceTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
