package com.tripleonetech.sbe.withdraw.condition;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum WithdrawConditionStatusEnum implements BaseCodeEnum {
    OPEN(0), COMPLETED(1),
    CLEARED(10), // Cleared condition due to balance falling below threshold
    CANCELED(11); // Manually canceled condition

    private int code;

    WithdrawConditionStatusEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
