package com.tripleonetech.sbe.withdraw.condition;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WithdrawConditionMapper {

    int insert(WithdrawCondition record);

    List<WithdrawCondition> getUnsettledConditionsForPlayer(@Param("playerId") Integer playerId);

    List<WithdrawCondition> getUnsettledConditions(@Param("queryForm") WithdrawConditionQueryForm queryForm,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    List<WithdrawCondition> getAllUnsettledConditions();

    BigDecimal getLockedOnAmountSum(Integer playerId);

    int updateBetAmount(@Param("id") Long id, @Param("betAmount") BigDecimal betAmount);

    int updateStatus(@Param("id") Long id, @Param("status") WithdrawConditionStatusEnum status);

    int batchUpdateStatus(@Param("conditionIds") List<Long> conditionIds,
            @Param("status") WithdrawConditionStatusEnum status);

}