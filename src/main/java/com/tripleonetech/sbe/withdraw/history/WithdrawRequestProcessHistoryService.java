package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflow;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSetting;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class WithdrawRequestProcessHistoryService {

    @Autowired
    private WithdrawWorkflowSettingMapper workflowSettingMapper;

    @Autowired
    private WithdrawRequestProcessHistoryMapper withdrawRequestProcessHistoryMapper;

    public void addWorkflowProcessHistory(WithdrawStatusEnum fromStatus, WithdrawStatusEnum toStatus, String comment,
                                          Integer operatorId, Integer withdrawRequestId) {
        WithdrawRequestProcessHistory processHistory = new WithdrawRequestProcessHistory();
        processHistory.setWithdrawRequestId(withdrawRequestId);
        processHistory.setFromStatus(fromStatus);
        processHistory.setToStatus(toStatus);
        processHistory.setComment(comment);
        processHistory.setCreatedBy(operatorId);
        processHistory.setUpdatedBy(operatorId);
        withdrawRequestProcessHistoryMapper.insert(processHistory);
    }

    public List<WithdrawWorkflowSetting> getAvailableWithdrawRequestWorkflow(Integer withdrawRequestId) {
        List<WithdrawWorkflowSetting> availableWorkflowSettings = new ArrayList<>();

        List<WithdrawWorkflow> withdrawWorkflows = workflowSettingMapper.getWorkflowWithProcessHistory(withdrawRequestId);

        WithdrawWorkflow nextFlow = withdrawWorkflows.stream()
                .filter(f -> f.getWithdrawRequestProcessHistoryIds() == null)
                .filter(f -> WithdrawStatusEnum.REJECTED.getCode() != f.getId())
                .filter(f -> WithdrawStatusEnum.CANCELED.getCode() != f.getId())
                .findFirst().orElse(null);

        Map<Integer, WithdrawWorkflowSetting> workflowMap = withdrawWorkflows.stream()
                .collect(Collectors.toMap(WithdrawWorkflowSetting::getId, Function.identity()));

        if (nextFlow != null) {
            availableWorkflowSettings.add(nextFlow);
            availableWorkflowSettings.add(workflowMap.get(WithdrawStatusEnum.REJECTED.getCode()));
            availableWorkflowSettings.add(workflowMap.get(WithdrawStatusEnum.CANCELED.getCode()));
        }

        return availableWorkflowSettings;
    }

}
