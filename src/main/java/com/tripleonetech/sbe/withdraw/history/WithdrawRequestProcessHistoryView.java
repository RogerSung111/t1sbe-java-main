package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class WithdrawRequestProcessHistoryView {

    @ApiModelProperty(value = "Original status of withdraw request, CREATED(1),SYSTEM_REVIEW(2),REVIEW1(3),REVIEW2(4),REVIEW3(5),REVIEW4(6),REVIEW5(7),REVIEW6(8),PAYING(9),PAID(10),REJECTED(11),CANCELED(12)")
    private WithdrawStatusEnum fromStatus;

    @ApiModelProperty(value = "New status of withdraw request, CREATED(1),SYSTEM_REVIEW(2),REVIEW1(3),REVIEW2(4),REVIEW3(5),REVIEW4(6),REVIEW5(7),REVIEW6(8),PAYING(9),PAID(10),REJECTED(11),CANCELED(12)")
    private WithdrawStatusEnum toStatus;

    @ApiModelProperty(value = "Detail information about withdraw process")
    private String comment;

    @ApiModelProperty(value = "Create time of withdraw process history")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "Last updated time of withdraw process history")
    private LocalDateTime updatedAt;

    @ApiModelProperty(value = "Creator of withdraw process history")
    private IdNameView createdBy;

    @ApiModelProperty(value = "Last updater of withdraw process history")
    private IdNameView updatedBy;

    public WithdrawStatusEnum getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(WithdrawStatusEnum fromStatus) {
        this.fromStatus = fromStatus;
    }

    public WithdrawStatusEnum getToStatus() {
        return toStatus;
    }

    public void setToStatus(WithdrawStatusEnum toStatus) {
        this.toStatus = toStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public IdNameView getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(IdNameView createdBy) {
        this.createdBy = createdBy;
    }

    public IdNameView getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(IdNameView updatedBy) {
        this.updatedBy = updatedBy;
    }

}
