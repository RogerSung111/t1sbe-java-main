package com.tripleonetech.sbe.withdraw.history;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WithdrawRequestProcessHistoryMapper {

    void insert(WithdrawRequestProcessHistory requestHistory);

    List<WithdrawRequestProcessHistoryView> getComments(@Param("withdrawRequestId") Integer withdrawRequestId);

    WithdrawRequestProcessHistory get(Integer id);

    List<WithdrawRequestProcessHistory> getWorkflowHistory(Integer withdrawRequestId);
}
