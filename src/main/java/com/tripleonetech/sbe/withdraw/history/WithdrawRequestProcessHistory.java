package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;

import java.time.LocalDateTime;

public class WithdrawRequestProcessHistory {

    private Integer id;

    private Integer withdrawRequestId;

    private String comment;

    private WithdrawStatusEnum fromStatus;

    private WithdrawStatusEnum toStatus;

    private LocalDateTime createdAt;

    private Integer createdBy;

    private LocalDateTime updatedAt;

    private Integer updatedBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WithdrawStatusEnum getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(WithdrawStatusEnum fromStatus) {
        this.fromStatus = fromStatus;
    }

    public WithdrawStatusEnum getToStatus() {
        return toStatus;
    }

    public void setToStatus(WithdrawStatusEnum toStatus) {
        this.toStatus = toStatus;
    }

    public Integer getWithdrawRequestId() {
        return withdrawRequestId;
    }

    public void setWithdrawRequestId(Integer withdrawRequestId) {
        this.withdrawRequestId = withdrawRequestId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "WithdrawRequestProcessHistory [id=" + id + ", withdrawRequestId=" + withdrawRequestId + ", comment="
                + comment + ", createdAt=" + createdAt + ", createdBy=" + createdBy + ", updatedAt=" + updatedAt
                + ", updatedBy=" + updatedBy + "]";
    }


}


