package com.tripleonetech.sbe.withdraw.history;

import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class WithdrawRequestProcessHistoryForm {
    @NotNull
    @ApiModelProperty(value = "Next status in withdraw workflow")
    private WithdrawStatusEnum nextStatus;
    @ApiModelProperty(value = "Comment")
    private String comment;

    public WithdrawStatusEnum getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(WithdrawStatusEnum nextStatus) {
        this.nextStatus = nextStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
