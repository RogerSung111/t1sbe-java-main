package com.tripleonetech.sbe.web.common;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.promo.cashback.CashbackExecutionTypeEnum;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSetting;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingMapper;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingView;
import com.tripleonetech.sbe.common.web.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.*;
import java.util.Date;


@Profile({"main", "api"})
@RestController
@RequestMapping("/promotions/cashback-setting")
@Api(tags = {"Site Info", "Promotion - Cashback"})
public class CashbackSettingController {
    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;
    @Autowired
    private Constant constant;

    @GetMapping("")
    @ApiOperation("Global cashback settings, including modifiable settings like calculation type, and read-only settings " +
            "like calculating time of daily cashback. Some read-only settings can only be modified by SA by changing " +
            "properties file.")
    public RestResponse<PromoCashbackSettingView> getCashbackSetting() {
        PromoCashbackSettingView view = new PromoCashbackSettingView();

        // Query cashbackSetting properties
        PromoCashbackSetting setting = promoCashbackSettingMapper.get();
        BeanUtils.copyProperties(setting, view);

        // Calculate start Time of Game logs (setting is depended on customer's time zone)
        LocalTime startTime = LocalTime.of(constant.getCashback().getCalculationTime(), 0);
        LocalDateTime startDateTime = LocalDateTime.of(LocalDate.now().minusDays(1),startTime);
        ZonedDateTime utcStartDateTime = startDateTime.atZone(ZoneId.of(constant.getTimezone()))
                .withZoneSameInstant(ZoneId.systemDefault());
        view.setDailyCashbackStartTime(utcStartDateTime.toLocalDateTime());

        // Calculate next execution type based on known information above
        String cashbackSchedule = "";
        if(view.getExecutionType() == CashbackExecutionTypeEnum.DAILY.getCode()) {
            cashbackSchedule = constant.getCashback().getDailySchedule();
        }else {
            cashbackSchedule = constant.getCashback().getRealtimeSchedule();
        }
        CronSequenceGenerator generator = new CronSequenceGenerator(cashbackSchedule);
        LocalDateTime nextCalculationTime = generator.next(
                new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        view.setNextCalculationTime(nextCalculationTime);

        // Return the object with all info populated
        return RestResponse.ok(view);
    }
}
