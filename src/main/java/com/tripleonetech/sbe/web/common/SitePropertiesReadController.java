package com.tripleonetech.sbe.web.common;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.site.SiteProperty;
import com.tripleonetech.sbe.site.SitePropertyKey.LiveChatInfoEnum;
import com.tripleonetech.sbe.site.SitePropertyKey.SiteInfoEnum;
import com.tripleonetech.sbe.site.SitePropertyMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Profile({"main", "api"})
@RestController
@RequestMapping("/site-properties")
@Api(tags = {"Site Properties"})
public class SitePropertiesReadController {
    @Autowired
    private SitePropertyMapper sitePropertyMapper;

    @GetMapping("/live-chat")
    @ApiOperation("Get customer service contact information")
    public RestResponse<LiveChatInfoForm> getLiveChatInfo() throws Exception {
        List<SiteProperty> list = sitePropertyMapper.list(LiveChatInfoEnum.values());
        LiveChatInfoForm view = SiteProperty.setFields(list, LiveChatInfoForm.class);
        return RestResponse.ok(view);
    }

    @GetMapping("/site-info")
    @ApiOperation("Get site information")
    public RestResponse<SiteInfoForm> getSiteInfo() throws Exception {
        List<SiteProperty> list = sitePropertyMapper.list(SiteInfoEnum.values());
        SiteInfoForm view = SiteProperty.setFields(list, SiteInfoForm.class);
        return RestResponse.ok(view);
    }

    public static class LiveChatInfoForm {
        @ApiModelProperty(value = "Live Chat URL")
        private String liveChatUrl;
        @ApiModelProperty(value = "Live Chat Code")
        private String liveChatCode;

        public String getLiveChatUrl() {
            return liveChatUrl;
        }

        public void setLiveChatUrl(String liveChatUrl) {
            this.liveChatUrl = liveChatUrl;
        }

        public String getLiveChatCode() {
            return liveChatCode;
        }

        public void setLiveChatCode(String liveChatCode) {
            this.liveChatCode = liveChatCode;
        }
    }

    public static class SiteInfoForm {
        @ApiModelProperty(value = "site title")
        private String title;

        @ApiModelProperty(value = "site logo image url")
        private String logo;

        @ApiModelProperty(value = "site slogan")
        private String slogan;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getSlogan() {
            return slogan;
        }

        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }
    }

}
