package com.tripleonetech.sbe.web.common;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigView;
import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiUtils;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.creditpoint.CreditPointMapper;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameTypeMapper;
import com.tripleonetech.sbe.game.GameTypeView;
import com.tripleonetech.sbe.game.GameTypeWithGameApi;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.game.log.GameApiIdNameView;
import com.tripleonetech.sbe.payment.Bank;
import com.tripleonetech.sbe.payment.BankMapper;
import com.tripleonetech.sbe.payment.PaymentApiFactory;
import com.tripleonetech.sbe.payment.config.PaymentApiConfig;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigView;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.privilege.SiteCurrencyView;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import com.tripleonetech.sbe.privilege.SiteTemplateView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/site-config")
@Profile(value = {"main", "api"})
@Api(tags = {"Site Info"})
public class SiteController {

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    @Autowired
    private PaymentApiConfigMapper paymentApiConfigMapper;

    @Autowired
    private GameTypeMapper gameTypeMapper;

    @Autowired
    private CreditPointMapper creditPointMapper;
    @Autowired
    private Constant constant;

    @Autowired
    private GameApiFactory gameApiFactory;

    @Autowired
    private PaymentApiFactory paymentApiFactory;

    @Autowired
    private BankMapper bankMapper;


    @GetMapping("/currencies")
    @ApiOperation("List of currencies supported by the site")
    public RestResponse<List<SiteCurrencyView>> getCurrencies() {
        List<SiteCurrencyView> currencyView = new ArrayList<>();
        List<SitePrivilege> currencies = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_CURRENCY);
        currencies.forEach(currency -> {
            currencyView.add(SiteCurrencyView.create(currency));
        });
        return RestResponse.ok(currencyView);
    }

    @GetMapping("/templates")
    @ApiOperation("List of template supported by the site")
    public RestResponse<List<SiteTemplateView>> getTemplates() {
        List<SitePrivilege> templates = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_TEMPLATE);
        return RestResponse.ok(templates.stream().map(SiteTemplateView::create).collect(Collectors.toList()));
    }

    @GetMapping("/languages")
    @ApiOperation("List of languages supported by the site")
    public RestResponse<List<String>> getLanguages() {
        List<String> languages = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.SITE_LANGUAGE);
        return RestResponse.ok(languages);
    }

    @GetMapping("/time-zone")
    @ApiOperation("The default time zone setting for the site")
    public RestResponse<String> getTimeZone() {
        return RestResponse.ok(constant.getTimezone());
    }

    @GetMapping("/game-apis")
    @ApiOperation("List of game APIs available and enabled for the site")
    public RestResponse<List<ApiConfigView>> getGameApis() {
        LocaleAwareQuery localeQuery = new LocaleAwareQuery() {};

        List<String> gameApisCodes = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.GAME_API);
        List<GameApiConfig> gameApiConfigs = new ArrayList<>();
        List<ApiConfigView> gameApiConfigViews = new ArrayList<>();
        List<GameTypeWithGameApi> listGameTypes = gameTypeMapper.listGameTypeWithGameApiByGameCodes(localeQuery, gameApisCodes);

        
        if (CollectionUtils.isNotEmpty(gameApisCodes)) {
            gameApiConfigs = gameApiConfigMapper.listGameApiConfigByCode(gameApisCodes, true);
        }

        for (GameApiConfig gameApiConfig : gameApiConfigs) {
            List<IdNameView> gameTypes = listGameTypes.stream()
                    .filter(gameType -> gameType.getGameApiCode().equals(gameApiConfig.getCode()))
                    .map(gameType -> IdNameView.create(gameType.getGameTypeId(), gameType.getGameTypeName()))
                    .collect(Collectors.toList());
            GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiConfig.getId());
            gameApiConfigViews.add(ApiConfigView.from(gameApiConfig, gameApi.isOnline(), gameTypes));
        }

        return RestResponse.ok(gameApiConfigViews);
    }


    @GetMapping("/available-game-apis")
    @ApiOperation("List of available game API metaFields for the site")
    public RestResponse<Map<String, List<ApiMetaField>>> getAvailableGameApis() {
        List<String> gameApisCode = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.GAME_API);
        Map<String, List<ApiMetaField>> metaFields = ApiUtils.getMetaFieldsByApiType(ApiTypeEnum.GAME);
        metaFields.keySet().retainAll(gameApisCode);

        return RestResponse.ok(metaFields);
    }

    @GetMapping("/payment-apis")
    @ApiOperation("List of payment APIs available and enabled for the site")
    public RestResponse<List<PaymentApiConfigView>> getPaymentApis() {
        List<PaymentApiConfig> paymentApiConfigs = paymentApiConfigMapper.list();
        List<PaymentApiConfigView> paymentApiConfigViews = new ArrayList<>();

        for (PaymentApiConfig paymentApiConfig : paymentApiConfigs) {
            PaymentApi api = paymentApiFactory.getById(paymentApiConfig.getId());
            paymentApiConfigViews.add(PaymentApiConfigView.from(paymentApiConfig, api.isOnline()));
        }

        return RestResponse.ok(paymentApiConfigViews);
    }

    @GetMapping("/available-payment-apis")
    @ApiOperation("List of available payment API metaFields for the site")
    public RestResponse<Map<String, List<ApiMetaField>>> getAvailablePaymentApis() {
        Map<String, List<ApiMetaField>> metaFields = ApiUtils.getMetaFieldsByApiType(ApiTypeEnum.PAYMENT);

        return RestResponse.ok(metaFields);
    }

    @GetMapping("/game-types")
    public RestResponse<List<GameTypeView>> getGameTypes() {
        Locale locale = LocaleContextHolder.getLocale();
        String defaultLanguage = constant.getFrontend().getDefaultLocale();
        List<GameTypeView> gameTypes = gameTypeMapper.listWithGameCount(locale.toLanguageTag(), defaultLanguage);
        for(GameTypeView gameType : gameTypes) {
            gameType.setGameApis(gameApiConfigMapper.listGameApiByGameTypeId(gameType.getId())
                    .stream().map(GameApiIdNameView::create).collect(Collectors.toList()));
        }

        return RestResponse.ok(gameTypes);
    }

    @GetMapping("/credit-point")
    @ApiOperation("Query credit points of the site")
    public RestResponse<BigDecimal> getCreditPoint() {
        return RestResponse.ok(creditPointMapper.get());
    }

    @GetMapping("/game-log-search-limit")
    @ApiOperation("GameLog reports searching time range limits, in days")
    public RestResponse<Map<String, Integer>> getGameLogQueryLimits() {
        Map<String, Integer> gamelogReportLimits = new HashMap<>();
        gamelogReportLimits.put("gamelogReportSearchLimit", Constant.getInstance().getReport().getGameLogDayRange());
        gamelogReportLimits.put("gamelogSummaryReportSearchLimit", Constant.getInstance().getReport().getGameLogSummaryDayRange());
        return RestResponse.ok(gamelogReportLimits);
    }

    @GetMapping("/banks")
    @ApiOperation("List of available banks")
    public RestResponse<List<Bank>> getBankList() {
        List<Bank> banks = bankMapper.list();
        return RestResponse.ok(banks);
    }

}
