package com.tripleonetech.sbe.web.opbackend;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.payment.PaymentApiFactory;
import com.tripleonetech.sbe.payment.config.PaymentApiConfig;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Profile("main")
@RestController
@Api(tags = { "z-Callback" })
public class DepositCallbackController {
    private static Logger logger = LoggerFactory.getLogger(DepositCallbackController.class);
    @Autowired
    private PaymentApiConfigMapper paymentApiConfigMapper;
    @Autowired
    private PaymentApiFactory paymentApiFactory;

    @PostMapping(path = "/payment-callback/{apiId}", consumes = "application/x-www-form-urlencoded")
    @ApiOperation("Handles callback from payment API")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "apiId", value = "Payment API ID"),
            @ApiImplicitParam(required = true, paramType = "body", name = "requestParams", value = "Url encoded representation of parameters needed by API")
    })
    public String callbackForFormUrlcoded(@PathVariable Integer apiId, @RequestParam Map<String, String> requestParams)
            throws RestResponseException {
        return callback(apiId, requestParams);
    }

    @PostMapping(path = "/payment-callback/{apiId}", consumes = "application/json")
    @ApiOperation("Handles callback from payment API")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "apiId", value = "Payment API ID"),
            @ApiImplicitParam(required = true, paramType = "body", name = "requestParams", value = "JSON representation of parameters needed by API")
    })
    public String callbackForApplicationJson(@PathVariable Integer apiId, @RequestBody Map<String, String> requestParams)
            throws RestResponseException {
        return callback(apiId, requestParams);
    }

    private String callback(Integer apiId, Map<String, String> requestParams)
            throws RestResponseException {
        PaymentApiConfig apiConfig = paymentApiConfigMapper.get(apiId);

        if (null == apiConfig) {
            logger.error("Callkback - API_ID:[{}] cannot be found, Parameters :[{}]", apiId, JsonUtils.objectToJson(requestParams));
            throw new RestResponseException(RestResponse.entityNotFound("apiConfig not found with config ID: " + apiId));
        }

        PaymentApi paymentApi = paymentApiFactory.getById(apiId);
        logger.info("Callback - Payment API:[{}], Parameters : [{}]", apiConfig.getName(), JsonUtils.objectToJson(requestParams));
        PaymentApiResult apiResult = (PaymentApiResult) paymentApi.callback(requestParams);
        if (!apiResult.isSuccess()) {
            throw new RestResponseException(RestResponse.apiFailed(apiResult.getMessage()));
        }
        return apiResult.getCallbackReturnMessage();
    }

}
