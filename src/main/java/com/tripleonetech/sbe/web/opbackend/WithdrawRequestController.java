package com.tripleonetech.sbe.web.opbackend;


import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.withdraw.*;
import com.tripleonetech.sbe.withdraw.WithdrawRequestForm.OperatorWithdrawRequestForm;
import com.tripleonetech.sbe.withdraw.history.WithdrawRequestProcessHistoryForm;
import com.tripleonetech.sbe.withdraw.history.WithdrawRequestProcessHistoryMapper;
import com.tripleonetech.sbe.withdraw.history.WithdrawRequestProcessHistoryService;
import com.tripleonetech.sbe.withdraw.history.WithdrawRequestProcessHistoryView;
import com.tripleonetech.sbe.withdraw.integration.WithdrawApi;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSetting;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/withdraw-requests")
@Profile("main")
@Api(tags = {"Deposits & Withdrawals"})
public class WithdrawRequestController extends BaseOpbackendController {
    private final static Logger logger = LoggerFactory.getLogger(WithdrawRequestController.class);

    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;
    @Autowired
    private WithdrawApiFactory withdrawApiFactory;
    @Autowired
    private WithdrawRequestProcessHistoryService withdrawRequestProcessHistoryService;
    @Autowired
    private WithdrawRequestService withdrawRequestService;
    @Autowired
    private WithdrawRequestProcessHistoryMapper withdrawRequestProcessHistoryMapper;

    @GetMapping("")
    @ApiOperation("Query withdraw requests. Allowed sort columns: amount, updatedAt, requestedDate (default)")
    public RestResponse<PageInfo<WithdrawRequestView>> list(WithdrawRequestQueryForm queryForm) {
        PageInfo<WithdrawRequestView> withdrawRequests =
                new PageInfo<>(withdrawRequestMapper.list(queryForm, queryForm.getPage(), queryForm.getLimit()));
        return RestResponse.ok(withdrawRequests);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get the detail of withdraw request")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of withdraw request")
    })
    public RestResponse<WithdrawRequestView> detail(@PathVariable long id) {
        return RestResponse.ok(withdrawRequestMapper.getView(id));
    }

    @GetMapping("/{id}/workflow")
    @ApiOperation("Get available workflow steps of withdraw request")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of withdraw request")
    })
    public RestResponse<List<WithdrawWorkflowSettingView>> getAvailableWorkflowSettingsOfWithdraw(@PathVariable Integer id) {
        List<WithdrawWorkflowSetting> workflowSettings = withdrawRequestProcessHistoryService.getAvailableWithdrawRequestWorkflow(id);
        List<WithdrawWorkflowSettingView> view = workflowSettings.stream()
                .map(WithdrawWorkflowSettingView::from)
                .collect(Collectors.toList());
        return RestResponse.ok(view);
    }

    @Transactional
    @PostMapping("/create")
    @ApiOperation("Submit a withdraw request")
    public RestResponse<RestResponseIdSuccess> createWithdrawRequest(
            @Valid @RequestBody OperatorWithdrawRequestForm form) throws RestResponseException {
        Operator operator = getLoginOperator();

        WithdrawRequest withdrawRequest = WithdrawRequest.from(form);

        if (!withdrawRequestService.newWithdrawal(withdrawRequest)) {
            throw new RestResponseException(RestResponse.operationFailed("freeze amount"));
        }
        String comment = "Created withdraw request by operator : " + operator.getUsername();
        withdrawRequestProcessHistoryService.addWorkflowProcessHistory(WithdrawStatusEnum.CREATED,
                WithdrawStatusEnum.CREATED, comment, operator.getId(), withdrawRequest.getId());

        return RestResponse.createSuccess(withdrawRequest.getId());
    }

    @Transactional
    @PostMapping(value = "/{id}")
    @ApiOperation("Update withdraw request according to the approval workflow step, and update wallet balance according to the target status. " +
            "All available workflow steps are: CREATE, SYSTEM_REVIEW, REVIEW1, REVIEW2, REVIEW3, REVIEW4, REVIEW5, REVIEW6, PAYING, PAID, REJECTED. " +
            "Actual available workflow steps available to a specific with GET /withdraw-requests/{id}/workflow")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of withdraw request")
    })
    public RestResponse<RestResponseDataSuccess> updateWithdrawRequest(@PathVariable Long id,
                                                                       @Valid @RequestBody WithdrawRequestProcessHistoryForm form) {
        WithdrawRequest withdrawRequest = withdrawRequestMapper.get(id);
        Operator operator = getLoginOperator();
        boolean isSuccessOperation;

        WithdrawStatusEnum nextStatus = form.getNextStatus();
        switch (nextStatus) {
            case PAID:
                isSuccessOperation = withdrawRequestService.approveWithdrawRequest(withdrawRequest);
                break;
            case REJECTED:
                isSuccessOperation = withdrawRequestService.rejectWithdrawRequest(withdrawRequest);
                break;
            default:
                isSuccessOperation = true;
        }

        if (!isSuccessOperation) {
            return RestResponse.operationFailed(String.format(
                    "Failed updating player's balance and withdraw request! Request ID: %s, target status %s", id,
                    nextStatus));
        } else {
            withdrawRequestProcessHistoryService.addWorkflowProcessHistory(withdrawRequest.getStatus(), nextStatus,
                    form.getComment(), operator.getId(), withdrawRequest.getId());
            return RestResponse.operationSuccess();
        }
    }

    @PostMapping("/submit-to-api/{apiId}/{withdrawRequestId}")
    @ApiOperation("Submit withdraw request to payment API")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "apiId", value = "The ID of withdraw API"),
            @ApiImplicitParam(required = true, paramType = "path", name = "withdrawRequestId", value = "The ID of withdraw request")
    })
    public RestResponse<RestResponseDataSuccess> submitWithdrawRequest(@PathVariable int apiId,
                                                                       @PathVariable long withdrawRequestId) {
        WithdrawRequest withdrawRequest = withdrawRequestMapper.get(withdrawRequestId);
        WithdrawApi withdrawApi = withdrawApiFactory.getById(apiId);

        //Call 3th-party API to pay to player
        ApiResult apiResult = withdrawApi.withdraw(withdrawRequest);
        if (!apiResult.isSuccess()) {
            logger.warn("Failed to submit withdraw request [{}] to API [{}]", withdrawRequestId, apiId);
            return RestResponse.apiFailed(apiResult.getMessage());
        }
        String comment = String.format("Submitted to API [%s]", withdrawApi.getApiCode());
        withdrawRequestProcessHistoryService.addWorkflowProcessHistory(withdrawRequest.getStatus(),
                WithdrawStatusEnum.PAYING, comment, getLoginOperator().getId(), withdrawRequest.getId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/callback/{apiId}/{withdrawRequestId}/{status}")
    @ApiOperation("Receive API callback. Note: This API is open to public, does not require oauth token.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "apiId", value = "The ID of withdraw API"),
            @ApiImplicitParam(required = true, paramType = "path", name = "withdrawRequestId", value = "The ID of withdraw request"),
            @ApiImplicitParam(required = true, paramType = "path", name = "status", value = "The status value provided by API"),
    })
    public RestResponse<RestResponseDataSuccess> callback(@PathVariable int apiId,
                                                          @PathVariable long withdrawRequestId,
                                                          @PathVariable int status) {

        WithdrawRequest request = withdrawRequestMapper.get(withdrawRequestId);
        Map<String, Object> param = new HashMap<>();
        param.put("withdrawRequestId", request.getId());
        param.put("status", status);

        WithdrawApi withdrawApi = withdrawApiFactory.getById(apiId);
        ApiResult apiResult = withdrawApi.callback(param);

        if (!apiResult.isSuccess()) {
            logger.warn("Failed to execute callback to withdraw API: [{}]", apiResult);
            return RestResponse.apiFailed(apiResult.getMessage());
        }
        String comment = "Callback by " + withdrawApi.getApiCode();
        withdrawRequestProcessHistoryService.addWorkflowProcessHistory(request.getStatus(), WithdrawStatusEnum.PAID,
                comment, getLoginOperator().getId(), request.getId());
        return RestResponse.operationSuccess();
    }

    @GetMapping("/{withdrawRequestId}/request-history")
    @ApiOperation("Get comments of withdraw request")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "withdrawRequestId", value = "The ID of withdraw request"),
    })
    public RestResponse<List<WithdrawRequestProcessHistoryView>> getComments(@PathVariable Integer withdrawRequestId) {
        List<WithdrawRequestProcessHistoryView> comments = withdrawRequestProcessHistoryMapper.getComments(withdrawRequestId);
        return RestResponse.ok(comments);
    }

    @GetMapping("/action-required-count")
    @ApiOperation("Get total count of pending withdrawal request")
    public RestResponse<Long> getPendingDepositCount() {
        return RestResponse.ok(withdrawRequestMapper.getPendingRequestCount());
    }
}
