package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingForm;
import com.tripleonetech.sbe.promo.cashback.PromoCashbackSettingMapper;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@Profile("main")
@RestController
@RequestMapping("/promotions/cashback-setting")
@Api(tags = {"Promotion - Cashback"})
public class PromoCashbackSettingController {
    @Autowired
    private PromoCashbackSettingMapper promoCashbackSettingMapper;

    @PostMapping("")
    @ApiOperation("Update global cashback settings. executionType: DAILY(1), REALTIME(2)")
    public RestResponse<RestResponseDataSuccess> updateCashbackSetting(
            @Valid @RequestBody PromoCashbackSettingForm form) {
        promoCashbackSettingMapper.update(form);
        return RestResponse.operationSuccess();
    }
}
