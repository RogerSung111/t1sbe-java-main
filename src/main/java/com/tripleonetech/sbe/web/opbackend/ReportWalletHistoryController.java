package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.report.wallethistory.WalletHistoryReport;
import com.tripleonetech.sbe.report.wallethistory.WalletHistoryReportMapper;
import com.tripleonetech.sbe.report.wallethistory.WalletHistoryReportQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reports/wallet-history")
@Profile("main")
@Api(value = "Wallet History Report", tags = { "Reports" })
public class ReportWalletHistoryController {
    //private final static Logger logger = LoggerFactory.getLogger(ReportWalletHistoryController.class);

    @Autowired
    private WalletHistoryReportMapper mapper;

    @GetMapping("")
    @ApiOperation("Query wallet history report")
    public RestResponse<PageInfo<WalletHistoryReport>> list(@Valid WalletHistoryReportQueryForm query) {
        List<WalletHistoryReport> list = mapper.query(query, query.getPage(), query.getLimit());
        return RestResponse.ok(new PageInfo<>(list));
    }
}