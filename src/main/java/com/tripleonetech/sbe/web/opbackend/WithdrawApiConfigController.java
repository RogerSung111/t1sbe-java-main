package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfig;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfigMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/withdraw-api")
@Profile("main")
@Api(tags = { "API Configuration - Withdraw" })
public class WithdrawApiConfigController extends ApiConfigController {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawApiConfigController.class);

    @Autowired
    private WithdrawApiConfigMapper withdrawApiMapper;

    @Override
    public ApiTypeEnum getApiType() {
        return ApiTypeEnum.WITHDRAW;
    }

    @GetMapping("")
    @ApiOperation("List all withdraw API configs")
    public RestResponse<List<WithdrawApiConfig>> list() {
        return RestResponse.ok(withdrawApiMapper.list());
    }

    @PostMapping("/{id}")
    @ApiOperation("Edit withdraw API config")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<RestResponseDataSuccess> editSubmit(
            @PathVariable Integer id, @RequestBody WithdrawApiConfig withdrawApiConfig) {
        withdrawApiMapper.update(withdrawApiConfig);
        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/{id}/status")
    @ApiOperation("Update withdraw API config. Available status: DISABLED(0), ENABLED(1), DELETED(20)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(
            @PathVariable Integer id, @RequestBody ApiConfigStatusForm form) {
        WithdrawApiConfig withdrawApiConfig = withdrawApiMapper.get(id);

        ApiConfigStatusEnum newApiStatus = ApiConfigStatusEnum.valueOf(form.getStatus());
        if(newApiStatus == null) {
            return RestResponse.operationFailed("Wrong API status");
        }
        if(withdrawApiConfig.getStatus().equals(ApiConfigStatusEnum.INACTIVE)
                || withdrawApiConfig.getStatus().equals(ApiConfigStatusEnum.DELETED)) {
            
            return RestResponse.operationFailed(String.format("Cannot switch API status to [%s], because the status of [%s] is [%s]",
                    newApiStatus, withdrawApiConfig.getName(), withdrawApiConfig.getStatus()));
        }
        withdrawApiConfig.setStatus(newApiStatus);
        withdrawApiMapper.update(withdrawApiConfig);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete a withdraw API config")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable Integer id) {
        withdrawApiMapper.delete(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/create")
    @ApiOperation("Create withdraw API config")
    public RestResponse<RestResponseIdSuccess> createSubmit(@RequestBody WithdrawApiConfig withdrawApiConfig) {
        withdrawApiMapper.insert(withdrawApiConfig);
        return RestResponse.createSuccess(withdrawApiConfig.getId());
    }
}
