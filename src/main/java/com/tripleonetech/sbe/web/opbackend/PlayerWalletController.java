package com.tripleonetech.sbe.web.opbackend;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameApiService;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.*;

import io.swagger.annotations.*;

@RestController
@RequestMapping("/players/{playerId}/wallets/{currency}")
@Profile("main")
@Api(tags = {"Player Wallet Management"})
public class PlayerWalletController extends BaseOpbackendController {
    private final static Logger logger = LoggerFactory.getLogger(PlayerWalletController.class);
    @Autowired
    private GameApiService gameApiService;
    @Autowired
    private WalletService walletService;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private GameApiFactory gameApiFactory;
    
    private static final String QUERY_PLAYER_FIALED = "Params are not specific to one player";

    @GetMapping("")
    @ApiOperation("All player wallet information, the record with NULL game API ID is main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player")
    })
    public RestResponse<PlayerWalletsView> list(@PathVariable int playerId,
                                                @PathVariable SiteCurrencyEnum currency) throws RestResponseException {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) == 0) {
            throw new RestResponseException(
                    RestResponse.badWalletRequest(
                            String.format("Player wallet not found for playerCredentialId [%s], currency [%s]",
                                    playerId, currency)));
        }

        PlayerWalletsView playerWallets = new PlayerWalletsView();
        PlayerIdView player = new PlayerIdView();
        player.setId(playerId);
        player.setUsername(players.get(0).getUsername());
        player.setCurrency(currency);
        playerWallets.setPlayer(player);

        Integer id = players.get(0).getId();
        playerWallets.addWallet(walletService.getMainWallet(id));

        Map<Integer, Wallet> walletMap = walletMapper.getActiveGameWalletsByPlayerId(id);
        SortedMap<Integer, Wallet> walletMapSorted = new TreeMap<>(walletMap);
        playerWallets.getWallets().addAll(
                walletMapSorted.values().stream()
                        .map(e -> GameWalletView.create(e, currency, gameApiService.allowDecimalTransfer(e.getGameApiId())))
                        .collect(Collectors.toList())
        );

        return RestResponse.ok(playerWallets);
    }

    @Transactional
    @PostMapping("/add-balance")
    @ApiOperation("Manually add amount to main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player")
    })
    public RestResponse<RestResponseDataSuccess> mainDeposit(
            @PathVariable int playerId, @PathVariable SiteCurrencyEnum currency,
            @Valid @RequestBody AmountForm form) {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Integer id = players.get(0).getId();
        walletService.addBalanceToMainWallet(id, form.getAmount(), getLoginOperator().getUsername(), getLoginOperator().getId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/deduct-balance")
    @ApiOperation("Manually deduct amount from main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player")
    })
    public RestResponse<RestResponseDataSuccess> mainWithdraw(@PathVariable int playerId,
                                                              @PathVariable SiteCurrencyEnum currency, @Valid @RequestBody AmountForm form) {
        if (BigDecimal.ZERO.compareTo(form.getAmount()) > 0) {
            return RestResponse.invalidParameter("The amount should be positive number.");
        }
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Integer id = players.get(0).getId();
        // check balance in main wallet
        BigDecimal mainWalletBalance = walletService.getMainWalletBalance(id);
        if (mainWalletBalance.compareTo(form.getAmount()) < 0) {
            return RestResponse.insufficientBalance("Insufficient balance, current main wallet balance = " + mainWalletBalance);
        }

        walletService.deductBalanceFromMainWallet(id, form.getAmount(), this.getLoginOperator().getUsername(), getLoginOperator().getId());

        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/{gameApiId}/transfer-in")
    @ApiOperation("Transfer amount to game balance from main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API")
    })
    public RestResponse<RestResponseDataSuccess> mainToSub(@PathVariable int playerId,
                                                           @PathVariable SiteCurrencyEnum currency,
                                                           @PathVariable int gameApiId,
                                                           @Valid @RequestBody AmountForm form) {
        GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiId);
        if(!ApiConfigStatusEnum.ENABLED.equals(gameApi.getStatus())) {
            return RestResponse.operationFailed("Cannot deposit amount into disabled game wallet"); 
        }
        
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) > 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Integer id = players.get(0).getId();
        walletService.depositGame(id, gameApiId, form.getAmount(), getLoginOperator().getId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/{gameApiId}/transfer-out")
    @ApiOperation("Transfer amount from game balance to main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API")
    })
    public RestResponse<RestResponseDataSuccess> subToMain(@PathVariable int playerId,
                                                           @PathVariable SiteCurrencyEnum currency,
                                                           @PathVariable int gameApiId,
                                                           @Valid @RequestBody AmountForm form) {
        GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiId);
        if(!ApiConfigStatusEnum.ENABLED.equals(gameApi.getStatus())) {
            return RestResponse.operationFailed("Cannot withdraw amount from disabled game wallet"); 
        }
        
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Integer id = players.get(0).getId();
        walletService.withdrawGame(id, gameApiId, form.getAmount(), getLoginOperator().getId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/{gameApiId}/transfer-in-all")
    @ApiOperation("Transfer all balance to specific sub-wallet from main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API to transfer all balance to")
    })
    public RestResponse<RestResponseDataSuccess> mainAllToSub(@PathVariable int playerId,
                                                              @PathVariable SiteCurrencyEnum currency, @PathVariable int gameApiId) {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Player player = players.get(0);

        BigDecimal amount = walletService.getMainWalletBalance(player.getId());
        walletService.depositGame(player.getId(), gameApiId, amount, getLoginOperator().getId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/{gameApiId}/transfer-out-all")
    @ApiOperation("Transfer all balance back to main wallet from specific sub-wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API to withdraw all balance from")
    })
    public RestResponse<RestResponseDataSuccess> subAllToMain(@PathVariable int playerId,
                                                              @PathVariable SiteCurrencyEnum currency, @PathVariable int gameApiId) {
        
        GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiId);
        if(!ApiConfigStatusEnum.ENABLED.equals(gameApi.getStatus())) {
            return RestResponse.operationFailed("Cannot withdraw amount from disabled game wallet"); 
        }
        
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Player player = players.get(0);

        BigDecimal amount = gameApiService.syncBalance(gameApiId, player).getBalance();
        walletService.withdrawGame(player.getId(), gameApiId, amount, getLoginOperator().getId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/collect-all-balances")
    @ApiOperation("Transfer all game balance back to main wallet (Note that this action iterates thru all wallets and will take considerable amount of time.)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player")
    })
    public RestResponse<RestResponseDataSuccess> subsAllToMain(@PathVariable int playerId,
                                                               @PathVariable SiteCurrencyEnum currency) throws RestResponseException {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Player player = players.get(0);
        List<Wallet> subWallets = new ArrayList<>(walletMapper.getAllGameWalletsByPlayerId(player.getId()).values());
        int[] gameApiIds = subWallets.stream().mapToInt(Wallet::getGameApiId).toArray();

        boolean allSuccess = true;
        boolean allFailure = true;
        List<RestResponseIdSuccess> idResultList = new ArrayList<>();
        for (Integer gameApiId : gameApiIds) {
            GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiId);
            if(!ApiConfigStatusEnum.ENABLED.equals(gameApi.getStatus())) {
                logger.debug("Exculde disbaled Game API : {}", gameApi.getApiCode());
                continue;
            }
            BigDecimal amount = gameApiService.syncBalance(gameApiId, player).getBalance();
            if (BigDecimal.ZERO.compareTo(amount) == 0) {
                continue;
            }

            Operator operator = getLoginOperator();
            ApiResult withdrawApiResult = walletService.withdrawGame(player.getId(), gameApiId, amount, operator == null ? null : operator.getId());
            RestResponseIdSuccess idSuccess = RestResponseIdSuccess.success(gameApiId);
            allFailure = allFailure && !withdrawApiResult.isSuccess();
            if (!withdrawApiResult.isSuccess()) {
                allSuccess = false;
                idSuccess = RestResponseIdSuccess.fail(gameApiId, withdrawApiResult.getResponseEnum().toString());
            }
            idResultList.add(idSuccess);
        }

        if (allFailure) {
            throw new RestResponseException(RestResponse.operationFailed(idResultList));
        } else if (!allSuccess) {
            throw new RestResponseException(RestResponse.multiStatus(idResultList));
        } else {
            return RestResponse.operationSuccess();
        }
    }

    @GetMapping("/{gameApiId}/get-balance")
    @ApiOperation("Returns game wallet information for the given game API. (Note: This method triggers communication with external game API and refreshes wallet balance if it's dirty.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency of the player"),
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API to check balance of")
    })
    public RestResponse<? extends Object> getCurrentBalance(@PathVariable int playerId,
                                                            @PathVariable SiteCurrencyEnum currency, @PathVariable int gameApiId) {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (CollectionUtils.size(players) != 1) {
            return RestResponse.invalidParameter(QUERY_PLAYER_FIALED);
        }

        Player player = players.get(0);

        // special case for getting main wallet balance
        if (gameApiId == 0) {
            Wallet mainWallet = walletService.getMainWallet(player.getId());
            return RestResponse.ok(GameWalletView.create(mainWallet, currency));
        }

        Wallet wallet = walletMapper.get(player.getId(), gameApiId);
        if (wallet == null) {
            return RestResponse.badWalletRequest("Wallet not found");
        }


        Wallet walletRefreshed = walletService.getCurrentBalance(player, wallet);
        if (walletRefreshed.isError()) {
            return RestResponse.badWalletRequest(walletRefreshed.getErrorCode());
        }
        GameWalletView gameWalletView = GameWalletView.create(walletRefreshed, currency,
                gameApiService.allowDecimalTransfer(walletRefreshed.getGameApiId()));
        return RestResponse.ok(gameWalletView);
    }

    public static class AmountForm {
        @ApiModelProperty(required = true, name = "amount", value = "amount")
        @NotNull(message = "amount is empty")
        private BigDecimal amount;

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    }
}
