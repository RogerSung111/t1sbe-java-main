package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionQueryForm;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionStatusEnum;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/players/withdraw-condition")
@Profile("main")
@Api(tags = {"Player Wallet Management"})
public class WithdrawConditionController {
    @Autowired
    private WithdrawConditionMapper withdrawConditionMapper;

    @GetMapping("")
    @ApiOperation("List OPEN withdraw conditions")
    public RestResponse<PageInfo<WithdrawConditionView>> list(@Valid WithdrawConditionQueryForm queryForm) {
        PageInfo<WithdrawConditionView> withdrawConditions = new PageInfo<>(WithdrawConditionView.createList(
                withdrawConditionMapper.getUnsettledConditions(queryForm, queryForm.getPage(),
                        queryForm.getLimit())));
        return RestResponse.ok(withdrawConditions);
    }

    @PostMapping("/{id}/cancel")
    @ApiOperation("Cancel withdraw condition by id")
    public RestResponse<RestResponseDataSuccess> cancel(@PathVariable Long id) {
        int updated = withdrawConditionMapper.updateStatus(id, WithdrawConditionStatusEnum.CANCELED);
        if (updated == 1) {
            return RestResponse.operationSuccess();
        } else {
            return RestResponse.operationFailed("No record is updated");
        }
    }
}
