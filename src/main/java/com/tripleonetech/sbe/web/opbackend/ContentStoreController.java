package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.cms.ContentStoreService;
import com.tripleonetech.sbe.cms.ContentStoreView;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/cms/content-store")
@Profile("main")
@Api(tags = { "CMS & Templates" })
public class ContentStoreController {
    @Autowired
    private ContentStoreService storeService;

    @GetMapping("/all")
    @ApiOperation("List all store entrys in key-value form")
    public RestResponse<Map<String, ContentStoreView>> getAll() {
        Map<String, ContentStoreView> stores = storeService.getAll();
        return RestResponse.ok(stores);
    }

    @GetMapping("/{key}")
    @ApiOperation("Get single store entry by key")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "key", value = "The key of the store entry")
    })
    public RestResponse<ContentStoreView> get(@PathVariable String key) throws RestResponseException {

        ContentStoreView storeEntry = storeService.get(key);
        if(storeEntry == null){
            throw new RestResponseException(RestResponse.entityNotFound(String.format("Cms key: [%s] not found", key)));
        }
        return RestResponse.ok(storeEntry);
    }

    @PostMapping("/{key}")
    @ApiOperation("Edit single store entry by key, available type is 0-html, 1-json, 2-text")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "key", value = "The key of the cms")
    })
    public RestResponse<RestResponseDataSuccess> edit(@PathVariable String key,
            @RequestBody ContentStoreView view) {

        int result = storeService.save(key, view);

        return result > 0 ? RestResponse.operationSuccess() : RestResponse.operationFailed("No data be saved");
    }

    @PostMapping("/delete/{key}")
    @ApiOperation("Delete single store entry by key")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "key", value = "The key of the cms")
    })
    public RestResponse<RestResponseDataSuccess> delete(
            @PathVariable String key) {

        int result = storeService.delete(key);

        return result > 0 ? RestResponse.operationSuccess() : RestResponse.operationFailed("No data be deleted");
    }

}
