package com.tripleonetech.sbe.web.opbackend;

import io.swagger.annotations.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RejectReasonForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.promo.bonus.*;

@RestController
@RequestMapping("/promo-bonus")
@Profile("main")
@Api(tags = {"Promotion Bonus"})
public class PromoBonusController extends BaseOpbackendController {
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private BonusService bonusService;

    @GetMapping("")
    @ApiOperation("Query bonuses")
    public RestResponse<PageInfo<BonusView>> list(@Valid PromoBonusQueryForm formObj) {
        return RestResponse.ok(new PageInfo<>(bonusService.query(formObj)));
    }

    @GetMapping("/campaigns")
    @ApiOperation("Query bonuses for campaigns")
    public RestResponse<PageInfo<BonusView>> listCampaigns(@Valid PromoCampaignBonusQueryForm formObj) {
        return RestResponse.ok(new PageInfo<>(bonusService.queryCampaign(formObj)));
    }

    @PostMapping("/{id}/approve")
    @ApiOperation("Approve this bonus and pay to player's wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "Bonus ID")
    })
    public RestResponse<RestResponseDataSuccess> approve(@PathVariable Integer id) {
        updateStatusById(id, BonusStatusEnum.APPROVED);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/approve-batch")
    @ApiOperation("Batch approve bonuses and update wallet balance according to the target status. Note that due to " +
            "the need to adjust each wallet balance in the process, this API could take long to complete.")
    public RestResponse<RestResponseDataSuccess> approveBatch(@Valid @RequestBody BonusBatchUpdateForm form) {
        form.getIds().forEach(id -> updateStatusById(id, BonusStatusEnum.APPROVED));
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/reject")
    @ApiOperation("Reject this bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "Bonus ID")
    })
    public RestResponse<RestResponseDataSuccess> reject(@PathVariable Integer id,
                                                        @Valid @RequestBody RejectReasonForm form) {
        updateStatusById(id, BonusStatusEnum.REJECTED, form.getReason());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/reject-batch")
    @ApiOperation("Batch reject bonuses")
    public RestResponse<RestResponseDataSuccess> rejectBatch(@Valid @RequestBody BonusBatchUpdateForm form) {
        form.getIds().forEach(id -> updateStatusById(id, BonusStatusEnum.REJECTED));
        return RestResponse.operationSuccess();
    }

    private void updateStatusById(Integer id, BonusStatusEnum status) {
        updateStatusById(id, status, null);
    }

    private void updateStatusById(Integer id, BonusStatusEnum status, String reason) {
        PromoBonus promoBonus = promoBonusMapper.queryById(id);

        if (BonusStatusEnum.APPROVED == status) {
            bonusService.approve(promoBonus, getLoginOperator().getUsername(), getLoginOperator().getId());
        } else if (BonusStatusEnum.REJECTED == status) {
            bonusService.reject(promoBonus, reason);
        }
    }

    private static class BonusBatchUpdateForm {
        @ApiModelProperty(required = true, value = "Multiple bonus IDs")
        private List<Integer> ids;
        @ApiModelProperty(value = "Reject reason")
        private String reason;

        public List<Integer> getIds() {
            return ids;
        }

        public void setIds(List<Integer> ids) {
            this.ids = ids;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
