package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.TimeRangeForm;
import com.tripleonetech.sbe.withdraw.report.WithdrawSummaryReportMapper;
import com.tripleonetech.sbe.withdraw.report.WithdrawSummaryReportView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reports")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportWithdrawSummaryController extends BaseOpbackendController {
    @Autowired
    private WithdrawSummaryReportMapper withdrawSummaryReportMapper;

    @GetMapping("/withdraw-summary")
    @ApiOperation("List summary of withdraw, allowed sort columns: Allowed sort columns: date (default), currency")
    public RestResponse<PageInfo<WithdrawSummaryReportView>> queryDepositSummaryReport(@Valid TimeRangeForm form) {
        List<WithdrawSummaryReportView> withdrawSummaryReportView = withdrawSummaryReportMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(withdrawSummaryReportView));
    }
}
