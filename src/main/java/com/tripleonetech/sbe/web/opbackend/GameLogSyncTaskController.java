package com.tripleonetech.sbe.web.opbackend;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.game.log.sync.*;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/game-log-sync-tasks")
@Profile("main")
@Api(tags = {"Game Log Sync"})
public class GameLogSyncTaskController extends BaseOpbackendController {
    
    @Autowired
    private GameLogSyncTaskMapper gameLogSyncTaskMapper;
    
    @GetMapping("")
    @ApiOperation("List game log sync tasks")
    public RestResponse<PageInfo<GameLogSyncTaskView>> list(@Valid GameLogSyncTaskQueryForm form) {
        List<GameLogSyncTaskView> gameLogSyncTasks = gameLogSyncTaskMapper.listGameLogSyncTasks(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(gameLogSyncTasks));
    }

    @PostMapping("")
    @ApiOperation("Create a new game logs sync task")
    public RestResponse<RestResponseIdSuccess> createSyncTask(@RequestBody GameLogSyncTaskNewForm form) {
        form.setSyncUsername(getLoginOperator().getUsername());
        GameLogSyncTask gameLogSyncTask = GameLogSyncTask.create(form);
        gameLogSyncTaskMapper.insert(gameLogSyncTask);
        return RestResponse.createSuccess(gameLogSyncTask.getId());
    }

}
