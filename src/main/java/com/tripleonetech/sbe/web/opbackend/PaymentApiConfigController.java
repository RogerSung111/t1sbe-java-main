package com.tripleonetech.sbe.web.opbackend;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.payment.PaymentApiFactory;
import com.tripleonetech.sbe.payment.PaymentMethod;
import com.tripleonetech.sbe.payment.PaymentMethodMapper;
import com.tripleonetech.sbe.payment.config.PaymentApiConfig;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;
import com.tripleonetech.sbe.payment.integration.PaymentApi;

import io.swagger.annotations.*;

@RestController
@RequestMapping("/system/payment-api")
@Profile("main")
@Api(tags = {"API Configuration - Payment"})
public class PaymentApiConfigController extends ApiConfigController {
    private final static Logger logger = LoggerFactory.getLogger(PaymentApiConfigController.class);

    @Autowired
    private PaymentApiConfigMapper paymentApiMapper;
    @Autowired
    private PaymentApiFactory paymentApiFactory;
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;

    @Override
    public ApiTypeEnum getApiType() {
        return ApiTypeEnum.PAYMENT;
    }

    @GetMapping("")
    @ApiOperation("List all payment API configs")
    public RestResponse<List<PaymentApiConfig>> list() {
        return RestResponse.ok(paymentApiMapper.list());
    }

    @GetMapping("/{id}")
    @ApiOperation("List single payment valid API configs ")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<PaymentApiConfig> listSingleApi(@PathVariable int id) {
        PaymentApiConfig apiConfig = paymentApiMapper.get(id);
        return RestResponse.ok(apiConfig);
    }

    @PostMapping("/create")
    @ApiOperation("Create a new payment API config")
    public RestResponse<RestResponseIdSuccess> createNewPaymentApiConfig(@RequestBody PaymentApiConfig paymentApiConfig)
        throws RestResponseException {
        logger.debug("Inserting new game API config [{}]", paymentApiConfig.getName());
        if (paymentApiMapper.insert(paymentApiConfig) > 0) {
            return RestResponse.createSuccess(paymentApiConfig.getId());
        } else {
            String errorMsg = String.format("Failed inserting a new PaymentApiConfig: [Code: %s, Name:%s, currency:%s]",
                    paymentApiConfig.getCode(), paymentApiConfig.getName(), paymentApiConfig.getCurrency());
            logger.warn(errorMsg);
            throw new RestResponseException(RestResponse.operationFailed(errorMsg));
        }
    }
    
    @Transactional
    @PostMapping("/{id}")
    @ApiOperation("Modify a payment API config")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API")
    })
    public RestResponse<List<PaymentMethodSimpleView>> editPaymentApiConfig(@PathVariable int id,
            @RequestBody PaymentApiConfig paymentApiConfig) {
        paymentApiConfig.setId(id);
        logger.debug("Updating game API config [{}] with id [{}]", paymentApiConfig.getName(), paymentApiConfig.getId());
        paymentApiMapper.update(paymentApiConfig);
        
        //Update payment_method.xxxx_trans_amount, when payment_api_config.xxx_trans_amount > payment_method.xxx_trans_amount
        List<PaymentMethod> paymentMethods = paymentMethodMapper.queryPaymentMethodByPaymentApiId(paymentApiConfig.getId());
        List<PaymentMethodSimpleView> responsePaymentMethods = new ArrayList<>();
        paymentMethods.forEach(paymentMethod -> {
            boolean isUpdated = false;
            if(paymentMethod.getDailyMaxDeposit().compareTo(paymentApiConfig.getDailyMaxDeposit()) < 0) {
                paymentMethod.setDailyMaxDeposit(paymentApiConfig.getDailyMaxDeposit());
                isUpdated = true;
            }
            if(paymentMethod.getMinDepositPerTrans().compareTo(paymentApiConfig.getMinDepositPerTrans()) < 0) {
                paymentMethod.setMinDepositPerTrans(paymentApiConfig.getMinDepositPerTrans());
                isUpdated = true;
            }
            if(paymentMethod.getMaxDepositPerTrans().compareTo(paymentApiConfig.getMaxDepositPerTrans()) < 0) {
                paymentMethod.setMaxDepositPerTrans(paymentApiConfig.getMaxDepositPerTrans());
                isUpdated = true;
            }
            if(isUpdated) {
                paymentMethodMapper.update(paymentMethod);
                responsePaymentMethods.add(PaymentMethodSimpleView.create(paymentMethod));
            }
        });
        return responsePaymentMethods.size() > 0 ? RestResponse.ok(responsePaymentMethods) : RestResponse.ok(null);
    }

    @PostMapping("/{id}/status")
    @ApiOperation("Update payment API config status. Available status: DISABLED(0), ENABLED(1), DELETED(20)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(@PathVariable int id,
            @RequestBody ApiConfigStatusForm form) {
        PaymentApiConfig paymentApiConfig = paymentApiMapper.get(id);
        
        ApiConfigStatusEnum newApiStatus = ApiConfigStatusEnum.valueOf(form.getStatus());
        if(newApiStatus == null) {
            return RestResponse.operationFailed("Wrong API status");
        }
        if(paymentApiConfig.getStatus().equals(ApiConfigStatusEnum.INACTIVE)
                || paymentApiConfig.getStatus().equals(ApiConfigStatusEnum.DELETED)) {
            
            return RestResponse.operationFailed(String.format("Cannot switch API status to [%s], because the status of [%s] is [%s]",
                    newApiStatus, paymentApiConfig.getName(), paymentApiConfig.getStatus()));
        }
        paymentApiConfig.setStatus(newApiStatus);
        paymentApiMapper.update(paymentApiConfig);

        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete a payment API config")
    @ApiImplicitParams({ @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config") })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable int id) {
        paymentApiMapper.delete(id);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/{id}/test")
    @ApiOperation("Test API Connectivity")
    @ApiImplicitParams({ @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config") })
    public RestResponse<RestResponseDataSuccess> testConnectivity(@PathVariable int id) {
        PaymentApi api = paymentApiFactory.getById(id);
        return RestResponse.operationResult(api.isOnline());
    }
    
    
    private static class PaymentMethodSimpleView {
        
        @ApiModelProperty(value = "Payment Method ID")
        private Integer id;
        @ApiModelProperty(value = "Payment Method Name")
        private String name;
        
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        protected static PaymentMethodSimpleView create(PaymentMethod paymentMethod) {
            PaymentMethodSimpleView response = new PaymentMethodSimpleView();
            response.setId(paymentMethod.getId());
            response.setName(paymentMethod.getName());
            return response;
        }
    }
}
