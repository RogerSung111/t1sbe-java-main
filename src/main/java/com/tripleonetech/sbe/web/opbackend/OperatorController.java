package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.AlphanumericGenUtil;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.operator.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Profile("main")
@Api(tags = {"Operator Management"})
public class OperatorController extends BaseOpbackendController {

    @Autowired
    private OperatorMapper operatorMapper;
    @Autowired
    private Constant constant;

    @GetMapping("/operators")
    @ApiOperation("List of operators filtered by query criteria. Allowed sort columns: username, role, status, createdAt, id (default)")
    public RestResponse<PageInfo<OperatorView>> list(@Valid OperatorQueryForm formObj) {
        List<Operator> operators = operatorMapper.list(formObj, formObj.getPage(), formObj.getLimit());
        List<OperatorView> views = operators.stream().map(OperatorView::from).collect(Collectors.toList());
        return RestResponse.ok(new PageInfo<OperatorView>(views));
    }

    @GetMapping("/operator")
    @ApiOperation("Get detail of currently login operator")
    public RestResponse<OperatorView> detail() {
        return RestResponse.ok(OperatorView.from(getLoginOperator()));
    }

    @GetMapping("/operators/{operatorId}")
    @ApiOperation("Get detail of the specific operator")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "operatorId", value = "ID of the specified operator")
    })
    public RestResponse<OperatorView> detail(@PathVariable Integer operatorId) throws RestResponseException {
        Operator operator = operatorMapper.get(operatorId);
        if (operator == null) {
            throw new RestResponseException(
                    RestResponse.operatorNotFound(String.format("Operator not found with id = [%s]", operatorId))
            );
        }
        return RestResponse.ok(OperatorView.from(operator));
    }

    @PreAuthorize(Permissions.Operator.PASSWORD_UPDATE)
    @PostMapping("/operators/{operatorId}/password")
    @ApiOperation("Resets operator's password")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "operatorId", value = "ID of the specified operator")
    })
    public RestResponse<RestResponseDataPasswordSuccess> resetPassword(@PathVariable Integer operatorId) {
        Operator operator = operatorMapper.get(operatorId);
        if (operator == null) {
            return RestResponse.ok(RestResponseDataPasswordSuccess.error(String.format("Operator not found with id = [%s]", operatorId)));
        }

        String newPassword = AlphanumericGenUtil.generateAlphanumericCode(constant.getSite().getGeneratedPasswordLength());
        operator.setPassword(newPassword);
        operatorMapper.updatePassword(operatorId, operator.getPasswordHash());
        return RestResponse.ok(RestResponseDataPasswordSuccess.success(operatorId, newPassword));
    }

    @PostMapping("/operator/change-password")
    @ApiOperation("Change current operator's password, with original password validation")
    public RestResponse<RestResponseDataSuccess> changePassword(@Valid @RequestBody ChangePasswordForm form) {
        Operator operator = operatorMapper.get(getLoginOperator().getId());
        if (!operator.validatePassword(form.getOriginalPassword())) {
            return RestResponse.invalidPassword("The original password entered was invalid");
        }

        operator.setPassword(form.getNewPassword());
        operatorMapper.updatePassword(operator.getId(), operator.getPasswordHash());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/operators/{operatorId}/status")
    @ApiOperation("Updates an operator's status")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "operatorId", value = "ID of the specified operator")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(
            @PathVariable int operatorId,
            @RequestBody StatusForm form) {
        if (operatorMapper.get(operatorId) == null) {
            return RestResponse.operatorNotFound(String.format("Operator not found with id = [%s]", operatorId));
        }
        operatorMapper.updateStatus(operatorId, form.getStatus());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/operators/{operatorId}/role")
    @ApiOperation("Updates an operator's role")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "operatorId", value = "ID of the specified operator")
    })
    public RestResponse<RestResponseDataSuccess> updateRole(
            @PathVariable int operatorId,
            @RequestBody RoleIdForm form) {
        operatorMapper.updateRole(operatorId, form.getRoleId());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/operators/create")
    @ApiOperation("Create new operator")
    public RestResponse<RestResponseIdSuccess> create(@Valid @RequestBody OperatorForm operatorForm)
            throws RestResponseException {
        if (StringUtils.isEmpty(operatorForm.getUsername()) ||
                StringUtils.isEmpty(operatorForm.getPassword())) {
            throw new RestResponseException(RestResponse.invalidParameter("username and password cannot be empty!"));
        }

        if (null != operatorMapper.getByUsername(operatorForm.getUsername())) {
            throw new RestResponseException(RestResponse.usernameAlreadyExist(String.format("Username [%s] already exists", operatorForm.getUsername())));
        }

        Operator operator = new Operator();
        operator.setUsername(operatorForm.getUsername());
        operator.setPassword(operatorForm.getPassword().trim());
        operator.setRoleId(operatorForm.getRoleId());
        operator.setStatusEnum(OperatorStatusEnum.ENABLED);
        operatorMapper.insert(operator);
        return RestResponse.createSuccess(operator.getId());
    }

    @GetMapping("/operators/username-available/{username}")
    @ApiOperation("Checks whether a username is available or has been taken")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "username", value = "The username to check for availability")
    })
    public RestResponse<RestResponseDataSuccess> isUsernameAvailable(@PathVariable String username) {
        if (BooleanUtils.isTrue(operatorMapper.userExists(username))) {
            return RestResponse.usernameAlreadyExist(String.format("Username [%s] already exists", username));
        }
        return RestResponse.operationSuccess();
    }

    private static class RoleIdForm {
        private Integer roleId;

        public Integer getRoleId() {
            return roleId;
        }

        public void setRoleId(Integer roleId) {
            this.roleId = roleId;
        }
    }

}
