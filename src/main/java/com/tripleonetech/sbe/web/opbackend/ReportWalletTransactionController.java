package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.report.wallettransaction.WalletTransactionRecord;
import com.tripleonetech.sbe.report.wallettransaction.WalletTransactionReportMapper;
import com.tripleonetech.sbe.report.wallettransaction.WalletTransactionReportQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reports/wallet-transaction")
@Profile("main")
@Api(value = "Wallet Transactions Report", tags = { "Reports" }, description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Persons.")
public class ReportWalletTransactionController {
    //private final static Logger logger = LoggerFactory.getLogger(ReportWalletTransactionController.class);
    @Autowired
    private WalletTransactionReportMapper reportMapper;

    @GetMapping("")
    @ApiOperation("Query wallet transactions. Result is only sorted by createdAt DESC, sort in query form is ignored.")
    public RestResponse<PageInfo<WalletTransactionRecord>> query(@Valid WalletTransactionReportQueryForm queryForm) {
        List<WalletTransactionRecord> result =  reportMapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
        return RestResponse.ok(new PageInfo<>(result));
    }
}
