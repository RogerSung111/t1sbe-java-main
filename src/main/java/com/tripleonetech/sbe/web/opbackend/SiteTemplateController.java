package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/site-templates")
@Profile("main")
@Api(tags = {"CMS & Templates"})
public class SiteTemplateController {
    private final static Logger logger = LoggerFactory.getLogger(SiteTemplateController.class);

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @PostMapping("/{templateName}/active")
    @ApiOperation("Use a specific template in frontend")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "templateName", value = "name of the specified template", required = true),
    })
    public RestResponse<RestResponseDataSuccess> setTemplateActive(@PathVariable String templateName)
            throws RestResponseException {
        List<SitePrivilege> templates = sitePrivilegeMapper.getSitePrivileges(SitePrivilegeTypeEnum.SITE_TEMPLATE);
        if (templates.stream().noneMatch(t -> t.getValue().equals(templateName))) {
            throw new RestResponseException(
                    RestResponse.templateNotFound(String.format("template not found with template name = [%s]", templateName))
            );
        }

        templates.forEach(t -> t.setActive(t.getValue().equals(templateName)));
        sitePrivilegeMapper.batchUpdateInsert(templates);

        return RestResponse.operationSuccess();
    }

}
