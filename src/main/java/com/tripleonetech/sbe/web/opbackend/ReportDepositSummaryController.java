package com.tripleonetech.sbe.web.opbackend;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.TimeRangeForm;
import com.tripleonetech.sbe.payment.report.DepositSummaryReportMapper;
import com.tripleonetech.sbe.payment.report.DepositSummaryReportView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/reports")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportDepositSummaryController extends BaseOpbackendController {
    @Autowired
    private DepositSummaryReportMapper depositSummaryReportMapper;
    
    @GetMapping("/deposit-summary")
    @ApiOperation("List summary of deposit. Allowed sort columns: date (default), currency")
    public RestResponse<PageInfo<DepositSummaryReportView>> queryDepositSummaryReport(@Valid TimeRangeForm form) {
        List<DepositSummaryReportView> depositSummaryReportView = depositSummaryReportMapper.query(form, form.getPage(),
                form.getLimit());
        return RestResponse.ok(new PageInfo<>(depositSummaryReportView));
    }
}
