package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.operator.OperatorRole;
import com.tripleonetech.sbe.operator.OperatorRoleForm;
import com.tripleonetech.sbe.operator.OperatorRoleMapper;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Profile("main")
@Api(tags = {"Operator Management"})
public class OperatorRoleController {
    @Autowired
    private OperatorRoleMapper operatorRoleMapper;

    @GetMapping("/operator-roles")
    @ApiOperation("List of operators roles, not filtered")
    public RestResponse<List<OperatorRole>> list() {
        return RestResponse.ok(operatorRoleMapper.list());
    }

    @PostMapping("/operator-roles/create")
    @ApiOperation("Create new operator role")
    public RestResponse<RestResponseDataSuccess> update(@RequestBody OperatorRoleForm form) {
        OperatorRole role = OperatorRole.from(form);
        operatorRoleMapper.insert(role);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/operator-roles/{roleId}")
    @ApiOperation("Updates the operator role")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "roleId", value = "ID of the specified operator role")
    })
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int roleId, @RequestBody OperatorRoleForm form) {
        OperatorRole role = OperatorRole.from(form);
        role.setId(roleId);
        operatorRoleMapper.update(role);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/operator-roles/{roleId}/delete")
    @ApiOperation("Deletes the operator role")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "roleId", value = "ID of the specified operator role")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable int roleId) {
        operatorRoleMapper.delete(roleId);
        return RestResponse.operationSuccess();
    }
}
