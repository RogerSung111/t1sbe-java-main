package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.GameNameMapper;
import com.tripleonetech.sbe.game.GameNameQueryForm;
import com.tripleonetech.sbe.game.GameNameView;
import com.tripleonetech.sbe.game.gamelist.GameListSyncRunner;
import com.tripleonetech.sbe.game.integration.GameListApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping("/games")
@Profile("main")
@Api(tags = {"Game Management"})
public class GameNameController {
    @Autowired
    private GameNameMapper gameNameMapper;
    @Autowired
    private GameListSyncRunner gameListSyncRunner;

    @GetMapping("")
    @ApiOperation("Query from all games, allowed sort columns: gameApiCode, gameTypeName, gameName, id (default)")
    public RestResponse<PageInfo<GameNameView>> list(@Valid GameNameQueryForm model) {
        List<GameNameView> gameNameViews = gameNameMapper.query(model, model.getPage(), model.getLimit());
        return RestResponse.ok(new PageInfo<>(gameNameViews));
    }

    @Transactional
    @PostMapping("/update-sort")
    @ApiOperation("Update the order of specific id games")
    public RestResponse<RestResponseDataSuccess> batchUpdateSort(@RequestBody List<@Valid GameNameSortForm> form) {
        List<Integer> ids = form.stream().map(GameNameSortForm::getGameNameId).collect(Collectors.toList());
        Map<Integer, Integer> sortMap = form.stream().collect(
                Collectors.toMap(GameNameSortForm::getGameNameId, GameNameSortForm::getSort));

        List<GameName> gameNames = gameNameMapper.listByIds(ids);
        gameNames.forEach(g -> g.setSort(sortMap.get(g.getId())));

        gameNameMapper.batchUpdateSort(gameNames);
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/enabled")
    @ApiOperation("enable/disable the selected games under the specific id")
    public RestResponse<RestResponseDataSuccess> updateEnabled(@Valid @RequestBody GameNameForm form) {
        gameNameMapper.updateEnable(form.getEnabled(), form.getGameNameIds());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/assign")
    @ApiOperation("Assign the selected games under the specific game type")
    public RestResponse<RestResponseDataSuccess> assignGameType(@Valid @RequestBody AssignGameTypeForm form) {
        gameNameMapper.updateGameTypeId(form.getGameTypeId(), form.getGameNameIds());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/sync-game-list")
    @ApiOperation("Sync game list from t1 gamegateway")
    public RestResponse<RestResponseDataSuccess> syncGameList() {
        GameListApiResult result = gameListSyncRunner.manualSyncGameList();
        if (!result.isSuccess()) {
            return RestResponse.apiFailed(String.format("ApiResponseCode: [%s], msg: [%s]", result.getResponseEnum(), result.getMessage()));
        }
        return RestResponse.operationSuccess();
    }

    public static class GameNameForm {
        @ApiModelProperty(required = true, name = "gameNameIds", value = "The IDs of selected games.")
        @NotNull
        private List<Integer> gameNameIds;
        @ApiModelProperty(required = true, name = "enabled", value = "true or false")
        @NotNull
        private Boolean enabled;

        public List<Integer> getGameNameIds() {
            return gameNameIds;
        }

        public void setGameNameId(List<Integer> gameNameIds) {
            this.gameNameIds = gameNameIds;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }
    }

    @Validated
    public static class GameNameSortForm {
        @ApiModelProperty(required = true, name = "gameNameId", value = "The ID of selected game.")
        @NotNull
        private Integer gameNameId;
        @ApiModelProperty(required = true, name = "sort", value = "sort number of the game, the higher number, the more front")
        @NotNull
        private Integer sort;

        public Integer getGameNameId() {
            return gameNameId;
        }

        public void setGameNameId(Integer gameNameId) {
            this.gameNameId = gameNameId;
        }

        public Integer getSort() {
            return sort;
        }

        public void setSort(Integer sort) {
            this.sort = sort;
        }
    }

    public static class AssignGameTypeForm {
        @ApiModelProperty(required = true, name = "gameNameIds", value = "The IDs of selected games.")
        @NotNull
        private List<Integer> gameNameIds;
        @ApiModelProperty(required = true, name = "gameTypeId", value = "The ID of the game type to assign.")
        @NotNull
        private Integer gameTypeId;

        public List<Integer> getGameNameIds() {
            return gameNameIds;
        }

        public void setGameNameId(List<Integer> gameNameIds) {
            this.gameNameIds = gameNameIds;
        }

        public Integer getGameTypeId() {
            return gameTypeId;
        }

        public void setGameTypeId(Integer gameTypeId) {
            this.gameTypeId = gameTypeId;
        }
    }
}
