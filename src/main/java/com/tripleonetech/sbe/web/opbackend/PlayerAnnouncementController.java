package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.announcement.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/player/announcement")
@Profile("main")
@Api(tags = {"Message & Announcement"})
public class PlayerAnnouncementController {
    private final static Logger logger = LoggerFactory.getLogger(PlayerMessageController.class);

    @Autowired
    private PlayerAnnouncementService playerAnnouncementService;
    @Autowired
    private PlayerAnnouncementMapper mapper;

    @GetMapping("")
    @ApiOperation("Query announcements")
    public RestResponse<PageInfo<PlayerAnnouncementView>> queryMessage(@Valid PlayerAnnouncementQueryForm queryForm) {
        PageInfo<PlayerAnnouncementView> list = new PageInfo<>(mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit()));
        return RestResponse.ok(list);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get specific announcement")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "ID of the specified announcement", required = true),
    })
    public RestResponse<PlayerAnnouncementDetailView> get(@PathVariable Integer id) throws RestResponseException {
        PlayerAnnouncementDetailView announcement = mapper.getDetail(id);
        if (announcement == null) {
            throw new RestResponseException(RestResponse.playerAnnouncementNotFound(
                    String.format("Announcement [%d] not found", id)
            ));
        }
        return RestResponse.ok(announcement);
    }

    @PostMapping("/create")
    @ApiOperation("Create new announcement")
    public RestResponse<RestResponseIdSuccess> create(@RequestBody PlayerAnnouncementForm playerAnnouncement) throws RestResponseException {
        int id = playerAnnouncementService.create(playerAnnouncement);
        return RestResponse.createSuccess(id);
    }

    @PostMapping("/{id}")
    @ApiOperation("Update announcement by id")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "ID of the specified announcement", required = true),
    })
    public RestResponse<RestResponseDataSuccess> update(@PathVariable Integer id,
            @Valid @RequestBody PlayerAnnouncementForm playerAnnouncement) {
        playerAnnouncement.setId(id);
        int updatedRows = playerAnnouncementService.update(playerAnnouncement);
        if (updatedRows < 1) {
            String message = String.format("Failed updating player announcement [%d]", id);
            logger.error(message);
            return RestResponse.operationFailed(message);
        }
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete announcement by id")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "ID of the specified announcement", required = true),
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable Integer id) {
        playerAnnouncementService.delete(id);
        return RestResponse.operationSuccess();
    }

}
