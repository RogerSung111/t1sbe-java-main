package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.email.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reports")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportEmailController extends BaseOpbackendController {
    @Autowired
    private EmailVerificationHistoryMapper emailVerificationHistoryMapper;
    @Autowired
    private EmailHistoryMapper emailHistoryMapper;

    @GetMapping("/email-otp")
    @ApiOperation("List email verification history")
    public RestResponse<PageInfo<EmailVerificationHistoryView>> listEmailVerificationHistory(@Valid EmailVerificationHistoryQueryForm form){
        List<EmailVerificationHistoryView> view = emailVerificationHistoryMapper.listEmailHistory(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(view));
    }

    @GetMapping("/email-history")
    @ApiOperation("List history of emails sent by system")
    public RestResponse<PageInfo<EmailHistory>> listEmailHistory(@Valid EmailHistoryQueryForm form){
        List<EmailHistory> view = emailHistoryMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(view));
    }
}
