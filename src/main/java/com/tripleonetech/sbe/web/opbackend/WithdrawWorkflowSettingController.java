package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSetting;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingForm;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingMapper;
import com.tripleonetech.sbe.withdraw.workflow.WithdrawWorkflowSettingView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/withdraw-workflow")
@Profile("main")
@Api(tags = {"Deposit & Withdraw Setting"})
public class WithdrawWorkflowSettingController {
    @Autowired
    private WithdrawWorkflowSettingMapper workflowSettingMapper;

    @GetMapping("")
    @ApiOperation("List withdraw workflow settings")
    public RestResponse<List<WithdrawWorkflowSettingView>> list() {
        List<WithdrawWorkflowSettingView> view = new ArrayList<>();
        List<WithdrawWorkflowSetting> workflowSettings = workflowSettingMapper.list();

        workflowSettings.forEach(workflowSetting -> {
            view.add(WithdrawWorkflowSettingView.from(workflowSetting));
        });

        return RestResponse.ok(view);
    }

    @PostMapping("")
    @ApiOperation("Update withdraw workflow settings")
    public RestResponse<RestResponseDataSuccess> updateWorkflowSettings(@RequestBody List<WithdrawWorkflowSettingForm> form) {
        form.forEach(workflowItem -> {
            WithdrawWorkflowSetting withdrawWorkflowSetting = new WithdrawWorkflowSetting();
            withdrawWorkflowSetting.setId(workflowItem.getId().getCode());
            withdrawWorkflowSetting.setName(workflowItem.getName());
            withdrawWorkflowSetting.setEnabled(workflowItem.getEnabled());
            workflowSettingMapper.updateWorkflowSetting(withdrawWorkflowSetting);
        });
        return RestResponse.operationSuccess();
    }
}
