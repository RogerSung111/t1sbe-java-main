package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.payment.PaymentMethodDetailView;
import com.tripleonetech.sbe.payment.PaymentMethodForm;
import com.tripleonetech.sbe.payment.PaymentMethodQueryForm;
import com.tripleonetech.sbe.payment.service.PaymentMethodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/payment-methods")
@Profile("main")
@Api(tags = {"Deposit & Withdraw Setting"})
public class PaymentMethodController {
    //private final static Logger logger = LoggerFactory.getLogger(PaymentMethodController.class);

    @Autowired
    private PaymentMethodService paymentMethodService;

    @GetMapping("")
    @ApiOperation("List all payment methods. Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WECHAT(3), ALIPAY(4), OTHERS(0); Tag Operator: \"any\", \"all\"")
    public RestResponse<List<PaymentMethodDetailView>> list(PaymentMethodQueryForm queryForm) {
        return RestResponse.ok(paymentMethodService.query(queryForm));
    }

    @GetMapping("/{id}")
    @ApiOperation("Get detail of payment method. Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WECHAT(3), ALIPAY(4), OTHERS(0); Tag Operator: \"any\", \"all\"")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the specified payment method")
    public RestResponse<PaymentMethodDetailView> detail(@PathVariable int id)
        throws RestResponseException {
        PaymentMethodDetailView paymentMethodDetailView = paymentMethodService.getDetail(id);
        if(paymentMethodDetailView == null){
            throw new RestResponseException(
                    RestResponse.paymentMethodNotFound(String.format("Payment method [%s] not found", id)));
        }
        return RestResponse.ok(paymentMethodDetailView);
    }

    @PostMapping("/create")
    @ApiOperation("Create new payment method. Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WEXIN(3), ALIPAY(4), OTHERS(0). Note that 'chargeRatio' takes value of 0-100.")
    public RestResponse<RestResponseIdSuccess> createPaymentMethod(@Valid @RequestBody PaymentMethodForm form) {
        Integer id = paymentMethodService.create(form);
        return RestResponse.createSuccess(id);
    }

    @PostMapping(value = "/{id}/enabled")
    @ApiOperation("Enable/disable a payment method")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the payment method to be updated")
    public RestResponse<RestResponseDataSuccess> updateEnabled(@PathVariable int id, @Valid @RequestBody BooleanForm form) {
        paymentMethodService.updateEnabled(id, form.isEnabled());
        return RestResponse.operationResult(true);
    }

    @PostMapping("/{id}")
    @ApiOperation("Update payment method. Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WEXIN(3), ALIPAY(4), OTHERS(0). Note that 'chargeRatio' takes value of 0-100.")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the payment method to be updated")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id, @Valid @RequestBody PaymentMethodForm form) {
        paymentMethodService.update(id, form);
        return RestResponse.operationResult(true);
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete payment method")
    @ApiImplicitParam(paramType = "query", name = "id", value = "ID of the payment method to be deleted", required = true)
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable Integer id) throws RestResponseException {
        this.paymentMethodService.deleteById(id);
        return RestResponse.operationSuccess();
    }
}
