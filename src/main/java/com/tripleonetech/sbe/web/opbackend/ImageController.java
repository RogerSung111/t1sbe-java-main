package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.storage.ResourceUpload;
import com.tripleonetech.sbe.common.storage.ResourceUploadMapper;
import com.tripleonetech.sbe.common.storage.ResourceUploadQueryForm;
import com.tripleonetech.sbe.common.storage.StorageService;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/images")
@Profile("main")
@Api(tags = { "Resources" })
public class ImageController {
    private final String[] availableExtension = { "jpe", "jpg", "jpeg", "gif", "png" };

    @Autowired
    private StorageService storageService;
    @Autowired
    private ResourceUploadMapper resourceUploadMapper;

    @GetMapping("")
    @ApiOperation("List all uploaded images, allowed sort columns: createdAt (default), name, size, width, height")
    public RestResponse<PageInfo<ResourceUpload>> list(ResourceUploadQueryForm form) {
        return RestResponse.ok(new PageInfo<>(resourceUploadMapper.list(form, form.getPage(), form.getLimit())));
    }

    @PostMapping("/create")
    @ApiOperation("Upload image. Allowed file extensions: [\"jpe\", \"jpg\", \"jpeg\", \"gif\", \"png\"]")
    public RestResponse<RestResponseIdSuccess> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam(required = false) String name)
            throws IOException, RestResponseException {
        String extension = storageService.getFilenameExtension(file.getOriginalFilename());
        if (!storageService.isAvailableFileType(extension, availableExtension)) {
            throw new RestResponseException(
                    RestResponse.invalidParameter("Failed to store this file type ".concat(extension)));
        }

        String filePath = storageService.store(file);

        Resource resource = storageService.loadAsResource(filePath);
        ResourceUpload resourceUpload = new ResourceUpload();
        resourceUpload.setFilePath(filePath);
        resourceUpload.setFileName(resource.getFilename());
        resourceUpload.setSize(resource.contentLength());
        if (StringUtils.isNoneEmpty(name)) {
            resourceUpload.setName(name);
        } else {
            resourceUpload.setName(file.getOriginalFilename());
        }
        try (InputStream inputStream = resource.getInputStream()) {
            BufferedImage image = ImageIO.read(inputStream);
            resourceUpload.setWidth(image.getWidth());
            resourceUpload.setHeight(image.getHeight());
        }
        resourceUploadMapper.insert(resourceUpload);
        return RestResponse.ok(RestResponseImageSuccess.success(resourceUpload.getId(), resourceUpload.getFileUrlPath()));
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete uploaded image")
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable Integer id) {
        ResourceUpload resourceUpload = resourceUploadMapper.get(id);
        storageService.delete(resourceUpload.getFilePath());
        resourceUploadMapper.delete(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/edit")
    @ApiOperation("Edit recognized name of specified resource")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable Integer id, @RequestBody ImageUpdateForm form) {
        ResourceUpload resourceUpload = resourceUploadMapper.get(id);
        resourceUpload.setName(form.getName());
        resourceUploadMapper.update(resourceUpload);
        return RestResponse.operationSuccess();
    }

    public static class RestResponseImageSuccess extends RestResponseIdSuccess {
        private String filePath;

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public static RestResponseImageSuccess success(Integer id, String filePath) {
            RestResponseImageSuccess response = new RestResponseImageSuccess();
            response.setId(id);
            response.setFilePath(filePath);
            response.setSuccess(true);
            return response;
        }
    }

    public static class ImageUpdateForm {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
