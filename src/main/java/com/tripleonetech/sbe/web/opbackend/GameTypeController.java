package com.tripleonetech.sbe.web.opbackend;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.game.*;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.log.GameApiIdNameView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/game-types")
@Profile("main")
@Api(tags = {"Game Management"})
public class GameTypeController {

    @Autowired
    private GameTypeMapper gameTypeMapper;
    @Autowired
    private GameTypeI18nMapper gameTypeI18nMapper;
    @Autowired
    private GameNameMapper gameNameMapper;
    @Autowired
    private Constant constant;
    
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    @GetMapping("")
    @ApiOperation("List game types")
    public RestResponse<List<GameTypeView>> list() {
        List<GameTypeView> gameTypes = gameTypeMapper.listWithGameCount(
                LocaleContextHolder.getLocale().toLanguageTag(),
                constant.getFrontend().getDefaultLocale());
        for(GameTypeView gameType : gameTypes) {
            gameType.setGameApis(gameApiConfigMapper.listGameApiByGameTypeId(gameType.getId())
                    .stream().map(GameApiIdNameView::create).collect(Collectors.toList()));
        }

        return RestResponse.ok(gameTypes);
    }

    @Transactional
    @PostMapping("")
    @ApiOperation("Add / edit game type")
    public RestResponse<RestResponseIdSuccess> upsert(@RequestBody GameTypeForm model) {
        if(model.getId() == null || model.getId() == 0) {
            GameType gameType = new GameType();
            gameType.setEnabled(Boolean.TRUE);
            gameTypeMapper.insertGameType(gameType);
            gameTypeI18nMapper.upsert(gameType.getId(), model.getI18nNames());
            return RestResponse.createSuccess(gameType.getId());
        } else {
            gameTypeI18nMapper.upsert(model.getId(), model.getI18nNames());
            return RestResponse.createSuccess(model.getId());
        }

    }

    @GetMapping("/{id}")
    @ApiOperation("Get detail of the specific game type")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the specified game type")
    })
    public RestResponse<GameTypeDetailView> detail(@PathVariable Integer id) {
        GameType gameType = gameTypeMapper.getGameTypeById(id,
                LocaleContextHolder.getLocale().toLanguageTag(),
                constant.getFrontend().getDefaultLocale());
        GameTypeDetailView result = GameTypeDetailView.from(gameType);

        List<GameTypeI18n> gameTypeI18nList = gameTypeI18nMapper.listByGameType(id);
        result.setI18nNames(gameTypeI18nList);
        result.setId(id);
        return RestResponse.ok(result);
    }

    @Transactional
    @PostMapping(value = "/{id}/delete")
    @ApiOperation("Delete the specific game type. Note that game count has to be 0 for successful delete.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the specified game type")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable Integer id) {
        // If there are any games under the game type, can not disable.
        if (gameNameMapper.hasGame(id)) {
            return RestResponse.operationResult(false);
        }
        gameTypeMapper.delete(id);

        return RestResponse.operationSuccess();
    }
}
