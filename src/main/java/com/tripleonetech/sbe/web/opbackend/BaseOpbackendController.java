package com.tripleonetech.sbe.web.opbackend;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.operator.OperatorDetails;

public abstract class BaseOpbackendController {
    /**
     * Returns the instance of currently logged-in operator. Thread-safe.
     *
     * @return Currently logged in Operator instance.
     */
    protected Operator getLoginOperator() {
        Authentication oauth2Authentication = SecurityContextHolder.getContext().getAuthentication();
        if(oauth2Authentication == null) { // happens in Mockito tests
            return null;
        }
        if (oauth2Authentication.getPrincipal() instanceof OperatorDetails) {
            OperatorDetails operatorDetail = (OperatorDetails) oauth2Authentication.getPrincipal();
            return operatorDetail.getOperator();
        } else {
            return null;
        }
    }
}
