package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.Utils;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.game.GameTypeMapper;
import com.tripleonetech.sbe.game.GameTypeWithGameApi;
import com.tripleonetech.sbe.promo.cashback.*;
import com.tripleonetech.sbe.promo.cashback.CashbackForm.CashbackBatchUpdateForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping("/promotions/cashback")
@Profile("main")
@Api(tags = {"Promotion - Cashback"})
public class PromoCashbackController {
    @Autowired
    private PromoCashbackMapper promoCashbackMapper;

    @Autowired
    private GameTypeMapper gameTypeMapper;

    @GetMapping("")
    @ApiOperation("List ALL cashback rules")
    public RestResponse<List<CashbackView>> list() {
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        List<CashbackView> viewList = promoCashbackMapper.listView(locale, defaultLocale);
        return RestResponse.ok(viewList);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get details of the specific cashback rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of cashback rule")
    })
    public RestResponse<CashbackView> get(@PathVariable int id) {
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();
        CashbackView cashbackView = promoCashbackMapper.getView(id, locale, defaultLocale);
        return RestResponse.ok(cashbackView);
    }

    @PostMapping("/create")
    @Transactional
    @ApiOperation("Create a new cashback rule")
    public RestResponse<RestResponseIdSuccess> create(@Valid @RequestBody CashbackForm form) {
        CashbackView cashback = Utils.copyProperties(form, CashbackView.class);
        promoCashbackMapper.insert(cashback);
        insertSettings(Collections.singletonList(cashback));
        return RestResponse.createSuccess(cashback.getId());
    }

    @PostMapping("/{id}")
    @Transactional
    @ApiOperation("Edit a cashback rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the cashback rule to be updated")
    })
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id, @Valid @RequestBody CashbackForm form) {
        CashbackView cashback = Utils.copyProperties(form, CashbackView.class);
        cashback.setId(id);
        promoCashbackMapper.update(cashback);
        // an update results in deleting and re-inserting all related game types and bet ranges
        promoCashbackMapper.deleteGameTypesByRuleId(id);
        promoCashbackMapper.deleteSettingByRuleId(id);
        insertSettings(Collections.singletonList(cashback));
        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/{id}/enabled")
    @Transactional
    @ApiOperation("Enable / disable a cashback rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the cashback rule to be updated")
    })
    public RestResponse<RestResponseDataSuccess> updateEnabled(@PathVariable int id, @Valid @RequestBody BooleanForm form)
            throws RestResponseException {
        Cashback cashback = promoCashbackMapper.get(id);
        if (cashback == null) {
            throw new RestResponseException(
                    RestResponse.cashbackNotFound(String.format("cashback rule not found with id = [%s]", id))
            );
        }
        cashback.setEnabled(form.isEnabled());
        promoCashbackMapper.update(cashback);
        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/{id}/delete")
    @ApiOperation("Delete this cashback rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the cashback rule to be deleted")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable int id) throws RestResponseException {
        Cashback cashback = promoCashbackMapper.get(id);
        if (cashback == null) {
            throw new RestResponseException(
                    RestResponse.cashbackNotFound(String.format("cashback rule not found with id = [%s]", id))
            );
        }
        promoCashbackMapper.delete(id);
        return RestResponse.operationSuccess();
    }

    private List<PromoCashbackBetRange> getBetRangeSettings(CashbackView cashback, Integer cashbackId) {
        if (!cashback.hasSettings()) {
            return null;
        }

        List<PromoCashbackBetRange> betRanges = cashback.getSettings().stream()
                .map(b -> Utils.copyProperties(b, PromoCashbackBetRange.class))
                .collect(Collectors.toList());
        betRanges.forEach(b -> b.setRuleId(cashbackId));

        return betRanges;
    }

    private List<CashbackGameType> getGameTypeSettings(CashbackView cashback, Integer cashbackId) {
        if (!cashback.hasGameTypes()) {
            return null;
        }

        List<CashbackGameType> gameTypes = cashback.getGameTypes().stream()
                .map(g -> Utils.copyProperties(g, CashbackGameType.class))
                .collect(Collectors.toList());
        gameTypes.forEach(g -> g.setRuleId(cashbackId));

        return gameTypes;
    }

    @GetMapping(value = "/group/{groupId}")
    @ApiOperation("List cashback rule for the specific player group. Use groupId = 0 to refer to the default set of cashback rule.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "groupId", value = "ID of the specified player group.")
    })
    public RestResponse<List<CashbackView>> listByGroupId(@PathVariable Integer groupId) {
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLocale = Constant.getInstance().getBackend().getDefaultLocale();
        List<CashbackView> viewList = promoCashbackMapper.listViewByGroup(groupId, locale, defaultLocale);
        return RestResponse.ok(viewList);
    }

    @PostMapping("/group/{groupId}")
    @Transactional
    @ApiOperation("Save cashback rules for the specific player group")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "groupId", value = "ID of the specified player group.")
    })
    public RestResponse<RestResponseDataSuccess> upsertBatch(@PathVariable int groupId,
                                                             @Valid @RequestBody List<@Valid CashbackBatchUpdateForm> batchUpdateForm) {
        batchUpdateForm.forEach(f -> f.setGroupId(groupId));

        // batch insert
        List<CashbackView> cashbackList = batchUpdateForm.stream()
                .filter(f -> f.getId() == null)
                .map(form -> Utils.copyProperties(form, CashbackView.class))
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(cashbackList)) {
            promoCashbackMapper.insertBatch(cashbackList);
        }

        // batch update
        List<CashbackView> updatedList = batchUpdateForm.stream()
                .filter(f -> f.getId() != null)
                .map(f -> Utils.copyProperties(f, CashbackView.class))
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(updatedList)) {
            List<Integer> ruleIds = updatedList.stream().map(CashbackView::getId).collect(Collectors.toList());
            promoCashbackMapper.deleteGameTypesByRuleIds(ruleIds);
            promoCashbackMapper.deleteSettingByRuleIds(ruleIds);
            promoCashbackMapper.updateBatch(updatedList);
            cashbackList.addAll(updatedList);
        }

        insertSettings(cashbackList);

        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/group/{groupId}/delete")
    @ApiOperation("Delete this cashback rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "groupId", value = "ID of the specified player group.")
    })
    public RestResponse<RestResponseDataSuccess> groupDelete(@PathVariable int groupId) {
        promoCashbackMapper.groupDelete(groupId);
        return RestResponse.operationSuccess();
    }

    private void insertSettings(List<CashbackView> cashbackList) {
        List<PromoCashbackBetRange> allCashbackBetRange = new ArrayList<>();
        List<CashbackGameType> allCashbackGameType = new ArrayList<>();
        for (CashbackView cashback : cashbackList) {
            List<PromoCashbackBetRange> betRangeSettings = getBetRangeSettings(cashback, cashback.getId());
            if (betRangeSettings != null) {
                allCashbackBetRange.addAll(betRangeSettings);
            }
            List<CashbackGameType> gameTypeSettings = getGameTypeSettings(cashback, cashback.getId());
            if (gameTypeSettings != null) {
                allCashbackGameType.addAll(gameTypeSettings);
            }
        }
        if (allCashbackBetRange.size() > 0) {
            promoCashbackMapper.insertBatchSetting(allCashbackBetRange);
        }
        if (allCashbackGameType.size() > 0) {
            promoCashbackMapper.insertBatchGameType(allCashbackGameType);
        }
    }

    @GetMapping(value = "/group/{groupId}/usable-game-types")
    @ApiOperation("List usable game types for the specific cashback rules set. Use groupId = 0 to refer to the default set of cashback rule.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "groupId", value = "ID of the specified player group."),
            @ApiImplicitParam(paramType = "query", name = "currency", value = "Currency"),
    })
    public RestResponse<List<CashbackGameType>> getUsableGameTypes(@PathVariable Integer groupId,
                                                                   @RequestParam(required = false) SiteCurrencyEnum currency) {
        List<CashbackGameType> result = listUsableGameTypes(groupId, currency);
        return RestResponse.ok(result);
    }

    private List<CashbackGameType> listUsableGameTypes(Integer groupId, SiteCurrencyEnum currency) {
        LocaleAwareQuery localeQuery = new LocaleAwareQuery() {};
        List<GameTypeWithGameApi> gameTypeList = gameTypeMapper.listUsableGameTypesForCashback(groupId,
                localeQuery.getLocale(), localeQuery.getDefaultLocale());
        List<CashbackGameType> result = gameTypeList.stream()
                .filter(cgt -> cgt.getCurrency().equals(currency) || currency == null)
                .map(g -> {
                    CashbackGameType gameType = new CashbackGameType();
                    gameType.setGameApiId(g.getGameApiId());
                    gameType.setGameTypeId(g.getGameTypeId());
                    gameType.setGameApiName(g.getGameApiName());
                    gameType.setGameTypeName(g.getGameTypeName());
                    gameType.setCurrency(g.getCurrency());
                    return gameType;
                }).collect(Collectors.toList());

        return result;
    }

    // Preview is not useful now as it does not include Group. Will update its logic when front-end needs it.
//    @GetMapping("/preview")
//    public RestResponse<CashbackSettingPreview> preview() {
//        CashbackSettingPreview preview = new CashbackSettingPreview();
//        preview.setAllLimits(promoCashbackMapper.listLimits());
//
//        Set<String> existingGameApiAndTypes = new HashSet<>();
//        Map<String, BigDecimal> percentageMap = new HashMap<>();
//        List<CashbackGameTypeBetRange> percentages = promoCashbackMapper.listGameTypeBetRange();
//        for (CashbackGameTypeBetRange percentage : percentages) {
//            // list game types
//            String gameApiAndTypeId = GameApiAndType.concatAsId(percentage.getGameApiId(), percentage.getGameTypeId());
//            existingGameApiAndTypes.add(gameApiAndTypeId);
//
//            // list percentages
//            String mapKey = StringUtils.join(gameApiAndTypeId, "-", percentage.getLimit());
//            BigDecimal mapValue = percentage.getCashbackPercentage();
//            percentageMap.put(mapKey, mapValue);
//        }
//        preview.setExistingGameTypes(existingGameApiAndTypes);
//        preview.setPercentages(percentageMap);
//
//        return RestResponse.ok(preview);
//    }

//    public static class CashbackSettingPreview {
//        private List<Integer> allLimits;
//        private Set<String> existingGameTypes;
//        private Map<String, BigDecimal> percentages;
//
//        public List<Integer> getAllLimits() {
//            return allLimits;
//        }
//
//        public void setAllLimits(List<Integer> allLimits) {
//            this.allLimits = allLimits;
//        }
//
//        public Set<String> getExistingGameTypes() {
//            return existingGameTypes;
//        }
//
//        public void setExistingGameTypes(Set<String> existingGameTypes) {
//            this.existingGameTypes = existingGameTypes;
//        }
//
//        public Map<String, BigDecimal> getPercentages() {
//            return percentages;
//        }
//
//        public void setPercentages(Map<String, BigDecimal> percentages) {
//            this.percentages = percentages;
//        }
//    }
}
