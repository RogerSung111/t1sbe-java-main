package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayerTagService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/groups")
@Profile("main")
@Api(tags = {"Player Group & Tags"})
public class PlayerGroupController {

    @Autowired
    private PlayerTagService playerTagService;
    @Autowired
    private PlayerTagMapper playerTagMapper;

    @GetMapping("")
    @ApiOperation("List of all groups")
    public RestResponse<List<PlayerTag>> listAllTags() {
        return RestResponse.ok(playerTagService.listAllGroups());
    }

    @GetMapping("/{id}")
    @ApiOperation("Gets the detail of a group")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected group"),
    })
    public RestResponse<PlayerTag> getGroup(@PathVariable("id") int id) throws RestResponseException {
        PlayerTag group = playerTagService.getGroup(id);
        if (group == null) {
            throw new RestResponseException(
                    RestResponse.playerGroupNotFound("No group found with id " + id)
            );
        }
        return RestResponse.ok(group);
    }

    @PostMapping("/{id}")
    @ApiOperation("Update the group")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected group")
    })
    public RestResponse<RestResponseDataSuccess> saveGroup(
            @PathVariable("id") int id,
            @Valid @RequestBody PlayerGroupForm form) {
        PlayerTag group = playerTagService.getGroup(id);
        if (group == null) {
            return RestResponse.playerGroupNotFound("No group found with id " + id);
        }

        PlayerTag playerGroup = form.toPlayerGroup();
        playerGroup.setId(id);
        playerTagService.updatePlayerTag(playerGroup);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/create")
    @ApiOperation("Create new group")
    public RestResponse<RestResponseIdSuccess> create(
            @Valid @RequestBody PlayerGroupForm form) {
        PlayerTag group = form.toPlayerGroup();
        Integer id = playerTagService.createGroup(group);
        return RestResponse.createSuccess(id);
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete the group")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected group")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable("id") int id) {
        PlayerTag group = playerTagService.getGroup(id);
        if (group == null) {
            return RestResponse.playerGroupNotFound("No group found with id " + id);
        } else if (group.getId().equals(playerTagMapper.getDefaultGroupId())) {
            return RestResponse.operationFailed("Cannot delete system Default group tag: " + id
                    + " (before you can delete this group, you will need set system default group to another group)");
        }
        playerTagService.deletePlayerTag(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/set-default")
    @ApiOperation("Update system default group tag")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected group that you want to set as the system default group")
    })
    public RestResponse<RestResponseDataSuccess> setDefaultGroup(@PathVariable("id") int id) {
        PlayerTag group = playerTagMapper.get(id, true);
        if (group == null) {
            return RestResponse.playerGroupNotFound("No group found with id " + id);
        }
        Boolean result = playerTagService.setDefaultTag(group.getId());
        return RestResponse.operationResult(result);
    }

    public static class PlayerGroupForm {
        @ApiModelProperty(required = true, name = "name", value = "Group name")
        @NotNull
        private String name;
        @ApiModelProperty(required = true, name = "Automation Job ID", value = "Automation Job ID")
        private Integer automationJobId;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAutomationJobId() {
            return automationJobId;
        }

        public void setAutomationJobId(Integer automationJobId) {
            this.automationJobId = automationJobId;
        }

        public PlayerTag toPlayerGroup() {
            PlayerTag tag = new PlayerTag();
            tag.setPlayerGroup(true);
            tag.setName(this.getName());
            tag.setAutomationJobId(this.getAutomationJobId());
            return tag;
        }
    }
}
