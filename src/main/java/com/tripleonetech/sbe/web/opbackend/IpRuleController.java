package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.iprule.*;
import com.tripleonetech.sbe.iprule.country.IpRuleCountryMapper;
import com.tripleonetech.sbe.iprule.country.IpRuleCountryView;
import io.swagger.annotations.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/firewall-rules")
@Profile("main")
@Api(tags = { "System Settings" })
public class IpRuleController {
    @Autowired
    private IpRuleMapper ipRuleMapper;
    @Autowired
    private IpRuleCountryMapper ipRuleCountryMapper;
    @Autowired
    private Constant constants;

    @GetMapping("/countries")
    @ApiOperation("List all api profile country blacklist/whitelist")
    public RestResponse<PageInfo<IpRuleCountryView>> listIpRuleCountries() {
        String languageTag = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLanguageTag = constants.getBackend().getDefaultLocale();
        List<IpRuleCountryView> list = ipRuleCountryMapper.list(false, languageTag, defaultLanguageTag);
        return RestResponse.ok(new PageInfo<>(list));
    }

    @GetMapping("/countries/enabled-only")
    @ApiOperation("List all api profile country blacklist/whitelist")
    public RestResponse<PageInfo<IpRuleCountryView>> listIpRuleCountriesEnabled() {
        String languageTag = LocaleContextHolder.getLocale().toLanguageTag();
        String defaultLanguageTag = constants.getBackend().getDefaultLocale();
        List<IpRuleCountryView> list = ipRuleCountryMapper.list(true, languageTag, defaultLanguageTag);
        return RestResponse.ok(new PageInfo<>(list));
    }

    @PostMapping("/countries")
    @ApiOperation("Update front office country list, set which countries are allowed access")
    public RestResponse<RestResponseDataSuccess> updateCountryStatus(
            @Valid @RequestBody CountryListForm form) {
        ipRuleCountryMapper.clearEnabled();
        if(form.getCountryCodes().size() == 0) {
            return RestResponse.ok(RestResponseDataSuccess.success("Warning: No country is allowed access now."));
        }
        ipRuleCountryMapper.setEnabled(form.getCountryCodes());
        return RestResponse.operationSuccess();
    }

    @GetMapping("/{site}")
    @ApiOperation("List back office IP (whitelist) / front office IP (blacklist) rules, allowed sort columns: ip, createdAt, updatedAt")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "site", value = "0: BACK OFFICE, 1: FRONT OFFICE", allowableValues = "0,1")
    })
    public RestResponse<PageInfo<IpRule>> listMainIpRule(@PathVariable Integer site,
                                                         IpRuleQueryForm queryForm) {
        List<IpRule> ipRules = ipRuleMapper.list(site, queryForm, queryForm.getPage(), queryForm.getLimit());
        return RestResponse.ok(new PageInfo<>(ipRules));
    }

    @PostMapping("/{site}/create")
    @ApiOperation("Add IP rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "site", value = "0: BACK OFFICE, 1: FRONT OFFICE", allowableValues = "0,1")
    })
    public RestResponse<RestResponseIdSuccess> add(@PathVariable("site") Integer site,
            @RequestBody IpRuleForm ruleForm) {
        IpRule ipRule = new IpRule();
        BeanUtils.copyProperties(ruleForm, ipRule);
        ipRule.setSite(IpRuleConfigSiteEnum.valueOf(site));
        ipRuleMapper.insert(ipRule);
        return RestResponse.createSuccess(ipRule.getId());
    }

    @PostMapping("/{id}/update")
    @ApiOperation("Update IP rule. Note: Updating of site: frontend/backend is not allowed.")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable(value = "id") Integer id,
            @RequestBody IpRuleForm ruleForm) {
        IpRule ipRule = ipRuleMapper.selectByPrimaryKey(id);
        BeanUtils.copyProperties(ruleForm, ipRule);
        Boolean result = ipRuleMapper.updateByPrimaryKeySelective(ipRule);
        return RestResponse.operationResult(result);
    }

    @PostMapping("/{id}/delete")
    @ApiOperation("Delete IP rule")
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable("id") Integer id) {
        Boolean result = ipRuleMapper.deleteById(id);
        return RestResponse.operationResult(result);
    }

    public static class CountryListForm {
        @ApiModelProperty(required = true, value = "List of country codes to allow access")
        private List<String> countryCodes;

        public List<String> getCountryCodes() {
            if(countryCodes == null){
                countryCodes = new ArrayList<String>();
            }
            return countryCodes;
        }

        public void setCountryCodes(List<String> countryCodes) {
            this.countryCodes = countryCodes;
        }
    }
}
