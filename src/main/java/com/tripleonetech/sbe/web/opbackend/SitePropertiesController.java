package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.site.SiteProperty;
import com.tripleonetech.sbe.site.SitePropertyKey.LiveChatInfoEnum;
import com.tripleonetech.sbe.site.SitePropertyKey.SiteInfoEnum;
import com.tripleonetech.sbe.site.SitePropertyMapper;
import com.tripleonetech.sbe.web.common.SitePropertiesReadController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Profile("main")
@RestController
@RequestMapping("/site-properties")
@Api(tags = {"Site Properties"})
public class SitePropertiesController {
    @Autowired
    private SitePropertyMapper sitePropertyMapper;

    @Transactional
    @PostMapping("/live-chat")
    @ApiOperation("Update customer service contact information")
    public RestResponse<RestResponseDataSuccess> update(@RequestBody SitePropertiesReadController.LiveChatInfoForm form) throws Exception {
        List<SiteProperty> list = SiteProperty.getFields(LiveChatInfoEnum.values(), form);
        list.forEach(sitePropertyMapper::update);
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/site-info")
    @ApiOperation("Update site information")
    public RestResponse<RestResponseDataSuccess> updateSiteInfo(@RequestBody SitePropertiesReadController.SiteInfoForm siteInfoForm)
            throws Exception {
        List<SiteProperty> list = SiteProperty.getFields(SiteInfoEnum.values(), siteInfoForm);
        list.forEach(sitePropertyMapper::update);
        return RestResponse.operationSuccess();
    }
}
