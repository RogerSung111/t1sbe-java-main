package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.operator.OperatorPermission;

// Defines PreAuthorize expressions for permission evaluator
public class Permissions {
    public static class Player {
        public static final String LIST = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_LIST + "," + OperatorPermission.READ + "')";
        public static final String PROFILE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.READ + "')";
        public static final String CREATE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.WRITE + "')";
        public static final String PROFILE_UPDATE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.WRITE + "')";
        public static final String IDENTITY_UPDATE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.WRITE + "')";
        public static final String STATUS_UPDATE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.WRITE + "')";
        public static final String PASSWORD_UPDATE = "hasPermission('', '" + OperatorPermission.PLAYER + "," + OperatorPermission.PLAYER_ACCOUNT + "," + OperatorPermission.WRITE + "')";
    }

    public static class Operator {
        public static final String PASSWORD_UPDATE = "hasPermission('', '" + OperatorPermission.OPERATOR + "," + OperatorPermission.OPERATOR_ACCOUNT + "," + OperatorPermission.WRITE + "')";
    }
}
