package com.tripleonetech.sbe.web.opbackend;

import io.swagger.annotations.*;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusView;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.bonus.PromoCampaignBonusQueryForm;
import com.tripleonetech.sbe.promo.campaign.*;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDeposit;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDepositForm;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDepositMapper;
import com.tripleonetech.sbe.promo.campaign.login.*;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTask;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskForm;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/promotions/campaigns")
@Profile("main")
@Api(tags = { "Promotion - Campaign" })
public class PromoCampaignController {
    private static final Logger logger = LoggerFactory.getLogger(PromoCampaignController.class);

    @Autowired
    private PromoCampaignListMapper campaignListMapper;
    @Autowired
    private PromoCampaignDepositMapper depositMapper;
    @Autowired
    private PromoCampaignTaskMapper taskMapper;
    @Autowired
    private PromoBonusMapper bonusMapper;
    @Autowired
    private PromoCampaignLoginMapper loginMapper;
    @Autowired
    private PromoCampaignLoginRuleMapper campaignLoginRuleMapper;
    @Autowired
    private PromoCampaignService campaignService;
    @Autowired
    private PromoCampaignPlayerService campaignPlayerService;

    // Return the respective mapper
    private PromoCampaignMapper getPromoMapper(PromoTypeEnum type) {
        switch (type) {
        case CAMPAIGN_DEPOSIT:
            return depositMapper;
        case CAMPAIGN_TASK:
            return taskMapper;
        case CAMPAIGN_LOGIN:
            return loginMapper;
        }
        return null;
    }

    // -- Listing campaigns --
    @GetMapping("")
    @ApiOperation("List campaigns. status: DRAFT(0), PENDING_APPROVAL(1), REJECTED(9), APPROVED(10), DELETED(20); " +
            "running status: PENDING(0), RUNNING(1), FINISHED(2)")
    public RestResponse<PageInfo<PromoCampaignListView>> list(PromoCampaignListQueryForm form) {
        List<PromoCampaignListView> campaigns = campaignListMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(campaigns));
    }

    @GetMapping("/draft")
    @ApiOperation("List campaigns that's eligible for editing, including status: DRAFT(0), PENDING_APPROVAL(1), REJECTED(9)")
    public RestResponse<PageInfo<PromoCampaignListView>> listDraft(PromoCampaignListQueryForm form) {
        form.setStatus(Arrays.asList(
                CampaignApprovalStatusEnum.DRAFT.getCode(),
                CampaignApprovalStatusEnum.PENDING_APPROVAL.getCode(),
                CampaignApprovalStatusEnum.REJECTED.getCode()
        ));
        List<PromoCampaignListView> campaigns = campaignListMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(campaigns));
    }

    @GetMapping("/review")
    @ApiOperation("List campaigns that's been submitted for review, including status: PENDING_APPROVAL(1)")
    public RestResponse<PageInfo<PromoCampaignListView>> listReview(PromoCampaignListQueryForm form) {
        form.setStatus(Arrays.asList(
                CampaignApprovalStatusEnum.PENDING_APPROVAL.getCode()
        ));
        List<PromoCampaignListView> campaigns = campaignListMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(campaigns));
    }

    @GetMapping("/live")
    @ApiOperation("List campaigns that's been approved (i.e. live in production), including status: APPROVED(10)")
    public RestResponse<PageInfo<PromoCampaignListView>> listLive(PromoCampaignListQueryForm form) {
        form.setStatus(Arrays.asList(
                CampaignApprovalStatusEnum.APPROVED.getCode()
        ));
        form.setStartTime(ZonedDateTime.now()); // This will filter out completed
        List<PromoCampaignListView> campaigns = campaignListMapper.query(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(campaigns));
    }

    @GetMapping("/completed")
    @ApiOperation("List campaigns that's been completed (i.e. live in production), including status: APPROVED(10)")
    public RestResponse<PageInfo<PromoCampaignListView>> listCompleted(PromoCampaignListQueryForm form) {
        form.setStatus(Arrays.asList(
                CampaignApprovalStatusEnum.APPROVED.getCode()
        ));
        List<PromoCampaignListView> campaigns = campaignListMapper.queryCompleted(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(campaigns));
    }

    // Getting campaign info
    @GetMapping("/{type}/{id}")
    @ApiOperation("Get detail of specific campaign. status: DRAFT(0), PENDING_APPROVAL(1), APPROVED(10), DELETED(20)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task,login"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<PromoCampaign> get(@PathVariable String type, @PathVariable int id)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign model = getPromoMapper(promoType).get(id);
        if(model == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        return RestResponse.ok(model);
    }

    // Campaign stats
    @GetMapping("/{type}/{id}/bonuses")
    @ApiOperation("Get list of bonuses issued by this campaign. Allowed sort columns: only by bonusDate DESC")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task, login", allowableValues = "deposit,task,login"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<PageInfo<BonusView>> getBonuses(@PathVariable String type,
                                                        @PathVariable int id,
                                                        PageQueryForm queryForm)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaignBonusQueryForm bonusQueryForm = new PromoCampaignBonusQueryForm();

        BeanUtils.copyProperties(queryForm, bonusQueryForm);
        // type of campaign
        bonusQueryForm.setPromoTypes(Arrays.asList(promoType.getCode()));
        // id of campaign
        bonusQueryForm.setRuleId(id);

        List<BonusView> bonusList = bonusMapper.queryByCampaignQueryForm(bonusQueryForm, bonusQueryForm.getPage(), bonusQueryForm.getLimit());
        return RestResponse.ok(new PageInfo<>(bonusList));
    }

    @GetMapping("/{type}/{id}/players")
    @ApiOperation("Get list of players receiving bonus from this campaign. Allowed sort columns: id, username")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<PageInfo<PlayerIdView>> getPlayers(@PathVariable String type,
                                                 @PathVariable int id,
                                                 PageQueryForm queryForm)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        List<PlayerIdView> playerList = bonusMapper.queryBonusPlayerByCampaign(
                id, promoType.getCode(), queryForm,
                queryForm.getPage(), queryForm.getLimit());
        return RestResponse.ok(new PageInfo<>(playerList));
    }

    // Processing campaigns
    @PostMapping("/{type}/{id}/delete")
    @ApiOperation("Delete a campaign. A campaign has to be in DRAFT(0) status to be deleted.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable String type, @PathVariable int id)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.DRAFT && promo.getStatus() != CampaignApprovalStatusEnum.REJECTED) {
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in DRAFT(0) or REJECTED(9) status to be eligible for delete.", type, id))
            );
        }
        getPromoMapper(promoType).delete(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{type}/{id}/submit")
    @ApiOperation("Submit a draft campaign for approval. Status: DRAFT(0) --> PENDING_APPROVAL(1)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task, login", allowableValues = "deposit,task,login"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<RestResponseDataSuccess> submitForApproval(@PathVariable String type, @PathVariable int id)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.DRAFT){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in DRAFT(0) status in order to be submitted for approval.", type, id))
            );
        }
        getPromoMapper(promoType).submit(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{type}/{id}/reject")
    @ApiOperation("Rejects a pending approval campaign. Status: PENDING_APPROVAL(1) --> REJECTED(9)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<RestResponseDataSuccess> reject(
            @PathVariable String type, @PathVariable int id,
            @Valid @RequestBody RejectReasonForm reasonForm)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.PENDING_APPROVAL){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in PENDING_APPROVAL(1) status in order to reject.", type, id))
            );
        }
        getPromoMapper(promoType).reject(id, reasonForm.getReason());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{type}/{id}/approve")
    @ApiOperation("Approve a pending-approval campaign. Operator should have corresponding privilege to call this API. " +
            "Status: PENDING_APPROVAL(1) --> APPROVED(2)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the campaign")
    })
    public RestResponse<RestResponseDataSuccess> approve(@PathVariable String type, @PathVariable int id)
        throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.PENDING_APPROVAL){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in PENDING_APPROVAL(1) status in order to be approved.", type, id))
            );
        }
        // If campaign already passes start time when approved, it should start immediately, i.e. effectiveStartTime will be set as NOW
        LocalDateTime effectiveStartTime = null;
        if(promo.getStartTime().isBefore(LocalDateTime.now())) {
            effectiveStartTime = LocalDateTime.now();
        }
        getPromoMapper(promoType).approve(id, effectiveStartTime);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{type}/{id}/start")
    @ApiOperation("Start the campaign before its configured start time. The campaign must be in APPROVED status and PENDING running status to perform this action.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the campaign")
    })
    public RestResponse<RestResponseDataSuccess> start(@PathVariable String type, @PathVariable int id)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.APPROVED){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in APPROVED(1) status in order to be started.", type, id))
            );
        }
        if(promo.getRunningStatus() != CampaignRunningStatusEnum.PENDING) {
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] is [%s] and cannot be started again.", type, id, promo.getRunningStatus()))
            );
        }

        getPromoMapper(promoType).start(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{type}/{id}/terminate")
    @ApiOperation("Terminate the campaign before its configured end time. The campaign must be in APPROVED status and PENDING/RUNNING running status to perform this action.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task", allowableValues = "deposit,task"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the campaign")
    })
    public RestResponse<RestResponseDataSuccess> end(@PathVariable String type, @PathVariable int id)
            throws RestResponseException {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign promo = getPromoMapper(promoType).get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [%s][%d] not found", type, id))
            );
        }
        if(promo.getStatus() != CampaignApprovalStatusEnum.APPROVED){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] should be in APPROVED(1) status in order to be started.", type, id))
            );
        }
        if(promo.getRunningStatus() == CampaignRunningStatusEnum.FINISHED) {
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [%s][%d] is already finished.", type, id, promo.getRunningStatus()))
            );
        }

        getPromoMapper(promoType).terminate(id);
        return RestResponse.operationSuccess();
    }

    // -- Below controllers are specific to its type --
    // Note: Create form has @Valid while update form does not
    @PostMapping("/deposit/create")
    @ApiOperation("Create new deposit campaign. bonusReceiveCycle: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5); ")
    public RestResponse<RestResponseIdSuccess> save(@Valid @RequestBody PromoCampaignDepositForm model) {
        PromoCampaignDeposit campaignDeposit = PromoCampaignDeposit.from(model);
        campaignDeposit.setStatus(CampaignApprovalStatusEnum.DRAFT);
        depositMapper.insert(campaignDeposit);
        return RestResponse.createSuccess(campaignDeposit.getId());
    }

    @PostMapping("/task/create")
    @ApiOperation("Create new task campaign. bonusReceiveCycle: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5); " +
            "taskType: EmailVerificationBonus(0), SmsVerificationBonus(1), IdentityVerificationBonus(2), LoginBonus(3)")
    public RestResponse<RestResponseIdSuccess> save(@Valid @RequestBody PromoCampaignTaskForm model) {
        PromoCampaignTask campaignTask = PromoCampaignTask.from(model);
        campaignTask.setStatus(CampaignApprovalStatusEnum.DRAFT);
        taskMapper.insert(campaignTask);
        return RestResponse.createSuccess(campaignTask.getId());
    }

    @PostMapping("/login/create")
    @ApiOperation("Create new login campaign")
    public RestResponse<RestResponseIdSuccess> save(@Valid @RequestBody PromoCampaignLoginForm model)
            throws RestResponseException {
        PromoCampaignLogin campaignLogin = PromoCampaignLogin.from(model);
        campaignLogin.setStatus(CampaignApprovalStatusEnum.DRAFT);
        boolean result = campaignService.insertPromoCampaignLoginWithRules(campaignLogin);
        if (!result) {
            throw new RestResponseException(RestResponse.operationResult(false));
        }
        return RestResponse.createSuccess(campaignLogin.getId());
    }

    @PostMapping("/deposit/{id}")
    @ApiOperation("Update a deposit campaign. bonusReceiveCycle: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5);")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of deposit campaign to update")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id, @RequestBody PromoCampaignDepositForm model)
            throws RestResponseException {
        PromoCampaign promo = depositMapper.get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [deposit][%d] not found", id))
            );
        }
        if(promo.getStatus() == CampaignApprovalStatusEnum.APPROVED){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [deposit][%d] is in APPROVED(1) status, cannot be edited.", id))
            );
        }

        PromoCampaignDeposit campaignDeposit = PromoCampaignDeposit.from(model);
        campaignDeposit.setId(id);
        if(campaignDeposit.getFixedBonus() == null && campaignDeposit.getPercentageBonus() == null) {
            throw new RestResponseException(RestResponse.invalidParameter("Please provide either fixedBonus or percentageBonus"));
        }
        depositMapper.update(campaignDeposit);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/task/{id}")
    @ApiOperation("Update a task campaign. bonusReceiveCycle: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5); " +
            "taskType: EmailVerificationBonus(0), SmsVerificationBonus(1), IdentityVerificationBonus(2), LoginBonus(3)")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of deposit campaign to update")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id, @RequestBody PromoCampaignTaskForm model) throws RestResponseException {
        PromoCampaign promo = taskMapper.get(id);
        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [task][%d] not found", id))
            );
        }
        if(promo.getStatus() == CampaignApprovalStatusEnum.APPROVED){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [task][%d] is in APPROVED(1) status, cannot be edited.", id))
            );
        }

        PromoCampaignTask campaignTask = PromoCampaignTask.from(model);
        campaignTask.setId(id);
        taskMapper.update(campaignTask);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/login/{id}")
    @ApiOperation("Update a login campaign. bonusReceiveCycle: ONCE(0), UNLIMITED(1), DAILY(2), WEEKLY(3), MONTHLY(4), YEARLY(5)")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of login campaign to update")
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id,
            @RequestBody PromoCampaignLoginForm model) throws RestResponseException {
        PromoCampaign promo = loginMapper.get(id);

        if(promo == null){
            throw new RestResponseException(RestResponse.campaignNotFound(
                    String.format("Campaign [login][%d] not found", id))
            );
        }
        if(promo.getStatus() == CampaignApprovalStatusEnum.APPROVED){
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Campaign [login][%d] is in APPROVED(1) status, cannot be edited.", id))
            );
        }

        PromoCampaignLogin campaignLogin = PromoCampaignLogin.from(model);
        campaignLogin.setId(id);
        campaignService.updatePromoCampaignLogin(campaignLogin);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/login/{id}/days/create")
    @ApiOperation("Add new required login day rules for specified campaign")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of login campaign to add required login day rules")
    public RestResponse<RestResponseDataSuccess> addRequiredLoginDayRule(@PathVariable int id,
            @RequestBody List<PromoCampaignLoginRuleForm> rules) {
        List<PromoCampaignLoginRule> campaignLoginRules = new ArrayList<>();
        for (PromoCampaignLoginRuleForm rule : rules) {
            PromoCampaignLoginRule campaignLoginRule = new PromoCampaignLoginRule();
            BeanUtils.copyProperties(rule, campaignLoginRule);
            campaignLoginRule.setCampaignId(id > 0 ? id : null);
            campaignLoginRules.add(campaignLoginRule);
        }
        if (CollectionUtils.isEmpty(campaignLoginRules)) {
            return RestResponse.operationFailed("Rules cannot be null");
        }
        campaignLoginRuleMapper.insert(campaignLoginRules);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/login/{id}/days/delete")
    @ApiOperation("delete existing login campaign required login day rule")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of login campaign to delete required login day rules")
    public RestResponse<RestResponseDataSuccess> deleteRequiredLoginDayRule(@PathVariable long id,
            @Valid @RequestBody LoginDayBatchDeleteForm form) throws RestResponseException {
        List<RestResponseDataSuccess> iResultList = new ArrayList<>();
        for (Byte loginDay : form.getLoginDays()) {
            boolean result = campaignLoginRuleMapper.delete(id, loginDay);
            if (result) {
                iResultList.add(RestResponseDataSuccess.success("loginDay: " + loginDay.toString()));
            } else {
                iResultList.add(RestResponseDataSuccess.fail("loginDay: " + loginDay.toString()));
            }
        }
        if (!iResultList.isEmpty()) {
            throw new RestResponseException(RestResponse.multiStatus(iResultList));
        }
        return RestResponse.operationSuccess();
    }

    protected static class LoginDayBatchDeleteForm {
        @NotNull
        @ApiModelProperty(value = "Consecutive days of login", required = true)
        List<Byte> loginDays;

        public List<Byte> getLoginDays() {
            return loginDays;
        }

        public void setLoginDays(List<Byte> loginDays) {
            this.loginDays = loginDays;
        }
    }
}
