package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.*;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/messages")
@Profile("main")
@Api(tags = {"Message & Announcement"})
public class PlayerMessageController extends BaseOpbackendController {
    private final static Logger logger = LoggerFactory.getLogger(PlayerMessageController.class);

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerMessageService playerMessageService;
    @Autowired
    private PlayerMessageTemplateSettingMapper playerMessageTemplateSettingMapper;

    @GetMapping("")
    @ApiOperation("Query player message. Allowed sort columns: updatedAt, id (default)")
    public RestResponse<PageInfo<PlayerMessageView>> queryMessage(
            @Valid PlayerMessageQueryForm playerMessageQueryForm) {
        PageInfo<PlayerMessageView> messageList = new PageInfo<>(
                playerMessageService.listMessage(playerMessageQueryForm));
        return RestResponse.ok(messageList);
    }

    @GetMapping("/threads")
    @ApiOperation("Returns player message threads, sort by updatedAt (default) DESC only.")
    public RestResponse<PageInfo<PlayerMessageThreadListView>> queryThreads(
            @Valid PlayerMessageThreadListQueryForm playerMessageQueryForm) {
        PageInfo<PlayerMessageThreadListView> threadList = new PageInfo<>(
                playerMessageService.listThread(playerMessageQueryForm));
        return RestResponse.ok(threadList);
    }

    @GetMapping("/threads/{threadId}")
    @ApiOperation("Returns a single thread's messages, sort by updatedAt (default) DESC only")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "threadId", value = "The ID of the player message thread"),
    })
    public RestResponse<List<PlayerMessageView>> getThread(
            @PathVariable Integer threadId) {
        PlayerMessageQueryForm playerMessageQueryForm = new PlayerMessageQueryForm();
        playerMessageQueryForm.setThreadId(threadId);
        playerMessageQueryForm.setSort("updatedAt DESC");
        playerMessageQueryForm.setDeleted(false);
        List<PlayerMessageView> threadList = playerMessageService.listMessage(playerMessageQueryForm);
        return RestResponse.ok(threadList);
    }

    @PostMapping("/send")
    @ApiOperation("Send message to players")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "body", name = "playerMessage", value = "The message being send to player", dataType = "PlayerMessage")
    })
    public RestResponse<RestResponseIdSuccess> sendMessage(@Valid @RequestBody MessageForm messageForm) throws RestResponseException {
        Integer playerId = playerMapper.getPlayerIdByCredentialId(messageForm.getPlayerId(), messageForm.getCurrency());

        PlayerMessage playerMessage = new PlayerMessage();
        playerMessage.setContent(messageForm.getContent());
        playerMessage.setSubject(messageForm.getSubject());
        playerMessage.setPlayerId(playerId);
        playerMessage.setOperatorId(getLoginOperator().getId());
        playerMessageService.sendMessage(playerMessage);

        if (null == playerMessage.getId()) {
            String message = "Failed inserting player message: " + playerMessage.toString();
            logger.error(message);
            throw new RestResponseException(RestResponse.operationFailed(message));
        }
        if (playerMessage.getId() != null && playerMessage.getId() > 0) {
            playerMessageService.newThread(playerMessage.getId());
        }
        return RestResponse.createSuccess(playerMessage.getId());
    }

    @PostMapping("/{messageId}/reply")
    @ApiOperation("Reply to a message")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "messageId", value = "The ID of the player message"),
    })
    public RestResponse<RestResponseIdSuccess> replyMessage(
            @PathVariable Integer messageId,
            @Valid @RequestBody MessageContentForm contentForm) throws RestResponseException {
        PlayerMessage msg = playerMessageService.operatorReplyMessage(messageId, getLoginOperator().getId(), contentForm.getContent());
        if (null == msg || null == msg.getId()) {
            String message = String.format("Failed replying to message [%d]", messageId);
            logger.error(message);
            throw new RestResponseException(RestResponse.operationFailed(message));
        }
        return RestResponse.createSuccess(msg.getId());
    }

    @PostMapping("/{messageId}/delete")
    @ApiOperation("Delete message")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "messageId", value = "The ID of the player message"),
    })
    public RestResponse<RestResponseDataSuccess> deleteMessage(@PathVariable Integer messageId) {
        int updatedRow = playerMessageService.deleteMessage(messageId);
        if (updatedRow < 1) {
            String message = "Failed deleting message with message Id: " + messageId;
            logger.error(message);
            return RestResponse.operationFailed(message);
        }

        return RestResponse.operationSuccess();
    }

    @PostMapping("/delete-batch")
    @ApiOperation("Delete message")
    public RestResponse<RestResponseDataSuccess> batchDeleteMessage(@RequestBody @Valid BatchDeletedForm form) {
        int updatedRow = playerMessageService.batchDeleteMessage(form.getMessageIds());
        if (updatedRow < 1) {
            String message = "Failed deleting message with message Id: " + form.getMessageIds();
            logger.error(message);
            return RestResponse.operationFailed(message);
        }

        return RestResponse.operationSuccess();
    }

    @GetMapping("/templates")
    @ApiOperation("list all player message template setting")
    public RestResponse<List<PlayerMessageTemplateSettingView>> listTemplates() {
        List<PlayerMessageTemplateSettingView> resultList = new ArrayList<>();
        List<PlayerMessageTemplateSetting> playerMessageTemplateSettings = playerMessageTemplateSettingMapper.listAll();

        for (PlayerMessageTemplateSetting playerMessageTemplateSetting : playerMessageTemplateSettings) {
            PlayerMessageTemplateSettingView view = new PlayerMessageTemplateSettingView();
            BeanUtils.copyProperties(playerMessageTemplateSetting, view);
            view.setAvailableProperties(PlayerMessageTemplate.getAvailableProperties(playerMessageTemplateSetting.getPlayerMessageTemplateType()));
            view.setUpdatedAt(DateUtils.localToZoned(playerMessageTemplateSetting.getUpdatedAt()));

            resultList.add(view);
        }

        return RestResponse.ok(resultList);
    }


    @PostMapping("/templates/{templateId}")
    @ApiOperation("update player message template subject and content")
    public RestResponse<RestResponseDataSuccess> updateContent(@PathVariable Integer templateId, @RequestBody TemplateContentForm param) {
        PlayerMessageTemplateSetting playerMessageTemplateSetting = playerMessageTemplateSettingMapper.get(templateId);
        if (playerMessageTemplateSetting == null) {
            return RestResponse.entityNotFound(String.format("player message template not found, id = [%s]", templateId));
        }
        playerMessageTemplateSetting.setSubject(param.getSubject());
        playerMessageTemplateSetting.setContent(param.getContent());
        playerMessageTemplateSettingMapper.update(playerMessageTemplateSetting);

        return RestResponse.operationSuccess();
    }

    public static class MessageForm {
        @NotBlank
        @ApiModelProperty(value = "Message title", required = true)
        private String subject;

        @NotBlank
        @ApiModelProperty(value = "Message content", required = true)
        private String content;

        @NotNull
        @ApiModelProperty(value = "Player id", required = true)
        private Integer playerId;

        @NotNull
        @ApiModelProperty(value = "Currency", required = true)
        private SiteCurrencyEnum currency;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Integer getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Integer playerId) {
            this.playerId = playerId;
        }

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }
    }


    private static class TemplateContentForm {
        @ApiModelProperty(value = "system message subject")
        private String subject;
        @ApiModelProperty(value = "system message content")
        private String content;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class MessageContentForm {
        @NotEmpty
        @ApiModelProperty(value = "Message content")
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    private static class BatchDeletedForm {
        @NotEmpty
        @ApiModelProperty(required = true, value = "The ID of the player messages")
        private List<Integer> messageIds;

        public List<Integer> getMessageIds() {
            return messageIds;
        }

        public void setMessageIds(List<Integer> messageIds) {
            this.messageIds = messageIds;
        }
    }
}
