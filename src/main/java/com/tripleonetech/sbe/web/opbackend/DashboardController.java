package com.tripleonetech.sbe.web.opbackend;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.*;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopDepositCategoryEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopPopularGameCategoryEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopWithdrawCategoryEnum;
import com.tripleonetech.sbe.common.web.RestResponse;

@RestController
@RequestMapping("/dashboard")
@Profile("main")
@Api(tags = { "Dashboard" })
public class DashboardController {

    @Autowired
    private SummaryDailyMapper summaryDailyMapper;
    @Autowired
    private SummaryBetGrossProfitStatusMapper summaryBetGrossProfitStatusMapper;
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;
    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;
    @Autowired
    private SummaryTopDepositMapper summaryTopDepositMapper;
    @Autowired
    private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;
    @Autowired
    private SummaryTopWinnerMapper summaryTopWinnerMapper;
    @Autowired
    private SummaryTopPopularGameMapper summaryTopPopularGameMapper;
    @Autowired
    private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;
    @Autowired
    private Constant constant;

    @GetMapping(value = "")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency") })
    public RestResponse<SummaryDaily> viewSummaryDaily(@RequestParam SiteCurrencyEnum currency) {
        SummaryDaily summaryDaily = summaryDailyMapper.query(currency);
        if (summaryDaily == null) {
            summaryDaily = SummaryDaily.getEmptyObject();
        }
        return RestResponse.ok(summaryDaily);
    }

    @GetMapping(value = "/betgrossprofit")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "LAST_WEEK, LAST_MONTH", name = "period", value = "Period type of the record (LAST_WEEK, LAST_MONTH)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency") })
    public RestResponse<List<SummaryBetGrossProfitStatus>> viewSummaryBetGrossProfitStatus(
            @RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency) {
        int limit;
        switch (period) {
        case LAST_WEEK:
            limit = 8;
            break;
        default:
            limit = 12;
        }

        List<SummaryBetGrossProfitStatus> summaryBetGrossProfitStatuses = summaryBetGrossProfitStatusMapper
                .query(period, currency, limit);
        if (summaryBetGrossProfitStatuses.isEmpty()) {
            summaryBetGrossProfitStatuses.add(SummaryBetGrossProfitStatus.getEmptyObject());
        }
        return RestResponse.ok(summaryBetGrossProfitStatuses);
    }

    @GetMapping(value = "/playerstatus")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_WEEK, LAST_MONTH", name = "period", value = "Period type of the record (TODAY, LAST_WEEK, LAST_MONTH)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency") })
    public RestResponse<List<SummaryPlayerStatus>> viewSummaryPlayerStatus(
            @RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency) {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate today = LocalDate.now(zoneId);
        int limit;
        switch (period) {
        case LAST_WEEK:
            limit = 8;
            break;
        case TODAY:
            limit = today.getDayOfMonth() - 1;
            break;
        default:
            limit = 12;
        }

        List<SummaryPlayerStatus> summaryPlayerStatusList = summaryPlayerStatusMapper.query(period, currency, limit);
        if (CollectionUtils.isEmpty(summaryPlayerStatusList)) {
            summaryPlayerStatusList = SummaryPlayerStatus.getEmptyObjectList();
        }
        return RestResponse.ok(summaryPlayerStatusList);
    }

    @GetMapping(value = "/netprofit")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_WEEK, LAST_MONTH", name = "period", value = "Period type of the record (TODAY, LAST_WEEK, LAST_MONTH)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency") })
    public RestResponse<List<SummaryNetProfit>> viewSummaryNetProfit(@RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency) {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate today = LocalDate.now(zoneId);
        int limit;
        switch (period) {
        case LAST_WEEK:
            limit = 8;
            break;
        case TODAY:
            limit = today.getDayOfMonth() - 1;
            break;
        default:
            limit = 12;
        }

        List<SummaryNetProfit> summaryNetProfitList = summaryNetProfitMapper.query(period, currency, limit);
        if (CollectionUtils.isEmpty(summaryNetProfitList)) {
            summaryNetProfitList = SummaryNetProfit.getEmptyObjectList();
        }
        return RestResponse.ok(summaryNetProfitList);
    }

    @GetMapping(value = "/topdeposit")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_7_DAYS, LAST_30_DAYS", name = "period", value = "Period type of the record (REAL, DAILY, MONTHLY)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency"),
            @ApiImplicitParam(required = true, paramType = "query", name = "category", value = "Computing category of the record (BY_COUNT, BY_AMOUNT, BY_MAX_AMOUNT)") })
    public RestResponse<List<SummaryTopDeposit>> viewSummaryTopDeposit(@RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency,
            @RequestParam SummaryTopDepositCategoryEnum category) {
        List<SummaryTopDeposit> summaryTopDepositList = summaryTopDepositMapper.query(period, currency,
                category);
        if (summaryTopDepositList.isEmpty()) {
            summaryTopDepositList = SummaryTopDeposit.getEmptyObjectList();
        }
        return RestResponse.ok(summaryTopDepositList);
    }

    @GetMapping(value = "/topwithdrawal")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_7_DAYS, LAST_30_DAYS", name = "period", value = "Period type of the record (REAL, DAILY, MONTHLY)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency"),
            @ApiImplicitParam(required = true, paramType = "query", name = "category", value = "Computing category of the record (BY_COUNT, BY_AMOUNT;)") })
    public RestResponse<List<SummaryTopWithdrawal>> viewSummaryTopWithdrawal(@RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency,
            @RequestParam SummaryTopWithdrawCategoryEnum category) {
        List<SummaryTopWithdrawal> summaryTopWithdrawalList = summaryTopWithdrawalMapper.query(period, currency,
                category);
        if (summaryTopWithdrawalList == null) {
            summaryTopWithdrawalList = SummaryTopWithdrawal.getEmptyObjectList();
        }
        return RestResponse.ok(summaryTopWithdrawalList);
    }

    @GetMapping(value = "/topwinner")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_7_DAYS, LAST_30_DAYS", name = "period", value = "Period type of the record (REAL, DAILY, MONTHLY)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency") })
    public RestResponse<List<SummaryTopWinner>> viewSummaryTopWinner(@RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency) {
        List<SummaryTopWinner> summaryTopWinnerList = summaryTopWinnerMapper.query(period, currency);
        if (summaryTopWinnerList == null) {
            summaryTopWinnerList = SummaryTopWinner.getEmptyObjectList();
        }
        return RestResponse.ok(summaryTopWinnerList);
    }

    @GetMapping(value = "/toppopulargame")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_7_DAYS, LAST_30_DAYS", name = "period", value = "Period type of the record (REAL, DAILY, MONTHLY)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency"),
            @ApiImplicitParam(required = true, paramType = "query", name = "category", value = "Computing category of the record (BY_PLAYER, BY_BET, BY_WIN, BY_LOSS, BY_REV, BY_REV_RATIO, BY_AVERAGE_BET)"),
            @ApiImplicitParam(required = false, paramType = "query", name = "locale", value = "locale of the record (en-US,zh-CN,in,ko-KR,th,vi-VN). Default value of en-US if blank.") })
    public RestResponse<List<SummaryTopPopularGame>> viewSummaryTopGamePopular(
            @RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency,
            @RequestParam SummaryTopPopularGameCategoryEnum category,
            @RequestParam(required = false) String locale) {
        List<SummaryTopPopularGame> summaryTopPopularGameList = summaryTopPopularGameMapper.query(period, currency,
                category, locale, constant.getBackend().getDefaultLocale());
        if (summaryTopPopularGameList == null) {
            summaryTopPopularGameList = SummaryTopPopularGame.getEmptyObjectList();
        }
        return RestResponse.ok(summaryTopPopularGameList);
    }

    @GetMapping(value = "/gameproviderinfo")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", allowableValues = "TODAY, LAST_30_DAYS, THIS_MONTH", name = "period", value = "Period type of the record (REAL, DAILY, MONTHLY)"),
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency"),
            @ApiImplicitParam(required = false, paramType = "query", name = "locale", value = "locale of the record (en-US,zh-CN,in,ko-KR,th,vi-VN). Default value of en-US if blank.") })
    public RestResponse<List<SummaryGameProviderInfo>> viewSummaryGameProviderInfo(
            @RequestParam SummaryPeriodTypeEnum period,
            @RequestParam SiteCurrencyEnum currency,
            @RequestParam(required = false) String locale) {
        List<SummaryGameProviderInfo> summaryGameProviderInfoList = summaryGameProviderInfoMapper.query(period,
                currency, locale, constant.getBackend().getDefaultLocale());
        if (summaryGameProviderInfoList == null) {
            summaryGameProviderInfoList = SummaryGameProviderInfo.getEmptyObjectList();
        }
        return RestResponse.ok(summaryGameProviderInfoList);
    }
}
