package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSetting;
import com.tripleonetech.sbe.promo.referral.PromoPlayerReferralSettingMapper;
import io.swagger.annotations.*;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/promotions/referral")
@Profile("main")
@Api(tags = {"Promotion - Referral"})
public class PromoReferralController {

    @Autowired
    private PromoPlayerReferralSettingMapper promoReferralMapper;

    @GetMapping("/list")
    @ApiOperation("List ALL referral rules")
    public RestResponse<List<PromoPlayerReferralSettingView>> list() {
        List<PromoPlayerReferralSettingView> result = promoReferralMapper.list().stream()
                .map(PromoPlayerReferralSettingView::from)
                .collect(Collectors.toList());
        return RestResponse.ok(result);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get details of the specific referral rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of referral rule")
    })
    public RestResponse<PromoPlayerReferralSettingView> get(@PathVariable int id) {
        PromoPlayerReferralSetting promoReferral = promoReferralMapper.get(id);
        return RestResponse.ok(PromoPlayerReferralSettingView.from(promoReferral));
    }

    @PostMapping("/create")
    @Transactional
    @ApiOperation("Create a new referral rule")
    public RestResponse<RestResponseIdSuccess> create(@Valid @RequestBody ReferralSettingForm promoReferral) {
        PromoPlayerReferralSetting newSetting = new PromoPlayerReferralSetting();
        BeanUtils.copyProperties(promoReferral, newSetting);
        newSetting.setPlayerCredentialId(promoReferral.getPlayerId());
        promoReferralMapper.insert(newSetting);
        return RestResponse.createSuccess(newSetting.getId());
    }

    @PostMapping("/{id}")
    @Transactional
    @ApiOperation("Edit a referral rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the referral rule to be updated")
    })
    public RestResponse<RestResponseDataSuccess> update(@PathVariable int id, @Valid @RequestBody PromoPlayerReferralSettingView promoReferral) {
        promoReferral.setId(id);
        promoReferral.setPlayerCredentialId(promoReferral.getPlayerId());
        promoReferralMapper.update(promoReferral);
        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/{id}/enabled")
    @Transactional
    @ApiOperation("Enable / disable a referral rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the referral rule to be updated")
    })
    public RestResponse<RestResponseDataSuccess> updateEnabled(@PathVariable int id, @Valid @RequestBody BooleanForm form)
            throws RestResponseException {
        PromoPlayerReferralSetting promoReferral = promoReferralMapper.get(id);
        if (promoReferral == null) {
            throw new RestResponseException(
                    RestResponse.referralNotFound(String.format("referral rule not found with id = [%s]", id))
            );
        }
        promoReferral.setEnabled(form.isEnabled());
        promoReferralMapper.update(promoReferral);
        return RestResponse.operationSuccess();
    }

    @PostMapping(value = "/{id}/delete")
    @ApiOperation("Delete this referral rule")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the referral rule to be deleted")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable int id) throws RestResponseException {
        PromoPlayerReferralSetting promoReferral = promoReferralMapper.get(id);
        if (promoReferral == null) {
            throw new RestResponseException(
                    RestResponse.referralNotFound(String.format("referral rule not found with id = [%s]", id))
            );
        }
        promoReferral.setDeleted(true);
        promoReferralMapper.update(promoReferral);
        return RestResponse.operationSuccess();
    }

    @GetMapping(value = "/list/{playerId}")
    @ApiOperation("List referral rule for the specific player credential id. Use playerId = 0 to refer to the default set of referral rule.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "ID of the specified player.")
    })
    public RestResponse<List<PromoPlayerReferralSettingView>> listByCredentialId(@PathVariable Integer playerId) {
        List<PromoPlayerReferralSetting> list = promoReferralMapper.getByPlayerWithDefaultRules(null, playerId, null);

        List<PromoPlayerReferralSettingView> result = list.stream().map(PromoPlayerReferralSettingView::from).collect(Collectors.toList());
        if (playerId != 0) {
            result = result.stream().filter(rule -> rule.getPlayerCredentialId() != null).collect(Collectors.toList());
        }
        return RestResponse.ok(result);
    }

    public static class PromoPlayerReferralSettingView extends PromoPlayerReferralSetting {

        @ApiModelProperty(value = "Applicable player ID")
        private Integer playerId;

        public static PromoPlayerReferralSettingView from(PromoPlayerReferralSetting promoPlayerReferralSetting) {
            PromoPlayerReferralSettingView result = new PromoPlayerReferralSettingView();
            BeanUtils.copyProperties(promoPlayerReferralSetting, result);
            result.setPlayerId(promoPlayerReferralSetting.getPlayerCredentialId());

            return result;
        }

        public Integer getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Integer playerId) {
            this.playerId = playerId;
        }
    }

    public static class ReferralSettingForm {
        @ApiModelProperty(value = "Applicable player ID")
        private Integer playerId;

        @NotEmpty
        @ApiModelProperty(value= "Name of this referral")
        private String name;

        @ApiModelProperty(value= "Content (description) of this referral")
        private String content;

        @NotNull
        @ApiModelProperty(value = "Referral type: ONE_TIME_REVENUE(0), DEPOSIT_REVENUE(1), BET_REVENUE(2)", allowableValues = "0,1,2")
        private Integer referralType;

        @ApiModelProperty(value = "Fixed bonus amount for this referral")
        private BigDecimal fixedBonus;

        @NotNull
        @Range(max = 100, min = 0)
        @ApiModelProperty(value= "Percentage bonus for this referral")
        private BigDecimal bonusPercentage;

        @ApiModelProperty(required = true, value = "Whether bonus should need operator approval to release to player wallet")
        private boolean bonusAutoRelease;

        @ApiModelProperty(value= "Maximum first deposit amount this referral can grant")
        private BigDecimal minFirstDepositAmount;

        @ApiModelProperty(value= "Maximum bonus amount this referral can grant, can be empty if fixedBonus is used")
        private BigDecimal maxBonusAmount;

        @ApiModelProperty(value= "Minimum bonus amount this referral can grant, can be empty if fixedBonus is used")
        private BigDecimal minBonusAmount;

        @NotNull
        @ApiModelProperty(required = true, value = "How much withdraw condition should this bonus generate, minimum 0 i.e. no withdraw condition")
        private Integer withdrawConditionMultiplier;

        @NotNull
        @ApiModelProperty(required = true, value= "Currency of bonus")
        private SiteCurrencyEnum currency;

        public Integer getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Integer playerId) {
            this.playerId = playerId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Integer getReferralType() {
            return referralType;
        }

        public void setReferralType(Integer referralType) {
            this.referralType = referralType;
        }

        public BigDecimal getFixedBonus() {
            return fixedBonus;
        }

        public void setFixedBonus(BigDecimal fixedBonus) {
            this.fixedBonus = fixedBonus;
        }

        public BigDecimal getBonusPercentage() {
            return bonusPercentage;
        }

        public void setBonusPercentage(BigDecimal bonusPercentage) {
            this.bonusPercentage = bonusPercentage;
        }

        public boolean isBonusAutoRelease() {
            return bonusAutoRelease;
        }

        public void setBonusAutoRelease(boolean bonusAutoRelease) {
            this.bonusAutoRelease = bonusAutoRelease;
        }

        public BigDecimal getMinFirstDepositAmount() {
            return minFirstDepositAmount;
        }

        public void setMinFirstDepositAmount(BigDecimal minFirstDepositAmount) {
            this.minFirstDepositAmount = minFirstDepositAmount;
        }

        public BigDecimal getMaxBonusAmount() {
            return maxBonusAmount;
        }

        public void setMaxBonusAmount(BigDecimal maxBonusAmount) {
            this.maxBonusAmount = maxBonusAmount;
        }

        public BigDecimal getMinBonusAmount() {
            return minBonusAmount;
        }

        public void setMinBonusAmount(BigDecimal minBonusAmount) {
            this.minBonusAmount = minBonusAmount;
        }

        public Integer getWithdrawConditionMultiplier() {
            return withdrawConditionMultiplier;
        }

        public void setWithdrawConditionMultiplier(Integer withdrawConditionMultiplier) {
            this.withdrawConditionMultiplier = withdrawConditionMultiplier;
        }

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }
    }

}
