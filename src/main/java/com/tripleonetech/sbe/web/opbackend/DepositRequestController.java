package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.PaymentRequestForm.OperatorPaymentRequestForm;
import com.tripleonetech.sbe.payment.history.DepositRequestProcessHistoryMapper;
import com.tripleonetech.sbe.payment.history.DepositRequestProcessHistoryView;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.service.DepositRequestService;
import com.tripleonetech.sbe.player.Player;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/deposit-requests")
@Profile("main")
@Api(tags = {"Deposits & Withdrawals"})
public class DepositRequestController extends BaseOpbackendController {
    private static final Logger logger = LoggerFactory.getLogger(DepositRequestController.class);

    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private DepositRequestProcessHistoryMapper depositRequestProcessHistoryMapper;
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    @Autowired
    private DepositRequestService depositRequestService;

    @GetMapping("")
    @ApiOperation("Query deposit requests. This query will not return expired deposit requests by default. " +
            "Allowed sort columns: amount, expirationDate, requestedDate (default), "
            + "status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11), EXPIRED(12)")
    public RestResponse<PageInfo<DepositRequestView>> list(DepositRequestQueryForm query) {
        List<DepositRequestView> depositRequestView = depositRequestMapper.list(query, query.getPage(),
                query.getLimit());
        return RestResponse.ok(new PageInfo<>(depositRequestView));
    }

    @GetMapping("/{id}")
    @ApiOperation("Get the detail of deposit request, including status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of deposit request")
    })
    public RestResponse<DepositRequestView> getDetail(@PathVariable long id) {
        DepositRequestView view = depositRequestMapper.getView(id);
        return RestResponse.ok(view);
    }

    @Transactional
    @PostMapping("/payment-requests")
    @ApiOperation("Submit payment request")
    public RestResponse<PaymentApiResult> submitPaymentRequest(@Valid @RequestBody OperatorPaymentRequestForm paymentRequestForm)
            throws RestResponseException{
        Operator operator = getLoginOperator();

        //Create deposit_request
        DepositRequest depositRequest = depositRequestService.createDepositRequest(paymentRequestForm, operator);
        if (null == depositRequest) {
            throw new RestResponseException(RestResponse.operationFailed("Failed creating deposit request"));
        }

        PaymentApiResult result = new PaymentApiResult(ApiResponseEnum.OK);
        result.setParam("depositRequestId", depositRequest.getId());
        result.setParam("amount", depositRequest.getAmount());
        result.setParam("note", depositRequest.getNote());

        return RestResponse.ok(result);
    }

    @Transactional
    @PostMapping("/{id}")
    @ApiOperation("Update deposit request status and update wallet balance according to the target status")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of deposit request")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(@PathVariable long id,
                                                              @Valid @RequestBody DepositRequestApprovalForm form) throws RestResponseException {

        DepositStatusEnum status = form.getStatusEnum();
        DepositRequest depositRequest = depositRequestMapper.get(id);
        if (depositRequest.getStatus() != DepositStatusEnum.OPEN) {
            throw new RestResponseException(RestResponse.invalidStatus(
                    String.format("Deposit request [%d] should be in OPEN status to be updated, current status: [%s]",
                            id, depositRequest.getStatus().toString()))
            );
        }
        depositRequestService.processDepositRequest(depositRequest, status, getLoginOperator().getId());

        return RestResponse.operationSuccess();
    }

    @GetMapping("/{depositRequestId}/request-history")
    @ApiOperation("Get working history of deposit request, including action: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "depositRequestId", value = "ID of deposit request"),
    })
    public RestResponse<List<DepositRequestProcessHistoryView>> getWorkingHistory(@PathVariable Long depositRequestId) {
        List<DepositRequestProcessHistoryView> comments = depositRequestProcessHistoryMapper.getComments(depositRequestId);
        return RestResponse.ok(comments);
    }

    @GetMapping("/action-required-count")
    @ApiOperation("Get total count of pending deposit request")
    public RestResponse<Integer> getPendingDepositCount() {
        return RestResponse.ok(depositRequestMapper.getPendingRequestCount());
    }
}
