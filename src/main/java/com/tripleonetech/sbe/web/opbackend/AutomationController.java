package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.automation.*;
import com.tripleonetech.sbe.automation.scheduled.AutomationJobExecution;
import com.tripleonetech.sbe.common.model.NoPageQueryForm;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.player.PlayerIdView;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.tag.PlayerTagService;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/automation")
@Profile("main")
@Api(tags = { "Automation" })
public class AutomationController {
    private static final Logger logger = LoggerFactory.getLogger(AutomationController.class);
    @Autowired
    private AutomationJobMapper jobMapper;
    @Autowired
    private AutomationRecordMapper recordMapper;
    @Autowired
    private AutomationService automationService;
    @Autowired
    private AutomationJobExecution automationJobExecution;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerTagService playerTagService;

    @GetMapping("/jobs")
    @ApiOperation("List automation jobs. Allowed sort columns: name, startAt, updatedAt, createdAt, id (default)")
    public RestResponse<List<AutomationJob>> listJobs(NoPageQueryForm query) {
        List<AutomationJob> jobs = jobMapper.list(query);
        return RestResponse.ok(jobs);
    }

    @GetMapping("/jobs/{id}")
    @ApiOperation("Get single automation job")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the automation job")
    })
    public RestResponse<AutomationJob> getJob(@PathVariable Integer id) throws RestResponseException {
        AutomationJob job = jobMapper.get(id);
        if(job == null){
            throw new RestResponseException(RestResponse.entityNotFound(String.format("Automation job [%d] not found", id)));
        }
        return RestResponse.ok(job);
    }

    @GetMapping("/jobs/{id}/players")
    @ApiOperation("Get list of filtered players for this automation job")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the automation job")
    })
    public RestResponse<PageInfo<PlayerIdView>> getFilteredPlayers(@PathVariable Integer id, PageQueryForm query) throws RestResponseException {
        AutomationJob job = jobMapper.get(id);
        if(job == null){
            throw new RestResponseException(RestResponse.entityNotFound(String.format("Automation job [%d] not found", id)));
        }
        List<Integer> playerIds = automationService.getPlayerIdsByFilter(job.getFilters());
        List<PlayerIdView> players = playerMapper.listViewByPlayerIds(playerIds, query.getPage(), query.getLimit());
        return RestResponse.ok(new PageInfo<>(players));
    }

    @PostMapping("/jobs/create")
    @ApiOperation("Create new automation job. Run frequency: NEVER(0), EVERY_MINUTE(1), EVERY_HOUR(2), EVERY_DAY(3), EVERY_WEEK(4), EVERY_MONTH(5)")
    public RestResponse<RestResponseIdSuccess> createJob(@Validated @RequestBody AutomationJobForm form) throws Exception {
        validateGroupTag(form);

        jobMapper.insert(form);
        return RestResponse.createSuccess(form.getId());
    }

    private void validateGroupTag(AutomationJobForm form) throws RestResponseException {
        List<Integer> tagIds = new ArrayList<>();
        for (List<PlayerFilter> playerFilters : form.getFilters()) {
            List<String> valueList = playerFilters.stream()
                    .filter(e -> PlayerFilter.PlayerFilterEnum.parse(e.getId()) == PlayerFilter.PlayerFilterEnum.GROUP)
                    .collect(ArrayList::new,
                            (c, e) -> c.addAll(e.getValue()),
                            ArrayList::addAll);

            tagIds.addAll(valueList.stream().map(Integer::valueOf).collect(Collectors.toList()));
        }

        List<Integer> allGroupIds = playerTagService.listAllGroups().stream().map(e -> e.getId())
                .collect(Collectors.toList());
        for (Integer tagId : tagIds) {
            // Must be group tag id
            if (!allGroupIds.contains(tagId)) {
                throw new RestResponseException(
                        RestResponse.invalidParameter(String.format("Tag id [%d] is not a player group id", tagId)));
            }
        }
    }

    @PostMapping("/jobs/{id}")
    @ApiOperation("Update automation job. Run frequency: NEVER(0), EVERY_MINUTE(1), EVERY_HOUR(2), EVERY_DAY(3), EVERY_WEEK(4), EVERY_MONTH(5)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the automation job to be updated")
    })
    public RestResponse<RestResponseDataSuccess> updateJob(
            @PathVariable Integer id,
            @Validated @RequestBody AutomationJobForm form) throws RestResponseException {
        validateGroupTag(form);
        form.setId(id);
        jobMapper.update(form);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/jobs/{id}/records")
    @ApiOperation("Query automation run records. Allowed sort columns: jobName, runCount, createdAt, completedAt, updatedAt (default); " +
            "Status: IN_PROGRESS(1), COMPLETED(10), CANCELED(20), ERROR(21)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the automation job")
    })
    public RestResponse<PageInfo<AutomationRecord>> listRecords(
            @PathVariable Integer id,
            AutomationRecordQueryForm query) {
        query.setJobId(id);
        List<AutomationRecord> records = recordMapper.list(query);
        return RestResponse.ok(new PageInfo<>(records));
    }

    @PostMapping("/jobs/{id}/run")
    @ApiOperation("Execute the specified automation job.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of the automation job to be executed")
    })
    public RestResponse<RestResponseDataSuccess> runJob(@PathVariable Integer id) {
        AutomationJob job = jobMapper.get(id);
        automationJobExecution.executeManually(job);
        return RestResponse.operationSuccess();
    }
}
