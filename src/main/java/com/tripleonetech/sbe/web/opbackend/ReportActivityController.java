package com.tripleonetech.sbe.web.opbackend;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReport;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReportMapper;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReportQueryForm;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReportView;
import com.tripleonetech.sbe.report.operator.OperatorActivityReport;
import com.tripleonetech.sbe.report.operator.OperatorActivityReportMapper;
import com.tripleonetech.sbe.report.operator.OperatorActivityReportQueryForm;
import com.tripleonetech.sbe.report.operator.OperatorActivityReportView;
import com.tripleonetech.sbe.report.player.PlayerActivityReport;
import com.tripleonetech.sbe.report.player.PlayerActivityReportMapper;
import com.tripleonetech.sbe.report.player.PlayerActivityReportQueryForm;
import com.tripleonetech.sbe.report.player.PlayerActivityReportView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/reports")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportActivityController extends BaseOpbackendController{
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    @Autowired
    private OperatorActivityReportMapper operatorActivityReportMapper;
    @Autowired
    private ExtApiActivityReportMapper extApiActivityReportMapper;

    @GetMapping("/player-activity")
    @ApiOperation("Player activity report, sort columns: username, createdAt")
    public RestResponse<PageInfo<PlayerActivityReportView>> showPlayerActivityReport(PlayerActivityReportQueryForm form){
        List<PlayerActivityReport> results = playerActivityReportMapper.getReport(form, form.getPage(), form.getLimit());

        List<PlayerActivityReportView> playerActivityReportViews = results.stream()
                .map(PlayerActivityReportView::create).collect(Collectors.toList());

        PageInfo<PlayerActivityReport> pageInfo = new PageInfo<>(results);
        PageInfo<PlayerActivityReportView> listResult = new PageInfo<>();
        BeanUtils.copyProperties(pageInfo, listResult);
        listResult.setList(playerActivityReportViews);

        return RestResponse.ok(listResult);
    }

    @GetMapping("/player-activity/{id}")
    @ApiOperation("Get details of the specific player activity record")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of player activity report")
    })
    public RestResponse<PlayerActivityReport> getPlayerActivityReportDetail(@PathVariable long id) {
        PlayerActivityReport report = playerActivityReportMapper.get(id);

        return RestResponse.ok(report);
    }

    @GetMapping("/operator-activity")
    @ApiOperation("Operator activity report, sort columns: username, createdAt")
    public RestResponse<PageInfo<OperatorActivityReportView>> showOperatorActivityReport(
            OperatorActivityReportQueryForm form) {
        List<OperatorActivityReportView> results = operatorActivityReportMapper.getReport(form, form.getPage(), form.getLimit());

        PageInfo<OperatorActivityReportView> view = new PageInfo<>(results);
        return RestResponse.ok(view);
    }

    @GetMapping("/operator-activity/{id}")
    @ApiOperation("Get details of the specific operator activity record")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of operator activity report")
    })
    public RestResponse<OperatorActivityReport> getOperatorActivityReportDetail(@PathVariable long id) {
        OperatorActivityReport report = operatorActivityReportMapper.get(id);

        return RestResponse.ok(report);
    }

    @GetMapping("/ext-api-activity")
    @ApiOperation("Ext API activity report, sort columns: username, createdAt")
    public RestResponse<PageInfo<ExtApiActivityReportView>> showExtApiActivityReport(
            ExtApiActivityReportQueryForm form) {
        List<ExtApiActivityReportView> results = extApiActivityReportMapper.getReport(form, form
                .getPage(),
                form.getLimit());

        PageInfo<ExtApiActivityReportView> view = new PageInfo<>(results);
        return RestResponse.ok(view);
    }

    @GetMapping("/ext-api-activity/{id}")
    @ApiOperation("Get details of the specific operator activity record")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of ext api activity report")
    })
    public RestResponse<ExtApiActivityReport> getExtApiActivityReportDetail(@PathVariable long id) {
        ExtApiActivityReport report = extApiActivityReportMapper.get(id);

        return RestResponse.ok(report);
    }
}
