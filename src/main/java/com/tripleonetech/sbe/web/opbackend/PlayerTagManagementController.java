package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/players/{playerId}/tags")
@Profile("main")
@Api(tags = {"Player Group & Tags"})
@Validated
public class PlayerTagManagementController {

    private final boolean playerGroup = false;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;

    @Transactional
    @PostMapping("")
    @ApiOperation("Set player's tags")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> setTags(
            @PathVariable int playerId,
            @RequestBody TagIdsForm tagIdsForm) {
        List<Integer> tagIds = tagIdsForm.getTagIds();
        if (CollectionUtils.isEmpty(tagIds)) {
            return RestResponse.invalidParameter("No tag IDs found in request");
        }

        playersPlayerTagMapper.deleteAllWithoutPlayerGroup(playerId, !playerGroup);
        playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, tagIds);
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping(value = "/add")
    @ApiOperation("Add tags for given player")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> addTag(
            @PathVariable int playerId,
            @RequestBody TagIdsForm tagIdsForm) {
        playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, tagIdsForm.getTagIds());
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping(value = "/{tagId}/delete")
    @ApiOperation("Delete a tag for the specific player")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID"),
            @ApiImplicitParam(required = true, paramType = "path", name = "tagId", value = "The tag id to delete")
    })
    public RestResponse<RestResponseDataSuccess> deleteTag(
            @PathVariable int playerId,
            @PathVariable int tagId) {
        playersPlayerTagMapper.delete(playerId, Collections.singletonList(tagId));
        return RestResponse.operationSuccess();
    }


    private static class TagIdsForm {
        private List<Integer> tagIds;

        public List<Integer> getTagIds() {
            return tagIds;
        }

        public void setTagIds(List<Integer> tagIds) {
            this.tagIds = tagIds;
        }
    }
}
