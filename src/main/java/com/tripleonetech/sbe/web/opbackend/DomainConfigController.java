package com.tripleonetech.sbe.web.opbackend;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.domain.DomainConfig;
import com.tripleonetech.sbe.domain.DomainConfigForm;
import com.tripleonetech.sbe.domain.DomainConfigMapper;
import com.tripleonetech.sbe.domain.DomainConfigView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/domain-configs")
@Profile("main")
@Api(tags = { "System Settings" })
public class DomainConfigController extends BaseOpbackendController {
    
    @Autowired
    private DomainConfigMapper domainConfigMapper;
    
    @ApiOperation("Show all domain configs. Value for site: BackOffice(0), FrontOffice(1)")
    @GetMapping("")
    public RestResponse<List<DomainConfigView>> listDomainConfigs() {
        List<DomainConfig> domainConfigs = domainConfigMapper.list();
        List<DomainConfigView> view = domainConfigs.stream()
                .map(domain -> DomainConfigView.create(domain)).collect(Collectors.toList());
        return RestResponse.ok(view);
    }

    @ApiOperation("Batch update domains")
    @PostMapping("")
    public RestResponse<List<RestResponseIdSuccess>> batchUpdateDomainConfig(@RequestBody List<@Valid DomainConfigForm> form) throws RestResponseException{
        if(form.size() == 0) {
            throw new RestResponseException(RestResponse.invalidParameter("Must provide at least one domain info"));
        }
        domainConfigMapper.batchUpdate(form);
        List<RestResponseIdSuccess> responses = new ArrayList<>();
        form.forEach(item -> {
            responses.add(RestResponseIdSuccess.success(item.getId()));
        });
        return RestResponse.ok(responses);
    }
    
    @ApiOperation("Batch create domains")
    @PostMapping("/create")
    public RestResponse<List<RestResponseIdSuccess>> batchCreateDomainConfig(@RequestBody List<@Valid DomainConfigForm> form) throws RestResponseException{
        if(form.size() == 0) {
            throw new RestResponseException(RestResponse.invalidParameter("Must provide at least one domain info"));
        }
        domainConfigMapper.batchInsert(form);
        List<RestResponseIdSuccess> responses = new ArrayList<>();
        form.forEach(item -> {
            responses.add(RestResponseIdSuccess.success(item.getId()));
        });
        return RestResponse.ok(responses);
    }

    @ApiOperation("Delete domains")
    @PostMapping("/delete-batch")
    public RestResponse<RestResponseDataSuccess> deleteDomainConfig(@RequestBody List<Integer> domainConfigIds){
        domainConfigMapper.batchDelete(domainConfigIds);
        return RestResponse.operationSuccess();
    }
    
    
    @Transactional
    @ApiOperation("Enable or disable domain configs")
    @PostMapping("/enable-batch")
    public RestResponse<RestResponseDataSuccess> enableDomainConfig(@Valid @RequestBody List<DomainConfigSimpleForm> form) throws RestResponseException{
        
        if(form.size() == 0) {
            throw new RestResponseException(RestResponse.invalidParameter("Bad request with invalid parameters"));
        }
        for(DomainConfigSimpleForm domainConfig : form) {
            domainConfigMapper.enableDomainConfig(domainConfig.getId(), domainConfig.getEnabled());
        }
        return RestResponse.operationSuccess();
    }
    
    
    public static class  DomainConfigSimpleForm {
        @NotNull
        @ApiModelProperty(value = "Domain config ID")
        private Integer id;
        @NotNull
        @ApiModelProperty(value = "Enable domain name, enabled(1), disabled(0)")
        private Boolean enabled;
        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public Boolean getEnabled() {
            return enabled;
        }
        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }
    }
}
