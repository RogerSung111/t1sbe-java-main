package com.tripleonetech.sbe.web.opbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.player.message.PlayerMessageMapper;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import com.tripleonetech.sbe.promo.campaign.PromoCampaignListMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestMapper;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/todo")
@Profile("main")
public class TodoCountController extends BaseOpbackendController {
    @Autowired
    private PromoBonusMapper promoBonusMapper;
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;
    @Autowired
    private PromoCampaignListMapper promoCampaignListMapper;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;

    @GetMapping("")
    @ApiOperation("Get pending count of cashback, deposit request, withdraw request, campaign, bonus, player message.")
    public RestResponse<TodoCountView> getTodoCount() {
        TodoCountView view = new TodoCountView();

        // How many pending cashback bonus
        view.setCashbackPendingCount(promoBonusMapper.getPendingCount(true));
        // How many pending deposit request
        view.setDepositRequestPendingCount(depositRequestMapper.getPendingRequestCount());
        // How many pending withdraw request
        view.setWithdrawRequestPendingCount((int) withdrawRequestMapper.getPendingRequestCount());
        // How many pending campaign
        view.setCampaignPendingCount(promoCampaignListMapper.getPendingCount());
        // How many pending bonus exclude cashback
        view.setBonusPendingCount(promoBonusMapper.getPendingCount(false));
        // How many player message need to be reply
        view.setPlayerMessagePendingCount(playerMessageMapper.getUnreadCountOfOperator());

        return RestResponse.ok(view);
    }

    public static class TodoCountView {
        private int cashbackPendingCount;
        private int depositRequestPendingCount;
        private int withdrawRequestPendingCount;
        private int campaignPendingCount;
        private int bonusPendingCount;
        private int playerMessagePendingCount;

        public int getCashbackPendingCount() {
            return cashbackPendingCount;
        }

        public void setCashbackPendingCount(int cashbackPendingCount) {
            this.cashbackPendingCount = cashbackPendingCount;
        }

        public int getDepositRequestPendingCount() {
            return depositRequestPendingCount;
        }

        public void setDepositRequestPendingCount(int depositRequestPendingCount) {
            this.depositRequestPendingCount = depositRequestPendingCount;
        }

        public int getWithdrawRequestPendingCount() {
            return withdrawRequestPendingCount;
        }

        public void setWithdrawRequestPendingCount(int withdrawRequestPendingCount) {
            this.withdrawRequestPendingCount = withdrawRequestPendingCount;
        }

        public int getCampaignPendingCount() {
            return campaignPendingCount;
        }

        public void setCampaignPendingCount(int campaignPendingCount) {
            this.campaignPendingCount = campaignPendingCount;
        }

        public int getBonusPendingCount() {
            return bonusPendingCount;
        }

        public void setBonusPendingCount(int bonusPendingCount) {
            this.bonusPendingCount = bonusPendingCount;
        }

        public int getPlayerMessagePendingCount() {
            return playerMessagePendingCount;
        }

        public void setPlayerMessagePendingCount(int playerMessagePendingCount) {
            this.playerMessagePendingCount = playerMessagePendingCount;
        }
    }
}
