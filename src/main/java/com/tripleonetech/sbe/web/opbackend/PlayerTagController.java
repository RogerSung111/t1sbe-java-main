package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayerTagService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/tags")
@Profile("main")
@Api(tags = {"Player Group & Tags"})
public class PlayerTagController {
    @Autowired
    private PlayerTagService playerTagService;
    @Autowired
    private PlayerTagMapper playerTagMapper;

    @GetMapping("")
    @ApiOperation("List of all tags")
    public RestResponse<List<PlayerTag>> listAllTags() {
        return RestResponse.ok(playerTagService.listAllTags());
    }

    @GetMapping("/{id}")
    @ApiOperation("Get detail of the tag")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected tag"),
    })
    public RestResponse<PlayerTag> getTag(@PathVariable("id") int id) throws RestResponseException {
        PlayerTag tag = playerTagService.getTag(id);
        if (tag == null) {
            throw new RestResponseException(
                    RestResponse.playerTagNotFound("No tag found with id " + id)
            );
        }
        return RestResponse.ok(tag);
    }

    @PostMapping("/{id}")
    @ApiOperation("Update the tag")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected tag")
    })
    public RestResponse<RestResponseDataSuccess> saveTag(
            @PathVariable("id") int id,
            @Valid @RequestBody PlayerTagForm form) {
        PlayerTag tag = playerTagService.getTag(id);
        if (tag == null) {
            return RestResponse.playerTagNotFound("No tag found with id " + id);
        }

        PlayerTag playerTag = form.toPlayerTag();
        playerTag.setId(id);
        playerTagService.updatePlayerTag(playerTag);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/create")
    @ApiOperation("Create new tag")
    public RestResponse<RestResponseIdSuccess> create(
            @Valid @RequestBody PlayerTagForm form) {
        PlayerTag newTag = form.toPlayerTag();
        playerTagService.createTag(newTag);
        return RestResponse.createSuccess(newTag.getId());
    }

    @PostMapping(value = "/{id}/delete")
    @ApiOperation("Delete the tag")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "ID of the selected tag")
    })
    public RestResponse<RestResponseDataSuccess> delete(@PathVariable("id") int id) {
        PlayerTag tag = playerTagService.getTag(id);
        if (tag == null) {
            return RestResponse.playerTagNotFound("No tag found with id " + id);
        } else if (tag.getId().equals(playerTagMapper.getDefaultGroupId())) {
            return RestResponse.operationFailed("Cannot delete system Default group tag: " + id
                    + " (before you can delete this group, you will need reset system default group to another group)");
        }
        playerTagService.deletePlayerTag(id);
        return RestResponse.operationSuccess();
    }

    public static class PlayerTagForm {
        @ApiModelProperty(required = true, name = "name", value = "Tag name")
        @NotNull
        private String name;
        @ApiModelProperty(required = true, name = "Automation Job ID", value = "Automation Job ID")
        private Integer automationJobId;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAutomationJobId() {
            return automationJobId;
        }

        public void setAutomationJobId(Integer automationJobId) {
            this.automationJobId = automationJobId;
        }

        public PlayerTag toPlayerTag() {
            PlayerTag tag = new PlayerTag();
            tag.setPlayerGroup(false);
            tag.setName(this.getName());
            tag.setAutomationJobId(this.getAutomationJobId());
            return tag;
        }
    }
}
