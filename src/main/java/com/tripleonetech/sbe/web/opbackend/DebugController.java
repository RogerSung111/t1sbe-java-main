package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.iprule.country.GeoIpServiceViaApi;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/debug")
@Profile("main")
@Api(tags = {"Debug"})
public class DebugController {
    @Autowired
    private GeoIpServiceViaApi geoIpService;

    @GetMapping("/geo-ip/{ip}")
    public RestResponse<String> callGeoIpService(String ip) {
        String countryCode = geoIpService.getCountryCode(ip);
        return RestResponse.ok(String.format("IP: [%s], Country Code: [%s]", ip, countryCode));
    }
}
