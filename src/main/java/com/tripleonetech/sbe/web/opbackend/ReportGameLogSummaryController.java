package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryQueryForm;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryView.ListView;
import com.tripleonetech.sbe.game.log.ReportGameLogSummaryView.SummaryView;
import com.tripleonetech.sbe.common.web.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/reports/game-log-summary")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportGameLogSummaryController {
    @Autowired
    private Constant constant;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    @GetMapping("")
    @ApiOperation("Show Game Log Summary Report filtered by query criteria")
    public RestResponse<ReportView> list(@Valid ReportGameLogSummaryQueryForm form) {
        ReportView result = new ReportView();
        result.setList(new PageInfo<>(
                gameLogHourlyReportMapper.listReport(form, form.getPage(), form.getLimit(),
                        LocaleContextHolder.getLocale().toLanguageTag(),
                        constant.getBackend().getDefaultLocale())
        ));
        result.setSummary(gameLogHourlyReportMapper.querySummaryOfReport(form));
        return RestResponse.ok(result);
    }

    public static class ReportView {
        private PageInfo<ListView> list;
        private List<SummaryView> summary;

        public PageInfo<ListView> getList() {
            return list;
        }

        public void setList(PageInfo<ListView> list) {
            this.list = list;
        }

        public List<SummaryView> getSummary() {
            return summary;
        }

        public void setSummary(List<SummaryView> summary) {
            this.summary = summary;
        }
    }
}
