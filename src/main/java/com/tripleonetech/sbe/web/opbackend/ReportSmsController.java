package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryMapper;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryQueryForm;
import com.tripleonetech.sbe.sms.history.SmsOtpHistoryView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reports/sms-otp")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportSmsController extends BaseOpbackendController {
    @Autowired
    private SmsOtpHistoryMapper smsOtpHistoryMapper;

    @GetMapping("")
    @ApiOperation("List SMS OTP history")
    public RestResponse<PageInfo<SmsOtpHistoryView>> listOtp(@Valid SmsOtpHistoryQueryForm form){
        List<SmsOtpHistoryView> view = smsOtpHistoryMapper.listOtpHistoryByCriteria(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(view));
    }

}
