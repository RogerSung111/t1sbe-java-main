package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiUtils;
import com.tripleonetech.sbe.privilege.SiteConfigs;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class ApiConfigController {

    private Collection<String> apiCodes;
    private Map<String, List<ApiMetaField>> apiMetaFields;
    @Autowired
    private SiteConfigs siteConfigs;

    protected abstract ApiTypeEnum getApiType();

    /**
     * Fills the apiCodes and apiMetaFields based on implementing controller's type
     */
    @PostConstruct
    protected void init() {
        Map<String, List<ApiMetaField>> metaFields = ApiUtils.getMetaFieldsByApiType(getApiType());
        setApiCodes(metaFields.keySet());
        setApiMetaFields(metaFields);
    }

    public List<SiteCurrencyEnum> getSiteCurrencies() {
        return siteConfigs.getSiteCurrencies();
    }

    public Collection<String> getApiCodes() {
        return apiCodes;
    }

    public void setApiCodes(Collection<String> apiCodes) {
        this.apiCodes = apiCodes;
    }

    public Map<String, List<ApiMetaField>> getApiMetaFields() {
        return apiMetaFields;
    }

    public void setApiMetaFields(Map<String, List<ApiMetaField>> apiMetaFields) {
        this.apiMetaFields = apiMetaFields;
    }

    public static class ApiConfigStatusForm {
        @ApiModelProperty(required = true,
                value = "Target API config status: DISABLED(0), ENABLED(1), INACTIVE(10), DELETED(20)",
                allowableValues = "0,1,10,20")
        @NotNull(message = "No status value in request")
        private Integer status;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
