package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.game.log.GameLogMapper;
import com.tripleonetech.sbe.game.log.GameLogReportQueryForm;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.ListView;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.SummaryView;
import com.tripleonetech.sbe.common.web.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/reports/game-logs")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportGameLogsController {
    // private final static Logger logger = LoggerFactory.getLogger(ReportGameLogsController.class);

    @Autowired
    private GameLogMapper gameLogMapper;

    @GetMapping("")
    @ApiOperation("Query game logs report. Maximum date range is limited.")
    public RestResponse<ReportView> queryReport(@Valid GameLogReportQueryForm query) {
        ReportView result = new ReportView();
        result.setList(new PageInfo<>(gameLogMapper.listReport(query, query.getPage(), query.getLimit())));
        result.setSummary(gameLogMapper.queryReportSummaryByApi(query));
        return RestResponse.ok(result);
    }

    public static class ReportView {
        private PageInfo<ListView> list;
        private List<SummaryView> summary;
        public PageInfo<ListView> getList() {
            return list;
        }
        public void setList(PageInfo<ListView> list) {
            this.list = list;
        }
        public List<SummaryView> getSummary() {
            return summary;
        }
        public void setSummary(List<SummaryView> summary) {
            this.summary = summary;
        }
    }
}
