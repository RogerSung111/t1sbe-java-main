package com.tripleonetech.sbe.web.opbackend;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.TimeRangeForm;
import com.tripleonetech.sbe.report.SummaryReportMapper;
import com.tripleonetech.sbe.report.SummaryReportView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/reports/summary")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportSummaryController extends BaseOpbackendController {
    
    @Autowired
    private SummaryReportMapper summaryReportMapper;
    
    @GetMapping("/{currency}")
    @ApiOperation("List summary information. Allowed sort columns: date")
    public RestResponse<PageInfo<SummaryReportView>> querySummaryReport(@PathVariable SiteCurrencyEnum currency, @Valid TimeRangeForm form) {
        List<SummaryReportView> summaryReportView = summaryReportMapper.query(form, currency, form.getPage(),
                form.getLimit());
        return RestResponse.ok(new PageInfo<>(summaryReportView));
    }
}
