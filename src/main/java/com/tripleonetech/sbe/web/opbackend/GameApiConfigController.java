package com.tripleonetech.sbe.web.opbackend;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.common.web.BooleanForm;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameTypeMapper;
import com.tripleonetech.sbe.game.GameTypeWithGameApi;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.config.GameApiConfigService;
import com.tripleonetech.sbe.game.config.GameApiConfigView;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.privilege.SiteConfigs;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/system/game-api")
@Profile("main")
@Api(tags = { "API Configuration - Game" })
public class GameApiConfigController extends ApiConfigController {
    private static final Logger logger = LoggerFactory.getLogger(GameApiConfigController.class);

    @Autowired
    private GameApiConfigMapper gameMapper;
    @Autowired
    private SiteConfigs siteConfigs;
    @Autowired
    private GameApiFactory gameApiFactory;
    @Autowired
    private GameApiConfigService gameApiConfigService;
    @Autowired
    private GameTypeMapper gameTypeMapper;

    @Override
    public ApiTypeEnum getApiType() {
        return ApiTypeEnum.GAME;
    }

    @GetMapping("")
    @ApiOperation("List all game API configs")
    public RestResponse<List<GameApiConfigView>> list(@RequestParam(required=false) boolean showDeletedApi) {
        LocaleAwareQuery localeQuery = new LocaleAwareQuery() {};
        List<GameApiConfigView> listView = new ArrayList<>();
        
        List<String> privilegedApi = siteConfigs.getAvailableAPI(SitePrivilegeTypeEnum.GAME_API);
        if(CollectionUtils.isEmpty(privilegedApi)) {
            return RestResponse.ok(null);
        }
        List<GameApiConfig> listConfigs = gameMapper.listGameApiConfigByCode(privilegedApi, showDeletedApi);
        List<GameTypeWithGameApi> listGameTypes = gameTypeMapper.listGameTypeWithGameApiByGameCodes(localeQuery, privilegedApi);
        listConfigs.forEach(gameApiConfig -> {
            //GameApiConfig + List<GameType>
            listView.add(
                    GameApiConfigView.create(gameApiConfig,
                            listGameTypes.stream()
                                    .filter(gameType -> gameType.getGameApiCode().equals(gameApiConfig.getCode()))
                                    .map(gameType -> IdNameView.create(gameType.getGameTypeId(), gameType.getGameTypeName()))
                                    .collect(Collectors.toList())));
        });
        return RestResponse.ok(listView);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get single game API config")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<GameApiConfig> get(@PathVariable int id) {
        List<String> privilegedApi = siteConfigs.getAvailableAPI(SitePrivilegeTypeEnum.GAME_API);
        GameApiConfig apiConfig = gameMapper.get(id);
        if (privilegedApi.contains(apiConfig.getCode())) {
            return RestResponse.ok(apiConfig);
        }
        return RestResponse.ok(null);
    }

    @PostMapping("/{id}/status")
    @ApiOperation("Update game API config status. Available status: DISABLED(0), ENABLED(1), INACTIVE(10), DELETED(20)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(@PathVariable int id, @RequestBody ApiConfigStatusForm form) {
        GameApiConfig gameApiConfig = gameMapper.getIgnoreDeleted(id);

        ApiConfigStatusEnum newApiStatus = ApiConfigStatusEnum.valueOf(form.getStatus());
        if(newApiStatus == null) {
            return RestResponse.operationFailed("Wrong API status");
        }
        if(gameApiConfig.getStatus().equals(ApiConfigStatusEnum.INACTIVE)
                || gameApiConfig.getStatus().equals(ApiConfigStatusEnum.DELETED)) {

            return RestResponse.operationFailed(String.format("Cannot switch API status to [%s], because the status of [%s] is [%s]",
                    newApiStatus, gameApiConfig.getName(), gameApiConfig.getStatus()));
        }
        gameApiConfigService.updateStatus(id, newApiStatus);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/game-log-sync/enabled")
    @ApiOperation("Enable/disable gamelog sync for the given game API config")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")
    })
    public RestResponse<RestResponseDataSuccess> enableGameLogSync(@PathVariable int id, @Valid @RequestBody BooleanForm form){
        GameApiConfig gameApiConfig = gameMapper.getIgnoreDeleted(id);
        if(gameApiConfig != null) {
            gameApiConfig.setSyncEnabled(form.isEnabled());
        }
        gameMapper.update(gameApiConfig);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/{id}/test")
    @ApiOperation("Test API Connectivity")
    @ApiImplicitParams({@ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of API config")})
    public RestResponse<RestResponseDataSuccess> testConnectivity(@PathVariable int id) {
        GameApi<? extends ApiGameLog> api = gameApiFactory.getById(id);
        return RestResponse.operationResult(api.isOnline());
    }
}
