package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.player.tag.PlayersPlayerTag;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/players/{playerId}/groups")
@Profile("main")
@Api(tags = {"Player Group & Tags"})
@Validated
public class PlayerGroupManagementController {
    private final boolean playerGroup = true;

    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;

    @Transactional
    @PostMapping("")
    @ApiOperation("Set player's group")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> setGroup(
            @PathVariable int playerId,
            @RequestBody GroupIdForm groupIdForm) {
        Integer groupId = groupIdForm.getGroupId();
        if (groupId == null) {
            return RestResponse.invalidParameter("No group ID found in request");
        }

        playersPlayerTagMapper.deleteAllWithoutPlayerGroup(playerId, !playerGroup);
        playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, Collections.singletonList(groupId));
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping(value = "/add")
    @ApiOperation("Add group for given player")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> addGroup(
            @PathVariable int playerId,
            @RequestBody GroupIdForm groupIdForm) {
        List<PlayersPlayerTag> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, playerGroup);
        if (playersPlayerTags.size() > 0) {
            return RestResponse
                    .operationFailed("Cannot add more than one group! this player's group count: " + playersPlayerTags.size());
        }

        playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, Collections.singletonList(groupIdForm.getGroupId()));
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping(value = "/{groupId}/delete")
    @ApiOperation("Delete a group for the specific player")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID"),
            @ApiImplicitParam(required = true, paramType = "path", name = "groupId", value = "The group id to delete")
    })
    public RestResponse<RestResponseDataSuccess> deleteGroup(
            @PathVariable int playerId,
            @PathVariable int groupId) {
        playersPlayerTagMapper.delete(playerId, Collections.singletonList(groupId));
        return RestResponse.operationSuccess();
    }


    private static class GroupIdForm {
        private Integer groupId;

        public Integer getGroupId() {
            return groupId;
        }

        public void setGroupId(Integer groupId) {
            this.groupId = groupId;
        }
    }
}
