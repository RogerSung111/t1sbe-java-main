package com.tripleonetech.sbe.web.opbackend;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/players/{playerCredentialId}/bank-accounts/{currency}")
@Profile("main")
@Api(tags = {"Player Management"})
public class PlayerBankAccountController {
    private static final Logger logger = LoggerFactory.getLogger(PlayerBankAccountController.class);

    @Autowired
    private PlayerBankAccountMapper playerBankAccountMapper;
    @Autowired
    private PlayerMapper playerMapper;

    @GetMapping("")
    @ApiOperation("List player bank accounts")
    public RestResponse<List<PlayerBankAccountView>> get(
            @PathVariable int playerCredentialId,
            @PathVariable SiteCurrencyEnum currency) {
        List<PlayerBankAccount> playerBankAccounts =
                playerBankAccountMapper.listByPlayerCredential(playerCredentialId, currency);
        List<PlayerBankAccountView> playerBankAccountViews =
                playerBankAccounts.stream().map(e -> PlayerBankAccountView.from(e)).collect(Collectors.toList());
        return RestResponse.ok(playerBankAccountViews);
    }

    @PostMapping("/create")
    @ApiOperation("Create new player bank account")
    public RestResponse<RestResponseIdSuccess> create(
            @PathVariable int playerCredentialId,
            @PathVariable SiteCurrencyEnum currency,
            @Valid @RequestBody PlayerBankAccountForm form) throws RestResponseException {
        Integer playerId = playerMapper.getPlayerIdByCredentialId(playerCredentialId, currency);
        if(playerId == null) {
            throw new RestResponseException(
                    RestResponse.playerNotFound(
                            String.format("Player id=[%d] currency=[%s] not found", playerCredentialId, currency))
            );
        }
        PlayerBankAccount account = PlayerBankAccount.from(form);
        account.setPlayerId(playerId);
        playerBankAccountMapper.insert(account);
        if(account.isDefaultAccount()){
            playerBankAccountMapper.setDefault(account.getId(), playerId);
        }
        return RestResponse.createSuccess(account.getId());
    }

    @PostMapping("/{accountId}")
    @ApiOperation("Update player bank account")
    public RestResponse<RestResponseDataSuccess> update(
            @PathVariable int playerCredentialId,
            @PathVariable SiteCurrencyEnum currency,
            @PathVariable int accountId,
            @RequestBody PlayerBankAccountForm form) throws RestResponseException {
        Integer playerId = playerMapper.getPlayerIdByCredentialId(playerCredentialId, currency);
        if(playerId == null) {
            throw new RestResponseException(
                    RestResponse.playerNotFound(
                            String.format("Player id=[%d] currency=[%s] not found", playerCredentialId, currency))
            );
        }
        PlayerBankAccount account = PlayerBankAccount.from(form);
        account.setPlayerId(playerId);
        account.setId(accountId);
        playerBankAccountMapper.update(account);
        if(account.isDefaultAccount()){
            playerBankAccountMapper.setDefault(accountId, playerId);
        }
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{accountId}/set-default")
    @ApiOperation("Sets one of player's bank accounts as default")
    public RestResponse<RestResponseDataSuccess> setDefault(
            @PathVariable int playerCredentialId,
            @PathVariable SiteCurrencyEnum currency,
            @PathVariable int accountId) throws RestResponseException {
        PlayerBankAccount playerBankAccount = playerBankAccountMapper.getForPlayerCredential(playerCredentialId, currency, accountId);
        if(playerBankAccount == null){
            throw new RestResponseException(
                    RestResponse.playerBankAccountNotFound(
                            String.format("Bank account [%s] not found for player credential ID [%d] under currency [%s]",
                                accountId, playerCredentialId, currency)));
        }
        playerBankAccountMapper.setDefault(accountId, playerBankAccount.getPlayerId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{accountId}/delete")
    @ApiOperation("Delete player bank account")
    public RestResponse<RestResponseDataSuccess> delete(
            @PathVariable int playerCredentialId,
            @PathVariable SiteCurrencyEnum currency,
            @PathVariable int accountId) throws RestResponseException {
        Integer playerId = playerMapper.getPlayerIdByCredentialId(playerCredentialId, currency);
        if(playerId == null) {
            throw new RestResponseException(
                    RestResponse.playerNotFound(
                            String.format("Player id=[%d] currency=[%s] not found", playerCredentialId, currency))
            );
        }
        playerBankAccountMapper.delete(accountId);
        return RestResponse.operationSuccess();
    }

}
