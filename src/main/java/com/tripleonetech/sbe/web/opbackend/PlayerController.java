package com.tripleonetech.sbe.web.opbackend;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.AlphanumericGenUtil;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.oauth.T1sbeTokenService;
import com.tripleonetech.sbe.payment.PaymentMethodDetailView;
import com.tripleonetech.sbe.payment.service.PaymentMethodService;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagService;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletAmountForm;
import com.tripleonetech.sbe.player.wallet.WalletService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/players")
@Profile("main")
@Api(tags = {"Player Management"})
@Validated
public class PlayerController extends BaseOpbackendController {
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PlayerDetailInfoMapper playerDetailInfoMapper;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private WalletService walletService;
    @Autowired
    private Constant constant;
    @Autowired
    private PlayersPlayerTagService playersPlayerTagService;
    @Autowired
    private PaymentMethodService paymentMethodService;
    @Autowired
    private T1sbeTokenService t1sbeTokenService;


    @PreAuthorize(Permissions.Player.LIST)
    @GetMapping("")
    @ApiOperation("List of players filtered by query criteria. Allowed sort columns: username only")
    public RestResponse<PageInfo<PlayerMultiCurrencyView>> list(PlayerListQueryForm query) {
        List<PlayerMultiCurrencyView> playerCredentials = playerMapper.queryCredential(query, query.getPage(), query.getLimit());
        if (playerCredentials == null || playerCredentials.size() == 0) {
            return RestResponse.ok(new PageInfo<>());
        }

        List<Integer> credentialIds = playerCredentials.stream().map(PlayerMultiCurrencyView::getId).collect(Collectors.toList());
        List<PlayerMultiCurrencyView> players = playerMapper.query(credentialIds, query);
        // copy data in players to playerCredentials to preserve page info
        IntStream.range(0, playerCredentials.size())
                .forEach(i -> BeanUtils.copyProperties(players.get(i), playerCredentials.get(i)));
        return RestResponse.ok(new PageInfo<>(playerCredentials));
    }

    @PreAuthorize(Permissions.Player.LIST)
    @GetMapping("/suggestion")
    @ApiOperation("List of players' id, currency and username, filtered by currency and username. " +
            "Note that if username is blank, no result will return. Sort columns by username only.")
    public RestResponse<PageInfo<PlayerIdView>> suggestion(PlayerSuggestionQueryForm query) {
        List<PlayerIdView> suggestedPlayers = new ArrayList<>();
        if (!StringUtils.isBlank(query.getUsernameSearch())) {
            suggestedPlayers = playerMapper.suggest(
                    query.getUsernameSearch(),
                    query.getCurrency(), query.getPage(), query.getLimit());
        }
        return RestResponse.ok(new PageInfo<>(suggestedPlayers));
    }

    @PreAuthorize(Permissions.Player.LIST)
    @GetMapping("/{playerId}/refer-players")
    @ApiOperation("Players referred by specific player")
    public RestResponse<List<RefererView>> getReferPlayers(@PathVariable Integer playerId) {
        List<PlayerCredential> referralPlayers = playerCredentialMapper.findByReferralCredentialId(playerId);
        List<RefererView> refererViewList = referralPlayers.stream()
                .map(r -> new RefererView(r.getId(), r.getUsername()))
                .collect(Collectors.toList());
        return RestResponse.ok(refererViewList);
    }

    @GetMapping(value = "/{playerId}/email")
    @ApiOperation("Player's email")
    public RestResponse<PlayerEmailForm> getEmail(@PathVariable Integer playerId) {
        PlayerCredential playerCredential = playerCredentialMapper.get(playerId);
        PlayerEmailForm result = new PlayerEmailForm();
        result.setEmail(playerCredential.getEmail());
        return RestResponse.ok(result);
    }

    @PreAuthorize(Permissions.Player.PROFILE)
    @GetMapping("/{playerId}/profile")
    @ApiOperation("Returns player profile")
    public RestResponse<PlayerMultiCurrencyProfileView> getProfile(@PathVariable Integer playerId) {
        PlayerMultiCurrencyProfileView profile = playerProfileMapper.getProfileViewByCredentialId(playerId);
        return RestResponse.ok(profile);
    }

    @PreAuthorize(Permissions.Player.IDENTITY_UPDATE)
    @Transactional
    @PostMapping("/{playerId}/identity/{currency}")
    @ApiOperation("Updates player identity under given currency")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency")
    })
    public RestResponse<RestResponseDataSuccess> updateIdentity(
            @PathVariable Integer playerId, @PathVariable SiteCurrencyEnum currency,
            @Valid @RequestBody PlayerIdentityForm form) {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (players.size() == 0) {
            return RestResponse.entityNotFound("Player not exists.");
        }

        PlayerProfile profile = new PlayerProfile();
        BeanUtils.copyProperties(form, profile);
        profile.setPlayerId(players.get(0).getId());
        playerProfileMapper.update(profile);
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.PROFILE_UPDATE)
    @Transactional
    @PostMapping("/{playerId}/profile/{currency}")
    @ApiOperation("Updates player profile under given currency")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID"),
            @ApiImplicitParam(required = true, paramType = "path", name = "currency", value = "Currency")
    })
    public RestResponse<RestResponseDataSuccess> updateProfile(
            @PathVariable Integer playerId, @PathVariable SiteCurrencyEnum currency,
            @Valid @RequestBody PlayerProfileForm form) throws RestResponseException {
        List<Player> players = playerMapper.listByCredentialId(playerId, currency);
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player not found with playerId: %s, currency: %s",
                    playerId, currency));
        }

        PlayerProfile profile = new PlayerProfile();
        BeanUtils.copyProperties(form, profile);
        Integer id = players.get(0).getId();
        profile.setPlayerId(id);
        playerProfileMapper.update(profile);

        boolean tagUpdateSuccess = playersPlayerTagService.updatePlayersPlayerTag(form.getTags(), id);
        boolean groupUpdateSuccess = playersPlayerTagService.updatePlayersPlayerGroup(form.getGroup(), id);

        if (!tagUpdateSuccess || !groupUpdateSuccess) {
            StringBuilder errorMsg = new StringBuilder();
            if (!tagUpdateSuccess) {
                errorMsg.append(String.format("Invalid tag IDs: %s; ", form.getTags().toString()));
            }
            if (!groupUpdateSuccess) {
                errorMsg.append(String.format("Invalid group ID: [%s] ", form.getGroup()));
            }
            throw new RestResponseException(RestResponse.multiStatus(errorMsg.toString().trim()));
        }

        return RestResponse.operationSuccess();
    }

    @PostMapping("/batch-adjust-balance")
    @ApiOperation("Batch adjust balances to players' main wallet ")
    public RestResponse<RestResponseDataSuccess> addBalanceToMainWallet(
            @Valid @RequestBody List<WalletAmountForm> formList) throws RestResponseException {
        List<RestResponse> failedResults = new ArrayList<>();
        List<RestResponse> successfulResults = new ArrayList<>();

        for (WalletAmountForm form : formList) {
            Integer playerId = playerMapper.getPlayerIdByUniqueKey(form.getUsername(), form.getCurrency());
            // Check if player does not exists
            if (playerId == null) {
                failedResults.add(RestResponse.playerNotFound(String.format("Failed to add balance, player[%s, %s] was not found.",
                                form.getUsername(), form.getCurrency())));
                continue;
            }

            // Check if the balance is insufficient
            if (form.getAmount().compareTo(BigDecimal.ZERO) < 0) {
                Wallet wallet = walletService.getMainWallet(playerId);
                if (wallet.getBalance().add(form.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
                    failedResults.add(RestResponse.insufficientBalance(String.format("Failed to add balance, player[%s, %s] has insufficient balance.",
                                    form.getUsername(), form.getCurrency())));
                    continue;
                }
            }

            // adjust balance to main wallet
            if(form.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                walletService.addBalanceToMainWallet(playerId, form.getAmount(), form.getNote(),
                        getLoginOperator().getUsername(), getLoginOperator().getId());
            } else {
                walletService.deductBalanceFromMainWallet(playerId, form.getAmount().negate(), form.getNote(),
                        getLoginOperator().getUsername(), getLoginOperator().getId());
            }

            successfulResults.add(RestResponse.ok(String.format("Successfully adjust balance to Player's[%s, %s] main wallet.",
                            form.getUsername(), form.getCurrency())));

        }

        if (!failedResults.isEmpty()) {
            failedResults.addAll(successfulResults);
            throw new RestResponseException(RestResponse.multiStatus(failedResults));
        }

        return RestResponse.operationSuccess();
    }

    @GetMapping("/username-available/{username}")
    @ApiOperation("Checks whether a username is available or has been taken")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "username", value = "The username to check for availability")
    })
    public RestResponse<RestResponseDataSuccess> isUsernameAvailable(@PathVariable String username) {
        if (playerService.userExists(username)) {
            return RestResponse.usernameAlreadyExist(String.format("Username [%s] already exists", username));
        }

        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.CREATE)
    @Transactional
    @PostMapping("/create")
    @ApiOperation("Creates new player with currency")
    public RestResponse<RestResponseIdSuccess> create(@Valid @RequestBody PlayerNewForm form) {
        String newPassword = AlphanumericGenUtil
                .generateAlphanumericCode(constant.getPlayer().getGeneratedPasswordLength());
        Integer credentialId = playerService.register(form, newPassword, DeviceTypeEnum.MANUAL, null);
        playerCredentialMapper.updatePasswordPending(form.getUsername(), true);

        RestResponseDataPasswordSuccess result = RestResponseDataPasswordSuccess.success(credentialId, newPassword);
        return RestResponse.ok(result);
    }

    @PreAuthorize(Permissions.Player.STATUS_UPDATE)
    @PostMapping("/{playerId}/status")
    @ApiOperation("Updates player status. Submit with currency property to update currency-specific status, submit without currency property to update global status")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> updateStatus(
            @PathVariable Integer playerId,
            @RequestBody CurrencyStatusForm form) {
        // Update global status
        PlayerCredential credential = playerCredentialMapper.get(playerId);
        if (form.getCurrency() == null) {
            playerCredentialMapper.updateStatus(credential.getUsername(), PlayerStatusEnum.valueOf(form.getStatus()));
            return RestResponse.operationSuccess();
        }

        // Update currency-specific status
        List<Player> players = playerMapper.listByCredentialId(playerId, form.getCurrency());
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player not found with credential id [%s] & currency [%s]",
                    playerId, form.getCurrency()));
        }

        playerMapper.updateStatus(players.get(0).getId(), PlayerStatusEnum.valueOf(form.getStatus()));
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.STATUS_UPDATE)
    @Transactional
    @PostMapping("/{playerId}/activate-currency")
    @ApiOperation("Activates player account under the given currency")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> enableCurrency(
            @PathVariable Integer playerId,
            @Valid @RequestBody CurrencyForm form) {
        List<Player> players = playerMapper.listByCredentialId(playerId, form.getCurrency());
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player with credential ID [%d] not found", playerId));
        }

        // Update currency-specific status
        Player player = players.get(0);
        if (PlayerStatusEnum.CURRENCY_INACTIVE == player.getStatus()) {
            playerMapper.updateStatus(player.getId(), PlayerStatusEnum.ACTIVE);
        }
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.STATUS_UPDATE)
    @PostMapping("/{playerId}/cashback-enabled")
    @ApiOperation("Updates whether player is able to receive cashback bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> updateCashbackEnabled(
            @PathVariable Integer playerId,
            @Valid @RequestBody CurrencyBooleanForm form) {
        // Update currency-specific cashback status
        List<Player> players = playerMapper.listByCredentialId(playerId, form.getCurrency());
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player not found with playerId: %s, currency: %s",
                    playerId, form.getCurrency()));
        }

        playerMapper.updateCashbackEnabled(players.get(0).getId(), form.isEnabled());
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.STATUS_UPDATE)
    @PostMapping("/{playerId}/campaign-enabled")
    @ApiOperation("Updates whether player is able to receive campaign bonus")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> updateCampaignEnabled(
            @PathVariable Integer playerId,
            @Valid @RequestBody CurrencyBooleanForm form) {
        // Update currency-specific promo status
        List<Player> players = playerMapper.listByCredentialId(playerId, form.getCurrency());
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player not found with playerId: %s, currency: %s",
                    playerId, form.getCurrency()));
        }

        playerMapper.updateCampaignEnabled(players.get(0).getId(), form.isEnabled());
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.STATUS_UPDATE)
    @PostMapping("/{playerId}/withdraw-enabled")
    @ApiOperation("Updates whether player is able to withdraw")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> updateWithdrawEnabled(
            @PathVariable Integer playerId,
            @Valid @RequestBody CurrencyBooleanForm form) {
        // Update currency-specific withdraw status
        List<Player> players = playerMapper.listByCredentialId(playerId, form.getCurrency());
        if (players.size() == 0) {
            return RestResponse.playerNotFound(String.format("Player not found with playerId: %s, currency: %s",
                    playerId, form.getCurrency()));
        }

        playerMapper.updateWithdrawEnabled(players.get(0).getId(), form.isEnabled());
        return RestResponse.operationSuccess();
    }

    @PreAuthorize(Permissions.Player.PASSWORD_UPDATE)
    @Transactional
    @PostMapping("/{playerId}/password")
    @ApiOperation("Resets player's password")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataPasswordSuccess> resetPassword(@PathVariable Integer playerId) {
        PlayerCredential credential = playerCredentialMapper.get(playerId);
        String newPassword = AlphanumericGenUtil
                .generateAlphanumericCode(constant.getPlayer().getGeneratedPasswordLength());
        credential.setPassword(newPassword);
        playerCredentialMapper.updatePasswordEncrypt(credential.getUsername(), credential.getPasswordEncrypt());
        playerCredentialMapper.updatePasswordPending(credential.getUsername(), true);

        return RestResponse.ok(RestResponseDataPasswordSuccess.success(playerId, newPassword));
    }

    @PreAuthorize(Permissions.Player.PASSWORD_UPDATE)
    @Transactional
    @PostMapping("/{playerId}/withdraw-password")
    @ApiOperation("Resets player's withdraw password")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataPasswordSuccess> resetWithdrawPassword(@PathVariable Integer playerId) {
        PlayerCredential credential = playerCredentialMapper.get(playerId);
        String newWithdrawPassword = AlphanumericGenUtil.generateAlphanumericCode(constant.getPlayer().getGeneratedPasswordLength());
        credential.setWithdrawPassword(newWithdrawPassword);

        playerCredentialMapper.updateWithdrawPasswordHash(credential.getUsername(), credential.getWithdrawPasswordHash());

        return RestResponse.ok(RestResponseDataPasswordSuccess.success(playerId, newWithdrawPassword));
    }

    @PostMapping("/{playerId}/email")
    @ApiOperation("Update player's email")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    })
    public RestResponse<RestResponseDataSuccess> updateEmail(
            @PathVariable Integer playerId, @Valid @RequestBody PlayerEmailForm form) {
        PlayerCredential playerCredential = playerCredentialMapper.get(playerId);
        playerCredential.setEmail(form.getEmail());
        playerCredentialMapper.update(playerCredential);

        return RestResponse.operationSuccess();
    }

    @GetMapping(value = "/{playerId}/account-transaction-summary")
    @ApiOperation("Query transaction summary info(summary of deposit, withdrawal & bonus)")
    @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    public RestResponse<List<PlayerAccountTransactionSummaryView>> accountTransactionSummary(@PathVariable int playerId) {
        List<Player> players = playerMapper.listByCredentialId(playerId, null);
        Map<Integer, Player> playerMap = players.stream().collect(Collectors.toMap(Player::getId, Function.identity()));

        List<PlayerAccountTransactionSummaryView> transactionSummaryViewList =
                playerDetailInfoMapper.listTransactionSummary(new ArrayList<>(playerMap.keySet()));
        transactionSummaryViewList.forEach(t -> t.setCurrency(playerMap.get(t.getPlayerId()).getCurrency()));

        return RestResponse.ok(transactionSummaryViewList);
    }

    @GetMapping(value = "/{playerId}/payment-methods/{currency}")
    @ApiOperation("Return available payment methods for player")
    @ApiImplicitParam(required = true, paramType = "path", name = "playerId", value = "Player ID")
    public RestResponse<List<PaymentMethodDetailView>> availablePaymentMethods(
            @PathVariable int playerId, @PathVariable SiteCurrencyEnum currency) {
        return RestResponse.ok(paymentMethodService.queryAvailableFor(playerId, currency));
    }

    @GetMapping(value = "/list-tokens/{username}")
    @ApiOperation("Query tokens by player username)")
    @ApiImplicitParam(required = true, paramType = "path", name = "username", value = "Player username")
    public RestResponse<Collection<OAuth2AccessToken>> listTokens(@PathVariable String username) {
        Collection<OAuth2AccessToken> tokens = t1sbeTokenService.listPlayerTokens(username);
        return RestResponse.ok(tokens);
    }

    @GetMapping(value = "/revoke-tokens/{username}")
    @ApiOperation("Revoke tokens by player username)")
    @ApiImplicitParam(required = true, paramType = "path", name = "username", value = "Player username")
    public RestResponse<RestResponseDataSuccess> revokeTokens(@PathVariable String username) {
        return RestResponse.operationResult(t1sbeTokenService.revokePlayerTokens(username, false));
    }

    @GetMapping(value = "/revoke-token/{tokenValue}")
    @ApiOperation("Revoke token by token value)")
    @ApiImplicitParam(required = true, paramType = "path", name = "tokenValue", value = "Player username")
    public RestResponse<RestResponseDataSuccess> revokeToken(@PathVariable String tokenValue) {
        return RestResponse.operationResult(t1sbeTokenService.revokeToken(tokenValue));
    }

    protected static class CurrencyForm {
        @NotNull
        @ApiModelProperty(required = true, name = "Currency", value = "The player's currency")
        private SiteCurrencyEnum currency;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }
    }

    protected static class CurrencyStatusForm {
        @NotNull
        @ApiModelProperty(name = "status", value = "Target status")
        private Integer status;
        @NotNull
        @ApiModelProperty(required = false, name = "Currency", value = "The player's currency")
        private SiteCurrencyEnum currency;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }

    protected static class CurrencyBooleanForm {
        @ApiModelProperty(required = true, name = "Enabled", value = "Target enabled")
        private boolean enabled;
        @ApiModelProperty(required = true, name = "Currency", value = "The player's currency")
        private SiteCurrencyEnum currency;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }

    protected static class PlayerSuggestionQueryForm extends PageQueryForm {
        @ApiModelProperty(required = true, value = "Currency")
        private SiteCurrencyEnum currency;
        @ApiModelProperty(required = true, value = "Player username search text")
        private String usernameSearch;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public String getUsernameSearch() {
            return usernameSearch;
        }

        public void setUsernameSearch(String usernameSearch) {
            this.usernameSearch = usernameSearch;
        }
    }
}
