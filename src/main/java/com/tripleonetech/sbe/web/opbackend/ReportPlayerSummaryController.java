package com.tripleonetech.sbe.web.opbackend;

import java.util.List;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.TimeRangeForm;
import com.tripleonetech.sbe.player.report.PlayerSummaryReportForm;
import com.tripleonetech.sbe.player.report.PlayerSummaryReportMapper;
import com.tripleonetech.sbe.player.report.PlayerSummaryReportView;
import com.tripleonetech.sbe.report.player.PlayerActivityReportMapper;
import com.tripleonetech.sbe.report.player.PlayerOnlineReportMapper;
import com.tripleonetech.sbe.report.player.PlayerOnlineReportView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/reports")
@Profile("main")
@Api(tags = { "Reports" })
public class ReportPlayerSummaryController extends BaseOpbackendController {
    @Autowired
    private PlayerSummaryReportMapper playerSummaryReportMapper;
    @Autowired
    private PlayerOnlineReportMapper playerOnlineReportMapper;
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;

    @GetMapping("/player-summary")
    @ApiOperation("List summary of player count. Allowed sort columns: date (default), device")
    public RestResponse<PageInfo<PlayerSummaryReportView>> queryPlayerSummaryReport(
            @Valid PlayerSummaryReportForm form) {
        // TODO: 或許 PlayerSummaryReport 應該改名叫 PlayerRegisterReport?
        List<PlayerSummaryReportView> playerSummaryReportView = playerSummaryReportMapper.list(form, form.getPage(), form.getLimit());
        return RestResponse.ok(new PageInfo<>(playerSummaryReportView));
    }

    @GetMapping("/player-online")
    @ApiOperation("List statistic of player online. Allowed sort column: date (default)")
    public RestResponse<PageInfo<PlayerOnlineReportView>> queryPlayerOnlineReport(
            @Valid TimeRangeForm form) {
        List<PlayerOnlineReportView> list = playerOnlineReportMapper.list(form, form.getPage(), form.getLimit());
        List<Integer> counts = playerActivityReportMapper.queryPlayerCounts(form, form.getPage(), form.getLimit());
        IntStream.range(0, list.size()).forEach(i -> list.get(i).setTotal(counts.get(i)));
        return RestResponse.ok(new PageInfo<>(list));
    }
}
