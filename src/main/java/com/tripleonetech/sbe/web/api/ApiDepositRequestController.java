package com.tripleonetech.sbe.web.api;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositRequestQueryForm;
import com.tripleonetech.sbe.payment.DepositRequestView;
import com.tripleonetech.sbe.player.Player;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

@Profile("api")
@RestController
@Api(tags = { "Cashier" })
public class ApiDepositRequestController extends BaseApiController {
    @Autowired
    private DepositRequestMapper depositRequestMapper;

    @GetMapping("/payment-requests")
    @ApiOperation("Query current player's deposit requests. Allowed sort columns: amount, expirationDate, requestedDate (default)")
    public RestResponse<PageInfo<DepositRequestView>> list(ApiDepositRequestQueryForm form) {
        Player player = getLoginPlayer();
        DepositRequestQueryForm query = new DepositRequestQueryForm();
        BeanUtils.copyProperties(form, query);
        query.setUsername(player.getUsername());
        query.setCurrency(player.getCurrency());
        query.setRequestedDateEndLocal(form.getRequestedDateEnd());
        query.setRequestedDateStartLocal(form.getRequestedDateStart());
        List<DepositRequestView> depositRequestView = depositRequestMapper.list(query, query.getPage(),
                query.getLimit());
        return RestResponse.ok(new PageInfo<>(depositRequestView));
    }
    
    @GetMapping("/payment-requests/pending-amount")
    @ApiOperation("Get total amount of unapproved deposit requests")
    public RestResponse<BigDecimal> getPendingDepositAmount(){
        Player player = getLoginPlayer();
        return RestResponse.ok(depositRequestMapper.getPendingRequestAmount(player.getId()));
    }
    
    public static class ApiDepositRequestQueryForm extends PageQueryForm {
        @ApiModelProperty(value = "Begin time of request time search", required = false)
        private LocalDateTime requestedDateStart;
        @ApiModelProperty(value = "End time of request time search", required = false)
        private LocalDateTime requestedDateEnd;
        @ApiModelProperty(value = "Deposit request status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)", allowableValues = "0,1,10,11")
        private Integer status;

        public LocalDateTime getRequestedDateStart() {
            return requestedDateStart;
        }

        public void setRequestedDateStart(ZonedDateTime requestedDateStart) {
            this.requestedDateStart = DateUtils.zonedToLocal(requestedDateStart);
        }

        public LocalDateTime getRequestedDateEnd() {
            return requestedDateEnd;
        }

        public void setRequestedDateEnd(ZonedDateTime requestedDateEnd) {
            this.requestedDateEnd = DateUtils.zonedToLocal(requestedDateEnd);
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
