package com.tripleonetech.sbe.web.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tripleonetech.sbe.cms.ContentStoreService;
import com.tripleonetech.sbe.cms.ContentStoreView;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cms/content-store")
@Profile("api")
@Api(tags = { "Cms" })
public class ApiContentStoreController {
    @Autowired
    private ContentStoreService storeService;

    @GetMapping("/all")
    @ApiOperation("List all store entrys in key-value form")
    public RestResponse<Map<String, ContentStoreView>> getAll() {
        Map<String, ContentStoreView> stores = storeService.getAll();
        return RestResponse.ok(stores);
    }

    @GetMapping("/{key}")
    @ApiOperation("Get single store entry by key")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "key", value = "The key of the store entry")
    })
    public RestResponse<ContentStoreView> getJob(@PathVariable String key) throws RestResponseException {

        ContentStoreView storeEntry = storeService.get(key);
        if (storeEntry == null) {
            throw new RestResponseException(RestResponse.entityNotFound(String.format("Cms key: [%s] not found", key)));
        }
        return RestResponse.ok(storeEntry);
    }
    
}
