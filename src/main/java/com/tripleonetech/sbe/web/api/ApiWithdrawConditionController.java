package com.tripleonetech.sbe.web.api;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionMapper;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionService;
import com.tripleonetech.sbe.withdraw.condition.WithdrawConditionView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@Profile("api")
@RequestMapping(value = "/withdraw-conditions")
@Api(tags = { "Cashier" })
public class ApiWithdrawConditionController extends BaseApiController {
    @Autowired
    private WithdrawConditionMapper withdrawConditionMapper;
    @Autowired
    private WithdrawConditionService withdrawConditionService;

    @GetMapping("")
    @ApiOperation("List all withdraw conditions for current player")
    public RestResponse<List<WithdrawConditionView>> list() {
        return RestResponse.ok(WithdrawConditionView.createList(
                withdrawConditionMapper.getUnsettledConditionsForPlayer(getLoginPlayer().getId())));
    }

    @GetMapping("/balance")
    @ApiOperation("Returns withdrawable wallet balance and withdraw condition locked-on balance")
    public RestResponse<WithdrawConditionBalanceView> getBalance() {
        BigDecimal availableBalance = withdrawConditionService.getValidBalance(getLoginPlayer().getId());
        BigDecimal lockedOnBalance = withdrawConditionMapper.getLockedOnAmountSum(getLoginPlayer().getId());

        WithdrawConditionBalanceView view = new WithdrawConditionBalanceView();
        view.setLockedOn(lockedOnBalance);
        view.setAvailable(availableBalance);
        return RestResponse.ok(view);
    }

    public static class WithdrawConditionBalanceView {
        private BigDecimal available;
        private BigDecimal lockedOn;

        public BigDecimal getAvailable() {
            return available;
        }

        public void setAvailable(BigDecimal available) {
            this.available = available;
        }

        public BigDecimal getLockedOn() {
            return lockedOn;
        }

        public void setLockedOn(BigDecimal lockedOn) {
            this.lockedOn = lockedOn;
        }
    }
}
