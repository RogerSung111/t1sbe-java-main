package com.tripleonetech.sbe.web.api;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;
import com.tripleonetech.sbe.withdraw.WithdrawRequestForm;
import com.tripleonetech.sbe.withdraw.WithdrawRequestService;
import com.tripleonetech.sbe.withdraw.WithdrawStatusEnum;
import com.tripleonetech.sbe.withdraw.history.WithdrawRequestProcessHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Profile("api")
@Api(tags = { "Cashier" })
@RequestMapping(value = "/withdraw-requests")
public class ApiWithdrawalController extends BaseApiController {

    @Autowired
    private WithdrawRequestService service;
    @Autowired
    private WithdrawRequestProcessHistoryService withdrawRequestProcessHistoryService;

    @Transactional
    @PostMapping("/create")
    @ApiOperation("Submit a withdraw request")
    public RestResponse<RestResponseIdSuccess> createWithdrawRequest(
            @Valid @RequestBody WithdrawRequestForm form) throws RestResponseException {

        Player player = getLoginPlayerRefresh();
        if(null == player.getWithdrawPasswordHash()) {
            throw new RestResponseException(RestResponse.operationFailed("Please setup withdraw password"));
        }
        if(!player.validateWithdrawPassword(form.getWithdrawPassword())) {
            throw new RestResponseException(RestResponse.invalidPassword("Withdraw-Password verification is failed"));
        }
        WithdrawRequest withdrawRequest = WithdrawRequest.from(form);
        withdrawRequest.setPlayerId(player.getId());

        if (!service.newWithdrawal(withdrawRequest)) {
            throw new RestResponseException(RestResponse.operationFailed("freeze amount"));
        }
        String comment = "Created withdraw request by player : " + player.getUsername();
        withdrawRequestProcessHistoryService.addWorkflowProcessHistory(WithdrawStatusEnum.CREATED,
                WithdrawStatusEnum.CREATED, comment, null, withdrawRequest.getId());

        return RestResponse.createSuccess(withdrawRequest.getId());
    }
}
