package com.tripleonetech.sbe.web.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.validator.PropertiesPattern;
import com.tripleonetech.sbe.common.web.*;
import com.tripleonetech.sbe.email.EmailService;
import com.tripleonetech.sbe.email.EmailVerificationResultEnum;
import com.tripleonetech.sbe.email.EmailVerificationSuccessEvent;
import com.tripleonetech.sbe.oauth.T1sbeTokenService;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.player.message.PlayerMessageMapper;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import com.tripleonetech.sbe.report.player.PlayerActivityReport;
import com.tripleonetech.sbe.report.player.PlayerActivityReportService;
import com.tripleonetech.sbe.sms.history.SmsOtpValidationResultEnum;
import com.tripleonetech.sbe.sms.history.SmsOtpValidationSuccessEvent;
import com.tripleonetech.sbe.sms.service.SmsService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Validated
@RestController
@Profile("api")
@Api(tags = {"Player"})
public class ApiPlayerController extends BaseApiController {
    private final static Logger logger = LoggerFactory.getLogger(ApiPlayerController.class);

    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private PlayerBankAccountMapper playerBankAccountMapper;
    @Autowired
    private PlayerStatsMapper playerStatsMapper;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @Autowired
    private PlayerService playerService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    @Autowired
    private PlayerActivityReportService playerActivityReportService;
    @Autowired
    private PlayerVerificationQuestionI18nMapper playerVerificationQuestionI18nMapper;
    @Autowired
    private Constant constant;
    @Autowired
    private T1sbeTokenService t1sbeTokenService;

    @GetMapping("/player/info")
    @ApiOperation("Player's brief information, used to display in the header of player center")
    public RestResponse<PlayerInfoView> getPlayerInfo() {
        Player player = getLoginPlayer();
        PlayerInfoView playerInfo = playerProfileMapper.getPlayerInfo(player.getId());
        playerInfo.setUnreadMessageCount(playerMessageMapper.getUnreadCount(player.getId()));
        PlayerActivityReport secondToLastLogin = playerActivityReportService.getSecondToLastLogin(player.getId());
        if (secondToLastLogin != null) {
            playerInfo.setActivity(secondToLastLogin);
        }
        return RestResponse.ok(playerInfo);
    }

    @PostMapping("/player/switch-currency")
    @ApiOperation("switch other currency")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", name = "currency", value = "Currency")
    })
    public RestResponse<PlayerInfoView> switchCurrency(@NotNull @RequestParam String currency) throws RestResponseException {
        Player player = getLoginPlayer();
        SitePrivilege sitePrivilege = sitePrivilegeMapper.getSitePrivilege(SitePrivilegeTypeEnum.SITE_CURRENCY, currency);
        if (sitePrivilege == null || !sitePrivilege.isActive()) {
            throw new RestResponseException(
                    RestResponse.currencyNotAvailable(
                            String.format("Player id=[%d] currency=[%s] not found", player.getCredentialId(), currency))
            );
        }
        Player switchPlayer = playerMapper.getByUniqueKey(player.getUsername(), SiteCurrencyEnum.valueOf(currency));
        if (switchPlayer == null) {
            throw new RestResponseException(
                    RestResponse.playerNotFound(
                            String.format("Player id=[%d] currency=[%s] not found", player.getCredentialId(), currency))
            );
        }

        if (PlayerStatusEnum.BLOCKED.equals(switchPlayer.getStatus())) {
            throw new RestResponseException(
                    RestResponse.playerBlocked(
                            String.format("Player id=[%d] currency=[%s] has been blocked", player.getCredentialId(), currency))
            );
        }

        if (PlayerStatusEnum.CURRENCY_INACTIVE.equals(switchPlayer.getStatus())) {
            playerMapper.updateStatus(switchPlayer.getId(), PlayerStatusEnum.ACTIVE);
        }

        t1sbeTokenService.refreshUserDetail(SiteCurrencyEnum.valueOf(currency));
        return getPlayerInfo();
    }

    @GetMapping("/player/profile")
    @ApiOperation("Player's profile info, used in player profile page")
    public RestResponse<ApiPlayerProfileView> viewProfile() {
        Player player = getLoginPlayer();
        ApiPlayerProfileView playerProfile = playerProfileMapper.getApiProfileViewByPlayerId(player.getId());
        return RestResponse.ok(playerProfile);
    }

    @GetMapping("/player/refer-players")
    @ApiOperation("Players referred by current player")
    public RestResponse<List<RefererView>> viewReferPlayers() {
        Player player = getLoginPlayer();
        List<PlayerCredential> referralPlayers = playerCredentialMapper.findByReferralCredentialId(player.getCredentialId());
        List<RefererView> refererViewList = referralPlayers.stream()
                .map(r -> new RefererView(r.getId(), r.getUsername()))
                .collect(Collectors.toList());
        return RestResponse.ok(refererViewList);
    }

    @PostMapping("/player/profile")
    @ApiOperation("Edit player's profile info")
    public RestResponse<RestResponseDataSuccess> editProfileSubmit(
            @Valid @RequestBody ApiPlayerProfileForm playerProfileForm) {
        Player player = getLoginPlayer();
        PlayerProfile playerProfile = PlayerProfile.from(playerProfileForm);
        playerProfile.setPlayerId(player.getId());
        playerProfileMapper.update(playerProfile);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/player/identity")
    @ApiOperation("Player's identity info, used in player identity page")
    public RestResponse<PlayerMultiCurrency.IdentityView> viewIdentity() {
        Player player = getLoginPlayer();
        PlayerMultiCurrency.IdentityView playerIdentity = playerProfileMapper.getIdentityViewByPlayerId(player.getId());
        return RestResponse.ok(playerIdentity);
    }

    @PostMapping("/player/identity")
    @ApiOperation("Edit player's identity")
    public RestResponse<RestResponseDataSuccess> editIdentitySubmit(
            @Valid @RequestBody PlayerIdentityForm playerIdentityForm) {
        Player player = getLoginPlayer();
        PlayerProfile playerProfile = PlayerProfile.from(playerIdentityForm);
        playerProfile.setPlayerId(player.getId());
        playerProfileMapper.update(playerProfile);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/player/email")
    @ApiOperation("Player's email")
    public RestResponse<PlayerEmailForm> viewEmail() {
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(getLoginPlayer().getUsername());
        PlayerEmailForm result = new PlayerEmailForm();
        result.setEmail(playerCredential.getEmail());
        return RestResponse.ok(result);
    }

    @PostMapping("/player/email")
    @ApiOperation("Update player's email")
    public RestResponse<RestResponseDataSuccess> updateEmail(@Valid @RequestBody PlayerEmailForm form) {
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(getLoginPlayer().getUsername());
        playerCredential.setEmail(form.getEmail());
        playerCredentialMapper.update(playerCredential);
        return RestResponse.operationSuccess();
    }

    @GetMapping("/player/bank-accounts")
    @ApiOperation("Player's list of bank accounts")
    public RestResponse<List<PlayerBankAccountView>> getPlayerBankAccounts() {
        Player player = getLoginPlayer();
        List<PlayerBankAccount> playerBankAccounts = playerBankAccountMapper.listByPlayerId(player.getId());
        List<PlayerBankAccountView> playerBankAccountViews = new ArrayList<>();
        for (PlayerBankAccount playerBankAccount : playerBankAccounts) {
            PlayerBankAccountView view = new PlayerBankAccountView();
            BeanUtils.copyProperties(playerBankAccount, view);
            playerBankAccountViews.add(view);
        }
        return RestResponse.ok(playerBankAccountViews);
    }

    @PostMapping("/player/bank-accounts/{id}")
    @ApiOperation("Edit bank account")
    public RestResponse<RestResponseDataSuccess> editPlayerBankAccount(
            @PathVariable Integer id,
            @Valid @RequestBody PlayerBankAccountForm playerBankAccountForm) {
        Player player = getLoginPlayer();
        PlayerBankAccount playerBankAccount = PlayerBankAccount.from(playerBankAccountForm);
        playerBankAccount.setPlayerId(player.getId());
        playerBankAccount.setId(id);
        playerBankAccountMapper.update(playerBankAccount);
        if (playerBankAccount.isDefaultAccount()) {
            playerBankAccountMapper.setDefault(id, player.getId());
        }
        return RestResponse.operationSuccess();
    }

    @PostMapping("/player/bank-accounts/create")
    @ApiOperation("Create bank account")
    public RestResponse<RestResponseIdSuccess> createPlayerBankAccount(
            @Valid @RequestBody PlayerBankAccountForm playerBankAccountForm) {
        Player player = getLoginPlayer();
        PlayerBankAccount playerBankAccount = PlayerBankAccount.from(playerBankAccountForm);
        playerBankAccount.setPlayerId(player.getId());
        playerBankAccountMapper.insert(playerBankAccount);
        if (playerBankAccount.isDefaultAccount()) {
            playerBankAccountMapper.setDefault(playerBankAccount.getId(), player.getId());
        }
        return RestResponse.ok(RestResponseIdSuccess.success(playerBankAccount.getId()));
    }

    @PostMapping("/player/bank-accounts/{id}/set-default")
    @ApiOperation("Sets player's default bank account, also unsets the old default")
    public RestResponse<RestResponseDataSuccess> setDefaultPlayerBankAccount(
            @PathVariable Integer id) throws RestResponseException {
        Player player = getLoginPlayer();
        PlayerBankAccount playerBankAccount = playerBankAccountMapper.get(id);
        if (playerBankAccount == null || !player.getId().equals(playerBankAccount.getPlayerId())) {
            throw new RestResponseException(
                    RestResponse.playerBankAccountNotFound(
                            String.format("Bank account [%d] not found under player [%d][%s][%s]", id, player.getId(), player.getUsername(), player.getCurrency())
                    ));
        }
        playerBankAccountMapper.setDefault(id, player.getId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/player/bank-accounts/{id}/delete")
    @ApiOperation("Delete bank account")
    public RestResponse<RestResponseDataSuccess> deletePlayerBankAccount(@PathVariable Integer id) throws RestResponseException {
        Player player = getLoginPlayer();
        PlayerBankAccount playerBankAccount = playerBankAccountMapper.get(id);
        if (playerBankAccount == null || !player.getId().equals(playerBankAccount.getPlayerId())) {
            throw new RestResponseException(
                    RestResponse.playerBankAccountNotFound(
                            String.format("Bank account [%d] not found under player [%d][%s][%s]", id, player.getId(), player.getUsername(), player.getCurrency())
                    ));
        }
        playerBankAccountMapper.delete(id);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/player/password")
    @ApiOperation("Player change password.")
    public RestResponse<RestResponseDataSuccess> updatePassword(@Valid @RequestBody ChangePasswordForm form) {
        Player player = playerMapper.get(getLoginPlayer().getId());
        if (!player.validatePassword(form.getOriginalPassword())) {
            return RestResponse.invalidPassword("The original password entered was invalid");
        }

        // update
        playerService.updatePasswordByUsername(player.getUsername(), form.getNewPassword());
        t1sbeTokenService.revokePlayerTokens(player.getUsername(), false);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/player/withdraw-password")
    @ApiOperation("Change withdraw password.")
    public RestResponse<RestResponseDataSuccess> updateWithdrawPassword(@Valid @RequestBody ChangeWithdrawPasswordForm form) {
        Player player = getLoginPlayer();

        // verify login password
        if (!player.validatePassword(form.getLoginPassword())) {
            return RestResponse.invalidPassword("Login password verification is failed.");
        }
        player.setWithdrawPassword(form.getNewWithdrawPassword());
        playerCredentialMapper.updateWithdrawPasswordHash(player.getUsername(), player.getWithdrawPasswordHash());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/player/phone-verification/send")
    @ApiOperation("Send OTP to player's phone")
    public RestResponse<RestResponseDataSuccess> sendOtp(@Valid @RequestBody PlayerPhoneForm form) throws RestResponseException {
        Player player = getLoginPlayer();
        String otpCode = smsService.sendOtpSms(form.getPhoneNumber(), player.getId());
        return RestResponse.operationResult(StringUtils.isNotBlank(otpCode));
    }

    @PostMapping("/player/phone-verification")
    @ApiOperation("Validate player's phone number")
    public RestResponse<?> verifyAndSavePhone(@Valid @RequestBody ValidatePhoneForm form) {
        SmsOtpValidationResultEnum result = smsService.validateOtpCode(getLoginPlayer().getId(), form.getOtpCode(), form.getPhoneNumber());
        if (result.equals(SmsOtpValidationResultEnum.PASS)) {
            Player player = getLoginPlayer();
            PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(player.getId());
            playerProfile.setPhoneNumber(form.getPhoneNumber());
            playerProfile.setPhoneVerified(true);
            playerProfileMapper.update(playerProfile);

            eventPublisher.publishEvent(new SmsOtpValidationSuccessEvent(this, player.getId(), form.getOtpCode()));
            return RestResponse.operationSuccess();
        }
        return RestResponse.operationFailed(result.getMessage());
    }

    @PostMapping("/player/email-verification/send")
    @ApiOperation("Send OTP to player's email")
    public RestResponse<RestResponseDataSuccess> sendVerificationEmail(@Valid @RequestBody PlayerEmailForm form) {
        Player player = getLoginPlayer();
        String optCode = emailService.sendVerificationEmail(form.getEmail(), player.getId());
        return RestResponse.operationResult(StringUtils.isNotBlank(optCode));
    }

    @PostMapping(value = "/player/email-verification")
    @ApiOperation("Verify player's email (NOTE: this interface is for player to register and verify email address)")
    public RestResponse<? extends Object> verifyAndSaveEmail(@Valid @RequestBody VerifyEmailForm form) {
        EmailVerificationResultEnum result = emailService.validateEmailToken(getLoginPlayer().getId(), form.getOtpCode(), form.getEmail());
        if (result.equals(EmailVerificationResultEnum.PASS)) {
            Player player = getLoginPlayer();
            PlayerCredential playerCredential = playerCredentialMapper.getByPlayerId(player.getId());
            playerCredential.setEmail(form.getEmail());
            playerCredential.setEmailVerified(true);
            playerCredentialMapper.update(playerCredential);
            try {
                eventPublisher.publishEvent(new EmailVerificationSuccessEvent(this, player.getId(), form.getOtpCode()));
            } catch (Exception ex) {
                logger.error("Publishing EmailVerificationSuccessEvent Exception: ", ex);
            }
            return RestResponse.operationSuccess();
        }
        return RestResponse.operationFailed(result.getMessage());
    }

    @GetMapping("/player/stats")
    @ApiOperation("Player statistics, historical totals")
    public RestResponse<PlayerStatsView> getPlayerStats() {
        PlayerStats playerStats = playerStatsMapper.getByPlayerId(getLoginPlayer().getId());
        PlayerStatsView view = new PlayerStatsView(playerStats);
        return RestResponse.ok(view);
    }

    @GetMapping(value = "/verification-questions")
    @ApiOperation("List all of security verification questions")
    public RestResponse<List<PlayerVerificationQuestionI18n>> listVerificationQuestions() {
        return RestResponse.ok(playerVerificationQuestionI18nMapper.list(
                LocaleContextHolder.getLocale().toLanguageTag(),
                constant.getFrontend().getDefaultLocale()));
    }

    @PostMapping(value = "/player/verification-question")
    @ApiOperation("Update security verification question")
    public RestResponse<RestResponseDataSuccess> updateVerificationQuestion(
            @Valid @RequestBody PlayerVerificationQuestionForm form) {
        playerCredentialMapper.updateVerificationInfo(this.getLoginPlayer().getUsername(), form);
        return RestResponse.operationSuccess();
    }

    @GetMapping(value = "/player/verification-question")
    @ApiOperation("Get security verification question. This API can be invoked without login.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", name = "username", value = "Username")
    })
    public RestResponse<PlayerVerificationQuestionI18n> getVerificationQuestion(@NotBlank @RequestParam String username) {
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(username);
        if (playerCredential == null) {
            return RestResponse.ok(new PlayerVerificationQuestionI18n());
        }

        return RestResponse.ok(playerVerificationQuestionI18nMapper.get(
                playerCredential.getVerificationQuestionId(),
                LocaleContextHolder.getLocale().toLanguageTag(),
                constant.getFrontend().getDefaultLocale()));
    }

    @PostMapping(value = "/player/forget-password")
    @ApiOperation("Verify the answer to the security verification question, and if they are equal, change the password to a new password.")
    public RestResponse<RestResponseDataSuccess> forgetPassword(@Valid @RequestBody ForgetPasswordForm form) {
        PlayerCredential playerCredential = playerCredentialMapper.getByUsername(form.getUsername());
        if (playerCredential == null) {
            return RestResponse.entityNotFound("Player not exists.");
        }

        if (StringUtils.isBlank(playerCredential.getVerificationAnswer())
                || !form.getVerificationAnswer().equals(playerCredential.getVerificationAnswer())) {
            return RestResponse.operationFailed("The answer is incorrect.");
        }

        playerService.updatePasswordByUsername(playerCredential.getUsername(), form.getNewPassword());
        return RestResponse.operationSuccess();
    }

    @GetMapping(value = "/player/contact-preferences")
    @ApiOperation("Get contact preferences. EMAIL(1), SMS(2), PHONE_CALL(4), LETTER(8)")
    public RestResponse<ContactPreferenceForm> getContactPreferences() {
        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(getLoginPlayer().getId());
        ContactPreferenceForm view = new ContactPreferenceForm();
        view.setContactPreferences(playerProfile.getContactPreferences());
        return RestResponse.ok(view);
    }

    @Transactional
    @PostMapping(value = "/player/contact-preferences")
    @ApiOperation("Update contact preferences. EMAIL(1), SMS(2), PHONE_CALL(4), LETTER(8)")
    public RestResponse<RestResponseDataSuccess> updateContactPreferences(@RequestBody ContactPreferenceForm form) {
        PlayerProfile playerProfile = playerProfileMapper.getByPlayerId(getLoginPlayer().getId());
        playerProfile.setContactPreferences(form.getContactPreferences());
        playerProfileMapper.update(playerProfile);
        return RestResponse.operationSuccess();
    }

    @GetMapping(value = "/player/clean-token")
    @ApiOperation("Revoke other tokens")
    public RestResponse<RestResponseDataSuccess> revokeOtherTokens() {
        Player player = getLoginPlayer();
        boolean success = t1sbeTokenService.revokePlayerTokens(player.getUsername(), true);
        if (success) {
            return RestResponse.operationSuccess();
        } else {
            return RestResponse.operationFailed("Clean token failed");
        }
    }

    protected static class ContactPreferenceForm {
        private List<Integer> contactPreferenceList;

        public List<Integer> getContactPreferenceList() {
            return contactPreferenceList;
        }

        public void setContactPreferenceList(List<Integer> contactPreferenceList) {
            this.contactPreferenceList = contactPreferenceList;
        }

        @JsonIgnore
        public Integer getContactPreferences() {
            if (null == contactPreferenceList) {
                return 0;
            }
            return contactPreferenceList.stream().reduce(0, (a, b) -> a | b);
        }

        public void setContactPreferences(Integer contactPreferences) {
            if (null == contactPreferences) {
                return;
            }

            this.contactPreferenceList = Stream.of(ContactMethodEnum.values())
                    .filter(e -> (e.getCode() & contactPreferences) > 0)
                    .map(e -> e.getCode())
                    .collect(Collectors.toList());
        }
    }

    public static class ChangeWithdrawPasswordForm {
        @NotEmpty
        @ApiModelProperty(required = true, name = "loginPassword", value = "login password")
        private String loginPassword;
        @NotEmpty
        @ApiModelProperty(required = true, name = "newWithdrawPassword", value = "new withdraw password")
        private String newWithdrawPassword;

        public String getLoginPassword() {
            return loginPassword;
        }

        public void setLoginPassword(String loginPassword) {
            this.loginPassword = loginPassword;
        }

        public String getNewWithdrawPassword() {
            return newWithdrawPassword;
        }

        public void setNewWithdrawPassword(String newWithdrawPassword) {
            this.newWithdrawPassword = newWithdrawPassword;
        }
    }

    public static class ForgetPasswordForm {
        @NotBlank
        @ApiModelProperty(required = true, value = "The username of player")
        private String username;
        @NotBlank
        @Size(min = 3, max = 50)
        @ApiModelProperty(required = true, value = "The answer of verification question")
        private String verificationAnswer;
        @NotBlank
        @PropertiesPattern(property = "validation.password-regex", message = "{validation.password.invalid}")
        @ApiModelProperty(required = true, value = "New password to be changed")
        private String newPassword;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getVerificationAnswer() {
            return verificationAnswer;
        }

        public void setVerificationAnswer(String verificationAnswer) {
            this.verificationAnswer = verificationAnswer;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }
    }

    public static class ValidatePhoneForm extends PlayerPhoneForm {
        @NotEmpty
        @ApiModelProperty(required = true, name = "otpCode", value = "one time password code")
        private String otpCode;

        public String getOtpCode() {
            return otpCode;
        }

        public void setOtpCode(String otpCode) {
            this.otpCode = otpCode;
        }
    }

    public static class VerifyEmailForm extends PlayerEmailForm {
        @NotBlank
        @ApiModelProperty(required = true, name = "otpCode", value = "one time password code")
        private String otpCode;

        public String getOtpCode() {
            return otpCode;
        }

        public void setOtpCode(String otpCode) {
            this.otpCode = otpCode;
        }
    }

    protected static class PlayerStatsView {
        private PlayerStats playerStats;

        public PlayerStatsView(PlayerStats playerStats) {
            this.playerStats = Optional.ofNullable(playerStats).orElse(new PlayerStats());
        }

        public Integer getTotalDepositCount() {
            return Optional.ofNullable(playerStats.getTotalDepositCount()).orElse(0);
        }

        public BigDecimal getTotalDepositAmount() {
            return Optional.ofNullable(playerStats.getTotalDepositAmount()).orElse(BigDecimal.ZERO);
        }

        public ZonedDateTime getFirstDepositDate() {
            return DateUtils.localToZoned(playerStats.getFirstDepositDate());
        }

        public ZonedDateTime getLastDepositDate() {
            return DateUtils.localToZoned(playerStats.getLastDepositDate());
        }

        public Integer getTotalWithdrawCount() {
            return Optional.ofNullable(playerStats.getTotalWithdrawCount()).orElse(0);
        }

        public BigDecimal getTotalWithdrawAmount() {
            return Optional.ofNullable(playerStats.getTotalWithdrawAmount()).orElse(BigDecimal.ZERO);
        }

        public ZonedDateTime getFirstWithdrawDate() {
            return DateUtils.localToZoned(playerStats.getFirstWithdrawDate());
        }

        public ZonedDateTime getLastWithdrawDate() {
            return DateUtils.localToZoned(playerStats.getLastWithdrawDate());
        }

        public BigDecimal getTotalCashbackBonus() {
            return Optional.ofNullable(playerStats.getTotalCashbackBonus()).orElse(BigDecimal.ZERO);
        }

        public BigDecimal getTotalReferralBonus() {
            return Optional.ofNullable(playerStats.getTotalReferralBonus()).orElse(BigDecimal.ZERO);
        }

        public BigDecimal getTotalCampaignBonus() {
            return Optional.ofNullable(playerStats.getTotalCampaignBonus()).orElse(BigDecimal.ZERO);
        }

    }
}
