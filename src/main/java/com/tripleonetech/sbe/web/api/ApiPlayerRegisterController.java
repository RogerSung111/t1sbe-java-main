package com.tripleonetech.sbe.web.api;

import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.player.PlayerNewForm.PlayerRegisterForm;
import com.tripleonetech.sbe.player.PlayerService;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Profile("api")
@Api(tags = { "Player" })
public class ApiPlayerRegisterController extends BaseApiController {
    private static final Logger logger = LoggerFactory.getLogger(ApiPlayerRegisterController.class);

    @Autowired
    private PlayerService playerService;

    @PostMapping("/player/register")
    @ApiOperation("Player registration.")
    public RestResponse<RestResponseIdSuccess> register(@Valid @RequestBody PlayerRegisterForm formObj) throws RestResponseException {
        // if username exists
        if (playerService.userExists(formObj.getUsername())) {
            logger.warn("The username[{}] already exists.", formObj.getUsername());
            throw new RestResponseException(RestResponse.usernameAlreadyExist(String.format("Username [%s] already exists", formObj.getUsername())));
        }

        Integer credentialId = playerService.register(formObj, formObj.getPassword(),
                ClientHttpInfoUtils.getDeviceType(), ClientHttpInfoUtils.getIp());
        return RestResponse.createSuccess(credentialId);
    }

}
