package com.tripleonetech.sbe.web.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.campaign.*;
import com.tripleonetech.sbe.promo.campaign.deposit.PromoCampaignDepositMapper;
import com.tripleonetech.sbe.promo.campaign.login.PromoCampaignLoginMapper;
import com.tripleonetech.sbe.promo.campaign.task.PromoCampaignTaskMapper;

@RestController
@Profile("api")
@RequestMapping("/campaigns")
@Api(tags = { "Campaigns" })
public class ApiCampaignController extends BaseApiController {
    @Autowired
    private PromoCampaignPlayerMapper promoCampaignPlayerMapper;
    @Autowired
    private PromoCampaignDepositMapper depositMapper;
    @Autowired
    private PromoCampaignTaskMapper taskMapper;
    @Autowired
    private PromoCampaignPlayerService campaignService;
    @Autowired
    private PromoCampaignLoginMapper loginMapper;

    private List<PromoCampaignMapper> campaignMappers;

    // Lazy loading
    private List<PromoCampaignMapper> getCampaignMappers() {
        if (this.campaignMappers == null) {
            campaignMappers = new ArrayList<>();
            campaignMappers.add(depositMapper);
            campaignMappers.add(taskMapper);
            campaignMappers.add(loginMapper);
        }
        return campaignMappers;
    }

    // Return the respective mapper
    private PromoCampaignMapper getCampaignMapper(PromoTypeEnum type) {
        switch (type) {
        case CAMPAIGN_DEPOSIT:
            return depositMapper;
        case CAMPAIGN_TASK:
            return taskMapper;
        case CAMPAIGN_LOGIN:
            return loginMapper;
        }
        return null;
    }

    @GetMapping("")
    @ApiOperation("Returns all live (APPROVED and RUNNING) campaigns visible to current player. These campaigns should include player join status and bonus status.")
    public RestResponse<List<PromoCampaignPlayerView>> list() {
        int playerId = getLoginPlayer().getId();
        return RestResponse.ok(campaignService.getLiveCampaignsForPlayer(playerId));
    }

    @GetMapping("/{type}/{id}")
    @ApiOperation("Get detail of specific campaign")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "type", value = "The type of campaign: deposit, task, login", allowableValues = "deposit,task,login"),
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of campaign")
    })
    public RestResponse<PromoCampaign> get(@PathVariable String type, @PathVariable int id) {
        PromoTypeEnum promoType = PromoTypeEnum.getFromShortName(type);
        PromoCampaign model = getCampaignMapper(promoType).get(id);
        return RestResponse.ok(model);
    }

    @PostMapping("/join")
    @ApiOperation("Join a promotion. PromoType: CAMPAIGN_DEPOSIT(11), CAMPAIGN_TASK(12), CAMPAIGN_RESCUE(13)")
    public RestResponse<RestResponseDataSuccess> add(@Valid @RequestBody List<PromoCampaignPlayerForm> mappings) {
        mappings.stream().forEach(m -> m.setPlayerId(getLoginPlayer().getId()));
        for (PromoCampaignPlayerForm campaignPlayer : mappings) {
            switch (campaignPlayer.getPromoTypeEnum()) {
            case CAMPAIGN_DEPOSIT:
                PromoCampaignPlayer promoCampaignDepositPlayer = new PromoCampaignPlayer();
                BeanUtils.copyProperties(campaignPlayer, promoCampaignDepositPlayer);
                promoCampaignPlayerMapper.insert(promoCampaignDepositPlayer);
                break;
            case CAMPAIGN_RESCUE:
                break;
            default:
            }
        }
        return RestResponse.operationSuccess();
    }

}
