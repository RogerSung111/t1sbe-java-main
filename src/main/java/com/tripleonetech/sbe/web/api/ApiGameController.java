package com.tripleonetech.sbe.web.api;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameName;
import com.tripleonetech.sbe.game.GameNameMapper;
import com.tripleonetech.sbe.game.info.GameInfoMapper;
import com.tripleonetech.sbe.game.info.GameInfoQueryForm;
import com.tripleonetech.sbe.game.info.GameInfoView;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerFavoriteGame;
import com.tripleonetech.sbe.player.PlayerFavoriteGameMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@RestController
@Profile("api")
@Api(tags = { "Games" })
public class ApiGameController extends BaseApiController {
    private static Logger logger = LoggerFactory.getLogger(ApiGameController.class);

    @Autowired
    private GameInfoMapper gameInfoMapper;
    @Autowired
    private GameNameMapper gameNameMapper;
    @Autowired
    private GameApiFactory gameApiFactory;
    @Autowired
    private Constant constant;
    @Autowired
    private PlayerFavoriteGameMapper playerFavoriteGameMapper;

    @GetMapping("/games/{currency}")
    @ApiOperation("List of games filtered by query criteria. Allowed sort columns: gameNameId, gameCode, releaseDate, updatedAt (default)")
    public RestResponse<PageInfo<GameInfoView>> gameList(GameInfoQueryForm queryForm,
            @PathVariable SiteCurrencyEnum currency) {
        Integer loginPlayerId = Optional.ofNullable(this.getLoginPlayer()).map(player -> player.getId()).orElse(null);
        String locale = LocaleContextHolder.getLocale().toLanguageTag();
        PageInfo<GameInfoView> gameInfos =
                new PageInfo<>(gameInfoMapper.getGameInfoList(loginPlayerId, currency, queryForm,
                        locale, constant.getFrontend().getDefaultLocale(), queryForm.getPage(), queryForm.getLimit()));
        return RestResponse.ok(gameInfos);
    }

    @GetMapping("/launch-game")
    @ApiOperation("Launch a game")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "gameApiId", value = "The ID of game API", required = true, paramType = "query", dataType = "int"),
        @ApiImplicitParam(name = "gameCode", value = "Game Code", required = true, paramType = "query", dataType = "String"),
        @ApiImplicitParam(name = "platform", value = "Game platform: pc/mobile", required = false, paramType = "query", dataType = "String"),
        @ApiImplicitParam(name = "mode", value = "Game mode: real/trial", required = false, paramType = "query", dataType = "String", allowableValues = "real, trial"),
        @ApiImplicitParam(name = "language", value = "Language Tag, e.g. en-US, zh-CN, zh-TW", required = false, paramType = "query", dataType = "String")
    })
    public RestResponse<String> launchGame(
            @RequestParam(value = "gameApiId") Integer gameApiId,
            @RequestParam(value = "gameCode") String gameCode,
            @RequestParam(value = "platform", required = false, defaultValue = "pc") String platform,
            @RequestParam(value = "mode", required = false, defaultValue = "real") String mode,
            @RequestParam(value = "language", required = false, defaultValue = "en-US") String language) throws RestResponseException {
        GameApi<? extends ApiGameLog> api = gameApiFactory.getById(gameApiId);
        Player player = getLoginPlayer();
        GameName gameName = gameNameMapper.getGameByCode(api.getApiCode(), gameCode, language,
                "en-US");
        GameApiResult result = api.launchGame(player, gameName, platform, mode, language);

        if (!result.isSuccess()) {
            //T1Game error message: username does not belong this merchant
            if(result.getParam("code").equals("24")) {
                if(api.register(player).isSuccess()) {
                    result = api.launchGame(player, gameName, platform, mode, language);
                    return RestResponse.ok(result.getGameUrl());
                }
                logger.error("Launch game failed: [{}]", result);
                throw new RestResponseException(RestResponse.apiFailed("Launch game failed"));
            }
        }
        return RestResponse.ok(result.getGameUrl());
    }

    @GetMapping("/launch-game-lobby")
    @ApiOperation("Launch a game")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gameApiId", value = "The ID of game API", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "platform", value = "Game platform: pc/mobile", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "mode", value = "Game mode: real/trial", paramType = "query", dataType = "String", allowableValues = "real, trial"),
            @ApiImplicitParam(name = "language", value = "Language Tag, e.g. en-US, zh-CN, zh-TW", paramType = "query", dataType = "String")
    })
    public RestResponse<String> launchGameLobby(
            @RequestParam(value = "gameApiId") Integer gameApiId,
            @RequestParam(value = "platform", required = false, defaultValue = "pc") String platform,
            @RequestParam(value = "mode", required = false, defaultValue = "real") String mode,
            @RequestParam(value = "language", required = false, defaultValue = "en-US") String language) throws RestResponseException {
        GameApi<? extends ApiGameLog> api = gameApiFactory.getById(gameApiId);
        Player player = getLoginPlayer();

        GameApiResult result = api.launchGameLobby(player, platform, mode, language);

        if (!result.isSuccess()) {
            //T1Game error message: username does not belong this merchant
            if(result.getParam("code").equals("24")) {
                if(api.register(player).isSuccess()) {
                    result = api.launchGameLobby(player, platform, mode, language);
                    return RestResponse.ok(result.getGameUrl());
                }
                logger.error("Launch game failed: [{}]", result);
                throw new RestResponseException(RestResponse.apiFailed("Launch game failed"));
            }
        }
        return RestResponse.ok(result.getGameUrl());
    }

    @PostMapping("/favorite-game")
    public RestResponse<RestResponseDataSuccess> addFavoriteGame(@Valid @RequestBody FavoriteGameForm form) {
        PlayerFavoriteGame obj = form.toFavoriteGame();
        obj.setPlayerId(getLoginPlayer().getId());
        playerFavoriteGameMapper.insert(obj);
        return RestResponse.operationSuccess();
    }

    @PostMapping("/favorite-game/delete")
    public RestResponse<RestResponseDataSuccess> removeFavoriteGame(@Valid @RequestBody FavoriteGameForm form) {
        PlayerFavoriteGame obj = form.toFavoriteGame();
        obj.setPlayerId(getLoginPlayer().getId());
        playerFavoriteGameMapper.delete(obj);
        return RestResponse.operationSuccess();
    }

    public static class FavoriteGameForm {
        @NotNull
        @ApiModelProperty(value = "Game api id", required = true)
        private Integer gameApiId;
        @NotBlank
        @ApiModelProperty(value = "Game code", required = true)
        private String gameCode;
        public Integer getGameApiId() {
            return gameApiId;
        }
        public void setGameApiId(Integer gameApiId) {
            this.gameApiId = gameApiId;
        }
        public String getGameCode() {
            return gameCode;
        }
        public void setGameCode(String gameCode) {
            this.gameCode = gameCode;
        }
        public PlayerFavoriteGame toFavoriteGame() {
            PlayerFavoriteGame obj = new PlayerFavoriteGame();
            obj.setGameApiId(gameApiId);
            obj.setGameCode(gameCode);
            return obj;
        }
    }

}
