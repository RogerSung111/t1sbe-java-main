package com.tripleonetech.sbe.web.api;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.player.announcement.PlayerAnnouncementPlayerDetailView;
import com.tripleonetech.sbe.player.announcement.PlayerAnnouncementPlayerView;
import com.tripleonetech.sbe.player.announcement.PlayerAnnouncementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("api")
@Api(tags = {"Messages & Announcements"})
@RequestMapping("/announcements")
public class ApiAnnouncementController extends BaseApiController {
    @Autowired
    private PlayerAnnouncementService playerAnnouncementService;

    @GetMapping("")
    @ApiOperation("Query player's available announcements, allowed sort columns: startAt (default), endAt, createdAt, updatedAt")
    public RestResponse<PageInfo<PlayerAnnouncementPlayerView>> listAvailableAnnouncement(PageQueryForm form) {
        if (getLoginPlayer() == null) {
            return RestResponse.ok(new PageInfo<>(playerAnnouncementService.queryAvailableNoLogin(form)));
        }
        return RestResponse.ok(new PageInfo<>(playerAnnouncementService.queryAvailable(getLoginPlayer(), form)));
    }

    @GetMapping("/{id}")
    @ApiOperation("Query player's specific announcements by id")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "ID of the announcement", required = true),
    })
    public RestResponse<PlayerAnnouncementPlayerDetailView> getAnnouncement(@PathVariable Integer id) throws RestResponseException {
        PlayerAnnouncementPlayerDetailView announcement = playerAnnouncementService.getAvailable(getLoginPlayer(), id);
        if (announcement == null) {
            throw new RestResponseException(RestResponse.playerAnnouncementNotFound(
                    String.format("Announcement [%d] does not exist or is not accessible under current player", id)
            ));
        }
        return RestResponse.ok(announcement);
    }
}
