package com.tripleonetech.sbe.web.api;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.withdraw.WithdrawRequestMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestQueryForm;
import com.tripleonetech.sbe.withdraw.WithdrawRequestView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Profile("api")
@RestController
@Api(tags = { "Cashier" })
@RequestMapping("/withdraw-requests")
public class ApiWithdrawRequestController extends BaseApiController {
    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;

    @GetMapping("")
    @ApiOperation("Query withdraw requests. Allowed sort columns: amount, updatedAt, requestedDate (default)")
    public RestResponse<PageInfo<WithdrawRequestView>> list(ApiWithdrawRequestQueryForm form) {
        Player player = getLoginPlayer();
        WithdrawRequestQueryForm query = new WithdrawRequestQueryForm();
        BeanUtils.copyProperties(form, query);
        query.setRequestedDateStartLocal(form.getRequestedDateStart());
        query.setRequestedDateEndLocal(form.getRequestedDateEnd());
        query.setCurrency(player.getCurrency());
        query.setUsername(player.getUsername());
        PageInfo<WithdrawRequestView> withdrawRequests =
                new PageInfo<>(withdrawRequestMapper.list(query, query.getPage(), query.getLimit()));
        return RestResponse.ok(withdrawRequests);
    }
    
    @GetMapping("/pending-amount")
    @ApiOperation("Get total amount of unapproved withdraw requests")
    public RestResponse<BigDecimal> getPendingDepositAmount(){
        Player player = getLoginPlayer();
        return RestResponse.ok(withdrawRequestMapper.getPendingRequestAmount(player.getId()));
    }
    
    public static class ApiWithdrawRequestQueryForm extends PageQueryForm {
        @ApiModelProperty(value = "Start time")
        private LocalDateTime requestedDateStart;
        @ApiModelProperty(value = "End time")
        private LocalDateTime requestedDateEnd;
        @ApiModelProperty(value = "Withdraw request status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)",
                allowableValues = "0,1,10,11")
        private Integer status;

        public LocalDateTime getRequestedDateStart() {
            return requestedDateStart;
        }

        public void setRequestedDateStart(ZonedDateTime requestedDateStart) {
            this.requestedDateStart = DateUtils.zonedToLocal(requestedDateStart);
        }

        public LocalDateTime getRequestedDateEnd() {
            return requestedDateEnd;
        }

        public void setRequestedDateEnd(ZonedDateTime requestedDateEnd) {
            this.requestedDateEnd = DateUtils.zonedToLocal(requestedDateEnd);
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
