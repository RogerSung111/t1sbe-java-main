package com.tripleonetech.sbe.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerUserDetails;

/**
 * Provides common methods for controllers under API profile
 * 
 * @author Yunfei<yunfei.dev@tripleonetech.net>
 *         Created on 18 Sep 2019
 */
public abstract class BaseApiController {
    
    @Autowired
    private PlayerMapper playerMapper;
    /**
     * Returns the instance of currently logged-in player. Thread-safe.
     * 
     * @return
     */
    protected Player getLoginPlayer() {
        Authentication oauth2Authentication = SecurityContextHolder.getContext().getAuthentication();
        if (oauth2Authentication.getPrincipal() instanceof PlayerUserDetails) {
            PlayerUserDetails userDetail = (PlayerUserDetails) oauth2Authentication.getPrincipal();
            return userDetail.getPlayer();
        } else {
            return null;
        }
    }
    
    protected Player getLoginPlayerRefresh() {
        Authentication oauth2Authentication = SecurityContextHolder.getContext().getAuthentication();
        if (oauth2Authentication.getPrincipal() instanceof PlayerUserDetails) {
            PlayerUserDetails userDetail = (PlayerUserDetails) oauth2Authentication.getPrincipal();
            Player newPlayer = playerMapper.getByUniqueKey(userDetail.getPlayer().getUsername(), userDetail.getPlayer().getCurrency());
            userDetail.setPlayer(newPlayer);
            return userDetail.getPlayer();
        } else {
            return null;
        } 
    }
}
