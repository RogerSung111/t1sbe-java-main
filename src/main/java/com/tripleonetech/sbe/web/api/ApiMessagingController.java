package com.tripleonetech.sbe.web.api;

import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.message.*;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Validated
@RestController
@Profile("api")
@Api(tags = {"Messages & Announcements"})
@RequestMapping("/messages")
public class ApiMessagingController extends BaseApiController {
    private final static Logger logger = LoggerFactory.getLogger(ApiMessagingController.class);

    @Autowired
    private PlayerMessageService playerMessageService;
    @Autowired
    private PlayerMessageMapper playerMessageMapper;

    @GetMapping("")
    @ApiOperation("Query player's message")
    public RestResponse<List<PlayerMessageThreadView>> listPlayerMessage(PlayerMessageThreadQueryForm queryForm) {
        queryForm.setPlayerId(getLoginPlayer().getId());
        List<PlayerMessageThreadView> playerMessageThreads = playerMessageMapper.queryThreadsWithMessages(queryForm);
        return RestResponse.ok(playerMessageThreads);
    }

    @PostMapping("/send")
    @ApiOperation("Send message from player to backend")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "body", name = "playerMessage", value = "Player message ", dataType = "PlayerMessage"),
    })
    public RestResponse<RestResponseIdSuccess> sendMessage(@Valid @RequestBody MessageForm messageForm) throws RestResponseException {
        PlayerMessage playerMessage = new PlayerMessage();
        playerMessage.setSubject(messageForm.getSubject());
        playerMessage.setContent(messageForm.getContent());
        playerMessage.setPlayerId(getLoginPlayer().getId());
        playerMessage.setRead(true);
        playerMessageService.sendMessage(playerMessage);
        if (playerMessage.getId() == null) {
            String message = String.format("Failed to send message [%s] from [%s]",
                    playerMessage.getSubject(),
                    playerMessage.getSenderEnum() == PlayerMessageSenderEnum.OPERATOR ?
                            playerMessage.getOperatorUsername() : playerMessage.getPlayerUsername());
            logger.error(message);
            throw new RestResponseException(RestResponse.operationFailed(message));
        }
        if (playerMessage.getId() != null && playerMessage.getId() > 0) {
            playerMessageService.newThread(playerMessage.getId());
        }
        return RestResponse.createSuccess(playerMessage.getId());
    }

    @PostMapping("/{id}/reply")
    @ApiOperation("Reply to the given message's thread")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "Message ID")
    })
    public RestResponse<RestResponseIdSuccess> replyMessage(
            @PathVariable int id,
            @Valid @RequestBody MessageContentForm contentForm) throws RestResponseException {
        Integer addedId = playerMessageService.playerReplyMessage(id, contentForm.getContent())
                .getId();
        if (addedId == null) {
            String message = String.format("Failed replying to message [%d]", id);
            logger.error(message);
            throw new RestResponseException(RestResponse.operationFailed(message));
        }
        return RestResponse.createSuccess(addedId);
    }

    @PostMapping("/system-read")
    @ApiOperation("Set player's system messages as read")
    public RestResponse<RestResponseDataSuccess> setSystemMessageRead() {
        Player player = getLoginPlayer();
        playerMessageMapper.setSystemRead(player.getId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/read")
    @ApiOperation("Set this message as read")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "Message ID")
    public RestResponse<RestResponseDataSuccess> setMessageRead(
            @PathVariable int id) {
        Player player = getLoginPlayer();
        playerMessageMapper.setRead(id, player.getId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/all-read")
    @ApiOperation("Set all unread messages as read")
    public RestResponse<RestResponseDataSuccess> setMessageBatchRead() {
        Player player = getLoginPlayer();
        playerMessageMapper.setBatchRead(player.getId());
        return RestResponse.operationSuccess();
    }

    @PostMapping("/{id}/thread-read")
    @ApiOperation("Set this message's thread as read")
    @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "Message ID")
    public RestResponse<RestResponseDataSuccess> setThreadRead(
            @PathVariable int id) throws RestResponseException {
        PlayerMessage message = playerMessageMapper.get(id);
        if (message == null) {
            throw new RestResponseException(RestResponse.playerMessageNotFound(
                    String.format("Player message id [%s] not found", id)));
        }
        if (message.getThreadId() != null) {
            playerMessageMapper.setThreadRead(message.getThreadId(), message.getPlayerId());
        }
        return RestResponse.operationSuccess();
    }

    public static class MessageForm {
        @NotBlank
        @ApiModelProperty(value = "Message title", required = true)
        private String subject;
        @NotBlank
        @ApiModelProperty(value = "Message content", required = true)
        private String content;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class MessageContentForm {
        @NotEmpty
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}
