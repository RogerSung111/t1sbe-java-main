package com.tripleonetech.sbe.web.api;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.game.log.GameLogMapper;
import com.tripleonetech.sbe.game.log.GameLogReportQueryForm;
import com.tripleonetech.sbe.game.log.GameLogView;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.ListView;
import com.tripleonetech.sbe.game.log.ReportGameLogsView.SummaryView;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionQueryForm;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@Profile("api")
@RequestMapping("/reports")
@Api(tags = { "Reports" })
public class ApiReportController extends BaseApiController {
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;

    @Autowired
    private GameLogMapper gameLogMapper;

    @GetMapping("/transactions")
    @ApiOperation("Query the wallet transactions for player")
    public RestResponse<PageInfo<WalletTransactionView>> walletTransactionList(
            @Valid WalletTransactionQueryForm queryForm) {
        Player player = getLoginPlayer();
        queryForm.setPlayerId(player.getId());
        PageInfo<WalletTransactionView> walletTransactions = new PageInfo<>(
                walletTransactionMapper.list(queryForm, queryForm.getPage(), queryForm.getLimit()));

        return RestResponse.ok(walletTransactions);
    }

    @GetMapping("/game-logs")
    @ApiOperation("Query player's gamelogs")
    public RestResponse<GameLogReportView> gameLogsList(@Valid GameLogReportQueryForm formObj) {
        Player player = getLoginPlayer();
        formObj.setUsername(player.getUsername());
        formObj.setSort("bet_time");
        formObj.setAsc(false);
        GameLogReportView result = new GameLogReportView();

        // list
        List<ListView> gameLogs = gameLogMapper.listReport(formObj, formObj.getPage(), formObj.getLimit());
        PageInfo<ListView> pageInfo = new PageInfo<>(gameLogs);
        PageInfo<GameLogView> listResult = new PageInfo<>();
        BeanUtils.copyProperties(pageInfo, listResult);
        listResult.setList(GameLogView.toGameLogView(gameLogs));
        result.setList(listResult);

        // summary
        List<SummaryView> summaryList = gameLogMapper.queryReportSummaryByApi(formObj);
        result.setSummary(GameLogView.toGameLogViewFromSummary(summaryList));
        return RestResponse.ok(result);
    }

    public static class GameLogReportView {
        private PageInfo<GameLogView> list;
        private List<GameLogView> summary;

        public PageInfo<GameLogView> getList() {
            return list;
        }

        public void setList(PageInfo<GameLogView> list) {
            this.list = list;
        }

        public List<GameLogView> getSummary() {
            return summary;
        }

        public void setSummary(List<GameLogView> summary) {
            this.summary = summary;
        }
    }

    public static class WalletTransactionView {
        @ApiModelProperty("Wallet transaction ID")
        private Integer id;
        @ApiModelProperty("Transaction type")
        private Integer type;
        @ApiModelProperty("Player's ID")
        private Integer playerId;
        @ApiModelProperty("Pre-transaction balance amount")
        private BigDecimal balanceBefore;
        @ApiModelProperty("Post-transaction balance amount")
        private BigDecimal balanceAfter;
        @ApiModelProperty("Transaction amount")
        private BigDecimal amount;
        @ApiModelProperty("Game API ID")
        private Integer gameApiId;
        @ApiModelProperty("External transaction ID")
        private String externalTransactionId;
        @ApiModelProperty("Internal Transaction ID")
        private String internalTransactionId;
        @ApiModelProperty("Promotion type")
        private Integer promoType;
        @ApiModelProperty("Rule ID of promotion")
        private Integer promoRuleId;
        @ApiModelProperty("Detail information of transaction")
        private String note;
        @ApiModelProperty("Operator's ID")
        private Integer operatorId;
        @ApiModelProperty("Operator's name")
        private String operatorUsername;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Integer getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Integer playerId) {
            this.playerId = playerId;
        }

        public BigDecimal getBalanceBefore() {
            return balanceBefore;
        }

        public void setBalanceBefore(BigDecimal balanceBefore) {
            this.balanceBefore = balanceBefore;
        }

        public BigDecimal getBalanceAfter() {
            return balanceAfter;
        }

        public void setBalanceAfter(BigDecimal balanceAfter) {
            this.balanceAfter = balanceAfter;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public Integer getGameApiId() {
            return gameApiId;
        }

        public void setGameApiId(Integer gameApiId) {
            this.gameApiId = gameApiId;
        }

        public String getExternalTransactionId() {
            return externalTransactionId;
        }

        public void setExternalTransactionId(String externalTransactionId) {
            this.externalTransactionId = externalTransactionId;
        }

        public String getInternalTransactionId() {
            return internalTransactionId;
        }

        public void setInternalTransactionId(String internalTransactionId) {
            this.internalTransactionId = internalTransactionId;
        }

        public Integer getPromoType() {
            return promoType;
        }

        public void setPromoType(Integer promoType) {
            this.promoType = promoType;
        }

        public Integer getPromoRuleId() {
            return promoRuleId;
        }

        public void setPromoRuleId(Integer promoRuleId) {
            this.promoRuleId = promoRuleId;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public String getOperatorUsername() {
            return operatorUsername;
        }

        public void setOperatorUsername(String operatorUsername) {
            this.operatorUsername = operatorUsername;
        }

        @Override public String toString() {
            return "WalletTransactionView{" +
                    "id=" + id +
                    ", type=" + type +
                    ", playerId=" + playerId +
                    ", balanceBefore=" + balanceBefore +
                    ", balanceAfter=" + balanceAfter +
                    ", amount=" + amount +
                    ", gameApiId=" + gameApiId +
                    ", externalTransactionId='" + externalTransactionId + '\'' +
                    ", internalTransactionId='" + internalTransactionId + '\'' +
                    ", promoType=" + promoType +
                    ", promoRuleId=" + promoRuleId +
                    ", note='" + note + '\'' +
                    ", operatorId=" + operatorId +
                    ", operatorUsername='" + operatorUsername + '\'' +
                    '}';
        }
    }
}
