package com.tripleonetech.sbe.web.api;

import com.github.pagehelper.PageInfo;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.BonusService;
import com.tripleonetech.sbe.promo.bonus.BonusView;
import com.tripleonetech.sbe.promo.bonus.PromoBonusQueryForm;
import com.tripleonetech.sbe.promo.bonus.PromoCampaignBonusQueryForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;
import java.util.List;

@RestController
@Profile("api")
@RequestMapping("/bonuses")
@Api(tags = {"Bonuses"})
public class ApiBonusController extends BaseApiController {
    @Autowired
    private BonusService bonusService;

    @GetMapping("/cashback")
    @ApiOperation("Query cashback bonuses for current player.")
    public RestResponse<PageInfo<BonusView>> listCashback( PlayerBonusQueryForm form) {
        PromoBonusQueryForm query = new PromoBonusQueryForm();
        BeanUtils.copyProperties(form, query);

        query.setPlayerId(getLoginPlayer().getCredentialId());
        query.setCurrency(getLoginPlayer().getCurrency());
        query.getPromoTypes().add(PromoTypeEnum.CASHBACK.getCode());
        return RestResponse.ok(new PageInfo<>(bonusService.query(query)));
    }

    @GetMapping("/campaign")
    @ApiOperation("Query campaign bonuses for current player.")
    public RestResponse<PageInfo<BonusView>> listCampaign(PlayerCampaignBonusQueryForm form) {
        PromoCampaignBonusQueryForm query = new PromoCampaignBonusQueryForm();
        BeanUtils.copyProperties(form, query);

        query.setPlayerId(getLoginPlayer().getCredentialId());
        query.setCurrency(getLoginPlayer().getCurrency());
        return RestResponse.ok(new PageInfo<>(bonusService.queryCampaign(query)));
    }

    @GetMapping("/referral")
    @ApiOperation("Query referral bonuses for current player.")
    public RestResponse<PageInfo<BonusView>> listReferral (PlayerBonusQueryForm form) {
        PromoBonusQueryForm query = new PromoBonusQueryForm();
        BeanUtils.copyProperties(form, query);

        query.setPlayerId(getLoginPlayer().getCredentialId());
        query.setCurrency(getLoginPlayer().getCurrency());
        query.getPromoTypes().add(PromoTypeEnum.REFERRAL.getCode());
        return RestResponse.ok(new PageInfo<>(bonusService.query(query)));
    }

    public static class PlayerBonusQueryForm extends PageQueryForm {
        @ApiModelProperty(value = "Query start date (inclusive)")
        private ZonedDateTime bonusDateStart;
        @ApiModelProperty(value = "Query end date (inclusive)")
        private ZonedDateTime bonusDateEnd;
        @ApiModelProperty(value = "Bonus status: PENDING_APPROVAL(0), APPROVED(1), REJECTED(10)", allowableValues = "0,1,10")
        private Integer status;
        @ApiModelProperty(value = "Promotion rule ID")
        private Integer ruleId;

        public ZonedDateTime getBonusDateStart() {
            return bonusDateStart;
        }

        public void setBonusDateStart(ZonedDateTime bonusDateStart) {
            this.bonusDateStart = bonusDateStart;
        }

        public ZonedDateTime getBonusDateEnd() {
            return bonusDateEnd;
        }

        public void setBonusDateEnd(ZonedDateTime bonusDateEnd) {
            this.bonusDateEnd = bonusDateEnd;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getRuleId() {
            return ruleId;
        }

        public void setRuleId(Integer ruleId) {
            this.ruleId = ruleId;
        }

    }

    public static class PlayerCampaignBonusQueryForm extends PlayerBonusQueryForm {
        @ApiModelProperty(value = "Promotion types: CAMPAIGN_DEPOSIT(11), CAMPAIGN_TASK(12)", allowableValues = "11,12")
        private List<Integer> promoTypes;

        public List<Integer> getPromoTypes() {
            return promoTypes;
        }

        public void setPromoTypes(List<Integer> promoTypes) {
            this.promoTypes = promoTypes;
        }
    }
}
