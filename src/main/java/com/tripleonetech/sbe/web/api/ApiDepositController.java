package com.tripleonetech.sbe.web.api;

import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.service.DepositRequestService;
import com.tripleonetech.sbe.payment.service.PaymentMethodService;
import com.tripleonetech.sbe.player.Player;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Profile("api")
@RestController
@Api(tags = { "Cashier" })
public class ApiDepositController extends BaseApiController {
    private static Logger logger = LoggerFactory.getLogger(ApiDepositController.class);

    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    @Autowired
    private DepositRequestService depositRequestService;
    @Autowired
    private PaymentApiFactory paymentApiFactory;
    @Autowired
    private PaymentMethodService paymentMethodService;
    @Autowired
    private DepositRequestMapper depositRequestMapper;

    @GetMapping("/payment-methods")
    @ApiOperation("List of available payment methods; payment type: BANK_TRANSFER(1), QUICK_PAY(2), WECHAT(3), ALIPAY(4), OTHERS(0);"
            + "form type: ManualAtmForm(1), SimpleForm(2), QrForm(3), QuickPayForm(4)")
    public RestResponse<List<PaymentMethodView>> listAvailablePaymentMethods() throws RestResponseException {
        Player player = getLoginPlayer();
        return RestResponse.ok(paymentMethodService.queryAvailableForPayment(player.getCredentialId(), player.getCurrency()));
    }

    @PostMapping("/payment-requests")
    @ApiOperation("Submit payment request")
    public RestResponse<PaymentApiResult> submitPaymentRequest(@Valid @RequestBody PaymentRequestForm paymentApiNewForm) throws RestResponseException{
        Player player = getLoginPlayer();
        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentApiNewForm.getPaymentMethodId());

        //Create deposit_request
        DepositRequest depositRequest = depositRequestService.createDepositRequest(paymentApiNewForm, player);
        if (null == depositRequest) {
            throw new RestResponseException(RestResponse.operationFailed("Failed creating deposit request"));
        }

        PaymentApiResult result = null;
        // Use: Third-Party Payment API
        if (null != paymentMethod.getPaymentApiId()) {
            PaymentApi paymentApi = paymentApiFactory.getById(paymentMethod.getPaymentApiId());
            result = paymentApi.submitRequest(depositRequest);
            if (!result.isSuccess()) {
                throw new RestResponseException(RestResponse.apiFailed(result.getMessage()));
            }
        // Use: Manually Deposit
        } else {
            result = new PaymentApiResult(ApiResponseEnum.OK);
            result.setParam("depositRequestId", depositRequest.getId());
            result.setParam("amount", depositRequest.getAmount());
            result.setParam("note", depositRequest.getNote());
        }

        return RestResponse.ok(result);
    }

    @GetMapping("/payment-requests/{id}")
    @ApiOperation("Get detail of a deposit request; status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11), EXPIRED(12)")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "id", value = "The ID of deposit request")
    })
    public RestResponse<DepositRequestView> getPaymentRequestDetail(@PathVariable long id)
        throws RestResponseException {
        Player player = getLoginPlayer();
        DepositRequestView depositRequestView = depositRequestMapper.getView(id);
        if (depositRequestView == null || !depositRequestView.getPlayerId().equals(player.getId())) {
            throw new RestResponseException(
                    RestResponse.entityNotFound(String.format("Deposit request[%d] for player[%d] not found", id, player.getId()))
            );
        }
        return RestResponse.ok(depositRequestView);
    }

}
