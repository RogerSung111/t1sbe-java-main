package com.tripleonetech.sbe.web.api;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.common.web.RestResponseDataSuccess;
import com.tripleonetech.sbe.common.web.RestResponseException;
import com.tripleonetech.sbe.common.web.RestResponseIdSuccess;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.GameApiService;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.wallet.GameWalletView;
import com.tripleonetech.sbe.player.wallet.Wallet;
import com.tripleonetech.sbe.player.wallet.WalletMapper;
import com.tripleonetech.sbe.player.wallet.WalletService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@Profile("api")
@RequestMapping("/wallets")
@Api(tags = { "Wallet" })
public class ApiWalletController extends BaseApiController {
    private static final Logger logger = LoggerFactory.getLogger(ApiWalletController.class);

    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private GameApiService gameApiService;
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    @Autowired
    private GameApiFactory gameApiFactory;
    
    @GetMapping("")
    @ApiOperation("All player wallet information, the record with NULL game API ID is main wallet")
    public RestResponse<List<GameWalletView>> list() {
        Player player = getLoginPlayer();
        List<GameWalletView> wallets = new LinkedList<>();

        wallets.add(GameWalletView.create(walletService.getMainWallet(player.getId()), player.getCurrency()));
        Map<Integer, Wallet> walletMap = walletMapper.getActiveGameWalletsByPlayerId(player.getId());
        SortedMap<Integer, Wallet> walletMapSorted = new TreeMap<>(walletMap);
        wallets.addAll(walletMapSorted.values().stream()
                .map(e -> GameWalletView.create(e,
                        player.getCurrency(),
                        gameApiService.allowDecimalTransfer(e.getGameApiId()))
                )
                .collect(Collectors.toList())
        );
        return RestResponse.ok(wallets);
    }

    @GetMapping("/{gameApiId}")
    @ApiOperation("Returns game wallet information for the given game API. (Note: This method triggers communication with external game API and refreshes wallet balance if it's dirty.")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "Game API ID")
    })
    public RestResponse<? extends Object> getBalance(@PathVariable int gameApiId) {
        Player player = getLoginPlayer();
        int playerId = player.getId();

        Wallet wallet = walletMapper.get(playerId, gameApiId);
        if (wallet == null) {
            return RestResponse.badWalletRequest("Wallet not found");
        }

        Wallet walletRefreshed = walletService.getCurrentBalance(player, wallet);
        if (walletRefreshed.isError()) {
            return RestResponse.badWalletRequest(walletRefreshed.getErrorCode());
        }
        GameWalletView gameWalletView = GameWalletView.create(walletRefreshed, player.getCurrency());
        return RestResponse.ok(gameWalletView);
    }

    @Transactional
    @PostMapping(value = "/{gameApiId}/deposit")
    @ApiOperation("Deposit amount to game balance from main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API to deposit to"),
            @ApiImplicitParam(required = false, paramType = "query", name = "amount", value = "Amount of money to deposit, if empty, deposit all balance in main wallet")
    })
    public RestResponse<RestResponseDataSuccess> mainToSub(@PathVariable Integer gameApiId,
            @RequestParam Optional<BigDecimal> amount) {
        GameApiConfig gameApiConfig = gameApiConfigMapper.get(gameApiId);
        if(!gameApiConfig.getStatus().equals(ApiConfigStatusEnum.ENABLED)) {
            return RestResponse.operationFailed("Cannot deposit amount into disabled game");
        }

        Player player = getLoginPlayer();
        int playerId = player.getId();
        BigDecimal depositAmount = amount.orElse(walletService.getMainWalletBalance(playerId));
        logger.info("Deposit to game, player id:[{}], game api id:[{}], amount:[{}]", playerId, gameApiId, depositAmount);

        ApiResult result = walletService.depositGame(playerId, gameApiId, depositAmount, null);
        if (!result.isSuccess()) {
            return RestResponse.badWalletRequest(result.getMessage());
        }
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/{gameApiId}/withdraw")
    @ApiOperation("Withdraw amount from game balance to main wallet")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "path", name = "gameApiId", value = "ID of the game API to withdraw from"),
            @ApiImplicitParam(required = false, paramType = "query", name = "amount", value = "Amount of money to withdraw, if empty, withdraw all balance from this game wallet")
    })
    public RestResponse<RestResponseDataSuccess> subToMain(@PathVariable Integer gameApiId,
            @RequestParam Optional<BigDecimal> amount) {
        
        GameApiConfig gameApiConfig = gameApiConfigMapper.get(gameApiId);
        if(!ApiConfigStatusEnum.ENABLED.equals(gameApiConfig.getStatus())) {
            return RestResponse.operationFailed("Cannot withdraw amount from disabled game api wallet");
        }
        Player player = getLoginPlayer();
        int playerId = player.getId();
        BigDecimal withdrawAmount = amount.orElse(walletMapper.get(playerId, gameApiId).getBalance());
        logger.info("Withdraw from game, player id:[{}], game api id:[{}], amount:[{}]", playerId, gameApiId,
                withdrawAmount);

        ApiResult result = walletService.withdrawGame(playerId, gameApiId, withdrawAmount);
        if (!result.isSuccess()) {
            return RestResponse.badWalletRequest(result.getMessage());
        }
        return RestResponse.operationSuccess();
    }

    @Transactional
    @PostMapping("/collect-all-balances")
    @ApiOperation("Transfer all game balance back to main wallet (Note that this action iterates thru all wallets and will take considerable amount of time.)")
    public RestResponse<RestResponseDataSuccess> subsAllToMain() throws RestResponseException {
        Player player = getLoginPlayer();
        int playerId = player.getId();
        List<Wallet> subWallets = new ArrayList<>(walletMapper.getAllGameWalletsByPlayerId(playerId).values());
        int[] gameApiIds = subWallets.stream().mapToInt(Wallet::getGameApiId).toArray();

        boolean allSuccess = true;
        List<RestResponseIdSuccess> idResultList = new ArrayList<>();
        for (Integer gameApiId : gameApiIds) {
            GameApi<? extends ApiGameLog> gameApi = gameApiFactory.getById(gameApiId);
            if(!ApiConfigStatusEnum.ENABLED.equals(gameApi.getStatus())) {
                logger.debug("Exculde disbaled Game API : {}", gameApi.getApiCode());
                continue;
            }
            BigDecimal amount = gameApiService.syncBalance(gameApiId, player).getBalance();
            if (BigDecimal.ZERO.compareTo(amount) == 0) {
                continue;
            } else {
                ApiResult withdrawApiResult = walletService.withdrawGame(playerId, gameApiId, amount);
                RestResponseIdSuccess idSuccess = RestResponseIdSuccess.success(gameApiId);
                if (!withdrawApiResult.isSuccess()) {
                    allSuccess = false;
                    idSuccess = RestResponseIdSuccess.fail(gameApiId, withdrawApiResult.getResponseEnum().toString());
                }
                idResultList.add(idSuccess);
            }
        }
        if (!allSuccess) {
            throw new RestResponseException(RestResponse.multiStatus(idResultList));
        }

        return RestResponse.operationSuccess();
    }
}
