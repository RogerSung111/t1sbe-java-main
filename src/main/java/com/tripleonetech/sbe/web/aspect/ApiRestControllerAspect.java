package com.tripleonetech.sbe.web.aspect;

import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.regex.Pattern;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.config.Slf4jMDCFilterConfiguration;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerCredentialMapper;
import com.tripleonetech.sbe.report.player.PlayerActivityReport;
import com.tripleonetech.sbe.report.player.PlayerActivityReportMapper;
import com.tripleonetech.sbe.web.api.BaseApiController;

@Aspect
@Profile("api")
@Component
public class ApiRestControllerAspect extends BaseApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiRestControllerAspect.class);

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    public void restController() {
    }

    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;

    @AfterThrowing(value = "restController()", throwing = "throwable")
    public void afterRestApiThrow(JoinPoint point, Throwable throwable) {
        createReport(point, null, throwable);
        updateLastActivityTime();
    }

    @AfterReturning(pointcut = "restController()", returning = "result")
    public void afterRestApiReturn(JoinPoint point, Object result) {
        createReport(point, result, null);
        updateLastActivityTime();
    }

    private void createReport(JoinPoint point, Object result, Throwable throwable) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        // In case of proxies we split the ugly $$ part away
        String className = point.getTarget().getClass().getSimpleName().split(Pattern.quote("$$"))[0];
        Player loginPlayer = getLoginPlayer();

        Integer returnCode = null;
        if (Objects.nonNull(result) && result instanceof RestResponse) {
            returnCode = ((RestResponse<?>) result).getCode();
        }

        Object requestBody = getRequestBody(point);

        PlayerActivityReport report = new PlayerActivityReport();
        report.setRequestBody(Objects.isNull(requestBody) ? null : JsonUtils.objectToJson(requestBody));
        report.setReturnCode(returnCode);
        report.setPlayerId(Objects.isNull(loginPlayer) ? null : loginPlayer.getId());
        report.setInvoke(className + "." + signature.getName());
        report.setDevice(ClientHttpInfoUtils.getDeviceType());
        report.setIp(ClientHttpInfoUtils.getIp());
        report.setRequestUri(MDC.get("req.requestURI"));
        report.setHttpMethod(MDC.get("req.method"));
        report.setQueryString(MDC.get("req.queryString"));
        report.setResponse(Objects.isNull(result) ? null : JsonUtils.objectToJson(result));
        report.setException(Objects.isNull(throwable) ? null : ExceptionUtils.getStackTrace(throwable));
        report.setRequestUUID(MDC.get(Slf4jMDCFilterConfiguration.DEFAULT_MDC_UUID_TOKEN_KEY));
        playerActivityReportMapper.insert(report);
    }

    private void updateLastActivityTime() {
        Player loginPlayer = getLoginPlayer();
        if (loginPlayer != null)
            playerCredentialMapper.updateLastActivityTime(loginPlayer.getUsername(), LocalDateTime.now());
    }

    public Object getRequestBody(JoinPoint thisJoinPoint) {
        MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
        Annotation[][] annotationMatrix = methodSignature.getMethod().getParameterAnnotations();

        int index = -1;
        for (Annotation[] annotations : annotationMatrix) {
            index++;
            for (Annotation annotation : annotations) {
                if (annotation instanceof RequestBody) {
                    return thisJoinPoint.getArgs()[index];
                }
            }
        }
        return null;
    }
}
