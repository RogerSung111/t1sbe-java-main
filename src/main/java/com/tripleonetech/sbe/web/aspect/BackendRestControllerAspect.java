package com.tripleonetech.sbe.web.aspect;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.web.RestResponse;
import com.tripleonetech.sbe.config.Slf4jMDCFilterConfiguration;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.report.operator.OperatorActivityReport;
import com.tripleonetech.sbe.report.operator.OperatorActivityReportMapper;
import com.tripleonetech.sbe.web.opbackend.BaseOpbackendController;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.regex.Pattern;

@Aspect
@Profile("main")
@Component
public class BackendRestControllerAspect extends BaseOpbackendController {

    private static final Logger logger = LoggerFactory.getLogger(BackendRestControllerAspect.class);

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    public void restController() {
    }

    @Autowired
    private OperatorActivityReportMapper operatorActivityReportMapper;

    @AfterThrowing(value = "restController()", throwing = "throwable")
    public void exceptionLogger(JoinPoint point, Throwable throwable) {
        createReport(point, null, throwable);
    }

    @AfterReturning(pointcut = "restController()", returning = "result")
    public void afterLogger(JoinPoint point, Object result) {
        createReport(point, result, null);
    }

    private void createReport(JoinPoint point, Object result, Throwable throwable) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        // In case of proxies we split the ugly $$ part away
        String className = point.getTarget().getClass().getSimpleName().split(Pattern.quote("$$"))[0];
        Operator loginOperator = getLoginOperator();

        Integer returnCode = null;
        if (Objects.nonNull(result) && result instanceof RestResponse) {
            returnCode = ((RestResponse<?>) result).getCode();
        }

        Object requestBody = getRequestBody(point);
//        Object[] args = this.getArgs(point.getArgs());

        OperatorActivityReport report = new OperatorActivityReport();
        report.setRequestBody(Objects.isNull(requestBody) ? null : JsonUtils.objectToJson(requestBody));
        report.setReturnCode(returnCode);
        report.setOperatorId(Objects.isNull(loginOperator) ? null : loginOperator.getId());
        report.setUsername(Objects.isNull(loginOperator) ? null : loginOperator.getUsername());
        report.setInvoke(className + "." + signature.getName());
        report.setDevice(ClientHttpInfoUtils.getDeviceType());
        report.setIp(ClientHttpInfoUtils.getIp());
        report.setRequestUri(MDC.get("req.requestURI"));
        report.setHttpMethod(MDC.get("req.method"));
        report.setQueryString(MDC.get("req.queryString"));
        report.setResponse(Objects.isNull(result) ? null : JsonUtils.objectToJson(result));
        report.setException(Objects.isNull(throwable) ? null : ExceptionUtils.getStackTrace(throwable));
        report.setRequestUUID(MDC.get(Slf4jMDCFilterConfiguration.DEFAULT_MDC_UUID_TOKEN_KEY));
        operatorActivityReportMapper.insert(report);
    }

    private Object[] getArgs(Object[] args) {
        Object[] newArgs = new Object[args.length];
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof MultipartFile) {
                newArgs[i] = "MultipartFile: " + ((MultipartFile) args[i]).getOriginalFilename();
            } else {
                newArgs[i] = args[i];
            }
        }
        return newArgs;
    }

    public Object getRequestBody(JoinPoint thisJoinPoint) {
        MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
        Annotation[][] annotationMatrix = methodSignature.getMethod().getParameterAnnotations();

        int index = -1;
        for (Annotation[] annotations : annotationMatrix) {
            index++;
            for (Annotation annotation : annotations) {
                if (annotation instanceof RequestBody) {
                    return thisJoinPoint.getArgs()[index];
                }
            }
        }
        return null;
    }
}
