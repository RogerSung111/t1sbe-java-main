package com.tripleonetech.sbe.announcement;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnnouncementMapper {
    int insert(String message);

    List<Map<String, Object>> list();
}
