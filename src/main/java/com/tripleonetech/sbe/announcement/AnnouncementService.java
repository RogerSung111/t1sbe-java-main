package com.tripleonetech.sbe.announcement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnnouncementService {
    
    private static final Logger looger = LoggerFactory.getLogger(AnnouncementService.class);
    @Autowired
    AnnouncementMapper announcementMapper;
    
    public void addAnnouncement(String message) {
        looger.debug("setAnnouncement : [{}]", message);
        announcementMapper.insert(message);
    }
}
