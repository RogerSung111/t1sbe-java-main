package com.tripleonetech.sbe.report;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

public class SummaryReportView {
    @ApiModelProperty(value = "Date")
    private LocalDate date;
    @ApiModelProperty(value = "Count of registered player")
    private int playerCount;
    @ApiModelProperty(value = "Count of withdrawal requests")
    private int withdrawalCount;
    @ApiModelProperty(value = "Sum of withdrawal amount")
    private BigDecimal withdrawalAmount = BigDecimal.ZERO;
    @ApiModelProperty(value = "Count of deposit requests")
    private int depositCount;
    @ApiModelProperty(value = "Sum of deposit amount")
    private BigDecimal depositAmount = BigDecimal.ZERO;
    @ApiModelProperty(value = "Bet count")
    private int betCount;
    @ApiModelProperty(value ="The sum of bet amount")
    private BigDecimal bet = BigDecimal.ZERO;
    @ApiModelProperty(value ="The sum of payout")
    private BigDecimal payout = BigDecimal.ZERO;

    @ApiModelProperty(value = "Unique id for this data, used by frontend")
    public String getId() {
        return date.toString();
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public int getWithdrawalCount() {
        return withdrawalCount;
    }

    public void setWithdrawalCount(int withdrawalCount) {
        this.withdrawalCount = withdrawalCount;
    }

    public BigDecimal getWithdrawalAmount() {
        if(withdrawalAmount == null){
            return  BigDecimal.ZERO;
        }
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public int getDepositCount() {
        return depositCount;
    }

    public void setDepositCount(int depositCount) {
        this.depositCount = depositCount;
    }

    public BigDecimal getDepositAmount() {
        if(depositAmount == null){
            return  BigDecimal.ZERO;
        }
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public int getBetCount() {
        return betCount;
    }

    public void setBetCount(int betCount) {
        this.betCount = betCount;
    }

    public BigDecimal getBet() {
        if(bet == null){
            return  BigDecimal.ZERO;
        }
        return bet;
    }

    public void setBet(BigDecimal bet) {
        this.bet = bet;
    }

    public BigDecimal getPayout() {
        if(payout == null){
            return  BigDecimal.ZERO;
        }
        return payout;
    }

    public void setPayout(BigDecimal payout) {
        this.payout = payout;
    }
}
