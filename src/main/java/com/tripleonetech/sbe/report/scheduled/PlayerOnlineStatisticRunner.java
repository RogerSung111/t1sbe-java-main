package com.tripleonetech.sbe.report.scheduled;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.report.player.PlayerOnlineStatisticService;

@Profile("scheduled")
@Component
public class PlayerOnlineStatisticRunner {

    @Autowired
    private PlayerOnlineStatisticService playerOnlineStatisticService;

    @Scheduled(cron = "0 * * * * ?")
    public void runLastHour() {
        LocalDateTime lastHour = LocalDateTime.now().minusHours(1);
        playerOnlineStatisticService.createHourlyRecord(lastHour);
    }

}
