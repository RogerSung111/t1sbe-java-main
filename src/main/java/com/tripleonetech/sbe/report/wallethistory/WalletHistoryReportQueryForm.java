package com.tripleonetech.sbe.report.wallethistory;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class WalletHistoryReportQueryForm extends PageQueryForm {
    @NotNull
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "Start time", required = true)
    private LocalDateTime createdAtStart;
    @NotNull
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "End time", required = true)
    private LocalDateTime createdAtEnd;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Game API IDs")
    private List<Integer> gameApiIds;
    @NotNull
    @ApiModelProperty(value = "Player username")
    private String username;

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public List<Integer> getGameApiIds() {
        return gameApiIds;
    }

    public void setGameApiIds(List<Integer> gameApiIds) {
        this.gameApiIds = gameApiIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
