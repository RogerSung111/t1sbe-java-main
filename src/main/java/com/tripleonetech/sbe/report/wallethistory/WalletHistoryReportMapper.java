package com.tripleonetech.sbe.report.wallethistory;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WalletHistoryReportMapper {
    List<WalletHistoryReport> query(@Param("query") WalletHistoryReportQueryForm query,
                                    @Param("pageNum") Integer pageNum,
                                    @Param("pageSize") Integer pageSize);
}
