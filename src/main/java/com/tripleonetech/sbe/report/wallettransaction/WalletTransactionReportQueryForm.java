package com.tripleonetech.sbe.report.wallettransaction;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;

import io.swagger.annotations.ApiModelProperty;

public class WalletTransactionReportQueryForm extends PageQueryForm {
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "Start time")
    private LocalDateTime createdAtStart;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "End time")
    private LocalDateTime createdAtEnd;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Transaction type: DEPOSIT(0), WITHDRAWAL(1), TRANSFER_IN(2), TRANSFER_OUT(3), BONUS(4), " +
            "CASHBACK(5), FREEZE(7), WITHDRAWAL_CANCEL(9), ADD_BONUS(10), SUBTRACT_BONUS(11), ADD_BALANCE(12), " +
            "SUBTRACT_BALANCE(13), ADD_CASHBACK(14), ADD_WITHDRAWAL_CONDITION(15), PAYMENT_CHARGE(16)",
        allowableValues = "0,1,2,3,4,5,7,9,10,11,12,13,14,15,16")
    private Integer type;
    @ApiModelProperty(value = "Game API IDs")
    private List<Integer> gameApiIds;
    @ApiModelProperty(value = "Player username")
    private String username;
    @ApiModelProperty(value = "Player ID")
    private Integer playerId;

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }
    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getType() {
        return type;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not appear on swagger UI")
    public WalletTransactionTypeEnum getTypeEnum() {
        return Objects.isNull(getType()) ? null : WalletTransactionTypeEnum.valueOf(getType());
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Integer> getGameApiIds() {
        return gameApiIds;
    }

    public void setGameApiIds(List<Integer> gameApiIds) {
        this.gameApiIds = gameApiIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
