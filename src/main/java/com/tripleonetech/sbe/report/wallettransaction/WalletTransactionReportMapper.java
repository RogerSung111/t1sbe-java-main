package com.tripleonetech.sbe.report.wallettransaction;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Mapper
public interface WalletTransactionReportMapper {
    List<WalletTransactionRecord> query(@Param("query") WalletTransactionReportQueryForm query,
                                        @Param("pageNum") Integer pageNum,
                                        @Param("pageSize") Integer pageSize);
}
