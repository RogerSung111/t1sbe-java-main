package com.tripleonetech.sbe.report.player;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.web.TimeRangeForm;

@Mapper
public interface PlayerOnlineReportMapper {

    int insert(PlayerOnlineReport record);

    List<PlayerOnlineReportView> list(
            @Param("query") TimeRangeForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

}