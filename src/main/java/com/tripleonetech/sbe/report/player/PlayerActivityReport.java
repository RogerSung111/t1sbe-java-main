package com.tripleonetech.sbe.report.player;

import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.player.PlayerActivityTypeEnum;
import com.tripleonetech.sbe.player.PlayerIdView;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class PlayerActivityReport {
    private Long id;
    private Integer playerId;
    private PlayerActivityTypeEnum action;
    private String invoke;
    private String httpMethod;
    private String requestUUID;
    private Integer returnCode;
    private String requestUri;
    private String requestBody;
    private String queryString;
    private String response;
    private String exception;
    private DeviceTypeEnum device;
    private String ip;
    private LocalDateTime createdAt;
    private PlayerIdView player;
    private Integer playerCredentialId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public PlayerActivityTypeEnum getAction() {
        return action;
    }

    public void setAction(PlayerActivityTypeEnum action) {
        this.action = action;
    }

    public String getInvoke() {
        return invoke;
    }

    public void setInvoke(String invoke) {
        this.invoke = invoke;
    }

    public String getRequestUUID() {
        return requestUUID;
    }

    public void setRequestUUID(String requestUUID) {
        this.requestUUID = requestUUID;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public DeviceTypeEnum getDevice() {
        return device;
    }

    public void setDevice(DeviceTypeEnum device) {
        this.device = device;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public PlayerIdView getPlayer() {
        return player;
    }

    public void setPlayer(PlayerIdView player) {
        this.player = player;
    }

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    public LocalDate getCreatedAtLd() {
        return createdAt.toLocalDate();
    }

}
