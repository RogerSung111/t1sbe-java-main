package com.tripleonetech.sbe.report.player;

import java.time.LocalDateTime;

public class PlayerOnlineReport {
    private Integer id;

    private LocalDateTime timeHour;

    private Integer max;

    private Float average;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getTimeHour() {
        return timeHour;
    }

    public void setTimeHour(LocalDateTime timeHour) {
        this.timeHour = timeHour;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Float getAverage() {
        return average;
    }

    public void setAverage(Float average) {
        this.average = average;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}