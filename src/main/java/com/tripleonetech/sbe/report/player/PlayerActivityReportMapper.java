package com.tripleonetech.sbe.report.player;

import com.tripleonetech.sbe.common.web.TimeRangeForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface PlayerActivityReportMapper {

    void insert(PlayerActivityReport playerActivityReport);

    PlayerActivityReport get(Long id);

    PlayerActivityReport getSecondToLastLogin(Integer playerId);

    List<PlayerActivityReport> getReport(@Param("query") PlayerActivityReportQueryForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    List<Integer> queryPlayerCountsPerTenMinutes(@Param("timeHour") LocalDateTime timeHour);

    List<Integer> queryPlayerCounts(
            @Param("query") TimeRangeForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    Integer getConsecutiveLoginCountByPlayerId(@Param("playerId") int playerId, @Param("startFrom") String startFrom, @Param("zoneOffSetMin") Integer zoneOffSetMin);

}
