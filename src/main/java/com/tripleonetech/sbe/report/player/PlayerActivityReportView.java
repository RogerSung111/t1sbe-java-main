package com.tripleonetech.sbe.report.player;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.PlayerActivityTypeEnum;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class PlayerActivityReportView {
    private long id;
    private String username;
    private Integer playerId;
    private SiteCurrencyEnum currency;
    private PlayerActivityTypeEnum action;
    private String invoke;
    private Integer returnCode;
    private String httpMethod;
    private String requestUri;
    private String requestBody;
    private String queryString;
    private String response;
    private String exception;
    private Integer device;
    private String ip;
    private LocalDateTime createdAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public PlayerActivityTypeEnum getAction() {
        return action;
    }

    public void setAction(PlayerActivityTypeEnum action) {
        this.action = action;
    }

    public String getInvoke() {
        return invoke;
    }

    public void setInvoke(String invoke) {
        this.invoke = invoke;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public static PlayerActivityReportView create(PlayerActivityReport playerActivityReport) {
        PlayerActivityReportView playerActivityReportView = new PlayerActivityReportView();
        playerActivityReportView.setId(playerActivityReport.getId());
        playerActivityReportView.setPlayerId(playerActivityReport.getPlayerCredentialId());
        playerActivityReportView.setUsername(playerActivityReport.getPlayer().getUsername());
        playerActivityReportView.setInvoke(playerActivityReport.getInvoke());
        playerActivityReportView.setAction(playerActivityReport.getAction());
        playerActivityReportView.setCurrency(playerActivityReport.getPlayer().getCurrency());
        playerActivityReportView.setHttpMethod(playerActivityReport.getHttpMethod());
        playerActivityReportView.setRequestUri(playerActivityReport.getRequestUri());
        playerActivityReportView.setRequestBody(playerActivityReport.getRequestBody());
        playerActivityReportView.setQueryString(playerActivityReport.getQueryString());
        playerActivityReportView.setReturnCode(playerActivityReport.getReturnCode());
        playerActivityReportView.setResponse(playerActivityReport.getResponse());
        playerActivityReportView.setException(playerActivityReport.getException());
        playerActivityReportView.setCreatedAt(playerActivityReport.getCreatedAt());
        playerActivityReportView.setIp(playerActivityReport.getIp());
        if (playerActivityReport.getDevice() != null) { // avoid NPE if device is null
            playerActivityReportView.setDevice(playerActivityReport.getDevice().getCode());
        }
        return playerActivityReportView;
    }

}
