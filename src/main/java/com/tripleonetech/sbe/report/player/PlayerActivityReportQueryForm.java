package com.tripleonetech.sbe.report.player;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

public class PlayerActivityReportQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Player ID")
    private Integer playerCredentialId;
    @ApiModelProperty(hidden = true)
    private Integer playerId;
    @ApiModelProperty(value = "Player username")
    private String username;
    @ApiModelProperty(value = "Invoked controller/method")
    private String invoke;
    @ApiModelProperty(value = "POST or GET")
    private String httpMethod;
    @ApiModelProperty(value = "api uri path")
    private String requestUri;
    @ApiModelProperty(value = "Return code")
    private Integer returnCode;
    @ApiModelProperty(value = "currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "player activity type, LOGIN = 1")
    private Integer action;
    @ApiModelProperty(value="Player device: UNKNOWN(0), MANUAL(1), COMPUTER_HTML(2), MOBILE_HTML(3), APP_IOS(4), APP_ANDROID(5), TABLET_HTML(6)", allowableValues = "0,1,2,3,4,5,6")
    private Integer device;
    @ApiModelProperty(value = "Player IP")
    private String ip;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "Begin time of creation time search")
    private LocalDateTime createdAtStart;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "End time of creation time search")
    private LocalDateTime createdAtEnd;

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getInvoke() {
        return invoke;
    }

    public void setInvoke(String invoke) {
        this.invoke = invoke;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Integer getDevice() {
        return device;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not show on swagger UI")
    public DeviceTypeEnum getDeviceEnum() {
        return Objects.isNull(getDevice()) ? null : DeviceTypeEnum.valueOf(getDevice());
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStartLocal(LocalDateTime createdAtStart) {
        this.createdAtStart = createdAtStart;
    }

    public void setCreatedAtStartFrom(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEndLocal(LocalDateTime createdAtEnd) {
        this.createdAtEnd = createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
}
