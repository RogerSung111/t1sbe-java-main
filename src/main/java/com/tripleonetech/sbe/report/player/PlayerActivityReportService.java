package com.tripleonetech.sbe.report.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerActivityTypeEnum;

@Service
public class PlayerActivityReportService {
    
    public final static Integer LOGIN_SUCCESS = 20000;
    public final static Integer LOGIN_FAILURE = 40100;
    
    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    
    public void addPlayerActivityReport(Player player, PlayerActivityTypeEnum action, Integer returnCode) {
        
        PlayerActivityReport playerActivityReport = new PlayerActivityReport();
        playerActivityReport.setPlayerId(player.getId());
        playerActivityReport.setDevice(player.getLastLoginDevice());
        playerActivityReport.setIp(player.getLastLoginIp());
        playerActivityReport.setAction(action);
        playerActivityReport.setReturnCode(returnCode);
        playerActivityReportMapper.insert(playerActivityReport);
    }

    public PlayerActivityReport getSecondToLastLogin(Integer playerId) {
        return playerActivityReportMapper.getSecondToLastLogin(playerId);
    }
}
