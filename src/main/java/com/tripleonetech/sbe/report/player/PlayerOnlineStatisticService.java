package com.tripleonetech.sbe.report.player;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerOnlineStatisticService {

    @Autowired
    private PlayerActivityReportMapper playerActivityReportMapper;
    @Autowired
    private PlayerOnlineReportMapper playerOnlineReportMapper;

    public void createHourlyRecord(LocalDateTime timeHour) {
        timeHour = timeHour.truncatedTo(ChronoUnit.HOURS);
        List<Integer> playerCounts = playerActivityReportMapper.queryPlayerCountsPerTenMinutes(timeHour);

        PlayerOnlineReport record = new PlayerOnlineReport();
        record.setMax(playerCounts.stream().mapToInt(v -> v).max().orElse(0));
        record.setAverage((float) playerCounts.stream().mapToInt(v -> v).average().orElse(0));
        playerOnlineReportMapper.insert(record);

    }

}
