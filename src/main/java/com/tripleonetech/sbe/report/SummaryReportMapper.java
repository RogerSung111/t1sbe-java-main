package com.tripleonetech.sbe.report;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.TimeRangeForm;

@Mapper
public interface SummaryReportMapper {
    
    List<SummaryReportView> query(@Param("queryForm") TimeRangeForm queryForm, @Param("currency") SiteCurrencyEnum currency,
            @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);
}
