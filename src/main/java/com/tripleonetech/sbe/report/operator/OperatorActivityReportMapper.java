package com.tripleonetech.sbe.report.operator;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OperatorActivityReportMapper {

    void insert(OperatorActivityReport operatorActivityReport);

    OperatorActivityReport get(Long id);

    List<OperatorActivityReportView> getReport(
            @Param("query") OperatorActivityReportQueryForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);
}
