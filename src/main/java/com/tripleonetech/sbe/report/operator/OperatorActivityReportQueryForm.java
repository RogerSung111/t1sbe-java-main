package com.tripleonetech.sbe.report.operator;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;

import io.swagger.annotations.ApiModelProperty;

public class OperatorActivityReportQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Operator username")
    private String username;
    @ApiModelProperty(value = "Operator activity")
    private String invoke;
    @ApiModelProperty(value = "POST or GET")
    private String httpMethod;
    @ApiModelProperty(value = "api uri path")
    private String requestUri;
    @ApiModelProperty(value = "Operator device: UNKNOWN(0), MANUAL(1), COMPUTER_HTML(2), MOBILE_HTML(3), APP_IOS(4), APP_ANDROID(5), TABLET_HTML(6)", allowableValues = "0,1,2,3,4,5,6")
    private Integer device;
    @ApiModelProperty(value = "Return code")
    private Integer returnCode;
    @ApiModelProperty(value = "Operator IP")
    private String ip;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "Begin time of creation time search")
    private LocalDateTime createdAtStart;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "End time of creation time search")
    private LocalDateTime createdAtEnd;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInvoke() {
        return invoke;
    }

    public void setInvoke(String invoke) {
        this.invoke = invoke;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public Integer getDevice() {
        return device;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not show on swagger UI")
    public DeviceTypeEnum getDeviceEnum() {
        return Objects.isNull(getDevice()) ? null : DeviceTypeEnum.valueOf(getDevice());
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStartLd(LocalDateTime createdAtStart) {
        this.createdAtStart = createdAtStart;
    }

    public void setCreatedAtStartFrom(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEndLd(LocalDateTime createdAtEnd) {
        this.createdAtEnd = createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
}
