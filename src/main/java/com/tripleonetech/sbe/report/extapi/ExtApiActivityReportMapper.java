package com.tripleonetech.sbe.report.extapi;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ExtApiActivityReportMapper {

    int insert(ExtApiActivityReport record);

    ExtApiActivityReport get(Long id);

    List<ExtApiActivityReportView> getReport(
            @Param("query") ExtApiActivityReportQueryForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

}