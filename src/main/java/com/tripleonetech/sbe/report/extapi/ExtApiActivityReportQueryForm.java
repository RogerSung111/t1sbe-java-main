package com.tripleonetech.sbe.report.extapi;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;

import io.swagger.annotations.ApiModelProperty;

public class ExtApiActivityReportQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Player ID")
    private Integer playerCredentialId;
    @ApiModelProperty(hidden = true)
    private Integer playerId;
    @ApiModelProperty(value = "Player username")
    private String username;
    @ApiModelProperty(value = "Return code")
    private Integer returnCode;
    @ApiModelProperty(value = "Ext API type: AUTH(0), LAUNCH_GAME(1), DEPOSIT(2), WITHDRAW(3)")
    private Integer extApiType;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "Begin time of creation time search")
    private LocalDateTime createdAtStart;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @ApiModelProperty(value = "End time of creation time search")
    private LocalDateTime createdAtEnd;

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public Integer getExtApiType() {
        return extApiType;
    }

    public void setExtApiType(Integer extApiType) {
        this.extApiType = extApiType;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStartLd(LocalDateTime createdAtStart) {
        this.createdAtStart = createdAtStart;
    }

    public void setCreatedAtStartFrom(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEndLd(LocalDateTime createdAtEnd) {
        this.createdAtEnd = createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
}
