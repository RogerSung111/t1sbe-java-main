package com.tripleonetech.sbe.report.extapi;

import java.time.LocalDateTime;

import com.tripleonetech.sbe.aspect.ExtApiTypeEnum;

public class ExtApiActivityReportView {
    private Long id;

    private Integer playerId;

    private Integer playerCredentialId;

    private String username;

    private ExtApiTypeEnum extApiType;

    private String invoke;

    private Integer returnCode;

    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    public ExtApiTypeEnum getExtApiType() {
        return extApiType;
    }

    public void setExtApiType(ExtApiTypeEnum extApiType) {
        this.extApiType = extApiType;
    }

    public String getInvoke() {
        return invoke;
    }

    public void setInvoke(String invoke) {
        this.invoke = invoke == null ? null : invoke.trim();
    }


    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}