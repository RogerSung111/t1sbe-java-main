package com.tripleonetech.sbe.player;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;

public class PlayerWithdrawConditionView {

    private String transType;
    private BigDecimal wallet;
    private String promoRule;
    private String currency;
    private BigDecimal bounce;
    private LocalDateTime betAt;
    private String withdrawalCondition;
    private String remark;
    private BigDecimal depositAmount;
    private Integer status;

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public BigDecimal getWallet() {
        return wallet;
    }

    public void setWallet(BigDecimal wallet) {
        this.wallet = wallet;
    }

    public String getPromoRule() {
        return promoRule;
    }

    public void setPromoRule(String promoRule) {
        this.promoRule = promoRule;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getBounce() {
        return bounce;
    }

    public void setBounce(BigDecimal bounce) {
        this.bounce = bounce;
    }

    public ZonedDateTime getBetAt() {
        return DateUtils.localToZoned(betAt);
    }

    public void setBetAt(LocalDateTime betAt) {
        this.betAt = betAt;
    }

    public String getWithdrawalCondition() {
        return withdrawalCondition;
    }

    public void setWithdrawalCondition(String withdrawalCondition) {
        this.withdrawalCondition = withdrawalCondition;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
