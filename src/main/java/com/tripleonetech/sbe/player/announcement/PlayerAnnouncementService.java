package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.common.TagFilteredUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlayerAnnouncementService {
    @Autowired
    private PlayerAnnouncementMapper announcementMapper;
    @Autowired
    private PlayerAnnouncementPlayerTagMapper announcementPlayerTagMapper;

    public int create(PlayerAnnouncementForm playerAnnouncement) {
        PlayerAnnouncement announcement = PlayerAnnouncement.from(playerAnnouncement);
        int insertedRows = announcementMapper.insert(announcement);
        if (insertedRows > 0) {
            List<PlayerAnnouncementPlayerTag> tags = PlayerAnnouncementPlayerTag.from(playerAnnouncement, announcement.getId());
            if(tags.size() > 0) {
                announcementPlayerTagMapper.insert(tags);
            }
        }
        return announcement.getId();
    }

    public int update(PlayerAnnouncementForm playerAnnouncement) {
        PlayerAnnouncement announcement = PlayerAnnouncement.from(playerAnnouncement);
        int updatedRows = announcementMapper.update(announcement);
        if (updatedRows > 0) {
            announcementPlayerTagMapper.deleteByAnnouncementId(playerAnnouncement.getId());
            List<PlayerAnnouncementPlayerTag> tags = PlayerAnnouncementPlayerTag.from(playerAnnouncement, announcement.getId());
            if(tags.size() > 0) {
                announcementPlayerTagMapper.insert(tags);
            }
        }
        return updatedRows;
    }

    public void delete(int id) {
        announcementMapper.delete(id);
    }

    // Query all available player announcement
    public List<PlayerAnnouncementPlayerView> queryAvailableNoLogin(PageQueryForm form) {
        return queryAvailable(null, form);
    }

    // Query all available player announcement for given player
    public List<PlayerAnnouncementPlayerView> queryAvailable(Player player, PageQueryForm form) {
        Set<Integer> availableIds = getAvailableIdsForPlayer(player);
        if(availableIds.size() == 0){
            return new ArrayList<>();
        }
        return announcementMapper.queryByIds(availableIds, form, form.getPage(), form.getLimit());
    }

    // Returns one announcement available to the player, returns null if announcement does not exist or is not accessible by the player
    public PlayerAnnouncementPlayerDetailView getAvailable(Player player, int id) {
        Set<Integer> availableIds = getAvailableIdsForPlayer(player);
        Optional<Integer> announcement = availableIds.stream().filter(e -> e == id).findFirst();
        return announcement.map(i -> announcementMapper.getPlayerView(i)).orElse(null);
    }

    private Set<Integer> getAvailableIdsForPlayer(Player player) {
        List<PlayerAnnouncementTagFiltered> announcementIds = announcementMapper.queryAvailable(LocalDateTime.now());
        Map<Integer, PlayerAnnouncementTagFiltered> tagFilteredAnnouncements =
                announcementIds.stream().collect(Collectors.toMap(PlayerAnnouncementTagFiltered::getId, e -> e));

        List<Integer> tagIds = player != null ? player.getTagIds() : null;
        Integer groupId = player != null ? player.getGroupId() : null;
        return TagFilteredUtils.filterAvailableIds(tagFilteredAnnouncements, tagIds, groupId);
    }

}
