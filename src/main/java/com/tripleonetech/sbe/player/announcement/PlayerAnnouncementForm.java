package com.tripleonetech.sbe.player.announcement;

import com.sun.istack.NotNull;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.TagFilteredEntity;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class PlayerAnnouncementForm extends TagFilteredEntity {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @NotNull
    @ApiModelProperty(value = "Announcement title", required = true)
    private String title;
    @NotNull
    @ApiModelProperty(value = "Announcement content", required = true)
    private String content;
    @NotNull
    @ApiModelProperty(value = "Announcement valid from", required = true)
    private LocalDateTime startAt;
    @ApiModelProperty(value = "Announcement valid until")
    private LocalDateTime endAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(ZonedDateTime startAt) {
        this.startAt = DateUtils.zonedToLocal(startAt);
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public void setEndAt(ZonedDateTime endAt) {
        this.endAt = DateUtils.zonedToLocal(endAt);
    }
}
