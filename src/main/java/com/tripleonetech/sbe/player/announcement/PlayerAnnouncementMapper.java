package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Mapper
public interface PlayerAnnouncementMapper {

    PlayerAnnouncement get(int id);

    PlayerAnnouncementPlayerDetailView getPlayerView(int id);

    PlayerAnnouncementDetailView getDetail(int id);

    List<PlayerAnnouncementView> query(@Param("queryForm") PlayerAnnouncementQueryForm queryForm,
                                       @Param("pageNum") Integer pageNum,
                                       @Param("pageSize") Integer pageSize);

    // These two together provides available announcements for a given playerw
    List<PlayerAnnouncementTagFiltered> queryAvailable(@Param("currentTime") LocalDateTime currentTime);
    List<PlayerAnnouncementPlayerView> queryByIds(@Param("ids") Set<Integer> ids,
                                                  @Param("queryForm") PageQueryForm queryForm,
                                                  @Param("pageNum") Integer pageNum,
                                                  @Param("pageSize") Integer pageSize);


    int insert(PlayerAnnouncement form);

    int update(PlayerAnnouncement form);

    void delete(int id);
}
