package com.tripleonetech.sbe.player.announcement;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class PlayerAnnouncementPlayerView {
    @ApiModelProperty(value = "ID of this announcement")
    private Integer id;
    @ApiModelProperty(value = "Announcement title")
    private String title;
    @ApiModelProperty(value = "Announcement valid from")
    private LocalDateTime startAt;
    @ApiModelProperty(value = "Announcement valid until")
    private LocalDateTime endAt;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
