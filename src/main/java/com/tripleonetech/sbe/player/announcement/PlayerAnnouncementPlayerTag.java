package com.tripleonetech.sbe.player.announcement;

import java.util.ArrayList;
import java.util.List;

public class PlayerAnnouncementPlayerTag {
    private Integer announcementId;
    private Integer playerTagId;

    public Integer getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(Integer announcementId) {
        this.announcementId = announcementId;
    }

    public Integer getPlayerTagId() {
        return playerTagId;
    }

    public void setPlayerTagId(Integer playerTagId) {
        this.playerTagId = playerTagId;
    }

    public static PlayerAnnouncementPlayerTag create(Integer announcementId, Integer playerTagId) {
        PlayerAnnouncementPlayerTag item = new PlayerAnnouncementPlayerTag();
        item.announcementId = announcementId;
        item.playerTagId = playerTagId;
        return item;
    }

    public static List<PlayerAnnouncementPlayerTag> from(PlayerAnnouncementForm form, int announcementId) {
        List<PlayerAnnouncementPlayerTag> items = new ArrayList<>();
        form.getPlayerTagIdsAndGroupIds().stream().forEach(tagId -> {
            items.add(PlayerAnnouncementPlayerTag.create(announcementId, tagId));
        });
        return items;
    }
}