package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.TagFilteredView;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class PlayerAnnouncementView extends TagFilteredView {
    @ApiModelProperty(value = "ID of this announcement")
    private Integer id;
    @ApiModelProperty(value = "Announcement title")
    private String title;
    @ApiModelProperty(value = "Announcement valid from")
    private LocalDateTime startAt;
    @ApiModelProperty(value = "Announcement valid until")
    private LocalDateTime endAt;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(ZonedDateTime startAt) {
        this.startAt = DateUtils.zonedToLocal(startAt);
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public void setEndAt(ZonedDateTime endAt) {
        this.endAt = DateUtils.zonedToLocal(endAt);
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
