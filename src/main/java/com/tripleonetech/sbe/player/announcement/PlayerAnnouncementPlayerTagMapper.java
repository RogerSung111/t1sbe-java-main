package com.tripleonetech.sbe.player.announcement;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlayerAnnouncementPlayerTagMapper {

    int insert(List<PlayerAnnouncementPlayerTag> record);

    void deleteByAnnouncementId(int announcementId);
}
