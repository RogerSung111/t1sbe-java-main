package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.common.model.TagFilteredEntity;

public class PlayerAnnouncementTagFiltered extends TagFilteredEntity {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
