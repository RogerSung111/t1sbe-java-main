package com.tripleonetech.sbe.player.announcement;

import io.swagger.annotations.ApiModelProperty;

public class PlayerAnnouncementDetailView extends PlayerAnnouncementView {

    @ApiModelProperty(value = "Announcement content")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
