package com.tripleonetech.sbe.player.announcement;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class PlayerAnnouncementQueryForm extends PageQueryForm {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(value = "Search by announcement title", required = false)
    private String title;
    @ApiModelProperty(value = "Search by Announcement content", required = false)
    private String content;
    @ApiModelProperty(value = "Search by PlayerTagId id", required = false)
    private List<Integer> tags;
    @ApiModelProperty(value = "Search by date (announcement should be applicable to this date)", required = false)
    private LocalDateTime messageForDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public LocalDateTime getMessageForDate() {
        return messageForDate;
    }

    public void setMessageForDate(ZonedDateTime messageForDate) {
        this.messageForDate = DateUtils.zonedToLocal(messageForDate);
    }
}
