package com.tripleonetech.sbe.player;

import org.springframework.context.ApplicationEvent;

public class VerifyIdentitySuccessEvent extends ApplicationEvent {
    private Integer playerId;

    public VerifyIdentitySuccessEvent(Object source, Integer playerId) {
        super(source);

        this.playerId = playerId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
