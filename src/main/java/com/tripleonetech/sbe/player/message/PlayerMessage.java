package com.tripleonetech.sbe.player.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class PlayerMessage {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(hidden = true)
    private Integer threadId;
    @ApiModelProperty(hidden = true, value = "Player id")
    private Integer playerId;
    @ApiModelProperty(hidden = true)
    private Integer operatorId;
    @ApiModelProperty(hidden = true)
    private String operatorUsername;
    @ApiModelProperty(hidden = true)
    private String playerUsername;
    @NotBlank
    @ApiModelProperty(value = "Message title", required = true)
    private String subject;
    @NotBlank
    @ApiModelProperty(value = "Message content", required = true)
    private String content;
    @ApiModelProperty(hidden=true, value = "True means read")
    private boolean read;
    @ApiModelProperty(hidden = true, value = "Sender of this message: operator(0), player(1)")
    private Byte sender;
    @ApiModelProperty(hidden = true, value = "False by default")
    private boolean deleted;
    @ApiModelProperty(hidden = true, value = "Whether this is a system message. A system message is sent by a system event and cannot be replied to. Example of system message could be a message for promotion.")
    private boolean system;
    @ApiModelProperty(hidden = true)
    private LocalDateTime createdAt;
    @ApiModelProperty(hidden = true)
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorUsername() {
        return operatorUsername;
    }

    public void setOperatorUsername(String operatorUsername) {
        this.operatorUsername = operatorUsername;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Byte getSender() {
        return sender;
    }

    public void setSender(Byte sender) {
        this.sender = sender;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true, value = "Derived property, should not show")
    public PlayerMessageSenderEnum getSenderEnum() {
        return PlayerMessageSenderEnum.valueOf(this.sender);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "PlayerMessage{" +
                "id=" + id +
                ", playerId=" + playerId +
                ", operatorId=" + operatorId +
                ", operatorName='" + operatorUsername + '\'' +
                ", playerUsername='" + playerUsername + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", sender=" + sender +
                ", deleted=" + deleted +
                ", isSystemMessage=" + system +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

}
