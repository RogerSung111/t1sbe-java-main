package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.common.BaseNameEnum;

public class PlayerMessageTemplate {

    public enum Type implements BaseNameEnum {
        DEPOSIT,
        WITHDRAW,
        PROMO;

        @Override
        public String getName() {
            return this.toString();
        }
    }

    public final static class Deposit {
        public enum Properties implements PlayerMessageTemplateProperty {
            AMOUNT,
            PLAYER_NAME,
            DEPOSIT_TIME// TODO need to define more
        }
    }

    public final static class Withdraw {
        public enum Properties implements PlayerMessageTemplateProperty {
            AMOUNT,
            PLAYER_NAME,
            WITHDRAW_TIME// TODO need to define
        }
    }

    public final static class Promo {
        public enum Properties implements PlayerMessageTemplateProperty {
            AMOUNT // TODO need to define
        }
    }

    public interface PlayerMessageTemplateProperty {}

    public static PlayerMessageTemplateProperty[] getAvailableProperties(PlayerMessageTemplate.Type type) {
        switch (type) {
            case DEPOSIT:
                return Deposit.Properties.values();
            case WITHDRAW:
                return Withdraw.Properties.values();
            case PROMO:
                return Promo.Properties.values();
            default:
                return null;
        }
    }


}
