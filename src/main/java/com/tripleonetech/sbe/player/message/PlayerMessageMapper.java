package com.tripleonetech.sbe.player.message;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PlayerMessageMapper {

    PlayerMessage get(int id);

    int getUnreadCount(int playerId);

    void newThread(int id);

    int insert(PlayerMessage record);

    List<PlayerMessageView> query(@Param("queryForm") PlayerMessageQueryForm queryForm,
                                  @Param("pageNum") Integer pageNum,
                                  @Param("pageSize") Integer pageSize);

    // Note: this mapper does not contain messages under thread
    List<PlayerMessageThreadListView> queryThreads(@Param("queryForm") PlayerMessageThreadListQueryForm queryForm,
                                                   @Param("pageNum") Integer pageNum,
                                                   @Param("pageSize") Integer pageSize);

    // Note: this mapper does not provide pagination, but contains messages under thread
    List<PlayerMessageThreadView> queryThreadsWithMessages(@Param("queryForm") PlayerMessageThreadQueryForm queryForm);

    int setDeleted(int id);

    int setBatchDeleted(@Param("ids") List<Integer> ids);

    int setSystemRead(@Param("playerId")int playerId);

    int setRead(@Param("id")int id, @Param("playerId")int playerId);

    int setBatchRead(@Param("playerId")int playerId);

    int setThreadRead(@Param("threadId")int threadId, @Param("playerId")int playerId);

    int getUnreadCountOfOperator();
}
