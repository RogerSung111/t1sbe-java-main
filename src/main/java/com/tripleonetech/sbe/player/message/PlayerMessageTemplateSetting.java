package com.tripleonetech.sbe.player.message;


import java.time.LocalDateTime;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.*;

public class PlayerMessageTemplateSetting {

    private Integer id;

    private Type playerMessageTemplateType;

    private String subject;

    private String content;

    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Type getPlayerMessageTemplateType() {
        return playerMessageTemplateType;
    }

    public void setPlayerMessageTemplateType(Type playerMessageTemplateType) {
        this.playerMessageTemplateType = playerMessageTemplateType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

}
