package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.common.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

// Used in api profile to query player messages
public class PlayerMessageThreadQueryForm {
    @ApiModelProperty(hidden = true)
    private Integer playerId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(value = "Message sent time FROM", required = false)
    private LocalDateTime createdAtStart = LocalDateTime.now().minusMonths(1);
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(value = "Message sent time TO", required = false)
    private LocalDateTime createdAtEnd = LocalDateTime.now();
    @ApiModelProperty(value = "Thread ID", required = false)
    private Integer threadId;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }
}
