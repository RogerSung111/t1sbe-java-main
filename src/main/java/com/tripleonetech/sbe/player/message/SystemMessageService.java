package com.tripleonetech.sbe.player.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.*;

@Service
public class SystemMessageService {

    @Autowired
    private PlayerMessageTemplateSettingMapper playerMessageTemplateSettingMapper;

    @Autowired
    PlayerMessageService playerMessageService;


    public void sendSystemMessage(Type type, Map<PlayerMessageTemplateProperty, Object> params, List<Integer> playerIds) {
        PlayerMessageTemplateSetting templateSetting = playerMessageTemplateSettingMapper.getByType(type);

        String subject = replaceAllWithParams(templateSetting.getSubject(), params);
        String content = replaceAllWithParams(templateSetting.getContent(), params);

        playerIds.forEach(playerId ->{
            PlayerMessage systemMessage = new PlayerMessage();
            systemMessage.setSubject(subject);
            systemMessage.setContent(content);
            systemMessage.setSystem(true);
            systemMessage.setPlayerId(playerId);
            playerMessageService.sendMessage(systemMessage);
        });
    }

    private String replaceAllWithParams(String content, Map<PlayerMessageTemplateProperty, Object> params) {
        for (Map.Entry<PlayerMessageTemplateProperty, Object> entry : params.entrySet()) {
            String propertyName = entry.getKey().toString();
            content = content.replaceAll("\\["+propertyName+"\\]",  entry.getValue().toString());
        }

        return content;
    }
}
