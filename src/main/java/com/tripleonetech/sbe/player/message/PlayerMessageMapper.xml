<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.tripleonetech.sbe.player.message.PlayerMessageMapper">
    <resultMap id="BaseResultMap" type="com.tripleonetech.sbe.player.message.PlayerMessageView">
        <id column="id" jdbcType="INTEGER" property="id"/>
        <result column="player_id" jdbcType="INTEGER" property="playerId"/>
        <result column="operator_id" jdbcType="INTEGER" property="operatorId"/>
        <result column="operatorUsername" jdbcType="VARCHAR" property="operatorUsername"/>
        <result column="playerUsername" jdbcType="VARCHAR" property="playerUsername"/>
        <result column="subject" jdbcType="VARCHAR" property="subject"/>
        <result column="content" jdbcType="LONGVARCHAR" property="content"/>
        <result column="read" jdbcType="BIT" property="read"/>
        <result column="sender" jdbcType="TINYINT" property="sender"/>
        <result column="deleted" jdbcType="BIT" property="deleted"/>
        <result column="system" jdbcType="BIT" property="system"/>
        <result column="created_at" jdbcType="TIMESTAMP" property="createdAt"/>
        <result column="updated_at" jdbcType="TIMESTAMP" property="updatedAt"/>
    </resultMap>

    <resultMap id="ThreadListResultMap" type="com.tripleonetech.sbe.player.message.PlayerMessageThreadListView" autoMapping="true">
        <id column="thread_id" jdbcType="INTEGER" property="threadId"/>
        <association property="player" javaType="com.tripleonetech.sbe.common.web.IdNameView">
            <id column="player_id" property="id" />
            <result column="player_name" property="name" />
        </association>
        <association property="operator" javaType="com.tripleonetech.sbe.common.web.IdNameView">
            <id column="operator_id" property="id" />
            <result column="operator_name" property="name" />
        </association>
    </resultMap>

    <resultMap id="ThreadResultMap" type="com.tripleonetech.sbe.player.message.PlayerMessageThreadView">
        <id column="thread_id" jdbcType="INTEGER" property="threadId"/>
        <collection property="messages" javaType="List" ofType="com.tripleonetech.sbe.player.message.PlayerMessageView" autoMapping="true">
            <id column="id" property="id" />
        </collection>
    </resultMap>

    <sql id="Base_Column_List">
    pm.id, pm.thread_id, pm.player_id, pm.operator_id, o.username AS operatorUsername, p.username AS playerUsername, pm.subject,
    pm.content, pm.read, pm.sender, pm.deleted, pm.`system`, pm.created_at, pm.updated_at
    </sql>

    <select id="get" parameterType="int" resultType="com.tripleonetech.sbe.player.message.PlayerMessage">
        SELECT *
        FROM player_message
        WHERE id = #{id}
    </select>

    <select id="getUnreadCount" parameterType="int" resultType="int">
        SELECT COUNT(*)
        FROM player_message
        WHERE deleted = 0 AND player_id = #{playerId} AND `read` = 0
    </select>

    <select id="query" resultMap="BaseResultMap">
        SELECT
            <include refid="Base_Column_List"/>
        FROM player_message pm
            INNER JOIN player p ON pm.player_id = p.id
            LEFT JOIN operator o ON pm.operator_id = o.id
        <where>
            <if test="queryForm.id != null and queryForm.id > 0">
                AND pm.id = #{queryForm.id,jdbcType=INTEGER}
            </if>
            <if test="queryForm.playerId != null and queryForm.playerId > 0">
                AND pm.player_id = #{queryForm.playerId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.operatorId != null and queryForm.operatorId > 0">
                AND pm.operator_id = #{queryForm.operatorId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.threadId != null and queryForm.threadId > 0">
                AND pm.thread_id = #{queryForm.threadId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.operatorUsername != null and queryForm.operatorUsername != ''">
                AND o.username LIKE CONCAT('%', #{queryForm.operatorUsername,jdbcType=VARCHAR}, '%')
            </if>
            <if test="queryForm.playerUsername != null and queryForm.playerUsername != ''">
                AND p.username LIKE CONCAT('%', #{queryForm.playerUsername,jdbcType=VARCHAR}, '%')
            </if>
            <if test="queryForm.subject != null and queryForm.subject != ''">
                AND pm.subject LIKE CONCAT('%', #{queryForm.subject,jdbcType=VARCHAR}, '%')
            </if>
            <if test="queryForm.sender != null and queryForm.sender >= 0">
                AND pm.sender = #{queryForm.sender,jdbcType=TINYINT}
            </if>
            <if test="queryForm.deleted != null">
                AND pm.deleted = #{queryForm.deleted,jdbcType=BIT}
            </if>
            <if test="queryForm.system != null">
                AND pm.`system` = #{queryForm.system,jdbcType=BIT}
            </if>
            <if test="queryForm.createdAtStart != null">
                AND pm.created_at <![CDATA[ >= ]]> #{queryForm.createdAtStart,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.createdAtEnd != null">
                AND pm.created_at <![CDATA[ <= ]]> #{queryForm.createdAtEnd,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.updatedAtStart != null">
                AND pm.updated_at <![CDATA[ >= ]]> #{queryForm.updatedAtStart,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.updatedAtEnd != null">
                AND pm.updated_at <![CDATA[ <= ]]> #{queryForm.updatedAtEnd,jdbcType=TIMESTAMP}
            </if>
        </where>
        ORDER BY
        <choose>
            <when test='"updatedAt".equals(queryForm.sort)'>
                pm.updated_at
            </when>
            <otherwise>
                pm.id
            </otherwise>
        </choose>
        <if test='!queryForm.isAsc'>
            DESC
        </if>
    </select>

    <select id="queryThreads" resultMap="ThreadListResultMap">
    SELECT
        thread_id,
        (SELECT COUNT(*) FROM player_message pm_inner WHERE pm_inner.thread_id = player_message.thread_id) AS message_count,
        player_id,
        operator_id,
        player.username as player_name,
        operator.username as operator_name,
        `subject`,
        sender,
        player_message.created_at,
        (SELECT MAX(updated_at) FROM player_message pm_inner2 WHERE pm_inner2.thread_id = player_message.thread_id) AS updated_at
    FROM
        player_message
        INNER JOIN player ON player_message.player_id = player.id
        LEFT JOIN operator ON player_message.operator_id = operator.id
        <where>
            <if test="queryForm.playerId != null and queryForm.playerId > 0">
                AND player_id = #{queryForm.playerId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.operatorId != null and queryForm.operatorId > 0">
                AND operator_id = #{queryForm.operatorId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.threadId != null">
                AND thread_id = #{queryForm.threadId}
            </if>
            <if test="queryForm.createdAtStart != null">
                AND player_message.created_at <![CDATA[ >= ]]> #{queryForm.createdAtStart,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.createdAtEnd != null">
                AND player_message.created_at <![CDATA[ <= ]]> #{queryForm.createdAtEnd,jdbcType=TIMESTAMP}
            </if>
            AND deleted = 0 AND `system` = 0 AND player_message.thread_id = player_message.id
        </where>
    ORDER BY
        updated_at DESC
    </select>

    <select id="queryThreadsWithMessages" resultMap="ThreadResultMap">
    SELECT
        <include refid="Base_Column_List"/>
    FROM player_message pm
        INNER JOIN player p ON pm.player_id = p.id
        LEFT JOIN operator o ON pm.operator_id = o.id
    WHERE (
        <if test="queryForm.threadId == null">
        thread_id IS NULL OR
        </if>
        thread_id IN (
        SELECT DISTINCT thread_id
        FROM
            player_message
        <where>
            <if test="queryForm.playerId != null and queryForm.playerId > 0">
                AND player_id = #{queryForm.playerId,jdbcType=INTEGER}
            </if>
            <if test="queryForm.createdAtStart != null">
                AND created_at <![CDATA[ >= ]]> #{queryForm.createdAtStart,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.createdAtEnd != null">
                AND created_at <![CDATA[ <= ]]> #{queryForm.createdAtEnd,jdbcType=TIMESTAMP}
            </if>
            <if test="queryForm.threadId != null">
                AND thread_id = #{queryForm.threadId}
            </if>
            AND deleted = 0
        </where>
        ))
        AND pm.player_id = #{queryForm.playerId,jdbcType=INTEGER}
        AND deleted = 0
    ORDER BY created_at DESC, thread_id DESC, id DESC
    </select>

    <insert id="insert" parameterType="com.tripleonetech.sbe.player.message.PlayerMessage"
            useGeneratedKeys="true" keyProperty="id">
        insert into player_message
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="threadId != null">
                thread_id,
            </if>
            <if test="playerId != null">
                player_id,
            </if>
            <if test="operatorId != null">
                operator_id,
            </if>
            <if test="subject != null">
                subject,
            </if>
            <if test="content != null">
                content,
            </if>
            <if test="sender != null">
                sender,
            </if>
            <if test="read != null">
                `read`,
            </if>
            <if test="deleted != null">
                deleted,
            </if>
            <if test="system != null">
                `system`,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="threadId != null">
                #{threadId,jdbcType=INTEGER},
            </if>
            <if test="playerId != null">
                #{playerId,jdbcType=INTEGER},
            </if>
            <if test="operatorId != null">
                #{operatorId,jdbcType=INTEGER},
            </if>
            <if test="subject != null">
                #{subject,jdbcType=VARCHAR},
            </if>
            <if test="content != null">
                #{content,jdbcType=LONGVARCHAR},
            </if>
            <if test="sender != null">
                #{sender,jdbcType=TINYINT},
            </if>
            <if test="read != null">
                #{read,jdbcType=BIT},
            </if>
            <if test="deleted != null">
                #{deleted,jdbcType=BIT},
            </if>
            <if test="system != null">
                #{system,jdbcType=BIT},
            </if>
        </trim>
    </insert>

    <update id="setDeleted">
        update player_message
        SET deleted = 1
        where id = #{id}
    </update>

    <update id="setBatchDeleted">
        update player_message
        SET deleted = 1
        WHERE id IN
        <foreach item="id" collection="ids" separator="," open="(" close=")">
            #{id}
        </foreach>
    </update>

    <update id="setSystemRead">
        UPDATE player_message SET `read` = 1 WHERE player_id = #{playerId} AND `system` = 1
    </update>

    <update id="setRead">
        UPDATE player_message SET `read` = 1 WHERE id = #{id} AND player_id = #{playerId}
    </update>

    <update id="setBatchRead">
        UPDATE player_message SET `read` = 1
        WHERE player_id = #{playerId}
    </update>

    <update id="setThreadRead">
        UPDATE player_message SET `read` = 1 WHERE thread_id = #{threadId} AND player_id = #{playerId}
    </update>

    <update id="newThread">
        UPDATE player_message SET thread_id = id WHERE id = #{id}
    </update>

    <select id="getUnreadCountOfOperator" resultType="java.lang.Integer">
        SELECT COUNT(1)
        FROM
            player_message
        WHERE
            deleted = 0 AND `read` = 0 AND sender = 1
    </select>
</mapper>


