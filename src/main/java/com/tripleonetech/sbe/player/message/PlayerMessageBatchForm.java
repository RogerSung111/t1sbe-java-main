package com.tripleonetech.sbe.player.message;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PlayerMessageBatchForm {

    @NotEmpty
    @ApiModelProperty(value = "Player id", required = true)
    private List<Integer> playerId;

    @NotNull
    @ApiModelProperty(hidden = true)
    private Integer operatorId;

    @NotEmpty
    @ApiModelProperty(value = "Message title", required = true)
    private String subject;

    @NotEmpty
    @ApiModelProperty(value = "Message content", required = true)
    private String content;

    @ApiModelProperty(hidden = true, value = "Sender flag admin(0)")
    private final byte flag = 0;

    @ApiModelProperty(hidden = true)
    private Byte isSystemMessage;

    public List<Integer> getPlayerId() {
        return playerId;
    }

    public void setPlayerId(List<Integer> playerId) {
        this.playerId = playerId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public byte getFlag() {
        return flag;
    }

    public Byte getIsSystemMessage() {
        return isSystemMessage;
    }

    public void setIsSystemMessage(Byte isSystemMessage) {
        this.isSystemMessage = isSystemMessage;
    }
}
