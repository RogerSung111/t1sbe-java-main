package com.tripleonetech.sbe.player.message;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerMessageService {

    @Autowired
    private PlayerMessageMapper mapper;

    public void sendMessage(PlayerMessage message) {
        // Determine sender only if not given
        if (message.getSender() == null) {
            if (message.getOperatorId() != null && message.getPlayerId() != null) {
                // set sender flag as admin
                message.setSender((byte) 0);
            } else {
                // set sender flag as player
                message.setSender((byte) 1);
            }
        }

        mapper.insert(message);
    }

    public PlayerMessage playerReplyMessage(int messageId, String content) {
        PlayerMessage replyingToMessage = mapper.get(messageId);
        PlayerMessage newMessage = new PlayerMessage();

        BeanUtils.copyProperties(replyingToMessage, newMessage);
        newMessage.setSender((byte)PlayerMessageSenderEnum.PLAYER.getCode());
        newMessage.setRead(true); // set read to true when sender is player
        newMessage.setContent(content);

        this.sendMessage(newMessage);
        return newMessage;
    }

    public PlayerMessage operatorReplyMessage(int messageId, int operatorId, String content) {
        PlayerMessage replyingToMessage = mapper.get(messageId);
        PlayerMessage newMessage = new PlayerMessage();

        BeanUtils.copyProperties(replyingToMessage, newMessage);
        newMessage.setSender((byte)PlayerMessageSenderEnum.OPERATOR.getCode());
        newMessage.setOperatorId(operatorId);
        newMessage.setContent(content);

        this.sendMessage(newMessage);
        return newMessage;
    }

    public void newThread(int messageId) {
        mapper.newThread(messageId);
    }

    public List<PlayerMessageView> listMessage(PlayerMessageQueryForm queryForm) {
        return mapper.query(queryForm, queryForm.getPage(), queryForm.getLimit());
    }

    public List<PlayerMessageThreadListView> listThread(PlayerMessageThreadListQueryForm queryForm) {
        return mapper.queryThreads(queryForm, queryForm.getPage(), queryForm.getLimit());
    }

    public int deleteMessage(int msgId) {
        return mapper.setDeleted(msgId);
    }

    public int batchDeleteMessage(List<Integer> msgIds) {
        return mapper.setBatchDeleted(msgIds);
    }
}
