package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.web.IdNameView;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

// This model enables BO to display all threads in a list
public class PlayerMessageThreadListView {
    private int threadId;
    private int messageCount;
    private IdNameView player;
    private IdNameView operator;
    private String subject;
    private Byte sender;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public IdNameView getPlayer() {
        return player;
    }

    public void setPlayer(IdNameView player) {
        this.player = player;
    }

    public IdNameView getOperator() {
        return operator;
    }

    public void setOperator(IdNameView operator) {
        this.operator = operator;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Byte getSender() {
        return sender;
    }

    public void setSender(Byte sender) {
        this.sender = sender;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(updatedAt);
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
