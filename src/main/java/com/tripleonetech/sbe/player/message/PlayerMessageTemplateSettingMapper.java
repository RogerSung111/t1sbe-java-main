package com.tripleonetech.sbe.player.message;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PlayerMessageTemplateSettingMapper {

    PlayerMessageTemplateSetting get(Integer templateId);

    PlayerMessageTemplateSetting getByType(PlayerMessageTemplate.Type type);

    List<PlayerMessageTemplateSetting> listAll();

    int update(PlayerMessageTemplateSetting record);

}
