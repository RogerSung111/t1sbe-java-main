package com.tripleonetech.sbe.player.message;

import java.util.List;

// Wraps messages into a thread. Contains at least one message.
public class PlayerMessageThreadView {
    private Integer threadId;
    private List<PlayerMessageView> messages;

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public List<PlayerMessageView> getMessages() {
        return messages;
    }

    public void setMessages(List<PlayerMessageView> messages) {
        this.messages = messages;
    }

    public String getSubject() {
        return messages.get(0).getSubject();
    }

    public boolean isUnread() {
        for(PlayerMessageView message : messages){
            if(!message.isRead()) {
                return true;
            }
        }
        return false;
    }
}
