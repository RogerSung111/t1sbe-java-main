package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.player.message.PlayerMessageTemplate.PlayerMessageTemplateProperty;

import java.time.ZonedDateTime;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.*;

public class PlayerMessageTemplateSettingView {

    private Integer id;

    private Type playerMessageTemplateType;

    private String subject;

    private String content;

    private ZonedDateTime updatedAt;

    private PlayerMessageTemplateProperty[] availableProperties;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Type getPlayerMessageTemplateType() {
        return playerMessageTemplateType;
    }

    public void setPlayerMessageTemplateType(Type playerMessageTemplateType) {
        this.playerMessageTemplateType = playerMessageTemplateType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PlayerMessageTemplateProperty[] getAvailableProperties() {
        return availableProperties;
    }

    public void setAvailableProperties(PlayerMessageTemplateProperty[] availableProperties) {
        this.availableProperties = availableProperties;
    }
}
