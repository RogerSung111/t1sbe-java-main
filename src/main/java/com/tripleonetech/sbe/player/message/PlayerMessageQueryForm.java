package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class PlayerMessageQueryForm extends PageQueryForm {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(hidden = true)
    private Integer playerId;
    @ApiModelProperty(hidden = true)
    private Integer operatorId;
    @ApiModelProperty(value = "Thread ID", required = false)
    private Integer threadId;
    @ApiModelProperty(value = "Operator username", required = false)
    private String operatorUsername;
    @ApiModelProperty(value = "Player username", required = false)
    private String playerUsername;
    @ApiModelProperty(value = "Message subject", required = false)
    private String subject;
    @ApiModelProperty(value = "Sender of this message: operator(0), player(1)", required = false)
    private Byte sender;
    @ApiModelProperty(value = "Is message deleted", required = false)
    private Boolean deleted;
    @ApiModelProperty(value = "Whether this message is a system message", required = false)
    private Boolean system;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(value = "Searching by message sent time FROM", required = false)
    private LocalDateTime createdAtStart;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(value = "Searching by message sent time TO", required = false)
    private LocalDateTime createdAtEnd;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(hidden = true)
    private LocalDateTime updatedAtStart;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(hidden = true)
    private LocalDateTime updatedAtEnd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public String getOperatorUsername() {
        return operatorUsername;
    }

    public void setOperatorUsername(String operatorUsername) {
        this.operatorUsername = operatorUsername;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Byte getSender() {
        return sender;
    }

    public void setSender(Byte sender) {
        this.sender = sender;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }

    public LocalDateTime getUpdatedAtStart() {
        return updatedAtStart;
    }

    public void setUpdatedAtStart(ZonedDateTime updatedAtStart) {
        this.updatedAtStart = DateUtils.zonedToLocal(updatedAtStart);
    }

    public LocalDateTime getUpdatedAtEnd() {
        return updatedAtEnd;
    }

    public void setUpdatedAtEnd(ZonedDateTime updatedAtEnd) {
        this.updatedAtEnd = DateUtils.zonedToLocal(updatedAtEnd);
    }

    @Override public String toString() {
        return "PlayerMessageQueryForm{" +
                "id=" + id +
                ", playerId=" + playerId +
                ", operatorId=" + operatorId +
                ", operatorUsername='" + operatorUsername + '\'' +
                ", playerUsername='" + playerUsername + '\'' +
                ", subject='" + subject + '\'' +
                ", flag=" + sender +
                ", deleted=" + deleted +
                ", system=" + system +
                ", createdAtStart=" + createdAtStart +
                ", createdAtEnd=" + createdAtEnd +
                ", updatedAtStart=" + updatedAtStart +
                ", updatedAtEnd=" + updatedAtEnd +
                '}';
    }
}
