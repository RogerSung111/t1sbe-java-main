package com.tripleonetech.sbe.player.message;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PlayerMessageSenderEnum implements BaseCodeEnum {
    OPERATOR(0), PLAYER(1);

    private int code;

    PlayerMessageSenderEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static PlayerMessageSenderEnum valueOf(int code) {
        PlayerMessageSenderEnum[] enumConstants = PlayerMessageSenderEnum.values();
        for (PlayerMessageSenderEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
