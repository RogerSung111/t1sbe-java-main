package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PlayerRiskEnum implements BaseCodeEnum {
    LOW(1), MILD(3), MEDIUM(5), HIGH(7), VERY_HIGH(9);

    private static final int STEP_LENGTH = 2;
    private int code;

    PlayerRiskEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return this.code;
    }

    public static PlayerRiskEnum valueOf(Integer code) {
        PlayerRiskEnum[] enumConstants = PlayerRiskEnum.values();
        for (PlayerRiskEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }

    public PlayerRiskEnum decrease() {
        if(this.code > PlayerRiskEnum.LOW.code){
            return valueOf(this.code - STEP_LENGTH);
        }
        else {
            return PlayerRiskEnum.LOW;
        }
    }

    public PlayerRiskEnum increase() {
        if(this.code < PlayerRiskEnum.VERY_HIGH.code){
            return valueOf(this.code + STEP_LENGTH);
        }
        else {
            return PlayerRiskEnum.VERY_HIGH;
        }
    }
}
