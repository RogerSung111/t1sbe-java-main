package com.tripleonetech.sbe.player;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.CryptoUtils;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class PlayerCredential implements Serializable {
    private static final long serialVersionUID = 2612258233134477981L;
    private Integer id;
    private String username;
    private String passwordEncrypt;
    private String withdrawPasswordHash;
    private PlayerStatusEnum globalStatus;
    private Boolean passwordPending;
    private String referralCode;
    private Integer referralPlayerCredentialId;
    private String email;
    private Boolean emailVerified;
    private DeviceTypeEnum registerDevice;
    private String registerIp;
    private Integer verificationQuestionId;
    private String verificationAnswer;
    private LocalDateTime lastActivityTime;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordEncrypt() {
        return passwordEncrypt;
    }

    public void setPasswordEncrypt(String passwordEncrypt) {
        this.passwordEncrypt = passwordEncrypt;
    }
    
    public void setWithdrawPassword(String withdrawPassword) {
        this.withdrawPasswordHash = BCrypt.hashpw(withdrawPassword, BCrypt.gensalt());
    }
    
    public String getWithdrawPasswordHash() {
        return withdrawPasswordHash;
    }

    public void setWithdrawPasswordHash(String withdrawPasswordHash) {
        this.withdrawPasswordHash = withdrawPasswordHash;
    }
    
    public boolean validateWithdrawPassword(String password) {
        return BCrypt.checkpw(password, withdrawPasswordHash);
    }

    public PlayerStatusEnum getGlobalStatus() {
        return globalStatus;
    }

    public void setGlobalStatus(PlayerStatusEnum globalStatus) {
        this.globalStatus = globalStatus;
    }

    public Boolean getPasswordPending() {
        return passwordPending;
    }

    public void setPasswordPending(Boolean passwordPending) {
        this.passwordPending = passwordPending;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Integer getReferralPlayerCredentialId() {
        return referralPlayerCredentialId;
    }

    public void setReferralPlayerCredentialId(Integer referralPlayerCredentialId) {
        this.referralPlayerCredentialId = referralPlayerCredentialId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public DeviceTypeEnum getRegisterDevice() {
        return registerDevice;
    }

    public void setRegisterDevice(DeviceTypeEnum registerDevice) {
        this.registerDevice = registerDevice;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public Integer getVerificationQuestionId() {
        return verificationQuestionId;
    }

    public void setVerificationQuestionId(Integer verificationQuestionId) {
        this.verificationQuestionId = verificationQuestionId;
    }

    public String getVerificationAnswer() {
        return verificationAnswer;
    }

    public void setVerificationAnswer(String verificationAnswer) {
        this.verificationAnswer = verificationAnswer;
    }

    public LocalDateTime getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(LocalDateTime lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    public LocalDateTime getCreatedAtLocal() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(updatedAt);
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonIgnore
    @ApiModelProperty(value = "Password (Plain text) - plain password will be encrypted", required = true)
    public String getPassword() {
        return CryptoUtils.decryptPlayerPassword(getPasswordEncrypt());
    }

    public void setPassword(String password) {
        this.passwordEncrypt = CryptoUtils.encryptPlayerPassword(password);
    }

    public boolean validatePassword(String password) {
        if (StringUtils.isBlank(password)) {
            return false;
        }
        return password.equals(getPassword());
    }
    
   
}
