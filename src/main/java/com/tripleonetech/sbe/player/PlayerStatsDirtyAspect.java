package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.promo.PromoTypeEnum;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import com.tripleonetech.sbe.promo.bonus.PromoBonusMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class PlayerStatsDirtyAspect {

    @Autowired
    private PlayerStatsMapper playerStatsMapper;

    @Autowired
    private PromoBonusMapper promoBonusMapper;

    @After("execution(* com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper.insert(..)))")
    public void setWalletAmountToDirty(JoinPoint joinPoint) {
        // Get records that need to be updated
        WalletTransaction record = (WalletTransaction) joinPoint.getArgs()[0];

        // Add or update record, and set it as dirty
        if (WalletTransactionTypeEnum.DEPOSIT == record.getType()) {
            playerStatsMapper.upsertDirty(Arrays.asList(record.getPlayerId()), PlayerStatsTypeEnum.DEPOSIT_AMOUNT);
        }
        if (WalletTransactionTypeEnum.WITHDRAWAL == record.getType()) {
            playerStatsMapper.upsertDirty(Arrays.asList(record.getPlayerId()), PlayerStatsTypeEnum.WITHDRAW_AMOUNT);
        }
    }

    @After("execution(* com.tripleonetech.sbe.promo.bonus.PromoBonusMapper.insert(com.tripleonetech.sbe.promo.bonus.PromoBonus)))")
    public void setBonusAmountToDirtyWhenInsert(JoinPoint joinPoint) {
        // Get records that need to be updated
        PromoBonus record = (PromoBonus) joinPoint.getArgs()[0];
        setBonusAmountToDirty(record);
    }

    @After("execution(* com.tripleonetech.sbe.promo.bonus.PromoBonusMapper.update(com.tripleonetech.sbe.promo.bonus.PromoBonus)))")
    public void setBonusAmountToDirtyWhenUpdate(JoinPoint joinPoint) {
        // Get records that need to be updated
        PromoBonus record = (PromoBonus) joinPoint.getArgs()[0];
        setBonusAmountToDirty(record);
    }

    @After("execution(* com.tripleonetech.sbe.promo.bonus.PromoBonusMapper.updateStatus(..)))")
    public void setBonusAmountToDirtyWhenUpdateStatus(JoinPoint joinPoint) {
        // Get records that need to be updated
        Integer id = (Integer) joinPoint.getArgs()[0];
        PromoBonus record = promoBonusMapper.queryById(id);
        setBonusAmountToDirty(record);
    }

    private void setBonusAmountToDirty(PromoBonus record) {
        // Add or update record, and set it as dirty
        if (PromoTypeEnum.CASHBACK == record.getPromoType()) {
            playerStatsMapper.upsertDirty(Arrays.asList(record.getPlayerId()), PlayerStatsTypeEnum.CASHBACK_BONUS);
        }
        if (PromoTypeEnum.REFERRAL == record.getPromoType()) {
            playerStatsMapper.upsertDirty(Arrays.asList(record.getPlayerId()), PlayerStatsTypeEnum.REFERRAL_BONUS);
        }
        if (record.getPromoType().getCode() > 10) {
            playerStatsMapper.upsertDirty(Arrays.asList(record.getPlayerId()), PlayerStatsTypeEnum.CAMPAIGN_BONUS);
        }
    }

}
