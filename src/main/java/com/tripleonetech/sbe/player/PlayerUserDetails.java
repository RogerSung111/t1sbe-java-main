package com.tripleonetech.sbe.player;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class PlayerUserDetails implements UserDetails {
    private static final long serialVersionUID = 3179892706129249254L;

    private Player playerUser;

    public PlayerUserDetails(Player player) {
        this.playerUser = player;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (playerUser.getPasswordPending()) {
            return Arrays.asList(new SimpleGrantedAuthority("PASSWORD_PENDING"));
        }
        return null;
    }

    @Override
    public String getPassword() {
        return this.playerUser.getPassword();
    }

    @Override
    public String getUsername() {
        return this.playerUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        // Account expiry not implemented
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // Account lock not implemented
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // Credential expiry not implemented
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.playerUser.isEnabled();
    }

    public Player getPlayer() {
        return this.playerUser;
    }
    
    public void setPlayer(Player player) {
        this.playerUser = player;
    }

}
