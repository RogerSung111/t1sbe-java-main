package com.tripleonetech.sbe.player.tag;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
public class PlayersPlayerTagService {
    
    private final static Logger logger = LoggerFactory.getLogger(PlayersPlayerTagService.class);
    
    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;
    
    @Autowired
    private PlayerTagMapper playerTagMapper;

    
    @Transactional
    public boolean updatePlayersPlayerTag(List<Integer> newTagIds, Integer playerId) {
        List<Integer> playersPlayerTags = playersPlayerTagMapper.listByPlayerIdAndPlayerGroup(playerId, false)
                .stream().map(PlayersPlayerTag::getPlayerTagId).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(newTagIds)) {
            if(playersPlayerTags.size() > 0) {
                playersPlayerTagMapper.delete(playerId, playersPlayerTags);
            }
        } else {
            if(newTagIds.size() > 0 && !validPlayerTags(newTagIds)) {
                return false;
            }
            // Add
            List<Integer> newTags = newTagIds.stream().filter(tagId -> !playersPlayerTags.contains(tagId))
                    .collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(newTags)) {
                playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, newTags);
            }

            // Remove
            List<Integer> removeTags = playersPlayerTags.stream()
                    .filter(playersPlayerTag -> !newTagIds.contains(playersPlayerTag)).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(removeTags)) {
                playersPlayerTagMapper.delete(playerId, removeTags);
            }
        }

        return true;
    }
    
    private boolean validPlayerTags(List<Integer> newTagIds) {
        List<PlayerTag> playerTags = playerTagMapper.listPlayerTagsByIds(newTagIds).stream()
                .filter(playerTag -> playerTag.isPlayerGroup() == false).collect(Collectors.toList());
        if(playerTags.size() != newTagIds.size()) {
            logger.error("Inappropriate tag IDs. new_tag_id: {} ", newTagIds.toString());
            return false;
        }
        return true;
    }
    
    @Transactional
    public boolean updatePlayersPlayerGroup(Integer newGroupId, Integer playerId) {
        if(newGroupId != null && playerTagMapper.get(newGroupId, true) == null) {
            logger.error("Inappropriate group ID. new_group_id: {} ", newGroupId);
            return false;
        }
        
        PlayersPlayerTag playerGroup = playersPlayerTagMapper.getGroupByPlayerId(playerId);

        if(playerGroup != null && playerGroup.getPlayerTagId().equals(newGroupId)) {
            // No update needed
            return true;
        }
        if(playerGroup != null) {
            // Delete existing player group
            playersPlayerTagMapper.delete(playerId, Arrays.asList(playerGroup.getPlayerTagId()));
        }
        if(newGroupId != null) {
            // Add new group
            playersPlayerTagMapper.insertMultiTagsWithOnePlayer(playerId, Arrays.asList(newGroupId));
        }
        return true;
    }

    public void setPlayerDefaultGroupId(Integer playerId) {
        updatePlayersPlayerGroup(playerTagMapper.getDefaultGroupId(), playerId);
    }
}
