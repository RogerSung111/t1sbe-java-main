package com.tripleonetech.sbe.player.tag;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PlayerTagMapper {

    Boolean delete(Integer id);

    int insert(PlayerTag tag);

    PlayerTag getGroupByPlayerId(Integer playerId);

    List<PlayerTag> listAll(@Param("playerGroup") Boolean playerGroup);

    List<PlayerTag> listPlayerTagsByIds(@Param("ids") List<Integer> ids);

    List<PlayerTag> listByNameLike(@Param("tagName") String tagName);

    PlayerTag get(@Param("id") Integer id);
    PlayerTag get(@Param("id") Integer id, @Param("playerGroup") Boolean playerGroup);

    Boolean update(PlayerTag tag);

    int deleteAutomationJob(@Param("id") Integer id);

    Integer getDefaultGroupId();

    int setDefaultGroup(@Param("id") int id);
}
