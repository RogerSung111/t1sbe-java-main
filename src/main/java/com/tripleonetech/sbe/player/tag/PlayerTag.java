package com.tripleonetech.sbe.player.tag;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;

public class PlayerTag {
    @ApiModelProperty(value = "Tag ID")
    private Integer id;
    @ApiModelProperty(hidden = true)
    private boolean playerGroup;
    @ApiModelProperty(hidden = true)
    private boolean defaultTag;
    @ApiModelProperty(value = "Tag's name")
    private String name;
    @ApiModelProperty(value = "The referenced automation job")
    private Integer automationJobId;
    @ApiModelProperty(hidden = true)
    private LocalDateTime createdAt;
    @ApiModelProperty(hidden = true)
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isPlayerGroup() {
        return playerGroup;
    }

    public void setPlayerGroup(boolean playerGroup) {
        this.playerGroup = playerGroup;
    }

    public boolean isDefaultTag() {
        return defaultTag;
    }

    public void setDefaultTag(boolean defaultTag) {
        this.defaultTag = defaultTag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAutomationJobId() {
        return automationJobId;
    }

    public void setAutomationJobId(Integer automationJobId) {
        this.automationJobId = automationJobId;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(updatedAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

}
