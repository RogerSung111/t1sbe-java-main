package com.tripleonetech.sbe.player.tag;

public class PlayersPlayerTag {

    private Integer playerId;
    private Integer playerTagId;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getPlayerTagId() {
        return playerTagId;
    }

    public void setPlayerTagId(Integer playerTagId) {
        this.playerTagId = playerTagId;
    }
}
