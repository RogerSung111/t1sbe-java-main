package com.tripleonetech.sbe.player.tag;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PlayersPlayerTagMapper {

    List<PlayersPlayerTag> listByPlayerIdAndPlayerGroup(Integer playerId, @Param("playerGroup") Boolean playerGroup);

    List<PlayersPlayerTag> listByPlayerTagId(Integer playerTagId);

    PlayersPlayerTag getGroupByPlayerId(Integer playerId);

    List<PlayersPlayerTag> listGroupByPlayerIds(@Param("playerIds") List<Integer> playerIds);

    int insertMultiTagsWithOnePlayer(@Param("playerId") Integer playerId, @Param("tagIds") List<Integer> tagIds);

    int insertMultiPlayersWithOneTag(@Param("playerIds") List<Integer> playerIds, @Param("tagId") Integer tagId);

    int delete(@Param("playerId") Integer playerId, @Param("tagIds") List<Integer> tagIds);

    int deleteBatch(@Param("playerIds") List<Integer> playerIds, @Param("tagIds") List<Integer> tagIds);

    int deleteAllWithoutPlayerGroup(@Param("playerId") Integer playerId, @Param("playerGroup") Boolean playerGroup);

    List<Integer> getPlayerIdHasAnyTagOf(@Param("tagIds") List<Integer> tagIds);

    List<Integer> getPlayerIdHasAllTagOf(@Param("tagIds") List<Integer> tagIds);

    List<Integer> getPlayerIdNotAnyTagOf(@Param("tagIds") List<Integer> tagIds);
}
