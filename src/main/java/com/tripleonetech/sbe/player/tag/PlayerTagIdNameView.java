package com.tripleonetech.sbe.player.tag;

import com.tripleonetech.sbe.common.web.IdNameView;
import io.swagger.annotations.ApiModelProperty;

public class PlayerTagIdNameView extends IdNameView {
    @ApiModelProperty(value="Whether this tag is a group")
    private boolean group;

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }
}
