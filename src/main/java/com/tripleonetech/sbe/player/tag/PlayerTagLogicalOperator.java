package com.tripleonetech.sbe.player.tag;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseNameEnum;

public enum PlayerTagLogicalOperator implements BaseNameEnum {
    ANY("any"), ALL("all");

    private String name;

    PlayerTagLogicalOperator(String name) {
        this.name = name;
    }

    @JsonValue
    @Override
    public String getName() {
        return name;
    }

    @JsonCreator
    public static PlayerTagLogicalOperator parse(String name) {
        if("all".equalsIgnoreCase(name)){
            return ALL;
        }
        return ANY;
    }
}
