package com.tripleonetech.sbe.player.tag;

import com.tripleonetech.sbe.automation.AutomationJobMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PlayerTagService {
    private final static Logger logger = LoggerFactory.getLogger(PlayersPlayerTagService.class);

    @Autowired
    private PlayerTagMapper playerTagMapper;
    @Autowired
    private AutomationJobMapper automationJobMapper;

    public Integer createGroup(PlayerTag group) {
        group.setPlayerGroup(true); // ensure this is a group
        playerTagMapper.insert(group);
        return group.getId();
    }

    public Integer createTag(PlayerTag tag) {
        tag.setPlayerGroup(false); // ensure this is not a group
        playerTagMapper.insert(tag);
        return tag.getId();
    }

    public List<PlayerTag> listAllTags() {
        return playerTagMapper.listAll(false);
    }

    public List<PlayerTag> listAllGroups() {
        return playerTagMapper.listAll(true);
    }

    public PlayerTag getTag(Integer id) {
        return playerTagMapper.get(id, false);
    }

    public PlayerTag getGroup(Integer id) {
        return playerTagMapper.get(id, true);
    }

    @Transactional
    public Boolean updatePlayerTag(PlayerTag tag) {
        PlayerTag originalTag = playerTagMapper.get(tag.getId());
        if (null != originalTag.getAutomationJobId()
                && !originalTag.getAutomationJobId().equals(tag.getAutomationJobId())) {
            automationJobMapper.delete(originalTag.getAutomationJobId());
        }
        return playerTagMapper.update(tag);
    }

    public void deletePlayerTagAutomationJob(Integer id) {
        PlayerTag tag = playerTagMapper.get(id);
        if (tag.getAutomationJobId() != null) {
            automationJobMapper.delete(tag.getAutomationJobId());
        }
        playerTagMapper.deleteAutomationJob(id);
    }

    @Transactional
    public Boolean deletePlayerTag(Integer id) {
        PlayerTag tag = playerTagMapper.get(id);
        if (playerTagMapper.getDefaultGroupId().equals(id)) {
            logger.error("Unable to delete, this tag is the system default group ID. default_group_id: {} ",
                    id.toString());
            return false;
        }

        if (tag.getAutomationJobId() != null) {
            automationJobMapper.delete(tag.getAutomationJobId());
        }
        boolean result = playerTagMapper.delete(id);
        return result;
    }

    public Boolean setDefaultTag(Integer newGroupTagId) {
        PlayerTag playerTag = playerTagMapper.get(newGroupTagId,true);
        if (playerTag != null) {
            try {
                playerTagMapper.setDefaultGroup(newGroupTagId);
                return true;
            } catch (Exception ex) {
                logger.error("Exception: ", ex);
                return false;
            }
        } else {
            logger.error("Inappropriate group IDs. new_group_id: {} ", newGroupTagId.toString());
        }
        return false;
    }
}
