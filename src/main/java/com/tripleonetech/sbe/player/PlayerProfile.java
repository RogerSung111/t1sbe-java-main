package com.tripleonetech.sbe.player;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;

public class PlayerProfile {
    // player profile
    private int id;
    private int playerId;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String language;
    private String gender;
    private String address;
    private String city;
    private String countryCode;
    private String countryPhoneCode;
    private String phoneNumber;
    private boolean phoneVerified;
    private String line;
    private String skype;
    private String qq;
    private String wechat;
    private Integer contactPreferences;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }
    
    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public Integer getContactPreferences() {
        return contactPreferences;
    }

    public void setContactPreferences(Integer contactPreferences) {
        this.contactPreferences = contactPreferences;
    }

    //Virtual property
    public String getPhoneNumberWithCountryCode() {
        return this.getCountryPhoneCode() + this.getPhoneNumber();
    }
    
    public static PlayerProfile from(PlayerProfileForm form) {
        PlayerProfile profile = new PlayerProfile();
        BeanUtils.copyProperties(form, profile);
        return profile;
    }

    public static PlayerProfile from(PlayerIdentityForm form) {
        PlayerProfile profile = new PlayerProfile();
        BeanUtils.copyProperties(form, profile);
        return profile;
    }

    public static PlayerProfile from(ApiPlayerProfileForm form) {
        PlayerProfile profile = new PlayerProfile();
        BeanUtils.copyProperties(form, profile);
        return profile;
    }
}
