package com.tripleonetech.sbe.player;

import org.springframework.context.ApplicationEvent;

public class PlayerLoginSuccessEvent extends ApplicationEvent {
    private Integer playerId;

    public PlayerLoginSuccessEvent(Object source, Integer playerId) {
        super(source);

        this.playerId = playerId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
