package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class PlayerListQueryForm extends PageQueryForm implements LocaleAwareQuery {
    @ApiModelProperty(value = "currency", required = false)
    private String currency;
    @ApiModelProperty(value = "username", required = false)
    private String username;
    @ApiModelProperty(value = "Tag IDs", required = false)
    private List<Integer> tags;
    @ApiModelProperty(value = "Group ID", required = false)
    private Integer group;
    @ApiModelProperty(value = "Last login time", required = false)
    private LocalDateTime lastLoginTimeStart;
    @ApiModelProperty(value = "Last login time", required = false)
    private LocalDateTime lastLoginTimeEnd;
    @ApiModelProperty(value = "Last login device, UNKNOWN(0), MANUAL(1), COMPUTER(2), MOBILE(3), TABLET(6)", required = false)
    private Integer lastLoginDevice;
    @ApiModelProperty(value = "Last login ip", required = false)
    private String lastLoginIp;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Integer> getTags() {
        if(tags == null) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    // Returns tag IDs and group IDs together in a single list
    public List<Integer> getTagsIdsAndGroupIds() {
        List<Integer> allIds = new ArrayList<>(getTags());
        if(getGroup() != null){
            allIds.add(getGroup());
        }
        return allIds;
    }

    public LocalDateTime getLastLoginTimeStart() {
        return lastLoginTimeStart;
    }

    public void setLastLoginTimeStart(ZonedDateTime lastLoginTimeStart) {
        this.lastLoginTimeStart = DateUtils.zonedToLocal(lastLoginTimeStart);
    }

    public LocalDateTime getLastLoginTimeEnd() {
        return lastLoginTimeEnd;
    }

    public void setLastLoginTimeEnd(ZonedDateTime lastLoginTimeEnd) {
        this.lastLoginTimeEnd = DateUtils.zonedToLocal(lastLoginTimeEnd);
    }

    public Integer getLastLoginDevice() {
        return lastLoginDevice;
    }

    public void setLastLoginDevice(Integer lastLoginDevice) {
        this.lastLoginDevice = lastLoginDevice;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

}
