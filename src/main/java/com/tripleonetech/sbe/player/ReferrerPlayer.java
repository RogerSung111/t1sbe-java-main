package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class ReferrerPlayer {

    private Integer playerId;

    private SiteCurrencyEnum currency;

    // 被推薦人的player id
    private Integer referralPlayerId;

    private Integer playerCredentialId;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getReferralPlayerId() {
        return referralPlayerId;
    }

    public void setReferralPlayerId(Integer referralPlayerId) {
        this.referralPlayerId = referralPlayerId;
    }

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }
}
