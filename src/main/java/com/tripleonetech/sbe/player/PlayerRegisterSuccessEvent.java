package com.tripleonetech.sbe.player;

import org.springframework.context.ApplicationEvent;

public class PlayerRegisterSuccessEvent extends ApplicationEvent {
    /**
     * 
     */
    private static final long serialVersionUID = -1676937684595388068L;

    public PlayerRegisterSuccessEvent(Object source) {
        super(source);
    }

}
