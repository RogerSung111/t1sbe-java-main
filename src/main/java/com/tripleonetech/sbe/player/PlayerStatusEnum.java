package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PlayerStatusEnum implements BaseCodeEnum {
    BLOCKED(0), ACTIVE(1), CURRENCY_INACTIVE(9);

    private int code;

    PlayerStatusEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static PlayerStatusEnum valueOf(Integer code) {
        PlayerStatusEnum[] enumConstants = PlayerStatusEnum.values();
        for (PlayerStatusEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
