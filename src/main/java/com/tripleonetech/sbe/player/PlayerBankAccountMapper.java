package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PlayerBankAccountMapper {
    PlayerBankAccount get(@Param("id") int id);
    
    List<PlayerBankAccount> listByPlayerId(@Param("playerId") int playerId);

    PlayerBankAccount getForPlayerCredential(
            @Param("playerCredentialId") int playerCredentialId,
            @Param("currency") SiteCurrencyEnum currency,
            @Param("id") int id);

    List<PlayerBankAccount> listByPlayerCredential(
            @Param("playerCredentialId") int playerCredentialId,
            @Param("currency") SiteCurrencyEnum currency);
    
    int insert(PlayerBankAccount playerBankAccount);
        
    void delete(int id);
    
    int update(PlayerBankAccount playerBankAccount);

    int setDefault(@Param("id") int id, @Param("playerId") int playerId);
}
