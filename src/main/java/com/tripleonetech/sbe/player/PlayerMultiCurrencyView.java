package com.tripleonetech.sbe.player;

import java.util.ArrayList;
import java.util.List;

public class PlayerMultiCurrencyView extends PlayerMultiCurrency {
    private List<BalanceView> balances;
    private List<ProfileListView> profiles;
    private List<ActivityView> activities;

    public List<ProfileListView> getProfiles() {
        if (null == profiles) {
            profiles = new ArrayList<>();
        }
        return profiles;
    }

    public List<ActivityView> getActivities() {
        if (null == activities) {
            activities = new ArrayList<>();
        }
        return activities;
    }

    public List<BalanceView> getBalances() {
        if(this.balances == null) {
            this.balances = new ArrayList<>();
        }
        return this.balances;
    }

    public void setBalances(List<BalanceView> balances) {
        this.balances = balances;
    }

    public void setProfiles(List<ProfileListView> profiles) {
        this.profiles = profiles;
    }

    public void setActivities(List<ActivityView> activities) {
        this.activities = activities;
    }
}
