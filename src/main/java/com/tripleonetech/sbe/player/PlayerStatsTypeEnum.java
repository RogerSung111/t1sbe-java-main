package com.tripleonetech.sbe.player;

import java.util.Arrays;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PlayerStatsTypeEnum implements BaseCodeEnum {
    DEPOSIT_AMOUNT(1), WITHDRAW_AMOUNT(2),
    CASHBACK_BONUS(4), REFERRAL_BONUS(8), CAMPAIGN_BONUS(16);

    private int code;

    PlayerStatsTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static PlayerStatsTypeEnum parse(int code) {
        return Arrays.stream(PlayerStatsTypeEnum.values()).filter(e -> e.code == code).findFirst().orElse(null);
    }
}
