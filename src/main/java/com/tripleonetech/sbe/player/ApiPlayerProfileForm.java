package com.tripleonetech.sbe.player;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class ApiPlayerProfileForm {
    @Size(max = 255)
    @ApiModelProperty(value = "LINE")
    private String line;
    @Size(max = 255)
    @ApiModelProperty(value = "Skype")
    private String skype;
    @Size(max = 255)
    @ApiModelProperty(value = "QQ")
    private String qq;
    @Size(max = 255)
    @ApiModelProperty(value = "WeChat")
    private String wechat;
    @ApiModelProperty(value = "First name")
    private String firstName;
    @ApiModelProperty(value = "Last name")
    private String lastName;
    @Size(max = 1, min = 1, message = "Gender only takes 1 character")
    @ApiModelProperty(value = "Gender, only takes M/F")
    private String gender;
    @ApiModelProperty(value = "Birthday, format YYYY-MM-DD")
    private LocalDate birthday;
    @NotBlank
    @ApiModelProperty(value = "Language tag for player's locale. e.g. en-US, zh-CN, zh-TW", required = true)
    private String language;
    @ApiModelProperty(value = "Address plain text")
    private String address;
    @Size(max = 50)
    @ApiModelProperty(value = "Address city")
    private String city;
    @Size(max = 3, min = 3, message = "The country must follow ISO 3166-1 alpha-3. For example: CHN. Reference: https://www.iban.com/country-codes")
    @ApiModelProperty(value = "3 upper-case alphabets, ISO-31661 country code")
    private String countryCode;
    @Size(max = 6)
    @ApiModelProperty(value = "Country calling code, digits only, without + sign", required = true)
    private String countryPhoneCode;
    @Size(max = 20)
    @ApiModelProperty(value = "Phone number", required = true)
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

}
