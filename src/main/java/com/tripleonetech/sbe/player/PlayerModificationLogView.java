package com.tripleonetech.sbe.player;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;

public class PlayerModificationLogView {
    private String actionType;
    private String description;
    private LocalDateTime createdAt;
    private String createdBy;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
