package com.tripleonetech.sbe.player;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tripleonetech.sbe.common.AlphanumericGenUtil;
import com.tripleonetech.sbe.common.CryptoUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;
import com.tripleonetech.sbe.game.GameApiFactory;
import com.tripleonetech.sbe.game.integration.GameApi;
import com.tripleonetech.sbe.game.log.ApiGameLog;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagService;
import com.tripleonetech.sbe.player.wallet.WalletService;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import com.tripleonetech.sbe.report.player.PlayerActivityReportService;

@Service
public class PlayerService {
    private static final Logger logger = LoggerFactory.getLogger(PlayerService.class);
    private final static int REFERRAL_CODE_LENGTH = 7;

    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerProfileMapper playerProfileMapper;
    @Autowired
    private WalletService walletService;
    @Autowired
    private GameApiFactory gameApiFactory;
    @Autowired
    private PlayerActivityReportService playerActivityReportService;
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Autowired
    private PlayersPlayerTagService playersPlayerTagService;

    /**
     * Register規則：產生Player是依照在site_privilege設置的currency
     * @return credential id
     */
    @Transactional
    public Integer register(PlayerNewForm form, String password,
            DeviceTypeEnum registerDevice, String registerIp) {
        PlayerCredential playerCredential = new PlayerCredential();
        playerCredential.setUsername(form.getUsername());
        playerCredential.setPassword(password);
        playerCredential.setReferralCode(AlphanumericGenUtil.generateAlphanumericCode(REFERRAL_CODE_LENGTH));
        playerCredential.setReferralPlayerCredentialId(getReferrer(form.getReferralCode()));
        playerCredential.setEmail(form.getEmail());
        playerCredential.setEmailVerified(false);
        playerCredential.setRegisterDevice(registerDevice);
        playerCredential.setRegisterIp(registerIp);
        // For player summary report
        playerCredential.setCreatedAt(LocalDateTime.now());
        playerCredentialMapper.insert(playerCredential);

        List<String> siteCurrencies = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.SITE_CURRENCY);
        for (String siteCurrency : siteCurrencies) {
            PlayerStatusEnum playerStatus = siteCurrency.equals(form.getCurrency().name())? PlayerStatusEnum.ACTIVE : PlayerStatusEnum.CURRENCY_INACTIVE;
            Player newPlayer = Player.create(form.getUsername(), SiteCurrencyEnum.valueOf(siteCurrency), playerStatus);
            this.generatePlayer(newPlayer);
        }
        try {
            eventPublisher.publishEvent(new PlayerRegisterSuccessEvent(playerCredential));
        } catch (Exception e) {
            logger.error("Exception when publishing login success event: ", e);
        }
        return playerCredential.getId();
    }

    @Transactional
    public void recordLogin(Player player) {
        // update last login date
        player.setLastLoginTime(LocalDateTime.now());
        player.setLastLoginDevice(ClientHttpInfoUtils.getDeviceType());
        player.setLastLoginIp(ClientHttpInfoUtils.getIp());

        playerMapper.updateLastLoginInfo(player);

        // Logging in to a currency for the first time will turn inactive into active
        if (PlayerStatusEnum.CURRENCY_INACTIVE == player.getStatus()) {
            player.setStatus(PlayerStatusEnum.ACTIVE);
            playerMapper.updateStatus(player.getId(), player.getStatus());
        }

        // update report about player
        playerActivityReportService.addPlayerActivityReport(player,
                PlayerActivityTypeEnum.LOGIN,
                PlayerActivityReportService.LOGIN_SUCCESS);
        try {
            eventPublisher.publishEvent(new PlayerLoginSuccessEvent(this, player.getId()));
        } catch (Exception e) {
            logger.error("Exception when publishing login success event: ", e);
        }
    }

    @Transactional
    public void recordFailedLogin(Player player) {
        playerActivityReportService.addPlayerActivityReport(player,
                PlayerActivityTypeEnum.LOGIN,
                PlayerActivityReportService.LOGIN_FAILURE);
    }

    @Transactional
    public void updateStatus(Integer playerId, PlayerStatusEnum status) {
        playerMapper.updateStatus(playerId, status);
    }

    /**
     * Update password of player.
     * Only for the player to call, because this method will update the passwordPending to false.
     *
     * @param playerId
     * @param newPassword
     */
    @Transactional
    public void updatePasswordByUsername(String username, String newPassword) {
        playerCredentialMapper.updatePasswordEncrypt(username,
                CryptoUtils.encryptPlayerPassword(newPassword));
        playerCredentialMapper.updatePasswordPending(username, false);
    }

    public boolean userExists(String username) {
        PlayerCredential existingCredential = playerCredentialMapper.getByUsername(username);
        return null != existingCredential;
    }

    public Player generatePlayer(Player newPlayer) {
        // insert one record of player
        playerMapper.insert(newPlayer);

        // initial settings for the player
        this.setupInitialSettings(newPlayer);
        return newPlayer;
    }

    private void setupInitialSettings(Player newPlayer) {
        // Create: PlayerProfile (only basic join data)
        PlayerProfile profile = new PlayerProfile();
        profile.setPlayerId(newPlayer.getId());
        playerProfileMapper.insert(profile);

        // Create: wallets for the current APIs (matching currencies' only)
        walletService.createMainWallet(newPlayer.getId());
        this.setupGameWallets(newPlayer);

        // Join default group tag
        playersPlayerTagService.setPlayerDefaultGroupId(newPlayer.getId());
    }

    private void setupGameWallets(Player newPlayer) {
        Collection<GameApi<? extends ApiGameLog>> gameApis = gameApiFactory.getAll();
        for (GameApi<? extends ApiGameLog> api : gameApis) {
            // Do not create subwallets if API is under different currency
            if(api.getCurrency() != newPlayer.getCurrency()){
                continue;
            }
            walletService.createSubWallet(newPlayer.getId(), api.getId());
        }
    }

    private Integer getReferrer(String referralCode) {
        PlayerCredential referrer = null;
        if(StringUtils.isNoneBlank(referralCode)) {
            referrer = playerCredentialMapper.getByReferrerCode(referralCode);
        }
        return referrer == null? null:referrer.getId();
    }

}
