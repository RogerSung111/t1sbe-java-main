package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Profile("api")
public class PlayerUserDetailsService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUserDetailsService.class);

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // fetch request parameters to login - note: this method is invoked during token refresh too,
        // in this case most of the parameters are not given in the request
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        String currency = request.getParameter("currency");

        if(currency != null) {
            SitePrivilege sitePrivilege = sitePrivilegeMapper.getSitePrivilege(SitePrivilegeTypeEnum.SITE_CURRENCY, currency.toUpperCase());
            if (sitePrivilege == null || !sitePrivilege.isActive()) {
                throw new UsernameNotFoundException(String.format("Currency [%s] is not allowed", currency));
            }
        }

        Player player = playerMapper.getByUniqueKey(username, SiteCurrencyEnum.valueOf(currency));
        if (null == player) {
            // We do not want to give out the information whether username exists in our DB
            throw new UsernameNotFoundException("Wrong password or username not found");
        }

        if (PlayerStatusEnum.BLOCKED.equals(player.getStatus())) {
            throw new UsernameNotFoundException("The player has been blocked");
        }

        // Up to this step, we do not know whether the login was successful or not. We just obtain player object for
        // subsequent password validation.
        return new PlayerUserDetails(player);
    }

}
