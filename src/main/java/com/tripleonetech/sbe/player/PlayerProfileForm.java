package com.tripleonetech.sbe.player;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import java.util.List;

public class PlayerProfileForm {
    @NotBlank
    @ApiModelProperty(value = "Language tag for player's locale. e.g. en-US, zh-CN, zh-TW. For a complete list, visit https://www.oracle.com/java/technologies/javase/jdk8-jre8-suported-locales.html", required = true)
    private String language;
    @Size(max = 255)
    @ApiModelProperty(value = "LINE")
    private String line;
    @Size(max = 255)
    @ApiModelProperty(value = "Skype")
    private String skype;
    @Size(max = 255)
    @ApiModelProperty(value = "QQ")
    private String qq;
    @Size(max = 255)
    @ApiModelProperty(value = "WeChat")
    private String wechat;
    @ApiModelProperty(value = "Preferred contact method, multiple selection. e.g. EMAIL(1), SMS(2), PHONE_CALL(4), LETTER(8)")
    private List<Integer> contactPreferenceList;
    
    @ApiModelProperty(value = "Player group")
    private Integer group;
    @ApiModelProperty(value = "Player tags")
    private List<Integer> tags;
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public List<Integer> getContactPreferenceList() {
        return contactPreferenceList;
    }

    public void setContactPreferenceList(List<Integer> contactPreferenceList) {
        this.contactPreferenceList = contactPreferenceList;
    }

    @ApiModelProperty(hidden = true)
    public Integer getContactPreferences() {
        if (null == contactPreferenceList) {
            return 0;
        }
        return contactPreferenceList.stream().reduce(0, (a, b) -> a | b);
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }
}
