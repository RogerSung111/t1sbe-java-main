package com.tripleonetech.sbe.player;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlayerFavoriteGameMapper {
    int insert(PlayerFavoriteGame obj);

    int delete(PlayerFavoriteGame obj);

    List<PlayerFavoriteGame> listByPlayer(Integer playerId);
}
