package com.tripleonetech.sbe.player;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ApiPlayerProfileView {
    private SiteCurrencyEnum currency;
    private Integer status;
    private boolean cashbackEnabled;
    private boolean campaignEnabled;
    private boolean withdrawEnabled;
    private LocalDateTime createdAt;
    private Integer risk;
    private String language;
    private Integer group;
    private List<Integer> tags;
    private String line;
    private String skype;
    private String qq;
    private String wechat;
    private String firstName;
    private String lastName;
    private String gender;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    private String address;
    private String city;
    private String countryCode;
    private String countryPhoneCode;
    private String phoneNumber;
    private String referralCode;
    private RefererView referer;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }
    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isCashbackEnabled() {
        return cashbackEnabled;
    }

    public void setCashbackEnabled(boolean cashbackEnabled) {
        this.cashbackEnabled = cashbackEnabled;
    }

    public boolean isCampaignEnabled() {
        return campaignEnabled;
    }

    public void setCampaignEnabled(boolean campaignEnabled) {
        this.campaignEnabled = campaignEnabled;
    }

    public boolean isWithdrawEnabled() {
        return withdrawEnabled;
    }

    public void setWithdrawEnabled(boolean withdrawEnabled) {
        this.withdrawEnabled = withdrawEnabled;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getRisk() {
        return risk;
    }

    public void setRisk(Integer risk) {
        this.risk = risk;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public void setTagsList(String tagsList) {
        if (tagsList == null) {
            this.tags = new ArrayList<>();
        } else {
            this.tags = Arrays.stream(tagsList.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
        }
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public RefererView getReferer() {
        return referer;
    }

    public void setReferer(RefererView referer) {
        this.referer = referer;
    }
}
