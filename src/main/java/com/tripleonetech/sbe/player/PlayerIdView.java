package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

public class PlayerIdView {
    @ApiModelProperty(value = "Player ID")  // maps to our credential ID
    private int id;
    @ApiModelProperty(value = "Player currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Player username")
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
