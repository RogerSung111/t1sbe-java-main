package com.tripleonetech.sbe.player.wallet.history;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.web.api.ApiReportController.WalletTransactionView;

import java.util.List;
import java.util.Map;

@Mapper
public interface WalletTransactionMapper {

    int insert(WalletTransaction record);

    WalletTransaction selectByPrimaryKey(Integer id);

    List<WalletTransaction> querySummary(Map<String, Object> params);

    List<WalletTransactionView> list(@Param("queryForm") WalletTransactionQueryForm queryForm,
                                 @Param("pageNum") Integer pageNum,
                                 @Param("pageSize") Integer pageSize);
}
