package com.tripleonetech.sbe.player.wallet.history;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.promo.PromoTypeEnum;

public class WalletTransaction {
    private Integer id;

    private WalletTransactionTypeEnum type;

    private Integer playerId;

    private BigDecimal balanceBefore;

    private BigDecimal balanceAfter;

    private BigDecimal amount;

    private Integer gameApiId;

    private String externalTransactionId;

    private String internalTransactionId;

    private PromoTypeEnum promoType;

    private Integer promoRuleId;

    private String note;

    private Integer operatorId;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WalletTransactionTypeEnum getType() {
        return type;
    }

    public void setType(WalletTransactionTypeEnum type) {
        this.type = type;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public String getInternalTransactionId() {
        return internalTransactionId;
    }

    public void setInternalTransactionId(String internalTransactionId) {
        this.internalTransactionId = internalTransactionId;
    }

    public PromoTypeEnum getPromoType() {
        return promoType;
    }

    public void setPromoType(PromoTypeEnum promoType) {
        this.promoType = promoType;
    }

    public Integer getPromoRuleId() {
        return promoRuleId;
    }

    public void setPromoRuleId(Integer promoRuleId) {
        this.promoRuleId = promoRuleId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
