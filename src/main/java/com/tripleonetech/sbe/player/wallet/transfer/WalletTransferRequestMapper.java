package com.tripleonetech.sbe.player.wallet.transfer;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WalletTransferRequestMapper {

    int insert(WalletTransferRequest record);

    int updateStatus(@Param("id")Integer id, @Param("status")WalletTransferStatusEnum status);
}