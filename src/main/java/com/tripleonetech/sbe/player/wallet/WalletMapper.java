package com.tripleonetech.sbe.player.wallet;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WalletMapper {

    int insert(Wallet wallet);

    Wallet getMainWallet(@Param("playerId") int playerId);

    List<Wallet> getMainWallets(@Param("playerIds") List<Integer> playerIds);

    Wallet get(@Param("playerId")Integer playerId, @Param("gameApiId")Integer gameApiId);
    
    @MapKey("gameApiId")
    Map<Integer, Wallet> getAllGameWalletsByPlayerId(@Param("playerId")Integer playerId);

    @MapKey("gameApiId")
    Map<Integer, Wallet> getActiveGameWalletsByPlayerId(@Param("playerId")Integer playerId);
    
    int update(Wallet wallet);

    int setGameWalletsToDirty(@Param("gameApiId")Integer gameApiId, @Param("playerIds")int... playerIds);

    int depositGamePendingWallet(@Param("gameApiId")Integer gameApiId, @Param("playerId")int playerId, @Param("pendingWallet")BigDecimal pendingWallet);

    int withdrawGamePendingWallet(@Param("gameApiId")Integer gameApiId, @Param("playerId")int playerId, @Param("pendingWallet")BigDecimal pendingWallet);

    int adjustMainWallet(@Param("playerId")Integer playerId, @Param("amount")BigDecimal amount);

    int moveMainBalanceToPending(@Param("playerId") int playerId,  @Param("amount")BigDecimal amount);

    int returnMainBalanceFromPending(@Param("playerId") int playerId,  @Param("amount")BigDecimal amount);

    int deductMainBalanceFromPending(@Param("playerId") int playerId,  @Param("amount")BigDecimal amount);

    BigDecimal getBalanceSumByPlayerId(@Param("playerId") Integer playerId);
}
