package com.tripleonetech.sbe.player.wallet.transfer;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum WalletTransferStatusEnum implements BaseCodeEnum {
    CREATE(0),
    PROCESSING(1),
    FAIL(2),
    SUCCESS(3),
    PENDING(4);

    private int code;

    WalletTransferStatusEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
