package com.tripleonetech.sbe.player.wallet;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;

public class GameWalletView {

    private Integer gameApiId;
    private String gameApiName;
    private SiteCurrencyEnum currency;
    private BigDecimal balance;
    private BigDecimal pending;
    private Boolean dirty;
    private LocalDateTime lastSync;
    private boolean allowDecimalTransfer = true;
    // status of game api
    private ApiConfigStatusEnum status;

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public String getGameApiName() {
        return gameApiName;
    }

    public void setGameApiName(String gameApiName) {
        this.gameApiName = gameApiName;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPending() {
        return pending;
    }

    public void setPending(BigDecimal pending) {
        this.pending = pending;
    }

    public Boolean getDirty() {
        return dirty;
    }

    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }

    public ZonedDateTime getLastSync() {
        return DateUtils.localToZoned(lastSync);
    }

    public void setLastSync(LocalDateTime lastSync) {
        this.lastSync = lastSync;
    }

    private void setLastSyncLocal(LocalDateTime lastSyncLocal){
        this.lastSync = lastSyncLocal;
    }

    public boolean isAllowDecimalTransfer() {
        return allowDecimalTransfer;
    }

    public void setAllowDecimalTransfer(boolean allowDecimalTransfer) {
        this.allowDecimalTransfer = allowDecimalTransfer;
    }

    public ApiConfigStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApiConfigStatusEnum status) {
        this.status = status;
    }

    public static GameWalletView create(Wallet wallet, SiteCurrencyEnum currency) {
        return create(wallet, currency, true);
    }

    public static GameWalletView create(Wallet wallet, SiteCurrencyEnum currency, boolean allowDecimalTransfer) {
        GameWalletView view = new GameWalletView();
        view.setGameApiId(wallet.getGameApiId());
        view.setGameApiName(wallet.getName());
        view.setCurrency(currency);
        view.setBalance(wallet.getBalance());
        view.setPending(wallet.getPendingWallet());
        view.setDirty(wallet.isDirty());
        view.setLastSyncLocal(wallet.getCleanDate());
        view.setStatus(wallet.getStatus());
        view.setAllowDecimalTransfer(allowDecimalTransfer);
        return view;
    }
}
