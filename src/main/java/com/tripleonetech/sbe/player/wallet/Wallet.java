package com.tripleonetech.sbe.player.wallet;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;

public class Wallet {

    private Integer playerId;
    private Integer gameApiId;
    private String name;
    private BigDecimal balance;
    private BigDecimal pendingWallet;
    private String errorCode;

    private LocalDateTime cleanDate;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    // status of game api
    private ApiConfigStatusEnum status;

    public Wallet() {
    }

    public Wallet(Integer playerId, Integer gameApiId) {
        this.playerId = playerId;
        this.gameApiId = gameApiId;
        balance = BigDecimal.ZERO;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", getName(), getBalance().toString());
    }

    /**
     * Judge the sub wallet is dirty or not,
     * by (current time - clean_date) is large than constant "wallet.dirtyline" in minutes
     * 
     * @return
     */
    public Boolean isDirty() {
        Long dirtyLine = Long.valueOf(Constant.getInstance().getWallet().getDirtyline());
        return ChronoUnit.MINUTES.between(cleanDate, LocalDateTime.now()) > dirtyLine;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPendingWallet() {
        return pendingWallet;
    }

    public void setPendingWallet(BigDecimal pendingWallet) {
        this.pendingWallet = pendingWallet;
    }

    public LocalDateTime getCleanDate() {
        return cleanDate;
    }

    public void setCleanDate(LocalDateTime cleanDate) {
        this.cleanDate = cleanDate;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ApiConfigStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ApiConfigStatusEnum status) {
        this.status = status;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isError() {
        return getErrorCode() != null;
    }

}

