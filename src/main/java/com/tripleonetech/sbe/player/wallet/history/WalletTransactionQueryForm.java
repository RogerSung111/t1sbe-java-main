package com.tripleonetech.sbe.player.wallet.history;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;

import io.swagger.annotations.ApiModelProperty;

public class WalletTransactionQueryForm extends PageQueryForm {

    @ApiModelProperty(value = "Transaction type: DEPOSIT(0), WITHDRAWAL(1), TRANSFER_IN(2), TRANSFER_OUT(3), BONUS(4), " +
            "CASHBACK(5), FREEZE(7), WITHDRAWAL_CANCEL(9), ADD_BONUS(10), SUBTRACT_BONUS(11), ADD_BALANCE(12), " +
            "SUBTRACT_BALANCE(13), ADD_CASHBACK(14), ADD_WITHDRAWAL_CONDITION(15), PAYMENT_CHARGE(16)",
            allowableValues = "0,1,2,3,4,5,7,9,10,11,12,13,14,15,16", required = false)
    private Integer type;
    @ApiModelProperty(value = "Player ID", required = false)
    private Integer playerId;
    @NotNull
    @ApiModelProperty(value = "Begin time of created time search", required = true)
    private LocalDateTime createdAtStart;
    @NotNull
    @ApiModelProperty(value = "End time of created time search", required = true)
    private LocalDateTime createdAtEnd;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
}
