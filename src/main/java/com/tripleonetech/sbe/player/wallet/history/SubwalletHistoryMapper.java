package com.tripleonetech.sbe.player.wallet.history;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubwalletHistoryMapper {

    int insert(SubwalletHistory record);

    SubwalletHistory selectByPrimaryKey(Integer id);
}