package com.tripleonetech.sbe.player.wallet;

import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.creditpoint.CreditPointService;
import com.tripleonetech.sbe.game.GameApiService;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.message.PlayerMessageTemplate;
import com.tripleonetech.sbe.player.message.SystemMessageService;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.promo.bonus.PromoBonus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.DEPOSIT;
import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.PROMO;
import static com.tripleonetech.sbe.player.message.PlayerMessageTemplate.Type.WITHDRAW;
import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

@Service
public class WalletService {

    private static final Logger logger = LoggerFactory.getLogger(WalletService.class);

    @Autowired
    private GameApiService gameApiService;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private CreditPointService creditPointService;
    @Autowired
    private SystemMessageService systemMessageService;

    public Wallet createMainWallet(Integer playerId) {
        Wallet wallet = new Wallet(playerId, null);
        wallet.setBalance(BigDecimal.ZERO);
        wallet.setPendingWallet(BigDecimal.ZERO);
        walletMapper.insert(wallet);
        return wallet;
    }

    public Wallet createSubWallet(Integer playerId, Integer gameApiId) {
        Wallet wallet = new Wallet(playerId, gameApiId);
        wallet.setBalance(BigDecimal.ZERO);
        wallet.setPendingWallet(BigDecimal.ZERO);
        walletMapper.insert(wallet);
        return wallet;
    }

    public Wallet getMainWallet(int playerId) {
        return walletMapper.getMainWallet(playerId);
    }

    public List<Wallet> getMainWallets(List<Integer> playerIds) {
        return walletMapper.getMainWallets(playerIds);
    }

    /**
     * Queries given player's latest main wallet balance
     *
     * @param playerId
     * @return
     */
    public BigDecimal getMainWalletBalance(int playerId) {
        return getMainWallet(playerId).getBalance();
    }

    @Transactional
    public boolean depositToMainWallet(int playerId, BigDecimal amount) {
        return depositToMainWallet(playerId, amount, null, null);
    }

    @Transactional
    public boolean depositToMainWallet(int playerId, BigDecimal amount, String note, Integer operatorId) {
        if (BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new InvalidParameterException("The amount should be positive number.");
        }

        // System message
        Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params = new HashMap<>();
        params.put(PlayerMessageTemplate.Deposit.Properties.AMOUNT, amount);
        params.put(PlayerMessageTemplate.Deposit.Properties.PLAYER_NAME, playerMapper.get(playerId).getUsername());
        systemMessageService.sendSystemMessage(DEPOSIT, params, Collections.singletonList(playerId));

        return adjustMainWallet(playerId, amount, WalletTransactionTypeEnum.DEPOSIT, note);
    }

    @Transactional
    public boolean withdrawFromMainWallet(int playerId, BigDecimal amount, WalletTransactionTypeEnum walletTransactionType, Integer operatorId) {
        return withdrawFromMainWallet(playerId, amount, walletTransactionType, null, operatorId);
    }

    @Transactional
    public boolean withdrawFromMainWallet(int playerId, BigDecimal amount, String note, Integer operatorId) {
        return withdrawFromMainWallet(playerId, amount, WalletTransactionTypeEnum.WITHDRAWAL, note, operatorId);
    }

    @Transactional
    public boolean withdrawFromMainWallet(int playerId, BigDecimal amount, Integer operatorId) {
        return withdrawFromMainWallet(playerId, amount, WalletTransactionTypeEnum.WITHDRAWAL, operatorId);
    }

    @Transactional
    public boolean withdrawFromMainWallet(int playerId, BigDecimal amount,  WalletTransactionTypeEnum walletTransactionType,  String note, Integer operatorId) {
        // System message
        Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params = new HashMap<>();
        params.put(PlayerMessageTemplate.Withdraw.Properties.AMOUNT, amount);
        params.put(PlayerMessageTemplate.Withdraw.Properties.PLAYER_NAME, playerMapper.get(playerId).getUsername());
        systemMessageService.sendSystemMessage(WITHDRAW, params, Collections.singletonList(playerId));

        return adjustMainWallet(playerId, amount.negate(), walletTransactionType, note, operatorId);
    }

    private String commonNoteTemplate = "This transaction was made by %s.";

    @Transactional
    public boolean addBalanceToMainWallet(Integer playerId, BigDecimal amount, String operatorUsername, Integer operatorId) {
        return this.addBalanceToMainWallet(playerId, amount, String.format(commonNoteTemplate, operatorUsername), operatorUsername, operatorId);
    }

    @Transactional
    public boolean addBalanceToMainWallet(Integer playerId, BigDecimal amount, String note, String operatorUsername, Integer operatorId) {
        // System message
        Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params = new HashMap<>();
        params.put(PlayerMessageTemplate.Deposit.Properties.AMOUNT, amount);
        params.put(PlayerMessageTemplate.Deposit.Properties.PLAYER_NAME, playerMapper.get(playerId).getUsername());
        systemMessageService.sendSystemMessage(DEPOSIT, params, Collections.singletonList(playerId));

        return adjustMainWallet(playerId, amount, WalletTransactionTypeEnum.ADD_BALANCE,
                note, operatorId);
    }

    @Transactional
    public boolean addBonusToMainWallet(PromoBonus promoBonus, WalletTransactionTypeEnum walletTransactionTypeEnum,
            String operatorUsername, Integer operatorId) {
        // System message
        Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params = new HashMap<>();
        params.put(PlayerMessageTemplate.Deposit.Properties.AMOUNT, promoBonus.getBonusAmount());
        params.put(PlayerMessageTemplate.Deposit.Properties.PLAYER_NAME,
                playerMapper.get(promoBonus.getPlayerId()).getUsername());
        systemMessageService.sendSystemMessage(PROMO, params, Collections.singletonList(promoBonus.getPlayerId()));

        return adjustMainWallet(promoBonus.getPlayerId(), promoBonus.getBonusAmount(), walletTransactionTypeEnum,
                String.format(commonNoteTemplate, operatorUsername), operatorId, promoBonus);
    }

    public boolean deductBalanceFromMainWallet(Integer playerId, BigDecimal amount, String operatorUsername, Integer operatorId) {
        return deductBalanceFromMainWallet(playerId, amount, String.format(commonNoteTemplate, operatorUsername), operatorUsername, operatorId);
    }

    @Transactional
    public boolean deductBalanceFromMainWallet(Integer playerId, BigDecimal amount, String note, String operatorUsername, Integer operatorId) {
        // System message
        Map<PlayerMessageTemplate.PlayerMessageTemplateProperty, Object> params = new HashMap<>();
        params.put(PlayerMessageTemplate.Deposit.Properties.AMOUNT, amount);
        params.put(PlayerMessageTemplate.Deposit.Properties.PLAYER_NAME, playerMapper.get(playerId).getUsername());
        systemMessageService.sendSystemMessage(WITHDRAW, params, Collections.singletonList(playerId));

        return adjustMainWallet(playerId, amount.negate(), WalletTransactionTypeEnum.DEDUCT_BALANCE,
                note, operatorId);
    }


    private boolean adjustMainWallet(int playerId, BigDecimal amount, WalletTransactionTypeEnum walletTransactionType,
            String note) {
        return adjustMainWallet(playerId, amount, walletTransactionType, note, null, null);
    }

    private boolean adjustMainWallet(int playerId, BigDecimal amount, WalletTransactionTypeEnum walletTransactionType,
            String note, Integer operatorId) {
        return adjustMainWallet(playerId, amount, walletTransactionType, note, operatorId, null);
    }

    private boolean adjustMainWallet(int playerId, BigDecimal amount, WalletTransactionTypeEnum walletTransactionType,
            String note, Integer operatorId, PromoBonus promoBonus) {
        BigDecimal balanceBefore = getMainWalletBalance(playerId);
        int rows = walletMapper.adjustMainWallet(playerId, amount);
        if (rows <= 0) {
            logger.error("adjust main wallet balance failed!");
            return false;
        }
        WalletTransaction transaction = new WalletTransaction();
        transaction.setPlayerId(playerId);
        transaction.setAmount(amount.abs());
        transaction.setBalanceBefore(balanceBefore);
        transaction.setBalanceAfter(getMainWalletBalance(playerId));
        transaction.setType(walletTransactionType);
        transaction.setOperatorId(operatorId);
        transaction.setNote(note);
        if (null != promoBonus) {
            transaction.setPromoType(promoBonus.getPromoType());
            transaction.setPromoRuleId(promoBonus.getRuleId());
        }
        insertWalletTransaction(transaction);
        return true;
    }

    /**
     * Deposit amount from main wallet to game API
     *
     * @param playerId
     * @param gameApiId
     * @param amount
     */
    @LogGameTransaction
    @Transactional(isolation = REPEATABLE_READ)
    public ApiResult depositGame(int playerId, int gameApiId, BigDecimal amount) {
        return depositGame(playerId, gameApiId, amount, null);
    }

    /**
     * Deposit amount from main wallet to game API
     *
     * @param playerId
     * @param gameApiId
     * @param amount
     * @param operatorId
     */
    @LogGameTransaction
    @Transactional(isolation = REPEATABLE_READ)
    public ApiResult depositGame(int playerId, int gameApiId, BigDecimal amount, Integer operatorId) {
        Player player = playerMapper.get(playerId);
        BigDecimal mainWalletBalance = getMainWalletBalance(playerId);
        // There is game API accepts integer transaction only
        amount = gameApiService.allowDecimalTransfer(gameApiId) ? amount : amount.setScale(0, RoundingMode.DOWN);

        if (amount.compareTo(mainWalletBalance) > 0) {
            logger.error("Player's balance is not enough!");
            return new ApiResult(ApiResponseEnum.BAD_REQUEST);
        }

        if (!creditPointService.subtractCreditPoint(amount)) {
            logger.error("Subtract credit point failed!");
            return new ApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        }
        // Add amount to pending wallet
        walletMapper.depositGamePendingWallet(gameApiId, playerId, amount);
        ApiResult apiResult = gameApiService.deposit(gameApiId, player, amount);

        if (apiResult.isSuccess()) {
            setWalletToDirty(gameApiId, playerId);
            walletMapper.adjustMainWallet(playerId, amount.negate());
            WalletTransaction transaction = new WalletTransaction();
            transaction.setPlayerId(playerId);
            transaction.setAmount(amount);
            transaction.setGameApiId(gameApiId);
            transaction.setBalanceBefore(mainWalletBalance);
            transaction.setType(WalletTransactionTypeEnum.TRANSFER_OUT);
            transaction.setOperatorId(operatorId);
            Integer transactionId = insertWalletTransaction(transaction);
            creditPointService.recordHistory(amount.negate(), transactionId);
        }
        walletMapper.withdrawGamePendingWallet(gameApiId, playerId, amount);

        return apiResult;
    }

    /**
     * Withdraw amount from game API to main wallet
     *
     * @param playerId
     * @param gameApiId
     * @param amount
     *            null means the whole balance
     */
    @LogGameTransaction
    @Transactional(isolation = REPEATABLE_READ)
    public ApiResult withdrawGame(int playerId, int gameApiId, BigDecimal amount) {
        return withdrawGame(playerId, gameApiId, amount, null);
    }

    /**
     * Withdraw amount from game API to main wallet
     *
     * @param playerId
     * @param gameApiId
     * @param amount
     *            null means the whole balance
     * @param operatorId
     */
    @LogGameTransaction
    @Transactional(isolation = REPEATABLE_READ)
    public ApiResult withdrawGame(int playerId, int gameApiId, BigDecimal amount, Integer operatorId) {
        Player player = playerMapper.get(playerId);
        BigDecimal mainWalletBalance = getMainWalletBalance(playerId);
        // There is game API accepts integer transaction only
        amount = gameApiService.allowDecimalTransfer(gameApiId) ? amount : amount.setScale(0, RoundingMode.DOWN);

        // Add amount to pending wallet
        walletMapper.depositGamePendingWallet(gameApiId, playerId, amount);
        // Invoke game API : withdraw
        ApiResult apiResult = gameApiService.withdraw(gameApiId, player, amount);
        if (apiResult.isSuccess()) {
            // Set sub wallet to dirty
            setWalletToDirty(gameApiId, playerId);
            // Deposit main wallet
            walletMapper.adjustMainWallet(playerId, amount);
            // Add log of sub wallet transaction history
            WalletTransaction transaction = new WalletTransaction();
            transaction.setPlayerId(playerId);
            transaction.setAmount(amount);
            transaction.setGameApiId(gameApiId);
            transaction.setBalanceBefore(mainWalletBalance);
            transaction.setType(WalletTransactionTypeEnum.TRANSFER_IN);
            transaction.setOperatorId(operatorId);
            Integer transactionId = insertWalletTransaction(transaction);
            // Add credit point
            if (!creditPointService.addCreditPoint(amount)) {
                logger.error("Add credit point failed!");
                return new ApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
            }
            creditPointService.recordHistory(amount, transactionId);
        }
        // Deduct amount from pending wallet
        walletMapper.withdrawGamePendingWallet(gameApiId, playerId, amount);

        return apiResult;
    }

    /**
     * set Sub Wallet as dirty by gameApiId and playerId
     *
     * @param gameApiId
     * @param playerId
     * @return
     */
    public int setWalletToDirty(Integer gameApiId, int playerId) {
        return walletMapper.setGameWalletsToDirty(gameApiId, playerId);
    }

    /**
     * set Sub Wallets as dirty by gameCode and playerIds
     *
     * @param gameApiId
     * @param playerIds
     * @return
     */
    public int setWalletsToDirty(Integer gameApiId, int[] playerIds) {
        return walletMapper.setGameWalletsToDirty(gameApiId, playerIds);
    }

    /**
     * get balance from game API and write into sub wallet with current time
     *
     * @param player
     * @param wallet
     * @return
     */
    public Wallet getCurrentBalance(Player player, Wallet wallet) {
        GameApiResult syncBalanceResult = gameApiService.syncBalance(wallet.getGameApiId(), player);

        if (!syncBalanceResult.isSuccess()) {
            wallet.setErrorCode(syncBalanceResult.getResponseEnum().toString());
            return wallet;
        }

        wallet.setBalance(syncBalanceResult.getBalance());
        wallet.setCleanDate(LocalDateTime.now());
        walletMapper.update(wallet);
        return wallet;
    }

    private Integer insertWalletTransaction(WalletTransaction transaction) {
        walletTransactionMapper.insert(transaction);
        return transaction.getId();
    }

    /**
     * Freeze amount : transfer withdraw amount from main wallet to pending wallet
     *
     * @param playerId
     * @param amount
     *            (expected withdraw_amount)
     * @return true/false (success, failure)
     * @author eddie
     */
    @Transactional(isolation = REPEATABLE_READ)
    public boolean freezeWithdrawAmount(int playerId, BigDecimal amount) {
        if (BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new InvalidParameterException("The amount should be positive number.");
        }

        Wallet mainWallet = walletMapper.getMainWallet(playerId);
        if (amount.compareTo(mainWallet.getBalance()) > 0) {
            logger.warn("Withdraw amount [{}] is greater than player's main wallet [{}]", amount,
                    mainWallet.getBalance());
            return false;
        }

        walletMapper.moveMainBalanceToPending(playerId, amount);
        WalletTransaction transaction = new WalletTransaction();
        transaction.setPlayerId(playerId);
        transaction.setAmount(amount);
        transaction.setBalanceBefore(mainWallet.getBalance());
        transaction.setType(WalletTransactionTypeEnum.FREEZE);
        insertWalletTransaction(transaction);
        return true;

    }

    /**
     * return an amount to pending_wallet
     *
     * @param playerId
     * @param amount
     *            (expected withdraw_amount)
     * @return true/false (success, failure)
     * @author eddie
     */
    @Transactional(isolation = REPEATABLE_READ)
    public boolean returnMainBalanceFromPending(int playerId, BigDecimal amount) {

        Wallet mainWallet = walletMapper.getMainWallet(playerId);
        if (!validateUnfreezedAmount(amount, mainWallet.getPendingWallet())) {
            return false;
        }

        walletMapper.returnMainBalanceFromPending(playerId, amount);
        WalletTransaction transaction = new WalletTransaction();
        transaction.setPlayerId(playerId);
        transaction.setAmount(amount);
        transaction.setBalanceBefore(mainWallet.getBalance());
        transaction.setType(WalletTransactionTypeEnum.WITHDRAWAL_CANCEL);
        insertWalletTransaction(transaction);
        return true;
    }

    /**
     * deduct an amount from pending_wallet
     *
     * @param playerId
     * @param amount
     *            (expected withdraw_amount)
     * @return true/false (success, failure)
     * @author eddie
     */
    @Transactional(isolation = REPEATABLE_READ)
    public boolean deductMainBalanceFromPending(int playerId, BigDecimal amount) {

        Wallet mainWallet = walletMapper.getMainWallet(playerId);

        if (!validateUnfreezedAmount(amount, mainWallet.getPendingWallet())) {
            return false;
        }

        walletMapper.deductMainBalanceFromPending(playerId, amount);
        WalletTransaction transaction = new WalletTransaction();
        transaction.setPlayerId(playerId);
        transaction.setAmount(amount);
        transaction.setBalanceBefore(mainWallet.getBalance());
        transaction.setType(WalletTransactionTypeEnum.WITHDRAWAL_APPROVE);
        insertWalletTransaction(transaction);
        return true;
    }

    private boolean validateUnfreezedAmount(BigDecimal amount, BigDecimal pendingAmount) {
        if (BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new InvalidParameterException("The amount should be positive number.");
        }

        if (amount.compareTo(pendingAmount) > 0) {
            logger.warn("amount [{}] is greater than player's pending wallet [{}]", amount, pendingAmount);
            return false;
        }
        return true;
    }
}
