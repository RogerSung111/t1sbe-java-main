package com.tripleonetech.sbe.player.wallet.history;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class SubwalletHistory {
    private Integer id;

    private Integer type;

    private Integer playerId;

    private Integer gameApiId;

    private BigDecimal balanceBefore;

    private Boolean syncSuccess;

    private BigDecimal balanceAfter;

    private String createdBy;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public Boolean getSyncSuccess() {
        return syncSuccess;
    }

    public void setSyncSuccess(Boolean syncSuccess) {
        this.syncSuccess = syncSuccess;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
