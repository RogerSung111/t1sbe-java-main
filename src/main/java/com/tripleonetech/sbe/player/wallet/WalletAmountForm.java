package com.tripleonetech.sbe.player.wallet;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

import java.math.BigDecimal;

public class WalletAmountForm {

    @NotBlank
    @ApiModelProperty(value = "Username of player", required = true)
    private String username;

    @NotNull
    @ApiModelProperty(value = "Currency", required = true)
    private SiteCurrencyEnum currency;

    @NotNull
    @ApiModelProperty(value = "Amount to be added to wallet.", required = true)
    private BigDecimal amount;

    @ApiModelProperty(value = "Explain the reason for this transaction")
    private String note;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
