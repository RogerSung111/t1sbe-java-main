package com.tripleonetech.sbe.player.wallet.transfer;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WalletTransferRequest {
    private Integer id;

    private Integer playerId;

    private Integer gameApiId;

    private BigDecimal amount;

    private WalletTransferStatusEnum status;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public WalletTransferRequest(Integer playerId, Integer gameApiId, BigDecimal amount, WalletTransferStatusEnum status) {
        this.playerId = playerId;
        this.gameApiId = gameApiId;
        this.amount = amount;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletTransferStatusEnum getStatus() {
        return status;
    }

    public void setStatus(WalletTransferStatusEnum status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}