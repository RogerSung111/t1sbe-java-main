package com.tripleonetech.sbe.player.wallet;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

import java.math.BigDecimal;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.wallet.history.SubwalletHistory;
import com.tripleonetech.sbe.player.wallet.history.SubwalletHistoryMapper;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferStatusEnum;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferRequest;
import com.tripleonetech.sbe.player.wallet.transfer.WalletTransferRequestMapper;

@Aspect
@Component
public class WalletAspect {

    private static final Logger logger = LoggerFactory.getLogger(WalletAspect.class);

    @Autowired
    private WalletMapper walletMapper;
    @Autowired
    private WalletTransferRequestMapper walletTransferRequestMapper;
    @Autowired
    private SubwalletHistoryMapper subwalletHistoryMapper;

    @Around("@annotation(LogGameTransaction)")
    @Transactional(isolation = REPEATABLE_READ)
    public Object aroundWalletServiceGameTransaction(ProceedingJoinPoint point) {
        int playerId = (int) point.getArgs()[0];
        int gameApiId = (int) point.getArgs()[1];
        BigDecimal amount = (BigDecimal) point.getArgs()[2];
        boolean isDeposit = point.getSignature().getName().contains("deposit");

        WalletTransferRequest walletTransferRequest = new WalletTransferRequest(playerId, gameApiId, isDeposit ? amount : amount.negate(), WalletTransferStatusEnum.CREATE);
        walletTransferRequestMapper.insert(walletTransferRequest);

        Wallet wallet = walletMapper.get(playerId, gameApiId);
        String logString = point.getSignature().getName() +
                " playerId: [{}], amount: [{}], gameApiId: [{}]";

        // If pendingWallet > 0, then reject the transaction
        if (wallet.getPendingWallet().compareTo(BigDecimal.ZERO) != 0) {
            logger.info(logString + ", but pendingWallet is not empty",
                    playerId, amount, gameApiId);
            return null;
        }
        ApiResult apiResult = null;

        try {
            walletTransferRequestMapper.updateStatus(walletTransferRequest.getId(), WalletTransferStatusEnum.PROCESSING);
            // Execute point cut method
            apiResult = (ApiResult) point.proceed();

            if (apiResult.isSuccess()) {
                walletTransferRequestMapper.updateStatus(walletTransferRequest.getId(), WalletTransferStatusEnum.SUCCESS);
                logger.info(logString + ", is Success", playerId, amount, gameApiId);
            } else {
                walletTransferRequestMapper.updateStatus(walletTransferRequest.getId(), WalletTransferStatusEnum.FAIL);
                logger.warn(logString + ", is Failed", playerId, amount, gameApiId);
            }
        } catch (Throwable e) {
            walletTransferRequestMapper.updateStatus(walletTransferRequest.getId(), WalletTransferStatusEnum.PENDING);
            logger.error(logString + ", API ERROR:[{}]", playerId, amount, gameApiId, e.getStackTrace());
        }

        return apiResult;
    }

    @Around("@annotation(LogSyncBalance)")
    public Object aroundSyncBalance(ProceedingJoinPoint point) throws Throwable {
        int gameApiId = (int) point.getArgs()[0];
        Player player = (Player) point.getArgs()[1];
        // Execute point cut method
        GameApiResult apiResult = (GameApiResult) point.proceed();

        // Write transaction log
        Wallet wallet = walletMapper.get(player.getId(), gameApiId);
        if (wallet == null) {
            wallet = new Wallet(player.getId(), gameApiId);
            walletMapper.insert(wallet);
        }
        SubwalletHistory record = new SubwalletHistory();
        record.setPlayerId(player.getId());
        record.setGameApiId(gameApiId);
        record.setBalanceBefore(wallet.getBalance());
        record.setSyncSuccess(apiResult.isSuccess());
        record.setBalanceAfter(apiResult.getBalance());
        subwalletHistoryMapper.insert(record);
        return apiResult;
    }

}
