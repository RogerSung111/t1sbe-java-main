package com.tripleonetech.sbe.player.wallet;

import com.tripleonetech.sbe.player.PlayerIdView;

import java.util.ArrayList;
import java.util.List;

public class PlayerWalletsView {
    private PlayerIdView player;
    private List<GameWalletView> wallets;

    public PlayerIdView getPlayer() {
        return player;
    }

    public void setPlayer(PlayerIdView player) {
        this.player = player;
    }

    public List<GameWalletView> getWallets() {
        if (wallets == null) {
            wallets = new ArrayList<>();
        }
        return wallets;
    }

    public void setWallets(List<GameWalletView> wallets) {
        this.wallets = wallets;
    }

    public void addWallet(Wallet wallet) {
        getWallets().add(GameWalletView.create(wallet, getPlayer().getCurrency()));
    }
}
