package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PlayerMapper {
    Player get(int playerId);


    List<PlayerIdView> suggest(@Param("usernameSearch") String usernameSearch,
                               @Param("currency") SiteCurrencyEnum currency,
                               @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    Player getByUniqueKey(@Param("username") String username, @Param("currency") SiteCurrencyEnum currency);

    Integer getPlayerIdByUniqueKey(@Param("username") String username, @Param("currency") SiteCurrencyEnum currency);

    Integer getPlayerIdByCredentialId(@Param("playerCredentialId") int playerCredentialId,
                                    @Param("currency") SiteCurrencyEnum currency);

    // Transforms player credential ID into player ID. Note that parameter playerCredentialIds cannot be empty
    List<Integer> getPlayerIdsByCredentialIds(@Param("playerCredentialIds") List<Integer> playerCredentialIds,
                                              @Param("currency") SiteCurrencyEnum currency);
    List<Integer> getPlayerIdsByCredentialIds(@Param("playerCredentialIds") List<Integer> playerCredentialIds);
    
    List<Player> listByCurrency(SiteCurrencyEnum currency);

    List<Player> listByUsername(String username);

    List<Player> listByCredentialId(@Param("credentialId") Integer credentialId,
                                    @Param("currency") SiteCurrencyEnum currency);

    List<Player> listByPlayerIds(@Param("playerIds") List<Integer> playerIds, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    List<Player> listByPlayerIds(@Param("playerIds") List<Integer> playerIds);

    List<ReferrerPlayer> getReferralPlayerByPlayerIds(@Param("playerIds") List<Integer> playerIds);

    List<PlayerIdView> listViewByPlayerIds(@Param("playerIds") List<Integer> playerIds,
                                         @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    // 1. Perform queryCredential to return correct rows with page info
    List<PlayerMultiCurrencyView> queryCredential(@Param("query") PlayerListQueryForm query,
                    @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    // 2. Perform actual query with all details. Note: credentialIds should not be empty.
    List<PlayerMultiCurrencyView> query(@Param("credentialIds") List<Integer> credentialIds, @Param("query") PlayerListQueryForm query);

    int insert(Player newPlayer);

    int update(Player updatedPlayer);

    int updateStatus(@Param("playerId") Integer playerId, @Param("status") PlayerStatusEnum status);

    int updateStatusBatch(@Param("playerIds") List<Integer> playerIds, @Param("status") PlayerStatusEnum status);

    int updateCashbackEnabled(@Param("playerId") Integer playerId, @Param("enabled") boolean enabled);

    int updateCashbackEnabledBatch(@Param("playerIds") List<Integer> playerIds, @Param("enabled") boolean enabled);

    int updateCampaignEnabled(@Param("playerId") Integer playerId, @Param("enabled") boolean enabled);

    int updateCampaignEnabledBatch(@Param("playerIds") List<Integer> playerIds, @Param("enabled") boolean enabled);

    int updateWithdrawEnabled(@Param("playerId") Integer playerId, @Param("enabled") boolean enabled);

    int updateWithdrawEnabledBatch(@Param("playerIds") List<Integer> playerIds, @Param("enabled") boolean enabled);

    void updatePassword(@Param("playerId") int playerId, @Param("passwordEncrypt") String passwordEncrypt);

    int updateLastLoginInfo(Player player);

    // TODO: Needs refactor
    Long calculateData(Map<String, Object> params);

}
