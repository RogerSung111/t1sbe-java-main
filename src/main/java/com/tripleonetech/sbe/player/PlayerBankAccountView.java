package com.tripleonetech.sbe.player;

import org.springframework.beans.BeanUtils;

public class PlayerBankAccountView {
    private Integer id;
    private Integer bankId;
    private String accountHolderName;
    private String accountNumber;
    private String bankBranch;
    private boolean defaultAccount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public boolean isDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(boolean defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public static PlayerBankAccountView from(PlayerBankAccount account) {
        PlayerBankAccountView view = new PlayerBankAccountView();
        BeanUtils.copyProperties(account, view);
        return view;
    }
}
