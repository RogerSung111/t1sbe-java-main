package com.tripleonetech.sbe.player;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PlayerStats {
    private Integer id;
    private Integer playerId;
    private Integer totalDepositCount;
    private BigDecimal totalDepositAmount;
    private LocalDateTime firstDepositDate;
    private LocalDateTime lastDepositDate;
    private Integer totalWithdrawCount;
    private BigDecimal totalWithdrawAmount;
    private LocalDateTime firstWithdrawDate;
    private LocalDateTime lastWithdrawDate;
    private BigDecimal totalCashbackBonus;
    private BigDecimal totalReferralBonus;
    private BigDecimal totalCampaignBonus;
    private Integer dirty;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getTotalDepositCount() {
        return totalDepositCount;
    }

    public void setTotalDepositCount(Integer totalDepositCount) {
        this.totalDepositCount = totalDepositCount;
    }

    public BigDecimal getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public LocalDateTime getFirstDepositDate() {
        return firstDepositDate;
    }

    public void setFirstDepositDate(LocalDateTime firstDepositDate) {
        this.firstDepositDate = firstDepositDate;
    }

    public LocalDateTime getLastDepositDate() {
        return lastDepositDate;
    }

    public void setLastDepositDate(LocalDateTime lastDepositDate) {
        this.lastDepositDate = lastDepositDate;
    }

    public Integer getTotalWithdrawCount() {
        return totalWithdrawCount;
    }

    public void setTotalWithdrawCount(Integer totalWithdrawCount) {
        this.totalWithdrawCount = totalWithdrawCount;
    }

    public BigDecimal getTotalWithdrawAmount() {
        return totalWithdrawAmount;
    }

    public void setTotalWithdrawAmount(BigDecimal totalWithdrawAmount) {
        this.totalWithdrawAmount = totalWithdrawAmount;
    }

    public LocalDateTime getFirstWithdrawDate() {
        return firstWithdrawDate;
    }

    public void setFirstWithdrawDate(LocalDateTime firstWithdrawDate) {
        this.firstWithdrawDate = firstWithdrawDate;
    }

    public LocalDateTime getLastWithdrawDate() {
        return lastWithdrawDate;
    }

    public void setLastWithdrawDate(LocalDateTime lastWithdrawDate) {
        this.lastWithdrawDate = lastWithdrawDate;
    }

    public BigDecimal getTotalCashbackBonus() {
        return totalCashbackBonus;
    }

    public void setTotalCashbackBonus(BigDecimal totalCashbackBonus) {
        this.totalCashbackBonus = totalCashbackBonus;
    }

    public BigDecimal getTotalReferralBonus() {
        return totalReferralBonus;
    }

    public void setTotalReferralBonus(BigDecimal totalReferralBonus) {
        this.totalReferralBonus = totalReferralBonus;
    }

    public BigDecimal getTotalCampaignBonus() {
        return totalCampaignBonus;
    }

    public void setTotalCampaignBonus(BigDecimal totalCampaignBonus) {
        this.totalCampaignBonus = totalCampaignBonus;
    }

    public Integer getDirty() {
        return dirty;
    }

    public void setDirty(Integer dirty) {
        this.dirty = dirty;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
