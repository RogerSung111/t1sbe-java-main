package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Player extends PlayerCredential implements com.tripleonetech.sbe.game.Player, Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -469414178239163683L;
    private Integer id;
    private Integer credentialId;
    private SiteCurrencyEnum currency;
    private PlayerStatusEnum status;
    private boolean cashbackEnabled;
    private boolean campaignEnabled;
    private Boolean withdrawEnabled;
    private LocalDateTime lastLoginTime;
    private DeviceTypeEnum lastLoginDevice;
    private String lastLoginIp;
    private PlayerRiskEnum risk;
    private Integer groupId;
    private List<Integer> tagIds;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Integer credentialId) {
        this.credentialId = credentialId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public void setLastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public DeviceTypeEnum getLastLoginDevice() {
        return lastLoginDevice;
    }

    public void setLastLoginDevice(DeviceTypeEnum lastLoginDevice) {
        this.lastLoginDevice = lastLoginDevice;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public PlayerStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PlayerStatusEnum status) {
        this.status = status;
    }

    public boolean getCashbackEnabled() {
        return cashbackEnabled;
    }

    public void setCashbackEnabled(boolean cashbackEnabled) {
        this.cashbackEnabled = cashbackEnabled;
    }

    public boolean getCampaignEnabled() {
        return campaignEnabled;
    }

    public void setCampaignEnabled(boolean campaignEnabled) {
        this.campaignEnabled = campaignEnabled;
    }

    public Boolean getWithdrawEnabled() {
        return withdrawEnabled;
    }

    public void setWithdrawEnabled(Boolean withdrawEnabled) {
        this.withdrawEnabled = withdrawEnabled;
    }

    public ZonedDateTime getLastLoginTime() {
        return DateUtils.localToZoned(lastLoginTime);
    }

    public LocalDateTime getLastLoginTimeLocal() {
        return lastLoginTime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(String tagIds) {
        if (tagIds == null) {
            this.tagIds = new ArrayList<>();
        } else {
            this.tagIds = Arrays.stream(tagIds.split(",")).map(e -> Integer.parseInt(e)).collect(Collectors.toList());
        }
    }

    // -- Virtual Properties --
    public boolean isEnabled() {
        return PlayerStatusEnum.ACTIVE == this.getStatus() &&
                PlayerStatusEnum.ACTIVE == this.getGlobalStatus();
    }
    
    public static Player create(String username, SiteCurrencyEnum currency, PlayerStatusEnum status) {
        Player player = new Player();
        player.setUsername(username);
        player.setCurrency(currency);
        player.setStatus(status);
        return player;
    }
}
