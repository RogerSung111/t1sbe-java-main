<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.tripleonetech.sbe.player.PlayerProfileMapper">
    <resultMap id="playerInfoViewResult" type="com.tripleonetech.sbe.player.PlayerInfoView" autoMapping="true">
        <id property="id" column="id" />
        <association property="group" javaType="com.tripleonetech.sbe.player.PlayerInfoView$PlayerGroup">
            <id property="id" column="group_id" />
            <result property="name" column="group_name" />
        </association>
    </resultMap>

    <resultMap id="apiPlayerInfoViewResult" type="com.tripleonetech.sbe.player.ApiPlayerProfileView" autoMapping="true">
        <association property="referer" javaType="com.tripleonetech.sbe.player.RefererView">
            <id property="id" column="rpc_id" />
            <result property="username" column="rpc_username" />
        </association>
    </resultMap>

    <resultMap id="playerMultiCurrencyProfileViewResult" type="com.tripleonetech.sbe.player.PlayerMultiCurrencyProfileView" autoMapping="true">
        <id property="id" column="credential_id" />
        <result column="global_status" jdbcType="INTEGER" property="status" />
        <association property="referer" javaType="com.tripleonetech.sbe.player.RefererView">
            <id property="id" column="rpc_id" />
            <result property="username" column="rpc_username" />
        </association>
        <collection property="balances" ofType="com.tripleonetech.sbe.player.PlayerMultiCurrency$BalanceView" autoMapping="true">
            <id property="currency" column="currency" />
        </collection>
        <collection property="profiles" ofType="com.tripleonetech.sbe.player.PlayerMultiCurrency$ProfileView" autoMapping="true">
            <id property="currency" column="currency" />
        </collection>
        <collection property="identities" ofType="com.tripleonetech.sbe.player.PlayerMultiCurrency$IdentityView" autoMapping="true">
            <id property="currency" column="currency" />
        </collection>
    </resultMap>

    <select id="getPlayerInfo" parameterType="int" resultMap="playerInfoViewResult">
        SELECT
            player.id,
            player.username,
            player.currency,
            player_credential.password_pending,
            player_tag.id AS group_id,
            player_tag.name AS group_name,
            player_profile.phone_verified,
            player_credential.email_verified,
            NOT(ISNULL(player_credential.verification_question_id)) AS password_question_exists,
            NOT(ISNULL(player_credential.withdraw_password_hash)) AS withdraw_password_exists
        FROM
            player
            INNER JOIN player_credential ON player.username = player_credential.username
            LEFT JOIN player_profile ON player.id = player_profile.player_id
            LEFT JOIN players_player_tag ON players_player_tag.player_id = player.id
            LEFT JOIN player_tag ON player_tag.id = players_player_tag.player_tag_id AND player_tag.player_group = 1
        WHERE
            player.id = #{playerId}
            AND player.status != 9 <!-- exclude CURRENCY_INACTIVE -->
        ORDER BY ISNULL(group_id) LIMIT 1 <!-- only grab the first row, non-null groups if exists -->
    </select>

    <select id="getIdentityViewByPlayerId" parameterType="int" resultType="com.tripleonetech.sbe.player.PlayerMultiCurrency$IdentityView">
        SELECT
            player.currency,
            <!-- identity view -->
            player.created_at,
            player_profile.gender,
            player_profile.first_name,
            player_profile.last_name,
            player_profile.birthday,
            player_profile.address,
            player_profile.city,
            player_profile.country_code,
            player_profile.country_phone_code,
            player_profile.phone_number
        FROM
            player
            INNER JOIN player_profile ON player.id = player_profile.player_id
        WHERE
            player.id = #{playerId}
            AND player.status != 9 <!-- exclude CURRENCY_INACTIVE -->
    </select>

    <select id="getProfileViewByCredentialId" parameterType="int" resultMap="playerMultiCurrencyProfileViewResult">
        SELECT
            pc.id as credential_id,
            pc.username,
            pc.global_status,
            pc.password_pending,
            <!-- ID -->
            player.currency,
            <!-- profile view -->
            player.status,
            player.cashback_enabled,
            player.campaign_enabled,
            player.created_at,
            player.risk,
            GROUP_CONCAT(DISTINCT player_groups.player_tag_id) AS `group`,
            GROUP_CONCAT(DISTINCT player_tags.player_tag_id) AS `tags_list`,
            player_profile.language,
            player_profile.line,
            player_profile.skype,
            player_profile.qq,
            player_profile.wechat,
            player_profile.contact_preferences,
            <!-- balances -->
            MAX(IFNULL(wallet.balance, 0)) as total,
            COUNT(DISTINCT(deposit_request.id)) AS depositCount,
            COUNT(DISTINCT(withdraw_request.id)) AS withdrawCount,
            IFNULL((SELECT SUM(bonus_amount) FROM promo_bonus WHERE status = 0 AND player_id = player.id GROUP BY player_id), 0) AS pendingBonus,
            <!-- identities -->
            player.created_at,
            player_profile.gender,
            player_profile.first_name,
            player_profile.last_name,
            player_profile.birthday,
            player_profile.address,
            player_profile.city,
            player_profile.country_code,
            player_profile.country_phone_code,
            player_profile.phone_number,
            rpc.id rpc_id,
            rpc.username rpc_username
        FROM
            player_credential pc
            INNER JOIN player ON pc.username = player.username AND player.status != 9 <!-- exclude CURRENCY_INACTIVE -->
            INNER JOIN player_profile ON player.id = player_profile.player_id
            INNER JOIN player_tag
            LEFT JOIN players_player_tag player_groups ON (player.id = player_groups.player_id AND player_groups.player_tag_id = player_tag.id AND player_tag.player_group = 1)
            LEFT JOIN players_player_tag player_tags ON (player.id = player_tags.player_id AND player_tags.player_tag_id = player_tag.id AND player_tag.player_group = 0)
            LEFT JOIN wallet ON wallet.player_id = player.id AND game_api_id IS NULL
            LEFT JOIN deposit_request ON deposit_request.player_id = player.id AND deposit_request.status = 0
            LEFT JOIN withdraw_request ON withdraw_request.player_id = player.id AND withdraw_request.status = 0
            LEFT JOIN player_credential rpc ON pc.referral_player_credential_id = rpc.id
        WHERE
            pc.id = #{playerCredentialId}
        GROUP BY
            player.id
        ORDER BY
            player.currency
    </select>

    <select id="getProfileViewByPlayerId" parameterType="int" resultType="com.tripleonetech.sbe.player.PlayerMultiCurrency$ProfileView">
        SELECT
            player.currency,
            <!-- profile view -->
            player.status,
            player.cashback_enabled,
            player.campaign_enabled,
            player.risk,
            player.created_at,
            GROUP_CONCAT(player_groups.player_tag_id) AS `group`,
            GROUP_CONCAT(player_tags.player_tag_id) AS `tags_list`,
            player_profile.language,
            player_profile.line,
            player_profile.skype,
            player_profile.qq,
            player_profile.wechat,
            player_profile.contact_preferences
        FROM
        player
        INNER JOIN player_profile ON player.id = player_profile.player_id
        INNER JOIN player_tag
        LEFT JOIN players_player_tag player_groups ON (player.id = player_groups.player_id AND player_groups.player_tag_id = player_tag.id AND player_tag.player_group = 1)
        LEFT JOIN players_player_tag player_tags ON (player.id = player_tags.player_id AND player_tags.player_tag_id = player_tag.id AND player_tag.player_group = 0)
        LEFT JOIN wallet ON wallet.player_id = player.id AND game_api_id IS NULL
        WHERE
            player.status != 9 <!-- exclude CURRENCY_INACTIVE -->
            AND player.id = #{playerId}
    </select>
    <select id="getApiProfileViewByPlayerId" parameterType="int" resultMap="apiPlayerInfoViewResult">
        SELECT
            player.currency,
            <!-- profile view -->
            player.status,
            player.cashback_enabled,
            player.campaign_enabled,
            player.risk,
            player.created_at,
            GROUP_CONCAT(player_groups.player_tag_id) AS `group`,
            GROUP_CONCAT(player_tags.player_tag_id) AS `tags_list`,
            player_profile.language,
            player_profile.line,
            player_profile.skype,
            player_profile.qq,
            player_profile.wechat,
            player_profile.gender,
            player_profile.first_name,
            player_profile.last_name,
            player_profile.birthday,
            player_profile.address,
            player_profile.city,
            player_profile.country_code,
            player_profile.country_phone_code,
            player_profile.phone_number,
            player_profile.contact_preferences,
            pc.referral_code,
            rpc.id rpc_id,
            rpc.username rpc_username
        FROM
        player
        INNER JOIN player_credential pc ON player.username = pc.username
        INNER JOIN player_profile ON player.id = player_profile.player_id
        INNER JOIN player_tag
        LEFT JOIN players_player_tag player_groups ON (player.id = player_groups.player_id AND player_groups.player_tag_id = player_tag.id AND player_tag.player_group = 1)
        LEFT JOIN players_player_tag player_tags ON (player.id = player_tags.player_id AND player_tags.player_tag_id = player_tag.id AND player_tag.player_group = 0)
        LEFT JOIN wallet ON wallet.player_id = player.id AND game_api_id IS NULL
        LEFT JOIN player_credential rpc ON pc.referral_player_credential_id = rpc.id
        WHERE
            player.status != 9 <!-- exclude CURRENCY_INACTIVE -->
            AND player.id = #{playerId}
    </select>

    <select id="getByPlayerId" parameterType="int" resultType="com.tripleonetech.sbe.player.PlayerProfile">
        SELECT
            *
        FROM
            player_profile
        WHERE
            player_profile.player_id = #{playerId}
    </select>

    <insert id="insert" useGeneratedKeys="true" keyProperty="id">
        insert into player_profile
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="playerId != null">
                player_id,
            </if>
            <if test="firstName != null">
                first_name,
            </if>
            <if test="lastName != null">
                last_name,
            </if>
            <if test="birthday != null">
                birthday,
            </if>
            <if test="language != null">
                `language`,
            </if>
            <if test="gender != null">
                gender,
            </if>
            <if test="address != null">
                address,
            </if>
            <if test="city != null">
                city,
            </if>
            <if test="countryCode != null">
                country_code,
            </if>
            <if test="countryPhoneCode != null">
                country_phone_code,
            </if>
            <if test="line != null">
                line,
            </if>
            <if test="skype != null">
                skype,
            </if>
            <if test="qq != null">
                qq,
            </if>
            <if test="wechat != null">
                wechat,
            </if>
            <if test="contactPreferences != null">
                contact_preferences,
            </if>
            <if test="phoneNumber != null">
                phone_number,
            </if>
            <if test="phoneVerified != null">
                phone_verified,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="playerId != null">
                #{playerId,jdbcType=INTEGER},
            </if>
            <if test="firstName != null">
                #{firstName,jdbcType=VARCHAR},
            </if>
            <if test="lastName != null">
                #{lastName,jdbcType=VARCHAR},
            </if>
            <if test="birthday != null">
                #{birthday,jdbcType=DATE},
            </if>
            <if test="language != null">
                #{language, jdbcType=VARCHAR},
            </if>
            <if test="gender != null">
                #{gender,jdbcType=CHAR},
            </if>
            <if test="address != null">
                #{address,jdbcType=VARCHAR},
            </if>
            <if test="city != null">
                #{city,jdbcType=VARCHAR},
            </if>
            <if test="countryCode != null">
                #{countryCode,jdbcType=CHAR},
            </if>
            <if test="countryPhoneCode != null">
                #{countryPhoneCode,jdbcType=VARCHAR},
            </if>
            <if test="line != null">
                #{line,jdbcType=VARCHAR},
            </if>
            <if test="skype != null">
                #{skype,jdbcType=VARCHAR},
            </if>
            <if test="qq != null">
                #{qq,jdbcType=VARCHAR},
            </if>
            <if test="wechat != null">
                #{wechat,jdbcType=VARCHAR},
            </if>
            <if test="contactPreferences != null">
                #{contactPreferences},
            </if>
            <if test="phoneNumber != null">
                #{phoneNumber, jdbcType=VARCHAR},
            </if>
            <if test="phoneVerified != null">
                #{phoneVerified, jdbcType=TINYINT},
            </if>
        </trim>
    </insert>

    <update id="update">
        UPDATE player_profile
        SET
        <trim prefix="" suffix="" suffixOverrides=",">
            <if test="firstName != null">
                first_name = #{firstName},
            </if>
            <if test="lastName != null">
                last_name = #{lastName},
            </if>
            <if test="birthday != null">
                birthday = #{birthday},
            </if>
            <if test="language != null">
                `language` = #{language},
            </if>
            <if test="gender != null">
                gender = #{gender},
            </if>
            <if test="address != null">
                address = #{address},
            </if>
            <if test="city != null">
                city = #{city},
            </if>
            <if test="countryCode != null">
                country_code = #{countryCode},
            </if>
            <if test="countryPhoneCode != null">
                country_phone_code = #{countryPhoneCode},
            </if>
            <if test="line != null">
                line = #{line},
            </if>
            <if test="skype != null">
                skype = #{skype},
            </if>
            <if test="qq != null">
                qq = #{qq},
            </if>
            <if test="wechat != null">
                wechat = #{wechat},
            </if>
            <if test="phoneNumber != null">
                phone_number = #{phoneNumber},
            </if>
            <if test="phoneVerified != null">
                phone_verified = #{phoneVerified},
            </if>
            <if test="contactPreferences != null">
                contact_preferences = #{contactPreferences},
            </if>
        </trim>
        WHERE player_id = #{playerId}
    </update>
</mapper>
