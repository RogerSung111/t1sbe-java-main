package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

@Mapper
public interface PlayerStatsMapper {

    PlayerStats getByPlayerId(Integer playerId);

    int upsertDirty(@Param("list") Collection<Integer> playerIds,
            @Param("playerStatsType") PlayerStatsTypeEnum playerStatsType);

    int calculateDirtyWalletAmount(@Param("playerStatsType") PlayerStatsTypeEnum playerStatsType);

    int calculateDirtyBonusAmount(@Param("playerStatsType") PlayerStatsTypeEnum playerStatsType);

    List<Integer> getPlayerIdByFilterDeposit(@Param("currency") SiteCurrencyEnum currency,
            @Param("operator") String operator, @Param("value") String value);
}
