package com.tripleonetech.sbe.player;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class PlayerBankAccountForm {
    @NotNull
    @ApiModelProperty(value = "Bank ID", required = true)
    private Integer bankId;
    @NotNull
    @ApiModelProperty(value = "Bank account number", required = true)
    private String accountNumber;
    @NotNull
    @ApiModelProperty(value = "Bank account holder name", required = true)
    private String accountHolderName;
    @ApiModelProperty(value = "Whether this is the default bank account for current player")
    private boolean defaultAccount;
    @ApiModelProperty(value = "Bank's branch name")
    private String bankBranch;

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public boolean isDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(boolean defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }
}
