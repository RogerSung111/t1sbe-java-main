package com.tripleonetech.sbe.player.scheduled;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.player.PlayerStatsMapper;
import com.tripleonetech.sbe.player.PlayerStatsTypeEnum;

@Profile("scheduled")
@Component
public class PlayerStatsRunner {

    @Autowired
    private PlayerStatsMapper playerStatsMapper;

    @Scheduled(fixedDelay = 10 * 60 * 1000) // 10 minutes
    public void runCaculation() {
        this.caculateDirtyWalletAmount();
        this.caculateDirtyBonusAmount();
    }

    private void caculateDirtyWalletAmount() {
        List<PlayerStatsTypeEnum> playerStatsTypes = Arrays.asList(
                PlayerStatsTypeEnum.DEPOSIT_AMOUNT,
                PlayerStatsTypeEnum.WITHDRAW_AMOUNT);
        playerStatsTypes.forEach(playerStatsMapper::calculateDirtyWalletAmount);
    }

    private void caculateDirtyBonusAmount() {
        List<PlayerStatsTypeEnum> playerStatsTypes = Arrays.asList(
                PlayerStatsTypeEnum.CASHBACK_BONUS,
                PlayerStatsTypeEnum.REFERRAL_BONUS,
                PlayerStatsTypeEnum.CAMPAIGN_BONUS);
        playerStatsTypes.forEach(playerStatsMapper::calculateDirtyBonusAmount);
    }
}
