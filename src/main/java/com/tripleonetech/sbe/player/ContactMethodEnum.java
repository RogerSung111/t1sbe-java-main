package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum ContactMethodEnum implements BaseCodeEnum {
    EMAIL(1), SMS(2), PHONE_CALL(4), LETTER(8);

    private Integer code;

    ContactMethodEnum(Integer code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

}
