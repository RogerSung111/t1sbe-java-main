package com.tripleonetech.sbe.player.report;

import com.tripleonetech.sbe.common.web.TimeRangeForm;

import io.swagger.annotations.ApiModelProperty;

public class PlayerSummaryReportForm extends TimeRangeForm {
    @ApiModelProperty(value = "Whether data should be grouped by device")
    private boolean byDevice;

    public boolean isByDevice() {
        return byDevice;
    }

    public void setByDevice(boolean byDevice) {
        this.byDevice = byDevice;
    }
}
