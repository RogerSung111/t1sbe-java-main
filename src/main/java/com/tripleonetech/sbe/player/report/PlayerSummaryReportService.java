package com.tripleonetech.sbe.player.report;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.player.PlayerCredential;

@Service
public class PlayerSummaryReportService {

    private static final Logger logger = LoggerFactory.getLogger(PlayerSummaryReportService.class);

    @Autowired
    private PlayerSummaryReportMapper mapper;

    public void upsertCount(PlayerCredential playerCredential) {
        int lastTotalCount = ObjectUtils.defaultIfNull(mapper.getLastTotalCount(playerCredential.getRegisterDevice()),
                0);
        LocalDateTime timeHour = playerCredential.getCreatedAtLocal().truncatedTo(ChronoUnit.HOURS);
        mapper.addOneCount(timeHour, lastTotalCount + 1, playerCredential.getRegisterDevice());
    }
}
