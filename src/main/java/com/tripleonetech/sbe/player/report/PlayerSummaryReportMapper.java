package com.tripleonetech.sbe.player.report;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;

@Mapper
public interface PlayerSummaryReportMapper {

    List<PlayerSummaryReportView> list(
            @Param("query") PlayerSummaryReportForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    Integer getLastTotalCount(DeviceTypeEnum registerDevice);

    int addOneCount(@Param("timeHour") LocalDateTime timeHour, @Param("totalCount") int totalCount,
            @Param("device") DeviceTypeEnum registerDevice);

}