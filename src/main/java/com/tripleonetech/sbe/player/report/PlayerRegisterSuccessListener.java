package com.tripleonetech.sbe.player.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.player.PlayerCredential;
import com.tripleonetech.sbe.player.PlayerRegisterSuccessEvent;

@Profile("listener")
@Component
public class PlayerRegisterSuccessListener {

    private static final Logger logger = LoggerFactory.getLogger(PlayerRegisterSuccessListener.class);

    @Autowired
    private PlayerSummaryReportService service;

    @Async
    @EventListener
    public void listen(PlayerRegisterSuccessEvent event) {
        logger.info("Player register success event：" + event.getSource());
        service.upsertCount((PlayerCredential) event.getSource());
    }
}
