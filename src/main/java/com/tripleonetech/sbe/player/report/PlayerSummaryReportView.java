package com.tripleonetech.sbe.player.report;

import java.time.LocalDate;

import org.apache.commons.lang3.ObjectUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;

import io.swagger.annotations.ApiModelProperty;

public class PlayerSummaryReportView {
    @ApiModelProperty(value = "Date of player summary record")
    private LocalDate date;
    @ApiModelProperty(value = "Total players")
    private int totalCount;
    @ApiModelProperty(value = "Newly registered players")
    private int newCount;
    @JsonInclude(Include.NON_NULL)
    @ApiModelProperty(value = "Player's device")
    private DeviceTypeEnum device;

    @ApiModelProperty(value = "Unique id for this data, used by frontend")
    public String getId() {
        return String.format("%s-%s", date.toString(),
                ObjectUtils.defaultIfNull(device, DeviceTypeEnum.UNKNOWN).getCode());
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getNewCount() {
        return newCount;
    }

    public void setNewCount(int newCount) {
        this.newCount = newCount;
    }

    public DeviceTypeEnum getDevice() {
        return device;
    }

    public void setDevice(DeviceTypeEnum device) {
        this.device = device;
    }

}
