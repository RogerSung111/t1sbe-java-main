package com.tripleonetech.sbe.player;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class PlayerAccountTransactionSummaryView {

    @JsonIgnore
    private Integer playerId;
    private SiteCurrencyEnum currency;
    private int depositTotalTimes;
    private BigDecimal depositTotalAmount;
    private LocalDateTime depositLastDate;
    private int withdrawalTotalTimes;
    private BigDecimal withdrawalTotalAmount;
    private LocalDateTime withdrawalLastDate;
    private int bonusTotalTimes;
    private BigDecimal bonusTotalAmount;
    private BigDecimal totalTagBasedCashBack;
    private BigDecimal totalBetBasedCashback;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public int getDepositTotalTimes() {
        return depositTotalTimes;
    }

    public void setDepositTotalTimes(int depositTotalTimes) {
        this.depositTotalTimes = depositTotalTimes;
    }

    public BigDecimal getDepositTotalAmount() {
        return depositTotalAmount;
    }

    public void setDepositTotalAmount(BigDecimal depositTotalAmount) {
        this.depositTotalAmount = depositTotalAmount;
    }

    public ZonedDateTime getDepositLastDate() {
        return DateUtils.localToZoned(depositLastDate);
    }

    public void setDepositLastDate(ZonedDateTime depositLastDate) {
        this.depositLastDate = DateUtils.zonedToLocal(depositLastDate);
    }

    public int getWithdrawalTotalTimes() {
        return withdrawalTotalTimes;
    }

    public void setWithdrawalTotalTimes(int withdrawalTotalTimes) {
        this.withdrawalTotalTimes = withdrawalTotalTimes;
    }

    public BigDecimal getWithdrawalTotalAmount() {
        return withdrawalTotalAmount;
    }

    public void setWithdrawalTotalAmount(BigDecimal withdrawalTotalAmount) {
        this.withdrawalTotalAmount = withdrawalTotalAmount;
    }

    public ZonedDateTime getWithdrawalLastDate() {
        return DateUtils.localToZoned(withdrawalLastDate);
    }

    public void setWithdrawalLastDate(ZonedDateTime withdrawalLastDate) {
        this.withdrawalLastDate = DateUtils.zonedToLocal(withdrawalLastDate);
    }

    public int getBonusTotalTimes() {
        return bonusTotalTimes;
    }

    public void setBonusTotalTimes(int bonusTotalTimes) {
        this.bonusTotalTimes = bonusTotalTimes;
    }

    public BigDecimal getBonusTotalAmount() {
        return bonusTotalAmount;
    }

    public void setBonusTotalAmount(BigDecimal bonusTotalAmount) {
        this.bonusTotalAmount = bonusTotalAmount;
    }

    public BigDecimal getTotalTagBasedCashBack() {
        return totalTagBasedCashBack;
    }

    public void setTotalTagBasedCashBack(BigDecimal totalTagBasedCashBack) {
        this.totalTagBasedCashBack = totalTagBasedCashBack;
    }

    public BigDecimal getTotalBetBasedCashback() {
        return totalBetBasedCashback;
    }

    public void setTotalBetBasedCashback(BigDecimal totalBetBasedCashback) {
        this.totalBetBasedCashback = totalBetBasedCashback;
    }

}
