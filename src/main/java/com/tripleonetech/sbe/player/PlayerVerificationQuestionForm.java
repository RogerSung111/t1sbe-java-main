package com.tripleonetech.sbe.player;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class PlayerVerificationQuestionForm {
    @NotNull
    @ApiModelProperty(required = true, name = "verificationQuestionId", value = "Verification question id")
    private Integer verificationQuestionId;
    @Size(min = 3, max = 50)
    @ApiModelProperty(required = true, name = "verificationAnswer", value = "Verification answer")
    private String verificationAnswer;

    public Integer getVerificationQuestionId() {
        return verificationQuestionId;
    }

    public void setVerificationQuestionId(Integer verificationQuestionId) {
        this.verificationQuestionId = verificationQuestionId;
    }

    public String getVerificationAnswer() {
        return verificationAnswer;
    }

    public void setVerificationAnswer(String verificationAnswer) {
        this.verificationAnswer = verificationAnswer;
    }
}
