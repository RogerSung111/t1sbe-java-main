package com.tripleonetech.sbe.player;

import java.util.List;

public class PlayerMultiCurrencyProfileView extends PlayerMultiCurrency {
    private List<BalanceView> balances;
    private List<ProfileView> profiles;
    private List<IdentityView> identities;
    private RefererView referer;

    public List<BalanceView> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceView> balances) {
        this.balances = balances;
    }

    public List<ProfileView> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileView> profiles) {
        this.profiles = profiles;
    }

    public List<IdentityView> getIdentities() {
        return identities;
    }

    public void setIdentities(List<IdentityView> identities) {
        this.identities = identities;
    }

    public RefererView getReferer() {
        return referer;
    }

    public void setReferer(RefererView referer) {
        this.referer = referer;
    }
}
