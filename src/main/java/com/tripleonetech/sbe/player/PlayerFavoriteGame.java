package com.tripleonetech.sbe.player;

public class PlayerFavoriteGame {
    private Integer playerId;
    private Integer gameApiId;
    private String gameCode;

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(Integer gameApiId) {
        this.gameApiId = gameApiId;
    }

    public String getGameCode() {
        return gameCode;
    }

    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }

}
