package com.tripleonetech.sbe.player;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PlayerVerificationQuestionI18nMapper {
    PlayerVerificationQuestionI18n get(@Param("questionId") Integer questionId,
            @Param("locale") String locale,
            @Param("defaultLocale") String defaultLocale);

    List<PlayerVerificationQuestionI18n> list(@Param("locale") String locale,
            @Param("defaultLocale") String defaultLocale);

}
