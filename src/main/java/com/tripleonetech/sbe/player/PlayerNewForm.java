package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.validator.PropertiesPattern;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PlayerNewForm {
    @NotBlank
    @ApiModelProperty(value = "Username", required = true)
    private String username;

    @NotNull
    @ApiModelProperty(value = "Currency", required = true)
    private SiteCurrencyEnum currency;

    @Size(max = 7)
    @ApiModelProperty(value = "player's referral code, optional. It should not be longer than 7 characters.")
    private String referralCode;

    @Size(max = 255)
    @Email
    @ApiModelProperty(value = "Email address", required = true)
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static class PlayerRegisterForm extends PlayerNewForm {
        @PropertiesPattern(property = "validation.password-regex", message = "{validation.password.invalid}")
        @ApiModelProperty(value = "Password - plain text", required = true)
        private String password;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
