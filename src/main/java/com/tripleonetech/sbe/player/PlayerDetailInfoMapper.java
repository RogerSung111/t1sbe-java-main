package com.tripleonetech.sbe.player;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.player.wallet.PlayerWalletLogView;
import com.tripleonetech.sbe.player.wallet.PlayerWalletTransactionView;

@Mapper
public interface PlayerDetailInfoMapper {

    List<PlayerWalletTransactionView> listWalletTransaction(@Param("playerId") Integer playerId,
            @Param("type") Integer type, @Param("createdAtStart") LocalDateTime createdAtStart,
            @Param("createdAtEnd") LocalDateTime createdAtEnd);

    List<PlayerWalletLogView> listWalletLog(@Param("playerId") Integer playerId, @Param("type") Integer type,
            @Param("createdAtStart") LocalDateTime createdAtStart, @Param("createdAtEnd") LocalDateTime createdAtEnd);

    List<PlayerAccountTransactionSummaryView> listTransactionSummary(@Param("playerIds") List<Integer> playerIds);

}
