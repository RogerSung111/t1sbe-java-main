package com.tripleonetech.sbe.player;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class PlayerEmailForm {
    @NotBlank
    @Email
    @ApiModelProperty(value = "Player Email", required = true)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
