package com.tripleonetech.sbe.player;

public class RefererView {

    private Integer id;
    private String username;

    public RefererView() {
    }

    public RefererView(Integer id, String username) {
        this.id = id;
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
