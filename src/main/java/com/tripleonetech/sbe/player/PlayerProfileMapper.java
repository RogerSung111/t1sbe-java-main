package com.tripleonetech.sbe.player;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlayerProfileMapper {
    PlayerMultiCurrency.IdentityView getIdentityViewByPlayerId(int playerId);

    PlayerMultiCurrencyProfileView getProfileViewByCredentialId(int credentialId);

    PlayerMultiCurrency.ProfileView getProfileViewByPlayerId(int playerId);

    ApiPlayerProfileView getApiProfileViewByPlayerId(int playerId);

    PlayerProfile getByPlayerId(int playerId);

    int insert(PlayerProfile newPlayerProfile);

    int update(PlayerProfile updatedPlayerProfile);

    PlayerInfoView getPlayerInfo(int playerId);
}
