package com.tripleonetech.sbe.player;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.report.player.PlayerActivityReport;

import java.time.ZonedDateTime;

public class PlayerInfoView {
    private int id;
    private String username;
    private SiteCurrencyEnum currency;
    private Boolean passwordPending;
    private PlayerGroup group;
    private LoginInfo lastLogin;
    private boolean emailVerified;
    private boolean phoneVerified;
    private boolean withdrawPasswordExists;
    private boolean passwordQuestionExists;
    private int unreadMessageCount;

    public void setActivity(PlayerActivityReport activity) {
        LoginInfo lastLogin = new LoginInfo();
        lastLogin.setIp(activity.getIp());
        lastLogin.setTime(DateUtils.localToZoned(activity.getCreatedAt()));
        // TODO: set location
        this.setLastLogin(lastLogin);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Boolean getPasswordPending() {
        return passwordPending;
    }

    public void setPasswordPending(Boolean passwordPending) {
        this.passwordPending = passwordPending;
    }

    public PlayerGroup getGroup() {
        return group;
    }

    public void setGroup(PlayerGroup group) {
        this.group = group;
    }

    public LoginInfo getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LoginInfo lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public boolean isWithdrawPasswordExists() {
        return withdrawPasswordExists;
    }

    public void setWithdrawPasswordExists(boolean withdrawPasswordExists) {
        this.withdrawPasswordExists = withdrawPasswordExists;
    }

    public boolean isPasswordQuestionExists() {
        return passwordQuestionExists;
    }

    public void setPasswordQuestionExists(boolean passwordQuestionExists) {
        this.passwordQuestionExists = passwordQuestionExists;
    }

    public int getUnreadMessageCount() {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(int unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    static class PlayerGroup {
        private Integer id;
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    static class LoginInfo {
        private String ip;
        private ZonedDateTime time;
        private String location;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public ZonedDateTime getTime() {
            return time;
        }

        public void setTime(ZonedDateTime time) {
            this.time = time;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

    }

}
