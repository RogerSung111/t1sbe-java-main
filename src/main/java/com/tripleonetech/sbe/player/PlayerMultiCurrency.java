package com.tripleonetech.sbe.player;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.integration.DeviceTypeEnum;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlayerMultiCurrency {
    private Integer id;
    private String username;
    private Integer status;
    private Boolean passwordPending;
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getPasswordPending() {
        return passwordPending;
    }

    public void setPasswordPending(Boolean passwordPending) {
        this.passwordPending = passwordPending;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static class ProfileListView {
        private SiteCurrencyEnum currency;
        private Integer status;
        private boolean cashbackEnabled;
        private boolean campaignEnabled;
        private boolean withdrawEnabled;
        private LocalDateTime createdAt;
        private Integer risk;
        private String language;
        private Integer group;
        private List<Integer> tags;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public boolean isCashbackEnabled() {
            return cashbackEnabled;
        }

        public void setCashbackEnabled(boolean cashbackEnabled) {
            this.cashbackEnabled = cashbackEnabled;
        }

        public boolean isCampaignEnabled() {
            return campaignEnabled;
        }

        public void setCampaignEnabled(boolean campaignEnabled) {
            this.campaignEnabled = campaignEnabled;
        }

        public boolean isWithdrawEnabled() {
            return withdrawEnabled;
        }

        public void setWithdrawEnabled(boolean withdrawEnabled) {
            this.withdrawEnabled = withdrawEnabled;
        }

        public ZonedDateTime getCreatedAt() {
            return DateUtils.localToZoned(createdAt);
        }

        public void setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getRisk() {
            return risk;
        }

        public void setRisk(Integer risk) {
            this.risk = risk;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public Integer getGroup() {
            return group;
        }

        public void setGroup(Integer group) {
            this.group = group;
        }

        public List<Integer> getTags() {
            return tags;
        }

        public void setTags(List<Integer> tags) {
            this.tags = tags;
        }

        public void setTagsList(String tagsList) {
            if (tagsList == null) {
                this.tags = new ArrayList<>();
            } else {
                this.tags = Arrays.stream(tagsList.split(","))
                        .map(tag -> Integer.parseInt(tag))
                        .collect(Collectors.toList());
            }
        }
    }

    public static class IdentityView {
        private SiteCurrencyEnum currency;
        private String firstName;
        private String lastName;
        private String gender;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate birthday;
        private String address;
        private String city;
        private String countryCode;
        private String countryPhoneCode;
        private String phoneNumber;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public LocalDate getBirthday() {
            return birthday;
        }

        public void setBirthday(LocalDate birthday) {
            this.birthday = birthday;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getCountryPhoneCode() {
            return countryPhoneCode;
        }

        public void setCountryPhoneCode(String countryPhoneCode) {
            this.countryPhoneCode = countryPhoneCode;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }

    public static class ProfileView extends ProfileListView {
        private String line;
        private String skype;
        private String qq;
        private String wechat;
        private List<Integer> contactPreferenceList;

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getSkype() {
            return skype;
        }

        public void setSkype(String skype) {
            this.skype = skype;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getWechat() {
            return wechat;
        }

        public void setWechat(String wechat) {
            this.wechat = wechat;
        }

        public List<Integer> getContactPreferenceList() {
            return contactPreferenceList;
        }

        public void setContactPreferenceList(List<Integer> contactPreferenceList) {
            this.contactPreferenceList = contactPreferenceList;
        }

        public void setContactPreferences(Integer contactPreferences) {
            if (null == contactPreferences) {
                return;
            }

            this.contactPreferenceList = Stream.of(ContactMethodEnum.values())
                    .filter(e -> (e.getCode() & contactPreferences) > 0)
                    .map(e -> e.getCode())
                    .collect(Collectors.toList());
        }

    }

    public static class ActivityView {
        private SiteCurrencyEnum currency;
        private ZonedDateTime lastLoginTime;
        private DeviceTypeEnum lastLoginDevice;
        private String lastLoginIp;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public ZonedDateTime getLastLoginTime() {
            return lastLoginTime;
        }

        public void setLastLoginTime(ZonedDateTime lastLoginTime) {
            this.lastLoginTime = lastLoginTime;
        }

        public DeviceTypeEnum getLastLoginDevice() {
            return lastLoginDevice;
        }

        public void setLastLoginDevice(DeviceTypeEnum lastLoginDevice) {
            this.lastLoginDevice = lastLoginDevice;
        }

        public String getLastLoginIp() {
            return lastLoginIp;
        }

        public void setLastLoginIp(String lastLoginIp) {
            this.lastLoginIp = lastLoginIp;
        }
    }

    public static class BalanceView {
        private SiteCurrencyEnum currency;
        private BigDecimal total;
        private Integer depositCount;
        private Integer withdrawCount;
        private BigDecimal pendingBonus;

        public SiteCurrencyEnum getCurrency() {
            return currency;
        }

        public void setCurrency(SiteCurrencyEnum currency) {
            this.currency = currency;
        }

        public BigDecimal getTotal() {
            return total;
        }

        public void setTotal(BigDecimal total) {
            this.total = total;
        }

        public Integer getDepositCount() {
            return depositCount;
        }

        public void setDepositCount(Integer depositCount) {
            this.depositCount = depositCount;
        }

        public Integer getWithdrawCount() {
            return withdrawCount;
        }

        public void setWithdrawCount(Integer withdrawCount) {
            this.withdrawCount = withdrawCount;
        }

        public BigDecimal getPendingBonus() {
            return pendingBonus;
        }

        public void setPendingBonus(BigDecimal pendingBonus) {
            this.pendingBonus = pendingBonus;
        }
    }

}
