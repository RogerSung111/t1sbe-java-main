package com.tripleonetech.sbe.player;

import java.math.BigDecimal;

public class PlayerDepositSummaryTargetView {

    private String currency;
    private String symbol;
    private BigDecimal requiredTargetDepositAmount;
    private BigDecimal currentAccumulateDepositAmount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getRequiredTargetDepositAmount() {
        return requiredTargetDepositAmount;
    }

    public void setRequiredTargetDepositAmount(BigDecimal requiredTargetDepositAmount) {
        this.requiredTargetDepositAmount = requiredTargetDepositAmount;
    }

    public BigDecimal getCurrentAccumulateDepositAmount() {
        return currentAccumulateDepositAmount;
    }

    public void setCurrentAccumulateDepositAmount(BigDecimal currentAccumulateDepositAmount) {
        this.currentAccumulateDepositAmount = currentAccumulateDepositAmount;
    }

}
