package com.tripleonetech.sbe.player;

import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PlayerActivityTypeEnum implements BaseCodeEnum {
    LOGIN(1);
    
    private int code;


    PlayerActivityTypeEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }

    
}
