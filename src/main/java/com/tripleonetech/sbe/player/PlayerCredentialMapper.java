package com.tripleonetech.sbe.player;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

@Mapper
public interface PlayerCredentialMapper {
    PlayerCredential get(Integer id);

    PlayerCredential getByUsername(String username);

    PlayerCredential getByPlayerId(Integer playerId);

    PlayerCredential getByReferrerCode(String referralCode);

    int insert(PlayerCredential playerCredential);

    int update(PlayerCredential playerCredential);

    int updatePasswordEncrypt(@Param("username") String username, @Param("passwordEncrypt") String passwordEncrypt);

    void updateWithdrawPasswordHash(@Param("username") String username, @Param("withdrawPasswordHash") String withdrawPasswordHash);

    int updateStatus(@Param("username") String username, @Param("status") PlayerStatusEnum status);

    int updateLastActivityTime(@Param("username") String username,
            @Param("lastActivityTime") LocalDateTime lastActivityTime);

    int updatePasswordPending(@Param("username") String username, @Param("passwordPending") boolean passwordPending);

    List<PlayerCredential> findNeedAddingPlayers(SiteCurrencyEnum currency);

    List<PlayerCredential> findByReferralCredentialId(Integer credentialId);

    List<Integer> getPlayerIdByFilterCreatedAt(
            @Param("operator") String operator,
            @Param("createdAt") LocalDateTime createdAt);

    int updateVerificationInfo(@Param("username") String username, @Param("form") PlayerVerificationQuestionForm form);
}
