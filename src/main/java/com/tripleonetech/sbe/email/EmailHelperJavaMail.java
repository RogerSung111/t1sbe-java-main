package com.tripleonetech.sbe.email;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailHelperJavaMail implements EmailHelper {
    @Autowired
    private JavaMailSenderImpl mailSender;

    private void setSenderIfBlank(MailModel mailModel) {
        String sender = StringUtils.firstNonBlank(mailModel.getFrom(), mailSender.getUsername());
        mailModel.setFrom(sender);
    }

    @Override
    public void sendMail(MailModel mailModel) throws Exception {
        setSenderIfBlank(mailModel);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setSubject(mailModel.getSubject());
        helper.setText(mailModel.getContent());
        helper.setFrom(mailModel.getFrom());
        helper.setTo(mailModel.getRecipients().toArray(new String[mailModel.getRecipients().size()]));
        mailSender.send(message);
    }

}
