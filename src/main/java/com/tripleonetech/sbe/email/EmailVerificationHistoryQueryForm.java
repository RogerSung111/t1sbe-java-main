package com.tripleonetech.sbe.email;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;

public class EmailVerificationHistoryQueryForm extends PageQueryForm {
    @ApiModelProperty(hidden = true)
    private Long id;
    @ApiModelProperty(value = "Search by player email")
    private String email;
    @ApiModelProperty(value = "Search by player username")
    private String username;
    @ApiModelProperty(value = "Search by verified flag")
    private Boolean verified;
    @ApiModelProperty(value = "Search by created date (start)")
    private LocalDateTime createdAtStart;
    @ApiModelProperty(value = "Search by created date (end)")
    private LocalDateTime createdAtEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }
}
