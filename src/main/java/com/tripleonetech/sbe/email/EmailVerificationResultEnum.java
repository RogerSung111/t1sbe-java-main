package com.tripleonetech.sbe.email;

public enum EmailVerificationResultEnum {
    PASS(true, "Pass"), FAIL_OTP_EXPIRED(false, "OTP code is expired"), FAIL_WRONG_OTP(false,
            "Wrong OTP"), FAIL_OTP_USED(false, "OTP code has already been used");

    private EmailVerificationResultEnum(Boolean pass, String message) {
        this.pass = pass;
        this.message = message;
    }

    private boolean pass;
    private String message;

    public boolean isPass() {
        return pass;
    }

    public void setPass(boolean pass) {
        this.pass = pass;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
