package com.tripleonetech.sbe.email;

public interface EmailHelper {
    public void sendMail(MailModel mailModel) throws Exception;
}
