package com.tripleonetech.sbe.email;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.model.PageQueryForm;

public class EmailHistoryQueryForm extends PageQueryForm {
    @ApiModelProperty(hidden = true)
    private Long id;
    @ApiModelProperty(value = "Search by sender's email")
    private String senderAddress;
    @ApiModelProperty(value = "Search by recipient's(player's) email")
    private String recipientAddress;
    @ApiModelProperty(value = "Search by subject")
    private String subject;
    @ApiModelProperty(value = "Search by email content")
    private String content;
    @ApiModelProperty(value = "Search by player username")
    private String username;
    @ApiModelProperty(value = "Search by created date (start)")
    private LocalDateTime createdAtStart;
    @ApiModelProperty(value = "Search by created date (end)")
    private LocalDateTime createdAtEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress == null ? null : senderAddress.trim();
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress == null ? null : recipientAddress.trim();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(ZonedDateTime createdAtStart) {
        this.createdAtStart = DateUtils.zonedToLocal(createdAtStart);
    }

    public LocalDateTime getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(ZonedDateTime createdAtEnd) {
        this.createdAtEnd = DateUtils.zonedToLocal(createdAtEnd);
    }

}
