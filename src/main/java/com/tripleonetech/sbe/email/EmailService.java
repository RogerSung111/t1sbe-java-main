package com.tripleonetech.sbe.email;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.AlphanumericGenUtil;
import com.tripleonetech.sbe.common.Constant;

@Service
public class EmailService {
    private final static Logger logger = LoggerFactory.getLogger(EmailService.class);
    private static int EMAIL_OTP_EXPIRY_TIME_MINUTE = 5;
    private final static int EMAIL_OTP_CODE_LENGTH = 6;

    @Autowired
    private Constant constant;
    @Autowired
    private EmailHistoryMapper emailHistoryMapper;
    @Autowired
    private EmailVerificationHistoryMapper emailVerificationHistoryMapper;

    private EmailHelper emailHelper;

    @Autowired
    public void setEmailHelper(EmailHelper emailHelper) {
        this.emailHelper = emailHelper;
    }

    private void sendTextMail(String subject, String text, String sendTo, String sendFrom, Integer playerId)
            throws Exception {
        MailModel mailModel = new MailModel();
        mailModel.setSubject(subject);
        mailModel.setContent(text);
        mailModel.setFrom(sendFrom);
        mailModel.addRecipients(sendTo);
        emailHelper.sendMail(mailModel);

        EmailHistory record = EmailHistory.create(mailModel.getFrom(), sendTo, subject, text, playerId);
        emailHistoryMapper.insert(record);
    }

    public String sendVerificationEmail(String emailAddress, Integer playerId) {
        String otpCode = AlphanumericGenUtil.generateRandomNumberCode(EMAIL_OTP_CODE_LENGTH);

        // store email verification token and expiry dateTime
        EmailVerificationHistory emailVerificationHistory = new EmailVerificationHistory();
        if (null != constant.getPlayer().getEmailVerificationOtpValidMinutes()) {
            EMAIL_OTP_EXPIRY_TIME_MINUTE = constant.getPlayer().getEmailVerificationOtpValidMinutes();
        }
        emailVerificationHistory.setExpirationTime(
                LocalDateTime.now().plusMinutes(EMAIL_OTP_EXPIRY_TIME_MINUTE));
        emailVerificationHistory.setEmail(emailAddress);
        emailVerificationHistory.setPlayerId(playerId);
        emailVerificationHistory.setOtpCode(otpCode);
        emailVerificationHistoryMapper.insert(emailVerificationHistory);

        // send email otp code to player
        try {
            sendTextMail("Email Verification!",
                    String.format("Email verification OTP code: %s, expires in %d minutes", otpCode,
                            EMAIL_OTP_EXPIRY_TIME_MINUTE),
                    emailAddress, null, playerId);
            return otpCode;
        } catch (Exception e) {
            // logger
            logger.error("Exception: ", e);
            return null;
        }

    }

    public EmailVerificationResultEnum validateEmailToken(Integer playerId, String otpCode, String emailAddress) {
        EmailVerificationHistory emailVerificationHistory = emailVerificationHistoryMapper.getByPlayerIdAndEmail(playerId, emailAddress);
        if (emailVerificationHistory == null || !otpCode.equals(emailVerificationHistory.getOtpCode())) {
            return EmailVerificationResultEnum.FAIL_WRONG_OTP;
        }
        if (LocalDateTime.now().compareTo(emailVerificationHistory.getExpirationTime()) > 0) {
            return EmailVerificationResultEnum.FAIL_OTP_EXPIRED;
        }
        if (emailVerificationHistory.getVerified()) {
            return EmailVerificationResultEnum.FAIL_OTP_USED;
        }
        emailVerificationHistoryMapper.updateRecordToVerified(emailVerificationHistory.getId());
        return EmailVerificationResultEnum.PASS;
    }
}
