package com.tripleonetech.sbe.email;

import java.time.LocalDateTime;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class EmailHistory {
    private Long id;

    private String senderAddress;

    private String recipientAddress;

    private String subject;

    private String content;

    private Integer playerId;


    private LocalDateTime createdAt;

    //Association columns
    private String username;
    private SiteCurrencyEnum currency;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress == null ? null : senderAddress.trim();
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress == null ? null : recipientAddress.trim();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public static EmailHistory create(String senderAddress, String recipientAddress, String subject, String content, Integer playerId) {

        EmailHistory emailHistory = new EmailHistory();
        emailHistory.setSenderAddress(senderAddress);
        emailHistory.setRecipientAddress(recipientAddress);
        emailHistory.setSubject(subject);
        emailHistory.setContent(content);
        emailHistory.setPlayerId(playerId);
        return emailHistory;
    }
}
