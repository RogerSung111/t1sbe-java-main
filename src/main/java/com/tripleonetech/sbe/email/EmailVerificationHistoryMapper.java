package com.tripleonetech.sbe.email;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface EmailVerificationHistoryMapper {
    int insert(EmailVerificationHistory record);

    EmailVerificationHistory getByPlayerIdAndEmail(@Param("playerId") Integer playerId, @Param("email") String email);

    int updateRecordToVerified(long id);

    List<EmailVerificationHistoryView> listEmailHistory(@Param("queryForm") EmailVerificationHistoryQueryForm form,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);
}
