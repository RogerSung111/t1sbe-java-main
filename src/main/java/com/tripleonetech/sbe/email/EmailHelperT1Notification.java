package com.tripleonetech.sbe.email;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.notification.T1CommonNotification.MessageTypeEnum;
import com.tripleonetech.sbe.common.notification.T1Notification;
import com.tripleonetech.sbe.common.notification.T1NotificationApi;

@Component
public class EmailHelperT1Notification implements EmailHelper {

    @Autowired
    private Constant constant;
    @Autowired
    private T1NotificationApi notificationApi;

    private void setSenderIfBlank(MailModel mailModel) {
        String sender = StringUtils.firstNonBlank(mailModel.getFrom(), constant.getT1Notification().getMailUsername());
        mailModel.setFrom(sender);
    }

    @Override
    public void sendMail(MailModel mailModel) throws Exception {
        setSenderIfBlank(mailModel);

        T1Notification notification = new T1Notification()
                .type(MessageTypeEnum.MAIL_TXT)
                .subject(mailModel.getSubject())
                .content(mailModel.getContent())
                .sender(mailModel.getFrom())
                .addRecipients(mailModel.getRecipients().toArray(new String[mailModel.getRecipients().size()]));
        ApiResult apiResult = notificationApi.send(notification);
        if (ApiResponseEnum.OK != apiResult.getResponseEnum()) {
            throw new Exception("Failed to send mail.");
        }
    }

}
