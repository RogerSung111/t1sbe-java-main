package com.tripleonetech.sbe.email;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MailModel {
    private String subject;
    private String content;
    private String from;
    private List<String> recipients;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getRecipients() {
        if (recipients == null) {
            recipients = new ArrayList<>();
        }
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public void addRecipients(String... recipients) {
        this.getRecipients().addAll(Arrays.asList(recipients));
    }

}
