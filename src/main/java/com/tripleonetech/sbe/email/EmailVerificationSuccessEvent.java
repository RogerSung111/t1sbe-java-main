package com.tripleonetech.sbe.email;

import org.springframework.context.ApplicationEvent;

public class EmailVerificationSuccessEvent extends ApplicationEvent {
    private String token;
    private Integer playerId;

    public EmailVerificationSuccessEvent(Object source, Integer playerId, String token) {
        super(source);

        this.playerId = playerId;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
