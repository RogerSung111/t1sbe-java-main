package com.tripleonetech.sbe.email;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface EmailHistoryMapper {
    int insert(EmailHistory record);

    List<EmailHistory> query(@Param("queryForm") EmailHistoryQueryForm queryForm, @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);
}
