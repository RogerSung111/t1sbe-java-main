package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopPopularGameCategoryEnum;

public class SummaryTopPopularGame {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private SummaryTopPopularGameCategoryEnum category;
    private byte rank;
    private String gameApiCode;
    private String gameCode;
    private String gameName;
    private int playerCount;
    private BigDecimal betAmount;
    private BigDecimal playerWin;
    private BigDecimal playerLoss;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public SummaryTopPopularGameCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(SummaryTopPopularGameCategoryEnum category) {
        this.category = category;
    }

    public byte getRank() {
        return rank;
    }

    public void setRank(byte rank) {
        this.rank = rank;
    }

    public String getGameApiCode() {
        return gameApiCode;
    }

    public void setGameApiCode(String gameApiCode) {
        this.gameApiCode = gameApiCode;
    }

    public String getGameCode() {
        return gameCode;
    }

    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getPlayerWin() {
        return playerWin;
    }

    public void setPlayerWin(BigDecimal playerWin) {
        this.playerWin = playerWin;
    }

    public BigDecimal getPlayerLoss() {
        return playerLoss;
    }

    public void setPlayerLoss(BigDecimal playerLoss) {
        this.playerLoss = playerLoss;
    }

    public static List<SummaryTopPopularGame> getEmptyObjectList() {
        SummaryTopPopularGame summaryTopPopularGame = new SummaryTopPopularGame();
        summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
        summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
        summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
        List<SummaryTopPopularGame> summaryTopPopularGameList = new ArrayList<>();
        summaryTopPopularGameList.add(summaryTopPopularGame);
        return summaryTopPopularGameList;
    }

}
