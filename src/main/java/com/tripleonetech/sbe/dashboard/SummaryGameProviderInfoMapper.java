package com.tripleonetech.sbe.dashboard;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SummaryGameProviderInfoMapper {

    int insert(SummaryGameProviderInfo record);

    List<SummaryGameProviderInfo> query(@Param("period") SummaryPeriodTypeEnum period,
            @Param("currency") SiteCurrencyEnum currency, @Param("locale") String locale,
            @Param("defaultLocale") String defaultLocale);
}
