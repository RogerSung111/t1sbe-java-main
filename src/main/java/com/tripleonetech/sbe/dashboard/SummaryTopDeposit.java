package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopDepositCategoryEnum;

public class SummaryTopDeposit {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private SummaryTopDepositCategoryEnum category;
    private byte rank;
    private String playerUsername;
    private long depositCount;
    private String depositMethod;
    private BigDecimal depositAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public SummaryTopDepositCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(SummaryTopDepositCategoryEnum category) {
        this.category = category;
    }

    public byte getRank() {
        return rank;
    }

    public void setRank(byte rank) {
        this.rank = rank;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public long getDepositCount() {
        return depositCount;
    }

    public void setDepositCount(long depositCount) {
        this.depositCount = depositCount;
    }

    public String getDepositMethod() {
        return depositMethod;
    }

    public void setDepositMethod(String depositMethod) {
        this.depositMethod = depositMethod;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public static List<SummaryTopDeposit> getEmptyObjectList() {

        SummaryTopDeposit summaryTopDeposit = new SummaryTopDeposit();
        summaryTopDeposit.setDepositAmount(BigDecimal.ZERO);
        List<SummaryTopDeposit> summaryTopDepositList = new ArrayList<>();
        summaryTopDepositList.add(summaryTopDeposit);

        return summaryTopDepositList;
    }
}
