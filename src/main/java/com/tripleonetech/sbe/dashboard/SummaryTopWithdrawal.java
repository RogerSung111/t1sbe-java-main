package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryTopWithdrawCategoryEnum;

public class SummaryTopWithdrawal {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private SummaryTopWithdrawCategoryEnum category;
    private byte rank;
    private String playerUsername;
    private long withdrawalCount;
    private BigDecimal withdrawalAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public SummaryTopWithdrawCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(SummaryTopWithdrawCategoryEnum category) {
        this.category = category;
    }

    public byte getRank() {
        return rank;
    }

    public void setRank(byte rank) {
        this.rank = rank;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public long getWithdrawalCount() {
        return withdrawalCount;
    }

    public void setWithdrawalCount(long withdrawalCount) {
        this.withdrawalCount = withdrawalCount;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public static List<SummaryTopWithdrawal> getEmptyObjectList() {
        SummaryTopWithdrawal summaryTopWithdrawal = new SummaryTopWithdrawal();
        summaryTopWithdrawal.setWithdrawalAmount(BigDecimal.ZERO);
        List<SummaryTopWithdrawal> summaryTopWithdrawalList = new ArrayList<>();
        summaryTopWithdrawalList.add(summaryTopWithdrawal);
        return summaryTopWithdrawalList;
    }
}
