package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryGameProviderInfo {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private byte rank;
    private int gameApiId;
    private int gameTypeId;
    private BigDecimal bet;
    private BigDecimal win;
    private BigDecimal loss;
    private String gameApiName;
    private String gameTypeName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public byte getRank() {
        return rank;
    }

    public void setRank(byte rank) {
        this.rank = rank;
    }

    public int getGameApiId() {
        return gameApiId;
    }

    public void setGameApiId(int gameApiId) {
        this.gameApiId = gameApiId;
    }

    public int getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(int gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public BigDecimal getBet() {
        return bet;
    }

    public void setBet(BigDecimal bet) {
        this.bet = bet;
    }

    public BigDecimal getWin() {
        return win;
    }

    public void setWin(BigDecimal win) {
        this.win = win;
    }

    public BigDecimal getLoss() {
        return loss;
    }

    public void setLoss(BigDecimal loss) {
        this.loss = loss;
    }

    public String getGameApiName() {
        return gameApiName;
    }

    public void setGameApiName(String gameApiName) {
        this.gameApiName = gameApiName;
    }

    public String getGameTypeName() {
        return gameTypeName;
    }

    public void setGameTypeName(String gameTypeName) {
        this.gameTypeName = gameTypeName;
    }

    public static List<SummaryGameProviderInfo> getEmptyObjectList() {
        SummaryGameProviderInfo summaryGameProviderInfo = new SummaryGameProviderInfo();
        summaryGameProviderInfo.setBet(BigDecimal.ZERO);
        summaryGameProviderInfo.setWin(BigDecimal.ZERO);
        summaryGameProviderInfo.setLoss(BigDecimal.ZERO);
        List<SummaryGameProviderInfo> summaryGameProviderInfoList = new ArrayList<>();
        summaryGameProviderInfoList.add(summaryGameProviderInfo);
        return summaryGameProviderInfoList;
    }

    @Override
    public String toString() {
        return "SummaryGameProviderInfo [id=" + id + ", period=" + period + ", timezone=" + timezone + ", currency="
                + currency + ", startDate=" + startDate + ", rank=" + rank + ", gameApiId=" + gameApiId
                + ", gameTypeId=" + gameTypeId + ", bet=" + bet + ", win=" + win + ", loss=" + loss + ", gameApiName="
                + gameApiName + ", gameTypeName=" + gameTypeName + "]";
    }

}
