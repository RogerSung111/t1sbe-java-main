package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryTopWinner {

    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private byte rank;
    private String playerUsername;
    private BigDecimal playerWin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public byte getRank() {
        return rank;
    }

    public void setRank(byte rank) {
        this.rank = rank;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public BigDecimal getPlayerWin() {
        return playerWin;
    }

    public void setPlayerWin(BigDecimal playerWin) {
        this.playerWin = playerWin;
    }

    public static List<SummaryTopWinner> getEmptyObjectList() {
        SummaryTopWinner summaryTopWinner = new SummaryTopWinner();
        summaryTopWinner.setPlayerWin(BigDecimal.ZERO);
        List<SummaryTopWinner> summaryTopWinnerList = new ArrayList<>();
        summaryTopWinnerList.add(summaryTopWinner);
        return summaryTopWinnerList;
    }
}
