package com.tripleonetech.sbe.dashboard;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SummaryNetProfit {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private BigDecimal cashback;
    private BigDecimal bonus;
    private BigDecimal playerWin;
    private BigDecimal playerLoss;
    private BigDecimal betAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public BigDecimal getCashback() {
        return cashback;
    }

    public void setCashback(BigDecimal cashback) {
        this.cashback = cashback;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public BigDecimal getPlayerWin() {
        return playerWin;
    }

    public void setPlayerWin(BigDecimal playerWin) {
        this.playerWin = playerWin;
    }

    public BigDecimal getPlayerLoss() {
        return playerLoss;
    }

    public void setPlayerLoss(BigDecimal playerLoss) {
        this.playerLoss = playerLoss;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public static List<SummaryNetProfit> getEmptyObjectList() {
        SummaryNetProfit summaryNetProfit = new SummaryNetProfit();
        summaryNetProfit.setCashback(BigDecimal.ZERO);
        summaryNetProfit.setBonus(BigDecimal.ZERO);
        summaryNetProfit.setPlayerLoss(BigDecimal.ZERO);
        summaryNetProfit.setPlayerWin(BigDecimal.ZERO);
        summaryNetProfit.setBetAmount(BigDecimal.ZERO);
        List<SummaryNetProfit> summaryNetProfitList = new ArrayList<>();
        summaryNetProfitList.add(summaryNetProfit);
        return summaryNetProfitList;
    }
}
