package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryBetGrossProfitStatus {

    private int id;

    private SummaryPeriodTypeEnum period;

    private String timezone;

    private SiteCurrencyEnum currency;

    private String startDate;

    private BigDecimal betAmount;

    private BigDecimal playerWin;

    private BigDecimal playerLoss;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getPlayerWin() {
        return playerWin;
    }

    public void setPlayerWin(BigDecimal playerWin) {
        this.playerWin = playerWin;
    }

    public BigDecimal getPlayerLoss() {
        return playerLoss;
    }

    public void setPlayerLoss(BigDecimal playerLoss) {
        this.playerLoss = playerLoss;
    }

    public static SummaryBetGrossProfitStatus getEmptyObject() {
        SummaryBetGrossProfitStatus summaryBetGrossProfitStatus = new SummaryBetGrossProfitStatus();
        summaryBetGrossProfitStatus.setBetAmount(BigDecimal.ZERO);
        summaryBetGrossProfitStatus.setPlayerLoss(BigDecimal.ZERO);
        summaryBetGrossProfitStatus.setPlayerWin(BigDecimal.ZERO);
        return summaryBetGrossProfitStatus;
    }

}
