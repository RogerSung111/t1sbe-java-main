package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

public class SummaryPlayerStatus {
    private int id;
    private SummaryPeriodTypeEnum period;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private long newPlayerWithoutDeposit;
    private long newPlayerWithDeposit;
    private long totalPlayerCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SummaryPeriodTypeEnum getPeriod() {
        return period;
    }

    public void setPeriod(SummaryPeriodTypeEnum period) {
        this.period = period;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getNewPlayerWithoutDeposit() {
        return newPlayerWithoutDeposit;
    }

    public void setNewPlayerWithoutDeposit(long newPlayerWithoutDeposit) {
        this.newPlayerWithoutDeposit = newPlayerWithoutDeposit;
    }

    public long getNewPlayerWithDeposit() {
        return newPlayerWithDeposit;
    }

    public void setNewPlayerWithDeposit(long newPlayerWithDeposit) {
        this.newPlayerWithDeposit = newPlayerWithDeposit;
    }

    public long getTotalPlayerCount() {
        return totalPlayerCount;
    }

    public void setTotalPlayerCount(long totalPlayerCount) {
        this.totalPlayerCount = totalPlayerCount;
    }

    public static List<SummaryPlayerStatus> getEmptyObjectList() {
        SummaryPlayerStatus summaryPlayerStatus = new SummaryPlayerStatus();
        List<SummaryPlayerStatus> summaryPlayerStatusList = new ArrayList<>();
        summaryPlayerStatusList.add(summaryPlayerStatus);

        return summaryPlayerStatusList;
    }

}
