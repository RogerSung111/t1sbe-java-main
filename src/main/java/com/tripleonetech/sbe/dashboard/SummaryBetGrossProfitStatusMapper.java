package com.tripleonetech.sbe.dashboard;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.calculation.SummaryPeriodTypeEnum;

@Mapper
public interface SummaryBetGrossProfitStatusMapper {

    int insert(SummaryBetGrossProfitStatus newRecord);

    List<SummaryBetGrossProfitStatus> query(@Param("period") SummaryPeriodTypeEnum period,
            @Param("currency") SiteCurrencyEnum currency, @Param("limit") int limit);

}
