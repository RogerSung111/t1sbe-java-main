package com.tripleonetech.sbe.dashboard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

@Mapper
public interface SummaryDailyMapper {

    int insert(SummaryDaily newRecord);

    SummaryDaily query(@Param("currency") SiteCurrencyEnum currency);

}
