package com.tripleonetech.sbe.dashboard.calculation;

public enum SummaryTopDepositCategoryEnum {
    BY_COUNT(0), BY_AMOUNT(1), BY_MAX_AMOUNT(2);

    private Integer categoryId;

    SummaryTopDepositCategoryEnum(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }
}
