package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryTopWinner;
import com.tripleonetech.sbe.dashboard.SummaryTopWinnerMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

@Component
public class SummaryTopWinnerCalculatorSource implements CalculatorRealTimeSource, CalculatorDailySource {

    @Autowired
    private SummaryTopWinnerMapper summaryTopWinnerMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;
    private byte rank;

    private SummaryTopWinner summaryTopWinner = new SummaryTopWinner();

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void initRecord() {
        summaryTopWinner.setPeriod(period);
        summaryTopWinner.setTimezone("UTC");
        summaryTopWinner.setCurrency(siteCurrency);
        summaryTopWinner.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopWinner.setRank(rank);
        summaryTopWinner.setPlayerUsername(null);
        summaryTopWinner.setPlayerWin(BigDecimal.ZERO);
        summaryTopWinnerMapper.insert(summaryTopWinner);
    }

    private void run() {
        rank = 1;
        /*
         * Insert placeholder data
         */
        initRecord();
        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        constant.getBackend().getDefaultLocale());

        Map<String, BigDecimal> resultMapByAmount = gameLogHourlyReports.stream()
                .collect(
                        Collectors.groupingBy(
                                d -> d.getPlayerUsername(),
                                Collectors.reducing(BigDecimal.ZERO, GameLogHourlyReport::getWin, BigDecimal::add)));

        List<Map.Entry<String, BigDecimal>> topWinnerByAmount = resultMapByAmount.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .collect(Collectors.toList());

        Iterator<Entry<String, BigDecimal>> topWinnerByAmountIterator = topWinnerByAmount.iterator();
        while (topWinnerByAmountIterator.hasNext()) {
            Map.Entry<String, BigDecimal> iteratorTmp = topWinnerByAmountIterator.next();
            summaryTopWinner.setPeriod(period);
            summaryTopWinner.setTimezone("UTC");
            summaryTopWinner.setCurrency(siteCurrency);
            summaryTopWinner.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
            summaryTopWinner.setRank(rank);
            summaryTopWinner.setPlayerUsername(iteratorTmp.getKey());
            summaryTopWinner.setPlayerWin(iteratorTmp.getValue());
            summaryTopWinnerMapper.insert(summaryTopWinner);
            rank++;
        }
    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            startTime = LocalDateTime.of(today, LocalTime.MIN);
            endTime = LocalDateTime.of(today, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.TODAY;
            run();
        }
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;

            // last 7days
            LocalDate dayStart = today.minus(Period.ofDays(8));
            LocalDate dayEnd = today.minus(Period.ofDays(1));
            startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
            endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_7_DAYS;
            run();

            // last 30days
            dayStart = today.minusDays(31);
            dayEnd = today.minusDays(1);
            startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
            endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_30_DAYS;
            run();
        }
    }
}
