package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.dashboard.SummaryNetProfit;
import com.tripleonetech.sbe.dashboard.SummaryNetProfitMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;

@Component
public class SummaryNetProfitCalculatorSource
        implements CalculatorDailySource, CalculatorWeeklySource, CalculatorMonthlySource {

    @Autowired
    private SummaryNetProfitMapper summaryNetProfitMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void run() {
        Map<String, Object> walletTransactionParams = new HashMap<>();
        walletTransactionParams.put("currency", siteCurrency);
        walletTransactionParams.put("createdAtStart", startTime);
        walletTransactionParams.put("createdAtEnd", endTime);

        Map<String, Object> depositCountQueryParams = new HashMap<>();
        depositCountQueryParams.put("currency", siteCurrency);
        depositCountQueryParams.put("requestDateStart", startTime);
        depositCountQueryParams.put("requestDateEnd", endTime);

        List<WalletTransaction> walletTransaction = walletTransactionMapper
                .querySummary(walletTransactionParams);
        // compute total amount of bonus
        BigDecimal bonus = walletTransaction.stream()
                .filter(x -> WalletTransactionTypeEnum.BONUS == x.getType())
                .map(WalletTransaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // compute total amount of cashback
        BigDecimal cashback = walletTransaction.stream()
                .filter(x -> WalletTransactionTypeEnum.CASHBACK == x.getType())
                .map(WalletTransaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        constant.getBackend().getDefaultLocale());
        // compute player total win
        BigDecimal playerWin = gameLogHourlyReports.stream()
                .map(GameLogHourlyReport::getWin)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // compute player total loss
        BigDecimal playerLoss = gameLogHourlyReports.stream()
                .map(GameLogHourlyReport::getLoss)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        SummaryNetProfit summaryNetProfit = new SummaryNetProfit();
        summaryNetProfit.setPeriod(period);
        summaryNetProfit.setTimezone("UTC");
        summaryNetProfit.setCurrency(siteCurrency);
        summaryNetProfit.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryNetProfit.setCashback(cashback);
        summaryNetProfit.setBonus(bonus);
        summaryNetProfit.setPlayerWin(playerWin);
        summaryNetProfit.setPlayerLoss(playerLoss);
        summaryNetProfitMapper.insert(summaryNetProfit);
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            LocalDate dayStart = today.minusDays(1);
            startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
            endTime = LocalDateTime.of(dayStart, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.TODAY;
            run();
        }
    }

    @Override
    public void calculateWeekly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // as per the requirement specification doc, Sunday will be the first day of the week
            LocalDate weekStart = today.minusDays(6 + today.plusDays(1).getDayOfWeek().getValue());
            LocalDate weekEnd = today.minusDays(today.plusDays(1).getDayOfWeek().getValue());
            startTime = LocalDateTime.of(weekStart, LocalTime.MIN);
            endTime = LocalDateTime.of(weekEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_WEEK;
            run();
        }
    }

    @Override
    public void calculateMonthly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            LocalDate previousMonth = today.minusMonths(1);
            LocalDate monthStart = previousMonth.withDayOfMonth(1);
            LocalDate monthEnd = previousMonth.withDayOfMonth(previousMonth.lengthOfMonth());
            startTime = LocalDateTime.of(monthStart, LocalTime.MIN);
            endTime = LocalDateTime.of(monthEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_MONTH;
            run();
        }
    }
}
