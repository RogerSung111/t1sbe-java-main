package com.tripleonetech.sbe.dashboard.calculation;

public enum SummaryTopWithdrawCategoryEnum {
    BY_COUNT(0), BY_AMOUNT(1);

    private Integer categoryId;

    SummaryTopWithdrawCategoryEnum(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }
}
