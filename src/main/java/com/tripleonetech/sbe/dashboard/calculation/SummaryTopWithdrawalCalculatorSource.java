package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryTopWithdrawal;
import com.tripleonetech.sbe.dashboard.SummaryTopWithdrawalMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestView;

@Component
public class SummaryTopWithdrawalCalculatorSource implements CalculatorRealTimeSource, CalculatorDailySource {

    @Autowired
    private SummaryTopWithdrawalMapper summaryTopWithdrawalMapper;
    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;
    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private byte rank;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;
    private SummaryTopWithdrawal summaryTopWithdrawal = new SummaryTopWithdrawal();

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void initRecord(SummaryTopWithdrawCategoryEnum category) {
        summaryTopWithdrawal.setPeriod(period);
        summaryTopWithdrawal.setTimezone("UTC");
        summaryTopWithdrawal.setCurrency(siteCurrency);
        summaryTopWithdrawal.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopWithdrawal.setCategory(category);
        summaryTopWithdrawal.setRank(rank);
        summaryTopWithdrawalMapper.insert(summaryTopWithdrawal);
    }

    private void run(SummaryTopWithdrawCategoryEnum category) {
        Map<String, Object> withdrawRequestParams = new HashMap<>();
        withdrawRequestParams.put("currency", siteCurrency);
        withdrawRequestParams.put("requestDateStart",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(startTime));
        withdrawRequestParams.put("requestDateEnd", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(endTime));
        withdrawRequestParams.put("status", 1);

        List<WithdrawRequestView> withdrawRequests = withdrawRequestMapper.querySummary(withdrawRequestParams);

        rank = 1;
        switch (category) {
        case BY_COUNT:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, Long> resultMapByCount = withdrawRequests.stream()
                    .collect(Collectors.groupingBy(d -> d.getUsername(), Collectors.counting()));

            List<Map.Entry<String, Long>> topPlayerByWithdrawCount = resultMapByCount.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, Long>> topPlayerByWithdrawCountIterator = topPlayerByWithdrawCount.iterator();
            while (topPlayerByWithdrawCountIterator.hasNext()) {
                Map.Entry<String, Long> iteratorTmp = topPlayerByWithdrawCountIterator.next();
                BigDecimal withdrawAmount = withdrawRequests.stream()
                        .filter(x -> StringUtils.equals(iteratorTmp.getKey(), x.getUsername()))
                        .map(WithdrawRequestView::getAmount)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                summaryTopWithdrawal.setPlayerUsername(iteratorTmp.getKey());
                summaryTopWithdrawal.setWithdrawalCount(iteratorTmp.getValue());
                summaryTopWithdrawal.setWithdrawalAmount(withdrawAmount);
                initRecord(category);
                rank++;
            }
            break;
        case BY_AMOUNT:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, BigDecimal> resultMapByAmount = withdrawRequests.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getUsername(),
                                    Collectors.reducing(BigDecimal.ZERO, WithdrawRequestView::getAmount,
                                            BigDecimal::add)));

            List<Map.Entry<String, BigDecimal>> topPlayerByWithdrawAmount = resultMapByAmount.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, BigDecimal>> topPlayerByWithdrawAmountIterator = topPlayerByWithdrawAmount
                    .iterator();
            while (topPlayerByWithdrawAmountIterator.hasNext()) {
                Map.Entry<String, BigDecimal> iteratorTmp = topPlayerByWithdrawAmountIterator.next();

                summaryTopWithdrawal.setPlayerUsername(iteratorTmp.getKey());
                summaryTopWithdrawal.setWithdrawalCount(0); // Not applicasble for this category
                summaryTopWithdrawal.setWithdrawalAmount(iteratorTmp.getValue());
                initRecord(category);
                rank++;
            }
            break;
        default:
        }
    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopWithdrawCategoryEnum c : SummaryTopWithdrawCategoryEnum.values()) {
                startTime = LocalDateTime.of(today, LocalTime.MIN);
                endTime = LocalDateTime.of(today, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.TODAY;
                run(c);
            }
        }
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopWithdrawCategoryEnum c : SummaryTopWithdrawCategoryEnum.values()) {
                // last 7days
                LocalDate dayStart = today.minusDays(8);
                LocalDate dayEnd = today.minusDays(1);
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_7_DAYS;
                run(c);

                // last 30days
                dayStart = today.minusDays(31);
                dayEnd = today.minusDays(1);
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_30_DAYS;
                run(c);
            }
        }
    }
}
