package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryTopDeposit;
import com.tripleonetech.sbe.dashboard.SummaryTopDepositMapper;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositRequestView;

@Component
public class SummaryTopDepositCalculatorSource
        implements CalculatorRealTimeSource, CalculatorDailySource {

    @Autowired
    private SummaryTopDepositMapper summaryTopDepositMapper;
    @Autowired
    private DepositRequestMapper depositRequestMapper;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;
    private byte rank;
    private SummaryTopDeposit summaryTopDeposit = new SummaryTopDeposit();

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void initRecord(SummaryTopDepositCategoryEnum category) {
        summaryTopDeposit.setPeriod(period);
        summaryTopDeposit.setTimezone("UTC");
        summaryTopDeposit.setCurrency(siteCurrency);
        summaryTopDeposit.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopDeposit.setCategory(category);
        summaryTopDeposit.setRank(rank);
        summaryTopDeposit.setPlayerUsername(null);
        summaryTopDeposit.setDepositCount(0);
        summaryTopDeposit.setDepositMethod(null);
        summaryTopDeposit.setDepositAmount(BigDecimal.ZERO);
        summaryTopDepositMapper.insert(summaryTopDeposit);
    }

    private void insertRecord(SummaryTopDepositCategoryEnum category) {
        summaryTopDeposit.setPeriod(period);
        summaryTopDeposit.setTimezone("UTC");
        summaryTopDeposit.setCurrency(siteCurrency);
        summaryTopDeposit.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopDeposit.setCategory(category);
        summaryTopDeposit.setRank(rank);
        summaryTopDepositMapper.insert(summaryTopDeposit);
    }

    private void run(SummaryTopDepositCategoryEnum category) {
        Map<String, Object> depositRequestParams = new HashMap<>();
        depositRequestParams.put("currency", siteCurrency);
        depositRequestParams.put("requestDateStart",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(startTime));
        depositRequestParams.put("requestDateEnd", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(endTime));
        depositRequestParams.put("status", 1);

        List<DepositRequestView> depositRequests = depositRequestMapper.querySummary(depositRequestParams);

        rank = 1;
        switch (category) {
        case BY_COUNT:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, Long> resultMapByCount = depositRequests.stream()
                    .collect(Collectors.groupingBy(d -> d.getUsername(), Collectors.counting()));

            List<Map.Entry<String, Long>> topPlayerByDepositCount = resultMapByCount.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, Long>> topPlayerByDepositCountIterator = topPlayerByDepositCount.iterator();
            while (topPlayerByDepositCountIterator.hasNext()) {
                Map.Entry<String, Long> iteratorTmp = topPlayerByDepositCountIterator.next();
                BigDecimal depositAmount = depositRequests.stream()
                        .filter(x -> iteratorTmp.getKey() == x
                                .getUsername())
                        .map(DepositRequestView::getAmount)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                summaryTopDeposit.setPlayerUsername(iteratorTmp.getKey());
                summaryTopDeposit.setDepositCount(iteratorTmp.getValue());
                summaryTopDeposit.setDepositMethod(null); // Not applicable for this category
                summaryTopDeposit.setDepositAmount(depositAmount);
                insertRecord(category);

                rank++;
            }
            break;
        case BY_AMOUNT:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, BigDecimal> resultMapByAmount = depositRequests.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getUsername(),
                                    Collectors.reducing(BigDecimal.ZERO, DepositRequestView::getAmount,
                                            BigDecimal::add)));

            List<Map.Entry<String, BigDecimal>> topPlayerByDepositAmount = resultMapByAmount.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, BigDecimal>> topPlayerByDepositAmountIterator = topPlayerByDepositAmount.iterator();
            while (topPlayerByDepositAmountIterator.hasNext()) {
                Map.Entry<String, BigDecimal> iteratorTmp = topPlayerByDepositAmountIterator.next();

                summaryTopDeposit.setPlayerUsername(iteratorTmp.getKey());
                summaryTopDeposit.setDepositCount(0); // Not applicable for this category
                summaryTopDeposit.setDepositMethod(null); // Not applicable for this category
                summaryTopDeposit.setDepositAmount(iteratorTmp.getValue());
                insertRecord(category);
                rank++;
            }
            break;
        case BY_MAX_AMOUNT:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            List<DepositRequestView> result = depositRequests
                    .stream()
                    .sorted(Comparator.comparing(
                            DepositRequestView::getAmount)
                            .reversed())
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<DepositRequestView> topPlayerByDepositMaxAmountIterator = result
                    .iterator();
            while (topPlayerByDepositMaxAmountIterator.hasNext()) {
                DepositRequestView iteratorTmp = topPlayerByDepositMaxAmountIterator.next();

                summaryTopDeposit.setPlayerUsername(iteratorTmp.getUsername());
                summaryTopDeposit.setDepositCount(0); // not applicable for this category
                summaryTopDeposit.setDepositMethod(iteratorTmp.getPaymentMethod().getName());
                summaryTopDeposit.setDepositAmount(iteratorTmp.getAmount());
                insertRecord(category);
                rank++;
            }
            break;
        default:
        }
    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopDepositCategoryEnum c : SummaryTopDepositCategoryEnum.values()) {
                startTime = LocalDateTime.of(today, LocalTime.MIN);
                endTime = LocalDateTime.of(today, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.TODAY;
                run(c);
            }
        }
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopDepositCategoryEnum c : SummaryTopDepositCategoryEnum.values()) {
                // last 7days
                LocalDate dayStart = today.minusDays(8);
                LocalDate dayEnd = today.minusDays(1);
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_7_DAYS;
                run(c);

                // last 30days
                dayStart = today.minusDays(31);
                dayEnd = today.minusDays(1);
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_30_DAYS;
                run(c);
            }
        }
    }
}
