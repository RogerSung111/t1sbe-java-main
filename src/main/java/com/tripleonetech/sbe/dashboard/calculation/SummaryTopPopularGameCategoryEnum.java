package com.tripleonetech.sbe.dashboard.calculation;

public enum SummaryTopPopularGameCategoryEnum {
    BY_PLAYER(0), BY_BET(1), BY_WIN(2), BY_LOSS(3), BY_REV(4), BY_REV_RATIO(5), BY_AVERAGE_BET(6);

    private Integer categoryId;

    SummaryTopPopularGameCategoryEnum(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }
}
