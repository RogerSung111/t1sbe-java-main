package com.tripleonetech.sbe.dashboard.calculation;

public interface CalculatorDailySource {
    void calculateDaily();
}
