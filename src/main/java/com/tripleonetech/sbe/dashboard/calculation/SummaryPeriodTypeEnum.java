package com.tripleonetech.sbe.dashboard.calculation;

public enum SummaryPeriodTypeEnum {
    TODAY(0), LAST_7_DAYS(1), LAST_30_DAYS(2), LAST_WEEK(3), THIS_MONTH(4), LAST_MONTH(5);

    private Integer typeId;

    SummaryPeriodTypeEnum(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getTypeId() {
        return typeId;
    }
}
