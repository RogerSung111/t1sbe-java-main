package com.tripleonetech.sbe.dashboard.calculation;

import java.lang.reflect.Modifier;
import java.util.Calendar;
import java.util.Set;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class CalculatorMonthly {
    @Autowired
    private ApplicationContext ctx;

    public void executeTaskOnLastDayOfMonth() {
        final Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DATE) == c.getActualMaximum(Calendar.DATE)) {
            run();
        }
    }

    public void run() {
        Reflections reflections = new Reflections(this.getClass().getPackage().getName());
        Set<? extends Class<?>> allClasses = reflections.getSubTypesOf(CalculatorMonthlySource.class);
        for (Class<?> clazz : allClasses) {
            if (Modifier.isAbstract(clazz.getModifiers())) {
                // Will not try to instantiate abstract class
                continue;
            }
            Object obj = ctx.getBean(clazz);
            if (obj instanceof CalculatorMonthlySource) {
                ((CalculatorMonthlySource) obj).calculateMonthly();
            }
        }
    }
}
