package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryTopPopularGame;
import com.tripleonetech.sbe.dashboard.SummaryTopPopularGameMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

@Component
public class SummaryTopPopularGameCalculatorSource implements CalculatorRealTimeSource, CalculatorDailySource {
    @Autowired
    private SummaryTopPopularGameMapper summaryTopPopularGameMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;
    private byte rank;

    private SummaryTopPopularGame summaryTopPopularGame = new SummaryTopPopularGame();

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void initRecord(SummaryTopPopularGameCategoryEnum category) {
        summaryTopPopularGame.setPeriod(period);
        summaryTopPopularGame.setTimezone("UTC");
        summaryTopPopularGame.setCurrency(siteCurrency);
        summaryTopPopularGame.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopPopularGame.setCategory(category);
        summaryTopPopularGame.setRank(rank);
        summaryTopPopularGame.setGameCode("");
        summaryTopPopularGame.setPlayerCount(0);
        summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
        summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
        summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
        summaryTopPopularGameMapper.insert(summaryTopPopularGame);
    }

    private void insertRecord(SummaryTopPopularGameCategoryEnum category) {
        summaryTopPopularGame.setPeriod(period);
        summaryTopPopularGame.setTimezone("UTC");
        summaryTopPopularGame.setCurrency(siteCurrency);
        summaryTopPopularGame.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryTopPopularGame.setCategory(category);
        summaryTopPopularGame.setRank(rank);
        summaryTopPopularGameMapper.insert(summaryTopPopularGame);
    }

    private void run(SummaryTopPopularGameCategoryEnum category) {
        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        constant.getBackend().getDefaultLocale());

        rank = 1;

        switch (category) {
        case BY_PLAYER:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, Integer> resultMapByPlayer = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    GameLogHourlyReport::getGameCode,
                                    Collectors.collectingAndThen(
                                            Collectors.mapping(GameLogHourlyReport::getPlayerUsername,
                                                    Collectors.toSet()),
                                            Set::size)));

            List<Map.Entry<String, Integer>> topPopularGameByPlayer = resultMapByPlayer.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());
            Iterator<Entry<String, Integer>> topPopularGameByPlayerIterator = topPopularGameByPlayer.iterator();
            while (topPopularGameByPlayerIterator.hasNext()) {
                Map.Entry<String, Integer> iteratorTmp = topPopularGameByPlayerIterator.next();
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(iteratorTmp.getValue());
                summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
                insertRecord(category);
                rank++;
            }
            break;
        case BY_BET:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, BigDecimal> resultMapByBet = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.reducing(BigDecimal.ZERO, GameLogHourlyReport::getBet,
                                            BigDecimal::add)));

            List<Map.Entry<String, BigDecimal>> topPopularGameByBet = resultMapByBet.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());
            Iterator<Entry<String, BigDecimal>> topPopularGameByBetIterator = topPopularGameByBet.iterator();
            while (topPopularGameByBetIterator.hasNext()) {
                Map.Entry<String, BigDecimal> iteratorTmp = topPopularGameByBetIterator.next();
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(iteratorTmp.getValue());
                summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
                insertRecord(category);
                rank++;
            }
            break;
        case BY_WIN:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, BigDecimal> resultMapByWin = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.reducing(BigDecimal.ZERO, GameLogHourlyReport::getWin,
                                            BigDecimal::add)));

            List<Map.Entry<String, BigDecimal>> topPopularGameByWin = resultMapByWin.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());
            Iterator<Entry<String, BigDecimal>> topPopularGameByWinIterator = topPopularGameByWin.iterator();
            while (topPopularGameByWinIterator.hasNext()) {
                Map.Entry<String, BigDecimal> iteratorTmp = topPopularGameByWinIterator.next();
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerWin(iteratorTmp.getValue());
                summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
                insertRecord(category);
                rank++;
            }
            break;
        case BY_LOSS:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, BigDecimal> resultMapByLoss = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.reducing(BigDecimal.ZERO, GameLogHourlyReport::getLoss,
                                            BigDecimal::add)));

            List<Map.Entry<String, BigDecimal>> topPopularGameByLoss = resultMapByLoss.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());
            Iterator<Entry<String, BigDecimal>> topPopularGameByLossIterator = topPopularGameByLoss.iterator();
            while (topPopularGameByLossIterator.hasNext()) {
                Map.Entry<String, BigDecimal> iteratorTmp = topPopularGameByLossIterator.next();
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerLoss(iteratorTmp.getValue());
                insertRecord(category);
                rank++;
            }
            break;
        case BY_REV:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            // TODO: Revise the double type to BigDecimal
            Map<String, Double> resultMapByRevenue = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.summingDouble(
                                            d -> (d.getLoss().add(d.getWin())).doubleValue())));

            List<Map.Entry<String, Double>> topPopularGameByRevenue = resultMapByRevenue.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, Double>> topPopularGameByRevenueIterator = topPopularGameByRevenue.iterator();
            while (topPopularGameByRevenueIterator.hasNext()) {
                Map.Entry<String, Double> iteratorTmp = topPopularGameByRevenueIterator.next();
                BigDecimal playerWin = gameLogHourlyReports.stream()
                        .filter(x -> iteratorTmp.getKey() == x.getGameCode())
                        .map(GameLogHourlyReport::getWin)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                BigDecimal playerLoss = gameLogHourlyReports.stream()
                        .filter(x -> iteratorTmp.getKey() == x.getGameCode())
                        .map(GameLogHourlyReport::getLoss)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerWin(playerWin);
                summaryTopPopularGame.setPlayerLoss(playerLoss);
                insertRecord(category);
                rank++;
            }
            break;
        case BY_REV_RATIO:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            Map<String, Double> resultMapByRevenueRatio = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.summingDouble(
                                            d -> ((d.getLoss().add(d.getWin())).divide(d.getBet(), 2,
                                                    RoundingMode.HALF_UP)).doubleValue())));

            List<Map.Entry<String, Double>> topPopularGameByRevenueRatio = resultMapByRevenueRatio.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());

            Iterator<Entry<String, Double>> topPopularGameByRevenueRatioIterator = topPopularGameByRevenueRatio
                    .iterator();
            while (topPopularGameByRevenueRatioIterator.hasNext()) {
                Map.Entry<String, Double> iteratorTmp = topPopularGameByRevenueRatioIterator.next();
                BigDecimal betAmount = gameLogHourlyReports.stream()
                        .filter(x -> iteratorTmp.getKey() == x.getGameCode())
                        .map(GameLogHourlyReport::getBet)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                BigDecimal playerWin = gameLogHourlyReports.stream()
                        .filter(x -> iteratorTmp.getKey() == x.getGameCode())
                        .map(GameLogHourlyReport::getWin)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                BigDecimal playerLoss = gameLogHourlyReports.stream()
                        .filter(x -> iteratorTmp.getKey() == x.getGameCode())
                        .map(GameLogHourlyReport::getLoss)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(betAmount);
                summaryTopPopularGame.setPlayerWin(playerWin);
                summaryTopPopularGame.setPlayerLoss(playerLoss);
                insertRecord(category);
                rank++;
            }

            break;
        case BY_AVERAGE_BET:
            /*
             * Insert placeholder data
             */
            initRecord(category);
            // TODO: Revise the double type to BigDecimal
            Map<String, Double> resultMapByAverageBet = gameLogHourlyReports.stream()
                    .collect(
                            Collectors.groupingBy(
                                    d -> d.getGameCode(),
                                    Collectors.collectingAndThen(
                                            Collectors.averagingDouble(d -> d.getBet().doubleValue()),
                                            average -> average)));

            List<Map.Entry<String, Double>> topPopularGameByAverageBet = resultMapByAverageBet.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toList());
            Iterator<Entry<String, Double>> topPopularGameByAverageBetIterator = topPopularGameByAverageBet.iterator();
            while (topPopularGameByAverageBetIterator.hasNext()) {
                Map.Entry<String, Double> iteratorTmp = topPopularGameByAverageBetIterator.next();
                summaryTopPopularGame.setGameCode(iteratorTmp.getKey());
                summaryTopPopularGame.setPlayerCount(0);
                summaryTopPopularGame.setBetAmount(new BigDecimal(iteratorTmp.getValue()));
                summaryTopPopularGame.setPlayerWin(BigDecimal.ZERO);
                summaryTopPopularGame.setPlayerLoss(BigDecimal.ZERO);
                insertRecord(category);
                rank++;
            }
            break;
        default:
        }
    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopPopularGameCategoryEnum c : SummaryTopPopularGameCategoryEnum.values()) {
                startTime = LocalDateTime.of(today, LocalTime.MIN);
                endTime = LocalDateTime.of(today, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.TODAY;
                run(c);
            }
        }
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            for (SummaryTopPopularGameCategoryEnum c : SummaryTopPopularGameCategoryEnum.values()) {
                // last 7days
                LocalDate dayStart = today.minus(Period.ofDays(8));
                LocalDate dayEnd = today.minus(Period.ofDays(1));
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_7_DAYS;
                run(c);

                // last 30days
                dayStart = today.minusDays(31);
                dayEnd = today.minusDays(1);
                startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
                endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
                period = SummaryPeriodTypeEnum.LAST_30_DAYS;
                run(c);
            }
        }
    }
}
