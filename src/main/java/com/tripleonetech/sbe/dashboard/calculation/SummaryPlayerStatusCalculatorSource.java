package com.tripleonetech.sbe.dashboard.calculation;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryPlayerStatus;
import com.tripleonetech.sbe.dashboard.SummaryPlayerStatusMapper;
import com.tripleonetech.sbe.player.PlayerMapper;

@Component
public class SummaryPlayerStatusCalculatorSource
        implements CalculatorWeeklySource, CalculatorMonthlySource, CalculatorDailySource {
    @Autowired
    private SummaryPlayerStatusMapper summaryPlayerStatusMapper;
    @Autowired
    private PlayerMapper playerMapper;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void run() {
        Map<String, Object> totalPlayerCountQueryParams = new HashMap<>();
        totalPlayerCountQueryParams.put("currency", siteCurrency);
        totalPlayerCountQueryParams.put("createdAtEnd", endTime);

        Map<String, Object> newPlayerCountQueryParams = new HashMap<>();
        newPlayerCountQueryParams.put("currency", siteCurrency);
        newPlayerCountQueryParams.put("createdAtStart", startTime);
        newPlayerCountQueryParams.put("createdAtEnd", endTime);

        Map<String, Object> newPlayerDepositCountQueryParams = new HashMap<>();
        newPlayerDepositCountQueryParams.put("currency", siteCurrency);
        newPlayerDepositCountQueryParams.put("requestDateStart", startTime);
        newPlayerDepositCountQueryParams.put("requestDateEnd", endTime);
        newPlayerDepositCountQueryParams.put("createdAtStart", startTime);
        newPlayerDepositCountQueryParams.put("createdAtEnd", endTime);

        Long totalPlayerCount = playerMapper.calculateData(totalPlayerCountQueryParams);
        Long newPlayerCount = playerMapper.calculateData(newPlayerCountQueryParams);
        Long newPlayerWithDeposit = playerMapper.calculateData(newPlayerDepositCountQueryParams);

        SummaryPlayerStatus summaryPlayerStatus = new SummaryPlayerStatus();
        summaryPlayerStatus.setPeriod(period);
        summaryPlayerStatus.setTimezone("UTC");
        summaryPlayerStatus.setCurrency(siteCurrency);
        summaryPlayerStatus.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryPlayerStatus.setNewPlayerWithDeposit(newPlayerWithDeposit);
        summaryPlayerStatus.setNewPlayerWithoutDeposit(newPlayerCount - newPlayerWithDeposit);
        summaryPlayerStatus.setTotalPlayerCount(totalPlayerCount);
        summaryPlayerStatusMapper.insert(summaryPlayerStatus);
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            LocalDate dayStart = today.minusDays(1);
            startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
            endTime = LocalDateTime.of(dayStart, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.TODAY;
            run();

        }
    }

    @Override
    public void calculateWeekly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // as per the requirement specification doc, Sunday will be the first day of the week
            LocalDate weekStart = today.minusDays(6 + today.plusDays(1).getDayOfWeek().getValue());
            LocalDate weekEnd = today.minusDays(today.plusDays(1).getDayOfWeek().getValue());
            startTime = LocalDateTime.of(weekStart, LocalTime.MIN);
            endTime = LocalDateTime.of(weekEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_WEEK;
            run();
        }
    }

    @Override
    public void calculateMonthly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            LocalDate previousMonth = today.minusMonths(1);
            LocalDate monthStart = previousMonth.withDayOfMonth(1);
            LocalDate monthEnd = previousMonth.withDayOfMonth(previousMonth.lengthOfMonth());
            startTime = LocalDateTime.of(monthStart, LocalTime.MIN);
            endTime = LocalDateTime.of(monthEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_MONTH;
            run();
        }
    }
}
