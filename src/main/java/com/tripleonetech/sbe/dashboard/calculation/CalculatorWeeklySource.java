package com.tripleonetech.sbe.dashboard.calculation;

public interface CalculatorWeeklySource {
    void calculateWeekly();
}
