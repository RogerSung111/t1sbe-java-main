package com.tripleonetech.sbe.dashboard.calculation;

public interface CalculatorMonthlySource {
    void calculateMonthly();
}
