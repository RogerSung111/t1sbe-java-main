package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.dashboard.SummaryDaily;
import com.tripleonetech.sbe.dashboard.SummaryDailyMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositRequestView;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.wallet.history.WalletTransaction;
import com.tripleonetech.sbe.player.wallet.history.WalletTransactionMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequest;
import com.tripleonetech.sbe.withdraw.WithdrawRequestMapper;
import com.tripleonetech.sbe.withdraw.WithdrawRequestView;

@Component
public class SummaryDailyCalculatorSource implements CalculatorRealTimeSource {
    @Autowired
    private SummaryDailyMapper summaryDailyMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private WithdrawRequestMapper withdrawRequestMapper;
    @Autowired
    private WalletTransactionMapper walletTransactionMapper;
    @Autowired
    private Constant constant;

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SiteCurrencyEnum siteCurrency;

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDate today = LocalDate.now(zoneId);
        startTime = LocalDateTime.of(today, LocalTime.MIN);
        endTime = LocalDateTime.of(today, LocalTime.MAX);
    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            Map<String, Object> depositRequestParams = new HashMap<>();
            depositRequestParams.put("currency", siteCurrency);
            depositRequestParams.put("requestDateStart", startTime);
            depositRequestParams.put("requestDateEnd", endTime);

            Map<String, Object> withdrawRequestParams = new HashMap<>();
            withdrawRequestParams.put("currency", siteCurrency);
            withdrawRequestParams.put("requestDateStart", startTime);
            withdrawRequestParams.put("requestDateEnd", endTime);

            Map<String, Object> walletTransactionParams = new HashMap<>();
            walletTransactionParams.put("currency", siteCurrency);
            walletTransactionParams.put("createdAtStart", startTime);
            walletTransactionParams.put("createdAtEnd", endTime);

            Map<String, Object> totalPlayerCountQueryParams = new HashMap<>();
            totalPlayerCountQueryParams.put("currency", siteCurrency);

            Map<String, Object> newPlayerCountQueryParams = new HashMap<>();
            newPlayerCountQueryParams.put("currency", siteCurrency);
            newPlayerCountQueryParams.put("createdAtStart", startTime);
            newPlayerCountQueryParams.put("createdAtEnd", endTime);

            // retrieve data from DB
            // get player count
            Long totalPlayerCount = playerMapper.calculateData(totalPlayerCountQueryParams);
            Long newPlayerCount = playerMapper.calculateData(newPlayerCountQueryParams);
            Long newPlayerWithDeposit = playerMapper.calculateData(depositRequestParams);

            // get total amount and count of deposit
            List<DepositRequestView> depositRequests = depositRequestMapper.querySummary(depositRequestParams);
            int depositCount = depositRequests.size();

            List<DepositRequest> approvedDepositRequests = depositRequestMapper.getApprovedInPeriod(startTime, endTime);
            BigDecimal depositAmount = approvedDepositRequests.stream()
                    .map(DepositRequest::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            // get total amount and count of withdrawal
            List<WithdrawRequestView> withdrawRequests = withdrawRequestMapper.querySummary(withdrawRequestParams);
            int withdrawalCount = withdrawRequests.size();

            List<WithdrawRequest> approvedWithdrawRequests = withdrawRequestMapper.getApprovedInPeriod(startTime,
                    endTime);
            BigDecimal withdrawalAmount = approvedWithdrawRequests.stream()
                    .map(WithdrawRequest::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            List<WalletTransaction> walletTransaction = walletTransactionMapper
                    .querySummary(walletTransactionParams);
            // compute total amount of bonus
            BigDecimal bonus = walletTransaction.stream()
                    .filter(x -> WalletTransactionTypeEnum.BONUS == x.getType())
                    .map(WalletTransaction::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            // compute total amount of cashback
            BigDecimal cashback = walletTransaction.stream()
                    .filter(x -> WalletTransactionTypeEnum.CASHBACK == x.getType())
                    .map(WalletTransaction::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                    .getRecordsPeriod(startTime, endTime, null,
                            constant.getBackend().getDefaultLocale());
            // compute player total betAmount
            BigDecimal betAmount = gameLogHourlyReports.stream()
                    .map(GameLogHourlyReport::getBet)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            // compute player total win
            BigDecimal playerTotalWin = gameLogHourlyReports.stream()
                    .map(GameLogHourlyReport::getWin)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            // compute player total loss
            BigDecimal playerTotalLoss = gameLogHourlyReports.stream()
                    .map(GameLogHourlyReport::getLoss)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            // compute total active player
            Long activePlayerCount = gameLogHourlyReports.stream()
                    .map(GameLogHourlyReport::getPlayerUsername)
                    .distinct().count();
            // compute all game type player counts
            Map<String, Integer> playerCountByGameTypesMap = gameLogHourlyReports.stream().collect(
                    Collectors.groupingBy(
                            GameLogHourlyReport::getGameTypeName,
                            Collectors.collectingAndThen(
                                    Collectors.mapping(GameLogHourlyReport::getPlayerUsername, Collectors.toSet()),
                                    Set::size)));
            // convert playerCountByGameTypes to JSON
            String playerCountByGameTypesJson = JsonUtils.objectToJson(playerCountByGameTypesMap);

            SummaryDaily summaryDaily = new SummaryDaily();
            summaryDaily.setTimezone("UTC");
            summaryDaily.setCurrency(siteCurrency);
            summaryDaily.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
            summaryDaily
                    .setTotalPlayerCount(totalPlayerCount);
            summaryDaily.setTotalDepositCount(depositCount);
            summaryDaily.setTotalDepositAmount(depositAmount);
            summaryDaily.setTotalWithdrawalCount(withdrawalCount);
            summaryDaily.setTotalWithdrawalAmount(withdrawalAmount);
            summaryDaily.setBonus(bonus);
            summaryDaily.setCashback(cashback);
            // profit = (player total loss - player total win) - cashback - bonus
            // summaryDaily.setNetProfit(profit);
            summaryDaily.setBetAmount(betAmount);
            // depositBalance = total deposit amount - total withdrawal amount
            // summaryDaily.setDepositBalance(depositBalance);
            summaryDaily.setNewPlayerWithoutDeposit(newPlayerCount - newPlayerWithDeposit);
            summaryDaily.setNewPlayerWithDeposit(newPlayerWithDeposit);
            summaryDaily.setActivePlayerCount(activePlayerCount);
            summaryDaily.setActivePlayerCountByGametype(playerCountByGameTypesJson);
            summaryDaily.setPlayerTotalWin(playerTotalWin);
            summaryDaily.setPlayerTotalLoss(playerTotalLoss);
            summaryDailyMapper.insert(summaryDaily);
        }
    }
}
