package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryBetGrossProfitStatus;
import com.tripleonetech.sbe.dashboard.SummaryBetGrossProfitStatusMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

@Component
public class SummaryBetGrossProfitStatusCalculatorSource implements CalculatorWeeklySource, CalculatorMonthlySource {
    @Autowired
    private SummaryBetGrossProfitStatusMapper summaryBetGrossProfitStatusMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void run() {
        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        constant.getBackend().getDefaultLocale());
        // compute player total betAmount
        BigDecimal betAmount = gameLogHourlyReports.stream()
                .map(GameLogHourlyReport::getBet)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // compute player total win
        BigDecimal playerWin = gameLogHourlyReports.stream()
                .map(GameLogHourlyReport::getWin)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // compute player total loss
        BigDecimal playerLoss = gameLogHourlyReports.stream()
                .map(GameLogHourlyReport::getLoss)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        SummaryBetGrossProfitStatus summaryBetGrossProfitStatus = new SummaryBetGrossProfitStatus();
        summaryBetGrossProfitStatus.setPeriod(period);
        summaryBetGrossProfitStatus.setTimezone("UTC");
        summaryBetGrossProfitStatus.setCurrency(siteCurrency);
        summaryBetGrossProfitStatus.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryBetGrossProfitStatus.setBetAmount(betAmount);
        summaryBetGrossProfitStatus.setPlayerWin(playerWin);
        summaryBetGrossProfitStatus.setPlayerLoss(playerLoss);
        summaryBetGrossProfitStatusMapper.insert(summaryBetGrossProfitStatus);
    }

    @Override
    public void calculateWeekly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // as per the requirement specification doc, Sunday will be the first day of the week
            LocalDate weekStart = today.minusDays(6 + today.getDayOfWeek().plus(1).getValue());
            LocalDate weekEnd = today.minusDays(today.plusDays(1).getDayOfWeek().getValue());
            startTime = LocalDateTime.of(weekStart, LocalTime.MIN);
            endTime = LocalDateTime.of(weekEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_WEEK;
            run();
        }
    }

    @Override
    public void calculateMonthly() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            LocalDate previousMonth = today.minusMonths(1);
            LocalDate monthStart = previousMonth.withDayOfMonth(1);
            LocalDate monthEnd = previousMonth.withDayOfMonth(previousMonth.lengthOfMonth());
            startTime = LocalDateTime.of(monthStart, LocalTime.MIN);
            endTime = LocalDateTime.of(monthEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_MONTH;
            run();
        }
    }
}
