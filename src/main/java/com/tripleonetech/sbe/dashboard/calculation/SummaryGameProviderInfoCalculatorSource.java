package com.tripleonetech.sbe.dashboard.calculation;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.dashboard.SummaryGameProviderInfo;
import com.tripleonetech.sbe.dashboard.SummaryGameProviderInfoMapper;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

@Component
public class SummaryGameProviderInfoCalculatorSource
        implements CalculatorRealTimeSource, CalculatorDailySource {
    @Autowired
    private SummaryGameProviderInfoMapper summaryGameProviderInfoMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;
    @Autowired
    private Constant constant;

    private static LocalDate today;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private SummaryPeriodTypeEnum period;
    private SiteCurrencyEnum siteCurrency;
    private byte rank;

    private SummaryGameProviderInfo summaryGameProviderInfo = new SummaryGameProviderInfo();

    @PostConstruct
    public void init() {
        ZoneId zoneId = ZoneId.of("UTC");
        today = LocalDate.now(zoneId);
    }

    private void initRecord() {
        summaryGameProviderInfo.setPeriod(period);
        summaryGameProviderInfo.setTimezone("UTC");
        summaryGameProviderInfo.setCurrency(siteCurrency);
        summaryGameProviderInfo.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryGameProviderInfo.setRank(rank);
        summaryGameProviderInfo.setGameApiId(0);
        summaryGameProviderInfo.setGameTypeId(0);
        summaryGameProviderInfo.setBet(BigDecimal.ZERO);
        summaryGameProviderInfo.setWin(BigDecimal.ZERO);
        summaryGameProviderInfo.setLoss(BigDecimal.ZERO);
        summaryGameProviderInfoMapper.insert(summaryGameProviderInfo);
    }

    private void insertRecord() {
        summaryGameProviderInfo.setPeriod(period);
        summaryGameProviderInfo.setTimezone("UTC");
        summaryGameProviderInfo.setCurrency(siteCurrency);
        summaryGameProviderInfo.setStartDate(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(startTime));
        summaryGameProviderInfo.setRank(rank);
        summaryGameProviderInfoMapper.insert(summaryGameProviderInfo);
    }

    private void run() {
        rank = 1;

        /*
         * Insert placeholder data
         */
        initRecord();
        List<GameLogHourlyReport> gameLogHourlyReports = gameLogHourlyReportMapper
                .getRecordsPeriod(startTime, endTime, null,
                        constant.getBackend().getDefaultLocale());

        /*
         * Sum up the bet amount & grouping by GameApi & GameType
         */
        Map<Integer, Map<Integer, BigDecimal>> resultMapByPlayer = gameLogHourlyReports.stream()
                .collect(
                        Collectors.groupingBy(
                                GameLogHourlyReport::getGameApiId,
                                Collectors.groupingBy(GameLogHourlyReport::getGameTypeId,
                                        Collectors.reducing(
                                                BigDecimal.ZERO, GameLogHourlyReport::getBet, BigDecimal::add))));

        /*
         * Convert Map to TopGameProviderByBet list
         */
        List<TopGameProviderByBet> topGameProviderByBetList = new ArrayList<>();

        // <gameApiId, <gameTypeId, betAmount>>
        Iterator<Map.Entry<Integer, Map<Integer, BigDecimal>>> p = resultMapByPlayer.entrySet().iterator();
        while (p.hasNext()) {
            Map.Entry<Integer, Map<Integer, BigDecimal>> pPair = p.next();
            // <gameTypeId, betAmount>
            Iterator<Map.Entry<Integer, BigDecimal>> c = (pPair.getValue()).entrySet().iterator();
            while (c.hasNext()) {
                TopGameProviderByBet topGameProviderByBet = new TopGameProviderByBet();
                Map.Entry<Integer, BigDecimal> cPair = c.next();
                topGameProviderByBet.setGameApiId(pPair.getKey());
                topGameProviderByBet.setGameTypeId(cPair.getKey());
                topGameProviderByBet.setBet(cPair.getValue());
                topGameProviderByBetList.add(topGameProviderByBet);
            }
        }

        /*
         * Rank by bet amount
         */
        topGameProviderByBetList.sort(Comparator.comparing(TopGameProviderByBet::getBet).reversed());

        Iterator<TopGameProviderByBet> iTopGameProviderByBet = topGameProviderByBetList.iterator();
        while (iTopGameProviderByBet.hasNext()) {
            TopGameProviderByBet providerByBet = iTopGameProviderByBet.next();
            BigDecimal bet = gameLogHourlyReports.stream()
                    .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                    .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                    .map(GameLogHourlyReport::getBet)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal win = gameLogHourlyReports.stream()
                    .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                    .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                    .map(GameLogHourlyReport::getWin)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal loss = gameLogHourlyReports.stream()
                    .filter(x -> providerByBet.getGameApiId() == x.getGameApiId())
                    .filter(x -> providerByBet.getGameTypeId() == x.getGameTypeId())
                    .map(GameLogHourlyReport::getLoss)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            summaryGameProviderInfo.setGameApiId(providerByBet.getGameApiId());
            summaryGameProviderInfo.setGameTypeId(providerByBet.getGameTypeId());
            summaryGameProviderInfo.setBet(bet);
            summaryGameProviderInfo.setWin(win);
            summaryGameProviderInfo.setLoss(loss);
            summaryGameProviderInfo.setRank(rank);
            insertRecord();
            rank++;
        }
    }

    public class TopGameProviderByBet {
        private int gameApiId;
        private int gameTypeId;
        private BigDecimal bet;

        public int getGameApiId() {
            return gameApiId;
        }

        public void setGameApiId(int gameApiId) {
            this.gameApiId = gameApiId;
        }

        public int getGameTypeId() {
            return gameTypeId;
        }

        public void setGameTypeId(int gameTypeId) {
            this.gameTypeId = gameTypeId;
        }

        public BigDecimal getBet() {
            return bet;
        }

        public void setBet(BigDecimal bet) {
            this.bet = bet;
        }

    }

    @Override
    public void calculateRealTime() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            startTime = LocalDateTime.of(today, LocalTime.MIN);
            endTime = LocalDateTime.of(today, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.TODAY;
            run();
        }
    }

    @Override
    public void calculateDaily() {
        for (SiteCurrencyEnum sc : SiteCurrencyEnum.values()) {
            siteCurrency = sc;
            // this month
            LocalDate monthStart = today.withDayOfMonth(1);
            startTime = LocalDateTime.of(monthStart, LocalTime.MIN);
            endTime = LocalDateTime.of(today, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.THIS_MONTH;
            run();

            // last 30days
            LocalDate dayStart = today.minusDays(31);
            LocalDate dayEnd = today.minusDays(1);
            startTime = LocalDateTime.of(dayStart, LocalTime.MIN);
            endTime = LocalDateTime.of(dayEnd, LocalTime.MAX);
            period = SummaryPeriodTypeEnum.LAST_30_DAYS;
            run();
        }
    }
}
