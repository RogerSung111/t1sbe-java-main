package com.tripleonetech.sbe.dashboard.calculation.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.dashboard.calculation.CalculatorDaily;
import com.tripleonetech.sbe.dashboard.calculation.CalculatorMonthly;
import com.tripleonetech.sbe.dashboard.calculation.CalculatorRealTime;
import com.tripleonetech.sbe.dashboard.calculation.CalculatorWeekly;

@Component
@Profile("scheduled")
public class CalculatorRunner {
    private static final Logger logger = LoggerFactory.getLogger(CalculatorRunner.class);

    @Autowired
    private CalculatorRealTime realTimeCalculator;
    @Autowired
    private CalculatorDaily dailyCalculator;
    @Autowired
    private CalculatorWeekly weeklyCalculator;
    @Autowired
    private CalculatorMonthly monthlyCalculator;

    @Scheduled(cron = "0 */15 * * * *")
    public void runRealTime() {
        logger.info("Running real time calculator task");
        realTimeCalculator.run();
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void runDaily() {
        logger.info("Running daily calculator task");
        dailyCalculator.run();
    }

    @Scheduled(cron = "0 59 23 * * SAT")
    public void runWeekly() {
        logger.info("Running weekly calculator task");
        weeklyCalculator.run();
    }

    @Scheduled(cron = "0 59 23 28-31 * *")
    public void runMonthly() {
        logger.info("Running monthly calculator task");
        monthlyCalculator.executeTaskOnLastDayOfMonth();
    }

}
