package com.tripleonetech.sbe.dashboard.calculation;

public interface CalculatorRealTimeSource {
    void calculateRealTime();
}
