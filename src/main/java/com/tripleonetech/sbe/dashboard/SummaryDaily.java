package com.tripleonetech.sbe.dashboard;

import java.math.BigDecimal;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;

public class SummaryDaily {

    private int id;
    private String timezone;
    private SiteCurrencyEnum currency;
    private String startDate;
    private long totalPlayerCount;
    private long totalDepositCount;
    private BigDecimal totalDepositAmount;
    private long totalWithdrawalCount;
    private BigDecimal totalWithdrawalAmount;
    private BigDecimal bonus;
    private BigDecimal cashback;
    private BigDecimal betAmount;
    private long newPlayerWithoutDeposit;
    private long newPlayerWithDeposit;
    private long activePlayerCount;
    private String activePlayerCountByGametype;
    private BigDecimal playerTotalWin;
    private BigDecimal playerTotalLoss;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getTotalPlayerCount() {
        return totalPlayerCount;
    }

    public void setTotalPlayerCount(long totalPlayerCount) {
        this.totalPlayerCount = totalPlayerCount;
    }

    public long getTotalDepositCount() {
        return totalDepositCount;
    }

    public void setTotalDepositCount(long totalDepositCount) {
        this.totalDepositCount = totalDepositCount;
    }

    public BigDecimal getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public long getTotalWithdrawalCount() {
        return totalWithdrawalCount;
    }

    public void setTotalWithdrawalCount(long totalWithdrawalCount) {
        this.totalWithdrawalCount = totalWithdrawalCount;
    }

    public BigDecimal getTotalWithdrawalAmount() {
        return totalWithdrawalAmount;
    }

    public void setTotalWithdrawalAmount(BigDecimal totalWithdrawalAmount) {
        this.totalWithdrawalAmount = totalWithdrawalAmount;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public BigDecimal getCashback() {
        return cashback;
    }

    public void setCashback(BigDecimal cashback) {
        this.cashback = cashback;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public long getNewPlayerWithoutDeposit() {
        return newPlayerWithoutDeposit;
    }

    public void setNewPlayerWithoutDeposit(long newPlayerWithoutDeposit) {
        this.newPlayerWithoutDeposit = newPlayerWithoutDeposit;
    }

    public long getNewPlayerWithDeposit() {
        return newPlayerWithDeposit;
    }

    public void setNewPlayerWithDeposit(long newPlayerWithDeposit) {
        this.newPlayerWithDeposit = newPlayerWithDeposit;
    }

    public long getActivePlayerCount() {
        return activePlayerCount;
    }

    public void setActivePlayerCount(long activePlayerCount) {
        this.activePlayerCount = activePlayerCount;
    }

    public String getActivePlayerCountByGametype() {
        return activePlayerCountByGametype;
    }

    public void setActivePlayerCountByGametype(String activePlayerCountByGametype) {
        this.activePlayerCountByGametype = activePlayerCountByGametype;
    }

    public BigDecimal getPlayerTotalWin() {
        return playerTotalWin;
    }

    public void setPlayerTotalWin(BigDecimal playerTotalWin) {
        this.playerTotalWin = playerTotalWin;
    }

    public BigDecimal getPlayerTotalLoss() {
        return playerTotalLoss;
    }

    public void setPlayerTotalLoss(BigDecimal playerTotalLoss) {
        this.playerTotalLoss = playerTotalLoss;
    }

    public static SummaryDaily getEmptyObject() {
        SummaryDaily summaryDaily = new SummaryDaily();
        summaryDaily.setTotalDepositAmount(BigDecimal.ZERO);
        summaryDaily.setTotalWithdrawalAmount(BigDecimal.ZERO);
        summaryDaily.setBonus(BigDecimal.ZERO);
        summaryDaily.setCashback(BigDecimal.ZERO);
        summaryDaily.setBetAmount(BigDecimal.ZERO);
        summaryDaily.setPlayerTotalWin(BigDecimal.ZERO);
        summaryDaily.setPlayerTotalLoss(BigDecimal.ZERO);
        return summaryDaily;
    }
}
