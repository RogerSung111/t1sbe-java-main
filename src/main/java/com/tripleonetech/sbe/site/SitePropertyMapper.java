package com.tripleonetech.sbe.site;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SitePropertyMapper {
    SiteProperty get(String key);

    default SiteProperty get(SitePropertyKey key) {
        return this.get(key.getName());
    }

    List<SiteProperty> list(List<String> list);

    default List<SiteProperty> list(SitePropertyKey[] array) {
        List<String> list = Stream.of(array).map(e -> e.getName()).collect(Collectors.toList());
        return this.list(list);
    }

    int insert(SiteProperty property);

    int update(SiteProperty property);

    default void saveOrInsert(SiteProperty property) {
        int i = this.update(property);
        if (i < 1) {
            this.insert(property);
        }
    }

    int delete(String key);
}
