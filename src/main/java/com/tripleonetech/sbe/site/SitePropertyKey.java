package com.tripleonetech.sbe.site;

public interface SitePropertyKey {
    String getName();

    enum LiveChatInfoEnum implements SitePropertyKey {
        LIVE_CHAT_URL, LIVE_CHAT_CODE;

        @Override
        public String getName() {
            return this.name();
        }
    }

    enum SiteInfoEnum implements SitePropertyKey {
        TITLE, LOGO, SLOGAN;

        @Override
        public String getName() {
            return this.name();
        }
    }
}
