package com.tripleonetech.sbe.site;

import com.google.common.base.CaseFormat;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SiteProperty {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(SitePropertyKey sitePropertyKey) {
        this.key = sitePropertyKey.getName();
    }

    public static List<SiteProperty> getFields(SitePropertyKey[] keyArray, Object source) throws Exception {
        List<SiteProperty> list = new ArrayList<>();
        for (SitePropertyKey sitePropertyKey : keyArray) {
            Field field = source.getClass().getDeclaredField(
                // Convert enum's member naming convention to camelCase
                CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sitePropertyKey.getName())
            );
            field.setAccessible(true);
            SiteProperty entity = new SiteProperty();
            entity.setKey(sitePropertyKey.getName());
            entity.setValue((String) field.get(source));
            list.add(entity);
        }
        return list;
    }

    public static <T> T setFields(List<SiteProperty> list, Class<T> clazz) throws Exception {
        T obj = clazz.newInstance();
        for (SiteProperty siteProperty : list) {
            Field field = obj.getClass().getDeclaredField(
                // Convert enum's member naming convention to camelCase
                CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, siteProperty.getKey())
            );
            field.setAccessible(true);
            field.set(obj, siteProperty.getValue());
        }
        return obj;
    }
}
