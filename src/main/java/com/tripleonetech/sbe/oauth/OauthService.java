package com.tripleonetech.sbe.oauth;

import java.time.LocalDateTime;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.operator.*;

@Profile("main")
@Service
public class OauthService {
    
    private final static Logger logger = LoggerFactory.getLogger(OauthService.class);
    private final static int REFRESH_TOKEN_IN_30_MINUTES = 30;
    @Autowired
    private DefaultTokenServices defaultTokenServices;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private OperatorMapper operatorMapper;
    @Autowired
    private Constant constant;
    @Autowired
    private OperatorOauthMapper operatorOauthMapper;
    
    public OAuth2AccessToken findOauth2Token(String accessToken) {
        return tokenStore.readAccessToken(accessToken);
    }
    
    public OAuth2AccessToken createOauth2Token(UserDetails userDetail) {

        String clientId = constant.getOauth().getClient();
        Collection<? extends GrantedAuthority> authorities = userDetail.getAuthorities();
        
        OAuth2Request oAuth2Request = new OAuth2Request(null, clientId,
                authorities, true, null, null, null, null,  null);
        
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetail, null, authorities);
        OAuth2Authentication oauth = new OAuth2Authentication(oAuth2Request, authenticationToken);
        OAuth2AccessToken token = defaultTokenServices.createAccessToken(oauth);
        logger.debug("Create OAuth Token - username:{}, access_token:{}, refresh_token:{}", userDetail.getUsername(),
                token.getValue(), token.getRefreshToken().getValue());
        return token;

    }
    
    /**
     * 刷新refresh_token : 當已經創建過的refresh＿token在未來10分鐘將會失效，就會重新產生，避免heartbeat抓到過期refresh_token
     * refresh_token 失效標準： 下次runner執行時間 >  Operator_oauth.NextRefreshTime
     */
    public void refreshExpiredOperatorRefreshToken(String username) {
        Operator operator = operatorMapper.getByUsername(username);
        if(null == operator) {
            logger.error("Operator username: {}, operator is not existed.", username);
            return;
        }
        OperatorOauth operatorOauth = operatorOauthMapper.get(operator.getId());
        OperatorDetails operatorDetails = new OperatorDetails(operator);
        String operatorOauthRefreshToken = "";
        OAuth2AccessToken oauthAccessToken = null;
        //find existing token from db
        if(null != operatorOauth) {
            oauthAccessToken = findOauth2Token(operatorOauth.getAccessToken());
            if(null != oauthAccessToken) {
                operatorOauthRefreshToken = operatorOauth.getRefreshToken();
                //verify expiration_date
                LocalDateTime nextScheduledTime = LocalDateTime.now().plusSeconds(constant.getPublisher().getHeartbeatDelayMilliseconds()/1000);
                if(nextScheduledTime.compareTo(operatorOauth.getNextRefreshTime()) > 0) {
                    logger.debug("NextTimeRefreshToken:{}, refresh_token's next_refresh_time: {} ", nextScheduledTime, operatorOauth.getNextRefreshTime());
                    oauthAccessToken = createOauth2Token(operatorDetails);
                    logger.info("Renew access_token:{}, refresh_token: {}", oauthAccessToken.getValue(), oauthAccessToken.getRefreshToken().getValue());
                }
            }
        }
        // create a new access token when token cannot find in tokenStore
        if(null == oauthAccessToken) {
            oauthAccessToken = createOauth2Token(operatorDetails);
            logger.info("Create new access_token:{}, refresh_token: {}", oauthAccessToken.getValue(), oauthAccessToken.getRefreshToken().getValue());
        }
        
        //update token
        if(!operatorOauthRefreshToken.equals(oauthAccessToken.getRefreshToken().getValue())) {
            OperatorOauth newOperatorOauth = new OperatorOauth();
            newOperatorOauth.setOperatorId(operator.getId());
            newOperatorOauth.setAccessToken(oauthAccessToken.getValue());
            newOperatorOauth.setRefreshToken(oauthAccessToken.getRefreshToken().getValue());
            newOperatorOauth.setNextRefreshTime(LocalDateTime.now().plusMinutes(REFRESH_TOKEN_IN_30_MINUTES));
            operatorOauthMapper.upsert(newOperatorOauth);
        }
    }  
    
    public OperatorOauth getOperatorOauth(String username) {
        Operator operator = operatorMapper.getByUsername(username);
        OperatorOauth operatorOauth = operatorOauthMapper.get(operator.getId());
        return operatorOauth;
    }

}
