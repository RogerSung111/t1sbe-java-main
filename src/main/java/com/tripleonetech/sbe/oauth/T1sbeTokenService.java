package com.tripleonetech.sbe.oauth;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerUserDetails;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Objects;

@Service
public class T1sbeTokenService {

    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private DefaultTokenServices tokenServices;
    @Autowired
    private Constant constant;

    public void refreshUserDetail(SiteCurrencyEnum currency) {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication oauth2Authentication = context.getAuthentication();
        PlayerUserDetails user = (PlayerUserDetails) oauth2Authentication.getPrincipal();

        // get other currency player
        Player player = playerMapper.getByUniqueKey(user.getUsername(), currency);
        PlayerUserDetails playerUserDetails = new PlayerUserDetails(player);

        String currentToken = ((OAuth2AuthenticationDetails) oauth2Authentication.getDetails()).getTokenValue();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(currentToken);
        OAuth2Authentication authentication = tokenStore.readAuthentication(currentToken);

        // renew OAuth2Authentication with other currency player
        oauth2Authentication = new UsernamePasswordAuthenticationToken(playerUserDetails,
                oauth2Authentication.getCredentials(), oauth2Authentication.getAuthorities());
        OAuth2Authentication oauth = new OAuth2Authentication(authentication.getOAuth2Request(), oauth2Authentication);

        // refresh token store
        tokenStore.storeAccessToken(accessToken, oauth);
        // refresh context
        context.setAuthentication(oauth2Authentication);
    }


    public Boolean revokePlayerTokens(String username, Boolean retainPresent) {
        Collection<OAuth2AccessToken> tokens = listPlayerTokens(username);
        if (retainPresent) {
            Authentication oauth2Authentication = SecurityContextHolder.getContext().getAuthentication();
            final OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) oauth2Authentication.getDetails();
            String currentToken = details.getTokenValue();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(currentToken);
            tokens.remove(accessToken);
        }
        for (OAuth2AccessToken otherToken : tokens) {
            tokenStore.removeAccessToken(otherToken);
            if (Objects.nonNull(otherToken.getRefreshToken())) {
                tokenStore.removeRefreshToken(otherToken.getRefreshToken());
            }
        }
        tokens = tokenStore.findTokensByClientIdAndUserName(constant
                .getOauthPlayer().getClient(), username);
        if (retainPresent) {
            return CollectionUtils.size(tokens) == 1;
        } else {
            return CollectionUtils.sizeIsEmpty(tokens);
        }
    }

    public Collection<OAuth2AccessToken> listPlayerTokens(String username) {
        return tokenStore.findTokensByClientIdAndUserName(constant
                .getOauthPlayer().getClient(), username);
    }

    public boolean revokeToken(String tokenValue) {
        return tokenServices.revokeToken(tokenValue);
    }

}
