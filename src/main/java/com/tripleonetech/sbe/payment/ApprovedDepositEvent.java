package com.tripleonetech.sbe.payment;

import org.springframework.context.ApplicationEvent;

public class ApprovedDepositEvent extends ApplicationEvent {

    /**
     * 
     */
    private static final long serialVersionUID = -8076263420307288780L;

    public ApprovedDepositEvent(DepositRequest request) {
        super(request);
    }
}