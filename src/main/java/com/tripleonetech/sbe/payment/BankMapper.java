package com.tripleonetech.sbe.payment;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BankMapper {
    Bank get(int id);

    List<Bank> withdrawBankList();

    List<Bank> list();
}
