package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.LocaleAwareQuery;
import com.tripleonetech.sbe.common.model.PageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class DepositRequestQueryForm extends PageQueryForm implements LocaleAwareQuery {
    @ApiModelProperty(value = "Start time", required = false)
    private LocalDateTime requestedDateStart;
    @ApiModelProperty(value = "End time", required = false)
    private LocalDateTime requestedDateEnd;
    @ApiModelProperty(value = "Player ID", required = false)
    private Integer playerId;
    @ApiModelProperty(value = "Username", required = false)
    private String username;
    @ApiModelProperty(value = "Currency", required = false)
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Deposit request status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11), EXPIRED(12)", allowableValues = "0,1,10,11,12", required = false)
    private Integer status;
    @ApiModelProperty(value = "Payment APIs", required = false)
    private List<Integer> paymentApiIds;

    public LocalDateTime getRequestedDateStart() {
        return requestedDateStart;
    }

    public void setRequestedDateStart(ZonedDateTime requestedDateStartZoned) {
        this.requestedDateStart = DateUtils.zonedToLocal(requestedDateStartZoned);
    }

    public void setRequestedDateStartLocal(LocalDateTime requestedDateStartLocal) {
        this.requestedDateStart = requestedDateStartLocal;
    }

    public LocalDateTime getRequestedDateEnd() {
        return requestedDateEnd;
    }

    public void setRequestedDateEnd(ZonedDateTime requestedDateEndZoned) {
        this.requestedDateEnd = DateUtils.zonedToLocal(requestedDateEndZoned);
    }

    public void setRequestedDateEndLocal(LocalDateTime requestedDateEndLocal) {
        this.requestedDateEnd = requestedDateEndLocal;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @ApiModelProperty(hidden = true, value="Derived property, should not appear in swagger UI")
    public DepositStatusEnum getStatusEnum() {
        return Objects.isNull(getStatus()) ? null : DepositStatusEnum.valueOf(getStatus());
    }

    public List<Integer> getPaymentApiIds() {
        return paymentApiIds;
    }

    public void setPaymentApiIds(List<Integer> paymentApiIds) {
        this.paymentApiIds = paymentApiIds;
    }


}
