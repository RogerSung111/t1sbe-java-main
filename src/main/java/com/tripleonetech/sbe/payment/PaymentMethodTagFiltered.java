package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.model.TagFilteredEntity;

public class PaymentMethodTagFiltered extends TagFilteredEntity {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
