package com.tripleonetech.sbe.payment.config;

import com.tripleonetech.sbe.common.integration.ApiConfigView;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

public class PaymentApiConfigView extends ApiConfigView {
    private BigDecimal dailyMaxDeposit;
    private BigDecimal minDepositPerTrans;
    private BigDecimal maxDepositPerTrans;
    private BigDecimal chargeRatio;
    private BigDecimal fixedCharge;

    public BigDecimal getDailyMaxDeposit() {
        return dailyMaxDeposit;
    }

    public void setDailyMaxDeposit(BigDecimal dailyMaxDeposit) {
        this.dailyMaxDeposit = dailyMaxDeposit;
    }

    public BigDecimal getMinDepositPerTrans() {
        return minDepositPerTrans;
    }

    public void setMinDepositPerTrans(BigDecimal minDepositPerTrans) {
        this.minDepositPerTrans = minDepositPerTrans;
    }

    public BigDecimal getMaxDepositPerTrans() {
        return maxDepositPerTrans;
    }

    public void setMaxDepositPerTrans(BigDecimal maxDepositPerTrans) {
        this.maxDepositPerTrans = maxDepositPerTrans;
    }

    public BigDecimal getChargeRatio() {
        return chargeRatio;
    }

    public void setChargeRatio(BigDecimal chargeRatio) {
        this.chargeRatio = chargeRatio;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public static PaymentApiConfigView from(PaymentApiConfig config, boolean online) {
        PaymentApiConfigView apiConfigView = new PaymentApiConfigView();
        BeanUtils.copyProperties(config, apiConfigView);
        apiConfigView.setOnline(online);
        return apiConfigView;
    }
}
