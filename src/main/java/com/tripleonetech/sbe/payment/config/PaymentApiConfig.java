package com.tripleonetech.sbe.payment.config;

import com.tripleonetech.sbe.common.integration.ApiConfig;

import java.math.BigDecimal;

public class PaymentApiConfig extends ApiConfig {
    private BigDecimal dailyMaxDeposit;
    private BigDecimal minDepositPerTrans;
    private BigDecimal maxDepositPerTrans;
    private BigDecimal chargeRatio;
    private BigDecimal fixedCharge;

    public BigDecimal getDailyMaxDeposit() {
        if(dailyMaxDeposit == null){
            return BigDecimal.ZERO;
        }
        return dailyMaxDeposit;
    }

    public void setDailyMaxDeposit(BigDecimal dailyMaxDeposit) {
        this.dailyMaxDeposit = dailyMaxDeposit;
    }

    public BigDecimal getMinDepositPerTrans() {
        if(minDepositPerTrans == null){
            return BigDecimal.ZERO;
        }
        return minDepositPerTrans;
    }

    public void setMinDepositPerTrans(BigDecimal minDepositPerTrans) {
        this.minDepositPerTrans = minDepositPerTrans;
    }

    public BigDecimal getMaxDepositPerTrans() {
        if(maxDepositPerTrans == null){
            return BigDecimal.ZERO;
        }
        return maxDepositPerTrans;
    }

    public void setMaxDepositPerTrans(BigDecimal maxDepositPerTrans) {
        this.maxDepositPerTrans = maxDepositPerTrans;
    }

    public BigDecimal getChargeRatio() {
        if(chargeRatio == null) {
            return BigDecimal.ZERO;
        }
        return chargeRatio;
    }

    public void setChargeRatio(BigDecimal chargeRatio) {
        this.chargeRatio = chargeRatio;
    }

    public BigDecimal getFixedCharge() {
        if(fixedCharge == null){
            return BigDecimal.ZERO;
        }
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }
}
