package com.tripleonetech.sbe.payment.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.payment.PaymentApiFactory;

@Aspect
@Component
// This aspect keeps api factory's cache up to date
public class PaymentApiConfigMapperAspect {
    @Autowired
    private PaymentApiFactory paymentApiFactory;

    @After("execution(* com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper.update*(..)))")
    public void refreshPaymentApi(JoinPoint joinPoint) {
        PaymentApiConfig config = (PaymentApiConfig) joinPoint.getArgs()[0];
        paymentApiFactory.refreshById(config.getId());
    }

    @After("execution(* com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper.delete(..)))")
    public void refreshPaymentApiWhenDeleteExecution(JoinPoint joinPoint) {
        paymentApiFactory.refreshById((int) joinPoint.getArgs()[0]);
    }
}
