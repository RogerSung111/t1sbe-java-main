package com.tripleonetech.sbe.payment.config;

import com.tripleonetech.sbe.common.integration.ApiConfigMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PaymentApiConfigMapper extends ApiConfigMapper {
    @Override
    List<PaymentApiConfig> list();

    List<PaymentApiConfig> listPaymentApiConfigByCode(List<String> code);

    @Override
    PaymentApiConfig get(@Param("id") Integer id);

    int insert(PaymentApiConfig config);

    int update(PaymentApiConfig config);
    
    int delete(@Param("id") Integer id);


}
