package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.player.PlayerIdView;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PaymentMethodPlayerMapper {
    void insert(List<PaymentMethodPlayer> paymentAccountPlayer);

    void deleteByPaymentMethodId(int paymentMethodId);

    List<PlayerIdView> getPlayerInfoByPaymentMethod(int id);
}
