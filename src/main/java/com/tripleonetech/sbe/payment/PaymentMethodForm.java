package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.model.TagFilteredEntity;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PaymentMethodForm extends TagFilteredEntity {
    @NotNull
    @ApiModelProperty(value = "The name of payment method", required = true)
    private String name;
    @NotNull
    @ApiModelProperty(value = "Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WEXIN(3), ALIPAY(4), OTHERS(0)",
            allowableValues = "0,1,2,3,4",
            required = true)
    private Integer type;
    @NotNull
    @ApiModelProperty(value = "Currency of this payment method. Note that if a payment API is specified, " +
            "currency will follow that of the payment API", required = true)
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "The ID of payment API")
    private Integer paymentApiId;
    @ApiModelProperty(value = "Maximum amount of deposit per day")
    private BigDecimal dailyMaxDeposit;
    @ApiModelProperty(value = "Minimum amount of deposit per transaction")
    private BigDecimal minDepositPerTrans;
    @ApiModelProperty(value = "Maximum amount of deposit per transaction")
    private BigDecimal maxDepositPerTrans;
    @Max(value = 100)
    @Min(value = 0)
    @ApiModelProperty(value = "Payment charge percentage, values 0-100")
    private BigDecimal chargeRatio;
    @Min(value = 0)
    @ApiModelProperty(value = "Payment charge amount")
    private BigDecimal fixedCharge;
    @ApiModelProperty(value = "Payer of payment charge; true = PLAYER, false = PLATFORM")
    private boolean playerBearPaymentCharge;
    @Size(max = 65535)
    @ApiModelProperty(value = "Note")
    private String note;
    @ApiModelProperty(value = "Applicable player IDs (Note: Front-end will pass in player credential ID, will be converted to player ID upon saving)")
    private List<Integer> playerIds;
    @ApiModelProperty(value = "Priority of this payment method, larger number means higher priority")
    private int priority;

    public Integer getType() {
        return type;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not appear in swagger UI")
    public PaymentMethodTypeEnum getTypeEnum() {
        return PaymentMethodTypeEnum.valueOf(getType());
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getPaymentApiId() {
        return paymentApiId;
    }

    public void setPaymentApiId(Integer paymentApiId) {
        this.paymentApiId = paymentApiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getDailyMaxDeposit() {
        return dailyMaxDeposit;
    }

    public void setDailyMaxDeposit(BigDecimal dailyMaxDeposit) {
        this.dailyMaxDeposit = dailyMaxDeposit;
    }

    public BigDecimal getMinDepositPerTrans() {
        return minDepositPerTrans;
    }

    public void setMinDepositPerTrans(BigDecimal minDepositPerTrans) {
        this.minDepositPerTrans = minDepositPerTrans;
    }

    public BigDecimal getMaxDepositPerTrans() {
        return maxDepositPerTrans;
    }

    public void setMaxDepositPerTrans(BigDecimal maxDepositPerTrans) {
        this.maxDepositPerTrans = maxDepositPerTrans;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Integer> getPlayerIds() {
        if(playerIds == null) {
            playerIds = new ArrayList<>();
        }
        return playerIds;
    }

    public void setPlayerIds(List<Integer> playerIds) {
        this.playerIds = playerIds;
    }

    public BigDecimal getChargeRatio() {
        return chargeRatio;
    }

    public void setChargeRatio(BigDecimal chargeRatio) {
        this.chargeRatio = chargeRatio;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public boolean isPlayerBearPaymentCharge() {
        return playerBearPaymentCharge;
    }

    public void setPlayerBearPaymentCharge(boolean playerBearPaymentCharge) {
        this.playerBearPaymentCharge = playerBearPaymentCharge;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
