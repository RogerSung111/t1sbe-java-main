package com.tripleonetech.sbe.payment;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface DepositRequestMapper {
    List<DepositRequestView> list(
            @Param("query") DepositRequestQueryForm query,
            @Param("pageNum") Integer pageNum,
            @Param("pageSize") Integer pageSize);

    DepositRequest get(long id);

    DepositRequestView getView(long id);

    int insert(DepositRequest depositRequest);

    int updateStatus(@Param("id") long id, @Param("status") DepositStatusEnum status);

    int updateExtraInformation(DepositRequest depositRequest);

    int getPendingRequestCount();
    
    BigDecimal getPendingRequestAmount(Integer playerId);

    // Used for data calculation
    List<DepositRequestView> querySummary(Map<String, Object> params);

    List<DepositRequest> getApprovedInPeriod(
            @Param("syncStartHour") LocalDateTime syncStartHour,
            @Param("syncEndHour") LocalDateTime syncEndHour);
}
