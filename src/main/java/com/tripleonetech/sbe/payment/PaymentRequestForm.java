package com.tripleonetech.sbe.payment;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.tripleonetech.sbe.player.Player;
import io.swagger.annotations.ApiModelProperty;

public class PaymentRequestForm {
    @NotNull
    @ApiModelProperty(value = "Payment Method ID", required = true)
    private int paymentMethodId;
    @NotNull
    @ApiModelProperty(value = "Deposit Amount", required = true)
    private BigDecimal amount;
    @ApiModelProperty(value = "Bank Code", required = false)
    private String bankCode;

    public int getPaymentMethodId() {
        return paymentMethodId;
    }
    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getBankCode() {
        return bankCode;
    }
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public String toString() {
        return "PaymentApiFormView [paymentMethodId=" + paymentMethodId + ", amount="
                + amount + ", bankCode=" + bankCode + "]";
    }

    public static class OperatorPaymentRequestForm extends PaymentRequestForm {

        @NotNull
        @ApiModelProperty(value = "player id", required = true)
        private int playerId;

        public int getPlayerId() {
            return playerId;
        }

        public void setPlayerId(int playerId) {
            this.playerId = playerId;
        }
    }
}
