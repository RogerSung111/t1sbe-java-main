package com.tripleonetech.sbe.payment;

public class PaymentMethodPlayerTag {
    private int paymentMethodId;
    private int playerTagId;

    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public int getPlayerTagId() {
        return playerTagId;
    }

    public void setPlayerTagId(int playerTagId) {
        this.playerTagId = playerTagId;
    }
}
