package com.tripleonetech.sbe.payment;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class DepositRequestApprovalForm {
    @ApiModelProperty(value = "Comment of deposit request process history", required = false)
    private String comment;
    @NotNull
    @ApiModelProperty(required = true, value = "Deposit request status: OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)", allowableValues = "0,1,10,11")
    private Integer status;
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @ApiModelProperty(hidden = true, value="Derived property, should not appear in swagger UI")
    public DepositStatusEnum getStatusEnum() {
        return DepositStatusEnum.valueOf(getStatus());
    }
}
