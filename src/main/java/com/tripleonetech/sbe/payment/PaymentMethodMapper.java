package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper
public interface PaymentMethodMapper {
    PaymentMethod get(int id);

    // Query then getById using query result
    List<Integer> query(@Param("query") PaymentMethodQueryForm queryForm);
    List<PaymentMethodTagFiltered> queryAvailable(@Param("currency") SiteCurrencyEnum currency);
    List<Integer> queryAvailableByPlayerIds(@Param("playerCredentialId") int playerCredentialId, @Param("currency") SiteCurrencyEnum currency);

    // Filters available payment methods using daily limits. Also returns the sum of deposit amount calculated from limitStartTime until now
    List<PaymentMethod.PaymentMethodIdAndAmount> getIdWithAmounts(@Param("ids") Set<Integer> ids,
                                                                    @Param("limitStartTime")LocalDateTime limitStartTime);

    default PaymentMethodDetailView getById(Integer id) {
        List<PaymentMethodDetailView> paymentMethods = getById(new HashSet<>(Arrays.asList(id)), new PaymentMethodQueryForm());
        if(paymentMethods == null || paymentMethods.size() == 0) {
            return null;
        }
        return paymentMethods.get(0);
    }
    List<PaymentMethodDetailView> getById(@Param("ids") Set<Integer> ids,
                                          @Param("query") PaymentMethodQueryForm queryForm);

    // TODO: This needs refactor
    List<PaymentMethod> getAvailablePaymentMethodsByPlayerTag(@Param("currency") String currency,
                                                              @Param("playerTagId") int playerTagId);

    void insert(PaymentMethod paymentAccount);

    void update(PaymentMethod paymentAccount);

    void updateEnabled(@Param("id") int id, @Param("enabled") boolean enabled);

    void setDelete(int id);
    
    List<PaymentMethod> queryPaymentMethodByPaymentApiId(Integer paymentApiId);


}
