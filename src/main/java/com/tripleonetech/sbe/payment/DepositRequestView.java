package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class DepositRequestView {
    
    @ApiModelProperty(value = "Status of deposit request, OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11), EXPIRED(12)")
    private DepositStatusEnum status;
    
    @ApiModelProperty(value = "Deposit request ID")
    private Long id;
    
    @ApiModelProperty(value = "currency")
    private SiteCurrencyEnum currency;
    
    @ApiModelProperty(value = "deposit amount")
    private BigDecimal amount;
    
    @ApiModelProperty(value = "payment method")
    private PaymentMethod paymentMethod;
    
    @ApiModelProperty(value = "payment API ID")
    private Integer paymentApiId;
    
    @ApiModelProperty(value = "Request datetime")
    private ZonedDateTime requestedDate;
    
    @ApiModelProperty(value = "Expiration datetime")
    private ZonedDateTime expirationDate;
    
    @ApiModelProperty(value = "Last updated time")
    private LocalDateTime updatedAt;
    
    @ApiModelProperty(value = "player username")
    private String username;
    
    @ApiModelProperty(value = "player ID")
    private Integer playerId;
    
    @ApiModelProperty(value = "external payment ID")
    private String externalUid;

    public DepositStatusEnum getStatus() {
        if(DepositStatusEnum.OPEN == status && ZonedDateTime.now().isAfter(getExpirationDate())){
            return DepositStatusEnum.EXPIRED;
        }
        return status;
    }

    public void setStatus(DepositStatusEnum status) {
        this.status = status;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getPaymentApiId() {
        return paymentApiId;
    }

    public void setPaymentApiId(Integer paymentApiId) {
        this.paymentApiId = paymentApiId;
    }

    public ZonedDateTime getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(ZonedDateTime requestedDate) {
        this.requestedDate = requestedDate;
    }

    public ZonedDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(this.updatedAt);
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getExternalUid() {
        return externalUid;
    }

    public void setExternalUid(String externalUid) {
        this.externalUid = externalUid;
    }

}
