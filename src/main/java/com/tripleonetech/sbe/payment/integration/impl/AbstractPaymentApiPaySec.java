package com.tripleonetech.sbe.payment.integration.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.CryptoUtils;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.integration.AbstractPaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.player.wallet.WalletService;

import io.netty.util.internal.StringUtil;

@Component
@Scope("prototype")
public abstract class AbstractPaymentApiPaySec extends AbstractPaymentApi {

    private final static Logger logger = LoggerFactory.getLogger(AbstractPaymentApiPaySec.class);

    private final static String PAYMENT_STATUS_SUCCESS = "SUCCESS";
    private final static String PAYMENT_STATUS_FAILED = "FAILED";
    private final static String PAYMENT_STATUS_PENDING = "PENDING"; // Timeout in 45 mins
    private final static String PAYMENT_STATUS_DECLINED = "DECLINE";

    private final static String CALLBACK_RESPONSE_MESSAGE_OK = "OK";

    HttpResponse response;

    @Autowired
    WalletService walletService;

    @Autowired
    DepositRequestMapper depositRequestMapper;

    @Override
    public PaymentApiResult submitRequest(DepositRequest depositRequest) {
        PaymentApiResult apiResult = new PaymentApiResult(ApiResponseEnum.OK);

        if (null != depositRequest.getId() && depositRequest.getId() > 0) {

            depositRequest = depositRequestMapper.get(depositRequest.getId());
            // 2.Get PAYSEC's token
            String token = getApiRequestToken(depositRequest); // PostAsJSON
            if (StringUtils.isBlank(token)) {
                logger.error("Genereate PAYSEC's token failed");
                return new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
            }

            // 3. Set Redirect URL depending on paymentType (QRCODE/Bank)
            if (getPaymentType().equals("QRCODE")) {
                String qrCodeUrl = getQrCodeUrl(token);// PostAsJSON
                apiResult.setParam("url", qrCodeUrl);
            } else {
                apiResult.setParam("url", getConfig().get("sendTokenFormUrl"));
                apiResult.setParam("token", token);
            }

        } else {
            logger.info("Creating deposit_request is unsuccessful");
            return new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        }

        return apiResult;
    }

    public String getPaymentType() {
        return null;
    }

    protected String getSignature(Map<String, String> params) {

        // example : signature = cartId;orderAmount;currency;merchantCode;version
        String signature = "";

        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            signature += entry.getValue();
            if (iterator.hasNext()) {
                signature += ";";
            }
        }
        logger.debug("Before encrypt - Signature :[{}]", signature);
        String signatureSha256 = DigestUtils.sha256Hex(signature);
        String signatureHash = CryptoUtils.bcryptHash(signatureSha256, getConfig().get("key"));

        boolean isVerified = CryptoUtils.verifyBcryptHash(signatureSha256, getConfig().get("key"), signatureHash);
        if (!isVerified) {
            logger.error("VerifyHash is failed");
            return null;
        }

        return signatureHash;
    }

    protected Map<String, Object> generateRequestParams(DepositRequest depositRequest) {

        Map<String, Object> paramMap = new HashMap<>();
        Map<String, Object> header = getHeaderParams(depositRequest);
        Map<String, Object> body = getBodyParams(depositRequest);
        paramMap.put("header", header);
        paramMap.put("body", body);

        logger.debug("Generated params : {} ", JsonUtils.objectToJson(paramMap));

        return paramMap;
    }

    protected Map<String, Object> getHeaderParams(DepositRequest depositRequest) {

        // --init params
        Long cartId = depositRequest.getId();
        String orderAmount = depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        // combine params for signature
        Map<String, String> params = new LinkedHashMap<>();
        params.put("cartId", String.valueOf(cartId));
        params.put("orderAmount", orderAmount);
        params.put("currency", getConfig().get("currency"));
        params.put("merchantCode", getConfig().get("merchantCode"));
        params.put("version", getConfig().get("version"));

        // signature = cartId;orderAmount;currency;merchantCode;version
        String signature = getSignature(params);

        // header
        Map<String, Object> header = new HashMap<>();
        header.put("version", getConfig().get("version"));
        header.put("merchantCode", getConfig().get("merchantCode"));
        header.put("signature", signature);

        return header;
    }

    protected Map<String, Object> getBodyParams(DepositRequest depositRequest) {

        Long cartId = depositRequest.getId();
        String orderAmount = depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        Map<String, Object> body = new HashMap<>();
        body.put("channelCode", getConfig().get("channelCode"));
        body.put("notifyURL", getConfig().get("notifyUrl"));
        body.put("returnURL", getConfig().get("returnUrl"));
        body.put("orderAmount", orderAmount);
        body.put("orderTime", String.valueOf(depositRequest.getRequestedDate()));
        body.put("cartId", cartId);
        body.put("currency", getConfig().get("currency"));

        return body;
    }

    @SuppressWarnings("unchecked")
    public String getApiRequestToken(DepositRequest depositRequest) {
        logger.info("Request PAYSEC's Token...");

        Map<String, Object> params = generateRequestParams(depositRequest);
        if (null == params) {
            logger.error("GenerateParams failed");
            return null;
        }

        // submit request to get Token from PAYSEC
        response = postAsJsonData(getConfig().get("sendTokenFormUrl"), params);

        String responseStr = "";
        try {
            responseStr = new BasicResponseHandler().handleResponse(response);
        } catch (HttpResponseException e) {
            logger.error("HttpResponseException : {}" + e);
        } catch (IOException e) {
            logger.error("IOException : {}" + e);
        }
        logger.debug("PAYSEC- Response  : {} ", responseStr);

        Map<String, Map<String, String>> responseMap = new HashMap<>();
        responseMap = JsonUtils.jsonToObject(responseStr, responseMap.getClass());

        if (StringUtils.isBlank(responseMap.get("body").get("token"))) {
            return null;
        }

        return responseMap.get("body").get("token");

    }

    @SuppressWarnings("unchecked")
    private String getQrCodeUrl(String token) {
        Map<String, Object> params = new HashMap<>();
        params.put("token", token);

        response = postAsJsonData(getConfig().get("sendTokenJsonUrl"), params);
        String responseStr = "";
        try {
            responseStr = new BasicResponseHandler().handleResponse(response);
        } catch (HttpResponseException e) {
            logger.error("HttpResponseException : {}" + e);
        } catch (IOException e) {
            logger.error("IOException : {}" + e);
        }
        Map<String, String> responseMap = new HashMap<>();
        responseMap = JsonUtils.jsonToObject(responseStr, responseMap.getClass());
        return responseMap.get("qrCode");
    }

    @Override
    public PaymentApiResult callback(Map<String, ? extends Object> param) {
        
        String status = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("status")))) {
            status = String.valueOf(param.get("status"));
        }
        String cartId = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("cartId")))) {
            cartId = String.valueOf(param.get("cartId"));
        }

        DepositRequest depositRequest = depositRequestMapper.get(Long.valueOf(cartId));

        if (depositRequest == null) {
            logger.error("Callback - Cannot find this deposit_request(id:{})", cartId);
            return new PaymentApiResult(ApiResponseEnum.BAD_REQUEST);
        }

        // Only process OPEN deposit_request & Avoid deposit_request is executed many times
        if (depositRequest.getStatus() != DepositStatusEnum.OPEN) {
            logger.debug(
                    "Callback - The status of depsoit_request is not [OPEN], deposit_request_id:[{}], current status: [{}]",
                    cartId, DepositStatusEnum.values()[depositRequest.getStatus().getCode()]);
            return PaymentApiResult.callbackOk("OK");
        }

        // Validate responded parameters
        if (!validateCallbackRespndedParams(depositRequest, param)) {
            logger.debug("Callback - Validate responded parameters failed");
            return new PaymentApiResult(ApiResponseEnum.BAD_REQUEST);
        }

        // According responded status to implement following action
        if (!StringUtil.isNullOrEmpty(status)) {
            switch (status) {
            case PAYMENT_STATUS_PENDING:
                return new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);

            case PAYMENT_STATUS_SUCCESS:
                // 1. Update status of deposit_request (SUCCESS)
                depositRequestMapper.updateStatus(Long.valueOf(cartId), DepositStatusEnum.APPROVED);
                logger.info("Callback - Approve deposit_request(id:{}), orderAmount:{} / respondedAmount:{}", cartId,
                        depositRequest.getAmount(), String.valueOf(param.get("orderAmount")));
                // 2. Transfer the amount into play's main wallet
                walletService.depositToMainWallet(depositRequest.getPlayerId(), depositRequest.getAmount());
                logger.debug("Callback - Deposit into main wallet, deposit_request(id:{}), player:{} / orderAmount:{}",
                        cartId, depositRequest.getUsername(), String.valueOf(param.get("orderAmount")));
                break;

            case PAYMENT_STATUS_DECLINED:
                depositRequestMapper.updateStatus(Long.valueOf(cartId), DepositStatusEnum.REJECTED);
                logger.info("Callback - Deposit is declined , deposit_request(id:{})", cartId);
                break;

            case PAYMENT_STATUS_FAILED:
                // 1. Update status of deposit_request (FAILED)
                depositRequestMapper.updateStatus(Long.valueOf(cartId), DepositStatusEnum.REJECTED);
                logger.info("Callback - Deposit failed, deposit_request(id:{})", cartId);
                break;
            }
        }

        return PaymentApiResult.callbackOk("OK");
    }

    private boolean validateCallbackRespndedParams(DepositRequest depositRequest, Map<String, ? extends Object> param) {
        // Validate responded params
        String cartId = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("cartId")))) {
            cartId = String.valueOf(param.get("cartId"));
        }

        BigDecimal orderAmount = new BigDecimal(0);
        if (!StringUtils.isBlank(String.valueOf(param.get("orderAmount")))) {
            orderAmount = new BigDecimal(String.valueOf(param.get("orderAmount")));
        }

        String currency = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("currency")))) {
            currency = String.valueOf(param.get("currency"));
        }

        String version = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("version")))) {
            version = String.valueOf(param.get("version"));
        }
        String status = "";
        if (!StringUtils.isBlank(String.valueOf(param.get("status")))) {
            status = String.valueOf(param.get("status"));
        }

        // Signature format : cartId;orderAmount;currency;merchantCode;version;status
        Map<String, String> signatureMap = new LinkedHashMap<>();
        signatureMap.put("cartId", cartId);
        signatureMap.put("orderAmount", orderAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        signatureMap.put("currency", currency);
        signatureMap.put("merchantCode", getConfig().get("merchantCode"));
        signatureMap.put("version", version);
        signatureMap.put("status", status);

        if (depositRequest.getAmount().compareTo(orderAmount) != 0) {
            logger.error(
                    "Callback - Validate Order Amount failed, becuase responded orderAmount is different with orderAmount of deposit_request");
            return false;
        }
        // signature should be same with responded signature
        String signature = getSignature(signatureMap);
        if (!signature.equals(String.valueOf(param.get("signature")))) {
            logger.error(
                    "Callback - Validate Signature failed, becuase signature is different with responded signature.");
            return false;
        }

        return true;
    }

}
