package com.tripleonetech.sbe.payment.integration;


import com.tripleonetech.sbe.common.integration.ApiCallbackable;
import com.tripleonetech.sbe.common.integration.ThirdPartyApi;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;

public interface PaymentApi extends ThirdPartyApi, ApiCallbackable {
    
    PaymentApiResult submitRequest(DepositRequest depositRequest);
    
    PaymentFormTypeEnum getFormType();
}
