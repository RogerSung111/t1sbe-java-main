package com.tripleonetech.sbe.payment.integration.impl.paybus;

import java.math.BigDecimal;

import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.integration.impl.PaymentBaseResponse;

public class PayBusDepositInformationResponse extends PayBusResponse implements PaymentBaseResponse {
    
    private Data data;
    
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        
   
        private String merchantCode;
        private BigDecimal amount;
        private BigDecimal realAmount;
        private Integer status;
        private Integer refundStatus;
        private String orderNo;
        private String customNo;
        private String bank;
        private String currency;
        private String sign;
        private String platformOrderNo;
    
        public String getMerchantCode() {
            return merchantCode;
        }
    
    
        public void setMerchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
        }
    
    
        public BigDecimal getAmount() {
            return amount;
        }
    
    
        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    
    
        public BigDecimal getRealAmount() {
            return realAmount;
        }
    
    
        public void setRealAmount(BigDecimal realAmount) {
            this.realAmount = realAmount;
        }
    
    
        public Integer getStatus() {
            return status;
        }
    
    
        public void setStatus(Integer status) {
            this.status = status;
        }
    
    
        public Integer getRefundStatus() {
            return refundStatus;
        }
    
    
        public void setRefundStatus(Integer refundStatus) {
            this.refundStatus = refundStatus;
        }
    
    
        public String getOrderNo() {
            return orderNo;
        }
    
    
        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }
    
    
        public String getCustomNo() {
            return customNo;
        }
    
    
        public void setCustomNo(String customNo) {
            this.customNo = customNo;
        }
    
        public String getBank() {
            return bank;
        }
    
    
        public void setBank(String bank) {
            this.bank = bank;
        }
    
        public String getCurrency() {
            return currency;
        }
    
    
        public void setCurrency(String currency) {
            this.currency = currency;
        }
    
    
        public String getSign() {
            return sign;
        }
      
        public void setSign(String sign) {
            this.sign = sign;
        }
    
        public String getPlatformOrderNo() {
            return platformOrderNo;
        }
    
        public void setPlatformOrderNo(String platformOrderNo) {
            this.platformOrderNo = platformOrderNo;
        }
    }


    @Override
    public PaymentApiResult toApiResult() {
        PaymentApiResult result = new PaymentApiResult(getResponse());
        return result;
    }
}
