package com.tripleonetech.sbe.payment.integration.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;

@Component
@Scope("prototype")
public class PaymentApiOnePayBankTransferPay extends AbstractPaymentApiOnePay{
    
    private final static Logger logger = LoggerFactory.getLogger(PaymentApiOnePayBankTransferPay.class);
    private final static String API_VERSION = "1.0";
    private final static String ISSUING_BANK_UNIONPAY = "UNIONPAY";
    private final static String PAY_TYPE_EC = "EC";
    
    private static List<ApiMetaField> META_FIELDS;
    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("merchantId", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("privateKey", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("publicKey", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("notifyUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("returnUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("requestUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("bankList", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("formType", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("formExtraInfo", MetaFieldTypeEnum.TEXT, false));
            }
        };
    }
    
    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    @Override
    public Map<String, String> getBodyParams(DepositRequest depositRequest) {
        
        Map<String, String> requestBody = new HashMap<>();
        
        requestBody.put("amountFee", String.valueOf(depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP)));
        requestBody.put("merchantTradeId", String.valueOf(depositRequest.getId()));
        requestBody.put("version", API_VERSION);
        requestBody.put("issuingBank", ISSUING_BANK_UNIONPAY);
        requestBody.put("payType", PAY_TYPE_EC);
        requestBody.put("merchantId", getConfig("merchantId"));
        requestBody.put("notifyUrl", getConfig("notifyUrl"));
        requestBody.put("currency", getConfig().getCurrency().name());
        requestBody.put("inputCharset", "UTF-8");
        requestBody.put("goodsTitle", "Deposit");
        requestBody.put("returnUrl", getConfig("returnUrl"));
        requestBody.put("sign", Hex.encodeHexString(encodeSign(requestBody).getBytes()));
        requestBody.put("signType", "RSA");
        
        return requestBody;
    }
    
    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.SimpleForm;
    }
    
    //Submit request to third-party payment API
    @Override
    public PaymentApiResult submitRequest(DepositRequest depositRequest) {
        PaymentApiResult apiResult = new PaymentApiResult(ApiResponseEnum.OK);
        Map<String, String> params = getBodyParams(depositRequest);
        apiResult.setRedirectUrl(getConfig("requestUrl"));
        params.forEach((k,v) -> apiResult.setParam(k, v));
        logger.info("Will redirect URL:[{}], Body:[{}]", apiResult.getRedirectUrl(), apiResult.getParams().toString());
       
        return apiResult;
    }

    @Override
    DepositRequest findDepositRequest(Map<String, String> params) {
        
        Long depositRequestId = ObjectUtils.defaultIfNull(Long.parseLong(params.get("merchantTradeId")), 0L);
        return depositRequestMapper.get(depositRequestId);
        
    }
    
    
}
