package com.tripleonetech.sbe.payment.integration;

import java.util.Map;

import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;

import io.swagger.annotations.ApiModelProperty;

public class PaymentApiResult extends ApiResult {
    
    @ApiModelProperty(value="Load QR code image for paying")
    private String qrcodeImgUrl;
    
    @ApiModelProperty(value="Redirect to payment page")
    private String redirectUrl;
    
    @ApiModelProperty(value="Http Method of redirection")
    private String redirectHttpMethod;
    
    @ApiModelProperty(value="The parameters of redirection")
    private Map<String, Object> redirectParams;
    
    public PaymentApiResult(ApiResponseEnum responseEnum) {
        super(responseEnum);
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getQrcodeImgUrl() {
        return qrcodeImgUrl;
    }

    public void setQrcodeImgUrl(String qrcodeImgUrl) {
        this.qrcodeImgUrl = qrcodeImgUrl;
    }
    

    public String getRedirectHttpMethod() {
        return redirectHttpMethod;
    }

    public void setRedirectHttpMethod(String redirectHttpMethod) {
        this.redirectHttpMethod = redirectHttpMethod;
    }

    public Map<String, Object> getRedirectParams() {
        return redirectParams;
    }

    public void setRedirectParams(Map<String, Object> redirectParams) {
        this.redirectParams = redirectParams;
    }
    
    
    public static PaymentApiResult callbackOk(String returnMessage) {
        PaymentApiResult paymentApiResult = new PaymentApiResult(ApiResponseEnum.OK);
        paymentApiResult.setParam("callbackResponse", returnMessage);
        return paymentApiResult;
    }
        
    public static PaymentApiResult callbackFailure(String returnMessage) {
        PaymentApiResult paymentApiResult = new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        paymentApiResult.setParam("callbackResponse",returnMessage);
        return paymentApiResult;
    }
    
    public String getCallbackReturnMessage() {
        return String.valueOf(getParam("callbackResponse"));
    }
    
}
