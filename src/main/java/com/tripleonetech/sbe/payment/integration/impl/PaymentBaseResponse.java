package com.tripleonetech.sbe.payment.integration.impl;

import com.tripleonetech.sbe.payment.integration.PaymentApiResult;

public interface PaymentBaseResponse {
    PaymentApiResult toApiResult();
}
