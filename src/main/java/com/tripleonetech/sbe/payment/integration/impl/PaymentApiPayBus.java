package com.tripleonetech.sbe.payment.integration.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ClientHttpInfoUtils;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;
import com.tripleonetech.sbe.payment.integration.AbstractPaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.integration.impl.paybus.PayBusDepositInformationResponse;
import com.tripleonetech.sbe.payment.integration.impl.paybus.PayBusDepositRequisitionResponse;
import com.tripleonetech.sbe.payment.integration.impl.paybus.PayBusPlatformInformationResponse;
import com.tripleonetech.sbe.payment.service.DepositRequestService;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;

@Component
@Scope("prototype")
public class PaymentApiPayBus extends AbstractPaymentApi {
    
    private static final Logger logger = LoggerFactory.getLogger(PaymentApiPayBus.class);
    private static final int PAYBUS_DEPOSIT_STATUS_NEW = 1;
    private static final int PAYBUS_DEPOSIT_STATUS_UNSUCCESSFUL = 3;
    private static final int PAYBUS_DEPOSIT_STATUS_PROCESSING = 5;
    private static final int PAYBUS_DEPOSIT_STATUS_SECCESS = 7;
    private static final int PAYBUS_DEPOSIT_STATUS_FAILED = 9;
    
    private static ArrayList<ApiMetaField> META_FIELDS;

    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("merchantCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("signKey", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("requestUrl", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("notifyUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("platformId", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("channel", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("subChannel", MetaFieldTypeEnum.TEXT, true));

            }
        };
    }
    
    @Autowired
    private DepositRequestMapper depositRequestMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private DepositRequestService depositRequestService;
    
    @Override
    public PaymentApiResult submitRequest(DepositRequest depositRequest) {
        
        Map<String, Object> params = new HashMap<>();
        params.put("merchant_code", getConfig().getFields().get("merchantCode"));
        params.put("custom_no", String.valueOf(depositRequest.getId()));
        params.put("amount", depositRequest.getAmount().toPlainString());
        params.put("secure_str", String.valueOf(depositRequest.getId()));
        params.put("notify_url", getConfig().getFields().get("notifyUrl"));
        params.put("platform_id", getConfig().getFields().get("platformId"));
        params.put("channel", getConfig().getFields().get("channel"));
        if(null != getConfig().getFields().get("subChannel")) {
            params.put("sub_channel", getConfig().getFields().get("subChannel"));
        }
        
        Map<String, Object> playerInfo = new HashMap<>();
        Player player = playerMapper.get(depositRequest.getPlayerId());
        playerInfo.put("id", String.valueOf(player.getId()));
        playerInfo.put("name", player.getUsername());
        playerInfo.put("ip", ClientHttpInfoUtils.getIp());
        params.put("player", playerInfo);
        
        HttpResponse httpResponse = postAsJsonData(getConfig().getFields().get("requestUrl").concat("/pay/deposit"), signParams(params));
        PaymentApiResult paymentApiResult = handleResponse(httpResponse, PayBusDepositRequisitionResponse.class);        
        if(paymentApiResult.isSuccess()) {
            String paybusResponse = String.valueOf(paymentApiResult.getParam("response"));
            PayBusDepositRequisitionResponse paybusDepositRequisitionResponse = JsonUtils.jsonToObject(paybusResponse, PayBusDepositRequisitionResponse.class);
            depositRequest.setExternalUid(paybusDepositRequisitionResponse.getData().getOrderNo());
        }
        depositRequestMapper.updateExtraInformation(depositRequest);
        return paymentApiResult;
    }

    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.SimpleForm;
    }

    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }
    /**
     * PAYBUS_API will recall our callback API every 15, 15, 30, 180, 1800, 1800, 1800, 1800, 3600 seconds, 
     * if no response "OK" to PAYBUS_API. 
     */
    @Override
    public PaymentApiResult callback(Map<String, ? extends Object> param) {

        if (null == param) {
            logger.error("Callback Failed - ErrorCode: [BAD_REQUEST], Message: [ The parameters of callback is empty ]");
            return new PaymentApiResult(ApiResponseEnum.BAD_REQUEST);
        }

        if (!validateSign(new HashMap<String, Object>(param))) {
            //驗證signature失敗
            logger.error("Callback Failed - ErrorCode: [SIGNATURE_VERIFICATION_FAILED], Input parameters:[{}]", param.toString());
            return new PaymentApiResult(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED);
        }

        Long depositRequestId = Long.valueOf(String.valueOf(param.get("custom_no")));
        DepositRequest depositRequest = depositRequestMapper.get(depositRequestId);
        if (null == depositRequest) {
            //訂單不存在
            logger.error("Callback Failed - ErrorCode: [ACTION_UNSUCCESSFUL], Message:[Cannot find out deposit_request by using deposit_request_id:{}]",
                    depositRequestId);
            return new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        }
        if (!depositRequest.getStatus().equals(DepositStatusEnum.OPEN)) {
            //表示訂單已經關閉， 直接return OK 給PAYBUS_API
            logger.info("Deposit Request was Closed. Return OK to PAYBUS_API directly");
            return PaymentApiResult.callbackOk("OK");
        }

        Integer status = Integer.parseInt(String.valueOf(param.get("status")));
        switch (status) {
        case PAYBUS_DEPOSIT_STATUS_SECCESS:
            depositRequestService.processDepositRequest(depositRequest, DepositStatusEnum.APPROVED, 0);
            logger.info("Payment approved, deposit_request ID:[{}], Player's deposit amount:[{}], real amount:[{}], Paybus_API message : [{}]",
                    depositRequest.getId(), param.get("amount"), param.get("real_amount"),
                    depositRequest.getNote());
            break;
        case PAYBUS_DEPOSIT_STATUS_UNSUCCESSFUL:
            depositRequestService.processDepositRequest(depositRequest, DepositStatusEnum.REJECTED, 0);
            logger.info("Payment unsuccessful, Paybus_API message : [{}]", depositRequest.getNote());
            break;

        case PAYBUS_DEPOSIT_STATUS_FAILED:
            depositRequestService.processDepositRequest(depositRequest, DepositStatusEnum.REJECTED, 0);
            logger.info("Payment Failed, DepositRequest ID:[{}], Paybus_API message : [{}]", depositRequest.getId(),
                    depositRequest.getNote());
            break;
        default:
            logger.info("unprocessed status : [{}], DepositRequest ID:[{}]", status, depositRequest.getId());
            return PaymentApiResult.callbackFailure("FAILED");
        }

        return PaymentApiResult.callbackOk("OK");
    }

    public PaymentApiResult handleResponse(HttpResponse httpResponse, Class<? extends PaymentBaseResponse> clazz) {
        
        if(null == httpResponse) {
            logger.error("Failed to process empty HttpResponse");
            return new PaymentApiResult(ApiResponseEnum.ACTION_UNSUCCESSFUL);
        }
        
        PaymentApiResult paymentApiResult = null;
        try {
            String response = EntityUtils.toString(httpResponse.getEntity());
            logger.info("Paybus response : {}", response);
            
            int responseStatusCode = httpResponse.getStatusLine().getStatusCode();
            if (HttpStatus.SC_OK != responseStatusCode) {
                logger.error("Unexpected HTTP status: [{}], error message:[{}]", responseStatusCode, response);
                return new PaymentApiResult(ApiResponseEnum.SERVICE_UNAVAILABLE);
            }
            
            JsonNode apiResponseData = JsonUtils.jsonToJsonNode(response).get("data");
            if (null != apiResponseData && null != apiResponseData.get("sign")) {
                Map<String, Object> data = new HashMap<>(
                        JsonUtils.jsonToMap(apiResponseData.toString(), String.class, Object.class));
                if (!validateSign(data)) {
                    return new PaymentApiResult(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED);
                }
            }

            paymentApiResult = JsonUtils.jsonToObject(response, clazz, PropertyNamingStrategy.SNAKE_CASE).toApiResult();
            paymentApiResult.setParam("response", response);
        }catch(Exception e) {
            logger.error("Exception:{}", e);
            return new PaymentApiResult(ApiResponseEnum.GENERAL_ERROR);
        }
        return paymentApiResult;
    }
    
    /**
     * Validate "sign" from PAYBUS_API
     * @param params
     * @return true/false 
     */
    protected boolean validateSign(Map<String, Object> params) {
        Map<String, Object> newParams = new HashMap<>(params);
        signParams(newParams);
        logger.debug("Sign provided by PayBus :{} \n , Sign generated by T1SBE: {}", 
                params.get("sign"), newParams.get("sign"));
        if(newParams.get("sign").equals(params.get("sign"))) {
            return true;
        }
        return false;
    }
    protected Map<String, Object> signParams(Map<String, Object> params) {
        // remove parameter[sign]
        params.remove("sign");

        // The order should be sorted by parameter name in alphabetical order
        SortedMap<String, String> sortedMap = new TreeMap<String, String>();
        for (String k : params.keySet()) {
            if(null == params.get(k)) {
                continue;
            }
            if (params.get(k) instanceof Map) {
                continue;
            }
            if (params.get(k) instanceof List<?>) {
                continue;
            }
            if(params.get(k) instanceof JSONObject) {
                continue;
            }
            sortedMap.put(k, params.get(k).toString());
        }

        Collection<String> paramValues = sortedMap.values();
        String signData = StringUtils.join(paramValues, "");
        signData = StringUtils.join(signData, getConfig().getFields().get("signKey"));

        logger.info("Signing data [{}] with sign_key [{}]", signData, getConfig().getFields().get("signKey"));
        // use SHA1 to get signature
        String signature = DigestUtils.sha1Hex(signData);
        params.put("sign", signature);
        return params;
    }
    
    public PayBusPlatformInformationResponse queryPlatformInfomation() {
        String requestUrl = getConfig().getFields().get("requestUrl").concat("/pay/info/platform");
        Map<String, Object> params = new HashMap<>();
        params.put("merchant_code", getConfig().getFields().get("merchantCode"));
        Map<String,String> getParams = signParams(params).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> (String)e.getValue()));
        HttpResponse httpResponse = this.submitGet(requestUrl, getParams);
        PaymentApiResult paymentApiResult = handleResponse(httpResponse, PayBusPlatformInformationResponse.class);
        PayBusPlatformInformationResponse response = JsonUtils.jsonToObject(String.valueOf(paymentApiResult.getParam("response")), PayBusPlatformInformationResponse.class);
        return response;
    }
    
    /***
     * TODO: 尚未接Controller，提供後台查詢，只先定義出來
     */
    public PaymentApiResult queryDepositInformation(DepositRequest depositRequest) {
        String requestUrl = getConfig().getFields().get("requestUrl").concat("/pay/query");
        Map<String, Object> params = new HashMap<>();
        params.put("merchant_code", getConfig().getFields().get("merchantCode"));
        params.put("custom_no", String.valueOf(depositRequest.getId()));
        params.put("secure_str", String.valueOf(depositRequest.getId()));
        Map<String,String> getParams = signParams(params).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> (String)e.getValue()));
        HttpResponse httpResponse = this.submitGet(requestUrl, getParams);
        PaymentApiResult paymentApiResult = handleResponse(httpResponse, PayBusDepositInformationResponse.class);
        return paymentApiResult;
    }
    
}
