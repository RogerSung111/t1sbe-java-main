package com.tripleonetech.sbe.payment.integration.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("prototype")
public class PaymentApiOnePayEwalletQrPay extends AbstractPaymentApiOnePay {
    
    private final static Logger logger = LoggerFactory.getLogger(PaymentApiOnePayEwalletQrPay.class);
    private static List<ApiMetaField> META_FIELDS;
    private static final String PAYMENT_CHANNEL_UNIONPAY = "UNIONPAY";
    private static final String FLAG_SUCCESS = "SUCCESS";
    private static final String FLAG_FAILED = "FAILED";
    
    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("merchantId", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("privateKey", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("publicKey", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("notifyUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("returnUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("requestUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("formType", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("formExtraInfo", MetaFieldTypeEnum.TEXT, false));
            }
        };
    }
    
    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }
    
    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.QrForm;
    }
    
    @Override
    public Map<String, String> getBodyParams(DepositRequest depositRequest) {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("app_id", getConfig("merchantId"));
        requestBody.put("currency", getConfig().getCurrency().name());
        requestBody.put("amount", String.valueOf(depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP)));
        requestBody.put("order_no", String.valueOf(depositRequest.getId()));        
        requestBody.put("payment_channel", depositRequest.getPaymentChannel());
        return requestBody;
    }
    
    /**
     * Call onepay's qrcodePay api,  api reutrn qrurl, imgurl and sign.
     */
    @Override
    public PaymentApiResult submitRequest(DepositRequest depositRequest) {
        PaymentApiResult apiResult = new PaymentApiResult(ApiResponseEnum.OK);
        Map<String, Object> params = new HashMap<>(getBodyParams(depositRequest));
        
        // call Onepay's API
        String response = "";
        try {    
            HttpResponse httpResponse = postAsJsonData(getConfig().get("requestUrl"), params);
            HttpEntity httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);
            logger.info("Onepay's response : [{}]", response);
        }  catch (IOException e) {
            logger.error("IOException : {}" + e);
            apiResult.setResponseEnum(ApiResponseEnum.BAD_REQUEST);
            return apiResult;
        }

        // process response
        JsonNode responseJson = JsonUtils.jsonToJsonNode(response);
        String flag = responseJson.get("flag").asText();
        String data = responseJson.get("data").toString();
        if (FLAG_SUCCESS.equals(flag) && StringUtils.isNotBlank(data)) {
            Map<String, String> dataMap = JsonUtils.jsonToMap(data);
            // verify signature
            if (verifySignature(dataMap)) {
                apiResult.setParam("qrUrl", dataMap.get("qrUrl"));
                apiResult.setParam("imgUrl", dataMap.get("imgUrl"));
            } else {
                apiResult.setResponseEnum(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED);
            }
        } else {
            logger.error("Failed to process Onepay's response. flag:[{}], data:[{}]", flag, data);
            apiResult.setResponseEnum(ApiResponseEnum.MALFORMED_REPLY);
        }
        return apiResult;
    }

    @Override
    DepositRequest findDepositRequest(Map<String, String> params) {
        Long depositRequestId = ObjectUtils.defaultIfNull(Long.parseLong(params.get("order_no")), 0L);
        return depositRequestMapper.get(depositRequestId);
    }

    
}
