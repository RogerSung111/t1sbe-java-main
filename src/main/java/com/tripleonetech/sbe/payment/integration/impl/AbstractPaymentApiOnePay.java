package com.tripleonetech.sbe.payment.integration.impl;

import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.common.RSACrypto;
import com.tripleonetech.sbe.common.Utils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.integration.AbstractPaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;


public abstract class AbstractPaymentApiOnePay extends AbstractPaymentApi {
     
    final static String SIGNATURE_ALGORITHM = "SHA1WithRSA";
    final static String PS_PAYMENT_SUCCESS = "PS_PAYMENT_SUCCESS";
    final static String PS_PAYMENT_FAIL = "PS_PAYMENT_FAIL";
    final static String CALLBACK_RETURN_MESSAGE_SUCCESS = "success";
    final static String CALL_BACK_RETURN_NESSAGE_FAIL = "fail";
    private final static Logger logger = LoggerFactory.getLogger(AbstractPaymentApiOnePay.class);

    @Autowired
    DepositRequestMapper depositRequestMapper;
    
    abstract Map<String, String> getBodyParams(DepositRequest depositRequest);
    abstract DepositRequest findDepositRequest(Map<String, String> params);
    
    public PaymentApiResult callback(Map<String, ? extends Object> param) {
        
        Map<String, String> verifyParams = param.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> (String)e.getValue()));
        
        DepositRequest depositRequest = findDepositRequest(verifyParams);
        if(null != depositRequest && 
                depositRequest.getStatus().equals(DepositStatusEnum.OPEN)) {
            // Verify signature
            if (!verifySignature(verifyParams)) {
                return new PaymentApiResult(ApiResponseEnum.SIGNATURE_VERIFICATION_FAILED);
            }
            // Update deposit_request
            String tradeStatus = ObjectUtils.defaultIfNull(verifyParams.get("tradeStatus"), "");
            switch(tradeStatus) {
                case PS_PAYMENT_SUCCESS:
                    depositRequestMapper.updateStatus(depositRequest.getId(), DepositStatusEnum.APPROVED);
                    break;   
                case PS_PAYMENT_FAIL:
                    depositRequestMapper.updateStatus(depositRequest.getId(), DepositStatusEnum.REJECTED);

            }
        }
        return PaymentApiResult.callbackOk("OK");
    }
    
    boolean verifySignature(Map<String, String> param) {
        //convert sign from hex to Decimal
        String sign = ObjectUtils.defaultIfNull(param.get("sign"), "");
        byte[] decodedSign = decodeSign(sign);
        
        //concat data
        param.remove("sign");
        param.remove("signType");
        Map<String, String> sortedParams = Utils.sortMapByKey(param);
        String data = Utils.concatParametersToString(sortedParams);
        logger.info("Raw data of signature: [{}]", data);
        
        //verify signature
        boolean result =  RSACrypto.verify(data.getBytes(), decodedSign, getConfig("publicKey"), SIGNATURE_ALGORITHM);
        if(!result) {
            logger.error("Signature validated fail, data : [{}] , sign:[{}]", param.toString(), sign);
        }
        
        return result;
    }
    
    byte[] decodeSign(String sign) {
        return Base64.decodeBase64(convertHexToString(sign));
    }
    
    String encodeSign(Map<String, String> params) {        
        //1. Sort
        Map<String, String> sortedParams = Utils.sortMapByKey(params);
        String sign = Utils.concatParametersToString(sortedParams);
        //2. RSA
        byte[] cryptoSign = RSACrypto.sign(sign.getBytes(), getConfig("privateKey"), SIGNATURE_ALGORITHM);
        return Base64.encodeBase64String(cryptoSign);
    }
    
    public String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        for( int i=0; i<hex.length()-1; i+=2 ){
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);  
        }
        logger.info("HexToDecimal :[{}]", sb.toString());
        return sb.toString();
    }
    
   
}
