package com.tripleonetech.sbe.payment.integration.impl.paybus;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.integration.impl.PaymentBaseResponse;

public class PayBusDepositRequisitionResponse extends PayBusResponse implements PaymentBaseResponse {
    
    private Data data;
    
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
    
        @JsonProperty("merchant_code")
        private String merchantCode;
        
        @JsonProperty("custom_no")
        private String customNo;
    
        @JsonProperty("pay_url")
        private String payUrl;
    
        @JsonProperty("form")
        private DepositForm form;
    
        @JsonProperty("amount")
        private BigDecimal amount;
    
        @JsonProperty("real_amount")
        private BigDecimal realAmount;
    
        @JsonProperty("status")
        private Integer status;
    
        @JsonProperty("order_no")
        private String orderNo;
    
        @JsonProperty("sign")
        private String sign;
    
        public String getMerchantCode() {
            return merchantCode;
        }
    
        public void setMerchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
        }
    
        public String getCustomNo() {
            return customNo;
        }
    
        public void setCustomNo(String customNo) {
            this.customNo = customNo;
        }
    
        public String getPayUrl() {
            return payUrl;
        }
    
        public void setPayUrl(String payUrl) {
            this.payUrl = payUrl;
        }
    
        public DepositForm getForm() {
            return form;
        }
    
        public void setForm(DepositForm form) {
            this.form = form;
        }
    
        public BigDecimal getAmount() {
            return amount;
        }
    
        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    
        public BigDecimal getRealAmount() {
            return realAmount;
        }
    
        public void setRealAmount(BigDecimal realAmount) {
            this.realAmount = realAmount;
        }
    
        public Integer getStatus() {
            return status;
        }
    
        public void setStatus(Integer status) {
            this.status = status;
        }
    
        public String getOrderNo() {
            return orderNo;
        }
    
        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }
    
        public String getSign() {
            return sign;
        }
    
        public void setSign(String sign) {
            this.sign = sign;
      
        }
    
        public static class DepositForm {
            
            @JsonProperty("pay_memberid")
            private String payMemberId;
            
            @JsonProperty("pay_orderid")
            private String payOrderId;
    
            @JsonProperty("pay_amount")
            private BigDecimal payAmount;
    
            @JsonProperty("pay_applydate")
            private String payApplyDate;
    
            @JsonProperty("pay_channelCode")
            private String payChannelCode;
    
            @JsonProperty("pay_userid")
            private String payUserId;
    
            @JsonProperty("pay_notifyurl")
            private String payNotifyUrl;
    
            @JsonProperty("pay_md5sign")
            private String payMd5Sign;
    
            public String getPayMemberId() {
                return payMemberId;
            }
    
            public void setPayMemberId(String payMemberId) {
                this.payMemberId = payMemberId;
            }
    
            public String getPayOrderId() {
                return payOrderId;
            }
    
            public void setPayOrderId(String payOrderId) {
                this.payOrderId = payOrderId;
            }
    
            public BigDecimal getPayAmount() {
                return payAmount;
            }
    
            public void setPayAmount(BigDecimal payAmount) {
                this.payAmount = payAmount;
            }
    
            public String getPayApplyDate() {
                return payApplyDate;
            }
    
            public void setPayApplyDate(String payApplyDate) {
                this.payApplyDate = payApplyDate;
            }
    
            public String getPayChannelCode() {
                return payChannelCode;
            }
    
            public void setPayChannelCode(String payChannelCode) {
                this.payChannelCode = payChannelCode;
            }
    
            public String getPayUserId() {
                return payUserId;
            }
    
            public void setPayUserId(String payUserId) {
                this.payUserId = payUserId;
            }
    
            public String getPayNotifyUrl() {
                return payNotifyUrl;
            }
    
            public void setPayNotifyUrl(String payNotifyUrl) {
                this.payNotifyUrl = payNotifyUrl;
            }
    
            public String getPayMd5Sign() {
                return payMd5Sign;
            }
    
            public void setPayMd5Sign(String payMd5Sign) {
                this.payMd5Sign = payMd5Sign;
            }

        }
    }

    @Override
    public PaymentApiResult toApiResult() {
        PaymentApiResult paymentApiResult = new PaymentApiResult(getResponse());
        paymentApiResult.setRedirectUrl(this.getData().getPayUrl());
        paymentApiResult.setRedirectParams(JsonUtils.objectToMap(this.getData().getForm()));
        paymentApiResult.setRedirectHttpMethod(this.getData().getForm()==null? "get":"post");
        return paymentApiResult;
    }
}
