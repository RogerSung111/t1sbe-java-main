package com.tripleonetech.sbe.payment.integration.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PaymentApiPaySecWeChat extends AbstractPaymentApiPaySec{
    
    private final static String PAYMENT_TYPE = "QRCODE";
    
    private static List<ApiMetaField> META_FIELDS;
    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("merchantCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("key", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("version", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("channelCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("notifyUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("returnUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("requestTokenUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("sendTokenFormUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("sendTokenJsonUrl", MetaFieldTypeEnum.URL, true));
            }
        };
    }
    
    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }
    
    @Override
    public String getPaymentType() {
        return PAYMENT_TYPE;
    }
    
    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.SimpleForm;
    }
}
