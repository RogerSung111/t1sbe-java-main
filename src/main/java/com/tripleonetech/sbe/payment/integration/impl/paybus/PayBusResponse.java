package com.tripleonetech.sbe.payment.integration.impl.paybus;

import com.tripleonetech.sbe.common.integration.ApiResponseEnum;

public abstract class PayBusResponse  {
    
    private final static int SUCCESS = 1;
    private final static int FAILURE = 0;
    
    private Boolean success;
    
    private String message;
    
    private Integer code;
        
    public Boolean getSuccess() {
        return success;
    }
    public void setSuccess(Boolean success) {
        this.success = success;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Integer getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }

    public ApiResponseEnum getResponse() {
        ApiResponseEnum responseEnum = null;
        switch(getCode()) {
        case SUCCESS:
            responseEnum = ApiResponseEnum.OK;
            break;
        case FAILURE:
            responseEnum = ApiResponseEnum.ACTION_UNSUCCESSFUL;
        }
        return responseEnum;
    }
}
