package com.tripleonetech.sbe.payment.integration.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PaymentApiPaySecQuickpay extends AbstractPaymentApiPaySec {

    private static final Logger logger = LoggerFactory.getLogger(PaymentApiPaySecQuickpay.class);
    private final static String PAYMENT_TYPE = "BANKPAGE";

    private static List<ApiMetaField> META_FIELDS;
    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("bankCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("merchantCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("key", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("version", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("channelCode", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("notifyUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("returnUrl", MetaFieldTypeEnum.URL, false));
                add(new ApiMetaField("requestTokenUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("sendTokenFormUrl", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("sendTokenJsonUrl", MetaFieldTypeEnum.URL, true));
            }
        };
    }
    
    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.SimpleForm;
    }
    
    @Override
    public Map<String, Object> getBodyParams(DepositRequest depositRequest) {
        Long cartId = depositRequest.getId();
        String orderAmount = depositRequest.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        Map<String, Object> body = new HashMap<>();
        body.put("channelCode", getConfig().get("channelCode"));
        body.put("notifyURL", getConfig().get("notifyURL"));
        body.put("returnURL", getConfig().get("returnURL"));
        body.put("orderAmount", orderAmount);
        body.put("orderTime", String.valueOf(depositRequest.getRequestedDate()));
        body.put("cartId", cartId);
        body.put("currency", getConfig().getCurrency().name());
        body.put("bankCode", getConfig().get("bankCode"));
        logger.info("Generated params of Body for QUICKPAY : [{}] ", body.toString());
        return body;
    }

    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    @Override
    public String getPaymentType() {
        return PAYMENT_TYPE;
    }

}
