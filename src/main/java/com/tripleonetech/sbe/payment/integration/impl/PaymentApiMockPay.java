package com.tripleonetech.sbe.payment.integration.impl;

import com.tripleonetech.sbe.common.integration.ApiMetaField;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.MetaFieldTypeEnum;
import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositRequestMapper;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.payment.PaymentFormTypeEnum;
import com.tripleonetech.sbe.payment.integration.AbstractPaymentApi;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.service.DepositRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A mock implementation of payment API.
 * 
 */
@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PaymentApiMockPay extends AbstractPaymentApi {
    // private static final Logger logger = LoggerFactory.getLogger(PaymentApiMockPay.class);
    private static List<ApiMetaField> META_FIELDS;

    @Autowired
    DepositRequestMapper depositRequestMapper;

    @Autowired
    private DepositRequestService depositRequestService;

    static {
        META_FIELDS = new ArrayList<ApiMetaField>() {
            {
                add(new ApiMetaField("url", MetaFieldTypeEnum.URL, true));
                add(new ApiMetaField("merchantId", MetaFieldTypeEnum.TEXT, true));
                add(new ApiMetaField("key", MetaFieldTypeEnum.TEXT, true));
            }
        };
    }
    
    @Override
    public PaymentFormTypeEnum getFormType() {
        return PaymentFormTypeEnum.SimpleForm;
    }
    
    @Override
    public List<ApiMetaField> getMetaFields() {
        return META_FIELDS;
    }

    @Override
    // Mock implementation of a deposit thru this API
    public PaymentApiResult submitRequest(DepositRequest depositRequest) {
        PaymentApiResult apiResult;

        if (null != depositRequest.getId()) {
            apiResult = new PaymentApiResult(ApiResponseEnum.OK);
            // mock information which are used for front-end to redirect
            apiResult.setParam("depositRequestId", depositRequest.getId());
            apiResult.setParam("amount", depositRequest.getAmount());
            apiResult.setParam("merchId", getConfig().get("merchantId"));
            apiResult.setParam("url", getConfig().get("url"));
            apiResult.setParam("key", getConfig().get("key"));

            // insert deposit_request failed
        } else
            apiResult = new PaymentApiResult(ApiResponseEnum.BAD_REQUEST);
        return apiResult;
    }

    public String generateApiUrl(Map<String, Object> param) {
        String apiUrl = "";
        if (null != param) {
            apiUrl = param.get("url").toString() + "?";
            param.remove("url");

            Iterator<Entry<String, Object>> iterator = param.entrySet().iterator();
            while (iterator.hasNext()) {
                apiUrl += iterator.next().getKey() + "=" + iterator.next().getValue().toString();
                if (iterator.hasNext())
                    apiUrl += "&";
            }
        }
        return apiUrl;
    }

    @Override
    public PaymentApiResult callback(Map<String, ? extends Object> param) {
        int orderId = (Integer) param.get("orderId");
        DepositStatusEnum status = (DepositStatusEnum) param.get("status");
        depositRequestMapper.updateStatus(orderId, status);
        return PaymentApiResult.callbackOk("OK");
    }

    @Override
    public boolean isOnline() {
        return true;
    }

}
