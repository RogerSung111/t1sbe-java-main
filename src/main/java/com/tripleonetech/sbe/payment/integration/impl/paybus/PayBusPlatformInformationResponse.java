package com.tripleonetech.sbe.payment.integration.impl.paybus;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.payment.integration.PaymentApiResult;
import com.tripleonetech.sbe.payment.integration.impl.PaymentBaseResponse;

public class PayBusPlatformInformationResponse implements PaymentBaseResponse {

    private Integer total;
    private List<Item> items;
    
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public List<Item> getItems() {
        return items;
    }
    public void setItems(List<Item> items) {
        this.items = items;
    }
    public static class Item {
        private Integer id;
        
        private String platform;
        
        @JsonProperty("merchant_code")
        private String merchantCode;
        
        @JsonProperty("deposit_channels")
        private List<DepositChannel> depositChannels;
        
        @JsonProperty("withdraw_channels")
        private List<WithdrawChannel> withdrawChannels;
        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getPlatform() {
            return platform;
        }
        public void setPlatform(String platform) {
            this.platform = platform;
        }
        public String getMerchantCode() {
            return merchantCode;
        }
        public void setMerchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
        }
        public List<DepositChannel> getDepositChannels() {
            return depositChannels;
        }
        public void setDepositChannels(List<DepositChannel> depositChannels) {
            this.depositChannels = depositChannels;
        }
        public List<WithdrawChannel> getWithdrawChannels() {
            return withdrawChannels;
        }
        public void setWithdrawChannels(List<WithdrawChannel> withdrawChannels) {
            this.withdrawChannels = withdrawChannels;
        }
        
        public static class DepositChannel {
            private String name;
            private BigDecimal max;
            private BigDecimal min;
            private List<String> sub;
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
            public BigDecimal getMax() {
                return max;
            }
            public void setMax(BigDecimal max) {
                this.max = max;
            }
            public BigDecimal getMin() {
                return min;
            }
            public void setMin(BigDecimal min) {
                this.min = min;
            }
            
            public List<String> getSub() {
                return sub;
            }
            public void setSub(List<String> sub) {
                this.sub = sub;
            }
        }
        
        public static class WithdrawChannel {
            private String name;
            private BigDecimal max;
            private BigDecimal min;
            private List<String> sub;
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }
            public BigDecimal getMax() {
                return max;
            }
            public void setMax(BigDecimal max) {
                this.max = max;
            }
            public BigDecimal getMin() {
                return min;
            }
            public void setMin(BigDecimal min) {
                this.min = min;
            }
            
            public List<String> getSub() {
                return sub;
            }
            public void setSub(List<String> sub) {
                this.sub = sub;
            }      
        }
    }
    
    

    @Override
    public PaymentApiResult toApiResult() {
        PaymentApiResult paymentApiResult = new PaymentApiResult(ApiResponseEnum.OK);
        return paymentApiResult;
    }
    
}
