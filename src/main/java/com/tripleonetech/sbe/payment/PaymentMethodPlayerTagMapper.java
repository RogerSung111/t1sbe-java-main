package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.web.IdNameView;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PaymentMethodPlayerTagMapper {

    List<IdNameView> getPlayerTagInfoByPaymentMethod(int paymentMethodId);

    void insert(List<PaymentMethodPlayerTag> paymentAccountPlayerTag);

    void deleteByPaymentMethodId(int paymentMethodId);
}
