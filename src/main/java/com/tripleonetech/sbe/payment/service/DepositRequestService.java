package com.tripleonetech.sbe.payment.service;

import com.tripleonetech.sbe.common.WalletTransactionTypeEnum;
import com.tripleonetech.sbe.operator.Operator;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.PaymentRequestForm.OperatorPaymentRequestForm;
import com.tripleonetech.sbe.payment.history.DepositRequestProcessHistory;
import com.tripleonetech.sbe.payment.history.DepositRequestProcessHistoryMapper;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.wallet.WalletService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class DepositRequestService {
    private final static Logger logger = LoggerFactory.getLogger(DepositRequestService.class);
    private static final int PAYMENT_TERM_HOURS = 3;

    @Autowired
    private DepositRequestMapper depositRequestMapper;

    @Autowired
    private PaymentMethodMapper paymentMethodMapper;

    @Autowired
    private WalletService walletService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private DepositRequestProcessHistoryMapper depositRequestProcessHistoryMapper;

    private DepositRequest newDepositRequest(PaymentRequestForm paymentRequestForm, int playerId){
        PaymentMethod paymentMethod = paymentMethodMapper.get(paymentRequestForm.getPaymentMethodId());

        DepositRequest depositRequest = new DepositRequest();
        depositRequest.setPlayerId(playerId);
        depositRequest.setPaymentMethodId(paymentRequestForm.getPaymentMethodId());
        depositRequest.setAmount(paymentRequestForm.getAmount());
        depositRequest.setStatus(DepositStatusEnum.OPEN);
        BigDecimal chargeRatio = paymentMethod.getChargeRatio().divide(new BigDecimal(100));
        BigDecimal paymentCharge = paymentRequestForm.getAmount().multiply(chargeRatio).add(paymentMethod.getFixedCharge());
        depositRequest.setPaymentCharge(paymentCharge);
        depositRequest.setExpirationDate(LocalDateTime.now().plusHours(PAYMENT_TERM_HOURS));

        if (StringUtils.isNotBlank(paymentMethod.getNote())) {
            depositRequest.setNote(paymentMethod.getNote());
        }

        return depositRequest;
    }

    @Transactional
    public DepositRequest createDepositRequest(OperatorPaymentRequestForm paymentRequestForm, Operator operator) {
        int operatorId = operator.getId();
        DepositRequest depositRequest = newDepositRequest(paymentRequestForm, paymentRequestForm.getPlayerId());
        depositRequestMapper.insert(depositRequest);
        DepositRequestProcessHistory requestHistory = DepositRequestProcessHistory.createByOperator(
                DepositStatusEnum.OPEN, DepositStatusEnum.OPEN, depositRequest.getId(), null, operatorId, operatorId);
        depositRequestProcessHistoryMapper.insert(requestHistory);

        return depositRequest;
    }

    @Transactional
    public DepositRequest createDepositRequest(PaymentRequestForm paymentRequestForm, Player player) {
        DepositRequest depositRequest = newDepositRequest(paymentRequestForm, player.getId());
        depositRequestMapper.insert(depositRequest);

        DepositRequestProcessHistory requestHistory = DepositRequestProcessHistory.createByPlayer(
                DepositStatusEnum.OPEN, DepositStatusEnum.OPEN, depositRequest.getId(), null);
        depositRequestProcessHistoryMapper.insert(requestHistory);

//        return depositRequestMapper.get(depositRequest.getId());
        return depositRequest;
    }

    @Transactional
    public void processDepositRequest(DepositRequest depositRequest, DepositStatusEnum status, Integer operatorId) {
        if (DepositStatusEnum.OPEN.equals(depositRequest.getStatus())) {
            switch (status) {
                case APPROVED:
                    approveDepositRequest(depositRequest, operatorId);
                    break;
                case REJECTED:
                    rejectDepositRequest(depositRequest);
                    break;
            }

            DepositRequestProcessHistory requestHistory = DepositRequestProcessHistory.createByOperator(
                    depositRequest.getStatus(), status, depositRequest.getId(), null, operatorId, operatorId);
            depositRequestProcessHistoryMapper.insert(requestHistory);
        }
    }

    @Transactional
    public void approveDepositRequest(DepositRequest depositRequest, Integer operatorId) {
        logger.info("Ready to setup status as approve, deposit_request ID: {}", depositRequest.getId());
        //Add deposit_amount
        walletService.depositToMainWallet(depositRequest.getPlayerId(), depositRequest.getAmount());
        depositRequestMapper.updateStatus(depositRequest.getId(), DepositStatusEnum.APPROVED);
        //withdraw payment_charge from main_wallet if player bears payment charge
        PaymentMethod paymentMethod = paymentMethodMapper.get(depositRequest.getPaymentMethodId());
        if (paymentMethod.isPlayerBearPaymentCharge()) {
            walletService.withdrawFromMainWallet(depositRequest.getPlayerId(), depositRequest.getPaymentCharge(),
                    WalletTransactionTypeEnum.PAYMENT_CHARGE, operatorId);
        }
        applicationEventPublisher.publishEvent(new ApprovedDepositEvent(depositRequest));
    }

    public void rejectDepositRequest(DepositRequest depositRequest) {
        logger.info("Ready to setup status as rejected, deposit_request ID: {}", depositRequest.getId());
        depositRequestMapper.updateStatus(depositRequest.getId(), DepositStatusEnum.REJECTED);
    }

}
