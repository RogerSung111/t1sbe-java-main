package com.tripleonetech.sbe.payment.service;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.TagFilteredUtils;
import com.tripleonetech.sbe.payment.*;
import com.tripleonetech.sbe.payment.integration.PaymentApi;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaymentMethodService {
    private static Logger logger = LoggerFactory.getLogger(PaymentMethodService.class);

    @Autowired
    private PaymentMethodMapper paymentMethodMapper;
    @Autowired
    private PaymentMethodPlayerTagMapper paymentPlayerTagMapper;
    @Autowired
    private PaymentMethodPlayerMapper paymentPlayerMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PaymentApiFactory paymentApiFactory;

    // Two-step query to avoid issues in N-1 mapping
    public List<PaymentMethodDetailView> query(PaymentMethodQueryForm queryForm) {
        if(queryForm.getPlayerIds().size() > 0) {
            // convert player credential IDs to player IDs
            if(queryForm.getCurrency()  == null) {
                queryForm.setPlayerIds(
                        playerMapper.getPlayerIdsByCredentialIds(queryForm.getPlayerIds())
                );
            } else {
                queryForm.setPlayerIds(
                        playerMapper.getPlayerIdsByCredentialIds(queryForm.getPlayerIds(), queryForm.getCurrency())
                );
            }
        }
        List<Integer> ids = paymentMethodMapper.query(queryForm);
        if(ids.size() == 0){ // No result
            return new ArrayList<>();
        }
        List<PaymentMethodDetailView> paymentMethods = paymentMethodMapper.getById(new HashSet<>(ids), queryForm);
        return paymentMethods;
    }

    // Returns all available payment methods for given player credential id and currency
    // * Payment method is available if the given player has the matching Tag AND Group specified in it
    // * If Tag/Group is not present in a payment method, means it applies to all Tag/Group.
    // * If payment method has player ids, its tag/group ids are ignored, it's available if the given player id is specified
    // * If payment method has no tags, groups, or player ids, it's applicable to all players
    // Note that this method does not consider the daily limit yet
    public List<PaymentMethodDetailView> queryAvailableFor(int playerCredentialId, SiteCurrencyEnum currency) {
        Set<Integer> availableIds = queryAvailableIds(playerCredentialId, currency);
        PaymentMethodQueryForm queryForm = new PaymentMethodQueryForm();
        queryForm.setSort("priority DESC");
        return paymentMethodMapper.getById(availableIds, queryForm);
    }

    // Returns all available payment methods for given player, considering the daily limits of those payment methods
    // Important node: We omitted calculating 'Daily Remaining amount' for payment methods due to efficiency reason.
    // This could cause that the total amount accepted by this payment method exceeding the max allowed amount for a bit.
    public List<PaymentMethodView> queryAvailableForPayment(int playerCredentialId, SiteCurrencyEnum currency) {
        Set<Integer> ids = queryAvailableIds(playerCredentialId, currency);
        if(ids.size() == 0) {
            return new ArrayList<>();
        }

        // Query the corresponding deposit amounts for these IDs
        List<PaymentMethod.PaymentMethodIdAndAmount> idsAndAmounts = paymentMethodMapper.getIdWithAmounts(ids, DateUtils.getStartOfDay());
        Map<Integer, BigDecimal> idAmountMap = idsAndAmounts.stream().collect(Collectors.toMap(
                PaymentMethod.PaymentMethodIdAndAmount::getId,
                PaymentMethod.PaymentMethodIdAndAmount::getAmount
        ));

        PaymentMethodQueryForm queryForm = new PaymentMethodQueryForm();
        queryForm.setSort("priority DESC");
        List<PaymentMethodDetailView> paymentMethodDetails = paymentMethodMapper.getById(ids, queryForm);
        List<PaymentMethodView> paymentMethods = new ArrayList<>();

        paymentMethodDetails.stream().forEach(paymentMethodDetail -> {
            PaymentMethodView paymentMethod = PaymentMethodView.from(paymentMethodDetail);

            BigDecimal paymentMethodDepositAmount = idAmountMap.get(paymentMethodDetail.getId());
            BigDecimal paymentMethodBalanceAmount = null;

            // Decide whether the current deposit amount already exceed daily max
            // Has daily max configured
            if(paymentMethodDetail.getDailyMaxDeposit() != null
                && paymentMethodDetail.getDailyMaxDeposit().compareTo(BigDecimal.ZERO) > 0) {
                // Has deposit amount already
                if(paymentMethodDepositAmount != null) {
                    if (paymentMethodDetail.getDailyMaxDeposit().compareTo(paymentMethodDepositAmount) <= 0) {
                        // This payment method is already full
                        logger.debug("Payment method [{}] already reached [{}] for the day, exceeding max [{}]",
                                paymentMethodDetail.getId(), paymentMethodDepositAmount, paymentMethodDetail.getDailyMaxDeposit());
                        return;
                    } else {
                        // Calculate available max deposit amount
                        paymentMethodBalanceAmount = paymentMethodDetail.getDailyMaxDeposit().subtract(paymentMethodDepositAmount);
                        logger.debug("Payment method [{}] has balance capacity of [{}] left",
                                paymentMethodDetail.getId(), paymentMethodBalanceAmount);
                    }
                }
            }

            // Decide current transaction min and max
            BigDecimal minDeposit = Optional.of(paymentMethodDetail.getMinDepositPerTrans()).orElse(BigDecimal.ZERO);
            BigDecimal maxDeposit = Optional.of(paymentMethodDetail.getMaxDepositPerTrans()).orElse(BigDecimal.ZERO);
            if(paymentMethodBalanceAmount != null && minDeposit.compareTo(paymentMethodBalanceAmount) > 0) {
                // balance amount not able to match min deposit, also treat this payment method as full
                return;
            }
            if(paymentMethodBalanceAmount != null && maxDeposit.compareTo(paymentMethodBalanceAmount) > 0) {
                // balance amount balance falls below single max per trans, use the remaining balance as max per trans
                maxDeposit = paymentMethodBalanceAmount;
            }
            paymentMethod.setMinDeposit(minDeposit);
            paymentMethod.setMaxDeposit(maxDeposit);

            // assign form types
            if(paymentMethod.getPaymentApi() != null) { // 3rd party API payment
                PaymentApi api = paymentApiFactory.getById(paymentMethodDetail.getPaymentApi().getId());
                paymentMethod.setFormType(api.getFormType().getCode());
            }else { // manual payment form
                paymentMethod.setFormType(PaymentFormTypeEnum.ManualAtmForm.getCode());
            }
            paymentMethods.add(paymentMethod);
        });

        return paymentMethods;
    }

    private Set<Integer> queryAvailableIds(int playerCredentialId, SiteCurrencyEnum currency) {
        Player player = playerMapper.listByCredentialId(playerCredentialId, currency).get(0);

        List<PaymentMethodTagFiltered> paymentMethodIds = paymentMethodMapper.queryAvailable(currency);
        logger.debug("Found [{}] payment methods of currency [{}]", paymentMethodIds.size(), currency);

        Map<Integer, PaymentMethodTagFiltered> paymentMethodIdsMap =
                paymentMethodIds.stream().collect(Collectors.toMap(PaymentMethodTagFiltered::getId, e -> e));
        Set<Integer> availableIdsByTagFilter = TagFilteredUtils.filterAvailableIds(paymentMethodIdsMap, player.getTagIds(), player.getGroupId());
        logger.debug("After filtering by tags, left [{}] payment methods", availableIdsByTagFilter.size());

        List<Integer> availableIdsByPlayer = paymentMethodMapper.queryAvailableByPlayerIds(playerCredentialId, currency);
        logger.debug("Found [{}] payment methods for player [{}][{}]", availableIdsByPlayer.size(),
                player.getUsername(), player.getCurrency());

        Set<Integer> availableIds = new HashSet<Integer>(){{
            addAll(availableIdsByTagFilter);
            addAll(availableIdsByPlayer);
        }};

        logger.debug("Total unique payment methods found: [{}]", availableIds);
        return availableIds;
    }

    public PaymentMethodDetailView getDetail(int id) {
        return paymentMethodMapper.getById(id);
    }

    public PaymentMethod get(int id) {
        return paymentMethodMapper.get(id);
    }

    public Integer create(PaymentMethodForm paymentMethodForm) {
        PaymentMethod paymentMethod = PaymentMethod.from(paymentMethodForm);
        if(paymentMethodForm.getPlayerIds().size() > 0) {
            // convert player credential IDs to player IDs
            paymentMethod.setPlayerIds(
                    playerMapper.getPlayerIdsByCredentialIds(
                            paymentMethodForm.getPlayerIds(),
                            paymentMethodForm.getCurrency()));
        }
        paymentMethodMapper.insert(paymentMethod);
        insertConstraints(paymentMethod);
        return paymentMethod.getId();
    }

    public void update(int id, PaymentMethodForm paymentMethodForm) {
        PaymentMethod paymentMethod = PaymentMethod.from(paymentMethodForm);
        paymentMethod.setId(id);
        if(paymentMethodForm.getPlayerIds().size() > 0) {
            // convert player credential IDs to player IDs
            paymentMethod.setPlayerIds(
                    playerMapper.getPlayerIdsByCredentialIds(
                            paymentMethodForm.getPlayerIds(),
                            paymentMethodForm.getCurrency()));
        }
        paymentPlayerTagMapper.deleteByPaymentMethodId(paymentMethod.getId());
        paymentPlayerMapper.deleteByPaymentMethodId(paymentMethod.getId());
        paymentMethodMapper.update(paymentMethod);
        insertConstraints(paymentMethod);
    }

    public void updateEnabled(int paymentMethodId, boolean enabled) {
        paymentMethodMapper.updateEnabled(paymentMethodId, enabled);
    }

    public void deleteById(int paymentMethodId) {
        paymentMethodMapper.setDelete(paymentMethodId);
    }

    private void insertConstraints(PaymentMethod paymentMethod) {
        List<Integer> playerTagIds = paymentMethod.getPlayerTagIds();
        if (!CollectionUtils.isEmpty(playerTagIds)) {
            List<PaymentMethodPlayerTag> paymentMethodPlayerTags = new ArrayList<>();
            for (Integer playerTagId : playerTagIds) {
                PaymentMethodPlayerTag paymentMethodPlayerTag = new PaymentMethodPlayerTag();
                paymentMethodPlayerTag.setPaymentMethodId(paymentMethod.getId());
                paymentMethodPlayerTag.setPlayerTagId(playerTagId);
                paymentMethodPlayerTags.add(paymentMethodPlayerTag);
            }
            paymentPlayerTagMapper.insert(paymentMethodPlayerTags);
        }

        List<Integer> playerIds = paymentMethod.getPlayerIds();
        if(!CollectionUtils.isEmpty(playerIds)){
            List<PaymentMethodPlayer> paymentMethodPlayers = new ArrayList<>();
            for(Integer playerId : playerIds) {
                PaymentMethodPlayer paymentMethodPlayer = new PaymentMethodPlayer();
                paymentMethodPlayer.setPlayerId(playerId);
                paymentMethodPlayer.setPaymentMethodId(paymentMethod.getId());
                paymentMethodPlayers.add(paymentMethodPlayer);
            }
            paymentPlayerMapper.insert(paymentMethodPlayers);
        }
    }

}
