package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.model.NoPageQueryForm;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodQueryForm extends NoPageQueryForm {
    @ApiModelProperty(value = "The id of payment method")
    private Integer id;
    @ApiModelProperty(value = "The name of payment method")
    private String name;
    @ApiModelProperty(value = "Payment method type: BANK_TRANSFER(1), QUICK_PAY(2), WEXIN(3), ALIPAY(4), OTHERS(0)",
            allowableValues = "0,1,2,3,4")
    private Integer type;
    @ApiModelProperty(value = "Currency of this payment method. Note that if a payment API is specified, " +
            "currency will follow that of the payment API")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "The ID of payment API")
    private Integer paymentApiId;
    @ApiModelProperty(value = "Applicable player-tag IDs")
    private List<Integer> playerTagIds;
    @ApiModelProperty(value = "Applicable player-group IDs")
    private List<Integer> playerGroupIds;
    @ApiModelProperty(value = "Applicable player IDs")
    private List<Integer> playerIds;
    @ApiModelProperty(value = "Who pays payment charge; true = PLAYER, false = PLATFORM")
    private Boolean playerBearPaymentCharge;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not appear in swagger UI")
    public PaymentMethodTypeEnum getTypeEnum() {
        return getType() == null ? null : PaymentMethodTypeEnum.valueOf(getType());
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getPaymentApiId() {
        return paymentApiId;
    }

    public void setPaymentApiId(Integer paymentApiId) {
        this.paymentApiId = paymentApiId;
    }

    public List<Integer> getPlayerTagIds() {
        if(playerTagIds == null) {
            playerTagIds = new ArrayList<>();
        }
        return playerTagIds;
    }

    public void setPlayerTagIds(List<Integer> playerTagIds) {
        this.playerTagIds = playerTagIds;
    }

    public List<Integer> getPlayerGroupIds() {
        if(playerGroupIds == null){
            playerGroupIds = new ArrayList<>();
        }
        return playerGroupIds;
    }

    public void setPlayerGroupIds(List<Integer> playerGroupIds) {
        this.playerGroupIds = playerGroupIds;
    }

    @ApiModelProperty(hidden = true, value="Derived property, return all tags and groups IDs")
    public List<Integer> getPlayerTagIdsAndGroupIds() {
        return new ArrayList<Integer>() {{
            addAll(getPlayerTagIds());
            addAll(getPlayerGroupIds());
        }};
    }

    public List<Integer> getPlayerIds() {
        if(playerIds == null){
            playerIds = new ArrayList<>();
        }
        return playerIds;
    }

    public void setPlayerIds(List<Integer> playerIds) {
        this.playerIds = playerIds;
    }

    public Boolean getPlayerBearPaymentCharge() {
        return playerBearPaymentCharge;
    }

    public void setPlayerBearPaymentCharge(Boolean playerBearPaymentCharge) {
        this.playerBearPaymentCharge = playerBearPaymentCharge;
    }
}

