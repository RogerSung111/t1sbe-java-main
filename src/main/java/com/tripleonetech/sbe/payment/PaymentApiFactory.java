package com.tripleonetech.sbe.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.integration.ApiConfigMapper;
import com.tripleonetech.sbe.common.integration.ApiFactory;
import com.tripleonetech.sbe.common.integration.ApiTypeEnum;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;
import com.tripleonetech.sbe.payment.integration.PaymentApi;

@Component
public class PaymentApiFactory extends ApiFactory<PaymentApi> {
    @Autowired
    private PaymentApiConfigMapper configMapper;

    @Override
    protected ApiTypeEnum getApiType() {
        return ApiTypeEnum.PAYMENT;
    }

    @Override
    protected ApiConfigMapper getConfigMapper() {
        return this.configMapper;
    }
}
