package com.tripleonetech.sbe.payment.report;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.payment.DepositRequest;

public class DepositSummaryReport {
    
    private LocalDateTime timeHour;
    private SiteCurrencyEnum currency;
    private Integer depositCount;
    private Integer depositorCount;
    private BigDecimal depositAmount;
    private Boolean dirty;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    
    
    
    public LocalDateTime getTimeHour() {
        return timeHour;
    }
    public void setTimeHour(LocalDateTime timeHour) {
        this.timeHour = timeHour;
    }
    public SiteCurrencyEnum getCurrency() {
        return currency;
    }
    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }
    public Integer getDepositCount() {
        return depositCount;
    }
    public void setDepositCount(Integer depositCount) {
        this.depositCount = depositCount;
    }
    public Integer getDepositorCount() {
        return depositorCount;
    }
    public void setDepositorCount(Integer depositorCount) {
        this.depositorCount = depositorCount;
    }
    public BigDecimal getDepositAmount() {
        return depositAmount;
    }
    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }
    public Boolean getDirty() {
        return dirty;
    }
    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    public static DepositSummaryReport create(DepositRequest depositRequest, SiteCurrencyEnum currency) {
        DepositSummaryReport depositSummaryReport = new DepositSummaryReport();
        depositSummaryReport.setDepositAmount(depositRequest.getAmount());
        depositSummaryReport.setCurrency(currency);
        depositSummaryReport.setDepositCount(1);
        depositSummaryReport.setDepositorCount(1);
        depositSummaryReport.setDirty(false);
        depositSummaryReport.setTimeHour(depositRequest.getRequestedDate().truncatedTo(ChronoUnit.HOURS));
        return depositSummaryReport;
    }
}
