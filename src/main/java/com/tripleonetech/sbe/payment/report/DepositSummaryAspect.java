package com.tripleonetech.sbe.payment.report;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.payment.DepositRequest;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.player.PlayerMapper;

@Aspect
@Component
public class DepositSummaryAspect {
    
    private static final Logger logger = LoggerFactory.getLogger(DepositSummaryAspect.class);
    
    @Autowired
    private DepositSummaryReportMapper depositSummaryReportMapper;
    
    @Autowired
    private PlayerMapper playerMapper;
    
    @After("execution(* com.tripleonetech.sbe.payment.service.DepositRequestService.processDepositRequest(..)))")
    public void setAsDirty(JoinPoint joinPoint) {
        DepositRequest depositRequest = (DepositRequest) joinPoint.getArgs()[0];
        DepositStatusEnum status = (DepositStatusEnum) joinPoint.getArgs()[1];

        if (null == depositRequest || DepositStatusEnum.APPROVED != status) {
            return;
        }
        Player player = playerMapper.get(depositRequest.getPlayerId());
        DepositSummaryReport depositSummaryReport = DepositSummaryReport.create(depositRequest, player.getCurrency());
        depositSummaryReportMapper.upsertDirty(depositSummaryReport);
    }

}
