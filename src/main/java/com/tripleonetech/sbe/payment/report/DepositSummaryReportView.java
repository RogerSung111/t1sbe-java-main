package com.tripleonetech.sbe.payment.report;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DepositSummaryReportView {
    @ApiModelProperty(value = "Date of deposit summary record")
    private LocalDate date;
    @ApiModelProperty(value = "Currency")
    private SiteCurrencyEnum currency;
    @ApiModelProperty(value = "Count of deposit requests")
    private int depositCount;
    @ApiModelProperty(value = "Number of deposit players")
    private int depositorCount;
    @ApiModelProperty(value = "Sum of deposit amount")
    private BigDecimal depositAmount;

    @ApiModelProperty(value = "Unique id for this data, used by frontend")
    public String getId() {
        return String.format("%s-%s", date.toString(), currency.toString());
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public int getDepositCount() {
        return depositCount;
    }

    public void setDepositCount(int depositCount) {
        this.depositCount = depositCount;
    }

    public int getDepositorCount() {
        return depositorCount;
    }

    public void setDepositorCount(int depositorCount) {
        this.depositorCount = depositorCount;
    }

    public BigDecimal getDepositAmount() {
        if(depositAmount == null){
            return  BigDecimal.ZERO;
        }
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }
}
