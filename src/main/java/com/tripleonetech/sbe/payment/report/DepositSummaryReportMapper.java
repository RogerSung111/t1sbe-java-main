package com.tripleonetech.sbe.payment.report;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.web.TimeRangeForm;

@Mapper
public interface DepositSummaryReportMapper {
    
    void upsertDirty(DepositSummaryReport depositSummaryReport);
    
    void refreshDirty();
    
    DepositSummaryReport get(@Param("timeHour") LocalDateTime cutoffTime, @Param("currency") SiteCurrencyEnum currency);
    
    List<DepositSummaryReport> listDirty();

    List<DepositSummaryReportView> query(@Param("query") TimeRangeForm query,
                                         @Param("pageNum") Integer pageNum,
                                         @Param("pageSize") Integer pageSize);
}
