package com.tripleonetech.sbe.payment.report.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.payment.report.DepositSummaryReportMapper;

@Profile("scheduled")
@Component
public class DepositSummaryCalculator {
    
    private final static Logger logger = LoggerFactory.getLogger(DepositSummaryCalculator.class);
    private final static long FIXED_DELEY_TEN_MINUTES = 10 * 60 * 1000;// 10 minutes

    @Autowired
    private DepositSummaryReportMapper depositSummaryReportMapper;

    @Scheduled(fixedDelay = FIXED_DELEY_TEN_MINUTES)
    public void runHourlyReport() {
        logger.info("Start refreshing dirty records of deposit_summary_report.");
        depositSummaryReportMapper.refreshDirty();
    }
    
}
