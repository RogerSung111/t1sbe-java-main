package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PaymentMethodTypeEnum implements BaseCodeEnum {
    BANK_TRANSFER(1), QUICK_PAY(2), WECHAT(3), ALIPAY(4), OTHERS(0);
    
    private int code;

    PaymentMethodTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
    
    public static PaymentMethodTypeEnum valueOf(Integer code) {
        PaymentMethodTypeEnum[] enumConstants = PaymentMethodTypeEnum.values();
        for (PaymentMethodTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
