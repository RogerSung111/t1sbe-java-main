package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.model.TagFilteredView;
import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.player.PlayerIdView;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PaymentMethodDetailView extends TagFilteredView {
    private int id;
    private String name;
    private int type;
    private SiteCurrencyEnum currency;
    private BigDecimal dailyMaxDeposit;
    private BigDecimal minDepositPerTrans;
    private BigDecimal maxDepositPerTrans;
    private BigDecimal chargeRatio;
    private BigDecimal fixedCharge;
    private boolean playerBearPaymentCharge;
    private int priority;
    private String note;
    private boolean enabled = true;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private IdNameView paymentApi;
    private List<PlayerIdView> players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public BigDecimal getDailyMaxDeposit() {
        return dailyMaxDeposit;
    }

    public void setDailyMaxDeposit(BigDecimal dailyMaxDeposit) {
        this.dailyMaxDeposit = dailyMaxDeposit;
    }

    public BigDecimal getMinDepositPerTrans() {
        return minDepositPerTrans;
    }

    public void setMinDepositPerTrans(BigDecimal minDepositPerTrans) {
        this.minDepositPerTrans = minDepositPerTrans;
    }

    public BigDecimal getMaxDepositPerTrans() {
        return maxDepositPerTrans;
    }

    public void setMaxDepositPerTrans(BigDecimal maxDepositPerTrans) {
        this.maxDepositPerTrans = maxDepositPerTrans;
    }

    public BigDecimal getChargeRatio() {
        return chargeRatio;
    }

    public void setChargeRatio(BigDecimal chargeRatio) {
        this.chargeRatio = chargeRatio;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public boolean isPlayerBearPaymentCharge() {
        return playerBearPaymentCharge;
    }

    public void setPlayerBearPaymentCharge(boolean playerBearPaymentCharge) {
        this.playerBearPaymentCharge = playerBearPaymentCharge;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public IdNameView getPaymentApi() {
        if(paymentApi == null || paymentApi.getName() == null){
            return null;
        }
        return paymentApi;
    }

    public void setPaymentApi(IdNameView paymentApi) {
        this.paymentApi = paymentApi;
    }

    public void setPaymentApi(int id, String name) {
        this.paymentApi = IdNameView.create(id, name);
    }

    public List<PlayerIdView> getPlayers() {
        // MyBatis collection will insert an empty object to the list when dealing with null left join
        if(players == null || players.size() == 1 && players.get(0).getUsername() == null){
            players = new ArrayList<>();
        }
        return players;
    }

    public void setPlayers(List<PlayerIdView> players) {
        this.players = players;
    }

}
