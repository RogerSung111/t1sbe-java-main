package com.tripleonetech.sbe.payment;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum DepositStatusEnum implements BaseCodeEnum {
    OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11), EXPIRED(12);

    private int code;

    DepositStatusEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return code;
    }

    @JsonCreator
    public static DepositStatusEnum valueOf(int status) {
        for (DepositStatusEnum enumInstance : DepositStatusEnum.values()) {
            if (status == enumInstance.code)
                return enumInstance;
        }
        return null;
    }

}
