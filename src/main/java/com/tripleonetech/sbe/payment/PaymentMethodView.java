package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.web.IdNameView;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

public class PaymentMethodView {
    @ApiModelProperty("Payment method ID")
    private int id;
    @ApiModelProperty("Payment API ID")
    private IdNameView paymentApi;
    @ApiModelProperty("Payment method name")
    private String name;
    @ApiModelProperty(value = "Payment method type. BANK_TRANSFER(1), QUICK_PAY(2), WECHAT(3), ALIPAY(4), OTHERS(0)", allowableValues = "0,1,2,3,4")
    private Integer type;
    @ApiModelProperty("note of payment method")
    private String note;
    @ApiModelProperty("Minmum amount of deposit")
    private BigDecimal minDeposit;
    @ApiModelProperty("Maximum amount of deposit")
    private BigDecimal maxDeposit;
    @ApiModelProperty(value = "Supported payment form type. ManualAtmForm(1), SimpleForm(2), QrForm(3), QuickPayForm(4)", allowableValues = "1,2,3,4")
    private int formType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IdNameView getPaymentApi() {
        return paymentApi;
    }

    public void setPaymentApi(IdNameView paymentApi) {
        this.paymentApi = paymentApi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getFormType() {
        return formType;
    }

    public void setFormType(int formType) {
        this.formType = formType;
    }

    public BigDecimal getMinDeposit() {
        return minDeposit;
    }

    public void setMinDeposit(BigDecimal minDeposit) {
        this.minDeposit = minDeposit;
    }

    public BigDecimal getMaxDeposit() {
        return maxDeposit;
    }

    public void setMaxDeposit(BigDecimal maxDeposit) {
        this.maxDeposit = maxDeposit;
    }

    public static PaymentMethodView from(PaymentMethodDetailView paymentMethodDetail){
        PaymentMethodView view = new PaymentMethodView();
        BeanUtils.copyProperties(paymentMethodDetail, view);
        return view;
    }
}
