package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.player.tag.PlayerTagLogicalOperator;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

// Note: Future dev: icon, qrcode, maxDepositCount
public class PaymentMethod {
    private int id;
    private String name;
    private PaymentMethodTypeEnum type;
    private Integer paymentApiId;
    private SiteCurrencyEnum currency;
    private BigDecimal dailyMaxDeposit;
    private BigDecimal minDepositPerTrans;
    private BigDecimal maxDepositPerTrans;
    private BigDecimal chargeRatio;
    private BigDecimal fixedCharge;
    private boolean playerBearPaymentCharge;
    private PlayerTagLogicalOperator playerTagOperator;
    private int priority;
    private String note; // max size: 65535
    private boolean enabled = true;
    private boolean deleted;
    private List<Integer> playerTagIds; // This includes both tag and group ids
    private List<Integer> playerIds;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public PaymentMethodTypeEnum getType() {
        return type;
    }

    public void setType(PaymentMethodTypeEnum type) {
        this.type = type;
    }

    public Integer getPaymentApiId() {
        return paymentApiId;
    }

    public void setPaymentApiId(Integer paymentApiId) {
        this.paymentApiId = paymentApiId;
    }

    public SiteCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(SiteCurrencyEnum currency) {
        // Only update if there is no currency
        if(this.currency == null) {
            this.currency = currency;
        }
        // Currency should be updated by setting actualCurrency
    }

    public void setActualCurrency(SiteCurrencyEnum currency) {
        this.currency = currency;
    }

    public BigDecimal getDailyMaxDeposit() {
        return dailyMaxDeposit;
    }

    public void setDailyMaxDeposit(BigDecimal dailyMaxDeposit) {
        this.dailyMaxDeposit = dailyMaxDeposit;
    }

    public BigDecimal getMinDepositPerTrans() {
        return minDepositPerTrans;
    }

    public void setMinDepositPerTrans(BigDecimal minDepositPerTrans) {
        this.minDepositPerTrans = minDepositPerTrans;
    }

    public BigDecimal getMaxDepositPerTrans() {
        return maxDepositPerTrans;
    }

    public void setMaxDepositPerTrans(BigDecimal maxDepositPerTrans) {
        this.maxDepositPerTrans = maxDepositPerTrans;
    }

    public BigDecimal getChargeRatio() {
        return chargeRatio;
    }

    public void setChargeRatio(BigDecimal chargeRatio) {
        this.chargeRatio = chargeRatio;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public boolean isPlayerBearPaymentCharge() {
        return playerBearPaymentCharge;
    }

    public void setPlayerBearPaymentCharge(boolean playerBearPaymentCharge) {
        this.playerBearPaymentCharge = playerBearPaymentCharge;
    }

    public PlayerTagLogicalOperator getPlayerTagOperator() {
        return playerTagOperator;
    }

    public void setPlayerTagOperator(PlayerTagLogicalOperator playerTagOperator) {
        this.playerTagOperator = playerTagOperator;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Integer> getPlayerTagIds() {
        if(playerTagIds == null){
            playerTagIds = new ArrayList<>();
        }
        return playerTagIds;
    }

    public void setPlayerTagIds(List<Integer> playerTagIds) {
        this.playerTagIds = playerTagIds;
    }

    @ApiModelProperty(hidden = true, value = "Derived property setter, used by mapper to properly set playerTag IDs into a list")
    public void setPlayerTagIdList(String playerTagIdList){
        this.playerTagIds = new ArrayList<>();

        if(StringUtils.isBlank(playerTagIdList)){
            return;
        }

        for(String anId : playerTagIdList.split(",")){
            this.playerTagIds.add(Integer.parseInt(anId));
        }
    }

    public List<Integer> getPlayerIds() {
        if(playerIds == null){
            playerIds = new ArrayList<>();
        }
        return playerIds;
    }

    public void setPlayerIds(List<Integer> playerIds) {
        this.playerIds = playerIds;
    }

    @ApiModelProperty(hidden = true, value = "Derived property setter, used by mapper to properly set player IDs into a list")
    public void setPlayerIdList(String playerIdList){
        this.playerIds = new ArrayList<>();

        if(StringUtils.isBlank(playerIdList)){
            return;
        }

        for(String anId : playerIdList.split(",")){
            this.playerIds.add(Integer.parseInt(anId));
        }
    }

    public static PaymentMethod from(PaymentMethodForm form){
        PaymentMethod paymentMethod = new PaymentMethod();
        BeanUtils.copyProperties(form, paymentMethod);
        paymentMethod.setType(form.getTypeEnum());
        paymentMethod.setPlayerTagIds(form.getPlayerTagIdsAndGroupIds());
        //paymentMethod.setPlayerBearPaymentCharge(form.isPlayerBearPaymentCharge());
        return paymentMethod;
    }

    // This class is used to store the accumulated deposit amount for each payment methods
    public static class PaymentMethodIdAndAmount {
        private int id;
        private BigDecimal amount;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    }
}
