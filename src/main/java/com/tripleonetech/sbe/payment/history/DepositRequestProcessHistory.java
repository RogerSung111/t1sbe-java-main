package com.tripleonetech.sbe.payment.history;

import com.tripleonetech.sbe.payment.DepositStatusEnum;

import java.time.LocalDateTime;

public class DepositRequestProcessHistory {

    private Long id;

    private DepositStatusEnum fromStatus;

    private DepositStatusEnum toStatus;

    private Long depositRequestId;

    private String comment;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Integer createdBy;

    private Integer updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DepositStatusEnum getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(DepositStatusEnum fromStatus) {
        this.fromStatus = fromStatus;
    }

    public DepositStatusEnum getToStatus() {
        return toStatus;
    }

    public void setToStatus(DepositStatusEnum toStatus) {
        this.toStatus = toStatus;
    }

    public Long getDepositRequestId() {
        return depositRequestId;
    }

    public void setDepositRequestId(Long depositRequestId) {
        this.depositRequestId = depositRequestId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    private static DepositRequestProcessHistory create(DepositStatusEnum fromStatus, DepositStatusEnum toStatus,
                                                       Long depositRequestId, String comment,
                                                       Integer createdBy, Integer updatedBy) {
        DepositRequestProcessHistory processHistory = new DepositRequestProcessHistory();
        processHistory.setDepositRequestId(depositRequestId);
        processHistory.setFromStatus(fromStatus);
        processHistory.setToStatus(toStatus);
        processHistory.setComment(comment);
        processHistory.setCreatedBy(createdBy);
        processHistory.setUpdatedBy(updatedBy);
        return processHistory;
    }

    public static DepositRequestProcessHistory createByOperator(DepositStatusEnum fromStatus, DepositStatusEnum toStatus,
                                                                Long depositRequestId, String comment,
                                                                Integer createdBy, Integer updatedBy) {
        return create(fromStatus, toStatus, depositRequestId, comment, createdBy, updatedBy);
    }

    public static DepositRequestProcessHistory createByPlayer(DepositStatusEnum fromStatus, DepositStatusEnum toStatus,
                                                              Long depositRequestId, String comment) {
        return create(fromStatus, toStatus, depositRequestId, comment, null, null);
    }

    public static DepositRequestProcessHistory createBySystem(DepositStatusEnum fromStatus, DepositStatusEnum toStatus,
                                                              Long depositRequestId, String comment) {
        return create(fromStatus, toStatus, depositRequestId, comment, 0, 0);
    }

}


