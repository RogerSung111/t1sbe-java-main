package com.tripleonetech.sbe.payment.history;

import com.tripleonetech.sbe.common.web.IdNameView;
import com.tripleonetech.sbe.payment.DepositStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class DepositRequestProcessHistoryView {

    @ApiModelProperty(value = "Original status of deposit request, OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)")
    private DepositStatusEnum fromStatus;

    @ApiModelProperty(value = "New status of deposit request, OPEN(0), APPROVED(1), REJECTED(10), CANCELED(11)")
    private DepositStatusEnum toStatus;

    @ApiModelProperty(value = "Detail information about deposit process")
    private String comment;

    @ApiModelProperty(value = "Create time of process history")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "Last updated time of process history")
    private LocalDateTime updatedAt;

    @ApiModelProperty(value = "Creator of deposit process history")
    private IdNameView createdBy;

    @ApiModelProperty(value = "Last updater of deposit process history")
    private IdNameView updatedBy;

    public DepositStatusEnum getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(DepositStatusEnum fromStatus) {
        this.fromStatus = fromStatus;
    }

    public DepositStatusEnum getToStatus() {
        return toStatus;
    }

    public void setToStatus(DepositStatusEnum toStatus) {
        this.toStatus = toStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public IdNameView getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(IdNameView createdBy) {
        this.createdBy = createdBy;
    }

    public IdNameView getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(IdNameView updatedBy) {
        this.updatedBy = updatedBy;
    }

}
