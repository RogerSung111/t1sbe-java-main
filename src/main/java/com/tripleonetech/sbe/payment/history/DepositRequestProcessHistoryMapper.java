package com.tripleonetech.sbe.payment.history;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DepositRequestProcessHistoryMapper {

    void insert(DepositRequestProcessHistory requestHistory);

    List<DepositRequestProcessHistoryView> getComments(@Param("depositRequestId") Long depositRequestId);

}
