package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum PaymentFormTypeEnum implements BaseCodeEnum {
    ManualAtmForm(1), SimpleForm(2), QrForm(3), QuickPayForm(4);

    private int code;

    PaymentFormTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
