package com.tripleonetech.sbe.payment;

import com.tripleonetech.sbe.player.PlayerProfile;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class DepositRequest {

    private Long id;
    private Integer playerId;
    private Integer paymentMethodId;
    private String paymentChannel;
    private BigDecimal amount;
    private BigDecimal paymentCharge;
    private DepositStatusEnum status;
    private String note;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime expirationDate;
    private String externalUid;
    private LocalDateTime requestedDate;
    private PlayerProfile playerProfile;
    
    /** player related */
    private String username;
    private Integer playerCredentialId;
    
    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }


    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DepositStatusEnum getStatus() {
        if(DepositStatusEnum.OPEN == status && LocalDateTime.now().isAfter(getExpirationDate())){
            return DepositStatusEnum.EXPIRED;
        }
        return status;
    }

    public void setStatus(DepositStatusEnum status) {
        this.status = status;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getExternalUid() {
        return externalUid;
    }

    public void setExternalUid(String externalUid) {
        this.externalUid = externalUid;
    }

    public LocalDateTime getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(LocalDateTime requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PlayerProfile getPlayerProfile() {
        return playerProfile;
    }

    public void setPlayerProfile(PlayerProfile playerProfile) {
        this.playerProfile = playerProfile;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    public BigDecimal getPaymentCharge() {
        return paymentCharge;
    }

    public void setPaymentCharge(BigDecimal paymentCharge) {
        this.paymentCharge = paymentCharge;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlayerCredentialId() {
        return playerCredentialId;
    }

    public void setPlayerCredentialId(Integer playerCredentialId) {
        this.playerCredentialId = playerCredentialId;
    }

    @Override
    public String toString() {
        return "DepositRequest [id=" + id + ", playerId=" + playerId + ", paymentMethodId=" + paymentMethodId
                + ", amount=" + amount + ", paymentCharge=" + paymentCharge +", status=" + status + ", note=" + note + ", createdAt=" + createdAt
                + ", updatedAt=" + updatedAt + ", expirationDate=" + expirationDate + ", requestedDate=" + requestedDate
                + "]";
    }

    
}
