package com.tripleonetech.sbe.aspect;

import java.util.Optional;
import java.util.regex.Pattern;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.player.Player;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReport;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReportMapper;
import com.tripleonetech.sbe.web.api.BaseApiController;

@Aspect
@Profile("api")
@Component
public class ExtApiAspect extends BaseApiController {

    private static final Logger logger = LoggerFactory.getLogger(ExtApiAspect.class);

    @Autowired
    private ExtApiActivityReportMapper mapper;


    @Around("@annotation(logExtApi)")
    public ApiResult createReport(ProceedingJoinPoint point, LogExtApi logExtApi) throws Throwable {
        ApiResult apiResult = (ApiResult) point.proceed();
        MethodSignature signature = (MethodSignature) point.getSignature();
        // In case of proxies we split the ugly $$ part away
        String className = point.getTarget().getClass().getSimpleName().split(Pattern.quote("$$"))[0];
        ExtApiTypeEnum extApiType = logExtApi.extApiType();
        ExtApiActivityReport report = new ExtApiActivityReport();
        report.setExtApiType(extApiType);
        report.setApiResult(JsonUtils.objectToJson(apiResult));
        report.setInvoke(className + "." + signature.getName());
        report.setReturnCode(apiResult.getResponseEnum().getCode());
        report.setParams(JsonUtils.objectToJson(point.getArgs()));
        report.setPlayerId(Optional.ofNullable(getLoginPlayer()).map(Player::getId).orElse(null));
        mapper.insert(report);
        return apiResult;
    }

}
