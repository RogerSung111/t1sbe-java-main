package com.tripleonetech.sbe.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReport;
import com.tripleonetech.sbe.report.extapi.ExtApiActivityReportMapper;

@Service
public class LogExtApiService {
    private static final Logger logger = LoggerFactory.getLogger(LogExtApiService.class);

    @Autowired
    private ExtApiActivityReportMapper mapper;

    public ApiResult createReport(ExtApiTypeEnum extApiType, String invoke, String params, ApiResult apiResult,
            Integer playerId) {
        // In case of proxies we split the ugly $$ part away
        ExtApiActivityReport report = new ExtApiActivityReport();
        report.setExtApiType(extApiType);
        report.setApiResult(JsonUtils.objectToJson(apiResult));
        report.setInvoke(invoke);
        report.setReturnCode(apiResult.getResponseEnum().getCode());
        report.setParams(params);
        report.setPlayerId(playerId);
        mapper.insert(report);
        return apiResult;
    }
}
