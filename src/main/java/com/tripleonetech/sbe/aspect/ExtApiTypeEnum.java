package com.tripleonetech.sbe.aspect;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum ExtApiTypeEnum implements BaseCodeEnum {

    AUTH(0), LAUNCH_GAME(1), DEPOSIT(2), WITHDRAW(3), DEFAULT(999);

    private int code;

    ExtApiTypeEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static ExtApiTypeEnum valueOf(Integer code) {
        ExtApiTypeEnum[] enumConstants = ExtApiTypeEnum.values();
        for (ExtApiTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
