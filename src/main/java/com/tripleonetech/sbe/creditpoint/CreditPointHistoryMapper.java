package com.tripleonetech.sbe.creditpoint;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CreditPointHistoryMapper {

    int insert(CreditPointHistory history);

    CreditPointHistory get(Integer id);

    int updateStatus(Integer id, CreditPointStatusEnum status);
}