package com.tripleonetech.sbe.creditpoint;

import com.tripleonetech.sbe.creditpoint.history.CreditPointTransactionMapper;
import com.tripleonetech.sbe.creditpoint.history.CreditPointTransactionTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class CreditPointService {
    private static final Logger logger = LoggerFactory.getLogger(CreditPointService.class);

    @Autowired
    private CreditPointMapper creditPointMapper;
    @Autowired
    private CreditPointTransactionMapper transactionMapper;

    @Transactional
    public boolean addCreditPoint(BigDecimal amount) {
        return creditPointMapper.add(amount) > 0 ? true : false;
    }

    @Transactional
    public boolean subtractCreditPoint(BigDecimal amount) {
        try {
            creditPointMapper.subtract(amount);
        }catch(org.springframework.jdbc.UncategorizedSQLException e) {
            logger.warn("The credit point is less than [{}]", amount);
            return false;
        }
        return true;
    }
    
    /**
     * For deposit/withdraw game only
     * 
     * @param amount
     * @param transactionId
     * @return
     */
    @Transactional
    public int recordHistory(BigDecimal amount, Integer transactionId) {
        return transactionMapper.insert(CreditPointTransactionTypeEnum.GAME, transactionId, amount);
    }
}
