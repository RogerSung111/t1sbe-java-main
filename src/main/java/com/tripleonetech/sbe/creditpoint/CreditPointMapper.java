package com.tripleonetech.sbe.creditpoint;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CreditPointMapper {

    BigDecimal get();

    int add(BigDecimal amount);

    int subtract(BigDecimal amount);

}
