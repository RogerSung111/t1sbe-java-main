package com.tripleonetech.sbe.creditpoint;

import com.tripleonetech.sbe.common.redis.CreditPointRequest;
import com.tripleonetech.sbe.creditpoint.history.CreditPointTransactionMapper;
import com.tripleonetech.sbe.creditpoint.history.CreditPointTransactionTypeEnum;
import com.tripleonetech.sbe.redis.T1DaemonCommand;
import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;
import com.tripleonetech.sbe.redis.publisher.T1DaemonMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("main")
@Service
public class CreditPointDaemonService {
    private static final Logger logger = LoggerFactory.getLogger(CreditPointDaemonService.class);

    @Autowired
    private CreditPointMapper creditPointMapper;
    @Autowired
    private CreditPointTransactionMapper creditPointTransactionMapper;
    @Autowired
    private CreditPointHistoryMapper creditPointHistoryMapper;
    @Autowired
    private T1DaemonMessageService redisService;

    public void processRequest(Object object) {
        CreditPointRequest request = (CreditPointRequest) object;

        CreditPointHistory history = new CreditPointHistory();
        BeanUtils.copyProperties(request, history);
        creditPointHistoryMapper.insert(history);
        try {
            switch (history.getStatus()) {
                case DEBIT:
                    history.setStatus(CreditPointStatusEnum.DEBIT_DONE);
                    creditPointMapper.subtract(history.getCreditPoint());
                    creditPointTransactionMapper.insert(CreditPointTransactionTypeEnum.MANUAL, null, history.getCreditPoint().negate());
                    break;
                case CREDIT:
                    history.setStatus(CreditPointStatusEnum.CREDIT_DONE);
                    creditPointMapper.add(history.getCreditPoint());
                    creditPointTransactionMapper.insert(CreditPointTransactionTypeEnum.MANUAL, null, history.getCreditPoint());
                    break;
                default:
            }
        } catch (Exception e) {
            logger.error("Request id:[{}] process error", history.getId());
            history.setStatus(history.getStatus() == CreditPointStatusEnum.DEBIT ? CreditPointStatusEnum.DEBIT_ERROR : CreditPointStatusEnum.CREDIT_ERROR);
        }
        creditPointHistoryMapper.updateStatus(history.getId(), history.getStatus());
        logger.debug(history.getStatus() + " " + history.getCreditPoint());

        BeanUtils.copyProperties(history, request);
        sendStatusBack(request);
    }

    private void sendStatusBack(CreditPointRequest request) {
        T1DaemonCommand messageObject = new T1DaemonCommand();
        messageObject.setMethodName(T1DaemonMethodEnum.CREDIT_POINT_STATUS);
        messageObject.setContent(request);
        redisService.sendToServer(messageObject);
    }

}
