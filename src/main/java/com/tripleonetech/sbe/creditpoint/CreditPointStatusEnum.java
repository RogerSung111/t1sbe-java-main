package com.tripleonetech.sbe.creditpoint;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum CreditPointStatusEnum implements BaseCodeEnum {
    OPEN(0), DEBIT(1), DEBIT_DONE(2), CREDIT(3), CREDIT_DONE(4), FINISHED(5), DEBIT_ERROR(7), CREDIT_ERROR(8), ERROR(9);

    private int code;

    CreditPointStatusEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }
}
