package com.tripleonetech.sbe.creditpoint;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CreditPointHistory implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 9104296870829489388L;

    private Integer id;

    private String from;

    private String to;

    private CreditPointStatusEnum status;

    private Boolean timeout;

    private BigDecimal creditPoint;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public CreditPointStatusEnum getStatus() {
        return status;
    }

    public void setStatus(CreditPointStatusEnum status) {
        this.status = status;
    }

    public Boolean getTimeout() {
        return timeout;
    }

    public void setTimeout(Boolean timeout) {
        this.timeout = timeout;
    }

    public BigDecimal getCreditPoint() {
        return creditPoint;
    }

    public void setCreditPoint(BigDecimal creditPoint) {
        this.creditPoint = creditPoint;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
