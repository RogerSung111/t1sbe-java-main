package com.tripleonetech.sbe.creditpoint.history;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface CreditPointTransactionMapper {

    int insert(@Param("creditPointTransactionType") CreditPointTransactionTypeEnum creditPointTransactionType,
               @Param("transactionId") Integer transactionId,
               @Param("creditPoint") BigDecimal creditPoint);

    CreditPointTransaction get(Integer id);

}