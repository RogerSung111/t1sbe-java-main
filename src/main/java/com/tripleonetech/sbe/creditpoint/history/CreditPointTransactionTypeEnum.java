package com.tripleonetech.sbe.creditpoint.history;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum CreditPointTransactionTypeEnum implements BaseCodeEnum {
    MANUAL(0), GAME(1);

    private int code;

    CreditPointTransactionTypeEnum(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

}
