package com.tripleonetech.sbe.creditpoint.history;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CreditPointTransaction {
    private Integer id;

    private CreditPointTransactionTypeEnum creditPointTransactionTypeEnum;

    private Integer transactionId;

    private BigDecimal creditPoint;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CreditPointTransactionTypeEnum getCreditPointTransactionTypeEnum() {
        return creditPointTransactionTypeEnum;
    }

    public void setCreditPointTransactionTypeEnum(CreditPointTransactionTypeEnum creditPointTransactionTypeEnum) {
        this.creditPointTransactionTypeEnum = creditPointTransactionTypeEnum;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getCreditPoint() {
        return creditPoint;
    }

    public void setCreditPoint(BigDecimal creditPoint) {
        this.creditPoint = creditPoint;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}