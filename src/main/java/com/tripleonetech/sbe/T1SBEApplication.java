package com.tripleonetech.sbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class T1SBEApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(T1SBEApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication apiApp = new SpringApplication(T1SBEApplication.class);
        apiApp.run(args);
    }
}
