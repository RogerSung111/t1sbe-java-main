package com.tripleonetech.sbe.redis.publisher.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;
import com.tripleonetech.sbe.redis.publisher.DataPublisher;
import com.tripleonetech.sbe.redis.publisher.service.HeartbeatPublisherService;

@Profile("main")
@Component
public class HeartbeatPublisher extends DataPublisher {
    
    @Autowired
    private HeartbeatPublisherService heartbeatPublisherService;

    @Scheduled(fixedDelayString = "${constant.publisher.heartbeat-delay-milliseconds}")
    public void publisher() throws JsonProcessingException {
        super.publish(T1DaemonMethodEnum.HEARTBEAT, heartbeatPublisherService.getHeartbeatData());
    }

  
}
