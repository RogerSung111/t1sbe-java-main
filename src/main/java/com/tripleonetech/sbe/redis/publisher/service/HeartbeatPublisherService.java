package com.tripleonetech.sbe.redis.publisher.service;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.creditpoint.CreditPointMapper;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.game.log.sync.GameLogSyncTaskMapper;
import com.tripleonetech.sbe.oauth.OauthService;
import com.tripleonetech.sbe.operator.OperatorOauth;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("main")
@Service
public class HeartbeatPublisherService {

    private static final Logger logger = LoggerFactory.getLogger(HeartbeatPublisherService.class);
    private final int TIME_PERIOD_HOURS = 1;
    private final static String SUPERADMIN_OPERATOR = "superadmin";

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;

    @Autowired
    private GameLogSyncTaskMapper gameLogSyncTaskMapper;

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    @Autowired
    private CreditPointMapper creditPointMapper;

    @Autowired
    private OauthService oauthService;

    public String getHeartbeatData() {
        Map<String, Object> heartbeatDataMap = new HashMap<>();
        LocalDateTime startTime = LocalDateTime.now().minusHours(TIME_PERIOD_HOURS);
        LocalDateTime endTime = LocalDateTime.now();
        // API Status
        List<Map<String, Object>> syncTaskLogList = gameLogSyncTaskMapper.getSyncTaskSummary(startTime, endTime);
        heartbeatDataMap.put("apiStatus", syncTaskLogList);
        // Site Privilege
        List<SitePrivilege> allPrivileges = sitePrivilegeMapper.allSitePrivileges();
        heartbeatDataMap.put("privilege", allPrivileges);

        Optional<SitePrivilege> currentTemplate = allPrivileges.stream().filter(
                p -> StringUtils.equals(SitePrivilegeTypeEnum.SITE_TEMPLATE.toString(), p.getType()) && p.isActive()
        ).findFirst();
        heartbeatDataMap.put("currentTemplate", currentTemplate.orElse(new SitePrivilege()).getValue());

        List<GameApiConfig> apiConfigs = gameApiConfigMapper.list();
        Map<Integer, String> configMap = new HashMap<>();
        if (apiConfigs.size() > 0) {
            configMap = apiConfigs.stream().collect(Collectors.toMap(GameApiConfig::getId, GameApiConfig::getMd5));
        }
        heartbeatDataMap.put("apiConfig", configMap);

        // Credit point
        BigDecimal balance = creditPointMapper.get();
        heartbeatDataMap.put("creditPoint", balance);

        // Report Time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime reportTime = LocalDateTime.now();
        heartbeatDataMap.put("lastReportTime", reportTime.format(formatter));

        //superadmin's oauth refresh_token
        oauthService.refreshExpiredOperatorRefreshToken(SUPERADMIN_OPERATOR);
        OperatorOauth operatorOauth = oauthService.getOperatorOauth(SUPERADMIN_OPERATOR);
        heartbeatDataMap.put("refreshToken", (operatorOauth == null) ? "" : operatorOauth.getRefreshToken());
        logger.info("Get heartbeat message : {}", JsonUtils.objectToJson(heartbeatDataMap));
        return JsonUtils.objectToJson(heartbeatDataMap);
    }

}
