package com.tripleonetech.sbe.redis.publisher;


import org.springframework.beans.factory.annotation.Autowired;

import com.tripleonetech.sbe.redis.T1DaemonCommand;
import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;

public abstract class DataPublisher {

    @Autowired
    T1DaemonMessageService redisService;

    protected void publish(T1DaemonMethodEnum methodName, Object data) {
        if (null == data) {
            return;
        }

        T1DaemonCommand messageObject = new T1DaemonCommand();
        messageObject.setMethodName(methodName);
        messageObject.setContent(data);
        redisService.sendToServer(messageObject);
    }

}
