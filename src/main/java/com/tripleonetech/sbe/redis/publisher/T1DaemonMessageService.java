package com.tripleonetech.sbe.redis.publisher;

import com.tripleonetech.sbe.command.CommandHistory;
import com.tripleonetech.sbe.command.CommandHistoryMapper;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.redis.RedisService;
import com.tripleonetech.sbe.redis.T1DaemonCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * This class provides implementation to PUB/SUB with the configured Redis server.
 *
 * @author Yunfei<yunfei.dev @ tripleonetech.net>
 * Created on 22 Dec 2018
 */
@Profile("main")
@Service
public class T1DaemonMessageService {

    @Autowired
    private RedisService redisService;
    @Autowired
    private Constant constant;
    @Autowired
    private CommandHistoryMapper commandHistoryMapper;

    public void sendToServer(T1DaemonCommand command) {
        CommandHistory commandHistory = new CommandHistory();
        Object content = command.getContent();
        commandHistory.setContent(content instanceof String ? (String) content : JsonUtils.objectToJson(content));
        commandHistory.setMethodName(command.getMethodName().toString());
        commandHistory.setCommandType(CommandHistory.CommandTypeEnum.PUBLISH);
        commandHistoryMapper.insert(commandHistory);

        String channel = "up." + constant.getDaemon().getServerId();
        String recieverPublicKey = constant.getDaemonServer().getPubKey();
        String providerPrivateKey = constant.getDaemon().getPrivKey();
        redisService.publishEncryptedAndSigned(command, channel, recieverPublicKey, providerPrivateKey);
    }
}
