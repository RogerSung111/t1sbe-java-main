package com.tripleonetech.sbe.redis.publisher.scheduled;

import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;
import com.tripleonetech.sbe.redis.publisher.DataPublisher;
import com.tripleonetech.sbe.redis.publisher.service.GameLogHourlyPublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("main")
@Component
public class GameLogHourlyPublisher extends DataPublisher {

    @Autowired
    private GameLogHourlyPublisherService gameLogHourlyPublisherService;

    @Scheduled(fixedDelayString = "${constant.publisher.game-log-delay-milliseconds}")
    public void publisher() {
        super.publish(T1DaemonMethodEnum.GAME_LOG_HOURLY_REPORT, gameLogHourlyPublisherService.getPublishData());
    }
}
