package com.tripleonetech.sbe.redis.publisher.service;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.game.log.GameLogHourlyReport;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;

@Service
public class GameLogHourlyPublisherService {
    private static final Logger logger = LoggerFactory.getLogger(GameLogHourlyPublisherService.class);

    @Autowired
    private GameLogHourlyReportMapper hourlyReportMapper;

    public List<GameLogHourlyReport> getPublishData() {
        LocalDateTime endTime = LocalDateTime.now();
        int duration = 2 * Constant.getInstance().getPublisher().getHeartbeatDelayMilliseconds() / 1000;// convert milliseconds to sec
        LocalDateTime startTime = endTime.minusSeconds(duration);

        List<GameLogHourlyReport> gameLogHourlyData = hourlyReportMapper.listByUpdatedAt(startTime, endTime);
        logger.info("Publish GameLogHourlyReports between [{}] and [{}], Count: [{}]", startTime, endTime,
                gameLogHourlyData.size());

        return gameLogHourlyData;
    }

}
