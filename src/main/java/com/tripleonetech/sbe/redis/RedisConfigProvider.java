package com.tripleonetech.sbe.redis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@ConditionalOnProperty("redis.enabled")
@Profile("main")
@Configuration
public class RedisConfigProvider {

    @Autowired
    private LettuceConnectionFactory redisConnectionFactory;

    @Autowired
    private List<TopicMessageListener> messageListeners;

    @Bean
    RedisMessageListenerContainer redisContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        for (TopicMessageListener listener : messageListeners) {
            container.addMessageListener(listener, listener.getTopic());
        }
        return container;
    }
}
