package com.tripleonetech.sbe.redis;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.common.RSACrypto;

/**
 * This class provides implementation to PUB/SUB with the configured Redis server.
 * 
 * @author Yunfei<yunfei.dev@tripleonetech.net>
 *         Created on 22 Dec 2018
 */
@Profile("main")
@Service
public class RedisService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisSerializer<Object> redisSerializer;

    public void publish(Object object, final String channel) {
        byte[] src = redisSerializer.serialize(object);
        String message = Base64.getEncoder().encodeToString(src);
        stringRedisTemplate.convertAndSend(channel, message);
    }

    public void publishSigned(Object object, final String channel, String providerPrivateKey) {
        // serialize
        byte[] data = redisSerializer.serialize(object);
        // sign
        byte[] signature = RSACrypto.sign(data, providerPrivateKey);
        byte[][] dataArray = new byte[][] { data, signature };
        // encode to base 64
        byte[] src = redisSerializer.serialize(dataArray);
        String message = Base64.getEncoder().encodeToString(src);
        stringRedisTemplate.convertAndSend(channel, message);
    }

    public void publishEncryptedAndSigned(Object object, final String channel, String recieverPublicKey, String providerPrivateKey) {
        // serialize
        byte[] data = redisSerializer.serialize(object);
        // encrypt
        data = RSACrypto.encrypt(data, recieverPublicKey);
        // sign
        byte[] signature = RSACrypto.sign(data, providerPrivateKey);
        byte[][] dataArray = new byte[][] { data, signature };
        // encode to base 64
        byte[] src = redisSerializer.serialize(dataArray);
        String message = Base64.getEncoder().encodeToString(src);
        stringRedisTemplate.convertAndSend(channel, message);
    }
}
