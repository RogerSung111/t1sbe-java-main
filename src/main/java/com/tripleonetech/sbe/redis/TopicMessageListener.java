package com.tripleonetech.sbe.redis;

import java.util.Base64;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.Topic;

import com.tripleonetech.sbe.common.RSACrypto;
import com.tripleonetech.sbe.redis.listener.T1SBEMessageListener;

public abstract class TopicMessageListener implements MessageListener {

    private Logger logger = LoggerFactory.getLogger(T1SBEMessageListener.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public abstract Collection<Topic> getTopic();

    public abstract void onMessage(Message message, byte[] pattern);

    @SuppressWarnings("unchecked")
    protected <T> T verifyAndDecrypt(Message message, String providerPublicKey, String recieverPrivateKey) {
        byte[] src = Base64.getDecoder().decode(message.getBody());
        byte[][] dataArray = (byte[][]) redisTemplate.getValueSerializer().deserialize(src);
        byte[] data = dataArray[0];
        byte[] signature = dataArray[1];
        if (RSACrypto.verify(data, signature, providerPublicKey)) {
            data = RSACrypto.decrypt(data, recieverPrivateKey);
            return (T) redisTemplate.getValueSerializer().deserialize(data);
        } else {
            logger.warn("verify failed");
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    protected <T> T verifyAndGetData(Message message, String providerPublicKey) {
        byte[] data = verify(message, providerPublicKey);
        return (T) redisTemplate.getValueSerializer().deserialize(data);
    }

    private byte[] verify(Message message, String providerPublicKey) {
        byte[] src = Base64.getDecoder().decode(message.getBody());
        byte[][] dataArray = (byte[][]) redisTemplate.getValueSerializer().deserialize(src);
        byte[] data = dataArray[0];
        byte[] signature = dataArray[1];
        if (RSACrypto.verify(data, signature, providerPublicKey)) {
            return data;
        } else {
            logger.warn("verify failed");
            return null;
        }
    }
}
