package com.tripleonetech.sbe.redis;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class T1DaemonCommand implements Serializable {

    private static final long serialVersionUID = 2018536357006551705L;
    
    private Map<String, String> applicableUsers = new HashMap<>();
    private Object content;
    private T1DaemonMethodEnum methodName;
    
    public Map<String, String> getApplicableUsers() {
        return applicableUsers;
    }

    public void setApplicableUsers(Map<String, String> applicableUsers) {
        this.applicableUsers = applicableUsers;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public T1DaemonMethodEnum getMethodName() {
        return methodName;
    }

    public void setMethodName(T1DaemonMethodEnum methodName) {
        this.methodName = methodName;
    }
}
