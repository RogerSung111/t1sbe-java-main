package com.tripleonetech.sbe.redis.listener;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.redis.TopicMessageListener;

/**
 * A simple example to subscribe string
 * @author Nicolas
 *
 */
@Profile("main")
@Component
public class T1SBEMessageListener extends TopicMessageListener {

    private Logger logger = LoggerFactory.getLogger(T1SBEMessageListener.class);

    @Override
    public Collection<Topic> getTopic() {
        return Arrays.asList(new PatternTopic("string*"));
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String channel = new String(message.getChannel());
        String content = message.toString();
        logger.info("Message Received from <" + channel + ">, publish content : <" + content + ">");
        
    }
}
