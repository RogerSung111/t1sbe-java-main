package com.tripleonetech.sbe.redis.listener;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.privilege.SitePrivilege;
import com.tripleonetech.sbe.privilege.SitePrivilegeMapper;

@Service
public class T1SBEMessageListenerService {

    private final static Logger logger = LoggerFactory.getLogger(T1SBEMessageListenerService.class);

    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;

    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    /**
     * Receive published message from T1SMP. 有Assign site_privilege & delete 才會到T1SBE處理
     */
    public void setGameApiConfig(String content) {
        logger.debug("content : {}", content);
        List<GameApiConfig> configList = JsonUtils.jsonToObjectList(content, GameApiConfig.class);
        List<GameApiConfig> newConfigList = new ArrayList<>();
        //Only update specific properties
        configList.forEach(config ->{
            GameApiConfig gameApiConfig = new GameApiConfig();
            gameApiConfig.setId(config.getId());
            gameApiConfig.setCode(config.getCode());
            gameApiConfig.setName(config.getName());
            gameApiConfig.setCurrency(config.getCurrency());
            gameApiConfig.setMeta(config.getMeta());
            gameApiConfig.setMd5(config.getMd5());
            gameApiConfig.setRemark(config.getRemark());

            GameApiConfig existedGameApiConfig = gameApiConfigMapper.getIgnoreDeleted(config.getId());
            //Existed API
            if (existedGameApiConfig != null) {
                if (config.getStatus().equals(ApiConfigStatusEnum.DELETED)) {
                    gameApiConfig.setStatus(ApiConfigStatusEnum.DELETED);
                }else {
                    gameApiConfig.setStatus(existedGameApiConfig.getStatus());
                }
            //New API
            }else {
                SitePrivilege sitePrivilege = sitePrivilegeMapper.getSitePrivilege(SitePrivilegeTypeEnum.GAME_API, config.getCode());
                if(null == sitePrivilege || sitePrivilege.isActive() == false ) {
                    gameApiConfig.setStatus(ApiConfigStatusEnum.INACTIVE);
                }else {
                    gameApiConfig.setStatus(ApiConfigStatusEnum.DISABLED);
                }
            }
            newConfigList.add(gameApiConfig);
        });
        if (!CollectionUtils.isEmpty(newConfigList)) {
            gameApiConfigMapper.upsert(newConfigList);
        }
    }

}
