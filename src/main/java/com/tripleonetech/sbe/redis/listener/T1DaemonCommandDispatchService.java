package com.tripleonetech.sbe.redis.listener;


import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tripleonetech.sbe.redis.publisher.scheduled.HeartbeatPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tripleonetech.sbe.announcement.AnnouncementService;
import com.tripleonetech.sbe.command.CommandHistory;
import com.tripleonetech.sbe.command.CommandHistoryMapper;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.creditpoint.CreditPointDaemonService;
import com.tripleonetech.sbe.privilege.SitePrivilegeService;
import com.tripleonetech.sbe.redis.T1DaemonCommand;
import com.tripleonetech.sbe.redis.T1DaemonMethodEnum;

@Profile("main")
@Service
public class T1DaemonCommandDispatchService {
    private static final Logger logger = LoggerFactory.getLogger(T1DaemonCommandDispatchService.class);

    @Autowired
    private AnnouncementService announcementService;
    @Autowired
    private SitePrivilegeService sitePrivilegeService;
    @Autowired
    private CreditPointDaemonService creditPointService;
    @Autowired
    private T1SBEMessageListenerService t1SBEMessageListenerService;
    @Autowired
    private HeartbeatPublisher heartbeatPublisher;

    @Autowired
    private Constant constant;
    @Autowired
    private CommandHistoryMapper commandHistoryMapper;

    public void dispatch(T1DaemonCommand command) {
        T1DaemonMethodEnum method = command.getMethodName();
        Map<String, String> receivers = command.getApplicableUsers();

        for (Map.Entry<String, String> entry : receivers.entrySet()) {
            logger.debug("Executor : [ServerId:{} , UserName:{}]", entry.getKey(), entry.getValue());
        }

        if (receivers.containsKey(constant.getDaemon().getServerId())) {
            CommandHistory commandHistory = new CommandHistory();
            Object content = command.getContent();
            commandHistory.setContent(content instanceof String ? (String) content : JsonUtils.objectToJson(content));
            commandHistory.setCommandType(CommandHistory.CommandTypeEnum.RECEIVE);
            commandHistory.setMethodName(command.getMethodName().toString());
            commandHistoryMapper.insert(commandHistory);

            switch (method) {
                case REFRESH_HEARTBEAT:
                    try {
                        // sleep for a random time to avoid batch request of heartbeat flooding SMP server
                        long sleepTime = (long) (Math.random() * 10000);
                        Thread.sleep(sleepTime);
                        heartbeatPublisher.publisher();
                    } catch (JsonProcessingException | InterruptedException e) {
                        logger.error("Failed to republish heartbeat data", e);
                    }
                    break;
                case SET_PRIVILEGE:
                    sitePrivilegeService.setPrivilege(command.getContent());
                    break;
                case DELETE_PRIVILEGE:
                    sitePrivilegeService.deletePrivilege(command.getContent());
                    break;
                case ANNOUNCEMENT:
                    announcementService.addAnnouncement(command.getContent().toString());
                    break;
                case CREDIT_POINT_EDIT:
                case CREDIT_POINT_STATUS:
                    creditPointService.processRequest(command.getContent());
                    break;
                case API_CONFIG:
                    t1SBEMessageListenerService.setGameApiConfig(command.getContent().toString());
                    break;
                default:
                    logger.error("Daemon: method [{}] does not exist", command);
            }
        }
    }
}
