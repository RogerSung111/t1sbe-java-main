package com.tripleonetech.sbe.redis.listener;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.redis.T1DaemonCommand;
import com.tripleonetech.sbe.redis.TopicMessageListener;

/**
 * Example for the listener in daemon client
 * @author Nicolas
 *
 */
@Profile("main")
@Component
public class T1DaemonMessageListener extends TopicMessageListener {
    private Logger logger = LoggerFactory.getLogger(T1DaemonMessageListener.class);

    @Autowired
    private T1DaemonCommandDispatchService redisMessageDispatchService;
    @Autowired
    private Constant constant;

    @Override
    public Collection<Topic> getTopic() {
        return Arrays.asList(new ChannelTopic("down"));
    }
    
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String channel = new String(message.getChannel());
        String serverPublicKey = constant.getDaemonServer().getPubKey();
        T1DaemonCommand object = verifyAndGetData(message, serverPublicKey);
        String content = Objects.isNull(object) ? "Object is null" : object.getMethodName().name();
        redisMessageDispatchService.dispatch(object);
        logger.info("MessageListener: " + this.getClass().getSimpleName() + " received from <" + channel + ">, publish content : <" + content + ">");
    }
}
