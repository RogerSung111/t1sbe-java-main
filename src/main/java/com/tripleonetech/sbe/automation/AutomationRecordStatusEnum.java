package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.BaseCodeEnum;

import java.util.Arrays;

public enum AutomationRecordStatusEnum implements BaseCodeEnum {
    UNDEFINED(0), IN_PROGRESS(1), COMPLETED(10), CANCELED(20), ERROR(21);

    private int code;

    AutomationRecordStatusEnum(int code){
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static AutomationRecordStatusEnum valueOf(int code){
        return Arrays.stream(AutomationRecordStatusEnum.values()).filter(e -> e.getCode() == code).findAny().orElse(null);
    }
}
