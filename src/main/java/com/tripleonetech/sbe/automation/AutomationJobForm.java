package com.tripleonetech.sbe.automation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.JsonUtils;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class AutomationJobForm {
    @ApiModelProperty(hidden = true, value="ID of this record")
    private Integer id;
    @ApiModelProperty(value = "Username", required = true)
    private String name;
    @NotEmpty
    @ApiModelProperty(value = "Filters selects player to apply action. GROUP(0), TAG(1), DEPOSIT(2), BET(3); ANY(\"any\"), ALL(\"all\"), NOT_ANY(\"notany\")," +
            " EQUAL(\"=\"), NOT_EQUAL(\"!=\"), LESS_THAN(\"<\"), MORE_THAN(\">\"), LESS_THAN_EQUAL(\"<=\"), MORE_THAN_EQUAL(\">=\")")
    private List<List<PlayerFilter>> filters;
    @ApiModelProperty(value = "JSON value of filters property", hidden = true)
    @JsonIgnore
    private String filtersJson;
    @NotEmpty
    @ApiModelProperty(value = "Actions to take against filtered players. ASSIGN_GROUP(0), ASSIGN_TAG(1), DELETE_TAG(2), DISABLE_PLAYER(3), DISABLE_CASHBACK(4), DISABLE_PROMO(5)")
    private List<AutomationAction> actions;
    @ApiModelProperty(value = "JSON value of actions property", hidden = true)
    @JsonIgnore
    private String actionsJson;
    @ApiModelProperty(value = "When should this job start running")
    private ZonedDateTime startAt;
    @ApiModelProperty(value = "Number of times this job should run, 0 means no limit")
    private Integer maxRun;
    @NotNull
    @ApiModelProperty(value = "How frequent this job should run: " +
            "NEVER(0), EVERY_MINUTE(1), EVERY_HOUR(2), EVERY_DAY(3), EVERY_WEEK(4), EVERY_MONTH(5)",
            allowableValues = "0,1,2,3,4,5")
    private Byte runFrequency;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<List<PlayerFilter>> getFilters() {
        return filters;
    }

    public void setFilters(List<List<PlayerFilter>> filters) {
        this.filters = filters;
        this.filtersJson = JsonUtils.objectToJson(filters);
    }

    public String getFiltersJson() {
        return filtersJson;
    }

    public void setFiltersJson(String filtersJson) {
        this.filtersJson = filtersJson;
        this.filters = JsonUtils.jsonTo2DObjectList(filtersJson, PlayerFilter.class);
    }

    public List<AutomationAction> getActions() {
        return actions;
    }

    public void setActions(List<AutomationAction> actions) {
        this.actions = actions;
        this.actionsJson = JsonUtils.objectToJson(actions);
    }

    public String getActionsJson() {
        return actionsJson;
    }

    public void setActionsJson(String actionsJson) {
        this.actionsJson = actionsJson;
        this.actions = JsonUtils.jsonToObjectList(actionsJson, AutomationAction.class);
    }

    public LocalDateTime getStartAtLocal() {
        return DateUtils.zonedToLocal(startAt);
    }

    public ZonedDateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(ZonedDateTime startAt) {
        this.startAt = startAt;
    }

    public Integer getMaxRun() {
        return maxRun;
    }

    public void setMaxRun(Integer maxRun) {
        this.maxRun = maxRun;
    }

    public Byte getRunFrequency() {
        return runFrequency;
    }

    public void setRunFrequency(Byte runFrequency) {
        this.runFrequency = runFrequency;
    }
}
