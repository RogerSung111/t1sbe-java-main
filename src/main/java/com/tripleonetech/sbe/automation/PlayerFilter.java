package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.BaseCodeEnum;

import java.util.Arrays;
import java.util.List;

/**
 * Used to represent a Filter condition in an Automation Job, used to select list of players satisfying this filter
 */
public class PlayerFilter {
    private int id;
    private List<String> value;
    private String operator;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public enum PlayerFilterEnum implements BaseCodeEnum {
        GROUP(0), TAG(1), DEPOSIT(2), BET(3), REGISTER_DATE(4);

        private int code;
        PlayerFilterEnum(int code) {
            this.code = code;
        }

        @Override
        public int getCode() {
            return code;
        }

        public static PlayerFilterEnum parse(int code) {
            return Arrays.stream(PlayerFilterEnum.values()).filter(e -> e.code == code).findFirst().orElse(null);
        }
    }

    public enum PlayerFilterOperatorEnum {
        // Operators for list data
        ANY("any"), ALL("all"), NOT_ANY("notany"),
        // Operators for value data
        EQUAL("="), NOT_EQUAL("!="), LESS_THAN("<"), MORE_THAN(">"), LESS_THAN_EQUAL("<="), MORE_THAN_EQUAL(">="),
        // Operators for time data
        BEFORE("before"), AFTER("after");

        private String operator;
        PlayerFilterOperatorEnum(String operator) {
            this.operator = operator;
        }

        public String getOperator(){
            return this.operator;
        }

        public static PlayerFilterOperatorEnum parse(String operator) {
            return Arrays.stream(PlayerFilterOperatorEnum.values()).filter(e -> e.operator.equals(operator)).findFirst().orElse(null);
        }
    }
}
