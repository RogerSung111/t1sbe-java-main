package com.tripleonetech.sbe.automation;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.game.log.GameLogHourlyReportMapper;
import com.tripleonetech.sbe.player.PlayerCredentialMapper;
import com.tripleonetech.sbe.player.PlayerMapper;
import com.tripleonetech.sbe.player.PlayerStatsMapper;
import com.tripleonetech.sbe.player.PlayerStatusEnum;
import com.tripleonetech.sbe.player.tag.PlayerTag;
import com.tripleonetech.sbe.player.tag.PlayerTagMapper;
import com.tripleonetech.sbe.player.tag.PlayersPlayerTagMapper;

@Service
// This service contains the actual logics of 'filter' and 'action'
public class AutomationService {
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerTagMapper playerTagMapper;
    @Autowired
    private PlayersPlayerTagMapper playersPlayerTagMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private PlayerStatsMapper playerStatsMapper;
    @Autowired
    private GameLogHourlyReportMapper gameLogHourlyReportMapper;

    public List<Integer> getPlayerIdsByFilter(List<List<PlayerFilter>> filters) {
        Set<Integer> unionPlayerIds = new HashSet<>();

        for (List<PlayerFilter> filterList : filters) {
            List<Integer> intersectionPlayerIds = new ArrayList<>();
            for (PlayerFilter filter : filterList) {
                List<Integer> tempPlayerIds = new ArrayList<>();

                PlayerFilter.PlayerFilterEnum filterEnum = PlayerFilter.PlayerFilterEnum.parse(filter.getId());
                String operator = filter.getOperator();
                PlayerFilter.PlayerFilterOperatorEnum operatorEnum = PlayerFilter.PlayerFilterOperatorEnum.parse(operator);

                switch (filterEnum) {
                    case GROUP:
                    case TAG:
                        List<Integer> tagIds = filter.getValue().stream().map(Integer::valueOf).collect(Collectors.toList());
                        switch (operatorEnum) {
                            case ANY:
                                tempPlayerIds = playersPlayerTagMapper.getPlayerIdHasAnyTagOf(tagIds);
                                break;
                            case ALL:
                                tempPlayerIds = playersPlayerTagMapper.getPlayerIdHasAllTagOf(tagIds);
                                break;
                            case NOT_ANY:
                                tempPlayerIds = playersPlayerTagMapper.getPlayerIdNotAnyTagOf(tagIds);
                                break;
                            default:
                                break;
                        }
                        break;
                    case DEPOSIT:
                        tempPlayerIds = playerStatsMapper.getPlayerIdByFilterDeposit(
                                SiteCurrencyEnum.valueOf(filter.getValue().get(0)),
                                operator, filter.getValue().get(1));
                        break;
                    case BET:
                        tempPlayerIds = gameLogHourlyReportMapper.getPlayerIdByFilter(
                                SiteCurrencyEnum.valueOf(filter.getValue().get(0)),
                                operator, filter.getValue().get(1));
                        break;
                    case REGISTER_DATE:
                        LocalDateTime createdAt = DateUtils.zonedToLocal(ZonedDateTime.parse(filter.getValue().get(0)));
                        tempPlayerIds = playerCredentialMapper.getPlayerIdByFilterCreatedAt(operator, createdAt);
                        break;
                    default:
                        break;
                }

                if (CollectionUtils.isEmpty(intersectionPlayerIds)) {
                    intersectionPlayerIds.addAll(tempPlayerIds);
                } else {
                    intersectionPlayerIds = intersectionPlayerIds.stream()
                            .filter(tempPlayerIds::contains).collect(Collectors.toList());
                }
            }
            unionPlayerIds.addAll(intersectionPlayerIds);
        }

        return new ArrayList<>(unionPlayerIds);
    }

    @Transactional
    public void applyActions(List<AutomationAction> actions, List<Integer> playerIds) {
        for (AutomationAction action : actions) {
            AutomationAction.AutomationActionEnum actionEnum = AutomationAction.AutomationActionEnum.parse(action.getId());
            switch (actionEnum) {
                case ASSIGN_GROUP: // assign group to specific players
                    List<Integer> allAddGroupIds = action.getValue().stream().map(Integer::valueOf).collect(Collectors.toList());
                    List<List<Integer>> tagSplitList = splitTagWhetherGroup(allAddGroupIds);
                    // No action if group is empty
                    if (tagSplitList.get(1).isEmpty()) {
                        return;
                    }

                    // Each player only have one group, except for the specified group, delete the other groups
                    if (tagSplitList.get(2).size() > 0) {// Delete
                        batchExecute(playerIds, list -> playersPlayerTagMapper.deleteBatch(list, tagSplitList.get(2)));
                    }

                    // Only add first item in group list
                    batchExecute(playerIds, list -> playersPlayerTagMapper.insertMultiPlayersWithOneTag(list,
                            tagSplitList.get(1).get(0)));
                    break;
                case ASSIGN_TAG: // assign tags to specific players
                    List<Integer> allAddTagIds = action.getValue().stream().map(Integer::valueOf).collect(Collectors.toList());
                    List<List<Integer>> splitList = splitTagWhetherGroup(allAddTagIds);
                    // if non-group tag
                    splitList.get(0).forEach(tag -> batchExecute(playerIds,
                            list -> playersPlayerTagMapper.insertMultiPlayersWithOneTag(list, tag)));
                    break;
                case DELETE_TAG: // delete tags of specific players
                    List<Integer> tagIds = action.getValue().stream().map(Integer::valueOf).collect(Collectors.toList());
                    batchExecute(playerIds, list -> playersPlayerTagMapper.deleteBatch(list, tagIds));
                    break;
                case DISABLE_PLAYER: // set status in player to BLOCKED
                    batchExecute(playerIds, list -> playerMapper.updateStatusBatch(list, PlayerStatusEnum.BLOCKED));
                    break;
                case DISABLE_CASHBACK:  // set cashbackStatus in player to BLOCKED
                    batchExecute(playerIds, list -> playerMapper.updateCashbackEnabledBatch(list, false));
                    break;
                case DISABLE_CAMPAIGN: // set promoStatus in player to BLOCKED
                    batchExecute(playerIds, list -> playerMapper.updateCampaignEnabledBatch(list, false));
                    break;
                case DISABLE_WITHDRAW: // set withdrawStatus in player to BLOCKED
                    batchExecute(playerIds, list -> playerMapper.updateWithdrawEnabledBatch(list, false));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param list
     * @param groupTags
     * @return index: </br>
     *         0: Non-group tags to be added;</br>
     *         1: Group tag to be added; </br>
     *         2: Except for the first group tag, the remaining group tags have to be deleted
     */
    private List<List<Integer>> splitTagWhetherGroup(List<Integer> list) {
        List<PlayerTag> allGroupTags = playerTagMapper.listAll(true);
        List<Integer> allGroupTagIds = allGroupTags.stream().map(tag -> tag.getId()).collect(Collectors.toList());

        List<List<Integer>> lists = new ArrayList<>(list.stream()
                .collect(Collectors.partitioningBy(s -> allGroupTagIds.contains(s)))
                .values());
        if (lists.get(1).size() > 0) {
            List<Integer> toDeleteGroupTagIds = allGroupTagIds.stream().filter(tagId -> !tagId.equals(lists.get(1).get(0)))
                    .collect(Collectors.toList());
            lists.add(toDeleteGroupTagIds);
        }

        return lists;
    }

    private <T> void batchExecute(List<T> datas, Consumer<? super List<T>> action) {
        Objects.requireNonNull(action);
        if (null == datas || datas.size() == 0) {
            return;
        }

        List<List<T>> dataCollection = splitList(datas);
        for (List<T> t : dataCollection) {
            action.accept(t);
        }
    }

    private int perUpdate = 100;

    private <T> List<List<T>> splitList(List<T> datas) {
        List<List<T>> dataCollection = new ArrayList<>();
        int countOfSkip = 0;
        while (countOfSkip < datas.size()) {
            List<T> list = datas.stream()
                    .skip(countOfSkip)
                    .limit(perUpdate)
                    .collect(Collectors.toList());
            dataCollection.add(list);

            countOfSkip += perUpdate;
        }

        return dataCollection;
    }
}
