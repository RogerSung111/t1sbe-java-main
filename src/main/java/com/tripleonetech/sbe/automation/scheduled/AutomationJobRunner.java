package com.tripleonetech.sbe.automation.scheduled;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tripleonetech.sbe.automation.*;

@Profile("scheduled")
@Component
public class AutomationJobRunner {

    @Autowired
    private AutomationJobMapper automationJobMapper;

    @Autowired
    private AutomationJobExecution automationJobExecution;

    @Scheduled(fixedDelay = 60000) // 1 minute
    public void runJobs() {
        List<AutomationJob> jobs = automationJobMapper.listExecutableJob();
        jobs.forEach(automationJobExecution::executeAutomatically);
    }

}
