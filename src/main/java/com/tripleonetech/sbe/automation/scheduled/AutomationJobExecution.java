package com.tripleonetech.sbe.automation.scheduled;

import com.tripleonetech.sbe.automation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AutomationJobExecution {
    private static final Logger logger = LoggerFactory.getLogger(AutomationJobExecution.class);

    @Autowired
    private AutomationService automationService;

    @Autowired
    private AutomationRecordMapper automationRecordMapper;

    public void executeManually(AutomationJob job) {
        AutomationRecord record = generateAutomationRecord(job);
        record.setAutomated(false);
        execute(job, record);
    }

    public void executeAutomatically(AutomationJob job) {
        // According to the frequency to judge whether to execute
        LocalDateTime lastRunTime = automationRecordMapper.getLastAutomatedRunTime(job.getId());
        if (!this.isAutomaticallyExecutable(AutomationJobRunFrequencyEnum.valueOf(job.getRunFrequency().intValue()),
                lastRunTime)) {
            return;
        }

        AutomationRecord record = generateAutomationRecord(job);
        record.setAutomated(true);
        execute(job, record);
    }

    private void execute(AutomationJob job, AutomationRecord record) {
        // Not started yet
        if (job.getStartAtLocal().isAfter(LocalDateTime.now())) {
            return;
        }

        // Save execution record
        record.setStatus((byte) AutomationRecordStatusEnum.IN_PROGRESS.getCode());
        automationRecordMapper.insert(record);

        // Execute auto actions
        try {
            List<Integer> playerIds = automationService.getPlayerIdsByFilter(job.getFilters());
            record.setAffectedPlayerIds(playerIds.stream().map(String::valueOf).collect(Collectors.joining(",")));
            record.setAffectedPlayerCount(playerIds.size());
            automationService.applyActions(job.getActions(), playerIds);
            record.setStatus((byte) AutomationRecordStatusEnum.COMPLETED.getCode());

        } catch (Exception e) {
            record.setStatus((byte) AutomationRecordStatusEnum.ERROR.getCode());
            logger.error("Exception: ", e);
        }

        // Completed
        record.setCompletedAt(LocalDateTime.now());
        automationRecordMapper.update(record);
    }

    private AutomationRecord generateAutomationRecord(AutomationJob job) {
        AutomationRecord record = new AutomationRecord();
        record.setJobId(job.getId());
        record.setJobName(job.getName());
        record.setRunCount(job.getRunCount() + 1);
        return record;
    }

    private boolean isAutomaticallyExecutable(AutomationJobRunFrequencyEnum runFrequency, LocalDateTime lastRunTime) {
        // If runFrequency is NEVER, then return false
        if (AutomationJobRunFrequencyEnum.NEVER == runFrequency) {
            return false;
        }

        // first run
        if (null == lastRunTime) {
            return true;
        }

        LocalDateTime availableRunTime = null;
        switch (runFrequency) {
            case EVERY_MINUTE:
                availableRunTime = lastRunTime.plusMinutes(1);
                break;
            case EVERY_HOUR:
                availableRunTime = lastRunTime.plusHours(1);
                break;
            case EVERY_DAY:
                availableRunTime = lastRunTime.plusDays(1);
                break;
            case EVERY_WEEK:
                availableRunTime = lastRunTime.plusWeeks(1);
                break;
            case EVERY_MONTH:
                availableRunTime = lastRunTime.plusMonths(1);
                break;
            default:
                return false;
        }

        return !LocalDateTime.now().isBefore(availableRunTime);
    }
}
