package com.tripleonetech.sbe.automation;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface AutomationRecordMapper {
    AutomationRecord get(Long id);
    LocalDateTime getLastAutomatedRunTime(Integer jobId);

    List<AutomationRecord> list(@Param("query") AutomationRecordQueryForm query);

    int insert(AutomationRecord record);

    int update(AutomationRecord record);
}