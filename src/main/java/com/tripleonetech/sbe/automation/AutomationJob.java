package com.tripleonetech.sbe.automation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;
import com.tripleonetech.sbe.common.JsonUtils;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class AutomationJob {
    private Integer id;
    private String name;
    private List<List<PlayerFilter>> filters;
    @JsonIgnore
    private String filtersJson;
    private List<AutomationAction> actions;
    @JsonIgnore
    private String actionsJson;
    private LocalDateTime startAt;
    private Integer maxRun;
    private Byte runFrequency;
    private boolean deleted;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    // Joined columns
    private Integer runCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public List<List<PlayerFilter>> getFilters() {
        return filters;
    }

    public void setFilters(List<List<PlayerFilter>> filters) {
        this.filters = filters;
        this.filtersJson = JsonUtils.objectToJson(filters);
    }

    public String getFiltersJson() {
        return filtersJson;
    }

    public void setFiltersJson(String filtersJson) {
        this.filtersJson = filtersJson;
        this.filters = JsonUtils.jsonTo2DObjectList(filtersJson, PlayerFilter.class);
    }

    public List<AutomationAction> getActions() {
        return actions;
    }

    public void setActions(List<AutomationAction> actions) {
        this.actions = actions;
        this.actionsJson = JsonUtils.objectToJson(actions);
    }

    public String getActionsJson() {
        return actionsJson;
    }

    public void setActionsJson(String actionsJson) {
        this.actionsJson = actionsJson;
        this.actions = JsonUtils.jsonToObjectList(actionsJson, AutomationAction.class);
    }

    public ZonedDateTime getStartAt() {
        return DateUtils.localToZoned(startAt);
    }

    @ApiModelProperty(hidden = true, value = "Derived property, should not show")
    public LocalDateTime getStartAtLocal() {
        return startAt;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public Integer getMaxRun() {
        return maxRun;
    }

    public void setMaxRun(Integer maxRun) {
        this.maxRun = maxRun;
    }

    public Byte getRunFrequency() {
        return runFrequency;
    }

    public void setRunFrequency(Byte runFrequency) {
        this.runFrequency = runFrequency;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(updatedAt);
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getRunCount() {
        return runCount;
    }

    public void setRunCount(Integer runCount) {
        this.runCount = runCount;
    }
}