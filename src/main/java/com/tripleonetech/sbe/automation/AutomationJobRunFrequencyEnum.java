package com.tripleonetech.sbe.automation;

import java.util.Arrays;

import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum AutomationJobRunFrequencyEnum implements BaseCodeEnum {
    NEVER(0), EVERY_MINUTE(1), EVERY_HOUR(2), EVERY_DAY(3), EVERY_WEEK(4), EVERY_MONTH(5);

    private int code;

    AutomationJobRunFrequencyEnum(int code){
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    public static AutomationJobRunFrequencyEnum valueOf(int code) {
        return Arrays.stream(AutomationJobRunFrequencyEnum.values()).filter(e -> e.getCode() == code).findAny()
                .orElse(null);
    }
}
