package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.BaseCodeEnum;

import java.util.Arrays;
import java.util.List;

/**
 * The action in an Automation Job. To define action to take against filtered players.
 */
public class AutomationAction {
    private int id;
    private List<String> value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public enum AutomationActionEnum implements BaseCodeEnum {
        ASSIGN_GROUP(0), ASSIGN_TAG(1), DELETE_TAG(2), DISABLE_PLAYER(3), DISABLE_CASHBACK(4), DISABLE_CAMPAIGN(5), DISABLE_WITHDRAW(6);

        private int code;
        AutomationActionEnum(int code) {
            this.code = code;
        }

        @Override
        public int getCode() {
            return code;
        }

        public static AutomationActionEnum parse(int code) {
            return Arrays.stream(AutomationActionEnum.values()).filter(e -> e.code == code).findFirst().orElse(null);
        }
    }
}
