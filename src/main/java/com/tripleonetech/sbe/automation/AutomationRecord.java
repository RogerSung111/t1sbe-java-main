package com.tripleonetech.sbe.automation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tripleonetech.sbe.common.DateUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class AutomationRecord {
    private Long id;
    private Integer jobId;
    private String jobName;
    private Integer runCount;
    private Byte status;
    private boolean automated;
    @JsonIgnore
    private String affectedPlayerIds;
    private Integer affectedPlayerCount;
    private LocalDateTime createdAt;
    private LocalDateTime completedAt;
    private LocalDateTime updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public Integer getRunCount() {
        return runCount;
    }

    public void setRunCount(Integer runCount) {
        this.runCount = runCount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public boolean isAutomated() {
        return automated;
    }

    public void setAutomated(boolean automated) {
        this.automated = automated;
    }

    public String getAffectedPlayerIds() {
        return affectedPlayerIds;
    }

    public void setAffectedPlayerIds(String affectedPlayerIds) {
        this.affectedPlayerIds = affectedPlayerIds;
    }

    public Integer getAffectedPlayerCount() {
        return affectedPlayerCount;
    }

    public void setAffectedPlayerCount(Integer affectedPlayerCount) {
        this.affectedPlayerCount = affectedPlayerCount;
    }

    public ZonedDateTime getCreatedAt() {
        return DateUtils.localToZoned(createdAt);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getCompletedAt() {
        return DateUtils.localToZoned(completedAt);
    }

    public void setCompletedAt(LocalDateTime completedAt) {
        this.completedAt = completedAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return DateUtils.localToZoned(updatedAt);
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
