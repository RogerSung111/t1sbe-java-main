package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.model.NoPageQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AutomationJobMapper {
    AutomationJob get(@Param("id") int id);

    List<AutomationJob> list(@Param("query") NoPageQueryForm query);

    List<AutomationJob> listExecutableJob();

    int delete(Integer id);

    int insert(AutomationJobForm record);

    int update(AutomationJobForm record);
}