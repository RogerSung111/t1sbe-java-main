package com.tripleonetech.sbe.automation;

import com.tripleonetech.sbe.common.model.PageQueryForm;
import io.swagger.annotations.ApiModelProperty;

public class AutomationRecordQueryForm extends PageQueryForm {
    @ApiModelProperty(hidden = true, value="This will be an URL parameter, so hidden in query form object")
    private Integer jobId;
    private Byte status;

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}
