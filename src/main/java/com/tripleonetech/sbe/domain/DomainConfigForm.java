package com.tripleonetech.sbe.domain;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class DomainConfigForm {
    
    @ApiModelProperty(value = "Domain config ID. Don't have to provide [id] when create a new domain name ", required = false)
    private Integer id;
    
    @NotNull
    @ApiModelProperty(value = "domain name", required = true)
    private String name;
    
    @NotNull
    @ApiModelProperty(value = "Enable domain name config, defualt FALSE ", required = false)
    private Boolean enabled;

    @NotNull
    @ApiModelProperty(value = "Use domain name at backoffice or frontoffice, BackOffice(0), FrontOffice(1)"
        , required = true, allowableValues = "0,1")
    private Integer site;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getSite() {
        return site;
    }

    public void setSite(Integer site) {
        this.site = site;
    }

}
