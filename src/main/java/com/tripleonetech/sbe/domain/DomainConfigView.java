package com.tripleonetech.sbe.domain;

import io.swagger.annotations.ApiModelProperty;

public class DomainConfigView {
    
    @ApiModelProperty(value = "Domain config ID")
    private Integer id;
    @ApiModelProperty(value = "Domain name")
    private String name;
    @ApiModelProperty(value = "Is the domain name enabled?")
    private Boolean enabled;
    @ApiModelProperty(value = "Domain name is used as backoffice or frontoffice. BackOffice (0), FrontOffice (1) ")
    private Integer site;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getSite() {
        return site;
    }

    public void setSite(Integer site) {
        this.site = site;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static DomainConfigView create(DomainConfig domainConfig) {
        DomainConfigView view = new DomainConfigView();
        view.setId(domainConfig.getId());
        view.setSite(domainConfig.getSite());
        view.setName(domainConfig.getName());
        view.setEnabled(domainConfig.getEnabled());
        return view;
    }
}
