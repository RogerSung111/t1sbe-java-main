package com.tripleonetech.sbe.domain;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DomainConfigMapper {
    
    List<DomainConfig> list();
    
    DomainConfig get(Integer id);
    
    void batchInsert(List<DomainConfigForm> form);
    
    //Implement betchupdate by using insert on duplicated
    void batchUpdate(List<DomainConfigForm> form);
        
    void batchDelete(List<Integer> ids);
    
    void enableDomainConfig(@Param("id") Integer id, @Param("enabled") Boolean enabled);
    
    
}
