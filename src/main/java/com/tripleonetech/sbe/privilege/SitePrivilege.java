package com.tripleonetech.sbe.privilege;

import java.time.LocalDate;

import com.tripleonetech.sbe.common.integration.UserPrivilegeView;

public class SitePrivilege {
    
    private String type;
    private String value;
    private Boolean active;
    private LocalDate createdAt;
    private LocalDate updatedAt;
   
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public Boolean isActive() {
        return active;
    }
    public void setActive(Boolean active) {
        this.active = active;
    }
    public LocalDate getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
    public LocalDate getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    public static SitePrivilege create(UserPrivilegeView userPrivilegeView, boolean isActive) {
        SitePrivilege sitePrivilege = new SitePrivilege();
        sitePrivilege.setType(userPrivilegeView.getType());
        sitePrivilege.setValue(userPrivilegeView.getValue());
        sitePrivilege.setActive(isActive);
        return sitePrivilege;
    }
   
}
