package com.tripleonetech.sbe.privilege;

public class SiteCurrencyView {
    
    private String currency;
    private Boolean active;
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public Boolean isActive() {
        return active;
    }
    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public static SiteCurrencyView create(SitePrivilege sitePrivilege) {
        SiteCurrencyView siteCurrencyView = new SiteCurrencyView();
        siteCurrencyView.setCurrency(sitePrivilege.getValue());
        siteCurrencyView.setActive(sitePrivilege.isActive());
        return siteCurrencyView;
    }
}
