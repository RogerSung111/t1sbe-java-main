package com.tripleonetech.sbe.privilege;

public class SiteTemplateView {

    private String template;

    private Boolean active;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public static SiteTemplateView create(SitePrivilege sitePrivilege) {
        SiteTemplateView siteCurrencyView = new SiteTemplateView();
        siteCurrencyView.setTemplate(sitePrivilege.getValue());
        siteCurrencyView.setActive(sitePrivilege.isActive());
        return siteCurrencyView;
    }
}
