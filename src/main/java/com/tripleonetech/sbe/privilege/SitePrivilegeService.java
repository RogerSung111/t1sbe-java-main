package com.tripleonetech.sbe.privilege;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import com.tripleonetech.sbe.common.integration.ApiConfigStatusEnum;
import com.tripleonetech.sbe.common.integration.UserPrivilegeView;
import com.tripleonetech.sbe.game.config.GameApiConfig;
import com.tripleonetech.sbe.game.config.GameApiConfigMapper;
import com.tripleonetech.sbe.payment.config.PaymentApiConfigMapper;
import com.tripleonetech.sbe.player.*;
import com.tripleonetech.sbe.withdraw.config.WithdrawApiConfigMapper;

@Service
public class SitePrivilegeService {
    
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerCredentialMapper playerCredentialMapper;
    @Autowired
    private PlayerMapper playerMapper;
    @Autowired
    private GameApiConfigMapper gameApiConfigMapper;
    @Autowired
    private PaymentApiConfigMapper paymentApiConfigMapper;
    @Autowired
    private WithdrawApiConfigMapper withdrawApiConfigMapper;
    
    
    private static final Logger logger = LoggerFactory.getLogger(SitePrivilegeService.class);

    public void setPrivilege(Object content) {
        
        List<SitePrivilege> newSitePrivileges = new ArrayList<>();
        //convert site_privilege_view to site_privilege
        List<SitePrivilege> activeSitePrivileges = JsonUtils.jsonToObjectList(String.valueOf(content), UserPrivilegeView.class).
                stream().map(userPrivilege ->  SitePrivilege.create(userPrivilege, true)).collect(Collectors.toList());
        
        //find out active to inactive site_privilege
        List<SitePrivilege> currentSitePrivileges = sitePrivilegeMapper.allSitePrivileges();
        for(SitePrivilege currentSitePrivilege : currentSitePrivileges) {
            boolean isActivePrivilege = false;
            for(SitePrivilege activeSitePrivilege : activeSitePrivileges) {
                if(activeSitePrivilege.getType().concat(activeSitePrivilege.getValue())
                        .equals(currentSitePrivilege.getType().concat(currentSitePrivilege.getValue()))) {
                    isActivePrivilege = true;
                    break;
                }
            }
            //Change active site privilege as inactive
            if(!isActivePrivilege && currentSitePrivilege.isActive()) {
                currentSitePrivilege.setActive(false);
                newSitePrivileges.add(currentSitePrivilege);
            }           
        }
        //inactive + active
        newSitePrivileges.addAll(activeSitePrivileges);
        sitePrivilegeMapper.batchUpdateInsert(newSitePrivileges);
        //Process currency
        List<SitePrivilege> currencySitePrivileges = newSitePrivileges.stream()
                .filter(privilege -> privilege.getType().equals(SitePrivilegeTypeEnum.SITE_CURRENCY.name())).collect(Collectors.toList());
        processSiteCurrency(currencySitePrivileges);
        //Process game api
        List<SitePrivilege> gameSitePrivileges = newSitePrivileges.stream()
                .filter(privilege -> privilege.getType().equals(SitePrivilegeTypeEnum.GAME_API.name())).collect(Collectors.toList());
        processGameApi(gameSitePrivileges);
        
    }

    @Transactional
    public void deletePrivilege(Object content) {
        UserPrivilegeView userPrivilegeView = new ObjectMapper().convertValue(content, UserPrivilegeView.class);
        SitePrivilege privilege = new SitePrivilege();
        privilege.setType(userPrivilegeView.getType());
        privilege.setValue(userPrivilegeView.getValue());
        sitePrivilegeMapper.deleteBy(privilege);
        logger.info("Delete sitePrivilege - Type:{}, Value:{}", privilege.getType(), privilege.getValue());
    }
    
    /**
     * 接收到推送的SITE_PRIVILEGE.GAME_API需要進行的相對應動作
     */
    public void processGameApi(List<SitePrivilege> gameSitePrivileges) {

        gameSitePrivileges.forEach(sitePrivilege -> {
            //SitePrivilege Status = Active
            if(sitePrivilege.isActive()) {
                List<GameApiConfig> gameApiConfigs = gameApiConfigMapper.listByCode(sitePrivilege.getValue());
                gameApiConfigs.forEach(gameApiConfig -> {
                    //API Status
                    if(gameApiConfig.getStatus().equals(ApiConfigStatusEnum.INACTIVE)) {
                        gameApiConfigMapper.updateStatusByCode(sitePrivilege.getValue(), ApiConfigStatusEnum.DISABLED);
                    }
                });
            //SitePrivilege Status = Inactive
            }else {
                gameApiConfigMapper.updateStatusByCode(sitePrivilege.getValue(), ApiConfigStatusEnum.INACTIVE);
            }
        });
    }
    
    /**
     * 接收到推送的SITE_PRIVILEGE.SITE_CURRENCY 需要進行的相對應動作
     */
    public void processSiteCurrency(List<SitePrivilege> currencySitePrivileges) {
        currencySitePrivileges.forEach(sitePrivilege ->{
            SiteCurrencyEnum siteCurrency = SiteCurrencyEnum.valueOf(sitePrivilege.getValue());
            if (sitePrivilege.isActive()) {
                addNewSiteCurrency(siteCurrency);
            } else {
                removeSiteCurrency(siteCurrency);
            }
        });
    }
    /**
     * When add a new currency into T1SBE, Should adjust following settings
     * 1. Add player
     * 2. Add player_profile
     * 3. Add wallet
     * 4. xxx_api.enable 保持disabled, 由子站點自行切換
     */
    private void addNewSiteCurrency(SiteCurrencyEnum newCurrency) {
        //Add player, player_profile and wallet
        List<PlayerCredential> playerCredentials = playerCredentialMapper.findNeedAddingPlayers(newCurrency);
        playerCredentials.forEach(playerCredential -> {
            playerService.generatePlayer(Player.create(playerCredential.getUsername(), newCurrency, PlayerStatusEnum.CURRENCY_INACTIVE));
        });
    }
    
    /**
     * When remove a currency from T1SBE, Should adjust following settings
     * 1. Change player.status to CURRENCY_INACTIVE
     * 2. disable APIs (game, payment, withdraw)
     */
    @Transactional
    private void removeSiteCurrency(SiteCurrencyEnum removeCurrency) {
        List<Player> players = playerMapper.listByCurrency(removeCurrency);
        players.forEach(player -> {
            playerService.updateStatus(player.getId(), PlayerStatusEnum.CURRENCY_INACTIVE);
        });
        
        gameApiConfigMapper.enableApiByCurrency(removeCurrency, ApiConfigStatusEnum.INACTIVE);
        paymentApiConfigMapper.enableApiByCurrency(removeCurrency, ApiConfigStatusEnum.INACTIVE);
        withdrawApiConfigMapper.enableApiByCurrency(removeCurrency, ApiConfigStatusEnum.INACTIVE);
    }
}
