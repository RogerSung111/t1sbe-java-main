package com.tripleonetech.sbe.privilege;

import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.SiteCurrencyEnum;
import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

@Repository
public class SiteConfigs {

    private static Map<SitePrivilegeTypeEnum, List<?>> sitePrivileges = new EnumMap<>(SitePrivilegeTypeEnum.class);

    private static Map<SitePrivilegeTypeEnum, LocalDateTime> lastSyncTimes = new EnumMap<>(SitePrivilegeTypeEnum.class);

    static {
        Stream.of(SitePrivilegeTypeEnum.values()).forEach(e -> lastSyncTimes.put(e, LocalDateTime.now()));
    }
    @Autowired
    private SitePrivilegeMapper sitePrivilegeMapper;
    @Autowired
    private Constant constant;

    @SuppressWarnings("unchecked")
    public List<String> getAvailableAPI(SitePrivilegeTypeEnum type) {
        checkExpired(type);
        if (!sitePrivileges.containsKey(type)) {
            sitePrivileges.put(type, sitePrivilegeMapper.list(type));
        }
        return (List<String>) sitePrivileges.get(type);
    }

    /**
     * Decide what language support be offered in front end
     * 
     * @return List<Locale>
     */
    @SuppressWarnings("unchecked")
    public List<Locale> getSiteLanguages() {
        checkExpired(SitePrivilegeTypeEnum.SITE_LANGUAGE);
        if (CollectionUtils.isEmpty(sitePrivileges.get(SitePrivilegeTypeEnum.SITE_LANGUAGE))) {
            List<String> languageStrings = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.SITE_LANGUAGE);
            sitePrivileges.put(SitePrivilegeTypeEnum.SITE_LANGUAGE, parseListStringToLocale(languageStrings));
        }
        return (List<Locale>) sitePrivileges.get(SitePrivilegeTypeEnum.SITE_LANGUAGE);
    }

    /**
     * Decide what currency support be offered in front end
     * 
     * @return List<Locale>
     */
    @SuppressWarnings("unchecked")
    public List<SiteCurrencyEnum> getSiteCurrencies() {
        checkExpired(SitePrivilegeTypeEnum.SITE_CURRENCY);
        if (CollectionUtils.isEmpty(sitePrivileges.get(SitePrivilegeTypeEnum.SITE_CURRENCY))) {
            List<String> currencyStrings = sitePrivilegeMapper.list(SitePrivilegeTypeEnum.SITE_CURRENCY);
            sitePrivileges.put(SitePrivilegeTypeEnum.SITE_CURRENCY, parseListStringToSiteCurrency(currencyStrings));
        }
        return (List<SiteCurrencyEnum>) sitePrivileges.get(SitePrivilegeTypeEnum.SITE_CURRENCY);
    }

    private List<Locale> parseListStringToLocale(List<String> strs) {
        List<Locale> tmp = new ArrayList<Locale>();
        strs.stream().forEach(s -> tmp.add(Locale.forLanguageTag(s)));
        return Collections.unmodifiableList(tmp);
    }

    private List<SiteCurrencyEnum> parseListStringToSiteCurrency(List<String> strs) {
        List<SiteCurrencyEnum> tmp = new ArrayList<SiteCurrencyEnum>();
        strs.stream().forEach(s -> tmp.add(SiteCurrencyEnum.valueOf(s)));
        return Collections.unmodifiableList(tmp);
    }

    private void checkExpired(SitePrivilegeTypeEnum type) {
        if (lastSyncTimes.get(type).until(LocalDateTime.now(), ChronoUnit.MINUTES) >= constant.getSitePrivilege().getRefresh()) {
            sitePrivileges.remove(type);
            lastSyncTimes.put(type, LocalDateTime.now());
        }
    }
}
