package com.tripleonetech.sbe.privilege;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tripleonetech.sbe.common.SitePrivilegeTypeEnum;

@Mapper
public interface SitePrivilegeMapper {

    List<String> list(SitePrivilegeTypeEnum type);
    
    List<SitePrivilege> getSitePrivileges(SitePrivilegeTypeEnum type);
    
    int set(@Param("type") String type, @Param("value") String value);
    
    SitePrivilege getSitePrivilege(@Param("type") SitePrivilegeTypeEnum type,@Param("value") String value);
    
    void batchUpdateInsert(List<SitePrivilege> sitePrivileges);
    
    void delete ();
    
    int deleteBy(SitePrivilege privilege);

    List<SitePrivilege> allSitePrivileges();
}