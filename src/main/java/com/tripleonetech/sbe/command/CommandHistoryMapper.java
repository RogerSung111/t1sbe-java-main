package com.tripleonetech.sbe.command;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommandHistoryMapper {

    int insert(CommandHistory commandHistory);

    CommandHistory get(int id);

}
