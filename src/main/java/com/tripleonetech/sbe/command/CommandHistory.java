package com.tripleonetech.sbe.command;

import com.tripleonetech.sbe.common.BaseCodeEnum;

import java.time.LocalDateTime;

public class CommandHistory {

    private Integer id;
    private String content;
    private String methodName;
    private CommandTypeEnum commandType;

    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public CommandTypeEnum getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandTypeEnum commandType) {
        this.commandType = commandType;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public enum CommandTypeEnum implements BaseCodeEnum {
        RECEIVE(0), PUBLISH(1);

        private final int code;

        CommandTypeEnum(int code) {
            this.code = code;
        }

        @Override
        public int getCode() {
            return this.code;
        }

        public static CommandTypeEnum valueOf(Integer code){
            return code == 1 ? PUBLISH : RECEIVE;
        }
    }

}
