package com.tripleonetech.sbe.iprule.country;

public interface GeoIpService {

    String getCountryCode(String ip);
}
