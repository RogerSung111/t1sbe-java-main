package com.tripleonetech.sbe.iprule.country;

import com.fasterxml.jackson.databind.JsonNode;
import com.tripleonetech.sbe.common.Constant;
import com.tripleonetech.sbe.common.JsonUtils;
import com.tripleonetech.sbe.common.integration.ApiResponseEnum;
import com.tripleonetech.sbe.common.integration.ApiResult;
import com.tripleonetech.sbe.common.integration.ConfigurableHttpService;
import com.tripleonetech.sbe.game.integration.GameApiResult;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Service
public class GeoIpServiceViaApi extends ConfigurableHttpService implements GeoIpService {

    private static final Logger logger = LoggerFactory.getLogger(GeoIpServiceViaApi.class);

    @Autowired
    private Constant constant;

    @Override
    public String getCountryCode(String ip) {
        
        try {
            String token = fetchAuthToken();
            if (Objects.isNull(token)) {
                logger.error("[{}] gets token failed", getClass().getSimpleName());
                return null;
            }
            String url = constant.getGeo().getUrl() + "/location/position";
            Map<String, String> headers = new HashMap<>();
            headers.put("X-MER-TOKEN", constant.getGeo().getId() + ":" + token);
            Map<String, String> params = new HashMap<>();
            params.put("ip", ip);
            HttpResponse response = getHttpService().submitGet(url, params, headers);
            int httpStatusCode = getResponseStatus(response);
            String resText = getResponseText(response);
            if (httpStatusCode != HttpStatus.SC_OK) {
                logger.error("Unexpected HTTP status: [{}] while invoke url: [{}]", httpStatusCode,
                        url);
                return null;
            }
            JsonNode resJsonNode = JsonUtils.jsonToJsonNode(resText);
            if(resJsonNode.get("country_code").isNull()) {
                return null;
            }
            return resJsonNode.get("country_code").textValue();
        } catch (ParseException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private static AuthTokenInfo authTokenCache = null;

    public String fetchAuthToken() {
        if (null != authTokenCache
                && authTokenCache.getTimeoutDatetime()
                        .isAfter(LocalDateTime.now().plusSeconds(10))) {
            return authTokenCache.getAuthToken();
        }

        ApiResult apiResult = generateAuthToken();
        JsonNode resJsonNode = (JsonNode) apiResult.getParam("response");
        if (!apiResult.isSuccess()) {
            logger.error("Failed to generate auth token");
            return "";
        }

        authTokenCache = new AuthTokenInfo();
        authTokenCache.setAuthToken(resJsonNode.get("token").textValue());
        authTokenCache.setTimeoutDatetime(LocalDateTime.now().plusSeconds(resJsonNode.get("ttl").intValue()));
        return authTokenCache.getAuthToken();
    }

    private ApiResult generateAuthToken() {
        String url = constant.getGeo().getUrl() + "/access/authentication";
        Map<String, Object> params = new HashMap<>();
        params.put("id", constant.getGeo().getId());
        params.put("unix", ZonedDateTime.now(ZoneId.systemDefault()).toEpochSecond());
        signParam(params);
        HttpResponse response = getHttpService().postAsJsonData(url, params);
        return handleResponse(response);
    }

    /**
     * -- Handling response --
     * 
     * @param response
     * @return
     */
    private ApiResult handleResponse(HttpResponse response) {
        if (response == null) {
            logger.error("Empty response");
            return new GameApiResult(ApiResponseEnum.SERVICE_UNAVAILABLE);
        }

        try {
            int httpStatusCode = getResponseStatus(response);
            if (httpStatusCode != HttpStatus.SC_OK) {
                logger.error("Unexpected HTTP status: [{}]", httpStatusCode);
                return new GameApiResult(ApiResponseEnum.SERVICE_UNAVAILABLE);
            }
            String responseText = getResponseText(response);
            JsonNode resJsonNode = JsonUtils.jsonToJsonNode(responseText);

            ApiResult result = new ApiResult(ApiResponseEnum.OK);
            result.setParam("response", resJsonNode);
            return result;
        } catch (ParseException | IOException e) {
            logger.error("Failed to parse response: [{}]", e);
            return new GameApiResult(ApiResponseEnum.MALFORMED_REPLY);
        }
    }

    /**
     * Concat all non-empty values to one string , append sign key to last, then use SHA1 to get signature
     * Ignore any json type field and sign field
     * 
     * @param params
     * @return
     */
    private Map<String, Object> signParam(Map<String, Object> params) {
        // remove parameter[sign]
        params.remove("sign");

        // The order should be sorted by parameter name in alphabetical order
        SortedMap<String, String> sortedMap = new TreeMap<String, String>();
        for (String k : params.keySet()) {
            if (params.get(k) instanceof Map) {
                continue;
            }
            if (params.get(k) instanceof List<?>) {
                continue;
            }
            sortedMap.put(k, params.get(k).toString());
        }

        Collection<String> paramValues = sortedMap.values();
        String signData = StringUtils.join(paramValues, "");
        signData = StringUtils.join(signData, constant.getGeo().getSignKey());

        logger.info("Signing data [{}] with signKey [{}]", signData, constant.getGeo().getSignKey());
        String signature = sign(signData);
        params.put("sign", signature);
        return params;
    }

    /**
     * use SHA1 to get signature
     *
     * @param data
     * @return
     */
    private static String sign(String data) {
        return DigestUtils.sha1Hex(data);
    }

    // --- HTTP response handling ---
    /**
     * Parses http status code from http response object
     *
     * @param response
     * @return
     */
    private int getResponseStatus(HttpResponse response) {
        int statusCode = response.getStatusLine().getStatusCode();
        logger.debug("For response hash: [{}], Status code: [{}]", response.hashCode(), statusCode);
        return statusCode;
    }

    /**
     * Parses http return text from response object
     *
     * @param response
     * @return
     * @throws ParseException
     * @throws IOException
     */
    private String getResponseText(HttpResponse response) throws ParseException, IOException {
        String responseBody = EntityUtils.toString(response.getEntity());
        logger.debug("For response hash: [{}], Response body: [{}]", response.hashCode(), responseBody);
        return responseBody;
    }

    public static class AuthTokenInfo {
        String authToken;
        LocalDateTime timeoutDatetime;

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public LocalDateTime getTimeoutDatetime() {
            return timeoutDatetime;
        }

        public void setTimeoutDatetime(LocalDateTime timeoutDatetime) {
            this.timeoutDatetime = timeoutDatetime;
        }

    }
}
