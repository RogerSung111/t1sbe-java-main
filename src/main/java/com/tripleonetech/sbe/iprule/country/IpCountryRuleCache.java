package com.tripleonetech.sbe.iprule.country;

import java.util.LinkedHashMap;
import java.util.Map;

public class IpCountryRuleCache {

    @SuppressWarnings("serial")
    private static Map<String, String> ipCountryCache = new LinkedHashMap<String, String>() {
        @Override
        protected boolean removeEldestEntry(@SuppressWarnings("rawtypes") final Map.Entry eldest) {
            return size() > 1500;
        }
    };

    public static String getCountry(String ip) {
        return ipCountryCache.get(ip);
    }

    public static String setCountry(String ip, String countryCode) {
        return ipCountryCache.put(ip, countryCode);
    }

    public static Boolean hasVisited(String ip) {
        return ipCountryCache.containsKey(ip);
    }

}
