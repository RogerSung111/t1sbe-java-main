package com.tripleonetech.sbe.iprule.country;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IpRuleCountryMapper {

    Boolean getEnabled(String countryCode);

    List<IpRuleCountryView> list(@Param("onlyEnabled") boolean onlyEnabled,
                                 @Param("locale") String locale,
                                 @Param("defaultLocale") String defaultLocale);

    int clearEnabled();

    int setEnabled(@Param("countryCodes") List<String> countryCodes);
}