package com.tripleonetech.sbe.iprule;

import io.swagger.annotations.ApiModelProperty;

public class IpRuleForm {

    @ApiModelProperty(value = "Enter IP address")
    private String ip;
    @ApiModelProperty(hidden = true)
    private IpRuleConfigSiteEnum site;
    private String remark;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public IpRuleConfigSiteEnum getSite() {
        return site;
    }

    public void setSite(IpRuleConfigSiteEnum site) {
        this.site = site;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
