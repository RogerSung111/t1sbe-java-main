package com.tripleonetech.sbe.iprule;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum IpRuleConfigSiteEnum implements BaseCodeEnum {

    BACK_OFFICE(0), FRONT_OFFICE(1);

    private int code;

    IpRuleConfigSiteEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static IpRuleConfigSiteEnum valueOf(Integer code) {
        IpRuleConfigSiteEnum[] enumConstants = IpRuleConfigSiteEnum.values();
        for (IpRuleConfigSiteEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
