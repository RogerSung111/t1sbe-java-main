package com.tripleonetech.sbe.iprule;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IpRuleMapper {
    Boolean insert(IpRule ipRule);

    List<IpRule> list(@Param("site") Integer site,
                      @Param("query") IpRuleQueryForm query,
                      @Param("pageNum") Integer pageNum,
                      @Param("pageSize") Integer pageSize);

    IpRule selectByPrimaryKey(Integer id);

    Boolean updateByPrimaryKeySelective(IpRule record);

    Boolean deleteById(Integer id);

    List<String> listFrontOfficeBlacklist();

    List<String> listBackOfficeWhitelist();
}
