package com.tripleonetech.sbe.iprule;

import io.swagger.annotations.ApiModelProperty;

import com.tripleonetech.sbe.common.model.PageQueryForm;

public class IpRuleQueryForm extends PageQueryForm {
    @ApiModelProperty(value = "Search by IP address")
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
