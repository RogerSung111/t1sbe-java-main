package com.tripleonetech.sbe.cms;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContentStoreMapper {

    List<ContentStore> list();

    ContentStoreView get(String key);

    int upsert(ContentStore cms);

    int delete(String key);

}