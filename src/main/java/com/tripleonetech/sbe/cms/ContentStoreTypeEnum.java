package com.tripleonetech.sbe.cms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.tripleonetech.sbe.common.BaseCodeEnum;

public enum ContentStoreTypeEnum implements BaseCodeEnum {
    HTML(0), JSON(1), TEXT(2);
    
    private int code;

    ContentStoreTypeEnum(int code) {
        this.code = code;
    }

    @Override
    @JsonValue
    public int getCode() {
        return this.code;
    }
    
    @JsonCreator
    public static ContentStoreTypeEnum valueOf(Integer code) {
        ContentStoreTypeEnum[] enumConstants = ContentStoreTypeEnum.values();
        for (ContentStoreTypeEnum e : enumConstants) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
