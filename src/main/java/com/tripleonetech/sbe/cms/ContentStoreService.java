package com.tripleonetech.sbe.cms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContentStoreService {
    
    private final static Logger logger = LoggerFactory.getLogger(ContentStoreService.class);
    
    @Autowired
    private ContentStoreMapper storeMapper;

    public Map<String, ContentStoreView> getAll() {
        List<ContentStore> list = storeMapper.list();
        
        Map<String, ContentStoreView> map = new HashMap<>();
        for (ContentStore storeEntry : list) {
            ContentStoreView cmsView = new ContentStoreView();
            cmsView.setType(storeEntry.getType());
            cmsView.setContent(storeEntry.getContent());
            map.put(storeEntry.getKey(), cmsView);
        }
        return map;
    }

    public ContentStoreView get(String key) {
        return storeMapper.get(key);
    }

    public int save(String key, ContentStoreView view) {
        ContentStore storeEntry = new ContentStore();
        storeEntry.setKey(key);
        storeEntry.setType(view.getType());
        storeEntry.setContent(view.getContent());
        return storeMapper.upsert(storeEntry);
    }

    public int delete(String key) {
        return storeMapper.delete(key);
    }
}
