package com.tripleonetech.sbe.cms;

public class ContentStoreView {

    private ContentStoreTypeEnum type;

    private String content;

    public ContentStoreTypeEnum getType() {
        return type;
    }

    public void setType(ContentStoreTypeEnum type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}