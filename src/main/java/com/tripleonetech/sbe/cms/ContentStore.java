package com.tripleonetech.sbe.cms;

import java.time.LocalDateTime;

public class ContentStore {

    private String key;

    private ContentStoreTypeEnum type;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private String content;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key == null ? null : key.trim();
    }

    public ContentStoreTypeEnum getType() {
        return type;
    }

    public void setType(ContentStoreTypeEnum type) {
        this.type = type;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}