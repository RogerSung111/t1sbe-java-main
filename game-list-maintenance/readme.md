#GameList Maintenance Steps

1. Export `game_description` and `game_type` table from PHP DB
2. Rename tables to `game_type_php` and `game_description_php` and after that import to JAVA DB
3. Check whether there is any new GameType (may use `game_type_mapping_ref.xlsx/Sheet2`, SBEJ-805) 
4. Manually re-define Game Types under `game_type_php` IF there is any new GameType (based on the default defined GameTypes under JAVA SBE db `game_type` table)
5. Update re-defined GameTypes to `game_type_php` table (update into afterMigrate_GameList.sql before running it)
6. Run afterMigrate_GameList.sql (update GameTypeIds into `game_description_mapping_php` table, generate data to `game_name` and insert data to `game_name_i18n` from `game_description_mapping_php` and to drop all unused tables)