#!/bin/bash
BASEDIR=$(dirname "$0")
echo "Copying db config for reset demo db: $BASEDIR/application-db-$1.properties ==> ./src/test/resources/application-db-test.properties"
cp -f $BASEDIR/$1/application-db-$1.properties ./src/test/resources/application-db-test.properties
cp -f $BASEDIR/$1/logback-spring-$1.xml ./src/test/resources/logback-spring.xml
echo "Remove afterMigrate as those data are for unit test only"
rm ./src/test/resources/db/migration/afterMigrate.sql
echo "Starting unit test for one: mvn -Dtest=SpringApplicationTest test"
mvn -Dtest=SpringApplicationTest test