#!/bin/bash
BASEDIR=$(dirname "$0")
deploy_server=$3

echo "Writing current time and branch information to properties file ==> $BASEDIR/application-$1.properties"
printf "\nconstant.swagger.last-updated=$(date)" > $BASEDIR/$2/application-$1.properties
BRANCH_NAME=$CI_COMMIT_REF_NAME
if [ -z "$BRANCH_NAME" ]; then
  BRANCH_NAME="master"
fi
echo "BRANCH_NAME: $BRANCH_NAME"
printf "\nconstant.swagger.branch=$BRANCH_NAME\n" >> $BASEDIR/$2/application-$1.properties

# Logback related config
printf "\nlogback.name=$1\n" >> $BASEDIR/$2/application-$1.properties
printf "\nlogging.config=file:webapps-$1-$2/logback-spring.xml\n" >> $BASEDIR/$2/application-$1.properties

mvn package -P $1 -Dmaven.test.skip=true

if [ $? -eq 0 ]; then
  echo "Packaging done, deploying: target/$1.war ===> vagrant@$deploy_server:~/deploy/gitlab-ci/$2/$1.war"
  scp $BASEDIR/$2/application-db-$2.properties vagrant@$deploy_server:~/deploy/gitlab-ci/$2/application-db-$1.properties
  scp $BASEDIR/$2/application-redis-$2.properties vagrant@$deploy_server:~/deploy/gitlab-ci/$2/application-redis-$1.properties
  scp $BASEDIR/$2/application-$1.properties vagrant@$deploy_server:~/deploy/gitlab-ci/$2/application-$1.properties
  scp $BASEDIR/$2/logback-spring-$2.xml vagrant@$deploy_server:~/deploy/gitlab-ci/$2/logback-spring-$1.xml
  scp target/$1.war vagrant@$deploy_server:~/deploy/gitlab-ci/$2/
  echo "Deployment done"
fi
