#!/bin/bash
BASEDIR=$(dirname "$0")
echo "Copying db config for unit test: $BASEDIR/application-db-test.properties ==> ./src/test/resources/application-db-test.properties"
cp -f $BASEDIR/application-db-test.properties ./src/test/resources/
cp -f $BASEDIR/application-redis-test.properties ./src/test/resources/
cp -f $BASEDIR/logback-spring-test.xml ./src/test/resources/logback-spring.xml
echo "Starting unit test: mvn test"
mvn test