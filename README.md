# T1SBE Main

[![pipeline status](https://git.smartbackend.com/sbe/t1sbe-java-main/badges/master/pipeline.svg)](https://git.smartbackend.com/sbe/t1sbe-java-main/commits/master)

Main project of the Java version of T1SBE system.

## Design Documents

The project exercises Code As Documentation practice. Coding standard and limited documentation is available in the [wiki](/wikis).

The daemon

## Setup

* Copy src/main/resources/sample.properties/* into src/main/resources/
* Copy src/test/resources/sample.properties/* into src/test/resources/

Edit the properties files to connect to your local db.

In Spring Tool Suite, configure Boot Dashboard with different run configurations, follow [this tutorial](https://spring.io/blog/2016/03/29/the-spring-boot-dashboard-in-sts-part-5-working-with-launch-configurations). You should at least configure for `main` and `api` profile.

Note: If developing using eclipse, consider import eclipse-formatter-java.xml to your formatter to keep coding style consistent.

## OAuth 2.0

The API uses OAuth 2.0 authentication's password and refresh token grants. Reference: https://www.oauth.com/oauth2-servers/access-tokens/

### Obtain OAuth access_token (login)

* Link: [http://localhost:8080/oauth/token](http://localhost:8080/oauth/token)
* Header
	- Content-Type: application/x-www-form-urlencoded
	- authorization: Basic [the auth string]
	- Header's auth string comes from `.withClient("").secret("")` in `AuthorizationServerConfigProvider.java`; it uses `constant.oauth.client`, `constant.oauth.password` properties defined in `application-api.properties`
		- You can also see this info populated on swagger UI Authorize form
		- [Here is a link to generate auth header manually](https://www.blitter.se/utils/basic-authentication-header-generator/), or you can [do this within postman](https://harperdbhelp.zendesk.com/hc/en-us/articles/115010250207-Basic-Auth-with-Postman)
		- **Important: Header's auth credential is defined per server deployment, please do not use body's username and password, superadmin / abcdef, to generate header's auth string.**
* Body (for admin backend login)
	- grant_type: password
	- username: superadmin
	- password: abcdef
* Body (for player login)
	- grant_type: password
	- username: test002
	- password: 123456
* Sample return value
```
	{
    "access_token": "91793c76-6067-4483-a70b-9ea046f31fc1",
    "token_type": "bearer",
    "refresh_token": "78a1747d-0066-4840-a663-bca581b80e9c",
    "expires_in": 119,
    "scope": "read write"
	}
```

> Note: The access_token expires after seconds specified in 'expires_in'. The client need to use refresh_token to obtain a new access_token when/before access_token expires.

### Obtain OAuth access_token (refresh)

* Header
	- (same as previous)
* Body
	- grant_type: refresh_token
	- refresh_token: [the refresh token]
* Sample return value
	- (same as previous)

### Access resource with access_token

* Example link: [http://localhost:8001/api/game/](http://localhost:8001/api/game/)
* Header
	- authorization: Bearer [access_token obtained from last step]

## Swagger

Example link: [http://localhost:8765/swagger-ui.html](http://localhost:8765/swagger-ui.html)

Note that Swagger UI enables oauth authentication, click the "Authorize" button before accessing any API.

## Maven build

Goal: package  
profile: see below

## Profiles (executable)

Important note: `db` and `db-test` profiles **MUST NOT be included in any profiles**, they must be explicitly specified in pom or unit test annotation.

### main (default, port = 8765)

This profile supplies backend API. Swagger UI enabled.

### api (port = 8001)

This profile supplies frontend API.

### daemon

Project runs as T1Daemon client.

### scheduled

Project runs scheduled jobs only. The execution beans are defined in specific sub-packages named `*.scheduled` and annotated using `@Scheduled`.

## Profiles (non-executable)

### db

Defines _production_ database connection and credentials. Must be explicitly specified in pom. Must NOT be used in unit test.

### db-test

Defines _test_ database connection and credentials. Must be explicitly specified in unit test annotation.

### redis

Defines connection credentials to connect to redis. Usually included by **daemon** profile only.

## Time Zone Convention

* Website default timezone: front-end should use GET:/site-config/timezone to obtain website's default timezone.
* Frontend <-> Backend: Times should be passed in the [ISO-8601](http://en.wikipedia.org/wiki/ISO_8601) format. Example: `2019-08-31T06:36:22+08:00` or `2019-08-31T06:36:22Z`. Frontend should convert received times to website default timezone for display.
* Backend <-> DB: All times are in UTC. Backend should convert received times to UTC before storing to DB.

## Times used in Game Logs

`bet_time` is used in Game Logs search and calculation. Details can be found in [this ticket](https://manage.tripleonetech.net/jira/browse/SBEJ-606).



## 一鍵式架站
目的：透過SMP(Daemon server) 可以遠端或本地去執行[一鍵式架站]腳本，快速搭建起SBE前台與後台以及排程程式。

下列六個Script，會依照順序逐一執行來建立SBE前台與後台，而各個腳本運作目標說明如下。
##### 1. one_clieck_to_deploy.sh : 
一鍵部署的流程控制：建立DB -> 建立域名 -> 打包war -> 打包vue -> 部署各專案至Tomcat -> 執行排程（scheduled, daemon)

##### 2. add_new_live_zone.sh
建立域名：www.{server-id}.t1t.games & admin.{server-id}.t1t.games
＊備註：此腳本由zuma提供。

##### 3. create_database.sh : 
建立客戶的SBE資料庫，資料庫命名採用 sbe_"server_id"
例如：sbe_t1t123

##### 4. run_pack_war.sh : 
＊此腳本會透過trigger piepline而執行（參照.gitlab-ci.yml)
透過此腳本運行，將會打包下列檔案並將檔案傳送至要部署的遠端客戶資料夾(Tomcat server)

部署的檔案：
(1) api.war, main.war, scheduled.jar
(2) application-db-api.properties, application-db-main.properties
(3) application-api.properties, application-main.properties, application-scheduled.properties
(4) logback-spring-api.xml, logback-spring-main.xml,logback-spring-scheduled.xml
(5) application-redis.properties

##### 5.run_pack_vue.sh
＊此腳本會透過trigger piepline而執行（參照.gitlab-ci.yml)
透過此腳本會將前端的代碼gitpull並進行npm run build:prod,
並將產生的dist/ 部署至server 此路徑/home/vagrant/deploy/gitlab-ci/frontend/底下, 
並備份前端代碼至客戶目錄下（ main-frontend, api-frontend）

備註：tomcat server客戶目錄範例： vagrant@t1tjavademo:~/deploy/site/{server_id}

##### 6.deploy_to_tomcat.sh
部署 api.war, main.war, api-frontend, main-frontend 以及相關的 *.properties 到tomcat server底下
 
部署路徑如下：(部署到tomcat的專案以軟連結到客戶目錄)
${Catalina_Base}/{server_id}/main => /home/vagrant/deploy/site/{server_id}/main
${Catalina_Base}/{server_id}/api
${Catalina_Base}/{server_id}/main-frontend
${Catalina_Base}/{server_id}/api-frontend

##### Templates : 
1. api.xml => 用於定義後台程式 path & env. properties （需替換內部值,經由deploy_to_tomcat.sh ）
2. ROOT.xml => 用於定義前台 path （需替換內部值,經由deploy_to_tomcat.sh）
3. logback-spring.xml => 純部署使用 （不需要修改）
4. application-scheduled.properties => 作為default值 (經由deploy_to_tomcat.sh）
5. application-redis.properties => 純部署使用（不需要修改）
6. host.txt => tomcat/server.xml 新增的樣板

## 一鍵式架站更新前端「game」·「backoffice」
 到Gitlab-CI分別運行下列stage:
 1. Demo one click update backoffice - 將重新發佈「backoffice-前端代碼」到所以有一鍵架站的站點 Ex: /home/vagrant/deploy/site/2ft412/main-frontend
 2. Demo one click update game - 將重新發佈「game-前端代碼」到所以有一鍵架站的站點 Ex: /home/vagrant/deploy/site/2ft412/api-frontend
 

## SitePrivilege_Game_API 改動的相對應處理
|  API 狀態 |  前/後台 |  遊戲列表 |  遊戲紀錄 |  遊戲錢包 |  遊戲Config/報表 |
| ------------ | ------------ | ------------ | ------------ | ------------ | ------------ |
| Disabled  | 前台  |  1. 不顯示在首頁遊戲大廳列表 |   |  1. 玩家無法再轉入錢，只可以轉出 |   |
| Disabled  | 後台  |   |  1. 即使game_api 是disabled狀態， 遊戲同步也要能繼續跑同步 |   |  1. 要能正常顯示所有game_api |
| Deleted  |  前台 |  1. 不顯示在首頁遊戲大廳列表 |   |  1. 錢包不顯示出來 <br> -  Deleted之前, 所有的獎金到子錢包都要做完相對應處理（下線需要把玩家錢包都做轉出） <br> - 如果還有錢在遊戲錢包，就無法再對錢包做轉出的操作 （先不禁止轉錢包操作 - 例如： 一次性轉所有錢到主錢包） |   |
| Deleted  | 後台  |  1. 不顯示在後台遊戲列表 <br> 2. 設置返水百分比時，不應該顯示 [ deleted game_api - game_type] 供選擇 |  1.不執行遊戲紀錄同步（配合sync_enabled) <br>- 停止插入game_logs_task <br> -考慮由daemonserver可以強制disable/deleted (站點是無法enabled) |  1. 錢包不顯示出來 |  1. game api 列表： 預設不顯示deleted資料. <br> /system/game-apis?showDeletedApi=false <br> 2. 報表的下拉game_api選單要能顯示全部支援的game_api <br> /system/game-apis?showDeletedApi=true |

## Site_Currency 改動的相對應處理
| T1SBE-Currency 增減   |  前/後台 | 玩家模組  | 報表  |  玩家錢包  | API Configs 設置  |
| ------------ | ------------ | ------------ | ------------ | ------------ | ------------ |
|  減 | 前台 |  1. 玩家無法登入 |   |   |   |
|  減 |  後台 | 1. 從daemonserver推播移除Currency後, 子站點site_currency更改為active=false, 而player.status = CURRENCY_INACTIVE  |  1. 報表一樣要可以查詢移除掉的currency資料（site_currency下拉表單要全部顯示，不管active/inactive）  |   |  1. Disable 相對應currency的API (xxx_api.enabled = false) |
|  加 |  前台 | 1. 玩家可以使用該currency登入  | ＊可登入即可進行所有操作  |  ＊可登入即可進行所有操作 |   |
|  加 | 後台 |  1. 從daemonserver推播增加Currency後,site_currency更改為active=true <br> 2. 當新增site_currency, 需要新增相對應的 player_profile & player. 並且將player.status = CURRENCY_INACTIVE （當玩家第一次登入就會變為ACTIVE） |   | 1. 當新增site_currency, 需要新增相對應的玩家主錢包  | 1. 當active site currency, 相對應的API不需要更改 enable => ture, 而保持false (切換enable可由子站自行切換）  |

## Daemon 与 Daemon Server 传递信息的文档

[点此下载](wikis/daemon-transfer-info.xlsx)

## Tomcat配置

1. 404導頁 [ref.] https://blog.csdn.net/skystephens/article/details/86547795

/apache-tomcat/conf/web.xml 內新增下列代碼
<error-page>
    <error-code>404</error-code> 
    <location>/index.html</location>
</error-page>

2. 支援gzip

於server.xml 新增下列幾項參數：[ref.] https://www.cnblogs.com/DDgougou/p/8675504.html

1. compression="on" 開啟壓縮。
2. compressionMinSize="1024" 大於1KB的文件才進行壓縮。「默認值2KB」
3. compressableMimeType="text/html,text/xml,application/javascript,text/css,text/plain,text/json, application/json" 會被壓缩的MIME類型。

Example: 
<Connector port="8080"
   protocol="HTTP/1.1"
   connectionTimeout="20000"
   redirectPort="8443"    
   compression="on" 
   compressionMinSize="2048" 
   noCompressionUserAgents="gozilla, traviata"   
   compressableMimeType="text/html,text/xml,text/javascript,application/javascript,text/css,text/plain,text/json,application/json"/>
   
3. 設定80 port請求自動指向8080 port Tomcat 服務
   以root角色執行下列指令, 來達到“不加port 8080，直接通過ip或域名訪問”
   
   1. iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
   2. iptables-save [if run "service iptables save", following error message will show up "iptables:unrecoginzed service" ]
   * https://www.cnblogs.com/EasonJim/p/6851007.html [About "iptables:unrecoginzed service" ]
  